<?php
class CronCommand extends CConsoleCommand {

    public function actionCloseBid(){

        $now = mktime(0, 0, 0, date("m"), date("d"), date("Y"));

        $models = Bid::model()->findAll('opened ='.Bid::OPENED );

        foreach ( $models as $model ) {

            $time = strtotime($model->date_end);

            if( $time <= $now ){

                $model->opened = Bid::CLOSED;

                $model->save( false );

            }

        }

    }

    /**
     * Выкачивание изображений для каталога товаров
     * Адреса изображения хранятся в промежуточной таблице TurnImages
     */
    public function actionUploadImages(){

        $models = TurnImages::model()->queue()->findAll();

        foreach( $models as $model ){
            $model->lock();
        }

        foreach( $models as $model )
        {
            $imageMgr = new ImageManager($model->url);

            if ($imageMgr->uploadImageUrl()) {

                $result = CJSON::decode($imageMgr->getSuccess());

                $model->completed();

                ProductImages::updateImg( $model->img_id, $result['file_name'], $result['img_id'] );

            } else {

                if( $model->number_tries < TurnImages::MAX_TRIES ){

                    $model->unlock();

                }else $model->error();
            }
        }
    }

    /**
     * Автоматическая разблокировка всех позиций картинок у которых застой больше 5 минут
     * Требуется как перестраховка, если вдруг перезагрузили сервер или еще что..
     */
    public function actionUploadImagesUnlock(){
        $models = TurnImages::model()->locked()->findAll();

        foreach($models as $model){
            $model->unlock();
        }
    }

    /**
     * Уведомление об окончании загрузки очереди картинок
     * Так же в этом экшене мы и ищем эти очереди
     */
    public function actionDownloadNotifications(){

        //Все очереди
        $models = TurnImages::model()->processed()->groupPos()->findAll();

        foreach($models as $model){

            if( $model->check() ){

                $msg = new UserMessages('new');

                $errors = TurnImages::model()->errors()->findAll('pos = :pos', array(':pos'=>$model->pos));

                $msg->title     = "Импорт товарных позиций и выгрузка изображений #{$model->pos}.";

                $msg->message   = "Импорт товарных позиций и изображений Прайс-листа закончен. Спасибо.\r\n\r\n";

                if($errors) $msg->message .= "Изображения которые небыли загружены:\r\n\r\n";

                foreach($errors as $e){

                    $msg->message .= $e->url."\r\n";

                }

                $msg->from_uid  = 16;

                $msg->to_uid    = 0;

                $msg->to_vid    = $model->vendor_id;

                if ($msg->save()){

                    $vendor = Vendor::model()->findByPk($model->vendor_id);

                    if ($vendor->notify_new_msg){
			
			            $_SERVER['SERVER_NAME'] = 'test.svitor.ru';

                        $from = User::model()->findByPk(16);

                        UserMessages::sendEmailNotify($vendor, $from, $msg, 'user');
                    }

                    $model->deleteAllPos();
                }
            }
        }

    }

    /**
     * Рассылка сообщает компаниям о конце акции
     * отправка в 01 час по МСК.
     */
    public function actionMailEndStock(){
        $currentDate = date('d-m-Y');
        $date_from = explode("-",$currentDate);
        $nextDate = mktime(22, 0, 0, $date_from[1], $date_from[0], $date_from[2]);

        $criteria            = new CDbCriteria;
        $criteria->condition = 'date_to = :nextDate';
        $criteria->params    = array(
            ':nextDate' => $nextDate
        );

        $vendorStock = VendorStock::model()->with('Vendor')->findAll($criteria);
        $res = '';
        $email = array();
        if (count($vendorStock)) {

            foreach ($vendorStock as $item) {
                $email[] = $item->Vendor->email;
            }
            $oMail = new UniSenderApi( Yii::app()->params['uni_sender_key'] );
            $res = $oMail->createCampaign(
                array(
                     'message_id' => Yii::app()->params['uni_sender_message_id'],
                     'contacts'   => implode(',', $email)
                )
            );
        }

        //Отладочная информация
        $file =DOCUMENT_ROOT.DIRECTORY_SEPARATOR."log".DIRECTORY_SEPARATOR."MailEndStock.txt";
        $f_o=fopen($file,"a+");
        fwrite($f_o, "\n\r---------------------------------------------------");
        fwrite($f_o, "\n\rВыполняется крон - ".Date('m-d-Y H:i:s'));
        fwrite($f_o, "\n\rКоличество закрытых акций - ".count($vendorStock));
        fwrite($f_o, "\n\rДата UnixTime - ".$nextDate);
        fwrite($f_o, "\n\rОтвет сервера - ".$res);
        fwrite($f_o, "\n\rРассылка - ".implode(',', $email));
        fclose($f_o);
    }

    /**
     * Импортировать контакты с сайта в UniSender
     * Запускается кроном каждый день
     * @param bool $all ( Импортировать все или только за 2 дня )
     */
    public function actionImportContactsUnisender( $all = false ){
        if( !$all ){
            $currentDate         = date( 'd-m-Y' );
            $preDate             = date( 'd-m-Y', strtotime( $currentDate ) - 60 * 60 * 48 );
            $date_from           = explode( "-", $preDate );
            $date_to             = explode( "-", $currentDate );

            $starDate            = mktime(0, 0, 0, $date_from[1], $date_from[0], $date_from[2]);
            $endDate             = mktime(23, 59, 59, $date_to[1], $date_to[0], $date_to[2]);

            $criteria            = new CDbCriteria;
            $criteria->condition = 'reg_date >= :starDate AND reg_date <= :endDate';
            $criteria->params    = array(
                ':starDate' => $starDate,
                ':endDate'  => $endDate
            );
        }else $criteria = '';

        $oMail = new UniSenderApi( Yii::app()->params['uni_sender_key'] );

        $countUsers   = $this->updateListUnisenderUser( $criteria, $oMail );

        $countVendors = $this->updateListUnisenderVendor( $criteria, $oMail );

        //Отладочная информация
        $file =DOCUMENT_ROOT.DIRECTORY_SEPARATOR."log".DIRECTORY_SEPARATOR."ImportContactsUnisender.txt";
        $f_o=fopen($file,"a+");
        fwrite($f_o, "\n\r---------------------------------------------------");
        fwrite($f_o, "\n\rВыполняется крон - ".Date('m-d-Y H:i:s'));
        fwrite($f_o, "\n\rКоличество пользователей - ".$countUsers);
        fwrite($f_o, "\n\rКоличество компаний - ".$countVendors);
        fclose($f_o);
    }

    /**
     * Обновляет контакты в uniSender -- пользователи
     *
     * @param string $criteria -- Условие выборки
     * @param        $oMail -- Обертка ( ссылка на объект ) UniSender
     *
     * @return bool
     */
    private function updateListUnisenderUser( $criteria = '', $oMail ){

        if(empty($oMail)) return false;

        $users   = User::model()->findAll( $criteria );

        if( count($users) == 0 ){ return false; }

        $count = count($users);

        $users = array_chunk( $users, 400 );

        foreach( $users as $packUsers ){
            $tempUser = array(
                'field_names' => array('email','email_list_ids'/*коды списков*/,'Name'),
                'data'        => array()
            );
            if( count( $packUsers ) ){
                foreach( $packUsers as $user ){
                    $tempUser['data'][] = array(
                        $user->email,
                        Yii::app()->params['uni_sender_user_list_id'],
                        $user->login,
                    );
                }

                $oMail->importContacts(
                    $tempUser
                );

                sleep(30);
            }
        }
        return $count;
    }

    /**
     * Обновляет контакты в uniSender -- Исполнители
     *
     * @param string $criteria -- Условие выборки
     * @param        $oMail -- Обертка ( ссылка на объект ) UniSender
     *
     * @return bool
     */
    private function updateListUnisenderVendor( $criteria = '', $oMail ){

        if(empty($oMail)) return false;

        $vendors = Vendor::model()->with('Categories')->findAll( $criteria );

        if( count($vendors) == 0 ){ return false; }

        $count = count($vendors);

        $vendors = array_chunk( $vendors, 400 );

        foreach( $vendors as $packUsers ){

            $tempVendor = array(
                'field_names' => array('email','email_list_ids','tag','Name'),
                'data'        => array()
            );

            if( count( $packUsers ) ){
                foreach( $packUsers as $vendor ){
                    $tags = array();
                    if( count($vendor->Categories) ){
                        foreach( $vendor->Categories as $tag ){
                            $tags[] = $tag->Category->title;
                        }
                    }
                    $tempVendor['data'][] = array(
                        $vendor->email,
                        Yii::app()->params['uni_sender_company_list_id'],
                        implode(', ', $tags),
                        $vendor->title,
                    );
                }
                $oMail->importContacts(
                    $tempVendor
                );
                sleep(30);
            }
        }
        return $count;
    }

    public function actionMailWeekly() {


        $week = time() - 604800;

        $criteria = new CDbCriteria;
        $criteria->condition = '(published_date > '.$week.' AND published_date < '.time().')';

        $lastPost = Post::model()->findAll($criteria);

        $messageWeddings = '';
        $messageHolidays = '';
        $messageOtherWeddings = '';
        $messageOtherHolidays = '';

        foreach($lastPost as $post){
            if($post['post_cat_id'] == 1){
                $messageWeddings .= "<h3><a href='http://svitor.ru/post/".$post['id']."'>".$post['title']."</a></h3>";
                $messageWeddings .= $post['content_1'];
                $messageWeddings .= "<a href='http://svitor.ru/post/".$post['id']."'>Читать дальше →</a>";

                $messageOtherHolidays .= "<h3><a href='http://svitor.ru/post/".$post['id']."'>".$post['title']."</a></h3>";
            }else{
                $messageHolidays .= "<h3><a href='http://svitor.ru/post/".$post['id']."'>".$post['title']."</a></h3>";
                $messageHolidays .= $post['content_1'];
                $messageHolidays .= "<a href='http://svitor.ru/post/".$post['id']."'>Читать дальше →</a>";

                $messageOtherWeddings .= "<h3><a href='http://svitor.ru/post/".$post['id']."'>".$post['title']."</a></h3>";
            }
        }

        if(!empty($messageWeddings)){

            if(!empty($messageOtherWeddings)){
                $messageOtherWeddings= "<h3>Остальные статьи:</h3>".$messageOtherWeddings;
            }
            $postArrWeddings = array(
                "title" => "Svitor.ru - новая информация, которая может быть вам полезна.",
                "message" => $messageWeddings.$messageOtherWeddings
            );
        }

        if(!empty($messageHolidays)){

            if(!empty($messageOtherHolidays)){
                $messageOtherHolidays= "<h3>Остальные статьи:</h3>".$messageOtherHolidays;
            }
            $postArrHolidays = array(
                "title" => "Svitor.ru - новая информация, которая может быть вам полезна.",
                "message" => $messageHolidays."<h3>Остальные статьи:</h3>".$messageOtherHolidays
            );
        }

        $weddings = UserWeddings::model()->findAll();
        $holidays = UserHolidays::model()->findAll();

        $userWeddings = array();

        foreach($weddings as $item){
            $userWeddings[] = $item['uid'];
        }

        $userHolidays = array();

        foreach($holidays as $item){
            if (!in_array($item['uid'], $userWeddings)) {
                $userHolidays[] = $item['uid'];
            }
        }

        $userHolidays = array_unique($userHolidays);
        $userWeddings = array_unique($userWeddings);

        if(!empty($postArrWeddings) && !empty($userWeddings)){
            foreach($userWeddings as $wedding){
                // сообщения для пользоватей со свадьбами
                $this->sendMail($postArrWeddings,(int)$wedding);
            }
        }

        if(!empty($postArrHolidays) && !empty($userHolidays)){
            foreach($userHolidays as $holiday){
                // сообщения для пользоватей с праздниками
                $this->sendMail($postArrHolidays,(int)$holiday);
            }
        }
    }

    public function actionMailWeddings() {

        $weekStart = time() - 604800;
        $weekEnd = $weekStart + 600;

        $criteria = new CDbCriteria;
        $criteria->condition = '(date > '.$weekStart.' AND date < '.$weekEnd.')';

        $lastPost = UserWeddings::model()->findAll($criteria);

        $postArr = array(
            "title" => "Дорогие друзья, мы искренне поздравляем вас с бракосочетанием!",
            "message" => 'Дорогие друзья, мы искренне поздравляем вас с бракосочетанием.<br>
            Желаем вам долгих лет счастья и любви. Надеемся, что вы поделитесь опытом подготовки
            и проведения свадьбы с другими молодоженами в разделе "Обмен опытом" и оставите отзывы
            о вашей команде. Уверены, что ваши истории вдохновят многих, не забудьте приложить фотографии!'
        );


        if($lastPost){
            foreach($lastPost as $post){
                $this->sendMail($postArr,(int)$post['uid']);  //
            }
        }
    }

    public function actionMailStock() {

        $fiveStat = time() - 432000;
        $fiveEnd = $fiveStat + 600;

        $criteriaFive = new CDbCriteria;
        $criteriaFive->condition = '(stamp > '.$fiveStat.' AND stamp < '.$fiveEnd.')';

        $fivePost = VendorStock::model()->findAll($criteriaFive);


        $twoStat = time() - 172800;
        $twoEnd = $twoStat + 600;

        $criteriaTwo = new CDbCriteria;
        $criteriaTwo->condition = '(stamp > '.$twoStat.' AND stamp < '.$twoEnd.')';

        $twoPost = VendorStock::model()->findAll($criteriaTwo);


        $oneStat = time() + 86400;
        $oneEnd = $oneStat + 600;

        $criteriaOne = new CDbCriteria;
        $criteriaOne->condition = '(stamp > '.$oneStat.' AND stamp < '.$oneEnd.')';

        $onePost = VendorStock::model()->findAll($criteriaOne);


        if($fivePost){
            foreach($onePost as $post){
                $this->sendMailStock($this->messageStock($post['title'],5),$post->Vendor->email);
            }
        }

        if($twoPost){
            foreach($twoPost as $post){
                $this->sendMailStock($this->messageStock($post['title'],2),$post->Vendor->email);
            }
        }

        if($onePost){
            foreach($twoPost as $post){
                $this->sendMailStock($this->messageStock($post['title'],1),$post->Vendor->email);
            }
        }

        $banStat = time() + 2678400;
        $banEnd = $banStat + 600;

        $criteriaBan = new CDbCriteria;
        $criteriaBan->condition = '(stock_stamp > '.$banStat.' AND stock_stamp < '.$banEnd.')';

        $banPost = Vendor::model()->findAll($criteriaBan);

        if($banPost){
            foreach($banPost as $post){

                $post['is_banned_stock'] = 0;
                $post['stock_stamp'] = '';

                if ($post->save())
                    echo 'ok';
            }
        }
    }

    private function messageStock($title,$day){
        if($day == 1){
            return $postArr = array(
                "title" => "Срок действия Вашей акции истек!",
                "message" => 'Сообщаем Вам что прошел '.$day.' день, после истечения срока действия Вашей акции - '.$title.'!'
            );
        } else {

            return $postArr = array(
                "title" => "Истекает срок акции!",
                "message" => 'Сообщаем Вам что до истечения срока акции - '.$title.' - осталось '.$day.' дней!'
            );
        }
    }

    public function send($arr,$user_id) {

        $to_uid = $user_id;
        $to_vid = 0;

        if (!empty($to_vid)) {
            $to_vid = Vendor::model()->findByPk($to_vid);
        } else {
            $to_uid = User::model()->findByPk($to_uid);
        }

        $msg = new UserMessages('new');

        $msg->attributes = $arr;
        $msg->from_uid = 16;
        $msg->to_uid = !empty($to_uid) ? $to_uid->id : 0;
        $msg->to_vid = !empty($to_vid) ? $to_vid->id : 0;

        if ($msg->save(false))
        {
            $from = User::model()->findByPk($msg->from_uid);

            // Если у получателя включены уведомления по Email - уведомим его о том, что ему прешло сообщение.
            $to = !empty($to_vid) ? $to_vid : $to_uid;


            $this->sendMail($arr,$user_id);

            echo 'ok';
        }
        else {
            echo "error";
        }
    }

    public function sendMail($arr,$user_id) {

        $to_uid = $user_id;
        $to_uid = User::model()->findByPk($to_uid);

        $to = !empty($to_vid) ? $to_vid : $to_uid;

        if (@$to->notify_new_msg){

            $subject = "Новая информация от svitor.ru";
            $headers = "Content-Type: text/html; charset=UTF-8"."\r\n";
            $headers .= "Subject: {$subject}"."\r\n";
            $headers .= 'From: Василиса Свитрова <noreply@svitor.ru>' . "\r\n" .
                'Reply-To: noreply@svitor.ru' . "\r\n";


            // Заполняем нужными данными
            $body = "<h2>".$arr['title']."</h2>";
            $body .= $arr['message']."<br>";
            $body .= "С наилучшими пожеланиями, Svitor.ru ";

            mail($to->email, $subject, $body, $headers);

            echo 'ok';
        }
        else {
            echo "error";
        }
    }

    public function sendMailStock($arr,$email) {

        $subject = "Информация по Вашей акции - svitor.ru";
        $headers = "Content-Type: text/html; charset=UTF-8"."\r\n";
        $headers .= "Subject: {$subject}"."\r\n";
        $headers .= 'From: Василиса Свитрова <noreply@svitor.ru>' . "\r\n" .
            'Reply-To: noreply@svitor.ru' . "\r\n";


        // Заполняем нужными данными
        $body = "<h2>".$arr['title']."</h2>";
        $body .= $arr['message']."<br/>";
        $body .= "С наилучшими пожеланиями, Svitor.ru ";

        mail($email, $subject, $body, $headers);

        echo 'ok';
    }
}
