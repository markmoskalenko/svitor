<?php
require_once('version.php');

$common_params = array(

    // Версии скриптов, стилей...
    'version'                 => $version,
    // Сколько данных выводить на страницу по умолчанию
    'items_per_page'          => 30,
    // Email для писем от сайта (From)
    'email_from'              => 'Свадьба и Торжество <info@svitor.ru>',
    'email_noreply'           => 'Свадьба и Торжество <noreply@svitor.ru>',
    // директория с шаблонами писем (без слеша в конце)
    'email_templates_path'    => '/var/www/svitor.ru/protected/views/mailer',
    'sender_email'            => 'noreply@svitor.ru',
    'sender_name'             => 'Василиса Свитрова',
    'uni_sender_key'          => '5iwdqdwnnbm5u3ns8szmsmpnuoir3srf3w9x3joo',
    'uni_sender_message_id'   => '9781069',
    'uni_sender_company_list' => '2033512',
    // Страна по умолчанию
    'default_country'         => 3159,
    // 3159 - Russia

    // Валюта сайта
    'currency'                => array(
        'name'  => 'rur',
        'char'  => 'р',
        'short' => 'руб',
        'str1'  => 'рубль', // 1 - рубль
        'str2'  => 'рубля', // 2 - рубля
        'str3'  => 'рублей' // 5 - рублей
    ),
    // Максимальное количество картинок, которое
    // может загрузить пользователь в фотоальбом
    'user_max_images'         => 50,
    // Ограничения по данным для компаний
    'product_max_image'       => 5,
    'product_max_video'       => 2,
    'vendor_max_image'        => 25,
    'vendor_max_video'        => 5,
    'vendor_max_category'     => 3,
    'vendor_max_address'      => 250,
    'vendor_max_certificates' => 5
);?>