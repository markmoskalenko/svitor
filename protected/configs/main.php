<?php
$db = require_once('db.php');
require_once('common.php');
require_once('auth_services.php');

return array (
    'name' => 'Свадьба и Торжество',
    'defaultController' => 'main',
    'preload'=>array('log'),

    'import'=>array(
        'application.models.*',
        'application.models.file.*',
        'application.models.user.*',
        'application.models.vendor.*',
        'application.models.category.*',
        'application.models.product.*',
        'application.models.area.*',
        'application.models.log.*',
        'application.components.*',
        'ext.shopping.*',
        'application.filters.*',
        'application.components.eauth.*',
        'application.components.eauth.services.*',

        'ext.eoauth.*',
        'ext.eoauth.lib.*',
        'ext.lightopenid.*',
        'ext.eauth.*',
        'ext.eauth.services.*'
    ),

    'params' => $common_params,

    'modules' => array(
        'gii' => array(
            'class' => 'system.gii.GiiModule',
            'password' => 'Funnyday5',
        ),

        'admin_cp' => array(
            'import' => array(
                'application.modules.admin_cp.components.*',
            )
        ),

        'mod_cp' => array(
            'import' => array(
                'application.modules.mod_cp.components.*',
            )
        ),

        'vendor_cp' => array(
            'import' => array(
                'application.modules.vendor_cp.components.*',
            )
        )
    ),

    'sourceLanguage' => 'ru',
    'language' => 'ru',

    'charset' => 'utf-8',

    'components' => array(

        'shoppingCart' => array(
            'class' => 'ext.shopping.EShoppingCart',
        ),

        'db' => $db['db.param'],

        'log' => array(
            'class' => 'CLogRouter',
            'routes' => array(
                array(
                    'class'=>'CFileLogRoute',
                    'levels'=>'error, warning', //trace, info, 
                    'categories'=>'system.*',
                )
            ),
        ),

        'urlManager'=>array(
            'urlFormat' => 'path',
            'showScriptName' => false,
            'rules' => array(
                'vendor/<id:\d+>' => 'vendor/profile',
                'user/<id:\d+>' => 'user/profile',

                'messages/<folder:inbox|outbox|bid>/<bid_id:\d+>' => 'messages/index',
                'messages/<folder:inbox|outbox|bid>' => 'messages/index',

                'reports/<cat>/*' => 'report/index',
                'reports/*' => 'report/index',

                'friends/uid/<uid:\d+>/*' => 'friend/index',
                'friends/*' => 'friend/index',

                'post/<id:\d+>' => 'post/view',
                //'posts/<cat>/*' => 'post/index',
                'posts/*' => 'post/index',
                'posts/page/<page:\d+>' => 'posts/index/page/<page>',

                'products/<cat>/*' => 'catalog/products',
                'products/*' => 'catalog/products',

                'vendor_cp/bid/<type:opened|closed>' => 'vendor_cp/bid/index',

                'vendors/addCategory/*' => 'vendors/addCategory',
                'vendors/editCategory/*' => 'vendors/editCategory',
                'vendors/getCategories/*' => 'vendors/getCategories',
                'vendors/deleteCategory/*' => 'vendors/deleteCategory',
                'vendors/team/*' => 'vendors/team',
                'vendors/favorites/*' => 'vendors/favorites',
                'vendors/<cat>/*' => 'catalog/vendors',

                's<id:\d+>/<act>/*' => 'site/view',
                's<id:\d+>' => 'site/view',

                'page/<name>' => 'pages/index'
            )
        ),

        'ImgHandler' => array(
            'class' => 'application.components.CImageHandler'
        ),

        'ImgManager' => array(
            'class' => 'application.components.ImageManager'
        ),

        'PearMail' => array(
            'class' => 'application.components.PearMail'
        ),

        'cache' => array(
            'class' => 'system.caching.CFileCache',
        ),

        /*'session' => array(
            'class' => 'system.web.CDbHttpSession',
            'connectionID' => 'db',
            'autoCreateSessionTable' => false,
        ),*/

        'errorHandler'=>array(
            'errorAction'=>'main/error',
        ),

        'userAuthService' => array(
            'class' => 'application.components.UserAuthService',
        ),

        'loid' => array(
            'class' => 'ext.lightopenid.loid',
        ),

        'eauth' => array(
            'class' => 'ext.eauth.EAuth',
            'popup' => true,        // Использовать всплывающее окно вместо перенаправления на сайт провайдера
            'cache' => false,       // Названия компонента для кеширования. False для отключения кеша. По умолчанию 'cache'.
            'cacheExpire' => 0,     // Время жизни кеша. По умолчанию 0 - означает перманентное кеширование.
            'services' => $auth_services
        ),

        'user' => array(
            'allowAutoLogin' => true,
            'stateKeyPrefix' => 'main',
            'loginUrl' => array('/user/login/'),
        ),
    ),
);
