<?php
class FunBar extends CWidget
{
    public $short_mode = false;

	public function run()
	{
        if (User::isGuest())
            return false;

		$holiday = mApi::getHoliday();
        $wedding = mApi::getWedding();

        $this->render('funbar', array(
            'holiday' => $holiday,
            'wedding' => $wedding
        ));
	}
}