<?php
class VendorPanel extends CWidget
{
    public $vendor_id;
    public $show_phone = false;
    public $profileMode = false;
    public $vendor_info;

    private $_user_vendor_favorite = array();
    private $_user_vendor_team = array();

    public function init()
    {
        if (empty($this->vendor_id) && empty($this->vendor_info))
            throw new ErrorException('Параметр vendor_id не был получен виджетом.');

        if (empty($this->vendor_info))
            $this->vendor_info = Vendor::model()->with('ReviewsCount')->cache(1000)->findByAttributes(array('id' => $this->vendor_id));

        if (!empty($this->vendor_info))
        {
            // Получаем список сохраненных компаний у пользователя
            $user_vendor_favorites = UserVendorFavorites::model()->findAllByAttributes(array('uid' => User::getId()));

            if (!empty($user_vendor_favorites))
                $this->_user_vendor_favorite = CHtml::listData($user_vendor_favorites, 'vendor_id', 'id');

            // Если активно какое-то торжество
            if (mApi::getFunId())
            {
                // Получаем список компаний в команде у пользователя, для активного торжества или свадьбы
                $user_vendor_team = UserVendorTeam::model()->findAllByAttributes(array(mApi::getFunType(true).'_id' => mApi::getFunId(), 'uid' => User::getId()));

                if (!empty($user_vendor_team))
                    $this->_user_vendor_team = CHtml::listData($user_vendor_team, 'vendor_id', 'id');
            }
        }
    }

    public function run()
    {
        $this->render($this->profileMode ? 'profile_vendor_panel' : 'vendor_panel', array(
            'vendor' => $this->vendor_info,
            'user_vendor_team' => $this->_user_vendor_team,
            'user_vendor_favorite' => $this->_user_vendor_favorite
        ));
    }
}