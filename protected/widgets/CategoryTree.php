<?php
class CategoryTree extends CWidget
{
    /************
     *  Список разделов каталога, статей,
     *  зависящий от кол-во данных в каждом разделе (Товары, Компании)
     *
     *  @string $type - тип данных, для которых строиться список разделов (products, vendors)
     *  @model $active_category - AR активной категории
     *
     ************/

    public $type = 'products';  // используется только в представлении
    public $active_category = array();

    private $_categories_data;
    private $_descendants_data;

    public function init()
    {
        $criteria = new CDbCriteria;

		if ($this->type == 'products') {
        $criteria->condition = 't.visible_prod = 1';		
		}
        else if ($this->type == 'vendors'){
		$criteria->condition = 't.visible_vndr = 1';		
		}
		else {$criteria->condition = 't.visible = 1';}	
		
        // Все root-категории
        $this->_categories_data = Category::model()->roots()->findAll($criteria);

        // Подразделы (если есть активная категория)
        if (!empty($this->active_category)) {

            if (!$this->active_category->isRoot())
                $root = $this->active_category->parent()->find();

            if (!empty($root)) {
                $this->_descendants_data = $root->children()->findAll($criteria);
            }
            else
            {
                $this->_descendants_data = $this->active_category->children()->findAll($criteria);
            }
        }
    }

    public function run()
    {
        $this->render('category_tree', array(
            'categories_data' => $this->_categories_data,
            'active_category' => $this->active_category,
            'descendants_data' => $this->_descendants_data
        ));
    }
}