<div class="auth_services">
    <?php foreach ($services as $name => $service): ?>
        <a href="<?php echo Yii::app()->createAbsoluteUrl($action, array('name' => $service->id)); ?>" class="<?php echo $service->id; ?> service" title="<?php echo $service->title; ?>" onclick="authServicePopup.show(this, {width:<?php echo $service->jsArguments['popup']['width']; ?>,height:<?php echo $service->jsArguments['popup']['height']; ?>}); return false;"></a>
    <?php endforeach; ?>
</div>