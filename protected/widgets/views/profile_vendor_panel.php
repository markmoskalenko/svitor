<div class="new_message">
    <?php

    foreach($vendor->Categories as $categories){
        if((int)$categories['cat_id'] == 68){
            $validCategory = 1;
        };
    }
    if ($vendor['notify_new_msg'] == 1 && !isset($validCategory)): ?>
        <div class="button_main">
            <button onclick="vendor.newMsg(this, <?php echo $vendor['id']; ?>); return false;">Написать сообщение</button>
        </div>
        <?php endif; ?>
    <div class="favorites">
    <?php if (isset($user_vendor_favorite[$vendor['id']])) : ?>
        <span class="vendor-saved vendor_action_links">В избранном</span>
    <?php else: ?>
        <a class="vendor-save vendor_action_links" onclick="vendor.addToFavorite(this, <?php echo $vendor['id']; ?>); return false;">Добавить в избранные</a>
    <?php endif; ?>
    </div>
</div>

<div class="add_toteam">
    <div class="add_vendor">
        <?php if (isset($user_vendor_team[$vendor['id']])): ?>
            <div class="status">
                Эта компания в вашей команде (<a onclick="vendor.removeFromTeam(this, <?php echo $vendor->id; ?>); return false;">удалить из команды</a>)
            </div>
        <?php else: ?>
            <div class="button_main">
                <button type="button" onclick="vendor.getCategories(this, <?php echo $vendor['id']; ?>);">Добавить в команду</button>
            </div>
        <?php endif; ?>
    </div>
    <?php if (!User::isGuest()): ?>
    <div class="fun_info_wrap">
        <div class="fun_info">
        <?php if (mApi::getFunType() == 0): ?>
            Свадьба &laquo;<?php $this->widget('application.widgets.FunBar'); ?>&raquo;
            <a onclick="selectFun.holiday(); return false;">&larr; изменить на торжество</a>
        <?php else: ?>
            Торжество &laquo;<?php $this->widget('application.widgets.FunBar'); ?>&raquo;
            <a onclick="selectFun.wedding(); return false;">&larr; изменить на свадьбу</a>
        <?php endif; ?>
        </div>
    </div>
    <?php endif; ?>
</div>