<ul class="nav">
    <?php if (User::isGuest()): ?>

        <li><a href="/user/login/">Вход</a></li>
        <li><a href="/user/registration/">Регистрация</a></li>
        <li class="r_first r_no_border"><a href="/vendor_cp">Вы компания?</a></li>

    <?php else: ?>

        <li class="username">
            <?php
            if (User::getSecurityLevel() > 0) {
                $cp = User::getSecurityLevel() > 1 ? 'mod_cp' : 'admin_cp';
                echo '<a href="/'.$cp.'/">'.Yii::app()->user->name.'</a>';
            } else {
                echo '<a href="/user/profile/">'.Yii::app()->user->name.'</a>';
            }
            ?>
        </li>

<!--        <li --><?php //echo Yii::app()->controller->action->getId() == 'myOrders' ? 'class="active"' : ''; ?><!-->
<!--            --><?php //echo CHtml::link('Мои заказы', Yii::app()->createUrl('/cart/myOrders'));?>
<!--        </li>-->
        <li class="cart_link<?php echo Yii::app()->controller->action->getId() == 'orders' ? ' active' : ''; ?>" <?php echo Yii::app()->shoppingCart->getCount() ? '' : 'style="display:none;"'; ?>>

            <?php echo CHtml::link('Корзина (<span style="font-weight:bold;">'.Yii::app()->shoppingCart->getCount().'</span>)', Yii::app()->createUrl('/cart/orders'));?>
        </li>

        <li><a title="Заявки" class="settings_link" href="/bid/myBid/">Заявки</a></li>

        <?php $msg_count = UserMessages::getCountInbox(); ?>
        <?php $msg_count = UserMessages::getCountInbox(); ?>

        <li><a title="Сообщения" class="messages_link" href="/messages/"><?php echo !empty($msg_count) ? ' <b>+' . $msg_count . '</b> ' : 0; ?></a></li>

        <?php $requests_count = UserFriends::getCountRequests(); ?>
        <li><a title="Друзья" class="friends_link" href="/friends/<?php echo !empty($requests_count) ? 'act/requests' : ''; ?>">Друзья<?php echo !empty($requests_count) ? ' <b>+' . $requests_count . '</b>' : ''; ?></a></li>

        <li><a title="Настройки" class="settings_link" href="/user/settings/">Настройки</a></li>
        <li class="r_first r_no_border magic_button"><a title="Выход" class="exit_link" href="/user/logout/">Выход</a></li>

    <?php endif; ?>
</ul>