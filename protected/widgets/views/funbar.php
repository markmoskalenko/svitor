<?php
$data = '';
if ($this->short_mode)
{
    if (!empty($holiday)) {
        echo '<span class="fun">&laquo;'.mApi::cutStr($holiday['title'], 50).'&raquo;</span>';
    }
    else if (!empty($wedding)) {
        echo '<span class="fun">&laquo;'.mApi::cutStr($wedding['bride_name'].' и '.$wedding['groom_name'], 50).'&raquo;</span>';
    }
}
else
{
    // Торжество
    if (!empty($holiday))
        $data = '<a onclick="selectFun.holiday(); return false;" onmouseover="tooltip.show(this, \'Нажмите, чтобы выбрать другое торжество\', {position:\'top-right\'});">'.mApi::cutStr($holiday['title'], 20).'</a>';

    // Свадьба
    else if (!empty($wedding))
    {
        $data = '<a onclick="selectFun.wedding(); return false;" onmouseover="tooltip.show(this, \'Нажмите, чтобы выбрать другую свадьбу\', {position:\'top-right\'});">'.mApi::cutStr($wedding['bride_name'].' и '.$wedding['groom_name'], 20).'</a>';
    }

    $fun_title = '';

    // Если не выбрано торжество...
    if (mApi::getFunType() == 1 && empty($holiday))
        $fun_title = '<span class="change_fun"><a onclick="selectFun.holiday(); return false;" onmouseover="tooltip.show(this, \'Нажмите, чтобы выбрать торжество\', {position:\'top-right\'});">торжество не выбрано</a></span>';


    // ...или не выбрана свадьба
    else if (mApi::getFunType() == 0 && empty($wedding))
    {
        $fun_title = '<span class="change_fun"><a onclick="selectFun.wedding(); return false;" onmouseover="tooltip.show(this, \'Нажмите, чтобы выбрать свадьбу\', {position:\'top-right\'});">свадьба не выбрана</a></span>';
    }

    echo '<span class="fun">'.$fun_title.$data.'</span>';
}