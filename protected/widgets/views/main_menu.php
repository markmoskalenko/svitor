<?php
$controller = Yii::app()->controller->getId();
$action = Yii::app()->controller->action->getId();

$planning_controllers = array(
    'holiday',
    'wedding',
    'dashboard',
    'checklist',
    'budget',
    'guests',
    'vendors',
    'wannaGifts',
    'routine',
    'photos',
    'site'
);
?>
<ul class="nav">
    <li<?php if (in_array($controller, $planning_controllers) || ($controller == 'user' && $action == 'login' || $action == 'registration')) echo ' class="active"'; ?>><a href="/dashboard">Планирование</a></li>
    <li<?php if ($controller == 'catalog') echo ' class="active"'; ?>><a href="/catalog">Каталог</a></li>
    <li<?php if ($controller == 'post') echo ' class="active"'; ?>><a href="/posts">Обмен опытом</a></li>
    <li<?php if ($controller == 'gallery') echo ' class="active"'; ?>><a href="/gallery">Фотоидеи</a></li>
    <li<?php if ($controller == 'event') echo ' class="active"'; ?>><a href="/event">Скидки, события</a></li>
    <li<?php if ($controller == 'bid') echo ' class="active"'; ?>><a href="/bid">Заявки</a></li>
</ul>