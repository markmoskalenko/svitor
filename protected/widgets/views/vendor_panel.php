<div class="vendor_panel">
    <div class="sidebar">
        <div class="vendor">
            <div class="name">&laquo;<?php echo mApi::cutStr(CHtml::decode($vendor['title']), 20); ?>&raquo;</div>

            <div class="phone">т. <?php echo $vendor['contact_phone']; ?></div>

            <div class="logo">
                <a href="<?php echo Yii::app()->createUrl('vendor/profile', array('id' => $vendor['id'])); ?>">
                    <img src="<?php echo Yii::app()->ImgManager->getUrlById($vendor['img_id'], 'medium', $vendor['img_filename']); ?>">
                </a>
            </div>

            <div class="info">
                <b>Рейтинг </b> <?php echo round($vendor['rating'], 2); ?> |
                <a href="<?php echo Yii::app()->createUrl('vendor/profile', array('id' => $vendor['id'], '#' => 'reviews')); ?>"><b>Отзывы</b> <?php echo '('.$vendor['ReviewsCount'].')'; ?></a>
            </div>
        </div>
        <?php
        if ($this->show_phone)
            echo '<div class="main_phone">'.$vendor['contact_phone'].'</div>';
        ?>
        <div id="vendor_<?php echo $vendor['id']; ?>">
            <div class="links">
                <ul>
                    <li>
                        <div class="add_toteam">
                            <?php if (isset($user_vendor_team[$vendor['id']])): ?>
                                <span class="in_team"><a href="/vendors/">(в команде)</a></span>
                            <?php else: ?>
                                <div class="button_main">
                                    <button type="button" onclick="vendor.getCategories(this, <?php echo $vendor['id']; ?>);">Добавить в команду</button>
                                </div>
                            <?php endif; ?>
                        </div>
                    </li>
                    <li><a onclick="vendor.newMsg(this, <?php echo $vendor['id']; ?>); return false;">Написать сообщение</a></li>
                    <li>
                    <?php if (isset($user_vendor_favorite[$vendor['id']])) : ?>
                        <span class="vendor-saved vendor_action_links">Сохранено</span>
                    <?php else: ?>
                        <a class="vendor-save vendor_action_links" onclick="vendor.addToFavorite(this, <?php echo $vendor['id']; ?>); return false;">Сохранить</a>
                    <?php endif; ?>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>