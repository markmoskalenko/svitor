<?php
if (!empty($categories_data))
{
    $level = 0;

    foreach($categories_data as $n => $category)
    {
        $special_class = '';
        if ($n == 0)
            $special_class = ' root_first';

        $active_class = '';
        if (!empty($active_category))
        {
            if ($active_category['id'] == $category['id'])
                $active_class = ' active';

            // Если это родитель активной категории, то выделяем его
            if ($active_category['root'] == $category['root'] && $category->lft == 1)
                $active_class = ' active';
        }

        $url_begin = $this->type.'/index';
        if ($this->type == 'products' || $this->type == 'vendors')
            $url_begin = 'catalog/'.$this->type;

        // Главный раздел
        if ($category->lft == 1)
        {
            $url = Yii::app()->createUrl($url_begin, array('cat' => $category['name']));
            echo '<div class="root'.$active_class.$special_class.'"><a href="'.$url.'">'.$category['title'].'</a></div>';
        }

        // Показываем подкатегории (если активен какой-то главный раздел и тип не "полезная информация")
        if (!empty($active_category) && $active_category['root'] == $category['root'] && !empty($descendants_data) && $this->type != 'info')
        {
            echo '<div class="subs_wrap">';
            foreach($descendants_data as $child)
            {
                $child_active_class = '';
                if ($active_category['id'] == $child['id'])
                    $child_active_class = ' active';

                $url = Yii::app()->createUrl($url_begin, array('cat' => $child['name']));
                echo '<div class="sub'.$child_active_class.'"><a href="'.$url.'">'.$child['title'].'</a></div>';
            }
            echo '</div>';
        }

        $level = $category->level;
    }
}
else
{
    echo 'Нет категорий...';
}