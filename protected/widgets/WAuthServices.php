<?php
class WAuthServices extends CWidget
{
    /**
     * @var string EAuth component name.
     */
    public $component = 'eauth';

    /**
     * @var array the services.
     * @see EAuth::getServices()
     */
    public $services = null;

    /**
     * @var array predefined services. If null then use all services. Default is null.
     */
    public $predefinedServices = null;

    /**
     * @var boolean whether to use popup window for authorization dialog. Javascript required.
     */
    public $popup = null;

    /**
     * @var string the action to use for dialog destination. Default: the current route.
     */
    public $action = null;


    public function init()
    {
        // EAuth component
        $component = Yii::app()->getComponent($this->component);

        // Some default properties from component configuration
        if (!isset($this->services))
            $this->services = $component->getServices();

        if (is_array($this->predefinedServices)) {
            $_services = array();
            foreach ($this->predefinedServices as $_serviceName) {
                if (isset($this->services[$_serviceName]))
                    $_services[$_serviceName] = $this->services[$_serviceName];
            }
            $this->services = $_services;
        }

        if (!isset($this->popup))
            $this->popup = $component->popup;

        // Set the current route, if it is not set.
        if (!isset($this->action))
            $this->action = Yii::app()->urlManager->parseUrl(Yii::app()->request);
    }

    public function run()
    {
        $this->render('auth_services', array(
            'id' => $this->getId(),
            'services' => $this->services,
            'action' => $this->action,
        ));
    }
}