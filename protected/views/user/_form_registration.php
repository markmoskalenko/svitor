<h2>Регистрация пользователя</h2>
<br>Уже есть аккаунт? <a href="/user/login">Войти &rarr;</a>
<?php echo CHtml::beginForm(); ?>
<div class="main_simple_form">
    <h2 class="auth_services_title">Быстрая регистрация</h2>
    <?php $this->widget('application.widgets.WAuthServices', array('action' => 'user/auth')); ?>

    <h2 class="auth_services_title">Обычная регистрация</h2>
    <div class="item">
        <label for="User_email">E-mail <span id="email_loader"></span></label>
        <div class="field" id="email">
            <?php echo CHtml::activeTextField($user, 'email', array('autocomplete' => 'off')); ?>
            <?php echo CHtml::error($user, 'email'); ?>
            <?php echo empty($_POST) ? '<div class="errorMessage" style="display: none;"></div>' : ''; ?>
        </div>
    </div>
    <div class="item">
        <label for="User_login">Логин</label>
        <div class="field">
            <?php echo CHtml::activeTextField($user, 'login', array('autocomplete' => 'off')); ?>
            <?php echo CHtml::error($user, 'login'); ?>
        </div>
    </div>
    <div class="item">
        <label for="User_password">Пароль</label>
        <div class="field">
            <?php echo CHtml::activePasswordField($user, 'password', array('autocomplete' => 'off')); ?>
            <?php echo CHtml::error($user, 'password'); ?>
        </div>
    </div>
    <div class="item">
        <label for="User_confirm_password">Повторите пароль</label>
        <div class="field">
            <?php echo CHtml::activePasswordField($user, 'confirm_password', array('autocomplete' => 'off')); ?>
            <?php echo CHtml::error($user, 'confirm_password'); ?>
        </div>
    </div>
<!--    <div class="item">
        <?php $this->widget('CCaptcha', array('buttonLabel' => '&larr; обновить', 'clickableImage' => true)); ?>
    </div>
    <div class="item">
        <label for="User_captcha_code">Код с картинки</label>
        <div class="field">
            <?php echo CHtml::activeTextField($user, 'captcha_code'); ?>
            <?php echo CHtml::error($user, 'captcha_code'); ?>
        </div>
    </div> -->
    <div class="item">
        <div class="field">
            <label>
                <?php echo CHtml::activeCheckbox($user, 'accept_terms'); ?>
                Я соглашаюсь с <a href="<?php echo Yii::app()->createUrl('pages/index', array('name' => 'terms-of-use')); ?>" target="_blank">пользовательским соглашением</a> и <a href="<?php echo Yii::app()->createUrl('pages/index', array('name' => 'privacy-policy')); ?>" target="_blank">политикой конфиденциальности</a>.
                <?php echo CHtml::error($user, 'accept_terms', array('id' => 'accept_terms_error')); ?>
            </label>

        </div>
    </div>
    <div class="item">
        <div class="button_main">
            <button type="submit" id="reg_submit"<?php if (empty($user->accept_terms)) echo ' disabled="disabled"'; ?>>Зарегистрироваться</button>
        </div>
    </div>
</div>
<?php echo CHtml::endForm(); ?>
<script src="/static/js/m_registration.js"></script>