<h1>Восстановление пароля</h1>
<div>Введите свой e-mail, чтобы получить новый пароль</div>

<div class="main_simple_form">
    <?php echo CHtml::beginForm(); ?>
    <div class="item">
        <label for="email">E-mail:</label>
        <div class="field">
            <input type="text" name="email" id="email" value="<?php if (!empty($_POST['email'])) echo $_POST['email']; ?>" autocomplete="off">
        </div>
        <span style="color:red;"><?php if (isset($error)) echo $error; ?></span>
    </div>
    <div class="item">
        <div class="button_main">
            <button type="submit">Восстановить</button>
        </div>
    </div>
    <?php echo CHtml::endForm(); ?>
</div>