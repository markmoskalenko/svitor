<h2><a href="/user/settings/">Настройки</a> &rarr; Персональная информация</h2>
<br>
<?php if (!empty($success)) echo '<div class="msg_success">' . $success . '</div>'; ?>
<?php echo CHtml::form('', 'post', array('enctype' => 'multipart/form-data')); ?>
<table class="default">

    <tr>
        <td class="r_al">Логин:</td>
        <td>
            <b><?php echo Yii::app()->user->name; ?></b>
        </td>
    </tr>

    <tr>
        <td class="left">E-mail:</td>
        <td>
            <?php echo CHtml::activeTextField($model, 'email'); ?>
            <?php echo CHtml::error($model, 'email'); ?>
        </td>
    </tr>

    <tr>
        <td class="left">Пол:</td>
        <td>
            <?php echo CHtml::activeRadioButtonList($model, 'gender', array('Мужчина', 'Женщина'), array('separator' => ' ')); ?>
            <?php echo CHtml::error($model, 'gender'); ?>
        </td>
    </tr>

    <tr>
        <td></td>
        <td>
            <div id="image_thumbs">
                <img src="<?php echo Yii::app()->ImgManager->getUrlById($model->img_id, 'medium', $model->img_filename); ?>">
                <img src="<?php echo Yii::app()->ImgManager->getUrlById($model->img_id, 'small', $model->img_filename); ?>">
            </div>
            <a id="upload_image">выбрать фотографию &rarr;</a>
            <input type="hidden" name="User[img_id]" id="img_id" value="<?php echo $model->img_id; ?>">
            <input type="hidden" name="User[img_filename]" id="img_filename"
                   value="<?php echo $model->img_filename; ?>">
        </td>
    </tr>
    <tr>
        <td></td>
        <td>
            <br>

            <div class="button_main">
                <button type="submit">Сохранить</button>
            </div>
        </td>
    </tr>
</table>
<?php echo CHtml::endForm(); ?>

<script src="/static/js/m_user_pinfo.js"></script>
<script src="/static/js/ajaxupload.js"></script>
<script>
    $(document).ready(function () {
        //document.domain = svitor.mainDomain;
        user_pinfo.uploadImg($('#upload_image'));
    });
</script>