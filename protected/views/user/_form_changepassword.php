<h2><a href="/user/settings/">Настройки</a> &rarr; Изменение пароля</h2>
<br>
<?php echo CHtml::beginForm(); ?>
<?php if (!empty($success)) echo '<div class="msg_success">' . $success . '</div>'; ?>
<table class="default">
    <tr>
        <td class="left">Текущий пароль:</td>
        <td>
            <?php echo CHtml::activePasswordField($user, 'old_password'); ?>
            <?php echo CHtml::error($user, 'old_password'); ?>
        </td>
    </tr>

    <tr>
        <td class="left">Новый пароль:</td>
        <td>
            <?php echo CHtml::activePasswordField($user, 'new_password'); ?>
            <?php echo CHtml::error($user, 'new_password'); ?>
        </td>
    </tr>

    <tr>
        <td class="left">Еще раз новый:</td>
        <td>
            <?php echo CHtml::activePasswordField($user, 'confirm_new_password'); ?>
            <?php echo CHtml::error($user, 'confirm_new_password'); ?>
        </td>
    </tr>
    <tr>
        <td></td>
        <td>
            <br>

            <div class="button_main">
                <button type="submit">Сохранить</button>
            </div>
        </td>
    </tr>
</table>
<?php echo CHtml::endForm(); ?>
