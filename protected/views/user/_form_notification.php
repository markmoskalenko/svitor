<h2><a href="/user/settings/">Настройки</a> &rarr; Уведомления</h2>
<br>
<?php echo CHtml::errorSummary($model); ?>
<?php echo CHtml::beginForm(); ?>
<?php if (!empty($success)) echo '<div class="msg_success">' . $success . '</div>'; ?>
<table class="default">
    <tr>
        <td class="top"><label><?php echo CHtml::activeCheckbox($model, 'notify_site_news'); ?> Получать новости
            сайта</label></td>
    </tr>
    <tr>
        <td class="top"><label><?php echo CHtml::activeCheckbox($model, 'notify_new_msg'); ?> Получать уведомления о
            новых сообщениях</label></td>
    </tr>
    <tr>
        <td>
            <br>

            <div class="button_main">
                <button type="submit">Сохранить</button>
            </div>
        </td>
    </tr>
</table>
<?php echo CHtml::endForm(); ?>