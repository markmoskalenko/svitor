<div class="title">
    <?php if ($total_friends_count > 12): ?>
    <?php $all_friends_url = Yii::app()->createUrl('friend/index', array('uid' => $profile_id)); ?>
        <a href="<?php echo $all_friends_url; ?>">Друзья <span class="count">(<?php echo $total_friends_count; ?>)</span></a>
        <span class="see_all"><a href="<?php echo $all_friends_url; ?>">все</a></span>
    <?php else: ?>
        Друзья <span class="count">(<?php echo $total_friends_count; ?>)</span>
    <?php endif; ?>
</div>
<div class="body">

    <?php foreach ($friends as $friend): ?>
    <?php $profile_url = Yii::app()->createUrl('user/profile', array('id' => $friend['User']['id'])); ?>

        <div class="friend">
            <a href="<?php echo $profile_url; ?>"><img src="<?php echo Yii::app()->ImgManager->getUrlById($friend['User']['img_id'], 'small', $friend['User']['img_filename']); ?>">
            <?php echo $friend['User']['login']; ?></a>
        </div>

    <?php endforeach; ?>
</div>