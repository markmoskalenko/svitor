<h2>Войти на сайт</h2>
    <br>Еще нет аккаунта? <a href="/user/registration">Регистрация &rarr;</a>

<div class="main_simple_form">
    <h2 class="auth_services_title">Быстрый вход</h2>
    <?php if (Yii::app()->user->hasFlash('auth_error')): ?>
        <div class="errorSummary alert-error" style="margin-bottom: 15px;">
            <?= Yii::app()->user->hasFlash('auth_error_msg') ?
                Yii::app()->user->getFlash('auth_error_msg') :
                'Что-то пошло не так. Попробуйте еще раз.'; ?>
        </div>
    <?php endif; ?>

    <?php $this->widget('application.widgets.WAuthServices', array('action' => 'user/auth')); ?>

    <h2 class="auth_services_title">Обычный вход</h2>
    <?php
    if (!empty($error))
        echo '<div class="errorSummary">Ошибка: ' . $error . '</div><br>';
    ?>
    <?php echo CHtml::beginForm(); ?>
    <div class="item">
        <label for="email">Логин или E-mail:</label>
        <div class="field">
            <input type="text" name="email" id="email" value="<?php if (!empty($_POST['email'])) echo $_POST['email']; ?>" autocomplete="off">
        </div>
    </div>
    <div class="item">
        <label for="password">Пароль:</label>
        <div class="field">
            <input type="password" id="password" name="password" value="" autocomplete="off">
        </div>
    </div>
    <div><a href="/user/forgotpassword">Забыли пароль?</a></div>
    <br>
    <div class="item">
        <div class="field">
        <label><input type="checkbox" name="rememberMe" value="1" checked="checked"> Запомнить меня</label>
        </div>
    </div>
    <div class="item">
        <div class="button_main">
            <button type="submit">Войти</button>
        </div>
		<div class="interbutton">или</div>
		<div class="button_main admin">
            <a class="button" href="http://demo.svitor.ru">Попробовать ДЕМОверсию</a>
        </div>
    </div>
    <?php echo CHtml::endForm(); ?>
</div>
