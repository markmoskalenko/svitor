<div class="user_profile">
    <div class="user_wrap">
        <div class="avatar">
            <?php if (!empty($profile_info['img_id'])): ?>
                <a class="upbox-button" href="<?php echo Yii::app()->ImgManager->getUrlById($profile_info['img_id'], 'large', $profile_info['img_filename']); ?>"><img src="<?php echo Yii::app()->ImgManager->getUrlById($profile_info['img_id'], 'medium', $profile_info['img_filename']); ?>"></a>
            <?php else: ?>
                <img src="<?php echo Yii::app()->ImgManager->getUrlById($profile_info['img_id'], 'medium', $profile_info['img_filename']); ?>">
            <?php endif; ?>
        </div>

        <div class="name">
            <h1><?php echo $profile_info['login']; ?></h1>
            <div class="last_activity">
                (посл. активность: <?php echo mApi::humanDatePrecise($profile_info->last_activity); ?>)
            </div>
        </div>

        <div class="buttons">
            <?php if ($profile_info->id != User::getId()): ?>
            <div class="new_message">
                <div class="button_main">
                    <button onclick="upBox.ajaxLoad('/messages/new/uid/<?php echo $profile_info['id']; ?>');">Написать сообщение</button>
                </div>
            </div>
                <?php if (empty($friend_invite) && !isset($friend_status)): ?>
                    <div class="add_friend">
                        <div class="button_main">
                            <button onclick="location.assign('/friend/add/uid/<?php echo $profile_info['id']; ?>');">Добавить в друзья</button>
                        </div>
                    </div>
                <?php else: ?>
                    <div class="friend_status">
                        <?php if (!empty($friend_invite) && $profile_info->id != User::getId() && $friend_status == null): ?>
                            <?php echo $profile_info['login']; ?> добавил вас в друзья. <a href="/friend/add/uid/<?php echo $profile_info['id']; ?>">Подтвердить заявку</a>.

                        <?php elseif ($profile_info->id != User::getId() && $friend_status == 'request'): ?>
                            Вы отправили заявку (<a href="/friend/remove/uid/<?php echo $profile_info->id; ?>">отменить</a>)

                        <?php elseif ($friend_status == 'friend'): ?>
                            <?php echo $profile_info['login']; ?> у вас в друзьях (<a href="/friend/remove/uid/<?php echo $profile_info->id; ?>" onclick="if (!confirm('Вы уверены?')) return false;">удалить из друзей</a>)

                        <?php endif; ?>
                    </div>
                <?php endif; ?>
            <?php else: ?>
                <ul class="nav">
                    <li><a href="<?php echo Yii::app()->createUrl('user/personalInfo'); ?>">редактировать информацию &rarr;</a></li>
                </ul>
            <?php endif; ?>
        </div>
    </div>

    <div class="info_main_wrap">
        <div class="info">
            <div class="item">
                <div class="title">Личная информация</div>
            </div>
            <div class="item">
                <div class="label r_al">Пол:</div>
                <div class="field">
                    <?php echo $profile_info->gender == 0 ? 'мужской' : 'женский'; ?>
                </div>
            </div>

            <div class="item indent-top">
                <div class="title">Проведенные праздники</div>
            </div>
            <div class="item">
                <div class="label r_al">Свадьбы:</div>
                <div class="field">
                    <?php if (!empty($counters['planning_wedding']) || !empty($counters['held_wedding'])): ?>
                        <?php echo $counters['planning_wedding'] . ' ' . mApi::plularStr($counters['planning_wedding'], 'планируемая', 'планируемых', 'планируемых'); ?>
                        и
                        <?php echo $counters['held_wedding'] . ' ' . mApi::plularStr($counters['held_wedding'], 'прошедшая', 'прошедших', 'прошедших'); ?>
                    <?php else: ?>
                        нет
                    <?php endif; ?>
                </div>
            </div>
            <div class="item">
                <div class="label r_al">Торжества:</div>
                <div class="field">
                    <?php if (!empty($counters['planning_holiday']) || !empty($counters['held_holiday'])): ?>
                        <?php echo $counters['planning_holiday'] . ' ' . mApi::plularStr($counters['planning_holiday'], 'планируемая', 'планируемых', 'планируемых'); ?>
                        и
                        <?php echo $counters['held_holiday'] . ' ' . mApi::plularStr($counters['held_holiday'], 'прошедшее', 'прошедших', 'прошедших'); ?>
                    <?php else: ?>
                        нет
                    <?php endif; ?>
                </div>
            </div>
<!--            <div class="item myOrder">--><?php //echo CHtml::link('Мои заказы', Yii::app()->createUrl('/cart/myOrders'));?><!--</div>-->
        </div>
    </div>

    <?php if (!empty($friends)): ?>
    <div class="box_wrap">
        <?php
        $this->renderPartial('_profile_friends', array(
            'friends' => $friends,
            'total_friends_count' => $total_friends_count,
            'profile_id' => $profile_info->id
        ));
        ?>
    </div>
    <?php endif; ?>
</div>