<div class="b-upbox">
    <div class="b-upbox_title">
        <div class="title">Региональные настройки</div>
    </div>
    <form onsubmit="return false">
        <div class="content">
            <table style="margin-top: 15px;">
                <tr>
                    <td class="td_name" style="width: 120px"><span class="red_text">*</span> Страна:</td>
                    <td id="user_country">
                        <?php
                        echo CHtml::dropDownList('User[country_id]', '', CHtml::listData($data, 'country_id', 'name'), array(
                            'empty' => '--- не выбрано ---',
                            'onchange' => 'user_area.selectCountry(this);'
                        ));
                        ?> <span id="user_region_loader"></span>
                    </td>
                </tr>
                <tr id="user_region" style="display: none;">
                    <td class="td_name">Регион:</td>
                    <td>
                        <?php
                        echo CHtml::dropDownList('User[region_id]', '', array(), array(
                            'onchange' => 'user_area.selectRegion(this);'
                        ));
                        ?> <span id="user_city_loader"></span>
                    </td>
                </tr>
                <tr id="user_city" style="display: none;">
                    <td class="td_name">Город:</td>
                    <td>
                        <?php
                        echo CHtml::dropDownList('User[city_id]', '', array(), array(
                            'onchange' => 'user_area.selectCity(this);'
                        ));
                        ?>
                    </td>
                </tr>
            </table>
        </div>
        <div class="buttons">
            <div class="button_main">
                <button onclick="user_area.saveInfo(this);">Сохранить</button>
            </div>
            <div class="button_grey">
                <button onclick="$.fancybox.close();">Отменить</button>
            </div>
        </div>
    </form>
</div>

<script>
    $(document).ready(function () {
        user_area.init({
            id: '#user',
            country: <?php echo Yii::app()->user->getState('country_id', Yii::app()->params['default_country']); ?>,
            region: <?php echo User::getRegionId(); ?>,
            city: <?php echo User::getCityId(); ?>,
            metro: 0
        });
    });
</script>