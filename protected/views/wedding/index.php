<?php
// Если нету свадеб, и получен параметр 'create', то откроем форму добавление новой свадьбы
if (empty($data) && isset($_GET['create']))
    echo '<script>$(document).ready(function(){ wedding.addNew(); });</script>';
?>

<h2>Ближайшие свадьбы</h2>
<div class="simple_add_button">
    <a onclick="wedding.addNew(); return false;">Спланировать новую свадьбу</a>
</div>
<table class="simple_table">
    <thead>
    <th align="left" width="350px">Название</th>
    <th align="left" width="110px">Дата</th>
    <th align="left">Осталось</th>
    <th width="100px"></th>
    </thead>
    <tbody>
    <?php
    if (!empty($data)):
        $coming_count = 0;
        foreach ($data as $wedding):
            if ($wedding['date'] < time() && date('d.m.Y', time()) != date('d.m.Y', $wedding['date']))
                continue;

            $coming_count++;
    ?>
        <tr class="active">
            <td align="left" onclick="wedding.setSelected(<?php echo $wedding['id']; ?>);">
                <?php $title = $wedding['bride_name'] . ' и ' . $wedding['groom_name']; ?>
                <?php echo mApi::cutStr($title, 100); ?>
            </td>
            <td onclick="wedding.setSelected(<?php echo $wedding['id']; ?>);">
                <?php
                if (date('d.m.Y', time()) == date('d.m.Y', $wedding['date'])) {
                    echo '<b>сегодня</b>';
                } else {
                    echo mApi::getDateWithMonth($wedding['date']);
                }
                ?>
            </td>
            <td onclick="wedding.setSelected(<?php echo $wedding['id']; ?>);">
                <?php
                if (time() < $wedding['date'])
                    echo mApi::getDaysLeft($wedding['date']);
                ?>
            </td>
            <td>
                <a class="edit magic_button" onclick="wedding.goEdit(<?php echo $wedding->id; ?>); return false;" onmouseover="tooltip.show(this, 'Редактировать', {position :'top-right'});"></a>
                <a class="trash magic_button" onclick="alert('Удаление пока не доступно...'); return false;" onmouseover="tooltip.show(this, 'Удалить', {position :'top-right'});"></a>
            </td>
        </tr>
    <?php
        endforeach;
    endif;
    if (empty($coming_count))
        echo '<tr><td class="empty_item" colspan="4">Нет ближайших свадеб...</td></tr>';
    ?>

    </tbody>
</table>

<br><br>

<h2>Прошедшие</h2>
<br>

<table class="simple_table">
    <thead>
    <th align="left" width="350px">Название</th>
    <th align="left" width="110px">Дата</th>
    <th></th>
    </thead>
    <tbody>
    <?php
    $count = 0;
    foreach ($data as $wedding) {
        if ($wedding->date > time() || date('d.m.Y', time()) == date('d.m.Y', $wedding['date']))
            continue;

        $count++;
        ?>

    <tr class="active">
        <td align="left" onclick="wedding.setSelected(<?php echo $wedding['id']; ?>);">
            <?php $title = $wedding['bride_name'] . ' и ' . $wedding['groom_name']; ?>
            <?php echo mApi::cutStr($title, 100); ?>
        </td>
        <td onclick="wedding.setSelected(<?php echo $wedding['id']; ?>);"><?php echo mApi::getDateWithMonth($wedding['date']); ?></td>
        <td>
            <a class="trash magic_button" onclick="alert('Удаление пока не доступно...'); return false;" onmouseover="tooltip.show(this, 'Удалить', {position :'top-right'});"></a>
        </td>
    </tr>
        <?php
    }
    if ($count == 0)
        echo '<tr><td class="empty_item" colspan="3">Нет прошедших свадеб...</td></tr>';
    ?>
    </tbody>
</table>
<script src="/static/js/m_wedding.js"></script>