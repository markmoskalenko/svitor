<div class="content" id="weddingForm">
    <div id="msg"></div>
    <form onsubmit="return false;">
        <div class="label_name">Дата свадьбы <span class="red_text">*</span></div>
        <div class="field">
            <?php
            if (!empty($wedding->date))
                $wedding->date = date('d.m.Y', $wedding->date);

            echo CHtml::activeTextField($wedding, 'date', array('style' => 'width: 90px;'));
            ?>
        </div>
        <div class="label_name">Имя невесты <span class="red_text">*</span></div>
        <div class="field">
            <?php
            $wedding->bride_name = CHtml::decode($wedding->bride_name);
            echo CHtml::activeTextField($wedding, 'bride_name');
            ?>
        </div>
        <div class="label_name">Имя жениха <span class="red_text">*</span></div>
        <div class="field">
            <?php
            $wedding->groom_name = CHtml::decode($wedding->groom_name);
            echo CHtml::activeTextField($wedding, 'groom_name');
            ?>
        </div>
    </form>
</div>

<div class="buttons"><span id="loader"></span>

    <div class="button_main">
        <button type="button"
                onclick="wedding.sendForm(this, '<?php echo $wedding->isNewRecord ? 'add' : 'edit/id/' . $wedding->id; ?>');"><?php echo $wedding->isNewRecord ? 'Добавить' : 'Сохранить'; ?></button>
    </div>
    <div class="button_grey">
        <button onclick="$.fancybox.close();">Отменить</button>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('#UserWeddings_date').datepicker({
            changeYear:true,
            dateFormat:'dd.mm.yy',
            minDate:new Date(<?php echo date('Y') . ',' . date('m') . ' - 1,' . date('d'); ?>),
            maxDate:new Date(2020, 1 - 1, 1),
            monthNames:['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
            monthNamesShort:['янв', 'фев', 'мар', 'апр', 'май', 'июн', 'июл', 'авг', 'сен', 'окт', 'ноя', 'дек'],
            dayNamesMin:["Вс", "Пн", "Вт", "Ср", "Чт", "Пт", "Сб"],
            showAnim:'drop'
        });
    });
</script>