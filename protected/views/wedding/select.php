<div class="content">
    <div class="b-selectfun_select">
        <?php
        $coming_count = 0; // Если нету ближайших свадеб, то предложим создать новую

        if (!empty($data)): ?>
            <?php foreach ($data as $var):
                if ($var['date'] < time() && date('d.m.Y', time()) != date('d.m.Y', $var['date']))
                    continue;

                $coming_count++;
                $coming_count_html = '';
                $wedding = mApi::getWedding();

                if (!empty($wedding) && mApi::getFunId() == $var['id'])
                    $coming_count_html = ' <span class="active">&larr; выбрано</span>';
                ?>

                <div class="element" onclick="location.assign(location.pathname+'?wedding=<?php echo $var['id']; ?>'+location.hash);">
                    <table>
                        <td>
                            <?php $title = $var['bride_name'] . ' и ' . $var['groom_name']; ?>
                            <?php echo mApi::cutStr($title, 100) . $coming_count_html; ?>
                        </td>
                        <td class="date">
                            <?php
                            if (date('d.m.Y', time()) == date('d.m.Y', $var['date'])) {
                                echo '<b>сегодня</b>';
                            } else {
                                echo mApi::getDateWithMonth($var['date']);
                            }
                            ?>
                        </td>
                    </table>
                </div>

                <?php endforeach; ?>
            <?php endif; ?>

        <?php if (empty($coming_count)): ?>
        <div align="center">
            <?php echo !empty($data) ? 'У вас нет ближайших свадеб...' : 'У вас нет планируемых свадеб...'; ?>
            <br><br>

            <div class="button_main">
                <button onclick="location.assign('/wedding/?create');">Перейти к созданию &rarr;</button>
            </div>
        </div>
        <?php endif; ?>
    </div>
</div>
<?php if (!empty($data)): ?>
<div class="buttons">
    <a href="/wedding/">Управление свадьбами</a>
</div>
<?php endif; ?>