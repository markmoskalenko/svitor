<link rel="stylesheet" type="text/css" href="/static/timePicker.css">
<div class="routine_title">
    Распорядок дня
    <?php $this->widget('application.widgets.FunBar', array('short_mode' => true)); ?>
</div>

<?php if (mApi::getFunId()): ?>

<div class="simple_add_button">
    <a onclick="routine.addNew(); return false;">Добавить</a>
</div>

<table class="simple_table routine_table">

    <?php if (!empty($routine_data)): ?>

    <tr class="thead">
        <th class="c_al time">Время</th>
        <th class="c_al note_th"></th>
        <th class="l_al event">Событие</th>
        <th class="l_al address">Адрес</th>
        <th class="l_al vendor">Компания-участник</th>
        <th class="l_al editor"></th>
    </tr>

    <?php foreach ($routine_data as $var): ?>
        <tr class="item" id="item_<?php echo $var['id']; ?>">

            <td class="l_al title"><?php echo $var['time']; ?></td>

            <td class="c_al note_th">
                <?php if (!empty($var->note)): ?>
                <div class="note" onmouseover="tooltip.show(this, '<?php echo $var->note; ?>', {special_class : 'white'});"></div>
                <?php endif; ?>
            </td>

            <td class="l_al event">
                <div class="event_text"><?php echo $var['event']; ?></div>
            </td>

            <td class="l_al address">
                <?php $url = 'http://maps.google.com/?output=embed&q=' . urlencode($var['address']); ?>
                <div class="address_text">
                    <?php echo !empty($var['address']) ? '<a class="upbox-various fancybox.iframe" href="' . $url . '" target="_blank">' . $var['address'] . '</a>' : '--'; ?>
                </div>
            </td>

            <td class="l_al vendor">
                <?php echo !empty($var['vendor_id']) ? '<a onclick="fastmsg.writeNew(this, ' . $var['Vendor']['id'] . '); return false;">' . mApi::cutStr($var['Vendor']['title'], 100) . '</a>' : '--'; ?>
            </td>

            <td class="l_al editor">
                <div class="editor_links">
                    <a class="edit magic_button" onclick="routine.goEdit(<?php echo $var['id']; ?>); return false;"
                       onmouseover="tooltip.show(this, 'Редактировать', {position :'top-right'});"></a>
                    <a class="delete magic_button"
                       onclick="routine.deleteItem(<?php echo $var['id']; ?>); return false;"
                       onmouseover="tooltip.show(this, 'Удалить', {position :'top-right'});"></a>
                </div>
            </td>
        </tr>
        <?php endforeach; ?>
    <tr class="js_empty_item empty_item">
        <td>Вы пока не добавили ни одного события...</td>
    </tr>

    <div id="trash-dialog" style="display: none;" title="Удалить подарок">Подарок будет удален без возможности
        восстановления.<br><br>Вы действительно хотите продолжить?
    </div>

    <?php else: ?>
    <tr class="empty_item">
        <td>Вы пока не добавили ни одного события...</td>
    </tr>
    <?php endif; ?>
</table>
<script src="/static/js/m_routine.js"></script>
<script src="/static/js/m_fastmsg.js"></script>
<?php else:
    $this->widget('application.widgets.SelectFun');
endif;
?>
