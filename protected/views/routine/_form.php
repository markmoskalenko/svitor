<div class="content" id="routineForm">
    <div id="msg"></div>
    <form onsubmit="return false;">

        <div class="label_name">Время <span class="red_text">*</span></div>
        <div class="field">
            <input type="text" name="UserRoutine[time]" id="time" style="width: 50px;" placeholder="чч:мм"
                   value="<?php echo $model->time; ?>">
        </div>

        <div class="label_name">Событие <span class="red_text">*</span></div>
        <div class="field">
            <?php
            $model->event = CHtml::decode($model->event);
            echo CHtml::activeTextArea($model, 'event', array(
                'style' => 'width: 98%; height: 40px;',
                'required' => 'required'
            ));
            ?>
        </div>

        <div class="label_name">Адрес</div>
        <div class="field">
            <?php
            $model->address = CHtml::decode($model->address);
            echo CHtml::activeTextArea($model, 'address', array(
                'style' => 'width: 98%; height: 40px;',
                'required' => 'required'
            ));
            ?>
        </div>

        <div class="label_name">Компания-участник</div>
        <div class="field">
            <?php
            echo CHtml::activeDropDownList($model, 'vendor_id', CHtml::listData($vendors_team, 'Vendor.id', 'Vendor.title'), array(
                'empty' => '-- не выбрано --',
                'style' => 'width: 100%;',
                'required' => 'required'
            ));
            ?>
        </div>

        <div id="box_note"<?php if (empty($model->note)) echo 'style="display:none;"'; ?>>
            <div class="label_name">Примечание</div>
            <div class="field">
                <?php
                $model->note = CHtml::decode($model->note);
                echo CHtml::activeTextArea($model, 'note', array('style' => 'width: 98%; height: 60px;'));
                ?>
            </div>
        </div>
        <div class="label_name">
            <a onclick="routine.noteSwitch(this); return false;"
               class="js_link"><?php echo empty($model->note) ? 'Добавить примечание' : 'Скрыть примечание'; ?></a>
        </div>
    </form>
</div>

<div class="buttons">
    <div class="button_main">
        <button type="button"
                onclick="routine.sendForm(this, '<?php echo $model->isNewRecord ? 'add' : 'edit/id/' . $model->id; ?>');">
            <?php echo $model->isNewRecord ? 'Добавить' : 'Сохранить'; ?>
        </button>
    </div>
    <div class="button_grey">
        <button onclick="$.fancybox.close();">Отменить</button>
    </div>
</div>

<script src="/static/js/timePicker.js"></script>
<script>
    $(document).ready(function () {
        $('#time').timePicker({
            startTime:'00:30'
        });
    });
</script>