<?php
if (!empty($comments)) {
    foreach ($comments as $comment) {
        $user_profile = Yii::app()->createUrl('user/profile', array('id' => $comment['User']['id']));
        $special_class = '';
        if ($comment->deleted == 1)
            $special_class = ' magic_button';

        echo '<table id="comment_' . $comment['id'] . '" class="comment_table' . $special_class . '">';
        echo '<tr>';
        echo '<td valign="top" width="55px">
                  <a href="' . $user_profile . '"><img src="' . Yii::app()->ImgManager->getUrlById($comment['User']['img_id'], 'small', $comment['User']['img_filename']) . '"></a>
                  </td>';
        echo '<td valign="top" style="padding: 0 5px 5px;">';
        echo '<b><a href="' . $user_profile . '">' . $comment['User']['login'] . '</a></b>';

        if ($comment->deleted == 0) {
            echo '<div class="comment_text">' . nl2br($comment['comment']) . '</div>';
            echo '<div class="comment_info">' . mApi::getDateWithMonth($comment['date']) . ' в ' . date('H:i', $comment['date']);

            if ($comment['User']['id'] == User::getId())
                echo ' | <a onclick="info.removeComment(' . $comment['id'] . '); return false;">удалить</a>';

            echo '</div>';
        } else {
            echo '<div class="comment_deleted">Пользователь удалил свой комментарий...</div>';
        }

        echo '</td>';

        echo '</tr>';
        echo '</table>';
    }
} else {
    echo '<div style="margin-top: 15px;">Пока нет ни одного комментария...</div>';
}
?>
<br>
<?php
$this->widget('CLinkPager', array(
    'pages' => $dataProvider->pagination,
    'htmlOptions' => array('class' => 'b-default_pager'),
    'firstPageLabel' => '',
    'lastPageLabel' => '',
    'prevPageLabel' => '&larr; Назад',
    'nextPageLabel' => 'Далее &rarr;',
    'header' => ''
));
?>
<div id="trash-comment-dialog" title="Удалить комментарий" style="display: none;">Вы действительно хотите удалить свой
    комментарий?
</div>
<div style="margin: 20px 0;" id="user_comment">
    <?php if (User::isGuest()): ?>
    Только <a href="<?php echo Yii::app()->createUrl('user/registration'); ?>">зарегистрированные</a> пользователи могут
    оставлять комментарии. <a href="<?php echo Yii::app()->createUrl('user/login'); ?>">Войти</a>.
    <?php else: ?>
    <b>Ваш комментарий:</b>
    <div id="comment_error"></div>
    <textarea style="width: 100%; height: 50px; margin: 10px 0;" id="comment"></textarea>

    <div class="button_main">
        <button onclick="info.sendComment(this, <?php echo $post_id; ?>);">Добавить</button>
    </div>
    <span style="color: #939393;"> (комментарий будет проверен модератором)</span>
    <?php endif; ?>
</div>