<table class="b-catalog-table">
    <td class="menu catalog_categories">
        <?php
        $this->widget('application.widgets.CategoryTree', array('type' => $type, 'active_category' => $category));
        ?>
    </td>

    <td class="content">

        <div class="info_search">
            <?php echo CHtml::beginForm(Yii::app()->createUrl('info'), 'GET'); ?>
                <?php echo CHtml::textField('q', Yii::app()->request->getParam('q'), array('placeholder' => 'Поиск по статьям')); ?>
                <div class="button_grey">
                    <button type="submit">Найти</button>
                </div>
            <?php echo CHtml::endForm(); ?>
        </div>

        <?php
        if (!empty($view_post) && empty($posts)) {
            $this->renderPartial('_view', array(
                'data' => $view_post,
                'recommendations' => $recommendations,
                'comments' => $comments,
                'comments_count' => $comments_count,
                'dataProvider' => $dataProvider
            ));
        } else {
            if (!empty($posts)) {
                $this->renderPartial('_posts_list', array('posts' => $posts, 'dataProvider' => $dataProvider));
            } else {
                echo 'К сожалению нам не удалось ничего найти... :(';
            }
        }
        ?>
    </td>
    <!-- <td class="params">

    </td> -->
</table>
