<h1><?php echo $data->title; ?></h1>
<div class="info_fulltext">
    <?php echo $data->full_text; ?>

    <div class="r_fl">
        <?php if ($data->author_vid != 0): ?>
        <?php $url = Yii::app()->createUrl('vendor/profile', array('id' => $data->author_vid)); ?>
        <b>Автор:</b> &laquo;<a href="<?php echo $url; ?>"><?php echo mApi::cutStr($data['Vendor']['title'], 50); ?></a>&raquo;
        <?php else: ?>
        <b>Источник:</b> <a href="http://<?php echo Yii::app()->request->getServerName(); ?>"><?php echo Yii::app()->request->getServerName(); ?></a>
        <?php endif; ?>
    </div>

    <?php if (!empty($recommendations)): ?>
    <div>
        <b>Читайте также:</b>
        <ul style="margin-top: 5px;">
            <?php foreach ($recommendations as $post): ?>
            <?php $url = Yii::app()->createUrl('info/index', array('cat' => $post['Category']['name'], 'view' => $post['id'])); ?>
            <li>
                <a href="<?php echo $url; ?>"><?php echo mApi::cutStr($post['title'], 120); ?></a></li>
            <?php endforeach; ?>
        </ul>
    </div>
    <?php endif; ?>

    <table style="width: 100%; border-collapse: collapse; margin-top: 15px; color: #777;">
        <tr>
            <td class="l_al">
                <script type="text/javascript" src="http://yandex.st/share/share.js" charset="utf-8"></script>
                <div style="margin-left: -6px;" class="yashare-auto-init" data-yashareL10n="ru" data-yashareType="button" data-yashareQuickServices="vkontakte,facebook,twitter,odnoklassniki,moimir,gplus"></div>
            </td>
        </tr>
    </table>

    <?php if (User::getSecurityLevel() > 0): ?>
    <?php Yii::app()->user->setReturnUrl(Yii::app()->request->url); ?>
    <b>Модератор:</b> <a href="/mod_cp/posts/edit/id/<?php echo $data->id; ?>">редактировать статью</a><br>
    <?php endif; ?>
</div>
<br>
<h2 id="comments">Комментарии<?php echo !empty($comments_count) ? ' (' . $comments_count . ')' : ''; ?></h2>

<?php
$this->renderPartial('_comments', array(
    'dataProvider' => $dataProvider,
    'comments' => $comments,
    'post_id' => $data->id
))
?>

<script src="/static/js/m_info.js"></script>