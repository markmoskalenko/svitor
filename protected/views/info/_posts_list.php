<?php
foreach ($posts as $post) {
    if (!empty($category)) {
        $url = Yii::app()->createUrl('info/index', array('cat' => $category->name, 'view' => $post['id']));
    } else {
        $url = Yii::app()->createUrl('info/index', array('cat' => $post['Category']['name'], 'view' => $post['id']));
    }
    echo '<div class="info_post">';
    echo '<a href="' . $url . '"><img src="' . Yii::app()->ImgManager->getUrlById($post['img_id'], 'medium', $post['img_filename']) . '" class="info_post_img"></a>';
    echo '<div class="info_post_title"><a href="' . $url . '">' . $post['title'] . '</a></div>';
    echo '<div class="info_post_text">' . InfoPosts::reducePost($post['full_text'], 500) . ' <br><a href="' . $url . '">Читать дальше &rarr;</a></div>';
    echo '</div>';
}

$this->widget('CLinkPager', array(
    'pages' => $dataProvider->pagination,
    'htmlOptions' => array('class' => 'b-default_pager'),
    'firstPageLabel' => '',
    'lastPageLabel' => '',
    'prevPageLabel' => '&larr; Назад',
    'nextPageLabel' => 'Далее &rarr;',
    'header' => '',
    'cssFile' => false
));
?>