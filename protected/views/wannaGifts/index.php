<div class="gifts_title">
    Подарки
    <?php $this->widget('application.widgets.FunBar', array('short_mode' => true)); ?>
</div>

<?php if (mApi::getFunId()): ?>

<div class="gifts_summary_info magic_button">
    Выбрано <span><?php echo $givers_count; ?></span> из <span><?php echo $all_count; ?></span> подарков
</div>

<div class="simple_add_button">
    <a onclick="wanna_gifts.addNew(); return false;">Добавить</a>
</div>

<table class="simple_table gifts_table">

    <?php if (!empty($wanna_gifts_data)): ?>

    <tr class="thead">
        <th class="c_al note_th"></th>
        <th class="l_al title">Название подарка</th>
        <th class="l_al link">Ссылка</th>
        <th class="l_al guest">Кто подарит</th>
        <th class="l_al editor"></th>
    </tr>

    <?php foreach ($wanna_gifts_data as $var): ?>
        <tr class="item" id="item_<?php echo $var['id']; ?>">
            <td class="c_al note_th">
                <?php if (!empty($var->description)): ?>
                <div class="note"
                     onmouseover="tooltip.show(this, '<?php echo $var->description; ?>', {special_class : 'white'});"></div>
                <?php endif; ?>
            </td>

            <td class="l_al title">
                <div class="title_text"><?php echo $var['title']; ?></div>
            </td>

            <td class="l_al link">
                <?php echo !empty($var['link']) ? '<a href="' . $var['link'] . '" target="_blank">есть</a>' : '--'; ?>
            </td>

            <td class="l_al guest">
                <?php echo !empty($var['guest_id']) ? $var['Guest']['fullname'] : '--'; ?>
            </td>

            <td class="l_al editor">
                <div class="editor_links">
                    <a class="edit magic_button" onclick="wanna_gifts.goEdit(<?php echo $var['id']; ?>); return false;"
                       onmouseover="tooltip.show(this, 'Редактировать', {position :'top-right'});"></a>
                    <a class="delete magic_button"
                       onclick="wanna_gifts.deleteItem(<?php echo $var['id']; ?>); return false;"
                       onmouseover="tooltip.show(this, 'Удалить', {position :'top-right'});"></a>
                </div>
            </td>
        </tr>
        <?php endforeach; ?>
    <tr class="js_empty_item empty_item">
        <td>Вы пока не добавили ни одного подарка...</td>
    </tr>

    <div id="trash-dialog" style="display: none;" title="Удалить подарок">Подарок будет удален без возможности
        восстановления.<br><br>Вы действительно хотите продолжить?
    </div>

    <?php else: ?>
    <tr class="empty_item">
        <td>Вы пока не добавили ни одного подарка...</td>
    </tr>
    <?php endif; ?>
</table>

<div class="gifts_guest_msg">
    <?php echo CHtml::beginForm(); ?>
    <div class="title">Обращение к гостям</div>
    Вы можете указать здесь то, что не хотели бы получить в подарок.<br><br>
    <?php echo !empty($success) ? '<div id="announce_msg"><span class="msg_success">' . $success . '</span><br></div>' : ''; ?>
    <?php
    echo CHtml::activeTextArea($guests_msg, 'content', array(
        'style' => 'width: 99%;'
    ));
    echo CHtml::error($guests_msg, 'content');
    ?>
    <div class="button_main">
        <button type="submit">Сохранить</button>
    </div>
    <?php echo CHtml::endForm(); ?>
</div>

<script src="/static/js/m_wanna_gifts.js"></script>
<?php if (!empty($success)): ?>
    <script>
        $(document).ready(function () {
            wanna_gifts.hideMessage();
        });
    </script>
    <?php endif; ?>
<?php else:
    $this->widget('application.widgets.SelectFun');
endif;
?>
