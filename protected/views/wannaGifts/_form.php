<div class="content" id="giftForm">
    <div id="msg"></div>
    <form onsubmit="return false;">
        <div class="label_name">Название <span class="red_text">*</span></div>
        <div class="field">
            <?php
            $model->title = CHtml::decode($model->title);
            echo CHtml::activeTextField($model, 'title', array(
                'style' => 'width: 98%;',
                'required' => 'required'
            ));
            ?>
        </div>

        <div class="label_name">Ссылка (где можно найти)</div>
        <div class="field">
            <?php
            echo CHtml::activeTextField($model, 'link', array(
                'style' => 'width: 98%;',
                'required' => 'required'
            ));
            ?>
        </div>

        <div class="label_name">Описание</div>
        <div class="field">
            <?php
            $model->description = CHtml::decode($model->description);
            echo CHtml::activeTextArea($model, 'description', array(
                'style' => 'width: 98%; height: 60px;'
            ));
            ?>
        </div>

        <div class="label_name">Кто подарит?</div>
        <div class="field">
            <?php
            echo CHtml::activeDropDownList($model, 'guest_id', CHtml::listData($guests, 'id', 'fullname'), array(
                'empty' => '-- не выбрано --',
                'style' => 'width: 98%;'
            ));
            ?>
        </div>
    </form>
</div>

<div class="buttons">
    <div class="button_main">
        <button type="button" id="send_button"
                onclick="wanna_gifts.sendForm(this, '<?php echo $model->isNewRecord ? 'add' : 'edit/id/' . $model->id; ?>');">
            <?php echo $model->isNewRecord ? 'Добавить' : 'Сохранить'; ?>
        </button>
    </div>
    <div class="button_grey">
        <button onclick="$.fancybox.close();">Отменить</button>
    </div>
</div>
