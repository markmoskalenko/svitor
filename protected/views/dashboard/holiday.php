<div class="dashboard_title">
    Пульт управления
    <?php $this->widget('application.widgets.FunBar', array('short_mode' => true)); ?>
</div>
<?php if (!empty($holiday)): ?>
    <div class="dashboard_wrap">
        <table class="simple_table">
            <thead>
                <th colspan="2" class="l_al">
                    Информация о торжестве
                </th>
            </thead>
            <tr>
                <td width="150px">Название:</td>
                <td>
                    <?php echo '<b>' . mApi::cutStr($holiday->title, 100) . '</b>'; ?>
                    <a onclick="holiday.goEdit(<?php echo $holiday->id; ?>); return false;">ред.</a>
                </td>
            </tr>
            <tr>
                <td class="title">Дата торжества:</td>
                <td>
                    <?php
                    echo mApi::getDateWithMonth($holiday['date']);

                    if (date('d.m.Y', time()) == date('d.m.Y', $holiday['date'])) {
                        echo ' (<b>сегодня</b>)';
                    } else {
                        $days_left = mApi::getDaysLeft($holiday['date']);
                        if (!empty($days_left))
                            echo ' (' . mApi::plularStr($days_left, 'остался', 'осталось', 'осталось') . ' <b>' . $days_left . '</b>)';
                    }
                    ?>
                    <a onclick="holiday.goEdit(<?php echo $holiday->id; ?>); return false;">ред.</a>
                </td>
            </tr>
            <tr>
                <td class="title">Список задач:</td>
                <td>
                    <?php echo '<b>' . $checklist_count . '</b> ' . mApi::plularStr($checklist_count, 'задача', 'задачи', 'задач'); ?>
                    <a href="/checklist/">ред.</a>
                </td>
            </tr>
            <tr>
                <td class="title">Список гостей:</td>
                <td>
                    <?php echo '<b>' . $guest_count . '</b> ' . mApi::plularStr($guest_count, 'гость', 'гостя', 'гостей'); ?>
                    <a href="/guests/">ред.</a>
                </td>
            </tr>
            <tr>
                <td class="title">Сайт торжества:</td>
                <td>
                    <b><?php echo (isset($usite->is_available) && $usite->is_available == 1) ? 'включен' : 'отключен'; ?></b> <a href="/site/settings">ред.</a>
                </td>
            </tr>
        </table>
        <script src="/static/js/m_holiday.js"></script>
    </div>
<?php else:
        $this->widget('application.widgets.SelectFun');
endif; ?>