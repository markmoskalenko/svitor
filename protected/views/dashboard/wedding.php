<div class="dashboard_title">
    Пульт управления
    <?php $this->widget('application.widgets.FunBar', array('short_mode' => true)); ?>
</div>
<?php if (!empty($wedding)): ?>
    <div class="dashboard_wrap">

        <table class="simple_table">
            <thead>
                <th colspan="2" class="l_al">Информация о свадьбе</th>
            </thead>
            <tr>
                <td class="title">Молодожены:</td>
                <td>
                    <?php $title = $wedding['bride_name'] . ' и ' . $wedding['groom_name']; ?>
                    <b><?php echo mApi::cutStr($title, 100); ?></b>
                    <a onclick="wedding.goEdit(<?php echo $wedding->id; ?>); return false;">ред.</a>
                </td>
            </tr>
            <tr>
                <td class="title">Дата свадьбы:</td>
                <td>
                    <?php
                    echo mApi::getDateWithMonth($wedding['date']);

                    if (date('d.m.Y', time()) == date('d.m.Y', $wedding['date'])) {
                        echo ' (<b>сегодня</b>)';
                    } else {
                        $days_left = mApi::getDaysLeft($wedding['date']);
                        if (!empty($days_left))
                            echo ' (' . mApi::plularStr($days_left, 'остался', 'осталось', 'осталось') . ' <b>' . $days_left . '</b>)';
                    }
                    ?>
                    <a onclick="wedding.goEdit(<?php echo $wedding->id; ?>); return false;">ред.</a>
                </td>
            </tr>

            <tr>
                <td class="title">Список задач:</td>
                <td>
                    <?php echo '<b>' . $checklist_count . '</b> ' . mApi::plularStr($checklist_count, 'задача', 'задачи', 'задач'); ?>
                    <a href="/checklist/">ред.</a>
                </td>
            </tr>
            <tr>
                <td class="title">Список гостей:</td>
                <td>
                    <?php echo '<b>' . $guest_count . '</b> ' . mApi::plularStr($guest_count, 'гость', 'гостя', 'гостей'); ?>
                    <a href="/guests/">ред.</a>
                </td>
            </tr>
            <tr>
                <td class="title">Свадебный сайт:</td>
                <td>
                    <b><?php echo (isset($usite->is_available) && $usite->is_available == 1) ? 'включен' : 'отключен'; ?></b> <a href="/site/settings">ред.</a>
                </td>
            </tr>
        </table>
        <script src="/static/js/m_wedding.js"></script>
    </div>

<?php else:
    $this->widget('application.widgets.SelectFun');
endif; ?>