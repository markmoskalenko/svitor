<table border="0" cellpadding="0" cellspacing="0">
    <td style="font-family: Arial, sans-serif; font-size: 14px; line-height: 150%;">
        <p>Здравствуйте!</p>

        <p>У Вас есть новое личное сообщение от <b>{from_name}</b>:</p>

        <p>
            <span style="color: #6d6d6d;font-style: italic;">Дата:</span> {msg_date}
            <br><span style="color: #6d6d6d;font-style: italic;">Тема:</span> {msg_title}
            <br>
            <br><span style="color: #6d6d6d;font-style: italic;">Сообщение:</span>
            <br>{msg}
            <br>
            <br><span style="color: #6d6d6d;">--------------------------------------------------------------------------------</span>
        </p>

        <p>Вы можете <a href="{messages_inbox_url}">перейти во входящие</a>, чтобы ответить.</p>

        <p>
            <br>С наилучшими пожеланиями, Svitor.ru
        </p>
    </td>
</table>