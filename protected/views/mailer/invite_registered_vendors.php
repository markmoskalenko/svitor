<table width="650px" align="center" cellpadding="0" cellspacing="0" style="border-collapse: collapse;">
    <tr>
        <td style="border-bottom: 1px solid #869b1b;">
            <table width="100%" cellpadding="0" cellspacing="0" style="border-collapse: collapse;">
                <td align="left">
                    <a href="http://svitor.ru"><img src="http://svitor.ru/static/mail_templates/invite_registered_vendors/images/logo.png"></a>
                </td>
                <td align="center">
                    <a href="http://svitor.ru"><img src="http://svitor.ru/static/mail_templates/invite_registered_vendors/images/characters.png"></a>
                </td>
            </table>
        </td>
    </tr>
    <tr>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td style="font-family: Arial, sans-serif; font-size: 14px; line-height: 150%;">

        Здравствуйте!<br><br />

        Вы зарегистрированы в нашем каталоге компаний Svitor!<br />
        Ваш профайл на основном сайте: <a href="http://svitor.ru/vendor/{vendor_id}">http://svitor.ru/vendor/{vendor_id}</a>

        <br /><br />

        Для управления этими данными перейдите в &laquo;Кабинет компании&raquo;:
        <br /><br />
        <a href="http://vendor.svitor.ru/user/login/">http://vendor.svitor.ru/user/login/</a><br />
        Ваш логин: <b>{email}</b><br />
        Ваш пароль: <b>{password}</b>
        <br /><br />

        <b>Svitor</b> - это удобный инструмент для планирования свадьбы или любого другого типа торжества. Одним из ключевых моментов &laquo;Svitor&raquo; является каталог компаний, в котором пользователи, планирующие свадьбу или торжество, любого типа, находят себе исполнителей.

        <br /><br />

        Особенностью <a href="http://svitor.ru/catalog/vendors">каталога компаний</a> является возможность быстрого подбора нужного исполнителя, по различным критериям, с помощью фильтра. Каждая компания может находиться сразу в нескольких разделах, а каждый раздел имеет свои &laquo;характеристики&raquo;, которые могут указывать на уникальность компании. Благодаря этому, пользователи могут быстро находить именно вас!

        </td>
    </tr>
    <tr>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>
            --<br>
            С уважением, <br>
            команда &laquo;Svitor&raquo;<br />
        </td>
    </tr>
</table>