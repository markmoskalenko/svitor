
<div class="user_profile">
    <div class="user_wrap">
        <div class="avatar">
            <?php if (!empty($profile_info['img_filename'])): ?>
                <a class="upbox-button" href="<?php echo Yii::app()->ImgManager->getUrlById($profile_info['img_id'], 'large', $profile_info['img_filename']); ?>"><img src="<?php echo Yii::app()->ImgManager->getUrlById($profile_info['img_id'], 'medium', $profile_info['img_filename']); ?>"></a>
            <?php else: ?>
                <img src="<?php echo Yii::app()->ImgManager->getUrlById($profile_info['img_id'], 'medium', $profile_info['img_filename']); ?>">
            <?php endif; ?>
        </div>

        <div class="name">
            <h1><?php echo mApi::cutStr($profile_info['title'], 60); ?></h1>
            <div class="last_activity">
                (посл. активность: <?php echo mApi::humanDatePrecise($profile_info->last_activity); ?>)
            </div>
        </div>

        <div class="buttons">
            <?php $this->widget('application.widgets.VendorPanel', array('vendor_info' => $profile_info, 'profileMode' => true)); ?>
        </div>
    </div>
    <div class="info_main_wrap">
        <div class="info">
            <div class="item">
                <div class="title">Каталог</div>
            </div>

            <div class="item">
                <div class="label r_al">Рейтинг:</div>
                <div class="field">
                    <div class="wrap">
                        <b><?php echo round($profile_info->rating, 2); ?></b>
                    </div>
                </div>
            </div>

            <?php if (!empty($profile_info['Categories'])): ?>
            <div class="item">
                <div class="label r_al">В разделах:</div>
                <div class="field">
                    <?php
                    $links = '';

                    foreach ($profile_info['Categories'] as $cat)
                        $links .= '<a href="' . Yii::app()->createUrl('catalog/vendors', array('cat' => $cat['Category']['name'])).'">' . $cat['Category']['title'] . '</a>, ';

                    echo rtrim($links, ', ');
                    ?>
                </div>
            </div>
            <?php endif; ?>

            <div class="item indent-top">
                <div class="title">Основная информация</div>
            </div>

            <?php if (!empty($profile_info['contact_phone'])): ?>
            <div class="item">
                <div class="label r_al">Телефон:</div>
                <div class="field">
                    <?php
                    echo $profile_info['contact_phone'];

                    if (!empty($profile_info['contact_phone2']))
                        echo ', ' . $profile_info['contact_phone2'];

                    if (!empty($profile_info['contact_phone3']))
                        echo ', ' . $profile_info['contact_phone3'];
                    ?>
                </div>
            </div>
            <?php endif; ?>

            <?php if (!empty($profile_info['contact_site'])): ?>
            <div class="item">
                <div class="label r_al">Сайт:</div>
                <div class="field">
                    <noindex><a href="<?php echo $profile_info['contact_site']; ?>" rel="nofollow" target="_blank">посетить сайт</a></noindex>
                </div>
            </div>
            <?php endif; ?>

            <?php if (!empty($profile_info['description'])): ?>
            <div class="item">
                <div class="label r_al">О компании:</div>
                <div class="field">
                    <div class="wrap">
                        <?php echo nl2br($profile_info['description']); ?>
                    </div>
                </div>
            </div>
            <?php endif; ?>

        </div>
    </div>
</div>

<div class="vendor_profile_tabs">
    <div id="tabs" class="b-small_tabs">
        <ul class="tabs_button">
            <li>
                <a href="#attributes" id="tab_attributes" onmousedown="tab.openTab('attributes');">Характеристики</a>
            </li>
            <li>
                <a href="#photos" id="tab_photos" onmousedown="tab.openTab('photos');">Фото <?php if (!empty($profile_info['Images'])) echo '(' . count($profile_info['Images']) . ')'; ?></a>
            </li>
            <li>
                <a href="#videos" id="tab_videos" onmousedown="tab.openTab('videos');">Видео <?php if (!empty($profile_info['Videos'])) echo '(' . count($profile_info['Videos']) . ')'; ?></a>
            </li>
            <!-- <li>
                <a href="#articles" id="tab_articles" onmousedown="tab.openTab('articles');">Статьи</a>
            </li> -->
            <li>
                <a href="#faq" id="tab_faq" onmousedown="tab.openTab('faq');">FAQ <?php if (!empty($profile_info['Faq'])) echo '(' . count($profile_info['Faq']) . ')'; ?></a>
            </li>
            <li>
                <a href="#reviews" id="tab_reviews" onmousedown="tab.openTab('reviews');">Отзывы <?php if (!empty($profile_info['Reviews'])) echo '(' . count($profile_info['Reviews']) . ')'; ?></a>
            </li>
            <li>
                <a href="#address" id="tab_address" onmousedown="tab.openTab('address');">Адреса <?php if (!empty($profile_info['Address'])) echo '(' . count($profile_info['Address']) . ')'; ?></a>
            </li>
            <?php if (!empty($profile_info['Certificates'])): ?>
            <li>
                <a href="#certificates" id="tab_certificates" onmousedown="tab.openTab('certificates');">Сертификаты и
                    Лицензии (<?php echo count($profile_info['Certificates']); ?>)</a>
            </li>
            <?php endif; ?>
        </ul>

        <div id="box_attributes" class="box_content description">
            <?php $this->renderPartial('_attributes', array('data' => $profile_info, 'attribute_values' => $attribute_values)); ?>
        </div>

        <div id="box_photos" class="box_content images" style="display: none;">
            <?php $this->renderPartial('_photos', array('data' => $profile_info)); ?>
        </div>

        <div id="box_videos" class="box_content videos" style="display: none;">
            <?php $this->renderPartial('_videos', array('data' => $profile_info)); ?>
        </div>

        <div id="box_articles" class="box_content" style="display: none;">
            <span class="empty_info">Тут будут ссылки на статьи...</span>
        </div>

        <div id="box_faq" class="box_content faq" style="display: none;">
            <?php $this->renderPartial('_faq', array('data' => $profile_info)); ?>
        </div>

        <div id="box_reviews" class="box_content" style="display: none;">
            <?php $this->renderPartial('_reviews', array('data' => $profile_info)); ?>
        </div>

        <div id="box_address" class="box_content addresses" style="display: none;">
            <?php $this->renderPartial('_addresses', array('data' => $profile_info)); ?>
        </div>

        <?php if (!empty($profile_info['Certificates'])): ?>
        <div id="box_certificates" class="box_content certificates" style="display: none;">
            <?php $this->renderPartial('_certificates', array('data' => $profile_info)); ?>
        </div>
        <?php endif; ?>
    </div>
</div>

<link rel="stylesheet" type="text/css" href="/static/fancybox/helpers/jquery.fancybox-buttons.css"/>
<script type="text/javascript" src="/static/fancybox/helpers/jquery.fancybox-buttons.js"></script>
<script type="text/javascript" src="/static/fancybox/helpers/jquery.fancybox-media.js"></script>

<script src="/static/js/m_vendors.js"></script>
<script src="/static/js/m_tabs.js"></script>
<script>
    $(document).ready(function () {
        tab.init({
            'default':'attributes'
        });
    });
</script>