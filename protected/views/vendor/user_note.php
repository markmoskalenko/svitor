<div class="content" id="userNoteForm">
    <div id="msg"></div>
    <form onsubmit="return false;">
        <div id="user_note">
            <?php
            $model->note = CHtml::decode($model->note);
            echo CHtml::activeTextarea($model, 'note', array(
                'style' => 'margin-top: 15px; width: 98%; height: 80px;',
                'onkeyup' => 'formHelper.updateCounter(this, \'user_note\', 500);',
                'onclick' => 'formHelper.updateCounter(this, \'user_note\', 500);',
                'onchange' => 'formHelper.updateCounter(this, \'user_note\', 500);',
            ));
            ?>
            <span class="counter note"></span>
        </div>
    </form>
</div>
<div class="buttons">
    <div class="button_main">
        <button type="button" onclick="vendor.saveNote(this, <?php echo $vendor_id; ?>);">Сохранить</button>
    </div>
    <div class="button_grey">
        <button onclick="$.fancybox.close();">Отменить</button>
    </div>
</div>