<?php
if (!empty($data['Address'])) {
    $n = 1;
    foreach ($data['Address'] as $address) {
        $address_str = '';

        echo '<div class="element"><table><td><span class="num">' . $n . '.</span></td><td>';

        // Если страна такая же как у пользователя, то выделим жирным
        if ($address->country_id == User::getCountryId()) {
            $country_name = '<b>' . $address['Country']['name'] . '</b>, ';
        } else {
            $country_name = $address['Country']['name'];
        }
        $address_str .= $country_name;

        // Регион
        if (!empty($address->region_id))
        {
            if ($address->region_id == User::getRegionId()) {
                $region_name = ', <b>' . $address['Region']['name'] . '</b>';
            } else {
                $region_name = ', ' . $address['Region']['name'];
            }
            $address_str .= $region_name;
        }
        // Страна
        if (!empty($address->city_id))
        {
            if ($address->city_id == User::getCityId()) {
                $city_name = ', <b>' . $address['City']['name'] . '</b>';
            } else {
                $city_name = ', ' . $address['City']['name'];
            }
            $address_str .= $city_name;
        }

        // Метро
        if (!empty($address->metro_id))
            $address_str .= ', м. ' . $address['Metro']['name'];

        // Адресс (улица, дом)
        if (!empty($address->address))
            $address_str .= ', ' . $address['address'];

        $maps_url = 'http://maps.google.com/?output=embed&q=' . urlencode(strip_tags($address_str));
        echo '<a class="upbox-various fancybox.iframe" href="' . $maps_url . '">' . $address_str . '</a>';

        // Описание (ТЦ, номер бутика\салона и т.д)
        if (!empty($address->description))
            echo ', ' . $address['description'];


        echo '<br><i>';

        // Время работы
        if (!empty($address->work_time))
            echo 'Время работы: ' . $address['work_time'];

        // Телефон
        if (!empty($address->phone))
            echo ', т. ' . $address['phone'];

        echo '</i></td></table></div>';
        $n++;
    }
} else {
    echo '<span class="empty_info">Нет ни одного адреса...</span>';
}
?>