<?php
if (!empty($data['Videos'])) {
    foreach ($data['Videos'] as $video) {

        $url = $video['video_provider'] == 'vimeo' ? 'http://vimeo.com/' . $video['video_id'] : 'http://www.youtube.com/watch?v=' . $video['video_id'];

        echo '<noindex><a rel="nofollow" class="upbox-media" title="' . $video['title'] . '" href="' . $url . '">';
        echo '<img src="' . $video['thumbnail_url'] . '" width="120px" height="90px">';
        echo '</a></noindex>';
    }
} else {
    echo '<span class="empty_info">Нет ни одного видео...</span>';
}
?>