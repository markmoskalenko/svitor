<div class="content" id="userReviewForm">
    <div id="msg"></div>
    <form onsubmit="return false;">
        <div class="label_name">Оценка <span class="red_text">*</span></div>
        <div class="field">
            <?php
            echo CHtml::activeRadioButtonList($model, 'quality', array(
                '1' => 'Ужасно',
                '2' => 'Плохо',
                '3' => 'Нормально',
                '4' => 'Хорошо',
                '5' => 'Отлично'
            ), array(
                'separator' => '&nbsp;&nbsp;'
            ));
            ?>
        </div>

        <div class="label_name">Комментарий <span class="red_text">*</span></div>
        <div class="field" id="user_review">
            <?php
            echo CHtml::activeTextarea($model, 'comment', array(
                'style' => 'width: 98%; height: 80px;',
                'onkeyup' => 'formHelper.updateCounter(this, \'user_review\', 500);',
                'onclick' => 'formHelper.updateCounter(this, \'user_review\', 500);',
                'onchange' => 'formHelper.updateCounter(this, \'user_review\', 500);',
            ));
            ?>
            <span class="counter note"></span>
        </div>
    </form>
</div>
<div class="buttons">
    <div class="button_main">
        <button type="button" onclick="vendor.saveReview(this, <?php echo $vendor_id; ?>, true);">Сохранить</button>
    </div>
    <div class="button_grey">
        <button onclick="$.fancybox.close();">Отменить</button>
    </div>
</div>