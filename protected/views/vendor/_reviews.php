<?php if (!User::isGuest()): ?>
    <?php if (mApi::getFunId()): ?>
        <div class="button_main">
            <button onclick="vendor.writeReview(this, <?php echo $data['id']; ?>);">Добавить отзыв</button>
        </div>
    <?php else: ?>
        <a onclick="selectFun.<?php echo mApi::getFunType(true); ?>();">Выберите <?php echo mApi::getFunType() == 0 ? 'свадьбу' : 'торжество'; ?>, чтобы оставить отзыв.</a>
    <?php endif; ?>
<br><br>
<?php endif; ?>

<?php
if (!empty($data['Reviews'])) {
    foreach ($data['Reviews'] as $var):
        ?>
    <table width="100%">
        <td valign="top" width="55px">
            <img src="<?php echo Yii::app()->ImgManager->getUrlById($var['User']['img_id'], 'small', $var['User']['img_filename']); ?>">
        </td>
        <td valign="top" align="left">
            <b><a href="<?php echo Yii::app()->createUrl('user/profile', array('id' => $var['User']['id'])); ?>"><?php echo $var['User']['login']; ?></a></b><br>
            <?php echo $var['comment']; ?>
            <br>
            <b>Оценка:</b> <?php echo $var['quality']; ?> из 5
        </td>
    </table><br>
    <?php
    endforeach;
} else {
    echo '<span class="empty_info">Нет ни одного отзыва...</span>';
}
?>