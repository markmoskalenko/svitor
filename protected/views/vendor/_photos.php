<?php
if (!empty($data['Images'])) {
    foreach ($data['Images'] as $img) {
        echo '<a class="upbox-button" rel="upbox-vendor-photos" href="' . Yii::app()->ImgManager->getUrlById($img['img_id'], 'large', $img['img_filename']) . '" title="' . $img['description'] . '">';
        echo '<img src="' . Yii::app()->ImgManager->getUrlById($img['img_id'], 'medium', $img['img_filename']) . '"></a>';
    }
} else {
    echo '<span class="empty_info">Нет ни одной фотографии...</span>';
}
?>