<?php
if (!empty($data['Certificates'])) {
    foreach ($data['Certificates'] as $certificate) {
        echo '<a class="upbox-button" rel="upbox-certificate" href="' . Yii::app()->ImgManager->getUrlById($certificate['img_id'], 'large', $certificate['img_filename']) . '" title="' . $certificate['description'] . '">';
        echo '<img src="' . Yii::app()->ImgManager->getUrlById($certificate['img_id'], 'medium', $certificate['img_filename']) . '" class="l_fl">';
        echo '</a>';

    }
} else {
    echo '<span class="empty_info">Нет ни одного сертификата и лицензии...</span>';
}
?>