<?php
foreach ($data['Categories'] as $cat) {
    echo '<div class="attr_cat">';
    echo '<div class="attr_cat_title">' . $cat['Category']['title'] . '</div>';

    $cat_attrs = $cat['Category']->AttrsForVendor;

    echo '<div class="attrs_wrap">';
    foreach ($cat_attrs as $attr) {
        echo '<div class="attr">';
        echo '<table>';
        echo '<td class="attr_title">' . $attr['title'] . '</td>';
        echo '<td class="attr_values">';


        if (!empty($attribute_values)) {
            $values = '';
            foreach ($attribute_values as $value) {
                if ($value->attr_id == $attr->id && $value->cat_id == $cat->cat_id) {
                    if (!empty($value['Values']['value_string'])) {
                        $values .= $value['Values']['value_string'] . ', ';
                    } else {
                        $values .= $value['Values']['value_boolean'] == 1 ? 'Да' : 'Нет';
                        $values .= ', ';
                    }
                }
            }
            $values = rtrim($values, ', ');

            if (empty($values)) {
                echo '--';
            } else {
                echo $values;
            }
        }

        echo '</td>';
        echo '</table>';
        echo '</div>';
    }
    echo '</div>';
    echo '</div>';
}