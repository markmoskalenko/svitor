<div class="gallery-filters">
    <div class="categories">
        <ul>
            <li><a href="<?= Yii::app()->createUrl('gallery/index'); ?>"<?= empty($active_cat_id) ? ' class="active"' : ''; ?>>Все</a></li>
        <? if (!empty($categories)): ?>
            <? foreach ($categories as $cat): ?>
            <li><a href="<?= Yii::app()->createUrl('gallery/index', array('category' => $cat->name)); ?>"<?= $active_cat_id == $cat->id ? ' class="active"' : ''; ?>><?= $cat->title; ?></a></li>
            <? endforeach; ?>
        <? endif; ?>
        </ul>
    </div>
    <? if (!empty($vendor)): ?>
    <div class="vendor">
        Показаны фотографии только от «<b><?= $vendor->title; ?></b>» — <a href="<?= Yii::app()->createUrl('gallery/index'); ?>">показать все</a>
    </div>
    <? endif; ?>
</div>
<div class="gallery-feed">
    <div class="info">
        <span onmouseover="tooltip.show(this, 'В этот раздел попадают только лучшие фотографии из профилей компаний: оригинальные свадебные идеи, оформление зала, платья, букеты, торты, детки и т.д.', {position:'top-right', delay:'50', special_class:'white'});">Какие фотографии попадают в фотоидеи?</span>
    </div>
    <? if ($photos->getData()): ?>
        <? foreach ($photos->getData() as $photo): ?>
        <? $profile_url = Yii::app()->createUrl('vendor/profile', array('id' => $photo['Vendor']['id'])); ?>
        <div class="item">
            <div class="aside">
               <a href="<?= $profile_url; ?>" class="avatar"><img src="<?= $imageManager->getUrlById($photo['Vendor']['img_id'], 'small', $photo['Vendor']['img_filename']); ?>"></a>
            </div>
            <div class="section">
                <div class="info">
                    <span class="date"><?= mApi::humanDatePrecise($photo->added_date); ?></span>
                    <div class="title">
                        <a href="<?= $profile_url; ?>"><?= $photo['Vendor']['title']; ?></a>
                        <div class="dropdown-wrap">
                            <ul class="dropdown">
                                <li><a onclick="vendor.newMsg(this, <?= $photo['Vendor']['id']; ?>);">Написать сообщение</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <? if (!empty($photo->description)): ?>
                <div class="description"><?= $photo->description; ?></div>
                <? endif; ?>
                <div class="photo">
                    <img src="<?= $imageManager->getUrlById($photo->img_id, 'large', $photo->img_filename); ?>" alt="<?= $photo['Category']['title']; ?>">
                </div>
                <? if ($photo['Vendor']['PhotoInGalleryCount'] > 1 && (empty($vendor) || $vendor->id != $photo['Vendor']['id'])): ?>
                <div class="more">
                    <a href="<?= Yii::app()->createUrl('gallery/index', array('vendor' => $photo['Vendor']['id'])); ?>">Показать еще <?= $photo['Vendor']['PhotoInGalleryCount']; ?> фото &rarr;</a>
                </div>
                <? endif; ?>
            </div>
        </div>
         <? endforeach; ?>
    <? else: ?>
        <div class="empty">Тут пока нет ни одной фотографии... :(</div>
    <? endif; ?>
</div>
<div class="gallery-paginator">
<?php
$this->widget('CLinkPager', array(
    'pages' => $photos->pagination,
    'htmlOptions' => array('class' => 'b-default_pager'),
    'firstPageLabel' => '',
    'lastPageLabel' => '',
    'prevPageLabel' => '&larr; Назад',
    'nextPageLabel' => 'Далее &rarr;',
    'header' => '',
    'cssFile' => false
));
?>
</div>
<script src="/static/js/m_vendors.js"></script>
<script>
$(document).ready(function(){
    $(".gallery-filters").sticky({topSpacing:0, className:'fixed-filters'});
});
</script>