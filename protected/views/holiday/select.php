<div class="content">
    <div class="b-selectfun_select">
        <?php
        $coming_count = 0; // Если нету ближайших торжеств, то предложим создать новое

        if (!empty($data)): ?>
            <?php foreach ($data as $var):
                if ($var['date'] < time() && date('d.m.Y', time()) != date('d.m.Y', $var['date']))
                    continue;

                $coming_count++;
                $coming_count_html = '';
                $holiday = mApi::getHoliday();

                if (!empty($holiday) && mApi::getFunId() == $var['id'])
                    $coming_count_html = ' <span class="active">&larr; выбрано</span>';
                ?>
                <div class="element" onclick="location.assign(location.pathname+'?holiday=<?php echo $var['id']; ?>'+location.hash);">
                    <table>
                        <td><?php echo mApi::cutStr($var['title'], 100) . $coming_count_html; ?></td>
                        <td class="date">
                            <?php
                            if (date('d.m.Y', time()) == date('d.m.Y', $var['date'])) {
                                echo '<b>сегодня</b>';
                            } else {
                                echo mApi::getDateWithMonth($var['date']);
                            }
                            ?>
                        </td>
                    </table>
                </div>

                <?php endforeach; ?>
            <?php endif; ?>

        <?php if (empty($coming_count)): ?>
        <div align="center">
            <?php echo !empty($data) ? 'У вас нет ближайших торжеств...' : 'У вас нет планируемых торжеств...'; ?>
            <br><br>

            <div class="button_main">
                <button onclick="location.assign('/holiday/?create');">Перейти к созданию &rarr;</button>
            </div>
        </div>
        <?php endif; ?>
    </div>
</div>
<?php if (!empty($data)): ?>
<div class="buttons">
    <a href="/holiday/">Управление торжествами</a>
</div>
<?php endif; ?>
