<div class="content" id="holidayForm">
    <div id="msg"></div>
    <form onsubmit="return false;">
        <div class="label_name">Дата <span class="red_text">*</span></div>
        <div class="field">
            <?php
            if (!empty($holiday->date))
                $holiday->date = date('d.m.Y', $holiday->date);

            echo CHtml::activeTextField($holiday, 'date', array('style' => 'width: 90px;'));
            ?>
        </div>

        <div class="label_name">Название <span class="red_text">*</span></div>
        <div class="field">
            <?php
            $holiday->title = CHtml::decode($holiday->title);
            echo CHtml::activeTextField($holiday, 'title', array('style' => 'width: 98%;'));
            ?>
        </div>

        <div class="label_name">Описание</div>
        <div class="field">
            <?php
            $holiday->description = CHtml::decode($holiday->description);
            echo CHtml::activeTextarea($holiday, 'description', array('style' => 'width: 98%; height: 60px;'));
            ?>
        </div>
    </form>
</div>

<div class="buttons"><span id="loader"></span>

    <div class="button_main">
        <button type="button"
                onclick="holiday.sendForm(this, '<?php echo $holiday->isNewRecord ? 'add' : 'edit/id/' . $holiday->id; ?>');"><?php echo $holiday->isNewRecord ? 'Добавить' : 'Сохранить'; ?></button>
    </div>
    <div class="button_grey">
        <button onclick="$.fancybox.close();">Отменить</button>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('#UserHolidays_date').datepicker({
            changeYear:true,
            dateFormat:'dd.mm.yy',
            minDate:new Date(<?php echo date('Y') . ',' . date('m') . ' - 1,' . date('d'); ?>),
            maxDate:new Date(2020, 1 - 1, 1),
            monthNames:['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
            monthNamesShort:['янв', 'фев', 'мар', 'апр', 'май', 'июн', 'июл', 'авг', 'сен', 'окт', 'ноя', 'дек'],
            dayNamesMin:["Вс", "Пн", "Вт", "Ср", "Чт", "Пт", "Сб"],
            showAnim:'drop'
        });
    });
</script>