<?php
// Если нету торжеств, и получен параметр 'create', то откроем форму добавление нового торжества
if (empty($data) && isset($_GET['create']))
    echo '<script>$(document).ready(function(){ holiday.addNew(); });</script>';
?>

<h2>Ближайшие торжества</h2>
<div class="simple_add_button">
    <a onclick="holiday.addNew(); return false;">Спланировать новое торжество</a>
</div>
<table class="simple_table">
    <thead>
    <th align="left" width="350px">Название</th>
    <th align="left" width="110px">Дата</th>
    <th align="left">Осталось</th>
    <th width="100px"></th>
    </thead>
    <tbody>
    <?php
    if (!empty($data)):
        $coming_count = 0;
        foreach ($data as $holiday):
            if ($holiday['date'] < time() && date('d.m.Y', time()) != date('d.m.Y', $holiday['date']))
                continue;

            $coming_count++;
    ?>
        <tr class="active">
            <td align="left" onclick="holiday.setSelected(<?php echo $holiday['id']; ?>);">
                <?php echo mApi::cutStr($holiday['title'], 100); ?>
            </td>
            <td onclick="holiday.setSelected(<?php echo $holiday['id']; ?>);">
                <?php
                if (date('d.m.Y', time()) == date('d.m.Y', $holiday['date'])) {
                    echo '<b>сегодня</b>';
                } else {
                    echo mApi::getDateWithMonth($holiday['date']);
                }
                ?>
            </td>
            <td onclick="holiday.setSelected(<?php echo $holiday['id']; ?>);">
                <?php
                if (time() < $holiday['date'])
                    echo mApi::getDaysLeft($holiday['date']);
                ?>
            </td>
            <td>
                <a class="edit magic_button" onclick="holiday.goEdit(<?php echo $holiday->id; ?>); return false;" onmouseover="tooltip.show(this, 'Редактировать', {position :'top-right'});"></a>
                <a class="trash magic_button" onclick="alert('Удаление пока не доступно...'); return false;" onmouseover="tooltip.show(this, 'Удалить', {position :'top-right'});"></a>
            </td>
        </tr>
    <?php
        endforeach;
    endif;
    if (empty($coming_count))
        echo '<tr><td colspan="4" class="empty_item">Нет ближайших торжеств...</td></tr>';
    ?>

    </tbody>
</table>

<br><br>

<h2>Прошедшие</h2>
<br>

<table class="simple_table">
    <thead>
    <th align="left" width="350px">Название</th>
    <th align="left" width="110px">Дата</th>
    <th></th>
    </thead>
    <tbody>
    <?php
    $count = 0;
    foreach ($data as $holiday) {
        if ($holiday->date > time() || date('d.m.Y', time()) == date('d.m.Y', $holiday['date']))
            continue;

        $count++;
        ?>

    <tr class="active">
        <td align="left" onclick="holiday.setSelected(<?php echo $holiday['id']; ?>);">
            <?php echo $holiday['title']; ?>
        </td>
        <td onclick="holiday.setSelected(<?php echo $holiday['id']; ?>);"><?php echo mApi::getDateWithMonth($holiday['date']); ?></td>
        <td>
            <a class="trash magic_button" onclick="alert('Удаление пока не доступно...'); return false;" onmouseover="tooltip.show(this, 'Удалить', {position :'top-right'});"></a>
        </td>
    </tr>
        <?php
    }
    if ($count == 0)
        echo '<tr><td colspan="3" class="empty_item">Нет прошедших торжеств...</td></tr>';
    ?>
    </tbody>
</table>
<script src="/static/js/m_holiday.js"></script>