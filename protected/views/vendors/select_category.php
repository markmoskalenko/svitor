<div class="content">
    <?php if (mApi::getFunId()): ?>
    <?php if (!empty($categories_data)): ?>
        <div class="b-category_select">
            <?php
            foreach ($categories_data as $cat) {
                if (isset($team_data[$cat['id']])) {
                    echo '<span>' . $cat['title'] . ' (уже используется)</span>';
                } else {
                    echo '<div class="element" onclick="vendor.addToTeam(this, ' . $vendor_id . ', ' . $cat['id'] . ');">' . $cat['title'] . '</div>';
                }
            }
            ?>
        </div>
        <?php else: ?>
        <br>
        <div align="center">Нет ни одной категории. <a href="/vendors/">Перейти к созданию &rarr;</a></div>
        <?php endif; ?>
    <?php else: ?>
    <br>
    <div align="center"><?php echo mApi::getFunType() == 0 ? 'Не выбрана свадьба. <a onclick="selectFun.wedding(); return false;">Выбрать &rarr;</a>' : 'Не выбрано торжество. <a onclick="selectFun.holiday(); return false;">Выбрать &rarr;</a>'; ?></div>
    <?php endif; ?>
</div>
<?php if (!empty($categories_data)): ?>
<div class="buttons">
    <a href="/vendors/">Управление категориями</a>
</div>
<?php endif; ?>