<div class="my_vendors_title">Мои компании</div>
<h2>Выберите категории, которые хотите скрыть</h2>
Вы можете скрыть некоторые разделы. Для этого поставьте галочку напротив раздела, который хотите скрыть и нажмите "Сохранить".
<br><br>
<?php
if (!empty($success))
    echo '<span class="msg_success">' . $success . '</span>';
?>
<form action="/vendors/manager/act/edit_list/" method="POST">
    <?php
    $str = '<ul>';

    foreach ($default_categories_data as $cat) {
        if ($cat['visible'] == '0') {
            $str .= '<li><label><input type="checkbox" name="category[' . $cat['id'] . ']" checked="checked"> ' . $cat['title'] . '</label></li>';
        } else {
            $str .= '<li><label><input type="checkbox" name="category[' . $cat['id'] . ']"> ' . $cat['title'] . '</label></li>';
        }
    }
    $str .= '</ul>';
    echo $str;
    ?>
    <br><br>
    <input type="hidden" value="Lw238wefklnwelkgn83g" name="tocken">
    <input type="submit" value="Сохранить" class="b-default_button"> или <a href="/vendors/manager/">Вернуться назад</a>
</form>