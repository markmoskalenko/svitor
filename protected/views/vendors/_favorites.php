<?php if (!empty($favorites_data)): ?>
<div id="removeFromFavorite-dialog" style="display: none;" title="Удалить из избранных">Вы действительно хотите
    продолжить?
</div>
<div class="user_vendors" style="padding-top: 24px;">
    <?php foreach ($favorites_data as $var): ?>
    <div class="category c_al">
        <div class="vendor_info" id="vendor_<?php echo $var['Vendor']['id']; ?>">
            <b title="<?php echo $var['Vendor']['title']; ?>"><?php echo mApi::subStr($var['Vendor']['title'], 0, 20); ?></b>
            <br><br>
            <a class="trash magic_button"
               onmouseover="tooltip.show(this, 'Удалить из избранных', {position:'top-right'});"
               onclick="vendor.removeFromFavorite(<?php echo $var['Vendor']['id']; ?>); return false;"></a>

            <a href="<?php echo Yii::app()->createUrl('vendor/profile', array('id' => $var['Vendor']['id'])); ?>">

                <div class="logo">
                    <img src="<?php echo Yii::app()->ImgManager->getUrlById($var['Vendor']['img_id'], 'medium', $var['Vendor']['img_filename']); ?>">
                </div>
            </a>

            <div class="cat_links add_toteam">
                <?php if (isset($user_vendor_team[$var['Vendor']['id']])): ?>
                <b>(в команде)</b>
                <?php else: ?>
                <a onclick="vendor.getCategories(this, <?php echo $var['Vendor']['id']; ?>);">Добавить в команду</a>
                <?php endif; ?>
            </div>
        </div>
    </div>
    <?php endforeach; ?>
</div>

<?php else: ?>
<br><br><br>
<div class="items_empty">Нет сохраненных компаний...</div>
<?php endif; ?>