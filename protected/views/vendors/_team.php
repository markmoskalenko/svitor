<?php if (!empty($categories_data)): ?>
<div id="delete-dialog" style="display: none;" title="Удалить категорию">Категория будет удалена без возможности
    восстановления.<br><br>Вы действительно хотите продолжить?
</div>
<div id="removeFromTeam-dialog" style="display: none;" title="Открепить компанию">Вы действительно хотите продолжить?
</div>

<div class="user_vendors">

    <?php foreach ($categories_data as $cat): ?>
    <?php $team = $cat->Team; ?>
    <div class="category c_al">

        <div class="category_info"
             id="cat_<?php echo $cat['id']; ?>"<?php if (!empty($team)) echo ' style="display: none;"'; ?>>
            <a class="edit magic_button" onmouseover="tooltip.show(this, 'Редактировать категорию', {position:'top-right'});" onclick="vendor.editCat(<?php echo $cat['id']; ?>); return false;"></a>
            <a class="delete magic_button" onmouseover="tooltip.show(this, 'Удалить категорию', {position:'top-right'});" onclick="vendor.deleteCat(<?php echo $cat['id']; ?>); return false;"></a>
            <img src="/static/images/miniatures/<?php echo $cat['icon']; ?>">

            <div class="cat_title">
                <span title="<?php echo $cat['title']; ?>"><?php echo mApi::cutStr($cat['title'], 40); ?></span>
            </div>

            <div class="cat_links">
                <a href="<?php echo !empty($cat['cat_link']) ? $cat['cat_link'] : '/catalog/vendors/'; ?>">Найти в каталоге &rarr;</a>
                <a href="/vendors/favorites/">Выбрать из сохраненных</a>
            </div>
        </div>

        <?php if (!empty($team)): ?>
        <div class="vendor_info" id="vendor_<?php echo $team['Vendor']['id']; ?>" onmouseover="vendor.showTooltip(this);">
            <a class="edit magic_button" onmouseover="tooltip.show(this, 'Редактировать категорию', {position:'top-right'});" onclick="vendor.editCat(<?php echo $cat['id']; ?>); return false;"></a>
            <a class="trash magic_button" onmouseover="tooltip.show(this, 'Открепить компанию', {position:'top-right'});" onclick="vendor.removeFromTeam(this, <?php echo $team['Vendor']['id']; ?>, <?php echo $cat['id']; ?>); return false;"></a>

            <div class="vendor_tooltip">
                <div class="title">
                    <a href="<?php echo Yii::app()->createUrl('vendor/profile', array('id' => $team['Vendor']['id'])); ?>">&laquo;<?php echo mApi::cutStr($team['Vendor']['title'], 20); ?>&raquo;</a>
                </div>
                <ul>
                    <li>&#151; <a onclick="vendor.newMsg(this, <?php echo $team['Vendor']['id']; ?>); return false;">Написать сообщение</a></li>
                    <li>&#151; <a onclick="vendor.writeReview(this, <?php echo $team['Vendor']['id']; ?>);">Добавить/изменить отзыв</a></li>
                    <li>&#151; <a onclick="vendor.editNote(this, <?php echo $team['Vendor']['id']; ?>);">Заметка <span class="user_note"><?php echo !empty($team['note']) ? '*' : ''; ?></span></a></li>
                </ul>
            </div>
            <a href="<?php echo Yii::app()->createUrl('vendor/profile', array('id' => $team['Vendor']['id'])); ?>">

                <div class="logo">
                    <img src="<?php echo Yii::app()->ImgManager->getUrlById($team['Vendor']['img_id'], 'medium', $team['Vendor']['img_filename']); ?>">
                </div>

                <div class="cat_title">
                    <span title="<?php echo $cat['title']; ?>"><?php echo mApi::cutStr($cat['title'], 40); ?></span>
                </div>
            </a>
        </div>
        <?php endif; ?>

    </div>

    <?php endforeach; ?>
</div>
<?php else: ?>
<div class="items_empty">Нет категорий....</div>
<?php endif; ?>