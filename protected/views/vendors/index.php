<div class="user_vendors_title">
    <?php echo mApi::getFunType() == 1 ? 'Мои компании' : 'Наши компании'; ?>
    <span><?php $this->widget('application.widgets.FunBar', array('short_mode' => true)); ?></span>
</div>

<?php
if (mApi::getFunId()):
    ?>

<div class="simple_sortby user_vendors_tabs">
    <ul>
        <li<?php if ($action == 'team') echo ' class="active"'; ?>>
            <a href="<?php echo Yii::app()->createUrl('vendors/team'); ?>"><?php echo mApi::getFunType() == 1 ? 'Моя команда' : 'Наша команда'; ?></a>
        </li>
        <li<?php if ($action == 'favorites') echo ' class="active"'; ?>>
            <a href="<?php echo Yii::app()->createUrl('vendors/favorites'); ?>">Избранные</a>
        </li>
    </ul>
</div>


<div class="simple_add_button">
    <?php if ($action == 'team'): ?>
    <a onclick="vendor.addCat(); return false;">Добавить категорию</a>
    <?php endif; ?>
</div>

<?php
    if ($action == 'favorites') {
        $this->renderPartial('_favorites', array(
            'favorites_data' => $favorites_data,
            'user_vendor_team' => $user_vendor_team
        ));
    } else {
        $this->renderPartial('_team', array(
            'categories_data' => $categories_data
        ));
    }
    ?>
<script src="/static/js/m_vendors.js"></script>

<?php else:
    $this->widget('application.widgets.SelectFun');
endif;
?>