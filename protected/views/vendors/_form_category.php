<div class="content" id="userCatForm">
    <div id="msg"></div>
    <form onsubmit="return false;">
        <div class="label_name">Название <span class="red_text">*</span></div>
        <div class="field">
            <?php
            echo CHtml::activeTextField($model, 'title', array(
                'style' => 'width: 98%;'
            ));
            ?>
        </div>
    </form>
</div>
<div class="buttons">
    <div class="button_main">
        <button type="button" id="send_button"
                onclick="upBox.sendForm(this, '/vendors/<?php echo $model->isNewRecord ? 'addCategory' : 'editCategory/id/' . $model->id; ?>', $('#userCatForm form').serialize());"><?php echo $model->isNewRecord ? 'Добавить' : 'Сохранить'; ?></button>
    </div>
    <div class="button_grey">
        <button onclick="$.fancybox.close();">Отменить</button>
    </div>
</div>