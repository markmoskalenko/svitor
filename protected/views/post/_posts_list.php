<?php foreach ($posts as $post): ?>
<?php $url = Yii::app()->createUrl('post/view', array('id' => $post->id)); ?>

<div class="post_wrap">
    <div class="post">
        <div class="title">
            <a href="<?php echo $url; ?>"><?php echo $post->title; ?></a>
        </div>
        <div class="body">
            <?php echo $post->content_1; ?>
            <div><a href="<?php echo $url; ?>">Читать дальше &rarr;</a></div>
        </div>
        <div class="post_infopanel">
            <ul class="info">
                <li>
                    <?php if (!empty($post['User'])): ?>
                    <?php $url = Yii::app()->createUrl('user/profile', array('id' => $post['User']['id'])); ?>
                    <b>Автор:</b> <a href="<?php echo $url; ?>"><?php echo $post['User']['login']; ?></a>

                    <?php elseif (!empty($post['Vendor'])): ?>
                    <?php $url = Yii::app()->createUrl('vendor/profile', array('id' => $post['Vendor']['id'])); ?>

                    <b>Автор:</b> &laquo;<a href="<?php echo $url; ?>"><?php echo mApi::cutStr($post['Vendor']['title'], 30); ?></a>&raquo;

                    <?php else: ?>
                    <b>Источник:</b> <a href="http://<?php echo Yii::app()->request->getServerName(); ?>"><?php echo Yii::app()->request->getServerName(); ?></a>
                    <?php endif; ?>
                </li>
                <li class="date">
                    <?php echo mApi::humanDatePrecise($post->published_date); ?>
                </li>
                <?php if (User::getSecurityLevel() > 0): ?>
                <?php Yii::app()->user->setReturnUrl(Yii::app()->request->url); ?>
                <li>
                    <a href="/mod_cp/post/edit/id/<?php echo $post->id; ?>">ред.</a>
                </li>
                <?php endif; ?>
            </ul>
            <div class="share42init sm_share r_fl"></div>
        </div>

    </div>
</div>

<?php endforeach; ?>
<?php
$this->widget('CLinkPager', array(
    'pages' => $posts_pagination,
    'htmlOptions' => array('class' => 'b-default_pager'),
    'firstPageLabel' => '',
    'lastPageLabel' => '',
    'prevPageLabel' => '&larr; Назад',
    'nextPageLabel' => 'Далее &rarr;',
    'header' => '',
    'cssFile' => false
));
?>