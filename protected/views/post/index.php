<table class="b-catalog-table">
    <td class="menu catalog_categories">
        <div class="button_main">
            <button onclick="location.assign('<?php echo Yii::app()->createUrl('post/add'); ?>');">+ Написать пост</button>
        </div>

        <?php if (!empty($categories)): ?>
        <?php foreach ($categories as $cat): ?>
            <div class="root<?php if (!empty($active_category) && $active_category->id == $cat->id) echo ' active'; ?>">
                <a href="<?php echo Yii::app()->createUrl('post/index', array('cat' => $cat->name)); ?>"><?php echo $cat->title; ?></a>
            </div>
            <?php endforeach; ?>
        <?php endif; ?>
    </td>

    <td class="content">
        <div class="posts_search">
            <?php echo CHtml::beginForm(Yii::app()->createUrl('post/index'), 'GET'); ?>
                <?php echo CHtml::textField('q', Yii::app()->request->getParam('q'), array('placeholder' => 'Поиск')); ?>
                <div class="button_grey">
                    <button type="submit">Найти</button>
                </div>
            <?php echo CHtml::endForm(); ?>
        </div>

        <?php
        if (!empty($posts)) {
            $this->renderPartial('_posts_list', array('posts' => $posts, 'posts_pagination' => $posts_pagination));
        } else {
            echo 'К сожалению нам не удалось ничего найти... :(';
        }
        ?>
    </td>
    <!-- <td class="params">

    </td> -->
</table>