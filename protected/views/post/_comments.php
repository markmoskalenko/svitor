<?php if (!empty($comments)): ?>
    <?php foreach ($comments as $comment): ?>
    <?php $profile_url = Yii::app()->createUrl('user/profile', array('id' => $comment['User']['id'])); ?>

    <table id="comment_<?php echo $comment['id']; ?>" class="comment_table<?php if ($comment->deleted == 1) echo ' magic_button'; ?>">
    <tr>
        <td valign="top" width="55px">
            <a href="<?php echo $profile_url; ?>"><img src="<?php echo Yii::app()->ImgManager->getUrlById($comment['User']['img_id'], 'small', $comment['User']['img_filename']); ?>"></a>
        </td>
        <td valign="top" style="padding: 0 5px 5px;">
            <a href="<?php echo $profile_url; ?>" class="username"><?php echo $comment['User']['login']; ?></a>

            <?php if ($comment->deleted == 0): ?>
                <div class="comment_text"><?php echo nl2br($comment['comment']); ?></div>
                <div class="comment_info"><?php echo mApi::humanDatePrecise($comment['date']); ?>

                    <?php if ($comment['User']['id'] == User::getId()): ?>
                     | <a onclick="info.removeComment(<?php echo $comment['id']; ?>); return false;">удалить</a>
                    <?php endif; ?>
                </div>
            <?php else: ?>
                <div class="comment_deleted">Пользователь удалил свой комментарий...</div>
            <?php endif; ?>
        </td>
    </tr>
    </table>
    <?php endforeach; ?>
<?php else: ?>
    <div style="margin-top: 15px;">Пока нет ни одного комментария...</div>
<?php endif; ?>

<br>

<?php
$this->widget('CLinkPager', array(
    'pages' => $comments_pagination,
    'htmlOptions' => array('class' => 'b-default_pager'),
    'firstPageLabel' => '',
    'lastPageLabel' => '',
    'prevPageLabel' => '&larr; Назад',
    'nextPageLabel' => 'Далее &rarr;',
    'header' => ''
));
?>
<div id="trash-comment-dialog" title="Удалить комментарий" style="display: none;">Вы действительно хотите удалить свой комментарий?</div>

<div style="margin-top: 10px;" id="user_comment">
    <?php if (User::isGuest()): ?>

        Только <a href="<?php echo Yii::app()->createUrl('user/registration'); ?>">зарегистрированные</a> пользователи могут
        оставлять комментарии. <a href="<?php echo Yii::app()->createUrl('user/login'); ?>">Войти</a>.

    <?php else: ?>

        <b>Ваш комментарий:</b>
        <div id="comment_error"></div>

        <textarea style="width: 100%; height: 50px; margin: 10px 0;" id="comment"></textarea>

        <div class="button_main">
            <button onclick="info.sendComment(this, <?php echo $post_id; ?>);">Добавить</button>
        </div>
        <span style="color: #939393;"> (комментарий будет проверен модератором)</span>

    <?php endif; ?>
</div>