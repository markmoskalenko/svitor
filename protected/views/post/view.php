<?php
if($post->id == 50){
    $this->pageTitle='Русская свадьба и ее традиции';
    $this->pageDescription='На нашем портале Вы найдете все о традициях русской свадьбы.';
    $this->pageKeywords='русская, свадьба, традиции, портал, svitor.';
}elseif($post->id == 52){
    $this->pageTitle='Организация и проведение корпоративных мероприятий';
    $this->pageDescription='Мы подскажем Вам, с чего начать и как организовать корпоратив.';
    $this->pageKeywords='организация, корпоратив, портал, svitor.';
}
?>
<div class="post_wrap" style="width: 880px; margin: 0 auto;">
    <a href="<?php echo Yii::app()->createUrl('post/index'); ?>">Обмен опытом</a> / <a href="<?php echo Yii::app()->createUrl('post/index', array('cat' => $post['PostCategory']['name'])); ?>"><?php echo $post['PostCategory']['title']; ?></a>

    <h1 class="title"><?php echo $post->title; ?></h1>

    <div class="b-media-thumbs" id="images_thumbs">
        <?php foreach($postImages as $image):?>
        <div class="img" id="img_<?php echo $image["id"];?>">
            <a class="upbox-button" rel="upbox-button" href="<?php echo $image["image_big"];?>"><img src="<?php echo $image["image_medium"];?>"></a>
        </div>
        <?php endforeach; ?>
    </div>
    <div class="post_fulltext">
        <?php echo $post->content_1 . $post->content_2; ?>
    </div>

    <div class="post_infopanel">
        <ul class="info">
            <li>
                <?php if (!empty($post['User'])): ?>
                <?php $url = Yii::app()->createUrl('user/profile', array('id' => $post['User']['id'])); ?>
                <b>Автор:</b> <a href="<?php echo $url; ?>"><?php echo $post['User']['login']; ?></a>

                <?php elseif (!empty($post['Vendor'])): ?>
                <?php $url = Yii::app()->createUrl('vendor/profile', array('id' => $post['Vendor']['id'])); ?>

                <b>Автор:</b> &laquo;<a href="<?php echo $url; ?>"><?php echo mApi::cutStr($post['Vendor']['title'], 30); ?></a>&raquo;

                <?php else: ?>
                <b>Источник:</b> <a href="http://<?php echo Yii::app()->request->getServerName(); ?>"><?php echo Yii::app()->request->getServerName(); ?></a>
                <?php endif; ?>
            </li>
            <li class="date">
                <?php echo mApi::humanDatePrecise($post->published_date); ?>
            </li>
            <?php if (User::getSecurityLevel() > 0): ?>
            <?php Yii::app()->user->setReturnUrl(Yii::app()->request->url); ?>
            <li>
                <a href="/mod_cp/post/edit/id/<?php echo $post->id; ?>">ред.</a>
            </li>
            <?php endif; ?>
        </ul>
        <div class="share42init sm_share r_fl"></div>
    </div>

    <?php if (!empty($recommendations)): ?>
    <div class="recommendations_wrap">
        <h3>Читайте также</h3>
        <ul>
            <?php foreach ($recommendations as $recommend_post): ?>
            <?php $url = Yii::app()->createUrl('post/view', array('id' => $recommend_post['id'])); ?>
            <li>
                <a href="<?php echo $url; ?>"><?php echo mApi::cutStr($recommend_post['title'], 120); ?></a></li>
            <?php endforeach; ?>
        </ul>
    </div>
    <?php endif; ?>
    <div class="comments_wrap">
        <h3 id="comments">Комментарии<?php echo !empty($comments_count) ? ' (' . $comments_count . ')' : ''; ?></h3>

        <?php
        $this->renderPartial('_comments', array(
            'comments_pagination' => $comments_pagination,
            'comments' => $comments,
            'post_id' => $post->id
        ))
        ?>
    </div>
</div>

<script src="/static/js/share42.js?<?php echo Yii::app()->params['version']['js']; ?>"></script>
<script src="/static/js/m_posts.js?<?php echo Yii::app()->params['version']['js']; ?>"></script>

<link rel="stylesheet" type="text/css" href="/static/fancybox/helpers/jquery.fancybox-buttons.css?<?php echo Yii::app()->params['version']['css']; ?>">
<script type="text/javascript" src="/static/fancybox/helpers/jquery.fancybox-buttons.js?<?php echo Yii::app()->params['version']['js']; ?>"></script>
