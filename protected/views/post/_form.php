<h2>Новый пост</h2>
<br>
<?php if (Yii::app()->user->hasFlash('add_post_success')): ?>
<div class="msg_success"><?php echo Yii::app()->user->getFlash('add_post_success'); ?></div>
<?php else: ?>
<div class="simple_desc">
    Постарайтесь написать полезную для других информацию, отразив с какими проблемами вы столкнулись при организации или проведении праздника, от чего бы вы хотели уберечь читателя. Возможно, вы просто захотите рассказать об "изюминке" вашего праздника? Это тоже может быть кому-то полезно!<br><br>
</div>
<div class="simple_form">
    <?php echo CHtml::beginForm(); ?>

    <div class="item">
        <div class="label">Раздел:</div>
        <div class="field">
            <?php echo CHtml::activeDropDownList($model, 'post_cat_id', $categories, array('empty' => '-- не выбрано --')); ?>
            <?php echo CHtml::error($model, 'post_cat_id'); ?>
        </div>
    </div>

    <div class="item">
        <div class="label">Название:</div>
        <div class="field d_width_50">
            <?php echo CHtml::activeTextField($model, 'title'); ?>
            <?php echo CHtml::error($model, 'title'); ?>
        </div>
    </div>

    <div class="item">
        <div class="field">
            <div class="button_main"><button id="upload_image">Загрузить фотографии &rarr;</button></div>
        </div>
    </div>

    <div class="item">
        <div class="field d_width_100">
            <?php echo CHtml::activeTextArea($model, 'content_2'); ?>
            <?php echo CHtml::error($model, 'content_2'); ?>
        </div>
    </div>

    <div class="item">
        <div class="field">
            <div class="button_main"><button id="upload_images">Загрузить фотографии &rarr;</button></div>
        </div>
    </div>

    <div class="item">
        <div class="b-media-thumbs_title">Изображения <span id="images_counter"><span>0</span> / <?php echo $max_images; ?></span></div>
        <div class="b-media-thumbs" id="images_thumbs"></div>
        <span id="loader"></span>
        <input id="imagePost" type="hidden" name="images" value="">
    </div>

    <div class="item">
        <div class="field">
            <div class="button_main">
                <button><?php echo $model->isNewRecord ? 'Опубликовать' : 'Сохранить изменения'; ?></button>
            </div>
        </div>
    </div>

    <?php echo CHtml::endForm(); ?>
</div>

<script type="text/javascript" src="/static/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="/static/ckeditor/adapters/jquery.js"></script>
<script src="/static/js/m_post_add.js"></script>
<script src="/static/js/ajaxupload.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('#Post_content_2').ckeditor();
        post_add.uploadImg($('#upload_image'));
        post_add.uploadImg($('#upload_images'));
    });
</script>
<?php endif; ?>