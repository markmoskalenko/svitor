<div class="clear"></div>
<?php foreach ($data as $cat){
        echo '<div class="b-catalog_cat">';
        echo '<a href="' . Yii::app()->createUrl('catalog/' . Yii::app()->controller->action->getId(), array('cat' => $cat['name'])) . '">';

        if (!empty($cat->icon)) {
            echo '<img src="/static/images/categories/' . $cat['icon'] . '">';
        } else {
            echo '<img src="/static/images/nophoto_medium.jpg">';
        }

        echo '<br>' . $cat['title'] . '</a>';
        echo '</div>';
} ?>
<div class="clear"></div>
