<?php
// Для формирования ссылки для удаления атрибута из фильтра
function getRemoveLink($attr_id, $get_value, $type)
{
    if (empty($attr_id) || empty($get_value))
        return;

    $params = $_GET;
    unset($params['page']);
    arsort($params);

    if (!empty($params[$attr_id])) {
        foreach ($params[$attr_id] as $n => $value) {
            if ($params[$attr_id][$n] == $get_value)
                unset($params[$attr_id][$n]);
        }
    }

    return str_replace('//', '/', urldecode(Yii::app()->createUrl('catalog/' . $type, $params)));
}

// Обработка данных из запроса и формирование данных для отображения фильтра
if (!empty($_GET)) {
    $filter = '';
    // Перебираем Атрибут => Значение
    foreach ($_GET as $get_attr_id => $get_values) {
        if (is_array($get_values)) {
            // Перебираем значения. Их может быть несколько, у одного аттрибута.
            foreach ($get_values as $val) {
                $attribute_str = '';
                // Подставляем название атрибута + название значения, в зависимости от типа
                foreach ($attributes as $attr) {
                    // Аттрибут
                    if ($attr['id'] == $get_attr_id) {
                        // Если тип логический, то просто название атрибута
                        if ($attr->type == 'boolean') {
                            $attribute_str .= '<table>';
                            $attribute_str .= '<td class="remove"><a href="' . getRemoveLink($get_attr_id, $val, $type) . '">×</a></td><td><b>' . $attr['title'] . '</b></td>';
                            $attribute_str .= '</table>';
                        } // Если тип строковый то название атрибута + значение
                        else {
                            $value_info = '';
                            foreach ($attr['Values'] as $attr_value) {
                                if ($attr_value['id'] == $val)
                                    $value_info = $attr_value;
                            }

                            // Если указанное значение не найдено, то не показываем атрибут в списке фильтра
                            if (empty($value_info))
                                continue;

                            $attribute_str .= '<table>';
                            $attribute_str .= '<td class="remove"><a href="' . getRemoveLink($get_attr_id, $value_info->id, $type) . '">×</a></td><td><b>' . $attr['title'] . ':</b> ' . $value_info->value_string . '</td>';
                            $attribute_str .= '</table>';
                        }
                    }
                }

                if (!empty($attribute_str))
                    $filter .= '<li>' . $attribute_str . '</li>';
            }
        }
    }
    if (!empty($filter)) {
        $str = '<div class="b-filter">';
        $str .= '<ul>';
        $str .= $filter;
        $str .= '</ul>';

        $params['cat'] = $category->name;

        if (!empty($_GET['country_id']))
            $params['country_id'] = intval($_GET['country_id']);

        if (!empty($_GET['region_id']))
            $params['region_id'] = intval($_GET['region_id']);

        if (!empty($_GET['city_id']))
            $params['city_id'] = intval($_GET['city_id']);

        if (!empty($_GET['metro_id']))
            $params['metro_id'] = intval($_GET['metro_id']);

        $str .= '<div class="reset_all">&uarr; <a href="' . Yii::app()->createUrl('catalog/' . $type, $params) . '">убрать все параметры</a></div>';
        $str .= '</div>';

        echo $str;
    } else if (!empty($attributes)) {
        echo '<div class="b-filter" align="center">Выберите необходимые параметры</div>';
    }
}

// Регионы
if ($type == 'vendors')
    $this->renderPartial('_regions', array('area_data' => $area_data, 'type' => $type));

// Список атрибутов
$this->renderPartial('_attributes', array('attributes' => $attributes, 'type' => $type));