<div class="b-attributes">
    <?php
    if (!empty($attributes)) {
        function createFilterUrl($attr, $value, $type)
        {
            $params = $_GET;
            $params[$attr . '[]'] = $value;
            unset($params['page']);
            return urldecode(Yii::app()->createUrl('catalog/' . $type, $params));
        }

        $boolean_li = '';
        foreach ($attributes as $attr) {
            if (isset($_GET[$attr->id]))
                continue;

            if ($attr->type == 'boolean') {
                foreach ($attr['Values'] as $value) {
                    if ($value->value_boolean == 1)
                        $boolean_li .= '<li><a href="' . createFilterUrl($attr->id, $value['id'], $type) . '">' . $attr['title'].' '.$value->countOnFilter . '</a></li>';
                }
            }
        }

        if (!empty($boolean_li)) {
            echo '<div class="attr">';
            echo '<div class="title">Особенности</div>';
            echo '<ul>' . $boolean_li . '</ul>';
            echo '</div>';
        }

        $i = 0;
        $hidden_more = false;
        $show_more_trigger = false;

        foreach ($attributes as $attr) {
            if (isset($_GET[$attr->id]))
                continue;

            $string_li = '';
            if ($attr->type == 'string') {
                foreach ($attr['Values'] as $value) {
                    if ($attr->is_multi == 1) {
                        $string_li .= '<li><input type="checkbox" name="' . $attr->id . '[]" value="' . $value['id'] . '"> <a href="' . createFilterUrl($attr->id, $value['id'], $type) . '"> ' . $value['value_string'].' '.$value->countOnFilter . '</a></li>';
                    } else {
                        $string_li .= '<li><a href="' . createFilterUrl($attr->id, $value['id'], $type) . '"> ' . $value['value_string'].' '.$value->countOnFilter . '</a></li>';
                    }
                }
            }

            // for show more attributes
            if ($i == 3 && $hidden_more == false) {
                echo '<div id="more_attributes" style="display: none;">';
                $hidden_more = true;
            }

            if ($string_li == '')
                continue;

            echo '<div class="attr" id="attr_' . $attr->id . '">';
            if ($attr->is_multi == 1) {
                echo '<div class="title">' . $attr['title'] . '</div>';
                echo '<ul>' . $string_li . '</ul>';
                echo '<div class="multy">&uarr; ';
                echo '<a onclick="catalog.showMulty(' . $attr->id . '); return false;" class="show_multy">несколько значений</a>';
                echo '<a class="apply_multy" onclick="catalog.applyMulty(this, ' . $attr->id . '); return false;">применить</a>';
                echo '</div>';
            } else {
                echo '<div class="title">' . $attr['title'] . '</div>';
                echo '<ul>' . $string_li . '</ul>';
            }
            echo '</div>';

            $show_more_trigger = true;
            $i++;
        }

        if ($hidden_more) {
            echo '</div>';
            if ($show_more_trigger)
                echo '<div class="show_more_params"><a onclick="catalog.moreAttrs(this); return false;">Показать еще параметры...</a></div>';
        }
    }
    ?>

</div>
