<div class="b-regions">
    <?php
    $params = $_GET;
    unset($params['country_id']);
    unset($params['region_id']);
    unset($params['city_id']);
    unset($params['metro_id']);
    ?>
    <?php echo CHtml::beginForm(Yii::app()->createUrl('catalog/' . $type, $params), 'GET'); ?>
    <table>
        <tr>
            <td class="td_name" style="width: 120px">Страна:</td>
            <td id="filter_country">
                <?php
                echo CHtml::dropDownList('country_id', Yii::app()->request->getParam('country_id'), CHtml::listData($area_data['countries'], 'country_id', 'name'), array(
                    'empty' => '--- не выбрано ---',
                    'onchange' => '$(\'#filter_region, #filter_city, #filter_metro\').remove(); submit();'
                ));
                ?> <span id="filter_region_loader"></span>
            </td>
        </tr>
        <?php if (!empty($area_data['regions'])): ?>
        <tr id="filter_region">
            <td class="td_name">Регион:</td>
            <td>
                <?php
                echo CHtml::dropDownList('region_id', Yii::app()->request->getParam('region_id'), CHtml::listData($area_data['regions'], 'region_id', 'name'), array(
                    'empty' => '--- не выбрано ---',
                    'onchange' => '$(\'#filter_city, #filter_metro\').remove(); submit();'
                ));
                ?> <span id="filter_city_loader"></span>
            </td>
        </tr>
        <?php endif; ?>
        <?php if (!empty($area_data['cities'])): ?>
        <tr id="filter_city">
            <td class="td_name">Город:</td>
            <td>
                <?php
                echo CHtml::dropDownList('city_id', Yii::app()->request->getParam('city_id'), CHtml::listData($area_data['cities'], 'city_id', 'name'), array(
                    'empty' => '--- не выбрано ---',
                    'onchange' => '$(\'#filter_metro\').remove(); submit();'
                ));
                ?>
            </td>
        </tr>
        <?php endif; ?>
        <?php if (!empty($area_data['metro_stantions'])): ?>
        <tr id="filter_metro">
            <td class="left top">Метро:</td>
            <td class="right">
                <?php
                echo CHtml::dropDownList('metro_id', Yii::app()->request->getParam('metro_id'), CHtml::listData($area_data['metro_stantions'], 'id', 'name'), array(
                    'class' => 'fixed',
                    'empty' => '--- не выбрано ---',
                    'onchange' => 'submit();'
                ));
                ?>
            </td>
        </tr>
        <?php endif; ?>
    </table>
    <?php echo CHtml::endForm(); ?>
</div>