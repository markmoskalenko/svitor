
<?php
if (!empty($vendors)) {
    foreach ($vendors as $n => $vendor) {
        $view_link = Yii::app()->createUrl('vendor/profile', array('id' => $vendor['id']));

        $special_class = '';
        if ($n == 0)
            $special_class = ' item_first';
        ?>
    <div class="item<?php echo $special_class; ?>">
        <table>
            <td class="icon">
                <a href="<?php echo $view_link; ?>"><img
                        src="<?php echo Yii::app()->ImgManager->getUrlById($vendor->img_id, 'medium', $vendor->img_filename); ?>"></a>
            </td>
            <td class="general">
                <div class="title"><a href="<?php echo $view_link; ?>"><?php echo $vendor['title']; ?></a></div>
                <div class="desc">
                    <?php
                    if (mb_strlen($vendor['description']) > 150) {
                        echo mb_substr($vendor['description'], 0, 145, Yii::app()->charset) . '... <a href="' . $view_link . '#about">далее &rarr;</a>';
                    } else {
                        echo $vendor['description'];
                    }
                    ?>
                </div>
                <div class="info">
                    <ul>
                        <li>
                            <?php
                            if (isset($user_vendor_favorite[$vendor['id']])) {
                                echo '<span class="vendor-saved vendor_action_links">Сохранено</span>';
                            } else {
                                echo '<a class="vendor-save vendor_action_links" onclick="vendor.addToFavorite(this, ' . $vendor['id'] . '); return false;">Сохранить</a>';
                            }
                            ?>
                        </li>
                        <li><a href="<?php echo $view_link . '#reviews'; ?>">Отзывы
                            (<?php echo $vendor['ReviewsCount']; ?>)</a></li>
                        <li>Рейтинг <b><?php echo round($vendor['rating'], 2); ?></b></li>
                        <?php echo isset($user_vendor_team[$vendor['id']]) ? '<li><b>(в команде)</b></li>' : ''; ?>
                    </ul>
                </div>
            </td>
        </table>
    </div>
    <?php
    }
    ?>
<div class="r_fl">
    <?php
    $this->widget('CLinkPager', array(
        'pages' => $pages,
        'htmlOptions' => array('class' => 'b-default_pager'),
        'firstPageLabel' => '',
        'lastPageLabel' => '',
        'prevPageLabel' => '&larr; Назад',
        'nextPageLabel' => 'Далее &rarr;',
        'header' => '',
        'cssFile' => false
    ));
    ?>
</div>
<?php
} else {
    echo 'Не найдено ни одной компании... :(';
}
?>