<?php
    $this->pageTitle        = isset( $category->title_catalog )         ? $category->title_catalog : '';
    $this->pageDescription  = isset( $category->description_catalog )   ? $category->description_catalog : '';
    $this->pageKeywords     = isset( $category->keywords_catalog )      ? $category->keywords_catalog : '';
?>
<table class="b-catalog-table">
    <td class="menu catalog_categories">
        <div class="button_main">
            <button onclick="window.location = '/vendor_cp/user/registration/'">+ Добавить компанию</button>
        </div>
        <?php
        $this->widget('application.widgets.CategoryTree', array('type' => $type, 'active_category' => $category));
        ?>
    </td>

    <td class="content">


        <?php
        $head_name = !empty($category) ? '<a href="/catalog/' . $type . '/">Компании</a> &rarr; ' . $category->title : 'Компании';
        ?>
        <h1><?php echo $head_name; ?></h1>

        <div class="search-form">
            <?php
            $params = $_GET;
            unset($params['q']);
            ?>
            <?php echo CHtml::beginForm(Yii::app()->createUrl('catalog/vendors', $params), 'GET'); ?>
            <?php echo CHtml::textField('q', Yii::app()->request->getParam('q'), array('placeholder' => 'Поиск по компаниям')); ?>
            <div class="button_grey">
                <button type="submit">Найти</button>
            </div>
            <?php echo CHtml::endForm(); ?>
        </div>

        <?php if(
            !count($vendors)                            &&
            $category                                   &&
            isset( $category->text_header_catalog )     &&
            !empty( $category->text_header_catalog ) ):
            ?>
            <div style="margin: 0 0 20px;"><?php echo $category->text_header_catalog; ?></div>
        <?php endif; ?>

        <?php
        // Корни (если не выбран раздел)
        if (!empty($roots) && !Yii::app()->request->getParam('q')) {
            $this->renderPartial('_descendants', array(
                                                      'data'    => $roots,
                                                      'parent'  => $category,
                                                 ));
        }
        // Предки
        else if (!empty($descendants) && !Yii::app()->request->getParam('q')) {
            $this->renderPartial('_descendants', array(
                                                      'data'    => $descendants,
                                                      'parent'  => $category,
                                                 ));
        }
        // Список компаний
        else if (isset($vendors))
        {
            if (empty($descendants))
                $showAttributes = true;

            $this->renderPartial('_vendors_list', array('vendors' => $vendors, 'user_vendor_favorite' => $user_vendor_favorite, 'user_vendor_team' => $user_vendor_team, 'pages' => $pages, 'category' => $category));
        }
        // Исключения
        else {
            if (empty($category))
                echo 'Раздел не существует...';
            else
                echo 'Раздел не содержит компаний...';
        }
        ?>

        <?php if(
            count($vendors)                             &&
            $category                                   &&
            isset( $category->text_header_catalog )     &&
            !empty( $category->text_header_catalog ) ):
            ?>
            <div style="margin: 20px 0 0"><?php echo $category->text_header_catalog; ?></div>
        <?php endif; ?>

        <?php if( $category && isset( $category->text_footer_catalog ) && !empty( $category->text_footer_catalog ) ): ?>
            <div  style="margin: 20px 0 0"><?php echo $category->text_footer_catalog; ?></div>
        <?php endif; ?>
    </td>

    <?php if ($showAttributes && !empty($category)): ?>
    <td class="params">
        <?php $this->actionFilters($category, $type); ?>
    </td>
    <?php endif; ?>

</table>
<script src="/static/js/m_vendors.js"></script>
<script src="/static/js/m_catalog.js"></script>