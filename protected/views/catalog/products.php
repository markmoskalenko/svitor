<?php
$this->pageTitle        = isset( $category->title_product )         ? $category->title_product : '';
$this->pageDescription  = isset( $category->description_product )   ? $category->description_product : '';
$this->pageKeywords     = isset( $category->keywords_product )      ? $category->keywords_product : '';
?>
<table class="b-catalog-table">
    <td class="menu catalog_categories">
        <?php
        $this->widget('application.widgets.CategoryTree', array('type' => $type, 'active_category' => $category));
        ?>
    </td>

    <td class="content">


        <?php
        if (!empty($product_view)) {
            $head_name = !empty($category) ? '<a href="' . Yii::app()->createUrl('catalog/' . $type) . '">Товары</a> &rarr; <a href="' . Yii::app()->createUrl('catalog/' . $type, array('cat' => $category->name)) . '">' . $category->title . '</a>' : 'Товары';
        } else {
            $head_name = !empty($category) ? '<a href="' . Yii::app()->createUrl('catalog/' . $type) . '">Товары</a> &rarr; ' . $category->title : 'Товары';
        }
        ?>
        <h1><?php echo $head_name; ?></h1>

        <?php if(
            !count($products)                            &&
            $category                                   &&
            isset( $category->text_header_product )     &&
            !empty( $category->text_header_product ) ):
            ?>
            <div style="margin: 0 0 20px;"><?php echo $category->text_header_product; ?></div>
        <?php endif; ?>

        <?php
        // Корни (если не выбран раздел)
        if (!empty($roots)) {
            $this->renderPartial('_descendants', array('data' => $roots));
        }
        // Предки
        else if (!empty($descendants)) {
            $this->renderPartial('_descendants', array('data' => $descendants));
        }
        // Список товаров
        else if (!empty($products) || isset($products) && empty($product_view)) {
            $showAttributes = true;
            $this->renderPartial('_products_list', array('products' => $products, 'pages' => $pages, 'category' => $category));
        }
        // Просматриваемый твовар
        else if (!empty($product_view)) {
            $this->renderPartial('_product_view', array('product' => $product_view, 'category' => $category));
        }
        // Исключения
        else {
            if (empty($category))
                echo 'Раздел не существует...';
            else
                echo 'Раздел не содержит товаров...';
        }
        ?>

        <?php if(
            count($products)                            &&
            $category                                   &&
            isset( $category->text_header_product )     &&
            !empty( $category->text_header_product ) ):
            ?>
            <div style="margin: 20px 0 0"><?php echo $category->text_header_product; ?></div>
        <?php endif; ?>

        <?php if( $category && isset( $category->text_footer_product ) && !empty( $category->text_footer_product ) ): ?>
            <div  style="margin: 20px 0 0"><?php echo $category->text_footer_product; ?></div>
        <?php endif; ?>
    </td>

    <?php if ($showAttributes && $category->id): ?>
    <td class="params">
        <?php $this->actionFilters($category, $type); ?>
    </td>
    <?php endif; ?>

</table>
<script src="/static/js/m_vendors.js"></script>
<script src="/static/js/m_catalog.js"></script>


