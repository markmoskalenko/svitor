<?php
if (!empty($product)) {
    $images = '';
    ?>
    <table class="b-catalog_itemview" <?php echo ($product['is_delete'] ? 'style="background-color: #F8F7F7;"' : ''); ?>>
        <td class="product">
            <h1>
                <?php echo $product['title']; ?>
                <?php if($product['is_delete']): ?>
                    - <span style="font-weight: bold; color: red;">Товар больше не продается</span>
                <?php endif; ?>
            </h1>

            <div class="description"><?php echo $product['description']; ?></div>

            <div class="media">
                <?php if (!empty($product['Images'])): ?>
                    <div class="images left">
                        <div class="title">Изображения</div>
                        <?php foreach ($product['Images'] as $i => $img):
                            $size = $i == 0 ? 'medium' : 'small';
                            ?>
                            <a class="upbox-button" rel="upbox-product-images" href="<?php echo Yii::app()->ImgManager->getUrlById($img->img_id, 'large', $img->img_filename); ?>" title="<?php echo $img->description; ?>"><img src="<?php echo Yii::app()->ImgManager->getUrlById($img->img_id, $size, $img->img_filename); ?>"></a>
                        <?php endforeach; ?>
                    </div>
                <?php endif; ?>
                            <div class="right">
                                <div class="cart button_main">
<!--                                    --><?php //if( !Yii::app()->shoppingCart->contains( 'Product'.$product['id'] ) ): ?>
<!--                                        --><?php //echo CHtml::link('В корзину','javascript:void(0)',array('class'=>'cart button', 'id'=>$product['id'], 'action'=>'add')); ?>
<!--                                    --><?php //else: ?>
<!--                                        --><?php //echo CHtml::link('Удалить из корзины','javascript:void(0)',array('class'=>'cart button','id'=>$product['id'], 'action'=>'remove')); ?>
<!--                                    --><?php //endif; ?>
                                </div>
                            </div>
                <div class="clear"></div>
            </div>

            <div class="b-small_tabs" style="margin-top: 20px;">
                <ul class="tabs_button">
                    <li><a href="#attributes" id="tab_attributes" onclick="tab.openTab('attributes');">Характеристики</a>
                    </li>
                </ul>
                <div id="box_attributes">
                    <?php
                    $cat_attrs = $category->AttrsForProduct;

                    foreach ($cat_attrs as $attr) {
                        echo '<table class="attributes_table">';
                        echo '<td>' . $attr['title'] . '</td>';
                        echo '<td>';

                        if (!empty($product['AttrValues'])) {
                            $values = '';
                            foreach ($product['AttrValues'] as $value) {
                                if ($value->attr_id == $attr->id) {
                                    if (!empty($value['Values']['value_string'])) {
                                        $values .= $value['Values']['value_string'] . ', ';
                                    } else {
                                        $values .= $value['Values']['value_boolean'] == 1 ? 'Да' : 'Нет';
                                        $values .= ', ';
                                    }
                                }
                            }
                            $values = rtrim($values, ', ');

                            if (empty($values)) {
                                echo '--';
                            } else {
                                echo $values;
                            }
                        }

                        echo '</td>';
                        echo '</table>';
                    }
                    ?>
                </div>
            </div>
        </td>
        <td class="vendor_info">
            <?php $this->widget('application.widgets.VendorPanel', array('vendor_id' => $product['vendor_id'])); ?>
        </td>
    </table>
    <script>
        $(document).ready(function () {
            tab.init({
                'default':'attributes'
            });
        });
    </script>
    <script src="/static/js/m_tabs.js"></script>
    <script src="/static/js/m_vendors.js"></script>

<link rel="stylesheet" type="text/css" href="/static/fancybox/helpers/jquery.fancybox-buttons.css"/>
    <script type="text/javascript" src="/static/fancybox/helpers/jquery.fancybox-buttons.js"></script>

<?php
} else {
    echo 'Такого товара не существует...';
}
