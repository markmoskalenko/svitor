<?php
if (!empty($products)) {
    foreach ($products as $n => $product) {
        $view_link = Yii::app()->createUrl('catalog/products', array('cat' => $category->name, 'view' => $product['id']));

        $special_class = '';
        if ($n == 0)
            $special_class = ' item_first';
        ?>
        <div class="item<?php echo $special_class; ?>">
            <table>
                <td class="icon"><a href="<?php echo $view_link; ?>"><img src="<?php echo Yii::app()->ImgManager->getUrlById($product['FirstImage']['img_id'], 'medium', $product['FirstImage']['img_filename']); ?>"></a>
                </td>
                <td class="general">
                    <div class="title"><a href="<?php echo $view_link; ?>"><?php echo $product['title']; ?></a></div>
                    <div class="desc">
                        <?php echo mApi::cutStr($product['description'], 140); ?>
                    </div>
                    <div class="info">
                        <ul>
                            <li>Продавец: <a
                                  href="<?php echo Yii::app()->createUrl('vendor/profile', array('id' => $product['Vendor']['id'])); ?>"><?php echo mb_substr($product['Vendor']['title'], 0, 10, Yii::app()->charset); ?>
                                    ...</a></li>
                            <li>Цена: <b><?php echo $product['price']; ?> руб.</b></li>
                        </ul>
                        <div class="cart button_main">
<!--                            --><?php //if( User::isGuest() ): ?>
<!--                                --><?php //echo CHtml::link('В корзину','/user/registration', array('class'=>'button')); ?>
<!--                            --><?php //else: ?>
<!--                                --><?php //if( !Yii::app()->shoppingCart->contains( 'Product'.$product['id'] ) ): ?>
<!--                                    --><?php //echo CHtml::link('В корзину','javascript:void(0)',array('class'=>'cart button', 'id'=>$product['id'], 'action'=>'add')); ?>
<!--                                --><?php //else: ?>
<!--                                    --><?php //echo CHtml::link('Удалить из корзины','javascript:void(0)',array('class'=>'cart button','id'=>$product['id'], 'action'=>'remove')); ?>
<!--                                --><?php //endif; ?>
<!--                            --><?php //endif; ?>
                        </div>
                    </div>
                </td>
            </table>
        </div>
    <?php
    }
    ?>
    <div class="r_fl">
        <?php
        $this->widget('CLinkPager', array(
                                         'pages' => $pages,
                                         'htmlOptions' => array('class' => 'b-default_pager'),
                                         'firstPageLabel' => '',
                                         'lastPageLabel' => '',
                                         'prevPageLabel' => '&larr; Назад',
                                         'nextPageLabel' => 'Далее &rarr;',
                                         'header' => '',
                                         'cssFile' => false
                                    ));
        ?>
    </div>
<?php
} else {
    echo 'Не найдено ни одного товара... :(';
}
?>