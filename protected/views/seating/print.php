<?php Yii::app()->clientScript->registerCssFile('/static/seating_print.css'); ?>
<?php foreach ($tables as $table): ?>

<div class="seating_table_title"><?php echo $table->title; ?> <span class="places_counter">(свободно <?php echo $table->places - count($table['Guests']); ?> из <?php echo $table->places; ?> мест)</span></div>
<div class="seating_table_guests">
    <?php if (!empty($table['Guests'])): ?>

        <table>
            <thead>
                <th>Имя</th>
                <th>Категория</th>
                <th>Номер места</th>
            </thead>
            <?php foreach ($table['Guests'] as $guest): ?>
            <tr>
                <td width="30%"><?php echo $guest->fullname; ?></td>
                <td width="30%"><?php echo $guest['GuestCategory']->title; ?></td>
                <td width="30%"><?php echo $guest->place; ?></td>
            </tr>
            <?php endforeach; ?>
        </table>

    <?php else: ?>
        За этим столом никто не сидит...
    <?php endif; ?>
</div>

<?php endforeach; ?>