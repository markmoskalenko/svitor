<div id="tabs" class="b-small_tabs">
    <ul class="tabs_button">
        <li>
            <a href="<?php echo Yii::app()->createUrl('site/design'); ?>"<?php if ($action == 'design') echo 'class=" active"'?>>Дизайн</a>
        </li>
        <li>
            <a href="<?php echo Yii::app()->createUrl('site/mainPage'); ?>"<?php if ($action == 'mainPage') echo 'class=" active"'?>>Главная
                страница</a>
        </li>
        <li>
            <a href="<?php echo Yii::app()->createUrl('site/gifts'); ?>"<?php if ($action == 'gifts') echo 'class=" active"'?>>Подарено</a>
        </li>
        <li>
            <a href="<?php echo Yii::app()->createUrl('site/settings'); ?>"<?php if ($action == 'settings') echo 'class=" active"'?>>Настройки
                сайта</a>
        </li>
    </ul>
</div>