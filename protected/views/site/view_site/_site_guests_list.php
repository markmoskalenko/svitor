<h1>Список гостей</h1>
<?php if (!empty($data)): ?>

<?php foreach ($data as $guest): ?>
    <table class="table_data guests">
        <td width="40px" class="guest_box">
            <?php if (!empty($guest['img_id'])): ?>
            <a class="upbox-button" rel="upbox-button"
               href="<?php echo Yii::app()->ImgManager->getUrlById($guest->img_id, 'large', $guest->img_filename); ?>"
               title="<?php echo $guest['fullname']; ?>"><img
                    src="<?php echo Yii::app()->ImgManager->getUrlById($guest->img_id, 'small', $guest->img_filename); ?>"></a>
            <?php else: ?>
            <img src="<?php echo Yii::app()->ImgManager->getUrlById($guest->img_id, 'small', $guest->img_filename); ?>">
            <?php endif; ?>
        </td>
        <td class="guest_box">
            <b><?php echo $guest['fullname']; ?></b>

            <?php if (!empty($guest['GuestCategory'])): ?>
            <br><span style="font-size: 11px; color: #555;"><?php echo $guest['GuestCategory']['title']; ?></span>
            <?php endif; ?>
        </td>
    </table>
    <?php endforeach; ?>

<?php else: ?>
Нет ни одного гостя...
<?php endif; ?>