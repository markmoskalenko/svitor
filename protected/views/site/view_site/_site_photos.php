<h1>Фотоальбом</h1>
<?php if (!empty($data)): ?>
    <div class="photos">
    <?php
        foreach ($data as $img)
        {
            echo '<div class="img">
            <a class="upbox-button" rel="upbox-images" href="'.Yii::app()->ImgManager->getUrlById($img->img_id, 'large', $img->img_filename).'" title="' . $img->description . '">
            <img src="'.Yii::app()->ImgManager->getUrlById($img->img_id, 'medium', $img->img_filename).'"></a>
            </div>';
        }
    ?>
    </div>
<?php else: ?>
Нет ни одной фотографии...
<?php endif; ?>