<h1>Подарено</h1>
<?php if (!empty($data)): ?>
<div class="gifts">
    <?php foreach ($data as $gift): ?>
    <div class="gift_box">
        <?php if (!empty($gift['img_id'])): ?>
        <a class="upbox-button" rel="upbox-button"
           href="<?php echo Yii::app()->ImgManager->getUrlById($gift['img_id'], 'large', $gift['img_filename']); ?>"
           title="<?php echo $gift['title']; ?>"><img
                src="<?php echo Yii::app()->ImgManager->getUrlById($gift['img_id'], 'medium', $gift['img_filename']); ?>"></a>
        <?php else: ?>
        <img src="<?php echo Yii::app()->ImgManager->getUrlById($gift['img_id'], 'medium', $gift['img_filename']); ?>">
        <?php endif; ?>

        <br><span title="<?php echo $gift['title']; ?>"><?php echo mApi::cutStr($gift['title'], 20); ?></span>
    </div>
    <?php endforeach; ?>
</div>
<?php else: ?>
Нет ни одного подарка...
<?php endif; ?>