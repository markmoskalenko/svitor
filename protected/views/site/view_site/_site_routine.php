<h1>Распорядок дня</h1>
<?php if (!empty($data)): ?>

<?php foreach ($data as $var): ?>
    <table class="table_data">
        <tr>
            <td width="50px"><b><?php echo $var['time']; ?></b></td>
            <td>
                <?php echo $var['event']; ?>

                <?php if (!empty($var['address'])): ?>
                <br><b>Адрес</b><br><a class="upbox-various fancybox.iframe address-link"
                                       href="http://maps.google.com/?output=embed&q=<?php echo urlencode($var['address']); ?>"><?php echo $var['address']; ?></a>
                <?php endif; ?>

                <?php if (!empty($var['vendor_id'])): ?>
                <?php $url = Yii::app()->createUrl('vendor/profile', array('id' => $var['Vendor']['id'])); ?>
                <br><b>Компания-участник</b>
                <table class="table_data">
                    <td width="35px">
                        <a href="<?php echo $url; ?>"><img height="30px"
                                                           src="<?php echo Yii::app()->ImgManager->getUrlById($var['Vendor']['img_id'], 'small', $var['Vendor']['img_filename']); ?>"></a>
                    </td>
                    <td>
                        <a href="<?php echo $url; ?>"><?php echo $var['Vendor']['title']; ?></a>
                    </td>
                </table>
                <?php endif; ?>
            </td>
        </tr>
    </table><br>
    <?php endforeach; ?>

<?php else: ?>
Нет событий...
<?php endif; ?>