<h1>Моя команда</h1>
<?php if (!empty($data)): ?>

<?php
    $i = 0; // Для подсчета компаний в комманде (т.к. массив в основном состоит из категорий)
    foreach ($data as $cat):
        if (empty($cat['Team']['Vendor']))
            continue;

        $i++;
        $url = Yii::app()->createUrl('vendor/profile', array('id' => $cat['Team']['Vendor']['id'])); ?>

    <?php echo $cat['title']; ?><br>
    <table class="table_data" style="margin: 5px 0 0 25px;">
        <td width="40px">
            <a href="<?php echo $url; ?>"><img height="30px"
                                               src="<?php echo Yii::app()->ImgManager->getUrlById($cat['Team']['Vendor']['img_id'], 'small', $cat['Team']['Vendor']['img_filename']); ?>"></a>
        </td>
        <td>
            <a href="<?php echo $url; ?>"><?php echo $cat['Team']['Vendor']['title']; ?></a>
        </td>
    </table>
    <br>
    <?php endforeach; ?>

<?php else: ?>
В команде нет компаний...
<?php endif; ?>

<?php if ($i == 0): ?>
В команде нет компаний...
<?php endif; ?>