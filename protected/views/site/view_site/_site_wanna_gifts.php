<h1>Подарки</h1>
<?php if (!empty($data['GuestsMsg'])): ?>
<div class="guestsMsg">
    <?php echo $data['GuestsMsg']['content']; ?>
</div>
<?php endif; ?>

<?php if (!empty($data['WannaGifts'])): ?>
Хотим в подарок...<br>
<ul>
    <?php foreach ($data['WannaGifts'] as $gift): ?>
    <li style="margin-bottom: 15px;">
        <b><?php echo $gift['title']; ?></b>

        <?php if (!empty($gift['description'])): ?>
        <div><?php echo $gift['description']; ?></div>
        <?php endif; ?>

        <?php if (!empty($gift['link'])): ?>
        <div><a href="<?php echo $gift['link']; ?>" target="_blank">ссылка &rarr;</a></div>
        <?php endif; ?>

        <?php if (!empty($gift['guest_id'])): ?>
        <div>Выбран гостем</div>
        <?php endif; ?>
    </li>
    <?php endforeach; ?>
</ul>
<?php else: ?>
Нет ни одного подарка...
<?php endif; ?>