<?php if ($usite->is_available == 0): ?>
<div class="top_available_warning">Сайт находится в отключенном режиме и доступен только вам! <a
        href="/site/settings?<?php echo str_replace('_id', '', $fun_type) . '=' . $usite->$fun_type; ?>">Перейти к
    настройкам сайта &rarr;</a></div>
<?php endif; ?>

<div class="header">
    <div class="site_info">
        <div class="title"><?php echo mApi::cutStr($usite->title, 100); ?></div>
        <?php $days_left = mApi::getDaysLeft($usite->date); ?>
        <div class="date"><?php echo mApi::getDateWithMonth($usite->date, 1, true); echo !empty($days_left) ? ' | ' . mApi::getDaysLeft($usite->date) . ' до начала' : ''; ?></div>
    </div>
</div>
<div class="content_top_hr"></div>
<table class="main_table">
    <td class="menu">
        <?php
        $this->renderPartial('view_site/_site_menu', array(
            'usite' => $usite,
            'act' => $act,
            'preview_template_id' => $preview_template_id,
            'preview_style_id' => $preview_style_id
        ));
        ?>
    </td>
    <td class="content">
        <?php
        $this->renderPartial('view_site/_site_' . $act, array(
            'usite' => $usite,
            'data' => $data
        ));
        ?>
    </td>
</table>