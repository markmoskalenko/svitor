<ul>
    <?php
    $preview = array();
    if (!empty($preview_template_id) && !empty($preview_style_id)) {
        $preview = array(
            'template' => $preview_template_id,
            'style' => $preview_style_id
        );
    }
    ?>
    <li<?php if ($act == 'index') echo ' class="active"'; ?>>
        <a href="<?php echo Yii::app()->createUrl('site/view', array_merge($preview, array('id' => $usite->id, 'act' => 'index'))); ?>">Главная</a>
    </li>
    <?php if ($usite->show_routine == 1): ?>
    <li<?php if ($act == 'routine') echo ' class="active"'; ?>>
        <a href="<?php echo Yii::app()->createUrl('site/view', array_merge($preview, array('id' => $usite->id, 'act' => 'routine'))); ?>">Распорядок
            дня</a>
    </li>
    <?php endif; ?>
    <?php if ($usite->show_guests_list == 1): ?>
    <li<?php if ($act == 'guests_list') echo ' class="active"'; ?>>
        <a href="<?php echo Yii::app()->createUrl('site/view', array_merge($preview, array('id' => $usite->id, 'act' => 'guests_list'))); ?>">Список
            гостей</a>
    </li>
    <?php endif; ?>
    <?php if ($usite->show_guests_seating == 1): ?>
    <li<?php if ($act == 'guests_seating') echo ' class="active"'; ?>>
        <a href="<?php echo Yii::app()->createUrl('site/view', array_merge($preview, array('id' => $usite->id, 'act' => 'guests_seating'))); ?>">Рассадка
            гостей</a>
    </li>
    <?php endif; ?>
    <?php if ($usite->show_vendors == 1): ?>
    <li<?php if ($act == 'vendors') echo ' class="active"'; ?>>
        <a href="<?php echo Yii::app()->createUrl('site/view', array_merge($preview, array('id' => $usite->id, 'act' => 'vendors'))); ?>"><?php echo mApi::getFunType() == 1 ? 'Моя команда' : 'Наша команда'; ?></a>
    </li>
    <?php endif; ?>
    <?php if ($usite->show_wanna_gifts == 1): ?>
    <li<?php if ($act == 'wanna_gifts') echo ' class="active"'; ?>>
        <a href="<?php echo Yii::app()->createUrl('site/view', array_merge($preview, array('id' => $usite->id, 'act' => 'wanna_gifts'))); ?>">Подарки</a>
    </li>
    <?php endif; ?>
    <?php if ($usite->show_gifts == 1): ?>
    <li<?php if ($act == 'gifts') echo ' class="active"'; ?>>
        <a href="<?php echo Yii::app()->createUrl('site/view', array_merge($preview, array('id' => $usite->id, 'act' => 'gifts'))); ?>">Подарено</a>
    </li>
    <?php endif; ?>
    <?php if ($usite->show_photos == 1): ?>
    <li<?php if ($act == 'photos') echo ' class="active"'; ?>>
        <a href="<?php echo Yii::app()->createUrl('site/view', array_merge($preview, array('id' => $usite->id, 'act' => 'photos'))); ?>">Фотоальбом
    </li>
    <?php endif; ?>
</ul>
