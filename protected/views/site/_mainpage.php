<?php echo CHtml::beginForm(); ?>
<?php if (!empty($success)) echo '<div id="msg"><div class="msg_success">' . $success . '</div><br></div>'; ?>

<?php echo CHtml::activeTextArea($usite, 'content'); ?>
<?php echo CHtml::error($usite, 'content'); ?>

<br>
<div class="button_main">
    <button type="submit">Сохранить</button>
</div>
<?php echo CHtml::endForm(); ?>

<script src="/static/js/m_usite.js"></script>
<script type="text/javascript" src="/static/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="/static/ckeditor/adapters/jquery.js"></script>
<script>
    $(document).ready(function () {
        $('#UserSite_content').ckeditor({width:'755px'});
    <?php if (!empty($success)): ?>
        usite.hideMessage();
        <?php endif; ?>
    });
</script>