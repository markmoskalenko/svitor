<div class="content" id="giftForm">
    <div id="msg"></div>
    <form onsubmit="return false;">

        <div class="inline_name" style="padding: 5px 0;">Название подарка <span class="red_text">*</span></div>
        <div class="field">
            <?php
            $model->title = CHtml::decode($model->title);
            echo CHtml::activeTextField($model, 'title', array(
                'style' => 'width: 98%;',
                'required' => 'required'
            ));
            ?>
        </div>

        <div class="field">
            <div id="image_thumbs">
                <img src="<?php echo Yii::app()->ImgManager->getUrlById($model->img_id, 'medium', $model->img_filename); ?>"
                     width="100px" height="100px">
                <img src="<?php echo Yii::app()->ImgManager->getUrlById($model->img_id, 'small', $model->img_filename); ?>">
            </div>
            <a id="upload_image">выбрать фотографию &rarr;</a>
            <input type="hidden" name="UserGifts[img_id]" id="img_id" value="<?php echo $model->img_id; ?>"
                   width="100px" height="100px">
            <input type="hidden" name="UserGifts[img_filename]" id="img_filename"
                   value="<?php echo $model->img_filename; ?>">
        </div>

    </form>
</div>
<div class="buttons">
    <div class="button_main">
        <button type="button"
                onclick="usite.sendGiftForm(this, '<?php echo $model->isNewRecord ? 'addGift' : 'editGift/id/' . $model->id; ?>');">
            <?php echo $model->isNewRecord ? 'Добавить' : 'Сохранить'; ?>
        </button>
    </div>
    <div class="button_grey">
        <button onclick="$.fancybox.close();">Отменить</button>
    </div>
</div>

<script src="/static/js/ajaxupload.js"></script>
<script>
    $(document).ready(function () {
        usite.uploadGiftImg($('#upload_image'));
    });
</script>
