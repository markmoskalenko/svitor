<?php echo CHtml::beginForm(); ?>
<?php if (!empty($success)) echo '<div id="msg"><div class="msg_success">' . $success . '</div><br></div>'; ?>
<div class="u_site_setting_title">Доступность сайта</div>
<ul class="u_site_settings">
    <li>
        <label>
            <div class="u_site_setting_on">
                <input type="radio" name="UserSite[is_available]"
                       value="1"<?php if ($usite->is_available == 1) echo ' checked="checked"'; ?>> Включен
            </div>
        </label>
        <label>
            <div class="u_site_setting_off">
                <input type="radio" name="UserSite[is_available]"
                       value="0"<?php if ($usite->is_available == 0) echo ' checked="checked"'; ?>> Отключен
            </div>
        </label>
    </li>
</ul>
<div class="u_site_setting_title">Какие разделы доступны на сайте?</div>
<ul class="u_site_settings">
    <li>
        <label><?php echo CHtml::activeCheckbox($usite, 'show_routine'); ?> Распорядок дня</label>
    </li>
    <li>
        <label><?php echo CHtml::activeCheckbox($usite, 'show_guests_list'); ?> Список гостей</label>
    </li>
    <li>
        <label><?php echo CHtml::activeCheckbox($usite, 'show_guests_seating'); ?> Рассадка гостей</label>
    </li>
    <li>
        <label><?php echo CHtml::activeCheckbox($usite, 'show_vendors'); ?> Моя команда</label>
    </li>
    <li>
        <label><?php echo CHtml::activeCheckbox($usite, 'show_wanna_gifts'); ?> Подарки</label>
    </li>
    <li>
        <label><?php echo CHtml::activeCheckbox($usite, 'show_gifts'); ?> Подарено</label>
    </li>
    <li>
        <label><?php echo CHtml::activeCheckbox($usite, 'show_photos'); ?> Фотоальбом</label>
    </li>
</ul>
<div class="button_main">
    <button type="submit">Сохранить</button>
</div>

<?php echo CHtml::endForm(); ?>

<script src="/static/js/m_usite.js"></script>
<?php if (!empty($success)): ?>
<script>
    $(document).ready(function () {
        usite.hideMessage();
    });
</script>
<?php endif; ?>