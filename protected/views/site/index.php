<div class="u_site_title">
    Сайт <?php echo mApi::getFunType() == 0 ? 'молодоженов' : 'торжества'; ?>
    <?php $this->widget('application.widgets.FunBar', array('short_mode' => true)); ?>
</div>

<?php
if (mApi::getFunId()):
    $url = '/checklist/index/' . Yii::app()->request->baseUrl;
    ?>
<div class="u_site_link">
    <a href="<?php echo Yii::app()->createUrl('site/view', array('id' => $usite->id)); ?>" target="_blank">Перейти на
        сайт &rarr;</a>
</div>
<br>

<?php $this->renderPartial('_tabs', array('action' => $action)); ?>
<div class="u_site_box">
    <?php $this->renderPartial('_' . strtolower($action), array_merge($data, array('usite' => $usite))); ?>
</div>
<?php else:
    $this->widget('application.widgets.SelectFun');
endif;
?>