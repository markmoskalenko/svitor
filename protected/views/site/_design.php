<?php if (!empty($templates)): ?>

<div id="msg" style="display: none;"></div>

<?php foreach ($templates as $tmp): ?>

    <?php
        echo '<div class="u_site_design">';
        echo '<div class="u_site_design_preview" id="tmp_' . $tmp->id . '">';

        $preview_url = Yii::app()->createUrl('site/view', array('id' => $usite->id)) . '?template=' . $tmp->id . '&style=' . $tmp->default_style_id;

        echo '<div class="u_site_design_preview_actionbar">';
        echo '<a href="' . $preview_url . '" class="preview_link" target="_blank">посмотреть</a>  | ';
        if ($usite->template_id == $tmp->id && $usite->template_style_id == $tmp->default_style_id) {
            echo '<b class="selected">выбрано</b><a class="select" style="display: none;" onclick="usite.selectTemplate(this, ' . $tmp->id . ', ' . $tmp->default_style_id . '); return false;">выбрать</a>';
        } else {
            echo '<b style="display: none;" class="selected">выбрано</b><a class="select" onclick="usite.selectTemplate(this, ' . $tmp->id . ', ' . $tmp->default_style_id . '); return false;">выбрать</a>';
        }
        echo '</div>';

        echo '<img src="/static/usite/' . $tmp['layout_name'] . '/' . $tmp['default_style_name'] . '/preview.jpg">';
        echo '</div>';

        echo '<div class="u_site_design_styles">';
        foreach ($tmp['Styles'] as $style) {
            echo '<img src="/static/usite/' . $tmp->layout_name . '/' . $style->name . '/icon.gif" id="' . $tmp->id . '_' . $style->id . '" title="' . $style->title . '" onclick="usite.showPreview(this, {id:' . $tmp->id . ', name:\'' . $tmp->layout_name . '\'}, {id:' . $style->id . ', name:\'' . $style->name . '\'});">';
        }
        echo '</div>';
        echo '</div>';
        ?>
    <?php endforeach; ?>

<?php else: ?>
Нет шаблонов для выбора...
<?php endif; ?>

<script src="/static/js/m_usite.js"></script>
<script>
    $(document).ready(function () {
        usite.init({
            site_id: <?php echo $usite->id; ?>,
            template_id: <?php echo $usite->template_id; ?>,
            style_id: <?php echo $usite->template_style_id; ?>
        });
    });
</script>