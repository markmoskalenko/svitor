<div class="simple_add_button">
    <a onclick="usite.addGift(); return false;">Добавить</a>
</div>

<table class="simple_table u_site_gifts_table">

    <?php if (!empty($gifts_data)): ?>

    <tr class="thead">
        <th class="c_al image"></th>
        <th class="l_al title">Название подарка</th>
        <th></th>
    </tr>

    <?php foreach ($gifts_data as $var): ?>
        <tr class="item" id="item_<?php echo $var['id']; ?>">
            <td class="c_al image"><img height="30px" src="<?php echo Yii::app()->ImgManager->getUrlById($var['img_id'], 'small', $var['img_filename']); ?>">
            </td>

            <td class="l_al title">
                <div class="title_text"><?php echo $var['title']; ?></div>
            </td>

            <td class="l_al">
                <div class="editor_links">
                    <a class="edit magic_button" onclick="usite.editGift(<?php echo $var['id']; ?>); return false;"
                       onmouseover="tooltip.show(this, 'Редактировать', {position :'top-right'});"></a>
                    <a class="delete magic_button" onclick="usite.deleteGift(<?php echo $var['id']; ?>); return false;"
                       onmouseover="tooltip.show(this, 'Удалить', {position :'top-right'});"></a>
                </div>
            </td>
        </tr>
        <?php endforeach; ?>
    <tr class="js_empty_item empty_item">
        <td colspan="3">Вы пока не добавили ни одного подарка...
        <td>
    </tr>

    <div id="trash-dialog" style="display: none;" title="Удалить подарок">Подарок будет удален без возможности
        восстановления.<br><br>Вы действительно хотите продолжить?
    </div>

    <?php else: ?>
    <tr class="empty_item">
        <td colspan="3">Вы пока не добавили ни одного подарка...
        <td>
    </tr>
    <?php endif; ?>
</table>
<script src="/static/js/m_usite.js"></script>