<div class="content">
    <div class="row">
        <div class="left"><h2>Добавление объявления</h2></div>
        <div class="right">
            <?php echo CHtml::link('Как составить объявление, чтобы подобрать исполнителя и не разочароваться?', $this->createUrl('/pages/index', array('name'=>'bid'))) ?></div>
        <div class="clear"></div>
    </div>

    <div class="row">
        <?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
    </div>
</div>