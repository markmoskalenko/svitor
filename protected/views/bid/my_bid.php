<div class="content">
    <div class="row">
        <h2>Мои заявки</h2>
    </div>

    <div class="row myBid">
        <?php $this->widget('zii.widgets.CListView', array(
                                                          'dataProvider'=>$dataProvider,
                                                          'itemView'=>'_view_my_bid',
                                                     )); ?>
    </div>
</div>
<div id="dialog" style="display: none;" title="Закрыть заявку?">
    После закрытия заявки она будет скрыта и недоступна у исполнителей, а исполнители больше не смогут отправлять вам ответы. <br> Переоткрыть заявку невозможно.</div>

<script type="text/javascript">
    function closeBid(id){
        $('#dialog').dialog({
            'modal' : true,
            'resizable' : false,
            'width' : 450,
            'buttons' : [
                {
                    text : 'Да',
                    click : function () {
                        document.location.assign('/bid/close/id/'+id);
                        return true;
                    }
                },
                {
                    text : 'Нет',
                    click : function () {
                        $(this).dialog('close');
                        return false;
                    }
                }
            ]
        });
    }
    $(function(){
        $('.show-description').click(function(){
            var id = $(this).attr('desc_id');
            $('.description-min').show();
            $('.description_full').hide();
            $(this).parent().hide();
            $("#description_"+id).show();
        });

    });
</script>