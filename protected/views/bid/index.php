<div class="content">
    <div class="row">
        <h2>Заявки и объявления</h2>
    </div>
    <div class="row title">
        <div class="left">
            <div class="search-form">
                <?php echo CHtml::form($this->createUrl('/bid/index'), 'GET'); ?>
                    <?php echo CHtml::textField('filter',$filter,array('placeholder'=>'Поиск по компаниям')) ?>
                    <div class="button_grey">
                        <button type="submit">Найти</button>
                    </div>
                <?php echo CHtml::endForm() ?>
            </div>
        </div>
        <div class="button_main"><?php echo CHtml::link('Создать', array('/bid/create'), array('class'=>'button')); ?></div>
    </div>

    <div class="row">
        <?php $this->widget('zii.widgets.CListView', array(
                                                          'dataProvider'=>$dataProvider,
                                                          'itemView'=>'_view',
                                                     )); ?>
    </div>
</div>
<div id="redirect-dialog" style="display: none;" title="Редирект">На сообщение можно ответить только из кабинета компании.<br><br>Перейти в кабинет компании?</div>
<script type="text/javascript">

function redirect_vendor($bid,$uid){
    $('#redirect-dialog').dialog({
        'modal' : true,
        'resizable' : false,
        'width' : 450,
        'buttons' : [
            {
                text : 'Да',
                click : function () {
                    document.location.assign('/vendor_cp/bid/index?uid='+$uid+'&bid='+$bid+'#message');
                }
            },
            {
                text : 'Нет',
                click : function () {
                    $(this).dialog('close');
                }
            }
        ]
    });
}

$(function(){
    $('.show-description').click(function(){
        var id = $(this).attr('desc_id');
        $('.description-min').show();
        $('.description_full').hide();
        $(this).parent().hide();
        $("#description_"+id).show();
    });


});
</script>
