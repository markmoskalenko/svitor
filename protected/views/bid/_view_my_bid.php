<div class="view post <?php echo $data->opened  ? '' : ' close'; ?>">
    <div class="post_wrap">
		<div class="right">
            <div class="row">
                <b>Количество ответов:</b>
                <?php echo CHtml::link(count($data->messages_all),$this->createUrl('/messages/index/', array('folder'=>'bid','bid_id'=>$data->id))) ?>
            </div>
            <?php if( $data->opened ): ?>
                <div class="row">
                    <div class="button_main"><?php echo CHtml::link('Закрыть заявку', '#', array('class'=>'button', 'onClick'=>'closeBid('.$data->id.'); return false;')); ?></div>
                </div>
            <?php else: ?>
                Заявка закрыта
            <?php endif; ?>

        </div>
		<div class="line_block top">
			<a href="#">Ищу исполнителя.</a>
			<span><?php echo $data->getAttributeLabel('cat_id'); ?>:</span>
			<?php echo CHtml::link($data->cat->title, Yii::app()->createUrl('/catalog/vendors', array('cat' => $data->cat->name))); ?>
			<span><?php echo Date('d-m-Y', strtotime($data->date_created)); ?> - <?php echo Date('d-m-Y', strtotime($data->date_end)); ?></span>
		</div>
		<div class="line_block">
            <div class="description-min">
                <?php echo CHtml::encode(mb_substr($data->description, 0, 45, 'UTF-8')); ?>... <?php echo CHtml::link('Подробнее','javascript:void(0)',array('class'=>'show-description','desc_id'=> $data->id)) ?>
            </div>
            <div class="description_full" style="display: none;" id="description_<?php echo $data->id; ?>">
                <?php echo CHtml::encode(CHtml::encode($data->description)); ?>
            </div>
		</div>
    </div>    
</div>