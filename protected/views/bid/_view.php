<div class="view post">
    <div class="post_wrap">
        <?php if(!empty($data->budget)): ?>
        <div class="price_box"><?php echo CHtml::encode($data->budget); ?> <?php echo is_numeric($data->budget) ? 'руб.' : ''; ?></div>
        <?php endif; ?>
        <div class="line_block top">
            <a href="#">Ищу исполнителя.</a>
            <span><?php echo $data->getAttributeLabel('cat_id'); ?>:</span>
            <?php echo CHtml::link($data->cat->title, Yii::app()->createUrl('/catalog/vendors', array('cat' => $data->cat->name))); ?>
        </div>
        <div class="line_block">
			<span><strong><?php echo $data->getAttributeLabel('user_id'); ?>:</strong>
                <?php echo CHtml::link(CHtml::encode($data->user->login), $this->createUrl( '/user/profile', array('id'=>$data->user_id))); ?></span>

            <?php if(isset($data->region->name) && !empty($data->region->name)): ?>
			<span><strong><?php echo $data->getAttributeLabel('region_id'); ?>:</strong>
                <?php echo CHtml::encode($data->region->name); ?></span>
            <?php endif; ?>

            <?php if(isset($data->cities->name) && !empty($data->cities->name)): ?>
			<span><strong><?php echo $data->getAttributeLabel('cities_id'); ?>:</strong>
                <?php echo CHtml::encode($data->cities->name); ?></span>
            <?php endif; ?>
			<span><strong><?php echo $data->getAttributeLabel('date_created'); ?>:</strong>
                <?php echo Date('d-m-Y', strtotime($data->date_created)); ?></span>
        </div>
        <div class="line_block">
			<div class="description-min">
                <?php if(iconv_strlen($data->description)>50): ?>
				<?php echo CHtml::encode(mb_substr($data->description, 0, 45, 'UTF-8')); ?>... <?php echo CHtml::link('Подробнее','javascript:void(0)',array('class'=>'show-description','desc_id'=> $data->id)) ?>
			    <?php else: ?>
			    <?php echo CHtml::encode($data->description); ?>
                <?php endif; ?>
            </div>
			<div class="description_full" style="display: none;" id="description_<?php echo $data->id; ?>">
				<?php echo CHtml::encode($data->description); ?>
			</div>
        </div>


        <div class="line_block">
			<span><strong><?php echo CHtml::encode($data->getAttributeLabel('type_holiday')); ?>:</strong>
                <?php echo CHtml::encode($data->type_holiday); ?></span>
        </div>
    </div>

    <?php if(!isset($_SESSION["vendor__id"])): ?>
        <div class="button_main"><?php echo CHtml::link('Ответить', $this->createUrl('/vendor_cp/user/registration'),array('class'=>'button')); ?></div>
    <?php elseif(!$data->messages): ?>
        <div class="button_main"><?php echo CHtml::link('Ответить', 'javascript:void(0)',array('onClick'=>"redirect_vendor({$data->id}, {$data->user_id})",'class'=>'button')); ?></div>
    <?php else: ?>
        <span style="color: red; font-weight: bold;">Заявка принята</span>
    <?php endif; ?>
</div>