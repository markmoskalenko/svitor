<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'bid-form',
	'enableAjaxValidation'=>false,
)); ?>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
        <?php echo $form->labelEx($model,'cat_id'); ?>
        <select name="Bid[cat_id]" style="width: 300px;">
            <?php $this->widget('application.modules.vendor_cp.widgets.AllCats', array('model' => $model,'type'=>'vendors')); ?>
        </select>
        <?php echo $form->error($model,'cat_id'); ?>

    </div>

	<div class="row">
		<?php echo $form->labelEx($model,'country_id'); ?>
		<?php echo $form->dropDownList($model,'country_id', Countries::getList(),
            array(
                 'empty'=>'- Выберите страну -',
                 //'options' => array(Yii::app()->request->getQuery('id')=>array('selected'=>true)),
                 'ajax' => array(
                     'type'=>'POST',
                     'url'=>$this->createUrl('/bid/getRegions'),
                     'update'=>'#Bid_region_id', //selector to update
                 ),
                 'style'=>'width: 300px;'
                 )

        ); ?>
		<?php echo $form->error($model,'country_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'region_id'); ?>
        <?php echo $form->dropDownList($model,'region_id', ($model->country_id > 0 ? Regions::getListDDByCountry($model->country_id) : array()),
            array(
                 'empty'=>'- Выберите регион -',
//                 'options' => array($model->region_id=>array('selected'=>true)),
                 'ajax' => array(
                     'type'=>'POST',
                     'url'=>$this->createUrl('/bid/getCities'),
                     'update'=>'#Bid_cities_id', //selector to update
                 ),
                'style'=>'width: 300px;'
        )

        ); ?>
		<?php echo $form->error($model,'region_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'cities_id'); ?>
		<?php echo $form->dropDownList($model, 'cities_id', ($model->region_id > 0 ? Cities::getListDDByRegion($model->region_id) : array()), array(
                                                                          'empty'=>'- Выберите город -',
                                                                          'style'=>'width: 300px;')); ?>
		<?php echo $form->error($model,'cities_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'type_holiday'); ?>
		<?php echo $form->textField($model,'type_holiday',array('size'=>20,'maxlength'=>20, 'style'=>'width: 290px;')); ?>
		<?php echo $form->error($model,'type_holiday'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'budget'); ?>
		<?php echo $form->textField($model,'budget',array( 'style'=>'width: 290px;')); ?>
		<?php echo $form->error($model,'budget'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'description'); ?>
        <p class="notice">(напишите, что или кого ищете, для чего; количество человек, которое будет присутствовать на празднике; какие требования предъявляются)</p>
		<?php echo $form->textArea($model,'description',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'description'); ?>
	</div>

	<div class="row button">
            <?php echo CHtml::submitButton('Добавить заявку', array('type'=>'button')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->