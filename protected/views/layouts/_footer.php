<div class="social_links">
    <div>
        <ul>
            <li><a class="vk" href="http://vk.com/public47429439" target="_blank"></a></li>
            <li><a class="facebook" href="http://www.facebook.com/pages/Свадьба-и-Торжество/584067711608598" target="_blank"></a></li>
            <li><a class="twitter" href="https://twitter.com/svitor_ru" target="_blank"></a></li>
            <li><a class="odnoklassniki" href="http://www.odnoklassniki.ru/group/51773039837400" target="_blank"></a></li>
        </ul>
    </div>
</div>
<div class="links">
    <ul>
        <?php if(isset($this->module) && $this->module->getName() == 'vendor_cp'):?>
        <li><a href="<?php echo Yii::app()->createUrl('pages/index', array('name' => 'help_company')); ?>">Помощь</a></li>
        <?php elseif(!Yii::app()->user->isGuest): ?>
        <li><a href="<?php echo Yii::app()->createUrl('pages/index', array('name' => 'help_user')); ?>">Помощь</a></li>
        <?php else: ?>
        <li><a href="<?php echo Yii::app()->createUrl('pages/index', array('name' => 'help')); ?>">Помощь</a></li>
        <?php endif;?>
        <li><a href="<?php echo Yii::app()->createUrl('pages/index', array('name' => 'feedback')); ?>">Контакты</a></li>
        <li><a href="<?php echo Yii::app()->createUrl('pages/index', array('name' => 'terms-of-use')); ?>">Пользовательское соглашение</a></li>
        <li><a href="<?php echo Yii::app()->createUrl('pages/index', array('name' => 'privacy-policy')); ?>">Политика конфиденциальности</a></li>
    </ul>
</div>
<div class="b-marinin" align="right">
    <!-- Yandex.Metrika counter -->
    <script type="text/javascript">
        (function (d, w, c) {
            (w[c] = w[c] || []).push(function() {
                try {
                    w.yaCounter20930014 = new Ya.Metrika({id:20930014,
                        webvisor:true,
                        clickmap:true,
                        trackLinks:true,
                        accurateTrackBounce:true});
                } catch(e) { }
            });
            var n = d.getElementsByTagName("script")[0],
                    s = d.createElement("script"),
                    f = function () { n.parentNode.insertBefore(s, n); };
            s.type = "text/javascript";
            s.async = true;
            s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";
            if (w.opera == "[object Opera]") {
                d.addEventListener("DOMContentLoaded", f, false);
            } else { f(); }
        })(document, window, "yandex_metrika_callbacks");
    </script>
    <noscript>
        <div rel="nofollow">
            <img src="//mc.yandex.ru/watch/20930014" style="position:absolute; left:-9999px;" alt="" />
        </div>
    </noscript>
    <!-- /Yandex.Metrika counter -->
    <noindex>
        <div rel="nofollow">
            <div class="stats r_fl">

                <!--LiveInternet counter--><script type="text/javascript"><!--
            document.write("<a rel='nofollow' href='http://www.liveinternet.ru/click' "+
                    "target=_blank><img src='//counter.yadro.ru/hit?t45.1;r"+
                    escape(document.referrer)+((typeof(screen)=="undefined")?"":
                    ";s"+screen.width+"*"+screen.height+"*"+(screen.colorDepth?
                            screen.colorDepth:screen.pixelDepth))+";u"+escape(document.URL)+
                    ";h"+escape(document.title.substring(0,80))+";"+Math.random()+
                    "' alt='' title='LiveInternet' "+
                    "border='0' width='31' height='31'><\/a>")
            //--></script><!--/LiveInternet-->
            </div>
        </div>
    <div <?php if(Yii::app()->controller->id == 'main'):?>style="margin-top: -12px;"<?php endif;?> >

        &copy; <?php echo date('Y', time()); ?> &laquo;Свадьба и Торжество&raquo;<br>
    </div>
    </noindex>
    <?php if(Yii::app()->controller->id == 'main'):?>
    <noindex>
        <div rel="nofollow">
            <a style="color: #999999;" href="http://svitor.ru/page/unisender">unisender.com</a>
        </div>
    </noindex>
    <?php endif?>
        <a href="http://creamov.com" target="_blank" title="Web-разработчик">Creamov.</a>
</div>
<script type="text/javascript">
    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-37206068-1']);
    _gaq.push(['_setDomainName', 'svitor.ru']);
    _gaq.push(['_setAllowLinker', true]);
    _gaq.push(['_trackPageview']);

    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
</script>