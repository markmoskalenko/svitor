<!DOCTYPE html>
<html>
<?php require_once('_header.php'); ?>
<body>
<div class="layout">
    <div class="layout_header" style="position: relative;">
        <div class="logo">
            <a href="/" title="Свадьба и Торжество" class="magic_button"></a>
        </div>
        <?php
        if(Yii::app()->user->name == 'testsvitor'){
            echo CHtml::link('Сохранить', Yii::app()->createUrl('/demo/save'), array('style'=>'position: absolute; top: 35px; right: 0px; padding: 4px;','class'=>'help_button'));
        }
        ?>

        <div class="topnav_wrap">
            <?php $this->widget('application.widgets.MainMenu'); ?>
        </div>
        <div class="topbar_wrap">
            <div class="userbar_wrap">
                <?php $this->widget('application.widgets.UserPanel'); ?>
            </div>
            <ul class="nav">
                <li<?php if (mApi::getFunType() == 1) echo ' class="active"'; ?>><a href="/dashboard/?holiday">Торжество</a>
                </li>
                <li<?php if (mApi::getFunType() == 0) echo ' class="active"'; ?>><a href="/dashboard/?wedding">Свадьба</a>
                </li>
            </ul>
        </div>
    </div>

    <?php
    $controller = Yii::app()->controller->getId();
    $action = Yii::app()->controller->action->getId();
    ?>
    <div class="layout_container">
        <div class="wrapper">
            <div class="left_sidebar">
                <ul class="nav">
                    <?php if (mApi::getFunType() == 0): ?>
                    <li <?php if ($controller == 'wedding' && $action == 'index') echo 'class="active"'; ?>>
                        <a href="/wedding" class="with_icon nav_icon_profile">Мои свадьбы</a>
                    </li>
                    <?php elseif (mApi::getFunType() == 1): ?>
                    <li <?php if ($controller == 'holiday' && $action == 'index') echo 'class="active"'; ?>>
                        <a href="/holiday" class="with_icon nav_icon_profile">Мои торжества</a>
                    </li>
                    <?php endif; ?>

                    <li class="funbar">
                        <?php $this->widget('application.widgets.FunBar'); ?>
                    </li>

                    <li <?php if ($controller == 'dashboard') echo 'class="active"'; ?>><a href="/dashboard" class="with_icon nav_icon_dashboard">Пульт управления</a></li>

                    <li class="separator">
                        <div class="sep"></div>
                    </li>
                    <li <?php if ($controller == 'checklist') echo 'class="active"'; ?>>
                        <a href="/checklist" class="with_icon nav_icon_checklist">Список задач</a>
                    </li>
                    <li <?php if ($controller == 'budget') echo 'class="active"'; ?>>
                        <a href="/budget" class="with_icon nav_icon_budget">Бюджет</a>
                    </li>
                    <li <?php if ($controller == 'guests' && $action == 'index') echo 'class="active"'; ?>>
                        <a href="/guests" class="with_icon nav_icon_guests">Список гостей</a>
                    </li>
                    <li <?php if ($controller == 'guests' && $action == 'seating') echo 'class="active"'; ?>>
                        <a href="/guests/seating" class="with_icon nav_icon_seating">Рассадка гостей</a>
                    </li>
                    <li <?php if ($controller == 'vendors') echo 'class="active"'; ?>>
                        <a href="/vendors" class="with_icon nav_icon_vendors"><?php echo mApi::getFunType() == 1 ? 'Мои компании' : 'Наши компании'; ?></a>
                    </li>
                    <li <?php if ($controller == 'wannaGifts') echo 'class="active"'; ?>>
                        <a href="/wannaGifts" class="with_icon nav_icon_gifts">Подарки</a>
                    </li>
                    <li <?php if ($controller == 'routine') echo 'class="active"'; ?>>
                        <a href="/routine" class="with_icon nav_icon_routine">Распорядок дня</a>
                    </li>
                    <li <?php if ($controller == 'user' && $action == 'photos') echo 'class="active"'; ?>>
                        <a href="/photos" class="with_icon nav_icon_photos">Фотоальбом</a>
                    </li>
                    <li <?php if ($controller == 'site') echo 'class="active"'; ?>>
                        <a href="/site" class="with_icon nav_icon_website">Сайт <?php echo mApi::getFunType() == 0 ? 'молодоженов' : 'торжества'; ?></a>
                    </li>
                </ul>
            </div>
            <div class="content_wrap">
                <div class="content">
                    <?php echo $content; ?>
                </div>
            </div>
        </div>
    </div>

    <div class="layout_footer">
        <?php require_once('_footer.php'); ?>
    </div>
</div>
</body>
</html>