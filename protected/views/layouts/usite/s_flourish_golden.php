<!DOCTYPE html>
<html>
<head>
    <title><?php echo Yii::app()->name; ?></title>
</head>
<body>
<div class="main_container">
    <?php echo $content; ?>
    <div class="footer">
        Powered by <a
            href="http://<?php echo Yii::app()->request->getServerName(); ?>"><?php echo Yii::app()->request->getServerName(); ?></a>
        | 2012
    </div>
</div>
</body>
</html>