<!DOCTYPE html>
<html>
<?php require_once('_header.php'); ?>
<body>
<div class="layout">
    <div class="layout_header">
        <div class="logo">
            <a href="/" title="Свадьба и Торжество" class="magic_button" alt="Свадьба и торжество"></a>
        </div>
        <div class="topnav_wrap">
            <?php $this->widget('application.widgets.MainMenu'); ?>
        </div>
        <div class="topbar_wrap">
            <div class="userbar_wrap">
                <?php $this->widget('application.widgets.UserPanel'); ?>
            </div>
            <?php if (Yii::app()->controller->getId() == 'catalog' || Yii::app()->controller->getId() == 'vendor'): ?>
                <ul class="nav">
                    <li<?php if (Yii::app()->controller->action->getId() == 'vendors' || Yii::app()->controller->getId() == 'vendor') echo ' class="active"'; ?>>
                        <a href="/catalog/vendors">Компании</a>
                    </li>
                    <li<?php if (Yii::app()->controller->action->getId() == 'products') echo ' class="active"'; ?>>
                        <a href="/catalog/products">Товары</a>
                    </li>
                </ul>
            <?php endif; ?>
        </div>
    </div>
    <div class="layout_container">
        <div class="wrapper">
            <?php echo $content; ?>
        </div>
    </div>

    <div class="layout_footer">
        <?php require_once('_footer.php'); ?>
    </div>
</div>
</body>
</html>