<head>
    <title><?php if(isset($this->pageTitle) && !empty($this->pageTitle)){ echo $this->pageTitle; } else { echo Yii::app()->name;} ?></title>
    <meta name="description" content="<?php if(isset($this->pageDescription)) echo $this->pageDescription; ?>">
    <meta name="keywords" content="<?php if(isset($this->pageKeywords)) echo $this->pageKeywords; ?>">
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="/static/global.css?<?php echo Yii::app()->params['version']['css']; ?>"/>
    <link rel="stylesheet" type="text/css" href="/static/global2.css?<?php echo Yii::app()->params['version']['css']; ?>"/>
    <link rel="stylesheet" type="text/css" href="/static/m_tooltip.css?<?php echo Yii::app()->params['version']['css']; ?>"/>
    <?php Yii::app()->getClientScript()->registerCoreScript( 'jquery' ); ?>
<!--    <script src="/static/js/jquery-1.8.0.min.js?--><?php //echo Yii::app()->params['version']['js']; ?><!--"></script>-->
    <script src="/static/js/jquery.placeholder.js?<?php echo Yii::app()->params['version']['js']; ?>"></script>
    <script src="/static/js/main.js?<?php echo Yii::app()->params['version']['js']; ?>"></script>
    <script src="/static/js/m_tooltip.js?<?php echo Yii::app()->params['version']['js']; ?>"></script>

    <!--[if IE 8]>
    <link rel="stylesheet" type="text/css" href="/static/ie8.css?<?php echo Yii::app()->params['version']['css']; ?>">
    <![endif]-->

    <link rel="stylesheet" type="text/css" href="/static/fancybox/jquery.fancybox.css?<?php echo Yii::app()->params['version']['css']; ?>">
    <script src="/static/fancybox/jquery.fancybox.pack.js"></script>

    <link rel="stylesheet" type="text/css" href="/static/humanity/jquery-ui-1.8.23.custom.css?<?php echo Yii::app()->params['version']['css']; ?>">
    <script src="/static/js/jquery-ui-1.8.23.custom.min.js"></script>

    <script>
        var svitor = {mainDomain: '<?php echo Yii::app()->request->getServerName(); ?>'};
    </script>
</head>
