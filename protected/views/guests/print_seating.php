<?php Yii::app()->clientScript->registerCssFile('/static/seating_print.css'); ?>

<div class="seating_table_guests">
    <table>
        <thead>
        <th>Имя</th>
        <th>Категория</th>
        <th>Название стола</th>
        <th>Номер места</th>
        </thead>
        <?php foreach ($guests as $guest): ?>
        <tr>
            <td width="25%"><?php echo $guest->fullname; ?></td>
            <td width="25%"><?php echo $guest['GuestCategory']['title']; ?></td>
            <td width="25%"><?php echo $guest['Seating']['title']; ?></td>
            <td width="25%"><?php echo $guest->place; ?></td>
        </tr>
        <?php endforeach; ?>
    </table>
</div>