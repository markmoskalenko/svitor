<div class="content" id="guestForm">
    <div id="msg"></div>
    <form onsubmit="return false;">
        <table class="input_data">
            <tr>
                <td style="padding: 10px 0 0 0;">
                    <div class="inline_name" style="padding: 5px 0;">Категория <span class="red_text">*</span></div>
                    <div class="field">
                        <?php
                        $list_data = CHtml::listData($guest_categories, 'id', 'title');
                        echo CHtml::dropDownList('UserGuests[guest_cat_id]', isset($model->guest_cat_id) ? $model->guest_cat_id : '', $list_data, array(
                            'empty' => '-- не выбрано --',
                            'required' => 'required'
                        ));
                        ?>
                    </div>

                    <div class="inline_name" style="padding: 10px 0 5px 0;">Имя <span class="red_text">*</span></div>
                    <div class="field">
                        <?php
                        $model->fullname = CHtml::decode($model->fullname);
                        echo CHtml::activeTextField($model, 'fullname', array(
                            'style' => 'width: 333px;',
                            'required' => 'required'
                        ));
                        ?>
                    </div>

                </td>
                <td class="t_va l_al">
                    <div id="image_thumbs">
                        <img src="<?php echo Yii::app()->ImgManager->getUrlById($model->img_id, 'medium', $model->img_filename); ?>">
                        <img src="<?php echo Yii::app()->ImgManager->getUrlById($model->img_id, 'small', $model->img_filename); ?>">
                    </div>
                    <a id="upload_photo">выбрать фотографию &rarr;</a>
                    <input type="hidden" name="UserGuests[img_id]" id="img_id" value="<?php echo $model->img_id; ?>">
                    <input type="hidden" name="UserGuests[img_filename]" id="img_filename"
                           value="<?php echo $model->img_filename; ?>">
                </td>
            </tr>
        </table>

        <table class="input_data">
            <tr>
                <td class="left_name" width="110px" style="padding-top: 7px;">Пол <span class="red_text">*</span</td>
                <td>
                    <?php
                    $list_data = array(
                        'male' => 'Мужской',
                        'female' => 'Женский'
                    );
                    echo CHtml::activeRadioButtonList($model, 'gender', $list_data, array(
                        'separator' => '&nbsp;&nbsp;&nbsp;',
                        'required' => 'required'
                    ));
                    ?>
                </td>
            </tr>
            <tr>
                <td class="left_name" width="110px">Дата рождения</td>
                <td>
                    <?php
                    $model->birthday = !empty($model->birthday) ? date('d.m.Y', $model->birthday) : '';
                    echo CHtml::activeTextField($model, 'birthday', array(
                        'style' => 'width: 90px;',
                        'placeholder' => 'дд.мм.гггг'
                    ));
                    ?>
                </td>
            </tr>
            <tr>
                <td class="left_name" width="110px">Телефон</td>
                <td>
                    <?php
                    $model->phone_number = CHtml::decode($model->phone_number);
                    echo CHtml::activeTextField($model, 'phone_number');
                    ?>
                </td>
            </tr>
            <tr>
                <td class="left_name" width="110px">Email</td>
                <td>
                    <?php echo CHtml::activeTextField($model, 'email'); ?>
                </td>
            </tr>
            <tr>
                <td class="left_name" width="110px">Вид меню</td>
                <td>
                    <?php
                    $list_data = CHtml::listData($guest_menu_types, 'id', 'title');
                    echo CHtml::dropDownList('UserGuests[menu_type_id]', isset($model->menu_type_id) ? $model->menu_type_id : '', $list_data, array(
                        'empty' => '-- не выбрано --'
                    ));
                    ?>
                </td>
            </tr>
        </table>
    </form>
</div>
<div class="buttons">
    <div class="button_main">
        <button type="button"
                onclick="guest.sendForm(this, '<?php echo $model->isNewRecord ? 'add' : 'edit/id/' . $model->id; ?>');">
            <?php echo $model->isNewRecord ? 'Добавить' : 'Сохранить'; ?>
        </button>
    </div>
    <div class="button_grey">
        <button onclick="$.fancybox.close();">Отменить</button>
    </div>
</div>

<script src="/static/js/ajaxupload.js"></script>
<script>
    $(document).ready(function () {
        $('#UserGuests_birthday').datepicker({
            changeYear:true,
            dateFormat:'dd.mm.yy',
            maxDate:new Date(<?php echo date('Y'); ?>, 1 - 1, 1),
            monthNames:['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
            monthNamesShort:['янв', 'фев', 'мар', 'апр', 'май', 'июн', 'июл', 'авг', 'сен', 'окт', 'ноя', 'дек'],
            dayNamesMin:["Вс", "Пн", "Вт", "Ср", "Чт", "Пт", "Сб"],
            showAnim:'drop',
            yearRange:"c-70:c+70",
            defaultDate:-7320,
        });
        guest.uploadPhoto($('#upload_photo'));
    });
</script>
