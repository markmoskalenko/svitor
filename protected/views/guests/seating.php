<div class="seating_title">
    Рассадка гостей
    <?php $this->widget('application.widgets.FunBar', array('short_mode' => true)); ?>
</div>
<?php
if (mApi::getFunId()):
?>
<div class="seating_switch_menu">
    <a onclick="planning.hideLeftMenu(this); return false;">&larr; Скрыть меню</a>
</div>
<script type="text/javascript" src="/static/seating/swfobject.js"></script>
<script type="text/javascript">
    var swfVersionStr = "11.1.0";
    var xiSwfUrlStr = "playerProductInstall.swf";
    var flashvars = {};
    flashvars.wedding_id = <?php echo mApi::getFunType() == 0 ? mApi::getFunId() : 0; ?>;
    flashvars.holiday_id = <?php echo mApi::getFunType() == 1 ? mApi::getFunId() : 0; ?>;
    flashvars.root_folder="/static/seating/";
    var params = {};
    params.quality = "high";
    params.bgcolor = "#f7f1bd";
    params.allowscriptaccess = "sameDomain";
    params.allowfullscreen = "true";
    var attributes = {};
    attributes.id = "Seating";
    attributes.name = "Seating";
    attributes.align = "middle";
    swfobject.embedSWF(
            flashvars.root_folder + "Seating.swf", "flashContent",
            "100%", "440px",
            swfVersionStr, xiSwfUrlStr,
            flashvars, params, attributes
    );

    var planning = {
        showLeftMenu : function (trigger) {
            $('.left_sidebar').show();
            $(trigger).attr('onclick', 'planning.hideLeftMenu(this); return false;');
            $(trigger).html('&larr; Скрыть меню');
        },

        hideLeftMenu : function (trigger) {
            $('.left_sidebar').hide();
            $(trigger).attr('onclick', 'planning.showLeftMenu(this); return false;');
            $(trigger).html('&rarr; Показать меню');
        }
    };
</script>

<div class="seating_content">
    <div id="flashContent">
        <p>Для того чтобы воспользоваться рассадкой гостей, вам необходимо иметь установленный <b>Adobe Flash Player</b> версии <b>11.1.0</b>.</p>
        <script type="text/javascript">
            var pageHost = ((document.location.protocol == "https:") ? "https://" : "http://");
            document.write("<a href='http://www.adobe.com/go/getflashplayer'><img src='"
                    + pageHost + "www.adobe.com/images/shared/download_buttons/get_flash_player.gif' alt='Get Adobe Flash player' /></a>" );
        </script>
    </div>
</div>

<?php else:
    $this->widget('application.widgets.SelectFun');
endif;
?>