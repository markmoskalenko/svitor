<table class="simple_table budget_table">
    <?php if (!empty($data)): ?>
    <tr class="thead_help">
        <td class="c_al td_note"></td>
        <td class="l_al td_title">Наименование</td>
        <td class="l_al more"></td>
        <td class="l_al plan">План</td>
        <td class="l_al fact">Факт</td>
        <td class="l_al paid">Оплачено</td>
        <td class="l_al left">Остаток</td>
        <td class="l_al date">Оплатить до</td>
        <td class="l_al editor"></td>
    </tr>

    <?php foreach ($data as $cat): ?>

        <tr class="thead">
            <th class="l_al th_title" colspan="9"><?php echo $cat['title']; ?></th>
        </tr>

        <?php foreach ($cat['UserBudget'] as $var): ?>

            <tr class="item" id="item_<?php echo $var['id']; ?>">
                <td class="l_al th_note">
                    <?php if (!empty($var->note)): ?>
                    <div class="note"
                         onmouseover="tooltip.show(this, '<?php echo $var->note; ?>', {special_class : 'white'});"></div>
                    <?php endif; ?>
                </td>

                <td class="l_al th_title">
                    <div class="title_text">
                        <span class="budget_active_value" onclick="budget.editVal(this, <?php echo $var['id']; ?>);"
                              onmouseover="tooltip.show(this, 'Кликните для быстрого редактирования', {position :'top-right', delay : 2000});"><?php echo $var['title']; ?></span>
                    </div>
                    <div class="input_shadow">
                        <input type="text" name="title" value="<?php echo $var['title']; ?>" maxlength="250"
                               class="edit_input">
                    </div>
                </td>

                <td class="c_al more">
                    <?php if (!empty($var->task_id)): ?>
                    <a class="active_task"
                       onclick="upBox.ajaxLoad('/checklist/edit/id/<?php echo $var->task_id; ?>'); return false"
                       onmouseover="tooltip.show(this, 'Редактировать задачу', {position: 'top-right'});"></a>
                    <?php else: ?>
                    <a class="none_task magic_button"
                       onclick="upBox.ajaxLoad('/checklist/add/budget_id/<?php echo $var->id; ?>'); return false"
                       onmouseover="tooltip.show(this, 'Добавить задачу', {position: 'top-right'});"></a>
                    <?php endif; ?>
                </td>

                <td class="l_al plan">
                    <span class="budget_active_value" onclick="budget.editVal(this, <?php echo $var['id']; ?>, 'int');"
                          onmouseover="tooltip.show(this, 'Кликните для быстрого редактирования', {position :'top-right', delay : 2000});"><?php echo $var['amount_plan']; ?></span>

                    <div class="input_shadow">
                        <input type="text" name="amount_plan" value="<?php echo $var['amount_plan']; ?>"
                               onkeyup="budget.checkIntValue(this);" maxlength="10" class="edit_input">
                    </div>
                </td>

                <td class="l_al fact">
                    <span class="budget_active_value" onclick="budget.editVal(this, <?php echo $var['id']; ?>, 'int');"
                          onmouseover="tooltip.show(this, 'Кликните для быстрого редактирования', {position :'top-right', delay : 2000});"><?php echo $var['amount_fact']; ?></span>

                    <div class="input_shadow">
                        <input type="text" name="amount_fact" value="<?php echo $var['amount_fact']; ?>"
                               onkeyup="budget.checkIntValue(this);" maxlength="10" class="edit_input">
                    </div>
                </td>

                <td class="l_al paid">
                    <span class="budget_active_value" onclick="budget.editVal(this, <?php echo $var['id']; ?>, 'int');"
                          onmouseover="tooltip.show(this, 'Кликните для быстрого редактирования', {position :'top-right', delay : 2000});"><?php echo $var['paid_amount']; ?></span>

                    <div class="input_shadow">
                        <input type="text" name="paid_amount" value="<?php echo $var['paid_amount']; ?>"
                               onkeyup="budget.checkIntValue(this);" maxlength="10" class="edit_input">
                    </div>
                </td>

                <td class="l_al left">
                    <?php echo $var['amount_fact'] - $var['paid_amount']; ?>
                </td>

                <td class="l_al date">
                    <?php echo !empty($var['date']) ? mApi::getDateWithMonth($var['date'], 1) : '--'; ?>
                </td>

                <td class="l_al editor">
                    <div class="editor_links">
                        <a class="edit magic_button" onclick="budget.editItem(<?php echo $var['id']; ?>); return false;"
                           onmouseover="tooltip.show(this, 'Редактировать', {position :'top-right'});"></a>
                        <a class="delete magic_button"
                           onclick="budget.deleteItem(<?php echo $var['id']; ?>); return false;"
                           onmouseover="tooltip.show(this, 'Удалить', {position :'top-right'});"></a>
                    </div>
                </td>
            </tr>

            <?php endforeach; ?>

        <?php endforeach; ?>
    <tr class="empty_item js_empty_item">
        <td colspan="8" class="l_al">Нет ни одной записи...</td>
    </tr>

    <?php else: ?>
    <tr class="empty_item">
        <td colspan="8" class="l_al">Нет ни одной записи...</td>
    </tr>
    <?php endif; ?>
</table>