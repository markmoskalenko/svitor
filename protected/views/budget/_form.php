<div class="content" id="budgetForm">
    <div id="msg"></div>
    <form onsubmit="return false;">
        <div class="label_name">Группа <span class="red_text">*</span></div>
        <div class="field">
            <?php
            $list_data = CHtml::listData($cat_mins, 'id', 'title');
            echo CHtml::dropDownList('UserBudget[cat_min_id]', isset($model->cat_min_id) ? $model->cat_min_id : '', $list_data, array(
                'empty' => '-- не выбрано --',
                'required' => 'required'
            ));
            ?>
        </div>

        <div class="label_name">Наименование <span class="red_text">*</span></div>
        <div class="field">
            <?php
            $model->title = CHtml::decode($model->title);
            echo CHtml::activeTextField($model, 'title', array(
                'style' => 'width: 98%;',
                'required' => 'required'
            ));
            ?>
        </div>

        <table class="input_data">
            <tr>
                <td class="left_name" width="120px">Оплатить до</td>
                <td>
                    <?php
                    $model->date = (!$model->isNewRecord && !empty($model->date)) ? date('d.m.Y', $model->date) : '';
                    echo CHtml::activeTextField($model, 'date', array(
                        'style' => 'width: 120px;',
                        'placeholder' => 'дд.мм.гггг'
                    ));
                    ?>
                    <span class="note">(дата <?php echo mApi::getFunType() == 1 ? 'торжества: ' . mApi::getDateWithMonth(mApi::getHoliday()->date, 1, true) : 'свадьбы: ' . mApi::getDateWithMonth(mApi::getWedding()->date, 1, true);?>
                        )</span>
                </td>
            </tr>
            <tr>
                <td class="left_name" width="120px">Сумма план.</td>
                <td>
                    <?php
                    echo CHtml::activeTextField($model, 'amount_plan', array(
                        'onchange' => 'budget_form.setAmountPlan(this);',
                        'onkeyup' => 'budget_form.setAmountPlan(this);',
                        'onclick' => 'budget_form.selectSum(this);',
                        'style' => 'width: 120px;',
                        'maxlength' => '10'
                    ));
                    ?>
                    <span class="note"><?php echo mApi::getCurrency('short'); ?>.</span>
                    &larr; <span id="result_counter">экономия 0<?php echo mApi::getCurrency(); ?></span>.
                </td>
            </tr>
            <tr>
                <td class="left_name" width="120px">Сумма факт.</td>
                <td>
                    <?php
                    echo CHtml::activeTextField($model, 'amount_fact', array(
                        'onchange' => 'budget_form.setAmountFact(this);',
                        'onkeyup' => 'budget_form.setAmountFact(this);',
                        'onclick' => 'budget_form.selectSum(this);',
                        'style' => 'width: 120px;',
                        'maxlength' => '10'
                    ));
                    ?>
                    <span class="note"><?php echo mApi::getCurrency('short'); ?>.</span>
                </td>
            </tr>
            <tr>
                <td class="left_name" width="120px">Оплачено</td>
                <td>
                    <?php
                    echo CHtml::activeTextField($model, 'paid_amount', array(
                        'onchange' => 'budget_form.setPaidAmount(this);',
                        'onkeyup' => 'budget_form.setPaidAmount(this);',
                        'onclick' => 'budget_form.selectSum(this);',
                        'style' => 'width: 120px;',
                        'maxlength' => '10'
                    ));
                    ?>
                    <span class="note"><?php echo mApi::getCurrency('short'); ?>.</span>
                    &larr; осталось оплатить <b><span
                        id="balance_counter">0</span></b><?php echo mApi::getCurrency(); ?>.
                    <div class="errorMessage" style="display: none;" id="paid_error">&uarr; превышает факт. сумму</div>
                </td>
            </tr>
        </table>

        <div id="box_note"<?php if (empty($model->note)) echo ' style="display:none;"'; ?>>
            <div class="label_name">Примечание</div>
            <div class="field">
                <?php
                $model->note = CHtml::decode($model->note);
                echo CHtml::activeTextArea($model, 'note', array('style' => 'width: 98%; height: 60px;'));
                ?>
            </div>
        </div>
        <div class="label_name"><a onclick="budget_form.noteSwitch(this); return false;"
                                   class="js_link"><?php echo empty($model->note) ? 'Добавить примечание' : 'Скрыть примечание'; ?></a>
        </div>
    </form>
</div>

<div class="buttons">
    <div class="button_main">
        <?php $add_link = !empty($model->task_id) ? 'add/task_id/' . $model->task_id : 'add'; ?>
        <button type="button" id="send_button"
                onclick="budget_form.sendData(this, '<?php echo $model->isNewRecord ? $add_link : 'edit/id/' . $model->id; ?>');"><?php echo $model->isNewRecord ? 'Добавить' : 'Сохранить'; ?></button>
    </div>
    <div class="button_grey">
        <button onclick="$.fancybox.close();">Отменить</button>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('#UserBudget_date').datepicker({
            changeYear:true,
            dateFormat:'dd.mm.yy',
            minDate:new Date(<?php echo date('Y') . ',' . date('m') . ' - 1,' . date('d'); ?>),
            maxDate:new Date(2020, 1 - 1, 1),
            monthNames:['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
            monthNamesShort:['янв', 'фев', 'мар', 'апр', 'май', 'июн', 'июл', 'авг', 'сен', 'окт', 'ноя', 'дек'],
            dayNamesMin:["Вс", "Пн", "Вт", "Ср", "Чт", "Пт", "Сб"],
            showAnim:'drop'
        });

        budget_form.init({
            plan: <?php echo !empty($model->amount_plan) ? $model->amount_plan : 0; ?>,
            fact: <?php echo !empty($model->amount_fact) ? $model->amount_fact : 0; ?>,
            paid: <?php echo !empty($model->paid_amount) ? $model->paid_amount : 0; ?>,
            currency:'<?php echo mApi::getCurrency(); ?>'
        });
    });
</script>
