<link rel="stylesheet" type="text/css" href="/static/timePicker.css">
<div class="budget_title">
    Бюджет
    <?php $this->widget('application.widgets.FunBar', array('short_mode' => true)); ?>
</div>

<?php
if (mApi::getFunId()):
    $url = '/budget/index/' . Yii::app()->request->baseUrl;
    ?>
<div class="budget_total_info magic_button">
    <table>
        <tr>
            <td>По плану:</td>
            <td class="r_al"><b
                    id="total_plan"><?php echo intval($total_info['total_plan']); ?></b> <?php echo mApi::getCurrency('short'); ?>.
            </td>
        </tr>
        <tr>
            <td>По факту:</td>
            <td class="r_al"><b
                    id="total_fact"><?php echo intval($total_info['total_fact']); ?></b> <?php echo mApi::getCurrency('short'); ?>.
            </td>
        </tr>
        <tr id="total_left">
            <td>
                <?php
                if ($total_info['total_plan'] != 0 || $total_info['total_fact'] != 0) {
                    $val = intval(round($total_info['total_plan'] - $total_info['total_fact']));

                    if ($val > 0) {
                        echo 'Экономия:</td><td class="r_al"><b class="budget_result_plus">' . $val . '</b>';
                    } else if ($val <= 0) {
                        echo 'Перерасход:</td><td class="r_al"><b class="budget_result_minus">' . $val . '</b>';
                    }
                } else {
                    echo 'Перерасход:</td><td class="r_al"><b>0</b>';
                }
                ?>
                <?php echo mApi::getCurrency('short'); ?>.
            </td>
        </tr>
        <tr class="last">
            <td>Оплачено:</td>
            <td class="r_al"><b
                    id="total_paid"><?php echo intval($total_info['total_paid']); ?></b> <?php echo mApi::getCurrency('short'); ?>.
            </td>
        </tr>
    </table>
</div>

<div class="simple_add_button">
    <a onclick="budget.addNew(); return false;">Добавить</a>
</div>

<div class="simple_sortby budget_sortby">
    <ul>
        <li<?php if ($show_by == 'list') echo ' class="active"'; ?>>
            <a href="<?php echo Yii::app()->createUrl($url, array('show_by' => 'list')); ?>">Список</a>
        </li>
        <li<?php if ($show_by == 'category') echo ' class="active"'; ?>>
            <a href="<?php echo Yii::app()->createUrl($url, array('show_by' => 'category')); ?>">По группам</a>
        </li>
    </ul>
</div>

<div id="trash-dialog" style="display: none;" title="Удалить">Удаленную запись невозможно будет восстановить. Если у
    этой записи имеется связь с задачей - она будет разорвана.<br><br>Вы действительно хотите продолжить?
</div>

<?php $this->renderPartial('by_' . $show_by, array('data' => $data, 'sort' => $sort)); ?>

<script src="/static/js/m_budget.js"></script>
<script src="/static/js/m_budget_form.js"></script>
<script src="/static/js/m_checklist_form.js"></script>
<script>
    $(document).ready(function () {
        budget.init({
            currency:'<?php echo mApi::getCurrency('short'); ?>'
        });
    });
</script>
<?php else:
    $this->widget('application.widgets.SelectFun');
endif;
?>