<div class="content" id="newMsgForm">
    <div id="msg"></div>
    <form onsubmit="return false;">
        <div class="label_name">Кому</div>
        <div class="field">
            <?php
            if (!empty($to_vid)) {
                echo '<a href="' . Yii::app()->createUrl('vendor/profile', array('id' => $to_vid->id)) . '" title="' . $to_vid->title . '">' . mApi::cutStr($to_vid->title, 60) . '</a>';

            } else {
                echo '<a href="' . Yii::app()->createUrl('user/profile', array('id' => $to_uid->id)) . '" title="' . $to_uid->login . '">' . mApi::cutStr($to_uid->login, 60) . '</a>';
            }
            ?>
        </div>

        <div class="label_name">Тема <span class="red_text">*</span></div>
        <div class="field">
            <?php
            $msg->title = CHtml::decode($msg->title);
            echo CHtml::activeTextField($msg, 'title', array(
                'style' => 'width: 98%;',
                'required' => 'required'
            ));
            ?>
        </div>

        <div class="field">
            <div class="label_name">Сообщение <span class="red_text">*</span></div>
            <div class="field">
                <?php
                $msg->message = CHtml::decode($msg->message);
                echo CHtml::activeTextArea($msg, 'message', array('style' => 'width: 98%; height: 60px;'));
                ?>
            </div>
        </div>
    </form>
</div>

<div class="buttons">
    <div class="button_main">
        <button type="button"
                onclick="msg.send(this, <?php echo !empty($to_uid) ? $to_uid->id : 0; ?>, <?php echo !empty($to_vid) ? $to_vid->id : 0; ?>, <?php echo $bid ?>);">
            Отправить
        </button>
    </div>
    <div class="button_grey">
        <button onclick="$.fancybox.close();">Отменить</button>
    </div>
</div>

<script>
    var msg = {
        send:function (trigger, to_uid, to_vid, bid) {
            var msg = $('#msg');
            var url = '/messages/new/';
            var old_button_content = $(trigger).html();
            $(trigger).html($.ajaxLoader()).attr('disabled', true);

            if (to_uid != 0) {
                url += 'uid/' + to_uid;
            } else {
                url += 'vid/' + to_vid;
            }

            url += '/bid/' + bid;

            var data = $('#newMsgForm form').serialize();

            $.post(url, data, function (re) {
                if (re == 'ok') {
                    $('.b-upbox .buttons').fadeOut(300);
                    $('#newMsgForm').fadeOut(300, function () {
                        $(this).html('<br><div align="center"><b>Сообщение успешно отправлено.</b></div>').fadeIn(100);
                    });
                    msg.hide();
                    setTimeout(function () {
                        $.fancybox.close();
                        window.location.hash = '';
                        location.reload();
                    }, 2500);
                } else {
                    $(trigger).html(old_button_content).removeAttr('disabled');
                    msg.html(re).fadeIn(200);
                }
            });
        }
    };
</script>
