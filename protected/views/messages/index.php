<h2>Сообщения</h2>

<div class="simple_folder_tabs">
    <ul>
        <li<?php if ($folder == 'inbox') echo ' class="active"'; ?>>
            <a href="<?php echo Yii::app()->createUrl('/messages/index', array('folder' => 'inbox')); ?>">Входящие</a>
        </li>
        <li<?php if ($folder == 'outbox') echo ' class="active"'; ?>>
            <a href="<?php echo Yii::app()->createUrl('/messages/index', array('folder' => 'outbox')); ?>">Отправленные</a>
        </li>
        <li<?php if ($folder == 'bid') echo ' class="active"'; ?>>
            <a href="<?php echo Yii::app()->createUrl('/messages/index', array('folder' => 'bid')); ?>">Ответы на заявки (<?php echo UserMessages::getCountBid(); ?>)</a>
        </li>
        <li<?php if (Yii::app()->controller->action == 'support') echo ' class="active"'; ?> style="float: right;">
            <a class="help_button" href="<?php echo Yii::app()->createUrl('messages/support'); ?>">Получить помощь</a>
        </li>
    </ul>
</div>

<div class="messages_action_buttons">
    <div class="delete">
        <div class="counter">Выделено <span class="selected_msg_count"><b>0</b> сообщений</span></div>
        <div class="button_main">
            <button onclick="messages.deleteSelected(this, '<?php echo $folder; ?>');">Удалить</button>
        </div>
    </div>
</div>

<?php if (!empty($messages)): ?>

<table class="simple_table messages_table">
    <tr>
        <th>
            <input type="checkbox" name="select_all" value="" onclick="$(':checkbox').attr('checked', $(this).is(':checked')); messages.select(this);">
        </th>
        <th class="l_al"><?php echo $folder == 'inbox' ? 'От' : 'Кому'; ?></th>
        <th></th>
        <th class="l_al">Тема</th>
        <th class="l_al">Дата</th>
        <?php if ($folder == 'bid'): ?>
            <th class="l_al">Заявка</th>
        <?php endif; ?>
    </tr>
    <?php $this->renderPartial('_' . $folder, array('messages' => $messages)); ?>
</table>

<div class="messages_pagination">
    <?php
    $this->widget('CLinkPager', array(
        'pages' => $dataProvider->pagination,
        'htmlOptions' => array('class' => 'b-default_pager'),
        'firstPageLabel' => '',
        'lastPageLabel' => '',
        'prevPageLabel' => '&larr; Назад',
        'nextPageLabel' => 'Далее &rarr;',
        'header' => '',
        'cssFile' => false
    ));
    ?>
</div>
<script src="/static/js/m_messages.js"></script>
<?php else: ?>
<div class="messages_empty">Нет сообщений...</div>
<?php endif; ?>