<h2>Сообщения</h2>

<div class="simple_folder_tabs">
    <ul>
        <li>
            <a href="<?php echo Yii::app()->createUrl('messages/index', array('folder' => 'inbox')); ?>">Входящие</a>
        </li>
        <li>
            <a href="<?php echo Yii::app()->createUrl('messages/index', array('folder' => 'outbox')); ?>">Отправленные</a>
        </li>
        <li>
            <a href="<?php echo Yii::app()->createUrl('messages/index', array('folder' => 'bid')); ?>">Ответы на заявки</a>
        </li>
        <li class="active">
            <a href="">Просмотр сообщения</a>
        </li>
        <li<?php if (Yii::app()->controller->action == 'support') echo ' class="active"'; ?> style="float: right;">
            <a href="<?php echo Yii::app()->createUrl('messages/support'); ?>">Получить помощь</a>
        </li>
    </ul>
</div>

<table class="simple_table messages_table">
    <tr>
        <th>&nbsp;</th>
    </tr>
</table>

<table class="message_view_table">
    <tr>
        <td class="left"><?php echo $message->from_uid != User::getId() ? 'От' : 'Кому'; ?>:</td>
        <td>
            <?php
            $msg_type = $message->from_uid != User::getId() ? 'From' : 'To';
            $msg_type_low = strtolower($msg_type);

            if (!empty($message[$msg_type_low . '_vid'])) {
                $author = mApi::cutStr($message[$msg_type . 'Vendor']['title'], 80);
                $url = Yii::app()->createUrl('vendor/profile', array('id' => $message[$msg_type_low . '_vid']));

                echo '<a href="' . $url . '" title="' . $message[$msg_type . 'Vendor']['title'] . '">';
                echo '<img class="avatar" src="' . Yii::app()->ImgManager->getUrlById($message[$msg_type . 'Vendor']['img_id'], 'small', $message[$msg_type . 'Vendor']['img_filename']) . '">';
                echo '</a>';

                echo '<a href="' . $url . '" title="' . $message[$msg_type . 'Vendor']['title'] . '">';
                echo '<b>' . $author . '</b>';
                echo '</a>';
            } else {
                $author = mApi::cutStr($message[$msg_type . 'User']['login'], 80);
                $url = Yii::app()->createUrl('user/profile', array('id' => $message[$msg_type_low . '_uid']));

                echo '<a href="' . $url . '" title="' . $message[$msg_type . 'User']['login'] . '">';
                echo '<img class="avatar" src="' . Yii::app()->ImgManager->getUrlById($message[$msg_type . 'User']['img_id'], 'small', $message[$msg_type . 'User']['img_filename']) . '">';
                echo '</a>';

                echo '<a href="' . $url . '" title="' . $message[$msg_type . 'User']['login'] . '">';
                echo '<b>' . $author . '</b>';
                echo '</a>';
            }
            ?>
            <br><span class="date"><?php echo mApi::getDateWithMonth($message['date'], 1); ?>
            в <?php echo date('H:i', $message['date']); ?></span>
        </td>
    </tr>
    <tr>
        <td class="left">Тема:</td>
        <td>
            <b><?php echo $message['title']; ?></b>
        </td>
    </tr>
    <tr>
        <td class="left top">Сообщение:</td>
        <td>
            <?php echo nl2br($message['message']); ?>
        </td>
    </tr>

    <?php if( $message->is_bid != 0 && !empty($message->is_bid) ): ?>
    <tr>
        <td class="left top">Заявка:</td>
        <td>
            <?php $bidDesc = Bid::model()->find('id = :id', array(':id'=>   (int)$message->is_bid)); ?>
            <?php if(isset($bidDesc->description)): ?>
                <span onmouseover="tooltip.show(this, '<?php  echo str_replace("\n",'',str_replace("\r\n",'',$bidDesc->description)); ?>', {position:'top-right'});"><?php echo Bid::model()->findByPk($message->is_bid)->type_holiday; ?></span>
            <?php endif; ?>
        </td>
    </tr>
    <?php endif; ?>

    <tr>
        <td></td>
        <td>
            <div id="error_box" style="display: none;"></div>
            <textarea class="message_reply_textarea" id="reply_text_<?php echo $message['id']; ?>"></textarea>
            <table>
                <td>
                    <div class="button_main">
                        <button onclick="messages.sendReply(this, <?php echo $message['id']; ?>);"><?php echo $message->from_uid != User::getId() ? 'Ответить' : 'Отправить'; ?></button>
                    </div>
                </td>
                <td>или <a onclick="messages.delete(this, <?php echo $message['id']; ?>);">Удалить</a></td>
            </table>
        </td>
    </tr>
</table>
<script src="/static/js/m_messages.js"></script>


