<?php foreach ($messages as $message): ?>
<tr>
    <td class="actions">
        <label><input type="checkbox" name="msg[]" value="<?php echo $message['id']; ?>" onclick="messages.select(this);"></label>
    </td>

    <?php if (!empty($message['from_vid'])): ?>
    <td class="avatar">
        <a href="<?php echo Yii::app()->createUrl('vendor/profile', array('id' => $message->from_vid)); ?>">
            <img src="<?php echo Yii::app()->ImgManager->getUrlById($message['FromVendor']['img_id'], 'small', $message['FromVendor']['img_filename']); ?>">
        </a>
    </td>
    <td class="author">
        <?php
        $author = mApi::cutStr($message['FromVendor']['title'], 15);

        echo '<a href="' . Yii::app()->createUrl('vendor/profile', array('id' => $message->from_vid)) . '" title="' . $message['FromVendor']['title'] . '">';
        echo $message->to_status == "new" ? '<b>' . $author . '</b>' : $author;
        echo '</a>';
        ?>
    </td>

    <?php else : ?>

    <td class="avatar">
        <a href="<?php echo Yii::app()->createUrl('user/profile', array('id' => $message->from_uid)); ?>">
            <img src="<?php echo Yii::app()->ImgManager->getUrlById($message['FromUser']['img_id'], 'small', $message['FromUser']['img_filename']); ?>">
        </a>
    </td>
    <td class="author">
        <?php
        $author = mApi::cutStr($message['FromUser']['login'], 15);

        echo '<a href="' . Yii::app()->createUrl('user/profile', array('id' => $message->from_uid)) . '" title="' . $message['FromUser']['login'] . '">';
        echo $message->to_status == "new" ? '<b>' . $author . '</b>' : $author;
        echo '</a>';
        ?>
    </td>
    <?php endif; ?>

    <td class="msg">
        <?php
        if ($message->to_status == 'new') {
            echo '<a href="' . Yii::app()->createUrl('messages/view', array('id' => $message->id)) . '"><b>' . $message['title'] . '</b></a>';
        } else {
            echo '<a href="' . Yii::app()->createUrl('messages/view', array('id' => $message->id)) . '">' . $message['title'] . '</a>';
        }
        ?>
    </td>
    <td class="date"
        onclick="location.assign('<?php echo Yii::app()->createUrl('messages/view', array('id' => $message->id)); ?>');">
        <span onmouseover="tooltip.show(this, 'Отправлено в <?php echo date('H:i', $message['date']); ?>', {position:'top-right'});"><?php echo mApi::getDateWithMonth($message['date'], 1); ?></span>
    </td>
</tr>
<?php endforeach; ?>