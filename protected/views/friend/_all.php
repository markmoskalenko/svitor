<div class="friends_list_wrap">
        <?php if (!empty($friends)): ?>

            <?php foreach ($friends as $friend):
                  $profile_url = Yii::app()->createUrl('user/profile', array('id' => $friend['User']['id']));
            ?>
                    <div class="friend_wrap">
                        <div class="avatar">
                            <a href="<?php echo $profile_url; ?>">
                                <img src="<?php echo Yii::app()->ImgManager->getUrlById($friend['User']['img_id'], 'small', $friend['User']['img_filename']); ?>">
                            </a>
                        </div>
                        <div class="info_wrap">
                            <div class="name">
                                <a href="<?php echo $profile_url; ?>"><?php echo $friend['User']['login']; ?></a>
                            </div>
                            <?php if ($friend['User']['id'] != User::getId()): ?>
                            <ul class="links">
                                <li>
                                    <a onclick="upBox.ajaxLoad('/messages/new/uid/<?php echo $friend['User']['id']; ?>');">Написать сообщение</a>
                                </li>
                                <li>
                                <?php if (!empty($user_info) && $user_info->id != User::getId()): ?>
                                    <a onclick="location.assign('/friend/add/uid/<?php echo $friend['User']['id']; ?>');">Добавить в друзья</a>
                                <?php else: ?>
                                    <a onclick="if (!confirm('Вы уверены?')) return false; location.assign('/friend/remove/uid/<?php echo $friend['User']['id']; ?>');">Удалить из друзей</a>
                                <?php endif; ?>
                                </li>
                            </ul>
                            <?php endif; ?>
                        </div>
                    </div>
            <?php endforeach; ?>

        <?php else: ?>
            <div class="empty_item">У вас пока нет ни одного друга...</div>
        <?php endif; ?>
</div>

<div style="margin-top: 15px;">
    <?php
    $this->widget('CLinkPager', array(
        'pages' => $dataProvider->pagination,
        'htmlOptions' => array('class' => 'b-default_pager'),
        'firstPageLabel' => '',
        'lastPageLabel' => '',
        'prevPageLabel' => '&larr; Назад',
        'nextPageLabel' => 'Далее &rarr;',
        'header' => '',
        'cssFile' => false
    ));
    ?>
</div>