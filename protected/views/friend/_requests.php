<div class="friends_list_wrap">
    <?php if (!empty($friends)): ?>

    <?php foreach ($friends as $friend):
        $profile_url = Yii::app()->createUrl('user/profile', array('id' => $friend['UserRequests']['id']));
        ?>
        <div class="friend_wrap">
            <div class="avatar">
                <a href="<?php echo $profile_url; ?>">
                    <img src="<?php echo Yii::app()->ImgManager->getUrlById($friend['UserRequests']['img_id'], 'small', $friend['User']['img_filename']); ?>">
                </a>
            </div>
            <div class="info_wrap">
                <div class="name">
                    <a href="<?php echo $profile_url; ?>"><?php echo $friend['UserRequests']['login']; ?></a>
                </div>
                <ul class="links">
                    <li>
                        <a onclick="upBox.ajaxLoad('/messages/new/uid/<?php echo $friend['UserRequests']['id']; ?>');">Написать сообщение</a>
                    </li>
                    <li>
                        <a onclick="location.assign('/friend/add/uid/<?php echo $friend['UserRequests']['id']; ?>');">Добавить в друзья</a>
                    </li>
                </ul>
            </div>
        </div>
        <?php endforeach; ?>

    <?php else: ?>
    <div class="empty_item">Нет новых заявок...</div>
    <?php endif; ?>
</div>

<div style="margin-top: 15px;">
    <?php
    $this->widget('CLinkPager', array(
        'pages' => $dataProvider->pagination,
        'htmlOptions' => array('class' => 'b-default_pager'),
        'firstPageLabel' => '',
        'lastPageLabel' => '',
        'prevPageLabel' => '&larr; Назад',
        'nextPageLabel' => 'Далее &rarr;',
        'header' => '',
        'cssFile' => false
    ));
    ?>
</div>