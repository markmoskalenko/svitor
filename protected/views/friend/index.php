<h2>Друзья<?php if (!empty($user_info) && $user_info->id != User::getId()) echo ' ' . $user_info->login; ?></h2>

<?php if (empty($user_info) || $user_info->id == User::getId()): ?>
    <div class="simple_folder_tabs">
        <ul>
            <li<?php if (empty($act)) echo ' class="active"'; ?>>
                <a href="<?php echo Yii::app()->createUrl('friend/index'); ?>">Все друзья</a>
            </li>
            <li<?php if ($act == 'requests') echo ' class="active"'; ?>>
                <a href="<?php echo Yii::app()->createUrl('friend/index', array('act' => 'requests')); ?>">Заявки в друзья<?php if (!empty($friends_requests_count)) echo ' <b>+' . $friends_requests_count . '</b>'; ?></a>
            </li>
        </ul>
    </div>
<?php endif; ?>

<?php
if ($act == 'requests') {
    $this->renderPartial('_requests', array('dataProvider' => $dataProvider, 'friends' => $dataProvider->getData()));
} else {
    $this->renderPartial('_all', array('user_info' => $user_info, 'dataProvider' => $dataProvider, 'friends' => $dataProvider->getData()));
}
?>