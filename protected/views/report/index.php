<table class="b-catalog-table">
    <td class="menu catalog_categories">
        <div class="button_main">
            <button onclick="location.assign('<?php echo Yii::app()->createUrl('report/add'); ?>');">+ Написать отчёт</button>
        </div>

        <?php if (!empty($categories)): ?>
            <?php foreach ($categories as $cat): ?>
                <div class="root<?php if (!empty($active_category) && $active_category->id == $cat->id) echo ' active'; ?>">
                    <a href="<?php echo Yii::app()->createUrl('report/index', array('cat' => $cat->name)); ?>"><?php echo $cat->title; ?></a>
                </div>
            <?php endforeach; ?>
        <?php endif; ?>
    </td>

    <td class="content">
        <?php if (empty($view_report)): ?>
        <h1>Отчёты</h1>

        <div class="info_search">
            <?php echo CHtml::beginForm(Yii::app()->createUrl('report'), 'GET'); ?>
            <?php echo CHtml::textField('q', Yii::app()->request->getParam('q'), array('placeholder' => 'Поиск по отчётам')); ?>
            <div class="button_grey">
                <button type="submit">Найти</button>
            </div>
            <?php echo CHtml::endForm(); ?>
        </div>
        <?php endif; ?>
        <?php
        if (!empty($view_report) && empty($reports)) {
            $this->renderPartial('_view', array(
                'view_report' => $view_report,
                'dataProvider' => $dataProvider
            ));
        } else {
            if (!empty($reports)) {
                $this->renderPartial('_reports_list', array('reports' => $reports, 'dataProvider' => $dataProvider));
            } else {
                echo 'К сожалению нам не удалось ничего найти... :(';
            }
        }
        ?>
    </td>
    <!-- <td class="params">

    </td> -->
</table>
<script src="/static/js/m_vendors.js"></script>
<script src="/static/js/m_catalog.js"></script>
