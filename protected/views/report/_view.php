<h1><?php echo $view_report->title; ?></h1>
<div class="info_fulltext">
    <?php echo $view_report->content; ?>

    <div class="r_fl">
        <?php $url = Yii::app()->createUrl('user/profile', array('id' => $view_report['User']['id'])); ?>
        <b>Автор:</b> &laquo;<a href="<?php echo $url; ?>"><?php echo mApi::cutStr($view_report['User']['login'], 30); ?></a>&raquo;
    </div>

    <table style="width: 100%; border-collapse: collapse; margin-top: 15px; color: #777;">
        <tr>
            <td class="l_al">
                <script type="text/javascript" src="http://yandex.st/share/share.js" charset="utf-8"></script>
                <div style="margin-left: -6px;" class="yashare-auto-init" data-yashareL10n="ru"
                     data-yashareType="button"
                     data-yashareQuickServices="vkontakte,facebook,twitter,odnoklassniki,moimir,gplus"></div>
            </td>
        </tr>
    </table>

    <?php if (User::getSecurityLevel() > 0): ?>
    <?php Yii::app()->user->setReturnUrl(Yii::app()->request->url); ?>
    <b>Модератор:</b> <a href="/mod_cp/report/edit/id/<?php echo $view_report->id; ?>">редактировать отчет</a><br>
    <?php endif; ?>
</div>