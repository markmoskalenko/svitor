<h2>Новый отчёт</h2>
<br>
<?php if (Yii::app()->user->hasFlash('add_report_success')): ?>
    <div class="msg_success"><?php echo Yii::app()->user->getFlash('add_report_success'); ?></div>
<?php else: ?>
    <div class="simple_desc">
        Постарайтесь в отчете отразить полезную, для других информацию, отразив с какими проблемами вы столкнулись при организации или проведении праздника, от чего бы вы хотели уберечь читателя. Возможно, вы просто захотите рассказать об "изюминке" вашего праздника? Это тоже может быть кому-то полезно. <br><br>
    </div>
    <div class="simple_form">
    <?php echo CHtml::beginForm(); ?>

        <div class="item">
            <div class="label">Раздел:</div>
            <div class="field">
                <?php echo CHtml::activeDropDownList($model, 'report_cat_id', $categories, array('empty' => '-- не выбрано --')); ?>
                <?php echo CHtml::error($model, 'report_cat_id'); ?>
            </div>
        </div>

        <div class="item">
            <div class="label">Название:</div>
            <div class="field d_width_50">
                <?php echo CHtml::activeTextField($model, 'title'); ?>
                <?php echo CHtml::error($model, 'title'); ?>
            </div>
        </div>

        <div class="item">
            <div class="field d_width_100">
                <?php echo CHtml::activeTextArea($model, 'content'); ?>
                <?php echo CHtml::error($model, 'content'); ?>
            </div>
        </div>

        <div class="item">
            <div class="field">
                <div class="button_main">
                    <button><?php echo $model->isNewRecord ? 'Опубликовать' : 'Сохранить изменения'; ?></button>
                </div>
            </div>
        </div>

    <?php echo CHtml::endForm(); ?>
    </div>

    <script type="text/javascript" src="/static/ckeditor/ckeditor.js"></script>
    <script type="text/javascript" src="/static/ckeditor/adapters/jquery.js"></script>
    <script type="text/javascript">
    $(document).ready(function () {
        $('#UserReports_content').ckeditor();
    });
    </script>
<?php endif; ?>