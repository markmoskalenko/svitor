<?php
foreach ($reports as $report) {
    if (!empty($active_category)) {
        $url = Yii::app()->createUrl('report/index', array('cat' => $active_category->name, 'view' => $report->id));
    } else {
        $url = Yii::app()->createUrl('report/index', array('cat' => $report['ReportCategory']['name'], 'view' => $report->id));
    }
    echo '<div class="info_post">';
    echo '<div class="info_post_title"><a href="' . $url . '">' . $report['title'] . '</a></div>';
    echo '<div class="info_post_text">' . $report['preview_content'] . ' <a href="' . $url . '">Читать дальше &rarr;</a></div>';
    echo '</div>';
}

$this->widget('CLinkPager', array(
    'pages' => $dataProvider->pagination,
    'htmlOptions' => array('class' => 'b-default_pager'),
    'firstPageLabel' => '',
    'lastPageLabel' => '',
    'prevPageLabel' => '&larr; Назад',
    'nextPageLabel' => 'Далее &rarr;',
    'header' => '',
    'cssFile' => false
));