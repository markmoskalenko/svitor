<div style="padding-left: 10px;">
    <table>
        <tr  style="vertical-align: top;height:40px;">
            <td colspan="2"><a href="/event/">События</a> / <?php echo $dataItem['title']; ?></td>
        </tr>
        <tr>
            <td style="width: 300px;"><h1><?php echo $dataItem['title']; ?></h1></div>
            </td>
            <td style="padding-top: 5px;">
<!--                c --><?php //echo date("d-m-Y",$dataItem['date_from']); ?><!-- по --><?php //echo date("d-m-Y",$dataItem['date_to']); ?>
            </td>
        </tr>
        <tr>
            <td colspan="2"><?php echo $dataItem['description'];?></td>
        </tr>
    </table>
</div>
<br>
<?php if (isset($dataItem['addres']) && !empty($dataItem['addres'])){?>

        <div id="box_address" class="box_content addresses">
            <?php
                $n = 1;
                $address_str = '';

                echo '<div class="element"><table><td><span class="num">Адрес: </span></td><td>';

                if ($dataItem['country'] == User::getCountryId()) {
                    $country_name = '<b>' . $country['name'] . '</b>, ';
                } else {
                    $country_name = $country['name'];
                }
                $address_str .= $country_name;

                if (!empty($dataItem['region'])) {
                    if ($dataItem['region'] == User::getRegionId()) {
                        $region_name = ', <b>' . $region['name'] . '</b>';
                    } else {
                        $region_name = ', ' . $region['name'];
                    }
                    $address_str .= $region_name;
                }

                if (!empty($dataItem['sity'])) {
                    if ($dataItem['sity'] == User::getCityId()) {
                        $city_name = ', <b>' . $city['name'] . '</b>';
                    } else {
                        $city_name = ', ' . $city['name'];
                    }
                    $address_str .= $city_name;
                }

                if (!empty($dataItem['addres']))
                    $address_str .= ', ' . $dataItem['addres'];

                $maps_url = 'http://maps.google.com/?output=embed&q=' . urlencode(strip_tags($address_str));
                echo '<a class="upbox-various fancybox.iframe" href="' . $maps_url . '">' . $address_str . '</a>';


                echo '<br><i>';

                echo '</td></table></div>';
            ?>
        </div>
<?php } ?>
<?php if (isset($dataItem['description_small']) && !empty($dataItem['description_small'])):?>
        <div>
            <i><?php echo $dataItem['description_small'];?></i>
        </div>
<?php endif;?>
<?php if($type == 'stock'):?>
<div>
    <a href="/vendor/<?php echo $company['id']; ?>">Компания "<?php echo $company['title']; ?>"</a>
</div>
<?php endif;?>

<link rel="stylesheet" type="text/css" href="/static/fancybox/helpers/jquery.fancybox-buttons.css"/>
<script type="text/javascript" src="/static/fancybox/helpers/jquery.fancybox-buttons.js"></script>
<script type="text/javascript" src="/static/fancybox/helpers/jquery.fancybox-media.js"></script>

<script src="/static/js/m_tabs.js"></script>
<script>
    $(document).ready(function () {
        tab.init({
            'default':'address'
        });
    });
</script>
