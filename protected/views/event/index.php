<div class="guests_title">
    Список гостей
    <?php $this->widget('application.widgets.FunBar', array('short_mode' => true)); ?>
</div>
<?php
if (mApi::getFunId()):
    ?>
<div class="guests_summary_info magic_button">
    Извещено <span class="notified_counter"><?php echo $notified_count; ?></span> из <span
        class="all_counter"><?php echo $all_count; ?></span> гостей
</div>

<div class="simple_add_button">
    <a onclick="guest.addNew(); return false;">Добавить</a>
</div>

<table class="simple_table guests_table">

    <?php if (!empty($data)): ?>

    <tr class="thead">
        <th class="c_al photo"></th>
        <th class="l_al name"><?php echo $sort->link('fullname', 'Имя'); ?></th>
        <th class="c_al category"><?php echo $sort->link('category', 'Категория'); ?></th>
        <th class="c_al phone"><?php echo $sort->link('phone', 'Телефон'); ?></th>
        <th class="c_al email"><?php echo $sort->link('email', 'Email'); ?></th>
        <th class="c_al menu_type"><?php echo $sort->link('menu', 'Вид меню'); ?></th>
        <th class="c_al notified"><?php echo $sort->link('notified', 'Извещен'); ?></th>
        <th class="l_al editor"></th>
    </tr>
    <?php foreach ($data as $var): ?>
        <tr class="item" id="item_<?php echo $var['id']; ?>">
            <td class="c_al photo"><img height="30px"
                                        src="<?php echo Yii::app()->ImgManager->getUrlById($var['img_id'], 'small', $var['img_filename']); ?>">
            </td>

            <td class="l_al name"><?php echo $var['fullname']; ?></td>
            <td class="c_al category"><?php echo $var['GuestCategory']['title']; ?></td>
            <td class="c_al phone"><?php echo !empty($var['phone_number']) ? $var['phone_number'] : '--'; ?></td>

            <td class="c_al email">
                <?php if (!empty($var['email'])): ?>
                <span onmouseover="tooltip.show(this, '<?php echo $var['email']; ?>', {position:'default', special_class:'white'});">есть</span>
                <?php else: ?>
                --
                <?php endif; ?>
            </td>

            <td class="c_al menu_type"><?php echo !empty($var['menu_type_id']) ? $var['GuestMenuTypes']['title'] : '--'; ?></td>

            <td class="c_al notified"><input name="notified" type="checkbox"
                                             onclick="guest.notified(this, <?php echo $var['id']; ?>)"
                                             value="1" <?php if ($var['notified']) echo ' checked="checked"'; ?>></td>

            <td class="l_al editor">
                <div class="editor_links">
                    <a class="edit magic_button" onclick="guest.goEdit(<?php echo $var['id']; ?>); return false;" onmouseover="tooltip.show(this, 'Редактировать', {position :'top-right'});"></a>
                    <a class="delete magic_button" onclick="guest.deleteItem(<?php echo $var['id']; ?>); return false;" onmouseover="tooltip.show(this, 'Удалить', {position :'top-right'});"></a>
                </div>
            </td>
        </tr>
        <?php endforeach; ?>
    <tr class="js_empty_item empty_item">
        <td colspan="7">Вы пока не добавили ни одного гостя...
        <td>
    </tr>

    <div id="trash-dialog" style="display: none;" title="Удалить гостя">Гость будет удален без возможности
        восстановления.<br><br>Вы действительно хотите продолжить?
    </div>

    <?php else: ?>
    <tr class="empty_item">
        <td colspan="7">Вы пока не добавили ни одного гостя...
        <td>
    </tr>
    <?php endif; ?>

</table>
<script src="/static/js/m_guests.js"></script>

<?php else:
    $this->widget('application.widgets.SelectFun');
endif;
?>