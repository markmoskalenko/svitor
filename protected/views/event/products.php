<script src="/static/js/calendar.js"></script>
<?php $pur = new Purifier(); ?>
<table class="b-catalog-table">

    <td class="content">
        <h1><?php echo Yii::app()->name; ?></h1>

        <div class="search-form">
            <?php echo CHtml::textField('q', Yii::app()->request->getParam('q'), array('placeholder' => 'Поиск по событиям','id'=>'find-text')); ?>
            <div class="button_grey">
                <button type="submit" id="find-text-button">Найти</button>
            </div>
        </div>
<!--СПИСОК-->
        <?php if( count($result['stock']) || count($result['divination']) || count($result['exhibition']) ): ?>
        <?php foreach( $result as $type=>$itemType ): ?>
              <?php if(count($itemType)): ?>
                  <?php foreach($itemType as $model): ?>
                        <div style="height: auto !important;" class="item">
                            <table>
                                <tr>
                                    <td style="width: 300px;">
                                        <div class="title"><?php echo CHtml::link($model->title, $this->createUrl( '/event/item',array('id'=>$type=='stock' ? $model->stock_id : $model->event_id,'type'=>$type) ) ); ?></div>
                                    </td>
                                    <td style="padding-top: 5px;">
                                    с <?php echo date("d-m-Y",(int)$model->date_from); ?> по <?php echo date("d-m-Y",(int)$model->date_to); ?>
                                    </td>

                                </tr>
                                <tr>
                                    <td class="general" colspan="2">
                                        <div class="desc">
                                                    <?php echo $pur->rest(mb_substr($model->description, 0, 450, 'UTF-8')).'...';?>
                                            <br>

                                        <?php echo CHtml::link('далее &rarr;', $this->createUrl( '/event/item',array('id'=>$type=='stock' ? $model->stock_id : $model->event_id,'type'=>$type) ) ); ?>

                                        </div>
                                        <?php if($type == 'stock'): ?>
                                            <?php echo CHtml::link('Компания "'.$model->Vendor->title.'"', $this->createUrl( '/vendor/profile',array('id'=>$model->vendor_id) ) ); ?>
                                        <?php endif; ?>
                                    </td>
                                </tr>
                            </table>
                        </div>
                  <?php endforeach; ?>
              <?php endif; ?>
        <?php endforeach; ?>
        <?php else: ?>
            Не найдено ни одного события..
        <?php endif; ?>
<!--КОНЕЦ СПИСОК-->
    </td>

    <td class="params">


        <div id="calendar_container"></div>

        <?php
        // Работает исключительно на магии.
        // Проверяем пары обязательных условий для отображения списка выбранных значений фильтра.
        if (
            ( /* STOCK */
              isset($_GET['is_active_stock']) &&
              $_GET['is_active_stock'] != 0   &&
              isset($_GET['stock'])           &&
              count($attrStock)               &&
              !empty($_GET['stock'])
            ) ||
            ( /* DIVINATION */
              isset($_GET['is_active_divination']) &&
              $_GET['is_active_divination'] != 0   &&
              isset($_GET['divination'])           &&
              count($attrDivination)               &&
              !empty($_GET['divination'])
            ) ||
            ( /* EXHIBITION */
              isset($_GET['is_active_exhibition']) &&
              $_GET['is_active_exhibition'] != 0   &&
              isset($_GET['exhibition'])           &&
              count($attrExhibition)               &&
              !empty($_GET['exhibition'])
            )
        ):
            ?>
            <div class="b-filter">
                <ul>
                    <?php foreach ($attrStock as $attr): ?>
                        <?php if ($attr->checkedOnFilter): ?>
                            <li>
                                <table>
                                    <tbody>
                                    <tr>
                                        <td class="remove">
                                            <?php echo CHtml::link(
                                                '×',
                                                'javascript:void(0)',
                                                array('class' => 'submit_form', 'ch' => "stock_{$attr->id}")
                                            ); ?>
                                        </td>
                                        <td><b><?php echo $attr->title; ?></b>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </li>
                        <?php endif; ?>
                    <?php endforeach; ?>
                    <?php if( count($attrDivination) ): ?>
                        <?php foreach( $attrDivination as $attr ): ?>
                            <?php if(!count($attr->Values)) continue; ?>
                            <?php foreach($attr->Values as $value): ?>
                                <?php if ($value->checkedOnFilter): ?>
                                    <li>
                                        <table>
                                            <tbody>
                                            <tr>
                                                <td class="remove">
                                                    <?php echo CHtml::link(
                                                        '×',
                                                        'javascript:void(0)',
                                                        array('class' => 'submit_form', 'ch' => "divination_{$value->id}")
                                                    ); ?>
                                                </td>
                                                <td><b><?php echo $value->value_string; ?></b>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </li>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        <?php endforeach; ?>
                    <?php endif; ?>

                    <!--                    ВЫСТАВКИ-->
                    <?php if( count($attrExhibition) ): ?>
                        <ul>
                            <?php foreach( $attrExhibition as $attr ): ?>
                                <?php if(!count($attr->Values)) continue; ?>
                                <?php foreach($attr->Values as $value): ?>
                                    <?php if ($value->checkedOnFilter): ?>
                                        <li>
                                            <table>
                                                <tbody>
                                                <tr>
                                                    <td class="remove">
                                                        <?php echo CHtml::link(
                                                            '×',
                                                            'javascript:void(0)',
                                                            array('class' => 'submit_form', 'ch' => "exhibition_{$value->id}")
                                                        ); ?>
                                                    </td>
                                                    <td><b><?php echo $value->value_string; ?></b>
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </li>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                            <?php endforeach; ?>
                        </ul>
                    <?php endif; ?>

                </ul>
                <div class="reset_all">↑ <?php echo CHtml::link('убрать все параметры', $this->createUrl('/event/products')); ?></div>
            </div>
        <?php else: ?>
            <div class="b-filter">
                Выберите необходимые параметры
            </div>
        <?php endif; ?>


        <div style="padding: 7px;">
            <?php echo CHtml::checkBox('is_active_stock', ( !isset($_GET['is_active_stock']) || $_GET['is_active_stock'] == 1  ? true : false ), array('id'=>'ch_isActiveStock','style'=>"float: left; margin-right: 15px;")); ?>
            <span>Акции</span>
        </div>

        <div style="padding: 7px;">
            <?php echo CHtml::checkBox('is_active_divination',  !isset($_GET['is_active_divination']) || $_GET['is_active_divination'] == 1  ? true : false, array('id'=>'ch_isActiveDivination','style'=>"float: left; margin-right: 15px;")); ?>
            <span>Гадания</span>
        </div>

        <div style="padding: 7px;">
            <?php echo CHtml::checkBox('is_active_exhibition',  !isset($_GET['is_active_exhibition']) || $_GET['is_active_exhibition'] == 1  ? true : false, array('id'=>'ch_isActiveExhibition','style'=>"float: left; margin-right: 15px;")); ?>
            <span>Выставки</span>
        </div>






<!--ФИЛЬТР-->
        <?php echo CHtml::form('/event/products', 'get', array('id'=>'filter-form')); ?>
        <?php echo CHtml::hiddenField('date',isset($_GET['date']) ? $_GET['date'] : ''); ?>
        <?php echo CHtml::hiddenField('is_active_stock',isset($_GET['is_active_stock']) ? $_GET['is_active_stock'] : '1'); ?>
        <?php echo CHtml::hiddenField('is_active_divination',isset($_GET['is_active_divination']) ? $_GET['is_active_divination'] : '1'); ?>
        <?php echo CHtml::hiddenField('is_active_exhibition',isset($_GET['is_active_exhibition']) ? $_GET['is_active_exhibition'] : '1'); ?>
        <?php echo CHtml::hiddenField('q',isset($_GET['q']) ? $_GET['q'] : ''); ?>

        <div class="b-regions">
                <div id="itemCountry" style="margin-bottom: 10px;">
                    <div style="float: left; width: 60px; padding-top: 4px;">Страна:</div>
                    <?php echo CHtml::dropDownList(
                        'country',
                        isset($_GET['country']) ? $_GET['country'] : '' ,
                        Countries::getList(),
                        array('id'=>'f_country', 'empty'=>'- Выберите страну -')
                    ) ?>
                </div>
            </div>

        <?php if(isset($_GET['country']) && !empty($_GET['country'])): ?>
            <div id="itemRegion" style="padding-bottom: 10px;">
                <div style="float: left; width: 60px; padding-top: 4px;">Регион:</div>
                <?php echo CHtml::dropDownList(
                    'region',
                    isset($_GET['region']) ? $_GET['region'] : '' ,
                    Regions::getListDDByCountry($_GET['country']),
                    array('id'=>'f_region', 'empty'=>'- Выберите регион -')
                ) ?>
            </div>
        <?php endif; ?>

        <?php if(isset($_GET['country']) && isset($_GET['region']) && !empty($_GET['country']) && !empty($_GET['region'])): ?>
            <div id="itemCity" style="padding-bottom: 10px;">
                <div style="float: left; width: 60px; padding-top: 4px;">Город:</div>
                <?php echo CHtml::dropDownList(
                    'city',
                    isset($_GET['city']) ? $_GET['city'] : '' ,
                    Cities::getListDDByRegion($_GET['region']),
                    array('id'=>'f_city', 'empty'=>'- Выберите город -')
                ) ?>
            </div>
        <?php endif; ?>

        <?php if(!isset($_GET['is_active_stock']) || $_GET['is_active_stock'] == 1): ?>
            <div class="b-attributes">
                <div class="bb-filter">Акции</div>
                <?php if( count($attrStock) ): ?>
                    <div class="attrA attr" id="attrA">
                        <ul>
                            <p class="title">Тип акции</p>
                            <?php foreach( $attrStock as $attr ): ?>
                                <li>
                                    <?php echo CHtml::checkBox('stock[]', $attr->checkedOnFilter, array('value'=>$attr->id, 'id'=>"stock_{$attr->id}")); ?>
                                    <?php echo CHtml::link(CHtml::label($attr->title, "stock_{$attr->id}"),'javascript:void(0)',array('class'=>'submit_form', 'ch'=>"stock_{$attr->id}")); ?>
                                </li>
                            <?php endforeach; ?>
                        </ul>
                        <div class="multy">
                            <?php echo CHtml::link('применить','javascript:void(0)',array('class'=>'apply_multy submit', 'at'=>'attrA')); ?>
                            ↑ <?php echo CHtml::link('Несколько значений','javascript:void(0)',array('class'=>'show_multy', 'at'=>'attrA')); ?>
                        </div>
                    </div>


<!--                    --><?php //echo CHtml::link('↑ Несколько значений','javascript:void(0)',array('class'=>'several-values')); ?>
                <?php endif; ?>
            </div>
<!--            --><?php //echo CHtml::link('↑ Применить','javascript:void(0)',array('class'=>'submit')); ?>
        <?php endif; ?>

        <?php if(!isset($_GET['is_active_divination']) || $_GET['is_active_divination'] == 1): ?>
            <div class="b-attributes">
                <?php if( count($attrDivination) ): ?>
                        <p class="bb-filter">Гадания</p>
                            <?php foreach( $attrDivination as $attr ): ?>
                                <?php if(!count($attr->Values)) continue; ?>
                                <div class="attr<?php echo $attr->id; ?> attr" id="attr<?php echo $attr->id; ?>">
                                    <ul>
                                    <p class="title"><?php echo $attr->title; ?></p>
                                        <?php foreach($attr->Values as $value): ?>
                                            <li>
                                                <?php echo CHtml::checkBox("divination[{$attr->id}][]", $value->checkedOnFilter, array('value'=>$value->id, 'id'=>"divination_{$value->id}")); ?>
                                                <?php echo CHtml::link(CHtml::label($value->value_string, "divination_{$value->id}"),'javascript:void(0)',array('class'=>'submit_form', 'ch'=>"divination_{$value->id}")); ?>

                                            </li>
                                        <?php endforeach; ?>
                                    </ul>
                                    <div class="multy">
                                        <?php echo CHtml::link('применить','javascript:void(0)',array('class'=>'apply_multy submit', 'at'=>"attr{$attr->id}")); ?>
                                        ↑ <?php echo CHtml::link('Несколько значений','javascript:void(0)',array('class'=>'show_multy', 'at'=>"attr{$attr->id}")); ?>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                    </div>
                <?php endif; ?>
            </div>
        <?php endif; ?>

        <?php if(!isset($_GET['is_active_exhibition']) || $_GET['is_active_exhibition'] == 1): ?>
            <div class="b-attributes">
                <p class="bb-filter">Выставки</p>
                <?php if( count($attrExhibition) ): ?>
                        <?php foreach( $attrExhibition as $attr ): ?>
                            <?php if(!count($attr->Values)) continue; ?>
                            <div class="attr<?php echo $attr->id; ?> attr" id="attr<?php echo $attr->id; ?>">
                            <ul>
                                <p class="title"><?php echo $attr->title; ?></p>
                                <?php foreach($attr->Values as $value): ?>
                                    <li>
                                        <?php echo CHtml::checkBox("exhibition[{$attr->id}][]", $value->checkedOnFilter, array('value'=>$value->id, 'id'=>"exhibition_{$value->id}")); ?>
                                        <?php echo CHtml::link(CHtml::label($value->value_string, "exhibition_{$value->id}"),'javascript:void(0)',array('class'=>'submit_form', 'ch'=>"exhibition_{$value->id}")); ?>
                                    </li>
                                <?php endforeach; ?>
                            </ul>
                            <div class="multy">
                                <?php echo CHtml::link('применить','javascript:void(0)',array('class'=>'apply_multy submit', 'at'=>"attr{$attr->id}")); ?>
                                ↑ <?php echo CHtml::link('Несколько значений','javascript:void(0)',array('class'=>'show_multy', 'at'=>"attr{$attr->id}")); ?>
                            </div>
                            </div>
                        <?php endforeach; ?>
                <?php endif; ?>
            </div>
        <?php endif; ?>
        <div class="clear"></div>
        <div class='button_main' style="float: right">
        <?php echo CHtml::link('Сбросить',array('/event/products'), array('class'=>'button')); ?>
        </div>
        </div>

        <?php echo CHtml::endForm(); ?>
<!--КОНЕЦ ФИЛЬТРА-->
    </td>
</table>


<script type="text/javascript">
    new Calendar({container :'calendar_container', city: 'event/products/date', first_year: 2004, language: '', 'start_year' : <?php echo $year;?>, 'start_month' : <?php echo $month;?>, 'active_day' : <?php echo $day;?>});
    $('.calendar').live('click',function(){
        $('#date').val($(this).attr('rel'));
        $('#filter-form').submit();
        return false;
    });

    $('#ch_isActiveStock').change(function(){
        var active = 1;
        if (!$(this).is(':checked')) { active = 0; } else active = 1;
        $('#is_active_stock').val(active);
        $('#filter-form').submit();

        console.log(active);
    });

    $('#ch_isActiveDivination').change(function(){
        var active = 1;
        if (!$(this).is(':checked')) { active = 0; } else active = 1;
        $('#is_active_divination').val(active);
        $('#filter-form').submit();
    });

    $('#ch_isActiveExhibition').change(function(){
        var active = 1;
        if (!$(this).is(':checked')) { active = 0; } else active = 1;
        $('#is_active_exhibition').val(active);
        $('#filter-form').submit();
    });


    $('#f_country').change(function(){
        if($('#f_region').length>0)  $('#f_region').get(0).selectedIndex = 0;
        if($('#f_city').length>0)    $('#f_city').get(0).selectedIndex = 0;
        $('#filter-form').submit();
    });
    $('#f_region').change(function(){
        if($('#f_city').length>0)    $('#f_city').get(0).selectedIndex = 0;
        $('#filter-form').submit();
    });
    $('#f_city').change(function(){
        $('#filter-form').submit();
    });

    $('.submit_form').click(function(){
        $("#"+$(this).attr("ch")).attr('checked', $("#"+$(this).attr("ch")).is(':checked') == true ? false : true);
        $('#filter-form').submit();
    });

    $('.submit').click(function(){
        $('#filter-form').submit();
    });
    $('#find-text-button').click(function(){
        $('#q').val($('#find-text').val());
        $('#filter-form').submit();
    });
    $('.show_multy').click(function(){
        var id = $(this).attr('at');
        $('#'+id).find('.apply_multy').hide();
        $('#'+id).find('.show_multy').show();

        $('#'+id+' :input[type=checkbox]').fadeIn(100);

        $('#'+id+' .show_multy').hide();
        $('#'+id+' .apply_multy').show();
        return false;
    })

</script>