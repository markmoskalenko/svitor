<div class="b-filter">
    <?php if(!empty($filter)):?>
    <ul>
    <?php
    foreach($filter as $filt):?>
        <li>
            <table>
                <tbody>
                <tr>
                    <td class="remove">
                        <a onclick="events.removeFilter(this.getAttribute('value'));" value="<?php echo $filt['url'];?>">×</a>
                    </td><td><b><?php echo $filt['title'];?></b>
                </td>
                </tr>
                </tbody>
            </table>
        </li>
    <?php endforeach;?>
    </ul>
    <div class="reset_all">↑ <a onclick="events.removeAllFilter();">убрать все параметры</a></div>
    <?php else:?>
    Выберите необходимые параметры
    <?php endif;?>
</div>
<div style="padding: 7px;">
    <input onchange="events.check(this.checked,'a',1);" type="checkbox" <?php if(!$srockParam) echo "checked";?> name="stock" style="float: left; margin-right: 15px;"><span>Акции</span>
</div>
<div style="padding: 7px;">
    <input onchange="events.check(this.checked,'a',2);" type="checkbox" <?php if(!$divinationParam) echo "checked";?> name="divination" style="float: left; margin-right: 15px;"><span>Гадания</span>
</div>
<div style="padding: 7px;">
    <input onchange="events.check(this.checked,'a',3);" type="checkbox" <?php if(!$exhibitionParam) echo "checked";?> name="exhibition" style="float: left; margin-right: 15px;"><span>Выставки</span>
</div>
<br>
<div class="b-regions">
    <?php
    // Если редактируется категория, у которой имеются дочерние разделы, то убираем из формы "Родитель" и "Сайты".
    if (!empty($countries)): ?>
    <div id="itemCountry" style="margin-bottom: 10px;">
        <div style="float: left; width: 60px; padding-top: 4px;">Страна:</div>
        <select style="width: 170px;" id="country" name="country" onchange="events.addRegion(this);">
            <option value="0">--- не выбрано ---</option>
            <?php foreach ($countries as $cat): ?>
            <option <?php if(!empty($c) && $c == $cat['country_id']){echo "selected";}?> value="<?php echo $cat['country_id'];?>"><?php echo $cat['name'];?></option>
            <?php endforeach;?>
        </select>
    </div>
    <?php endif; ?>
    <?php if (!empty($regions)): ?>
    <div id="itemRegion" style="padding-bottom: 10px;">
        <div style="float: left; width: 60px; padding-top: 4px;">Регион:</div>
        <select style="width: 170px;" id="region" name="region" onchange="events.addSity();">
            <option value="0">--- не выбрано ---</option>
            <?php foreach ($regions as $reg): ?>
                <option <?php if(!empty($g) && $g == $reg['region_id']){echo "selected";}?> value="<?php echo $reg['region_id'];?>"><?php echo $reg['name'];?></option>
                <?php endforeach; ?>
        </select>
    </div>
    <?php endif; ?>
    <?php if (!empty($cities)): ?>
    <div id="itemCity" style="padding-bottom: 10px;">
        <div style="float: left; width: 60px; padding-top: 4px;">Город:</div>
        <select style="width: 170px;" id="sity" name="sity" onchange="events.addISity();">
            <option value="0">--- не выбрано ---</option>
            <?php foreach ($cities as $cit):?>
                <option <?php if(!empty($i) && $i == $cit['city_id']){echo "selected";}?>  value="<?php echo $cit['city_id'];?>"><?php echo $cit['name'];?></option>
                <?php endforeach;?>
        </select>
    </div>
    <?php endif; ?>
</div>
<?php if(!isset($divinationParam) || $divinationParam == 0):?>
<div class="b-attributes">
    <?php
    foreach($divination as $item):
        $key = 0;
        foreach($item->Values as $value){
            if(!empty($d) && in_array($value['id'],$d)){
                    $key = 1;
            }
        }
        if(!$key):
    ?>
    <?php if(!isset($gad)):?>
    <div class="bb-filter">Гадания</div>
    <?php endif;?>
    <div class="attr<?php echo $item['id'];?>" id="attr<?php echo $item['id'];?>">
        <div class="title"><?php echo $item['title'];?></div>
        <ul>
            <?php foreach($item->Values as $value):
            ?>
            <li>
                <input type="checkbox" value="<?php echo $value['id'];?>" name="d[]">
                <a onclick="events.checkOther(this.getAttribute('value'),'d[]');" value="<?php echo $value['id'];?>"><?php echo $value['value_string'];?></a>
            </li>
            <?php endforeach;?>
        </ul>
        <div class="multy">
            ↑ <a class="show_multy" onclick="events.showMulty('attr<?php echo $item['id'];?>'); return false;">несколько значений</a>
            <a class="apply_multy" onclick="events.applyMulty('attr<?php echo $item['id'];?>'); return false;">применить</a>
        </div>
        <br>
    </div>
    <?php
        if(!isset($gad)){$gad=1;}
        endif;
        endforeach;
    ?>
</div>
<?php endif;?>
<?php if(!isset($exhibitionParam) || $exhibitionParam == 0):?>
<div class="b-attributes">
    <?php
    foreach($exhibition as $item):
        $keyE = 0;
        foreach($item->Values as $value){
            if(!empty($e) && in_array($value['id'],$e)){
                $keyE = 1;
            }
        }
        if(!$keyE):
            ?>
            <?php if(!isset($ex)):?>
            <div class="bb-filter">Выставки</div>
            <?php endif;?>
            <div class="attr<?php echo $item['id'];?>" id="attr<?php echo $item['id'];?>">
                <div class="title"><?php echo $item['title'];?></div>
                <ul>
                    <?php foreach($item->Values as $value):?>
                    <li>
                        <input type="checkbox" value="<?php echo $value['id'];?>" name="e[]">
                        <a onclick="events.checkOther(this.getAttribute('value'),'e[]');" value="<?php echo $value['id'];?>"><?php echo $value['value_string'];?></a>
                    </li>
                    <?php endforeach;?>
                </ul>
                <div class="multy">
                    ↑ <a class="show_multy" onclick="events.showMulty('attr<?php echo $item['id'];?>'); return false;">несколько значений</a>
                    <a class="apply_multy" onclick="events.applyMulty('attr<?php echo $item['id'];?>'); return false;">применить</a>
                </div>
                <br>
            </div>
            <?php
            if(!isset($ex)){$ex=1;}
        endif;
    endforeach;
    ?>
</div>
<?php endif;?>
<?php foreach($stock as $item){
    $keyS = 0;
    if(!empty($s) && $s != $item['id']){
        $keyS = 1;
    }
}
if(!$keyS):
?>
<?php if(!isset($srockParam) || $srockParam == 0):?>
<div class="bb-filter">Акции</div>
<div class="b-attributes">
    <div class="attrA" id="attrA">
        <ul>
            <?php foreach($stock as $item):?>
            <li>
                <input type="checkbox" value="<?php echo $item['id'];?>" name="s[]">
                <a onclick="events.checkOther(this.getAttribute('value'),'s[]');" value="<?php echo $item['id'];?>"><?php echo $item['title'];?></a>
            </li>
            <?php endforeach;?>
        </ul>
        <div class="multy">
            ↑ <a class="show_multy" onclick="events.showMulty('attrA'); return false;">несколько значений</a>
            <a class="apply_multy" onclick="events.applyMulty('attrA'); return false;">применить</a>
        </div>
        <br>
    </div>
</div>
<?php endif;?>
<?php endif;?>
<script src="/static/js/m_events.js"></script>

