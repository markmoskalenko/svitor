<div class="content" id="userPhotoEdit">
    <div id="msg"></div>
    <form onsubmit="return false;">
        <div id="photo_description">
            <?php
            $image->description = CHtml::decode($image->description);
            echo CHtml::activeTextarea($image, 'description', array(
                'style' => 'margin-top: 15px; width: 98%; height: 80px;',
                'onkeyup' => 'formHelper.updateCounter(this, \'photo_description\', 500);',
                'onclick' => 'formHelper.updateCounter(this, \'photo_description\', 500);',
                'onchange' => 'formHelper.updateCounter(this, \'photo_description\', 500);',
            ));
            ?>
            <span class="counter note"></span>
        </div>
    </form>
</div>
<div class="buttons">
    <div class="button_main">
        <button type="button" onclick="photos.save(this, <?php echo $image->id; ?>);">Сохранить</button>
    </div>
    <div class="button_grey">
        <button onclick="$.fancybox.close();">Отменить</button>
    </div>
</div>