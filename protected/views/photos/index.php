<div class="photos_title">
    Фотоальбом
    <?php $this->widget('application.widgets.FunBar', array('short_mode' => true)); ?>
</div>

<?php
if (mApi::getFunId()):
    $url = '/checklist/index/' . Yii::app()->request->baseUrl;
?>

<div class="simple_add_button">
    <a id="addImgButton"<?php if($images_count >= $max_images) echo ' style="display:none;"'; ?>>Добавить фотографию</a> <span id="loader"></span>
</div>

<div class="photos_thumbs" id="photos_thumbs">
    <?php
    if (!empty($images))
        foreach ($images as $img)
        {
            echo '<div class="img" id="img_'.$img['id'].'">
            <a class="upbox-button" rel="upbox-images" href="'.Yii::app()->ImgManager->getUrlById($img->img_id, 'large', $img->img_filename).'" title="' . $img->description . '">
            <img src="'.Yii::app()->ImgManager->getUrlById($img->img_id, 'medium', $img->img_filename).'"></a>
            <a class="delete magic_button" onclick="photos.remove(this, '.$img['id'].'); return false;" onmouseover="tooltip.show(this, \'Удалить\', {position:\'top-right\'});"></a>
            <br><a onclick="photos.edit('.$img['id'].'); return false;">редактировать</a>
            </div>';
        }
    ?>
</div>
<div class="photos_counter">
Загружено <span id="images_counter"><span><?php echo $images_count; ?></span> из <?php echo $max_images; ?> возможных.</span>
</div>

<script>
$(document).ready(function () {
    photos.init(<?php echo $images_count; ?>, <?php echo $max_images; ?>);
});
</script>
<script src="/static/js/ajaxupload.js"></script>
<script src="/static/js/m_photos.js"></script>

<?php else:
    $this->widget('application.widgets.SelectFun');
endif;
?>