<div class="row title_cart">Заказ №<?php echo $model->id;?></div>

<div style="display: none" id="vendor_<?php echo $model->vendor->id ?>">
    <div class="b-upbox">
        <div class="b-upbox_title">
            <div class="title">Условия доставки компании - <?php echo $model->vendor->title; ?></div>
        </div>
        <div class="content" id="weddingForm" style="padding: 20px 20px;">
            <?php echo $model->vendor->terms_delivery; ?>
        </div>
    </div>

</div>

<div class="right_cart">
    <?php if(!empty($model->vendor->terms_delivery)): ?>
        <?php echo CHtml::link('Условия доставки', 'javascript:void(0)', array('class'=>'delivery', 'divid'=>'vendor_'. $model->vendor->id)) ?>
    <?php else: ?>
        Компания не указала условия доставки товара.
    <?php endif; ?>
</div>
<div class="row title_cart">Продавец: Компания <?php echo CHtml::link($model->vendor->title, $this->createUrl('/vendor/profile',array( 'id' => $model->vendor->id ))); ?></div>
<?php
    $this->widget('zii.widgets.grid.CGridView', array(
         'dataProvider' => new CArrayDataProvider($model->bondOrders),
         'id'=>'grid-products',
         'htmlOptions'  => array('class' => 'grid-view', 'id'=>'grid-products'),
         'afterAjaxUpdate'=>'function(id, data) { setInlineEdit(); }',
         'columns'      => array(

             array(
                 'header'      => 'Товары:',
                 'type'        => 'raw',
                 'value'       => '!empty($data->product->FirstImage->img_id) ? CHtml::image(Yii::app()->ImgManager->getUrlById($data->product->FirstImage->img_id, "medium", $data->product->FirstImage->img_filename)) : ""',
                 'htmlOptions' => array('width' => '100px')
             ),
             array(
                 'header' => '',
                 'value'  => '$data->product->title'
             ),
             array(
                 'header' => 'Количество:',
                 'type'=>'raw',
                 'value'  =>'$data->count',
                 'htmlOptions' => array('width' => '50px')
             ),
             array(
                 'header' => 'Цена, руб.:',
                 'value'  => '$data->price." / шт."'
             ),
             array(
                 'header' => 'Сумма, руб.:',
                 'value'  => '$data->price * $data->count'
             ),
         ),
    ));
?>

    <div class="delivery_wrapper" style="padding: 20px 112px;text-align:right; ">
	<div>
	Стоимость доставки, руб.: <?php echo $model->delivery; ?>
	</div>
	<br/>
	<div>
Сумма, руб.: <?php echo $model->sum + $model->delivery;?>
</div>
</div>
<div class="button_main">
<?php echo CHtml::link('&larr; Вернуться к списку заказов', $this->createUrl('/cart/myOrders'),array('class'=>'button')) ?>
</div>
<script type="text/javascript">
    $('a.delivery').click(function(){
        upBox.showContent($('#'+$(this).attr('divid')).html());
    });
</script>