<?php if(count($orders)): ?>
<?php foreach($orders as $order): ?>
    <div class="right_cart">
		<?php if(!empty($order['vendor']->terms_delivery)): ?>
		<?php echo CHtml::link('Условия доставки', 'javascript:void(0)', array('class'=>'delivery', 'divid'=>'vendor_'. $order['vendor']->id)) ?>
		<?php else: ?>
		  Компания не указала условия доставки товара.
		<?php endif; ?>
	</div>
    <div class="row title_cart">Продавец: Компания <?php echo CHtml::link($order['vendor']->title, $this->createUrl('/vendor/profile',array( 'id' => $order['vendor']->id ))); ?></div>
    <div style="display: none" id="vendor_<?php echo $order['vendor']->id ?>">
        <div class="b-upbox">
            <div class="b-upbox_title">
                <div class="title">Условия доставки компании - <?php echo $order['vendor']->title; ?></div>
            </div>
            <div class="content" id="weddingForm" style="padding: 20px 20px;">
                <?php echo $order['vendor']->terms_delivery; ?>
            </div>
        </div>
    </div>
    <?php
    $this->widget('zii.widgets.grid.CGridView', array(
                                                     'dataProvider' => new CArrayDataProvider( $order['dataProvider'] ),
                                                     'columns' => array(
                                                         array(
                                                             'header'      => 'Товары:',
                                                             'type'        => 'raw',
                                                             'value'       => '!empty($data->FirstImage->img_id) ? CHtml::image(Yii::app()->ImgManager->getUrlById($data->FirstImage->img_id, "medium", $data->FirstImage->img_filename)) : ""',
                                                             'htmlOptions' => array('width' => '100px')
                                                         ),
                                                         array(
                                                             'header' => '',
                                                             'value'  => '$data->title'
                                                         ),
                                                         array(
                                                             'header' => 'Количество:',
                                                             'type'   => 'raw',
                                                             'value'  => 'CHtml::link(" + ",Yii::app()->createUrl("/cart/addCart", array("id"=>$data->id))) .$data->getQuantity() . CHtml::link(" - ",Yii::app()->createUrl("/cart/updateCart", array("id"=>$data->id,"quantity"=>($data->getQuantity()-1))))'
                                                         ),
                                                         array(
                                                             'header' => 'Цена,руб.:',
                                                             'value'  => '$data->price."/шт."'
                                                         ),
                                                         array(
                                                             'header' => 'Сумма,руб.:',
                                                             'value'  => '$data->getSumPrice()'
                                                         ),
                                                        array(
                                                            'class' => 'CButtonColumn',
                                                            'template'=>'{delete}',
                                                            'buttons'=>array
                                                            (
                                                                'delete' => array
                                                                (
                                                                    'url'=>'Yii::app()->createUrl("/cart/deleteCart",array("id"=>$data->id))',
                                                                ),

                                                            )
                                                        ),
                                                     ),
                                                ));
    ?>
<div class="bottom_info">
    <div class="total">Сумма, руб.: <?php echo $order['price']; ?> </div>
    <div class="button_main"><?php echo CHtml::link('Заказать все у продавца', $this->createUrl('/cart/createOrder',array('vendor_id'=>$order['vendor']->id)),array('class'=>'button')); ?></div>
</div>

    <?php endforeach; ?>
<script type="text/javascript">
        $('a.delivery').click(function(){
            upBox.showContent($('#'+$(this).attr('divid')).html());
        });
</script>
<?php else: ?>
  Корзина пуста
<?php endif; ?>