<?php
    $this->widget('zii.widgets.grid.CGridView', array(
         'dataProvider' => new CArrayDataProvider($model->bondOrders),
         'id'=>'grid-products',
         'htmlOptions'  => array('class' => 'grid-view', 'id'=>'grid-products'),
         'afterAjaxUpdate'=>'function(id, data) { setInlineEdit(); }',
         'columns'      => array(

             array(
                 'header'      => 'Изображение',
                 'type'        => 'raw',
                 'value'       => '!empty($data->product->FirstImage->img_id) ? CHtml::image(Yii::app()->ImgManager->getUrlById($data->product->FirstImage->img_id, "medium", $data->product->FirstImage->img_filename)) : ""',
                 'htmlOptions' => array('width' => '100px')
             ),
             array(
                 'header' => 'Товары',
                 'value'  => '$data->product->title'
             ),
             array(
                 'header' => 'Количество',
                 'type'=>'raw',
                 'value'  => 'CHtml::link(" + ", "javascript:void(0)", array("onClick"=>"add_p($data->order_id, $data->id, true, $(this))"))
."<span>".$data->count."</span>".CHtml::link(" - ", "javascript:void(0)", array("onClick"=>"add_p($data->order_id, $data->id, false, $(this))"))',
                 'htmlOptions' => array('width' => '50px')
             ),
             array(
                 'header' => 'Цена, руб.',
                 'value'  => '$data->price." / шт."'
             ),
             array(
                 'header' => 'Сумма, руб.',
                 'value'  => '$data->price * $data->count'
             ),

             array(
                 'class'=>'CButtonColumn',
                 'template'=>'{delete}',
                 'buttons'=>array
                 (
                     'delete' => array
                     (
                         'url'=>'Yii::app()->createUrl("/cart/deleteProduct", array("id"=>$data->id))',
                     ),
                 )

             ),
         ),
    ));
?>
<?php echo CHtml::link('Перейти к подтверждению заказа', $this->createUrl('/cart/confirmOrder', array('order_id'=>$model->id))) ?>
<script type="text/javascript">
    function add_p( order_id, product_id, increment, trigger ){
        var value = trigger.parent().find('span').text() ;
        if(increment){
            ++value;
        }else if(value>1){
            --value;
        }else return false;
        $.post('/cart/saveProduct',
          { 'order_id' : order_id, 'count' : value, 'product_id' : product_id }, function( data ) {
              $.fn.yiiGridView.update("grid-products");
        });
    }
</script>