<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'orders-form',
	'enableAjaxValidation'=>false,
)); ?>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'surname'); ?>
		<?php echo $form->textField($model,'surname',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'surname'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'name'); ?>
		<?php echo $form->textField($model,'name',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'middle_name'); ?>
		<?php echo $form->textField($model,'middle_name',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'middle_name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'phone'); ?>
		<?php echo $form->textField($model,'phone',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'phone'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'address'); ?>
		<?php echo $form->textField($model,'address',array('size'=>60,'maxlength'=>200)); ?>
		<?php echo $form->error($model,'address'); ?>
	</div>

    <?php echo $form->hiddenField($model,'user_id',array('size'=>20,'maxlength'=>20)); ?>
    <?php echo $form->hiddenField($model,'status'); ?>
    <?php echo $form->hiddenField($model,'delivery'); ?>
    <?php echo $form->hiddenField($model,'vendor_id',array('size'=>20,'maxlength'=>20)); ?>

	<div class="row buttons">
		<div class="button_main"><?php echo CHtml::link('&larr; Вернуться', $this->createUrl('/cart/orders'),array('class'=>'button')); ?></div>  <div class="button_main"><?php echo CHtml::submitButton('Далее →',array('class'=>'button')); ?></div>
	</div>

<?php $this->endWidget(); ?>

</div>