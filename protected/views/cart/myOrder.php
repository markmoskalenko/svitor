<div class="row filter_link">
    <span>Фильтр:</span>
    <?php echo CHtml::link(
        'Заказано',
        $this->createUrl('/cart/myOrders'),
        array('class' => (Yii::app()->request->getParam('status') != Orders::STATUS_CLOSED ? 'active' : ''))
    ); ?>
    <?php echo CHtml::link(
        'Закрыто',
        $this->createUrl('/cart/myOrders', array('status' => Orders::STATUS_CLOSED)),
        array('class' => (Yii::app()->request->getParam('status') == Orders::STATUS_CLOSED ? 'active' : ''))
    ); ?>
</div>
<?php
$this->widget(
    'zii.widgets.grid.CGridView',
    array(
         'dataProvider' => new CArrayDataProvider($model),
         'htmlOptions'  => array('class' => 'grid-view'),
         'columns'      => array(
             array(
                 'name'        => 'id',
                 'header'      => 'Заказ №:',
                 'type'        => 'raw',
                 'value'       => 'CHtml::link("№".$data->id,Yii::app()->createUrl("/cart/viewOrder", array("id"=>$data->id)))',
                 'htmlOptions' => array(
                     'width' => '55px',
                 ),
             ),
             array(
                 'header'      => 'Продавец:',
                 'htmlOptions' => array(
                     'width' => '300px',
                 ),
                 'type'        => 'raw',
                 'value'       => '$data->vendor->title',
             ),
             array(
                 'header' => 'Сумма:',
                 'value'  => '$data->sum+$data->delivery;',
             ),
             array(
                 'name'   => 'status',
                 'header' => 'Статус:',
                 'type'   => 'raw',
                 'value'  => 'Orders::getStatusTitle($data->status).($data->status == Orders::STATUS_NOT_CONFIRMED ? " - "."<div class=button_main>".CHtml::link("Подтвердить",Yii::app()->createUrl("/cart/confirmOrder", array("order_id"=>$data->id)),array("class"=>"button"))."</div>" : "")',
                 'filter' => Orders::getStatusOnFilter(),
             ),
         ),
    )
);
?>
