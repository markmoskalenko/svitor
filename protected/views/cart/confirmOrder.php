<div class="row title_cart">Продавец: Компания <?php echo CHtml::link($model->vendor->title, $this->createUrl('/vendor/profile',array( 'id' => $model->vendor->id ))); ?></div>
<div class="row title_cart">Параметры доставки: [ <?php echo CHtml::link('Изменить', $this->createUrl('/cart/editOrderContact',array( 'order_id' => $model->id ))); ?> ]</div>
<div class="row title_cart">Адрес: <?php echo $model->address; ?></div>
<div class="row title_cart">Получатель: <?php echo $model->surname; ?> <?php echo $model->name; ?></div>
<div class="row title_cart">Мобильный телефон: <?php echo $model->phone; ?></div>
<div class="row title_cart">Адрес электронной почты: <?php echo $model->user->email; ?></div>
<div class="row title_cart">Состав заказа: [ <?php echo CHtml::link('Изменить', $this->createUrl('/cart/editOrderProduct', array( 'order_id' => $model->id ))); ?> ]</div>

<?php
$this->widget(
    'zii.widgets.grid.CGridView',
    array(
         'dataProvider' => new CArrayDataProvider($model->bondOrders),
         'htmlOptions'  => array('class' => 'grid-view'),
         'columns'      => array(
             array(
                 'header'      => 'Товары:',
                 'type'        => 'raw',
                 'value'       => '!empty($data->product->FirstImage->img_id) ? CHtml::image(Yii::app()->ImgManager->getUrlById($data->product->FirstImage->img_id, "medium", $data->product->FirstImage->img_filename)) : ""',
                 'htmlOptions' => array('width' => '100px')
             ),
             array(
                 'header' => '',
                 'value'  => '$data->product->title'
             ),
            array(
                 'header' => 'Количество:',
                 'value'  => '$data->count',
                 'htmlOptions' => array('width' => '50px')
             ),
            array(
                'header' => 'Цена, руб.:',
                'value'  => '$data->price." / шт."'
            ),
            array(
                'header' => 'Сумма, руб.:',
                'value'  => '$data->price * $data->count'
            ),

         ),
    )
);
 ?>
<div class="bottom_info">
	<div class="total">Сумма, руб.: <?php echo $model->sum; ?></div>
	<div class="left_position">
		<div class="button_main"><?php echo CHtml::link('Подтвердить заказ', $this->createUrl('/cart/confirmOrder',array('order_id' => $model->id, 'confirm' => 1)),array('class'=>'button')); ?></div>
	</div>
</div>