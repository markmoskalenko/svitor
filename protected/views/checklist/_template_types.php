<div class="content" id="checklistForm">
    <div id="msg"></div>
    <form onsubmit="return false;">
        <div class="c_al checklist_template_types">
            <?php
            $list_data = CHtml::listData($template_types, 'id', 'title');
            echo CHtml::dropDownList('type_id', '', $list_data, array(
                'empty' => '-- выберите тип --',
                'onchange' => 'checklist.loadTemplates(this);',
                'style' => 'width: 300px;'
            ));
            ?>
             <span id="tempLoader"></span>
        </div>
    </form>
</div>
<div id="templates"></div>