<?php if (!empty($data)): ?>

<?php if ($show_by == 'date'): ?>
    <tr class="thead">
        <th class="c_al status"></th>
        <th class="c_al th_note"></th>
        <th class="l_al th_task">Задача</th>
        <th class="c_al more"></th>
        <th class="l_al date">Дата</th>
        <th class="l_al editor"></th>
    </tr>

    <?php foreach ($data as $task): ?>

        <tr class="task" id="task_<?php echo $task->id; ?>">

            <td class="c_al b_va status">
                <input type="checkbox" name="task_<?php echo $task->id; ?>"
                       onclick="checklist.changeFolder(this, <?php echo $task->id; ?>, 'todo');"
                       value="<?php echo $task->folder; ?>" checked="checked">
            </td>

            <td class="c_al th_note">
                <?php if (!empty($task->note)): ?>
                <div class="note"
                     onmouseover="tooltip.show(this, '<?php echo $task->note; ?>', {special_class : 'white'});"></div>
                <?php endif; ?>
            </td>

            <td class="l_al task_desc" style="text-decoration: line-through;">
                <?php
                if (!empty($task->link)) $task->task = '<a href="' . $task->link . '">' . $task->task . '</a>';
                ?>
                <div class="task_text"><?php echo $task->task; ?></div>
            </td>

            <td class="c_al more">
                <?php if (!empty($task->budget_id)): ?>
                <a class="active_budget"
                   onclick="upBox.ajaxLoad('/budget/edit/id/<?php echo $task->budget_id; ?>'); return false"
                   onmouseover="tooltip.show(this, 'Редактировать строку бюджета', {position: 'top-right'});">$</a>
                <?php endif; ?>
            </td>

            <td class="l_al date">
                <?php echo UserChecklist::getTaskDate($task->date); ?>
            </td>

            <td class="editor">
                <div class="editor_links">
                    <a class="edit magic_button" onclick="checklist.editTask(<?php echo $task->id; ?>); return false;"
                       onmouseover="tooltip.show(this, 'Редактировать', {position :'top-right'});"></a>
                    <a class="trash magic_button"
                       onclick="checklist.changeFolder(this, <?php echo $task->id; ?>, 'trash'); return false;"
                       onmouseover="tooltip.show(this, 'В корзину', {position :'top-right'});"></a>
                </div>
            </td>
        </tr>

        <?php endforeach; ?>


    <?php elseif ($show_by == 'category'): ?>

    <?php foreach ($data as $cat): ?>

        <tr class="thead">
            <th class="c_al status"></th>
            <th class="c_al th_note"></th>
            <th class="l_al th_task"><?php echo $cat['title']; ?></th>
            <th class="c_al more"></th>
            <th class="l_al date">Дата</th>
            <th class="l_al editor"></th>
        </tr>

        <?php foreach ($cat['UserChecklist'] as $task): ?>

            <tr class="task" id="task_<?php echo $task->id; ?>">

                <td class="c_al b_va status">
                    <input type="checkbox" name="task_<?php echo $task->id; ?>"
                           onclick="checklist.changeFolder(this, <?php echo $task->id; ?>, 'todo');"
                           value="<?php echo $task->folder; ?>" checked="checked">
                </td>

                <td class="c_al th_note">
                    <?php if (!empty($task->note)): ?>
                    <div class="note"
                         onmouseover="tooltip.show(this, '<?php echo $task->note; ?>', {special_class : 'white'});"></div>
                    <?php endif; ?>
                </td>

                <td class="l_al task_desc" style="text-decoration: line-through;">
                    <?php
                    if (!empty($task->link)) $task->task = '<a href="' . $task->link . '">' . $task->task . '</a>';
                    ?>
                    <div class="task_text"><?php echo $task->task; ?></div>
                </td>

                <td class="c_al more">
                    <?php if (!empty($task->budget_id)): ?>
                    <a class="active_budget"
                       onclick="upBox.ajaxLoad('/budget/edit/id/<?php echo $task->budget_id; ?>'); return false"
                       onmouseover="tooltip.show(this, 'Редактировать строку бюджета', {position: 'top-right'});">$</a>
                    <?php endif; ?>
                </td>

                <td class="l_al date">
                    <?php echo UserChecklist::getTaskDate($task->date); ?>
                </td>

                <td class="editor">
                    <div class="editor_links">
                        <a class="edit magic_button"
                           onclick="checklist.editTask(<?php echo $task->id; ?>); return false;"
                           onmouseover="tooltip.show(this, 'Редактировать', {position :'top-right'});"></a>
                        <a class="trash magic_button"
                           onclick="checklist.changeFolder(this, <?php echo $task->id; ?>, 'trash'); return false;"
                           onmouseover="tooltip.show(this, 'В корзину', {position :'top-right'});"></a>
                    </div>
                </td>
            </tr>

            <?php endforeach; ?>
        <?php endforeach; ?>

    <?php endif; ?>
<tr class="empty_item js_empty_item">
    <td class="l_al task_desc">Нет выполненных задач...</td>
</tr>

<?php else: ?>
<tr class="empty_item">
    <td class="l_al task_desc">Нет выполненных задач...</td>
</tr>
<?php endif; ?>