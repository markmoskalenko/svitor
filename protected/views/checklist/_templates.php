
<?php if (!empty($templates)): ?>
    <div class="checklist_templates">
    <form onsubmit="return false;">
        <?php foreach($templates as $cat): ?>

            <div class="checklist_templates_cat" id="temp_cat_<?php echo $cat['id']; ?>">
            <div class="cat_name">
                <label>
                <input type="checkbox" name="select_all" value="" onclick="$('#temp_cat_<?php echo $cat['id']; ?> :checkbox').attr('checked', $(this).is(':checked')); checklist.selectTempTask(this);">
                <?php echo $cat->title; ?>
                </label>
            </div>
            <?php foreach($cat['ChecklistTemplates'] as $temp): ?>

                <div class="task">
                    <label>
                        <input type="checkbox" name="template[]" value="<?php echo $temp['entry']; ?>" onclick="checklist.selectTempTask(this);">
                        <div class="text"><?php echo $temp->task; ?></div>
                    </label>
                </div>

            <?php endforeach; ?>
            </div>
        <?php endforeach; ?>
    </form>
    </div>
<?php else: ?>
    <div class="checklist_templates_empty">Нет шаблонов для этого типа...</div>
<?php endif; ?>

<div class="buttons">
    <div class="l_fl" id="selected_task_count" style="margin-top: 5px;">
        <span>Выбрано <b>0</b> задач</span>
    </div>
    <div class="button_main" id="add_selected_task">
        <button type="button" onclick="checklist.sendTempForm(this);" disabled="disabled">Добавить выбранные</button>
    </div>
    <div class="button_grey">
        <button onclick="$.fancybox.close();">Отменить</button>
    </div>
</div>