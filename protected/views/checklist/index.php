<link rel="stylesheet" type="text/css" href="/static/timePicker.css">
<div class="checklist_title">
    Список задач
    <?php $this->widget('application.widgets.FunBar', array('short_mode' => true)); ?>
</div>

<?php
if (mApi::getFunId()):
    $url = '/checklist/index/' . Yii::app()->request->baseUrl;
    ?>

<div class="checklist_summary_info magic_button">
    Выполнено <span class="done_counter"><?php echo $done_count; ?></span> из <span
        class="all_counter"><?php echo $data_count; ?></span> задач
</div>

<div class="simple_add_button l_fl">
    <a onclick="checklist.addTask(); return false;">Добавить</a>
</div>

<div class="simple_add_button">
    <a onclick="checklist.showTemplates(); return false;">Выбрать из шаблонов</a>
</div>

<div id="trash-dialog" style="display: none;" title="Переместить корзину">Задача будет перемещена в корзину. Если эта
    задача имеет связь со строкой бюджета, она будет разорвана.<br><br>Вы действительно хотите продолжить?
</div>
<div id="todo-dialog" style="display: none;" title="Восстановить задачу">Вы действительно хотите восстановить эту
    задачу?
</div>

<table class="checklist_filter">
    <td class="filter">
        <ul>
            <li<?php if ($folder == 'all') echo ' class="active"'; ?>>
                <a href="<?php echo Yii::app()->createUrl($url, array('folder' => 'all', 'show_by' => $show_by)); ?>">Все</a>
            </li>
            <li<?php if ($folder == 'todo') echo ' class="active"'; ?>>
                <a href="<?php echo Yii::app()->createUrl($url, array('folder' => 'todo', 'show_by' => $show_by)); ?>">Сделать
                    <b class="todo_counter"><?php echo $data_count - $done_count; ?></b></a>
            </li>
            <li<?php if ($folder == 'done') echo ' class="active"'; ?>>
                <a href="<?php echo Yii::app()->createUrl($url, array('folder' => 'done', 'show_by' => $show_by)); ?>">Выполнено
                    <b class="done_counter"><?php echo $done_count; ?></b></a>
            </li>
            <li<?php if ($folder == 'trash') echo ' class="active"'; ?>>
                <a href="<?php echo Yii::app()->createUrl($url, array('folder' => 'trash', 'show_by' => $show_by)); ?>">Корзина
                    <b class="trash_counter"><?php echo $trash_count; ?></b></a>
            </li>
        </ul>
        <?php if ($trash_count > 0 && $folder == 'trash'): ?>
        <div class="checklist_clean_trash"> &larr; <a onclick="checklist.cleanTrash(this); return false;">очистить</a>
        </div>
        <?php endif; ?>
    </td>
    <td class="simple_sortby checklist_viewby">
        <ul>
            <li<?php if ($show_by == 'date') echo ' class="active"'; ?>>
                <a href="<?php echo Yii::app()->createUrl($url, array('folder' => $folder, 'show_by' => 'date')); ?>">По
                    дате</a>
            </li>
            <li<?php if ($show_by == 'category') echo ' class="active"'; ?>>
                <a href="<?php echo Yii::app()->createUrl($url, array('folder' => $folder, 'show_by' => 'category')); ?>">По
                    группам</a>
            </li>
        </ul>
    </td>
</table>
<table class="simple_table checklist_table">
    <?php
    $this->renderPartial($folder, array('data' => $data, 'show_by' => $show_by));
    ?>
</table>

<script>
    $(document).ready(function () {
        checklist.init('<?php echo $folder; ?>');
    });
</script>
<script src="/static/js/m_checklist.js"></script>
<script src="/static/js/m_checklist_form.js"></script>
<script src="/static/js/m_budget_form.js"></script>

<?php else:
    $this->widget('application.widgets.SelectFun');
endif;
?>
