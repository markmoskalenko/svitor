<div class="content" id="checklistForm">
    <div id="msg"></div>
    <form onsubmit="return false;">
        <div class="label_name">Группа <span class="red_text">*</span></div>
        <div class="field">
            <?php
            $list_data = CHtml::listData($cat_mins, 'id', 'title');
            echo CHtml::dropDownList('UserChecklist[cat_min_id]', isset($model->cat_min_id) ? $model->cat_min_id : '', $list_data, array(
                'empty' => '-- не выбрано --',
                'required' => 'required'
            ));
            ?>
        </div>

        <div class="label_name">Задача <span class="red_text">*</span></div>
        <div class="field">
            <?php
            $model->task = CHtml::decode($model->task);
            echo CHtml::activeTextArea($model, 'task', array(
                'style' => 'width: 98%; height: 60px;',
                'required' => 'required'
            ));
            ?>
        </div>

        <div class="label_name">Дата &#151; время</div>
        <div class="field">
            <input type="text" name="UserChecklist[date]" id="date" style="width: 90px;" placeholder="дд.мм.гггг"
                   value="<?php echo (!$model->isNewRecord && !empty($model->date)) ? date('d.m.Y', $model->date) : ''; ?>">
            <?php
            if (!$model->isNewRecord && !empty($model->date) && date('H:i', $model->date) != '00:00') {
                $model->date = date('H:i', $model->date);
            } else {
                $model->date = '';
            }
            ?>
            <input type="text" name="UserChecklist[time]" id="time" style="width: 50px;" placeholder="чч:мм"
                   value="<?php echo $model->date; ?>">
            <span class="note">(дата <?php echo mApi::getFunType() == 1 ? 'торжества: ' . mApi::getDateWithMonth(mApi::getHoliday()->date, 1, true) : 'свадьбы: ' . mApi::getDateWithMonth(mApi::getWedding()->date, 1, true);?>
                )</span>
        </div>

        <?php if ($model->isNewRecord && empty($model->budget_id)): ?>
        <div class="label_name"></div>
        <div class="field">
            <label onmouseover="tooltip.show(this, 'После добавления этой задачи, сразу появится форма для добавления строки бюджета, которая будет связана с этой задачей.', {position:'top-right', delay:'200', special_class:'white'});"><input
                    type="checkbox" name="link_budget" value="1"> Добавить строку бюджета</label>
        </div>
        <?php endif; ?>

        <div id="box_note"<?php if (empty($model->note)) echo 'style="display:none;"'; ?>>
            <div class="label_name">Примечание</div>
            <div class="field">
                <?php
                $model->note = CHtml::decode($model->note);
                echo CHtml::activeTextArea($model, 'note', array('style' => 'width: 98%; height: 60px;'));
                ?>
            </div>
        </div>
        <div class="label_name"><a onclick="checklist_form.noteSwitch(this); return false;"
                                   class="js_link"><?php echo empty($model->note) ? 'Добавить примечание' : 'Скрыть примечание'; ?></a>
        </div>
    </form>
</div>

<div class="buttons">
    <div class="button_main">
        <?php $add_link = !empty($model->budget_id) ? 'add/budget_id/' . $model->budget_id : 'add'; ?>
        <button type="button"
                onclick="checklist_form.sendForm(this, '<?php echo $model->isNewRecord ? $add_link : 'edit/id/' . $model->id; ?>');"><?php echo $model->isNewRecord ? 'Добавить' : 'Сохранить'; ?></button>
    </div>
    <div class="button_grey">
        <button onclick="$.fancybox.close();">Отменить</button>
    </div>
</div>

<script src="/static/js/timePicker.js"></script>
<script>
    $(document).ready(function () {
        $('#date').datepicker({
            changeYear:true,
            dateFormat:'dd.mm.yy',
            minDate:new Date(<?php echo date('Y') . ',' . date('m') . ' - 1,' . date('d'); ?>),
            maxDate:new Date(2020, 1 - 1, 1),
            monthNames:['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
            monthNamesShort:['янв', 'фев', 'мар', 'апр', 'май', 'июн', 'июл', 'авг', 'сен', 'окт', 'ноя', 'дек'],
            dayNamesMin:["Вс", "Пн", "Вт", "Ср", "Чт", "Пт", "Сб"],
            showAnim:'drop'
        });

        $('#time').timePicker({
            startTime:'00:30'
        });
    });
</script>

