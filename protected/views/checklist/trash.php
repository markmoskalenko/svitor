<?php if (!empty($data)): ?>

<?php if ($show_by == 'date'): ?>

    <tr class="thead">
        <th class="c_al status"></th>
        <th class="c_al th_note"></th>
        <th class="l_al th_task">Задача</th>
        <th class="c_al more"></th>
        <th class="l_al date">Дата</th>
        <th class="l_al editor"></th>
    </tr>

    <?php foreach ($data as $task): ?>

        <tr class="task" id="task_<?php echo $task->id; ?>">

            <td class="c_al b_va status"></td>

            <td class="c_al th_note">
                <?php if (!empty($task->note)): ?>
                <div class="note"
                     onmouseover="tooltip.show(this, '<?php echo $task->note; ?>', {special_class : 'white'});"></div>
                <?php endif; ?>
            </td>

            <td class="l_al task_desc">
                <?php
                if (!empty($task->link)) $task->task = '<a href="' . $task->link . '">' . $task->task . '</a>';
                ?>
                <div class="task_text"><?php echo $task->task; ?></div>
            </td>

            <td class="c_al more">
            </td>

            <td class="l_al date">
                <?php echo UserChecklist::getTaskDate($task->date); ?>
            </td>

            <td class="editor">
                <div class="editor_links">
                    <a class="restore magic_button"
                       onclick="checklist.changeFolder(this, <?php echo $task->id; ?>, 'restore'); return false;"
                       onmouseover="tooltip.show(this, 'Восстановить', {position :'top-right'});"></a>
                    <a class="delete magic_button"
                       onclick="checklist.deleteTask(this, <?php echo $task->id; ?>); return false;"
                       onmouseover="tooltip.show(this, 'Удалить', {position :'top-right'});"></a>
                </div>
            </td>
        </tr>

        <?php endforeach; ?>


    <?php elseif ($show_by == 'category'): ?>

    <?php foreach ($data as $cat): ?>

        <tr class="thead">
            <th class="c_al status"></th>
            <th class="c_al th_note"></th>
            <th class="l_al th_task"><?php echo $cat['title']; ?></th>
            <th class="c_al more"></th>
            <th class="l_al date">Дата</th>
            <th class="l_al editor"></th>
        </tr>

        <?php foreach ($cat['UserChecklist'] as $task): ?>

            <tr class="task" id="task_<?php echo $task->id; ?>">


                <td class="c_al b_va status"></td>

                <td class="c_al th_note">
                    <?php if (!empty($task->note)): ?>
                    <div class="note"
                         onmouseover="tooltip.show(this, '<?php echo $task->note; ?>', {special_class : 'white'});"></div>
                    <?php endif; ?>
                </td>

                <td class="l_al task_desc">
                    <?php
                    if (!empty($task->link)) $task->task = '<a href="' . $task->link . '">' . $task->task . '</a>';
                    ?>
                    <div class="task_text"><?php echo $task->task; ?></div>
                </td>

                <td class="c_al more">
                </td>

                <td class="l_al date">
                    <?php echo UserChecklist::getTaskDate($task->date); ?>
                </td>

                <td class="editor">
                    <div class="editor_links">
                        <a class="restore magic_button"
                           onclick="checklist.changeFolder(this, <?php echo $task->id; ?>, 'restore'); return false;"
                           onmouseover="tooltip.show(this, 'Восстановить', {position :'top-right'});"></a>
                        <a class="delete magic_button"
                           onclick="checklist.deleteTask(this, <?php echo $task->id; ?>); return false;"
                           onmouseover="tooltip.show(this, 'Удалить', {position :'top-right'});"></a>
                    </div>
                </td>
            </tr>

            <?php endforeach; ?>
        <?php endforeach; ?>

    <?php endif; ?>
<tr class="empty_item js_empty_item">
    <td class="l_al task_desc">Корзина пуста...</td>
</tr>

<?php else: ?>
<tr class="empty_item">
    <td class="l_al task_desc">Корзина пуста...</td>
</tr>
<?php endif; ?>

