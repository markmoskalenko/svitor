<div class="page info_content">
    <?php if($_GET['name'] == "help_company" || $_GET['name'] == "help_user" || $_GET['name'] == "help"): ?>
    <div class="button_main">
        <button id="b-upbox">Получить помощь</button>
    </div>
    <div class="content" id="newMsgForm" style="display: none;">
        <div id="msg"></div>
        <form onsubmit="return false;">
            <div class="label_name">Кому: Администратору</div>

            <div class="label_name">Тема <span class="red_text">*</span></div>
            <div class="field">
                <?php
                $msg->title = CHtml::decode($msg->title);
                echo CHtml::activeTextField($msg, 'title', array(
                    'style' => 'width: 98%;',
                    'required' => 'required'
                ));
                ?>
            </div>

            <div class="field">
                <div class="label_name">Сообщение <span class="red_text">*</span></div>
                <div class="field">
                    <?php
                    $msg->message = CHtml::decode($msg->message);
                    echo CHtml::activeTextArea($msg, 'message', array('style' => 'width: 98%; height: 60px;'));
                    ?>
                </div>
            </div>
        </form>
        <div class="buttons">
            <div class="button_main">
                <button type="button"
                        onclick="msg.send(this, <?php echo $to_uid; ?>, <?php echo $to_vid; ?>);">
                    Отправить
                </button>
            </div>
            <div class="button_grey">
                <button class="b-upbox-close" ">Отменить</button>
            </div>
        </div>
    </div>
    <script>

        $('#b-upbox').on('click', function(){
            <?php
            if(isset($_GET['name']) && ($_GET['name'] == "help_company" || $_GET['name'] == "help_user" || $_GET['name'] == "help")){
                if (User::isGuest() && !isset($_SESSION["vendor__id"])){?>

                    $('#newMsgForm').html("Вы не зарегистрированы! Пожалуйста пройдите <a href='/user/registration/'>регистрацию</a>.")

                    //var delay = 3000;
                    //setTimeout("document.location.href='/user/registration/'", delay);

                    <?php }
            }
            ?>
            $('#newMsgForm').fadeIn(300);
        })
        $('.b-upbox-close').on('click', function(){
            $('#newMsgForm').fadeOut(300);
        })
        var msg = {
            send:function (trigger, to_uid, to_vid) {

                var msg = $('#msg');
                var url = '/messages/new/';
                var old_button_content = $(trigger).html();
                $(trigger).html($.ajaxLoader()).attr('disabled', true);

                if (to_uid != 0) {
                    url += 'uid/' + to_uid;
                } else {
                    url += 'vid/' + to_vid;
                }

                var data = $('#newMsgForm form').serialize();

                $.post(url, data, function (re) {
                    if (re == 'ok') {
                        $('.b-upbox .buttons').fadeOut(300);
                        $('#newMsgForm').fadeOut(300, function () {
                            $(this).html('<br><div align="center"><b>Сообщение успешно отправлено.</b></div>').fadeIn(100);
                        });
                        msg.hide();
                        setTimeout(function () {
                            $('#newMsgForm').fadeOut(300);
                        }, 2500);
                    } else {
                        $(trigger).html(old_button_content).removeAttr('disabled');
                        msg.html(re).fadeIn(200);
                    }
                });
            }
        };
    </script>
    <br>
    <br>
    <?php endif;?>
    <?php
        $this->pageTitle        = isset($site_page->title_h1) ? $site_page->title_h1 : $site_page->title;
        $this->pageDescription  = isset($site_page->description) ? $site_page->description : $site_page->title;
        $this->pageKeywords     = isset($site_page->keywords) ? $site_page->keywords : $site_page->title;
    ?>
    <h1><?php echo $site_page->title; ?></h1>
    <br>
    <?php echo $site_page->content; ?>
</div>