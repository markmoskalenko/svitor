<?php
class VendorController extends AppController
{
    public $defaultAction = 'profile';
    public $upbox_title = '';

    public function filters()
    {
        return array('accessControl');
    }

    public function accessRules()
    {
        return array(
            array('deny',
                  'actions' => array('UserNote', 'UserReview'),
                  'users' => array('?')
            ),
            array('allow',
                  'actions' => array('UserNote', 'UserReview'),
                  'users' => array('@')
            )
        );
    }

    public function actionProfile($id = null)
    {
        $id = intval($id);
        if (empty($id))
            $this->redirect('/');

        // Делаем жадную загрузку
        // TODO: кэшировать запрос
        $profile_info = Vendor::model()->with('Reviews', 'Faq', 'Videos', 'Images', 'Categories')->findByAttributes(array('id' => $id));

        if (empty($profile_info))
            $this->redirect('/');

        Yii::app()->name = $profile_info->title.' - '.Yii::app()->name;

        $attribute_values = VendorAttrValues::model()->with('Values')->findAllByAttributes(array('vendor_id' => $profile_info->id));

        $this->render('profile', array(
            'profile_info' => $profile_info,
            'attribute_values' => $attribute_values
        ));
    }

    public function actionUserNote($id = null)
    {
        if (!Yii::app()->request->isAjaxRequest)
            Yii::app()->end();

        if (!mApi::getFunId())
            mApi::sayError(1, 'Не выбрано торжество.');

        $this->layout = 'upbox';
        $this->upbox_title = 'Заметка';

        $id = intval($id);
        if (empty($id))
            Yii::app()->end();

        if (!Vendor::model()->findByAttributes(array('id' => $id)))
            Yii::app()->end();

        $model = UserVendorTeam::model()->findByAttributes(array('uid' => User::getId(), 'vendor_id' => $id, mApi::getFunType(true).'_id' => mApi::getFunId()));
        if (empty($model))
            Yii::app()->end();

        if (isset($_POST['UserVendorTeam']))
        {
            $model->attributes = $_POST['UserVendorTeam'];
            if ($model->save()) {
                echo 'ok';
            } else {
                echo CHtml::errorSummary($model);
            }
        }
        else
        {
            $this->render('user_note', array(
                'model' => $model,
                'vendor_id' => $id
            ));
        }
    }

    public function actionUserReview($id = 0)
    {
        if (!Yii::app()->request->isAjaxRequest)
            Yii::app()->end();

        $id = intval($id);
        if (empty($id))
            Yii::app()->end();

        if (!$vendor = Vendor::model()->findByAttributes(array('id' => $id)))
            Yii::app()->end();

        $this->layout = 'upbox';
        $this->upbox_title = 'Отзыв о компании &laquo;'.mApi::cutStr($vendor->title, 100).'&raquo;';

        if (!mApi::getFunId())
            Yii::app()->end();

        $in_user_team = UserVendorTeam::model()->findByAttributes(array(mApi::getFunType(true) . '_id' => mApi::getFunId(), 'vendor_id' => $vendor->id));
        if (empty($in_user_team)) {
            if (!empty($_POST['VendorReviews']))
                Yii::app()->end();

            $this->render('user_review_error');
            return;
        }

        $model = VendorReviews::model()->findByAttributes(array('uid' => User::getId(), 'vendor_id' => $vendor->id));
        if (empty($model))
            $model = new VendorReviews('add');

        if (isset($_POST['VendorReviews']))
        {
            $model->attributes = $_POST['VendorReviews'];
            $model['uid'] = User::getId();
            $model['vendor_id'] = $vendor->id;
            $model['date'] = time();

            if ($model->save()) {
                echo 'ok';
            } else {
                echo CHtml::errorSummary($model);
            }
        }

        else
        {
            $this->render('user_review', array(
                'model' => $model,
                'vendor_id' => $vendor->id
            ));
        }
    }

    public function actionTeam($act = null, $id = 0, $cat_id = 0)
    {
        if (!Yii::app()->request->isAjaxRequest)
            Yii::app()->end();

        if (User::isGuest())
            mApi::sayError(1, 'Необходимо авторизироваться на сайте.');

        if (!mApi::getFunId())
            mApi::sayError(1, 'Не выбрано торжество.');

        if (!$vendor_info = Vendor::model()->findByAttributes(array('id' => $id)))
            mApi::sayError(1, 'Компания не найдена.');

        $act = htmlspecialchars($act);
        $cat_id = intval($cat_id);

        // Пишем действие в лог
        $log = new UserLogVendors();
        $log['uid'] = User::getId();
        $log['vendor_id'] = $vendor_info->id;
        $log['user_cat_id'] = $cat_id;
        $log[mApi::getFunType(true).'_id'] = mApi::getFunId();

        if ($act == 'add')
        {
            $log['event'] = 'add_to_team';

            if (!UserVendorCategories::model()->findByAttributes(array('uid' => User::getId(), 'id' => $cat_id, mApi::getFunType(true).'_id' => mApi::getFunId())))
                mApi::sayError(1, 'Категория не существует.');

            $model = new UserVendorTeam('add');
            $model['uid'] = User::getId();
            $model[mApi::getFunType(true).'_id'] = mApi::getFunId();
            $model['vendor_id'] = $vendor_info->id;
            $model['user_cat_id'] = $cat_id;
            $model->save();
        }
        else if ($act == 'remove')
        {
            $log['event'] = 'remove_from_team';

            UserVendorTeam::model()->deleteAllByAttributes(array('uid' => User::getId(), 'vendor_id' => $vendor_info->id, mApi::getFunType(true).'_id' => mApi::getFunId()));
        }
        $log->save();

        echo 'ok';
    }

    public function actionFavorite($act = null, $id = 0)
    {
        if (!Yii::app()->request->isAjaxRequest)
            Yii::app()->end();

        if (User::isGuest())
            mApi::sayError(1, 'Необходимо авторизироваться на сайте.');

        if (!Vendor::model()->findByAttributes(array('id' => $id)))
            Yii::app()->end();

        $act = htmlspecialchars($act);
        $id = intval($id);

        if (empty($id))
            Yii::app()->end();

        // Пишем действие в лог
        $log = new UserLogVendors();
        $log['uid'] = User::getId();
        $log['vendor_id'] = $id;

        if ($act == 'add')
        {
            $log['event'] = 'add_to_favorites';

            $model = new UserVendorFavorites('add');
            $model['uid'] = User::getId();
            $model['vendor_id'] = $id;
            $model->save();
        }
        else if ($act == 'remove')
        {
            $log['event'] = 'remove_from_favorites';

            UserVendorFavorites::model()->deleteAllByAttributes(array('uid' => User::getId(), 'vendor_id' => $id));
        }
        $log->save();

        echo 'ok';
    }
}