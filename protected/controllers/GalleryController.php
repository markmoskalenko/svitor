<?php
class GalleryController extends AppController
{
    public function actionIndex($category = '')
    {
        $active_cat_id = 0;
        $vendor = array();
        $vendor_id = intval(Yii::app()->request->getParam('vendor'));
        $categories = GalleryCategory::model()->findAll();

        if (!empty($category))
        {
            foreach ($categories as $cat)
            {
                if ($category == $cat->name) {
                    $active_cat_id = $cat->id;
                    break;
                }
            }
        }

        $criteria = new CDbCriteria;
        $criteria->with = array('Vendor' => array('with' => 'PhotoInGalleryCount'), 'Category');
        $criteria->order = 'added_date DESC';

        if ($active_cat_id !== 0)
            $criteria->addCondition('cat_id = ' . $active_cat_id);

        if (!empty($vendor_id)) {
            $vendor = Vendor::model()->findByPk($vendor_id);

            if (!empty($vendor))
                $criteria->addCondition('vendor_id = ' . $vendor_id);
        }

        $photos = new CActiveDataProvider('Gallery', array(
            'pagination' => array(
                'pageSize' => 15,
                'pageVar' => 'page'
            ),
            'criteria' => $criteria
        ));

        Yii::app()->clientScript->registerCssFile('/static/gallery.css?2');

        $this->render('index', array(
            'photos' => $photos,
            'vendor' => $vendor,
            'categories' => $categories,
            'active_cat_id' => $active_cat_id,
            'imageManager' => new ImageManager
        ));
    }
}