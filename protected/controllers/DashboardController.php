<?php
class DashboardController extends AppController
{
    public $defaultAction = 'index';
    public $layout = 'planning';

    protected $user = '';

    public function actionIndex()
    {
        if (User::isGuest())
            $this->redirect('/user/login/');

        Yii::app()->name = 'Пульт управления - '.Yii::app()->name;

        $guest_count = array();
        $checklist_count = array();

        $wedding = mApi::getWedding();
        $holiday = mApi::getHoliday();

        if (!empty($wedding) || !empty($holiday))
        {
            $criteria = new CDbCriteria;
            $criteria->condition = 'uid = :uid AND '.mApi::getFunType(true).'_id = '.mApi::getFunId();
            $criteria->params = array('uid' => User::getId());

            // Количество гостей
            $guest_count = UserGuests::model()->count($criteria);

            // Количество задач
            $criteria->condition .= ' AND folder != "trash"';
            $checklist_count = UserChecklist::model()->count($criteria);
        }

        $usite = UserSite::model()->findByAttributes(array('uid' => User::getId(), mApi::getFunType(true).'_id' => mApi::getFunId()));

        $this->render(mApi::getFunType(true), array(
            'wedding' => $wedding,
            'holiday' => $holiday,

            'guest_count' => $guest_count,
            'checklist_count' => $checklist_count,

            'usite' => $usite
        ));
    }

}
?>