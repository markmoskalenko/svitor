<?php
class WannaGiftsController extends AppController
{
    public $layout = 'planning';
    public $upbox_title = '';

    public function filters()
    {
        return array('accessControl');
    }

    public function accessRules()
    {
        return array(
            array('deny',
                'actions' => array('Index', 'Add', 'Edit', 'Delete'),
                'users' => array('?')
            ),
            array('allow',
                'actions' => array('Index', 'Add', 'Edit', 'Delete'),
                'users' => array('@')
            )
        );
    }

    public function actionIndex()
    {
        Yii::app()->name = 'Подарки - '.Yii::app()->name;

        $wanna_gifts_data = array();
        $success = '';
        $all_count = 0;
        $givers_count = 0;
        $guests_msg = array();

        if (mApi::getFunId())
        {
            $criteria = new CDbCriteria;
            $criteria->condition = 't.uid = :uid AND t.'.mApi::getFunType(true).'_id = :fun_id';
            $criteria->params = array(
                ':uid' => User::getId(),
                ':fun_id' => mApi::getFunId()
            );

            $wanna_gifts_data = UserWannaGifts::model()->with('Guest')->findAll($criteria);;

            // "Обращение к гостям"
            $guests_msg = UserGuestMsg::model()->find($criteria);
            if (empty($guests_msg))
                $guests_msg = new UserGuestMsg('add');

            if (isset($_POST['UserGuestMsg']))
            {
                $guests_msg->attributes = $_POST['UserGuestMsg'];
                if ($guests_msg->isNewRecord)
                {
                    $guests_msg['uid'] = User::getId();
                    $guests_msg[mApi::getFunType(true).'_id'] = mApi::getFunId();
                }

                if ($guests_msg->save())
                    $success = 'Изменения успешно сохранены.';
            }

            $criteria->condition .= ' AND t.guest_id != 0';

            $all_count = count($wanna_gifts_data);
            $givers_count = UserWannaGifts::model()->count($criteria);
        }

        $this->render('index', array(
            'wanna_gifts_data' => $wanna_gifts_data,
            'all_count' => $all_count,
            'givers_count' => $givers_count,

            'guests_msg' => $guests_msg,
            'success' => $success
        ));
    }

    public function actionAdd()
    {
        $this->upbox_title = 'Добавление подарка';
        $this->layout = 'upbox';

        $model = new UserWannaGifts('add');

        // Список гостей (для выбора "Кто подарит?")
        $guests = UserGuests::model()->findAllByAttributes(array('uid' => User::getId(), mApi::getFunType(true).'_id' => mApi::getFunId()));

        if (isset($_POST['UserWannaGifts']))
        {
            $model->attributes = $_POST['UserWannaGifts'];
            $model->uid = User::getId();
            $model[mApi::getFunType(true).'_id'] = mApi::getFunId();

            if ($model->save()) {
                echo 'ok';
            } else {
                echo CHtml::errorSummary($model);
            }
        }
        else
        {
            $this->render('_form', array(
                'model' => $model,
                'guests' => $guests
            ));
        }
    }

    public function actionEdit($id = 0)
    {
        $id = intval($id);
        if (empty($id))
            Yii::app()->end();

        $this->upbox_title = 'Редактирование подарка';
        $this->layout = 'upbox';

        $model = UserWannaGifts::model()->findByAttributes(array('uid' => User::getId(), 'id' => $id));
        if (empty($model))
            Yii::app()->end();

        // Список гостей (для выбора "Кто подарит?")
        $guests = UserGuests::model()->findAllByAttributes(array('uid' => User::getId(), mApi::getFunType(true).'_id' => mApi::getFunId()));

        if (isset($_POST['UserWannaGifts']))
        {
            $model->scenario = 'edit';
            $model->attributes = $_POST['UserWannaGifts'];

            if ($model->save()) {
                echo 'ok';
            } else {
                echo CHtml::errorSymmary($model);
            }
        }
        else
        {
            $this->render('_form', array(
                'model' => $model,
                'guests' => $guests
            ));
        }
    }

    public function actionDelete($id = 0)
    {
        $id = intval($id);
        if (empty($id))
            Yii::app()->end();

        UserWannaGifts::model()->deleteAllByAttributes(array('id' => $id, 'uid' => User::getId()));
    }
}