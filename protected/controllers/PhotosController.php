<?php
class PhotosController extends AppController
{
    public $upbox_title;

    public function actionIndex()
    {
        Yii::app()->name = 'Фотоальбомы - '.Yii::app()->name;

        $this->layout = 'planning';

        $images = UserImages::model()->findAllByAttributes(array('uid' => User::getId(), mApi::getFunType(true) . '_id' => mApi::getFunId()));

        $images_count = count($images);
        $max_images = Yii::app()->params['user_max_images'];

        // Сохраняем только если кол-во изображений меньше user_max_images
        if (isset($_POST['img_id']) && $images_count < $max_images)
        {
            $image_info = $_POST;

            if ($image_info['status'] != 'ok' || !mApi::getFunId())
                die();

            $model = new UserImages('add');
            $model['uid'] = User::getId();
            $model[mApi::getFunType(true) . '_id'] = mApi::getFunId();
            $model['img_id'] = $image_info['img_id'];
            $model['img_filename'] = $image_info['file_name'];
            $model['added_date'] = time();

            if ($model->save())
            {
                $images_count++;

                $image_info['id'] = $model->id;
                $image_info['img_count'] = $images_count;

                echo json_encode($image_info);
            }
        }
        else
        {
            $this->render('index', array(
                'images_count' => $images_count,
                'max_images' => $max_images,
                'images' => $images
            ));
        }
    }

    public function actionEdit($id = 0)
    {
        $id = intval($id);
        if (empty($id) || !mApi::getFunId())
            Yii::app()->end();

        $image = UserImages::model()->findByAttributes(array('id' => $id, 'uid' => User::getId(), mApi::getFunType(true) . '_id' => mApi::getFunId()));
        if (empty($image))
            Yii::app()->end();

        $this->layout = 'upbox';
        $this->upbox_title = 'Редактирование описания';

        if (isset($_POST['UserImages']))
        {
            $image->attributes = $_POST['UserImages'];
            if ($image->save()) {
                echo 'ok';
            } else {
                echo CHtml::errorSummary($model);
            }
        }
        else
        {
            $this->render('_form', array(
                'image' => $image
            ));
        }
    }

    public function actionDelete($id = 0)
    {
        $id = intval($id);
        if (empty($id) || !mApi::getFunId())
            Yii::app()->end();

        $image = UserImages::model()->findByAttributes(array('id' => $id, 'uid' => User::getId(), mApi::getFunType(true) . '_id' => mApi::getFunId()));
        if (empty($image))
            Yii::app()->end();

        // Установим статус "удалено" для картинки
        FileImages::model()->updateAll(array('is_deleted' => 1), 'id = '.$image->img_id);

        // Удаляем картинку у пользователя
        $image->delete();
    }
}