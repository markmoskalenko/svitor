<?php
class EventController extends AppController
{
    public $defaultAction = 'products';
    public $pageTitle = null;

    public function actionProducts()
    {
        $date = Yii::app()->request->getParam('date');

        $date = explode(".", $date);

        if (isset($date[0]) && !empty($date[0])) $year = intval($date[0]);
            else $year = date('Y');

        if (isset($date[1]) && !empty($date[1])) $month = intval($date[1]);
            else $month = date('n');

        $day = 0;

        //Хранит модели результатов
        $result = array();

        //Тип фильтра - Aкции, Гадания, Выставки | NULL - ВСЕ |
        $type = null;

        //Атрибуты Aкций
        $attrStock      = Category::model()->findAllByAttributes(array('visible' => 1));

        //Атрибуты Гадания
        $attrDivination = Attribute::model()->findAllByAttributes(array('belongs_to' => 'divination'));

        //Атрибуты Выставки
        $attrExhibition = Attribute::model()->findAllByAttributes(array('belongs_to' => 'exhibitions'));

        //Общий параметр поиска
        $criteria = new CDbCriteria();
        $criteria->alias = 't';

        $criteria->condition
          = '
              (
               YEAR(FROM_UNIXTIME(date_from)) <= :year AND
               MONTH(FROM_UNIXTIME(date_from)) <= :month
              )
              AND
              (
                YEAR(FROM_UNIXTIME(date_to)) >= :year AND
                MONTH(FROM_UNIXTIME(date_to)) >= :month
              )';

        $criteria->params = array(':year' => $year, ':month' => $month);

        if(Yii::app()->request->getParam('q')){
            $criteria->addCondition('`t`.`description` LIKE "%'.CHtml::encode(Yii::app()->request->getParam('q')).'%"');
        }

//        criteria->compare('description',)$


        // ******************************************* ФИЛЬТР АКЦИЙ **************************************************//
        $criteriaStock = clone $criteria;
        $criteriaStock->join = 'LEFT JOIN `vendor_addresses` ON (`t`.`vendor_id` = `vendor_addresses`.`vendor_id`)';

        if(isset($_GET['country']) && !empty($_GET['country'])){
            $criteriaStock->addCondition('vendor_addresses.country_id = :country');
            $criteriaStock->params[':country'] = (int)$_GET['country'];
        }

        if(isset($_GET['city']) && !empty($_GET['city'])){
            $criteriaStock->addCondition('vendor_addresses.city_id = :city');
            $criteriaStock->params[':city'] = (int)$_GET['city'];
        }

        if(isset($_GET['region']) && !empty($_GET['region'])){
            $criteriaStock->addCondition('vendor_addresses.region_id = :region');
            $criteriaStock->params[':region'] = (int)$_GET['region'];
        }



        $criteriaStock->group = 't.stock_id';

        if (!isset($_GET['is_active_stock']) || $_GET['is_active_stock'] == 1 ? true : false) {

            if (Yii::app()->request->getParam('stock')) {

                $data = VendorStock::getByCategories(
                    Yii::app()->request->getParam('stock'),
                    $criteriaStock
                );

                $result['stock'] = $data && is_array($data) && count( $data ) ? $data : array();

                foreach ($attrStock as $i) {
                    in_array($i->id, Yii::app()->request->getParam('stock')) ? $i->checkedOnFilter = 1
                      : $i->checkedOnFilter = 0;
                }
            } else {
                $result['stock'] = VendorStock::model()->findAll($criteriaStock);
            }

        } else {
            $result['stock'] = array();
        }
        unset( $criteriaStock );

        // КОНЕЦ ФИЛЬТРА АКЦИЙ

        // ОБЩИЕ ПАРАМЕТРЫ ПОИСКА ЖДЯ ГАДАНИЙ И ВЫСТАВОК
        if (Yii::app()->request->getParam('city')) {
            $criteria->addCondition('sity = :city');
            $criteria->params[':city'] = (int) Yii::app()->request->getParam('city');
        }
        if (Yii::app()->request->getParam('region')) {
            $criteria->addCondition('region = :region');
            $criteria->params[':region'] = (int) Yii::app()->request->getParam('region');
        }
        if (Yii::app()->request->getParam('country')) {
            $criteria->addCondition('country = :country');
            $criteria->params[':country'] = (int) Yii::app()->request->getParam('country');
        }

        //ФИЛЬТР ГАДАНИЙ
        if (!isset($_GET['is_active_divination']) || $_GET['is_active_divination'] == 1 ? true : false) {

            if (Yii::app()->request->getParam('divination')) {

                $params = Yii::app()->request->getParam('divination');

                $data = Divination::getByAttr($params, clone $criteria);

                $result['divination'] = $data && is_array($data) && count( $data ) ? $data : array();

                foreach ($attrDivination as $i) {
                    if (!is_array($i->Values) || !count($i->Values)) {
                        continue;
                    }
                    foreach ($i->Values as $b) {
                        if(!isset($params[$i->id])) continue;
                        in_array($b->id, $params[$i->id]) ? $b->checkedOnFilter = 1 : $b->checkedOnFilter = 0;
                    }
                }

            } else {
                $result['divination'] = Divination::model()->findAll(clone $criteria);
            }

        } else {
            $result['divination'] = array();
        }

        //ФИЛЬТР  ВЫСТАВОК
        if (!isset($_GET['is_active_exhibition']) || $_GET['is_active_exhibition'] == 1 ? true : false) {

            if (Yii::app()->request->getParam('exhibition')) {

                $params = Yii::app()->request->getParam('exhibition');

                $data = Exhibitions::getByAttr($params, clone $criteria);

                $result['exhibition'] = $data && is_array($data) && count( $data ) ? $data : array();

                foreach ($attrExhibition as $i) {
                    if (!count($i->Values)) {
                        continue;
                    }
                    foreach ($i->Values as $b) {
                        in_array($b->id, $params[$i->id]) ? $b->checkedOnFilter = 1 : $b->checkedOnFilter = 0;
                    }
                }

            } else {
                $result['exhibition'] = Exhibitions::model()->findAll(clone $criteria);
            }

        } else {
            $result['exhibition'] = array();
        }

        Yii::app()->name = 'Скидки, события';

        $this->render(
            'products',
            array(
                 'attrStock'      => $attrStock,
                 'attrDivination' => $attrDivination,
                 'attrExhibition' => $attrExhibition,
                 'result'         => $result,
                 'year'           => $year,
                 'month'          => $month,
                 'day'            => $day,
            )
        );
    }

    public function actionItem()
    {

        $id   = intval(Yii::app()->request->getParam('id'));
        $type = Yii::app()->request->getParam('type');

        $country = '';
        $region  = '';
        $city    = '';
        $company = '';

        if ($type == 'divination') {
            $dataItem = Divination::model()->findByAttributes(array('event_id' => $id));

        } else {
            if ($type == 'exhibition') {
                $dataItem = Exhibitions::model()->findByAttributes(array('event_id' => $id));

                $country = Countries::model()->find(
                    'country_id=:country_id',
                    array(':country_id' => $dataItem['country'])
                );

                $region = Regions::model()->find('region_id=:region_id', array(':region_id' => $dataItem['region']));
                $city   = Cities::model()->find('city_id=:city_id', array(':city_id' => $dataItem['sity']));

            } else {
                $dataItem = VendorStock::model()->findByAttributes(array('stock_id' => $id));
                $company  = Vendor::model()->findByPk($dataItem['vendor_id']);
            }
        }

        Yii::app()->name = $dataItem['title'];

        $this->render(
            'item',
            array(

                 'dataItem' => $dataItem,
                 'country'  => $country,
                 'region'   => $region,
                 'city'     => $city,
                 'company'  => $company,
                 'type'     => $type
            )
        );
    }
}