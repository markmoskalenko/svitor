<?php
class ReportController extends AppController
{
    public $defaultAction = 'index';

    public function filters()
    {
        return array('accessControl');
    }

    public function accessRules()
    {
        return array(
            array('deny',
                'actions' => array('Add', 'Edit', 'Delete'),
                'users' => array('?')
            ),
            array('allow',
                'actions' => array('Add', 'Edit', 'Delete'),
                'users' => array('@')
            )
        );
    }

    public function actionIndex($cat = '', $view = 0)
    {
        $cat = strtolower(htmlspecialchars(trim($cat)));
        $active_category = array();
        $dataProvider = array();
        $view_report = array();
        $reports_data = array();

        Yii::app()->name = 'Полезная информация - '.Yii::app()->name;

        // Выборка категории
        if (!empty($cat))
            $active_category = ReportCategories::model()->findByAttributes(array('name' => $cat));

        if (!empty($active_category))
            Yii::app()->name = $active_category->title . ' - ' . Yii::app()->name;

        $categories = ReportCategories::model()->with(array(
            'UserReports' => array(
                'select' => false,
                'joinType' => 'INNER JOIN',
                'report_cat_id = t.id'
            )
        ))->cache(500)->findAll();

        // Просмотр отчета
        if (!empty($view))
        {
            $view_id = intval($view);
            if (empty($view_id))
                $this->redirect(Yii::app()->createUrl('report'));

            $criteria = new CDbCriteria;
            $criteria->condition = '(t.status = "published" OR t.status = "published_verification") AND t.id = :report_id';
            $criteria->order = 'published_date DESC';
            $criteria->params = array(':report_id' => $view_id);
            $criteria->with = array('User');

            $view_report = UserReports::model()->find($criteria);
            if (empty($view_report))
                $this->redirect(Yii::app()->createUrl('report'));

            Yii::app()->name = $view_report->title . ' - ' . Yii::app()->name;
        }
        // Иначе показываем список отчетов
        else
        {
            // Если категория есть, то ищем в ней отчеты
            if (!empty($active_category))
            {
                $criteria = new CDbCriteria;
                $criteria->condition = '(t.status = "published" OR t.status = "published_verification") AND t.report_cat_id = :report_cat_id';
                $criteria->order = 'published_date DESC';
                $criteria->params = array(':report_cat_id' => $active_category->id);

                // Если есть что-то в поиске
                if (!empty($_GET['q']))
                {
                    $query = trim($_GET['q']);
                    $criteria->addSearchCondition('t.title', $query, true);
                    $criteria->addSearchCondition('t.content', $query, true, 'OR');
                }

                $dataProvider = new CActiveDataProvider('UserReports', array(
                    'pagination' => array(
                        'pageSize' => Yii::app()->params['items_per_page'],
                        'pageVar' => 'page'
                    ),
                    'criteria' => $criteria
                ));
            }
            // Иначе показываем все отчеты из всех разделов
            else
            {
                $criteria = new CDbCriteria;
                $criteria->condition = '(t.status = "published" OR t.status = "published_verification")';
                $criteria->order = 'published_date DESC';
                $criteria->with = array('ReportCategory');

                // Если есть что-то в поиске
                if (!empty($_GET['q']))
                {
                    $query = trim($_GET['q']);
                    $criteria->addSearchCondition('t.title', $query, true);
                    $criteria->addSearchCondition('t.content', $query, true, 'OR');
                }

                $dataProvider = new CActiveDataProvider('UserReports', array(
                    'pagination' => array(
                        'pageSize' => Yii::app()->params['items_per_page'],
                        'pageVar' => 'page'
                    ),
                    'criteria' => $criteria
                ));
            }

            $reports_data = $dataProvider->getData();
        }

        $this->render('index', array(
            'categories' => $categories,
            'active_category' => $active_category,
            'view_report' => $view_report,
            'reports' => $reports_data,
            'dataProvider' => $dataProvider
        ));
    }

    public function actionAdd()
    {
        $model = new UserReports('add');

        if (isset($_POST['UserReports']))
        {
            $model->attributes = $_POST['UserReports'];
            $model->uid = User::getId();

            if ($model->save())
                Yii::app()->user->setFlash('add_report_success', 'Спасибо! Ваш отчёт был успешно отправлен. После проверки модератором он будет опубликован на сайте.');
        }

        $this->render('_form', array(
            'model' => $model,
            'categories' => CHtml::listData(ReportCategories::model()->cache(500)->findAll(), 'id', 'title')
        ));
    }

    public function actionEdit($id = 0)
    {
        $model = UserReports::model()->findByPk($id);

        if (empty($model))
            $this->redirect(Yii::app()->createUrl('report/index'));

        if (isset($_POST['UserReports']))
        {
            $model->attributes = $_POST['UserReports'];

            if ($model->save())
                Yii::app()->user->setFlash('add_report_success', 'Изменения были успешно сохранены.');
        }

        $this->render('_form', array(
            'model' => $model,
            'categories' => CHtml::listData(ReportCategories::model()->cache(500)->findAll(), 'id', 'title')
        ));
    }
}