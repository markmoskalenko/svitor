<?php
class AreaController extends AppController
{
    public $defaultAction = 'Select';

    public function actionSelect()
    {

        $data = Countries::model()->findAll(array('order' => 'sort DESC, name ASC'));
        $this->renderPartial('select', array(
            'data' => $data,
            'user' => User::getInfo()
        ));
    }

    public function actionGet_regions($country = 0, $select = '')
    {
        $country_id = intval($country);
        if (empty($country_id))
            Yii::app()->end();

        $data = Regions::model()->cache(1000)->findAllByAttributes(array('country_id' => $country_id), array('order' => 'name ASC'));
        if (empty($data))
            Yii::app()->end();

        $this->renderPartial('get_regions', array(
            'data' => $data,
            'select' => $select
        ));
    }

    public function actionGet_cities($region = 0, $select = '')
    {
        $region_id = intval($region);
        if (empty($region_id))
            Yii::app()->end();

        $data = Cities::model()->cache(1000)->findAllByAttributes(array('region_id' => $region_id), array('order' => 'name ASC'));
        if (empty($data))
            Yii::app()->end();

        $this->renderPartial('get_cities', array(
            'data' => $data,
            'select' => $select
        ));
    }

    public function actionGet_metro_stantions($city = 0, $select = '')
    {
        $city_id = intval($city);
        if (empty($city_id))
            Yii::app()->end();

        $data = MetroStantions::model()->cache(1000)->findAllByAttributes(array('city_id' => $city_id), array('order' => 'name ASC'));
        if (empty($data))
            Yii::app()->end();

        $this->renderPartial('get_metro_stantions', array(
            'data' => $data,
            'select' => $select
        ));
    }

    public function actionUpdate()
    {
        $model = User::getInfo();

        if (isset($_POST['User']))
        {
            $data = Yii::app()->request->getPost('User');
            $model->scenario = 'edit_area';

            $model->country_id = !empty($data['country_id']) ? $data['country_id'] : null;
            $model->region_id = !empty($data['region_id']) ? $data['region_id'] : null;
            $model->city_id = !empty($data['city_id']) ? $data['city_id'] : null;

            // Если не гость, то сохраняем в базу
            if (!User::isGuest())
            {
                if (!empty($model->country_id) && !Countries::model()->findByAttributes(array('country_id' => $model->country_id)))
                    mApi::sayError(1, 'Нет такой страны.');

                if (!empty($model->region_id) && !Regions::model()->findByAttributes(array('region_id' => $model->region_id)))
                    mApi::sayError(1, 'Нет такого региона.');

                if (!empty($model->city_id) && !Cities::model()->findByAttributes(array('city_id' => $model->city_id)))
                    mApi::sayError(1, 'Нет такого города.');

                if ($model->validate()) {
                    // Запишем изменения в базу
                    $model->update();
                } else {
                    mApi::sayError(1, 'Не удалось сохранить изменения. Попробуйте еще раз...');
                }
            }

            // Сохраним данные в сессию
            Yii::app()->user->setState('country_id', $model->country_id);
            Yii::app()->user->setState('region_id', $model->region_id);
            Yii::app()->user->setState('city_id', $model->city_id);

            echo 'ok';
        }
    }
}