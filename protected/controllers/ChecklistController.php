<?php
class ChecklistController extends AppController
{
    public $layout = 'planning';
    public $defaultAction = 'index';
    public $upbox_title = '';

    public function filters()
    {
        return array('accessControl');
    }

    public function accessRules()
    {
        return array(
            array('deny',
                  'actions' => array('Index', 'Add', 'Edit', 'Templates', 'AddByTemplates', 'SetStatus', 'CleanTrash', 'Delete'),
                  'users' => array('?')
            ),
            array('allow',
                  'actions' => array('Index', 'Add', 'Edit', 'Templates', 'AddByTemplates', 'SetStatus', 'CleanTrash', 'Delete'),
                  'users' => array('@')
            )
        );
    }

    public function actionIndex($folder = 0, $show_by = 'category')
    {
        Yii::app()->name = 'Список задач - '.Yii::app()->name;

        $folder = htmlspecialchars($folder);
        $show_by = htmlspecialchars($show_by);

        if ($folder != 'all' && $folder != 'todo' && $folder != 'done' && $folder != 'trash')
            $folder = 'all';

        if ($show_by != 'date' && $show_by != 'category')
            $show_by = 'category';

        $data = array();

        $data_count = 0;
        $done_count = 0;
        $trash_count = 0;

        if (mApi::getFunId())
        {
            $folder_condition = $folder != 'all' ? ' AND folder = "'.$folder.'"' : ' AND folder != "trash"';

            $criteria = new CDbCriteria;
            $criteria->condition = 'uid = :uid AND '.mApi::getFunType(true).'_id = '.mApi::getFunId().$folder_condition;
            $criteria->params = array('uid' => User::getId());

            if ($show_by == 'date')
            {
                $criteria->order = 'date IS NULL, date ASC';
                $data = UserChecklist::model()->findAll($criteria);
            }
            else if ($show_by == 'category')
            {
                $data = CategoryMiniatures::model()->with('UserChecklist')->findAll($criteria);
            }

            $criteria = new CDbCriteria;
            $criteria->condition = 'uid = '.User::getId().' AND '.mApi::getFunType(true).'_id = '.mApi::getFunId().' AND folder != "trash"';
            $data_count = UserChecklist::model()->count($criteria);

            $criteria = new CDbCriteria;
            $criteria->condition = 'uid = '.User::getId().' AND '.mApi::getFunType(true).'_id = '.mApi::getFunId().' AND folder = "done"';
            $done_count = UserChecklist::model()->count($criteria);

            $criteria = new CDbCriteria;
            $criteria->condition = 'uid = '.User::getId().' AND '.mApi::getFunType(true).'_id = '.mApi::getFunId().' AND folder = "trash"';
            $trash_count = UserChecklist::model()->count($criteria);
        }

        $this->render('index', array(
            'data' => $data,
            'data_count' => $data_count,
            'done_count' => $done_count,
            'trash_count' => $trash_count,


            'folder' => $folder,
            'show_by' => $show_by
        ));
    }

    public function actionAdd($budget_id = 0)
    {
        $this->upbox_title = 'Новая задача';
        $this->layout = 'upbox';

        $model = new UserChecklist('add');
        $cat_mins = CategoryMiniatures::model()->findAll('show_in_checklist = 1');

        $budget_id = intval($budget_id);
        if (!empty($budget_id)) {
            $budget = UserBudget::model()->cache(1000)->findByAttributes(array('uid' => User::getId(), 'id' => $budget_id));
            if (!empty($budget))
            {
                $model['budget_id'] = $budget->id;
                $model['cat_min_id'] = $budget->cat_min_id;
            }
        }

        if (isset($_POST['UserChecklist']))
        {
            $model->uid = User::getId();

            $model->folder = 'todo';
            $model[mApi::getFunType(true).'_id'] = mApi::getFunId();

            $model->attributes = $_POST['UserChecklist'];

            if ($model->validate())
            {
                if (!empty($model->date))
                {
                    $date = explode('.', $model->date);

                    if (!empty($model->time))
                        $time = explode(':', $model->time);

                    if (!empty($time)) {
                        $data_time = @mktime($time[0], $time[1], 00, $date[1], $date[0], $date[2]);
                    }
                    else {
                        $data_time = @mktime(00, 00, 00, $date[1], $date[0], $date[2]);
                    }

                    $model->date = $data_time;
                }

                if ($model->save(false))
                {
                    // Установим связь с бюджетом, если это необходимо.
                    if (!empty($budget))
                    {
                        $budget['task_id'] = $model->id;
                        $budget->update();

                    }

                    if (!empty($_POST['link_budget'])) {
                        echo '{"msg":"ok", "task_id":"'.$model->id.'"}';
                    } else {
                        echo '{"msg":"ok"}';
                    }
                }
            }
            else
            {
                $error = str_replace("\r\n", '', CHtml::errorSummary($model));
                $error = str_replace("\n", '', $error);
                $error = str_replace("\"", '\"', $error);
                echo '{"msg":"'.$error.'"}';
            }
        }
        else
        {
            $this->render('_form', array(
                'model' => $model,
                'cat_mins' => $cat_mins
            ));
        }
    }

    public function actionEdit($id = 0)
    {
        $this->upbox_title = 'Редактирование задачи';
        $this->layout = 'upbox';

        $id = intval($id);

        $model = UserChecklist::model()->findByAttributes(array('uid' => User::getId(), 'id' => $id));
        if (empty($model))
            Yii::app()->end();

        $cat_mins = CategoryMiniatures::model()->findAll('show_in_checklist = 1');

        if (isset($_POST['UserChecklist']))
        {
            $model->scenario = 'edit';
            $model->attributes = $_POST['UserChecklist'];

            if ($model->validate())
            {
                if (!empty($model->date))
                {
                    $date = explode('.', $model->date);

                    if (!empty($model->time))
                        $time = explode(':', $model->time);

                    if (!empty($time)) {
                        $data_time = @mktime($time[0], $time[1], 00, $date[1], $date[0], $date[2]);
                    }
                    else {
                        $data_time = @mktime(00, 00, 00, $date[1], $date[0], $date[2]);
                    }

                    $model->date = $data_time;
                }

                if ($model->save(false))
                    echo '{"msg":"ok"}';
            }
            else
            {
                $error = str_replace("\r\n", '', CHtml::errorSummary($model));
                $error = str_replace("\n", '', $error);
                $error = str_replace("\"", '\"', $error);
                echo '{"msg":"'.$error.'"}';
            }
        }
        else
        {
            $this->render('_form', array(
                'model' => $model,
                'cat_mins' => $cat_mins
            ));
        }
    }

    public function actionTemplates($type_id = 0)
    {
        if (!mApi::getFunId())
            Yii::app()->end();

        $type_id = intval($type_id);

        // Выбран тип, выводим шаблоны по категориям для этого типа
        if (!empty($type_id))
        {
            $this->layout = 'ajax';

            $criteria = new CDbCriteria;
            $criteria->condition = '(t.fun_type = "multi" OR t.fun_type = :fun_type) AND ' . mApi::getFunType(true) . '_type_id = :type_id';
            $criteria->params = array(':fun_type' => mApi::getFunType(true), ':type_id' => $type_id);

            $templates = CategoryMiniatures::model()->with('ChecklistTemplates')->findAll($criteria);

            $this->render('_templates', array(
                'templates' => $templates
            ));
        }
        // Если не выбран тип, то показываем список для выбора
        else
        {
            $this->upbox_title = 'Шаблоны задач';
            $this->layout = 'upbox';

            if (mApi::getFunType() == 1) {
                $template_types = HolidayTypes::model()->findAll();
            } else {
                $template_types = WeddingTypes::model()->findAll();
            }

            $this->render('_template_types', array(
                'template_types' => $template_types
            ));
        }
    }

    public function actionAddByTemplates()
    {
        if (empty($_GET['template']) || !Yii::app()->request->isAjaxRequest)
            mApi::sayError(1, 'Не удалось добавить выбранные задачи.');

        $criteria = new CDbCriteria;
        $criteria->addInCondition('entry', $_GET['template']);

        $checklist_templates = ChecklistTemplate::model()->findAll($criteria);

        if (empty($checklist_templates))
            mApi::sayError(1, 'Выбранные задачи не найдены.');

        foreach ($checklist_templates as $template)
        {
            if (empty($template['cat_min_id']))
                continue;

            $task = new UserChecklist('add');
            $task['uid'] = User::getId();
            $task[mApi::getFunType(true) . '_id'] = mApi::getFunId();
            $task['folder'] = 'todo';
            $task['cat_min_id'] = $template['cat_min_id'];
            $task['task'] = CHtml::decode($template['task']);
            $task['link'] = $template['link'];

            if ($task->save())
            {
                // Если в шаблоне укзано наименование для бюджета, то создаем строку в бюджете,
                // А также устанавливаем связь
                if (!empty($template['budget_title']))
                {
                    $budget = new UserBudget('add');
                    $budget['uid'] = $task->uid;
                    $budget[mApi::getFunType(true) . '_id'] = $task[mAPi::getFunType(true) . '_id'];
                    $budget['cat_min_id'] = $template['cat_min_id'];
                    $budget['title'] = CHtml::decode($template['budget_title']);
                    $budget['task_id'] = $task->id; // для привязки задачи к бюджету

                    if ($budget->save())
                    {
                        // установим связь задачи с бюджетом
                        $task['budget_id'] = $budget->id;
                        $task->update();
                    }
                }
            }
        }

        echo 'ok';
    }

    public function actionSetStatus($id = 0, $folder = 'done')
    {
        $this->layout = 'ajax';

        $id = intval($id);
        $folder = htmlspecialchars($folder);

        if (empty($id) || empty($folder))
            Yii::app()->end();

        if ($folder != 'todo' && $folder != 'done' && $folder != 'trash')
            Yii::app()->end();

        $task = UserChecklist::model()->findByAttributes(array('uid' => User::getId(), 'id' => $id));
        if (empty($task))
            Yii::app()->end();

        $task['folder'] = $folder;

        // Удаляем связь со строкой бюджета
        if ($folder == 'trash')
            $task['budget_id'] = 0;

        if ($task->update())
        {
            // Удаляем связь строки бюджета с этой задачей
            if ($folder == 'trash')
                UserBudget::model()->updateAll(array('task_id' => 0), 'uid = :uid AND task_id = :id', array(':uid' => User::getId(), ':id' => $task->id));

            echo $folder;
        }
        else
        {
            echo 0;
        }
    }

    public function actionCleanTrash()
    {
        UserChecklist::model()->deleteAllByAttributes(array('folder' => 'trash', 'uid' => User::getId()));
    }

    public function actionDelete($id = 0)
    {
        $id = intval($id);
        UserChecklist::model()->deleteAllByAttributes(array('id' => $id, 'uid' => User::getId()));
    }
}