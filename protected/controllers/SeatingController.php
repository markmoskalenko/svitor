<?php
class SeatingController extends AppController
{
    /**
     * Проверка прав доступа к получению\сохранению данных.
     */
    public function checkAccess()
    {
        if (User::isGuest())
            mApi::sayError(1, 'Необходимо авторизироваться на сайте.');

        if (!mApi::getFunId())
            mApi::sayError(1, 'Не выбрана свадьба или торжество.');

        return true;
    }

    /**
     * Создание нового стола.
     * @param $_POST UserGuestSeatingTables
     */
    public function actionAddTable()
    {
        $this->checkAccess();

        $model = new UserGuestSeatingTables('add');

        if (isset($_POST))
        {
            $model->attributes = $_POST;
            $model['uid'] = User::getId();
            $model[mApi::getFunType(true) . '_id'] = mApi::getFunId();

            if ($model->save()) {
                echo 'status=ok&id=' . $model->id;
            } else {
                echo 'Не удалось создать новый стол.';
            }
        }
    }

    /**
     * Обновление информации о существующем столе.
     * @param int $id уникальный идентификатор стола
     * @param $_POST UserGuestSeatingTables
     */
    public function actionEditTable($id = 0)
    {
        $this->checkAccess();

        $id = intval($id);
        if (empty($id))
            mApi::sayError(1, 'Не известный стол.');

        $model = UserGuestSeatingTables::model()->findByAttributes(array('uid' => User::getId(), 'id' => $id, mApi::getFunType(true) . '_id' => mApi::getFunId()));
        if (empty($model))
            mApi::sayError(1, 'Стол не найден.');

        if (isset($_POST))
        {
            $model->attributes = $_POST;

            if ($model->save()) {
                echo 'status=ok';
            } else {
                echo 'Не удалось сохранить даныне о столе ID: ' . $id;
            }
        }
    }

    /**
     * Удаление стола.
     * @param int $id уникальный идентификатор стола
     */
    public function actionDeleteTable($id = 0)
    {
        $this->checkAccess();

        $id = intval($id);
        if (empty($id))
            mApi::sayError(1, 'Не известный стол.');

        $condition = 'id = :id AND uid = :uid AND ' . mApi::getFunType(true) . '_id = ' . mApi::getFunId();
        $params = array(
            ':id' => $id,
            ':uid' => User::getId()
        );

        if (UserGuestSeatingTables::model()->deleteAll($condition, $params)) {
            echo 'status=ok';
        } else {
            echo 'Не удалось удалить стол ID: ' . $id;
        }
    }

    /*
     * Картинка комнаты
     */
    public function actionGetImage()
    {
        if (isset($GLOBALS['HTTP_RAW_POST_DATA']))
        {
            // get bytearray
            $image = $GLOBALS['HTTP_RAW_POST_DATA'];

            // add headers for download dialog-box
            header('Content-Type: image/jpeg');
            header('Content-Disposition: attachment; filename=seating_' . time() . '.jpg');
            echo $image;
        }
        else
        {
            echo 'Картинку не удалось нарисовать...';
        }
    }

    /**
     * Обновление данных о местоположение гостя за столом.
     * @param int $guest_id уникальный идентификатор гостя (required)
     * @param int $table_id уникальный идентификатор стола
     * @param int $place номер места за столом
     */
    public function actionChangePlace($guest_id = 0, $table_id = 0, $place = 0)
    {
        $guest_id = intval($guest_id);
        $table_id = intval($table_id);
        $place = intval($place);

        if (empty($guest_id))
            mApi::sayError(1, 'Полученные параметры не корректны.');

        $this->checkAccess();

        $condition = 'id = :guest_id AND uid = :uid AND ' . mApi::getFunType(true) . '_id = ' . mApi::getFunId();
        $params = array(
            ':guest_id' => $guest_id,
            ':uid' => User::getId()
        );

        if (UserGuests::model()->updateAll(array('table_id' => $table_id, 'place' => $place), $condition, $params)) {
            echo 'status=ok';
        } else {
            echo 'Не удалось сохранить местоположение гостя за столом.';
        }
    }

    /**
     * Установка размера комнаты торжества\свадьбы.
     * @param int $width ширина комнаты
     * @param int $height высота комнаты
     */
    public function actionSetRoomSize($width = 0, $height = 0)
    {
        $width = intval($width);
        $height = intval($height);

        if (empty($width) || empty($height) && ($width > 1000 || $height > 1000))
            mApi::sayError(1, 'Полученные параметры не корректны.');

        $this->checkAccess();

        $fun = mApi::getFunType() == 0 ? mApi::getWedding() : mApi::getHoliday();
        $fun->setScenario('set_room_size');

        $fun['seating_room_width'] = $width;
        $fun['seating_room_height'] = $height;

        if ($fun->update()) {
            // Обновим данные о свадьбе или торжестве в сессии
            mApi::getFunType() == 0 ? mApi::setWedding($fun) : mApi::getHoliday($fun);

            echo 'status=ok';
        }
        else {
            echo 'Не удалось сохранить размеры.';
        }
    }

    /**
     * Получение списка столов комнаты торжества\свадьбы (используется для flash-рассадки).
     * @param string $alt определяет тип вывода (XML или JSON)
     */
    public function actionGetAllTables($alt = '')
    {
        $alt = htmlspecialchars(strtolower($alt));

        if ($alt != 'xml' && $alt != 'json')
            mApi::sayError(1, 'Не получен параметр alt (возможные значения: xml, json)');

        $this->checkAccess();

        $tables_data = UserGuestSeatingTables::model()->findAllByAttributes(array('uid' => User::getId(), mApi::getFunType(true) . '_id' => mApi::getFunId()));

        if ($alt == 'xml')
        {
            header ("Content-type: text/xml");

            echo '<?xml version="1.0" encoding="utf-8"?>';

            $room_size = mApi::getFunType() == 0 ? mApi::getWedding() : mApi::getHoliday();

            echo '<xml>';
            echo '<size width="' . $room_size->seating_room_width . '" height="' . $room_size->seating_room_height . '" />';

            if (empty($tables_data))
                mApi::sayError(1, '</xml>');

            echo '<tables>';

            foreach ($tables_data as $table)
                echo '<table id="' . $table->id . '" type="' . $table->type .
                    '" name="' . $table->title . '" rotation="' . $table->rotation .
                    '" positX="' . $table->position_x . '" positY="' . $table->position_y .
                    '" places="' . $table->places . '" width="' . $table->width .
                    '" height="' . $table->height . '" diameter="' . $table->diameter .
                    '" note="' . $table->note . '"/>';

            echo '</tables>';
            echo '</xml>';
        }

        else if ($alt = 'json')
        {
            if (empty($tables_data))
                mApi::sayError(1, '{}');

            $data = '';
            foreach ($tables_data as $table)
            {
                $data .= '{"table_id":' . $table->id . ', "type":' . $table->type .
                    ',"name":"' . $table->title . '", "rotation":' . $table->rotation .
                    ', "positX":' . $table->postinion_x . ', "positY":' . $table->postinion_y .
                    ', "places":' . $table->places . ', "width":' . $table->width .
                    ', "height":' . $table->height . '", "diameter":' . $table->diameter .
                    ', "note":"' . $table->note . '"},';
            }
            echo rtrim($data, ',');
        }
    }

    /**
     * Печать столов + список гостей.
     */
    public function actionPrint()
    {
        $this->checkAccess();
        $this->layout = 'print';

        $tables = UserGuestSeatingTables::model()->with('Guests')->findAllByAttributes(array('uid' => User::getId(), mApi::getFunType(true) . '_id' => mApi::getFunId()));
        if (empty($tables))
            mApi::sayError(1, 'Нет данных для печати.');

        $this->render('print', array(
            'tables' => $tables
        ));
    }
}