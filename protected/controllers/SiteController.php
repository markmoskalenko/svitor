<?php
class SiteController extends AppController
{
    public $layout = 'planning';
    public $defaultAction = 'design';
    public $upbox_title = '';

    public function filters()
    {
        return array('accessControl');
    }

    public function accessRules()
    {
        return array(
            array('deny',
                'actions' => array('Design', 'Settings', 'MainPage', 'Gifts', 'addGift', 'editGift', 'deleteGift', 'uploadGiftImg'),
                'users' => array('?')
            ),
            array('allow',
                'actions' => array('Design', 'Settings', 'MainPage', 'Gifts', 'addGift', 'editGift', 'deleteGift', 'uploadGiftImg'),
                'users' => array('@')
            )
        );
    }

    public function loadUsite()
    {
        $title = mApi::getFunType() == 1 ? 'Сайт торжества' : 'Сайт молодоженов';
        Yii::app()->name = $title.' - '.Yii::app()->name;

        $usite = array();

        if (mApi::getFunId())
            $usite = UserSite::model()->findByAttributes(array(
                'uid' => User::getId(),
                mApi::getFunType(true).'_id' => mApi::getFunId(),
            ));

        return $usite;
    }

    public function actionDesign()
    {
        $success = '';
        $usite = $this->loadUsite();
        Yii::app()->name = 'Дизайн - '.Yii::app()->name;

        $templates = UsiteTemplates::model()->with('Styles')->findAllByAttributes(array('is_available' => 1));

        if (isset($_POST['UserSite']))
        {
            $usite->attributes = $_POST['UserSite'];

            if ($usite->save()) {
                echo 'ok'; die;
            }
        }

        $this->render('index', array(
            'action' => 'design',
            'usite' => $usite,
            'data' => array(
                'success' => $success,
                'templates' => $templates
            )
        ));
    }

    public function actionSettings()
    {
        $success = '';
        $usite = $this->loadUsite();
        Yii::app()->name = 'Настройки сайта - '.Yii::app()->name;

        if (isset($_POST['UserSite']))
        {
            $usite->attributes = $_POST['UserSite'];

            if ($usite->save())
                $success = 'Изменения успешно сохранены.';
        }

        $this->render('index', array(
            'action' => 'settings',
            'usite' => $usite,
            'data' => array(
                'success' => $success
            )
        ));
    }

    public function actionMainPage()
    {
        $success = '';
        $usite = $this->loadUsite();
        Yii::app()->name = 'Главная страница - '.Yii::app()->name;

        if (isset($_POST['UserSite']))
        {
            $usite->attributes = $_POST['UserSite'];

            if ($usite->save())
                $success = 'Изменения успешно сохранены.';
        }

        $this->render('index', array(
            'action' => 'mainPage',
            'usite' => $usite,
            'data' => array(
                'success' => $success
            )
        ));
    }

    public function actionGifts()
    {
        $gifts_data = array();
        $usite = $this->loadUsite();
        Yii::app()->name = 'Подарено - '.Yii::app()->name;

        if (mApi::getFunId())
            $gifts_data = UserGifts::model()->findAllByAttributes(array('uid' => User::getId(), mApi::getFunType(true).'_id' => mApi::getFunId()));

        $this->render('index', array(
            'action' => 'gifts',
            'usite' => $usite,
            'data' => array(
                'gifts_data' => $gifts_data
            )
        ));
    }

    public function actionAddGift()
    {
        $this->upbox_title = 'Добавление подарка';
        $this->layout = 'upbox';

        $model = new UserGifts('add');

        if (isset($_POST['UserGifts']))
        {
            $model->attributes = $_POST['UserGifts'];
            $model->uid = User::getId();
            $model[mApi::getFunType(true).'_id'] = mApi::getFunId();

            if ($model->save()) {
                echo 'ok';
            } else {
                echo CHtml::errorSummary($model);
            }
        }
        else
        {
            $this->render('_gifts_form', array(
                'model' => $model
            ));
        }
    }

    public function actionEditGift($id = 0)
    {
        $id = intval($id);

        $this->upbox_title = 'Редактирование подарка';
        $this->layout = 'upbox';

        $model = UserGifts::model()->findByAttributes(array('uid' => User::getId(), 'id' => $id));
        $model->scenario = 'edit';

        if (empty($model))
            Yii::app()->end();

        if (isset($_POST['UserGifts']))
        {
            $old_img_id = $model->img_id;

            $model->attributes = $_POST['UserGifts'];

            // Если изменили картинку, то старой присвоим статус "удалено"
            if ($old_img_id != $model->img_id)
                FileImages::model()->updateAll(array('is_deleted' => 1), 'id ='.$old_img_id);

            if ($model->save()) {
                echo 'ok';
            } else {
                echo CHtml::errorSymmary($model);
            }
        }
        else
        {
            $this->render('_gifts_form', array(
                'model' => $model
            ));
        }
    }

    public function actionDeleteGift($id = 0)
    {
        $id = intval($id);
        if (empty($id))
            Yii::app()->end();

        $gift = UserGifts::model()->findByAttributes(array('id' => $id, 'uid' => User::getId()));

        if (empty($gift))
            return;

        if (!empty($gift->img_id))
            FileImages::model()->updateAll(array('is_deleted' => 1), 'id ='.$gift->img_id);

        $gift->delete();
    }

    /*
     *  Просмотр сайта свадьбы/торжества
     *  также доступен просмотр http://example.com/s<id>
     *
     *  @params $id - уникальный ID свадьбы\торжества
     *
     */
    public function actionView($id = 0, $act = 'index')
    {
        $data = array();

        $id = intval($id);
        if (empty($id))
            $this->redirect('/');

        $usite = UserSite::model()->findByAttributes(array('id' => $id));
        if (empty($usite))
            throw new CHttpException(404);

        // Если сайт отключен, его может просматривать только юзер, который его создал
        if ($usite->is_available == 0 && (User::isGuest() || $usite->uid != User::getId()))
            throw new CHttpException(404);

        // Для <title>заголовка окна</title>
        Yii::app()->name = $usite->title;


        // Если не указан шаблон, поставим s_default
        if (empty($usite->template_id))
        {
            $usite->template_id = 1;
            $usite->template_style_id = 1;
        }

        // Предпросмотр шаблона+стиля
        $preview_template_id = intval(Yii::app()->request->getParam('template'));
        $preview_style_id = intval(Yii::app()->request->getParam('style'));

        if (!empty($preview_template_id) && !empty($preview_style_id))
        {
            $usite->template_id = $preview_template_id;
            $usite->template_style_id = $preview_style_id;
        }

        // Разбираемся какой шаблон и стили выбраны
        $template = $usite->Template;
        $template_style = $usite->TemplateStyle;

        if (empty($template) || empty($template_style)) // Если не найден шаблон или стиль, то прощаемся...
            throw new CHttpException(404);

        // Установим необходимый макет
        $this->layout = 'usite/'.$template->layout_name;

        // Подключим необходимый стиль, а также глобальный
        Yii::app()->clientScript->registerCssFile('/static/usite/'.$template->layout_name.'/'.$template_style->name.'/styles.css');
        Yii::app()->clientScript->registerCssFile('/static/usite/global.css');
        Yii::app()->clientScript->registerCssFile('/static/fancybox/jquery.fancybox.css');
        Yii::app()->clientScript->registerCssFile('/static/fancybox/helpers/jquery.fancybox-buttons.css');

        // А также jQuery, Fancybox, Main.js, m_Tooltip
        Yii::app()->clientScript->registerScriptFile('/static/js/jquery-1.8.0.min.js');
        Yii::app()->clientScript->registerScriptFile('/static/js/main.js');
        Yii::app()->clientScript->registerScriptFile('/static/fancybox/jquery.fancybox.pack.js');
        Yii::app()->clientScript->registerScriptFile('/static/fancybox/helpers/jquery.fancybox-buttons.js');
        Yii::app()->clientScript->registerScriptFile('/static/fancybox/helpers/jquery.fancybox-media.js');

        $fun_type = $usite->wedding_id == 0 ? 'holiday_id' : 'wedding_id';

        // Загружаем данные для активного раздела
        switch($act)
        {
            case 'index':
                break;

            case 'routine':
                if ($usite->show_routine == 0) { throw new CHttpException(404); break; }

                $data = UserRoutine::model()->with('Vendor')->findAllByAttributes(array($fun_type => $usite->$fun_type), array('order' => 'time ASC'));
                break;

            case 'guests_list':
                if ($usite->show_guests_list == 0) { throw new CHttpException(404); break; }

                $data = UserGuests::model()->with('GuestCategory')->findAllByAttributes(array($fun_type => $usite->$fun_type));
                break;

            case 'guests_seating':
                if ($usite->show_guests_seating == 0) { throw new CHttpException(404); break; }
                break;

            case 'vendors':
                if ($usite->show_vendors == 0) { throw new CHttpException(404); break; }

                $data = UserVendorCategories::model()->with('Team', 'Team.Vendor')->findAllByAttributes(array($fun_type => $usite->$fun_type));
                break;

            case 'wanna_gifts':
                if ($usite->show_wanna_gifts == 0) { throw new CHttpException(404); break; }

                $data['WannaGifts'] = UserWannaGifts::model()->findAllByAttributes(array($fun_type => $usite->$fun_type), array('order' => 'guest_id IS NULL, guest_id ASC'));
                $data['GuestsMsg'] = UserGuestMsg::model()->findByAttributes(array($fun_type => $usite->$fun_type));
                break;

            case 'gifts':
                if ($usite->show_gifts == 0) { throw new CHttpException(404); break; }

                $data = UserGifts::model()->findAllByAttributes(array($fun_type => $usite->$fun_type));
                break;

            case 'photos':
                if ($usite->show_photos == 0) { throw new CHttpException(404); break; }

                $data = UserImages::model()->findAllByAttributes(array($fun_type => $usite->$fun_type));
                break;

            // Если у нас такого раздела нету, то прощаемся...
            default:
                throw new CHttpException(404);
                break;
        }

        $this->render('view_site/site_main', array(
            'usite' => $usite,
            'act' => $act,
            'fun_type' => $fun_type,
            'data' => $data,

            'preview_template_id' => $preview_template_id,
            'preview_style_id' => $preview_style_id
        ));
    }
}