<?php
class VendorsController extends AppController
{
    public $defaultAction = 'Team';
    public $layout = 'planning';
    public $upbox_title = '';

	public function filters()
	{
		return array('accessControl');
	}

	public function accessRules()
	{
		return array(
			array('deny',
				'actions' => array('Team', 'Favorites', 'AddCategory', 'EditCategory'),
				'users' => array('?'),
			),
			array('allow',
				'actions' => array('Team', 'Favorites', 'AddCategory', 'EditCategory'),
				'users' => array('@'),
			)
		);
	}

    public function actionAddCategory()
    {
        if (!Yii::app()->request->isAjaxRequest)
            Yii::app()->end();

        $this->layout = 'upbox';
        $this->upbox_title = 'Новая категория';

        $model = new UserVendorCategories('add');
        if (isset($_POST['UserVendorCategories']))
        {
            $model->attributes = $_POST['UserVendorCategories'];
            $model->uid = User::getId();
            $model[mApi::getFunType(true).'_id'] = mApi::getFunId();

            if ($model->save()) {
                echo 'ok';
            } else {
                echo CHtml::errorSummary($model);
            }
            die();
        }
        else
        {
            $this->render('_form_category', array(
                'model' => $model
            ));
        }
    }

    public function actionEditCategory($id = 0)
    {
        if (!Yii::app()->request->isAjaxRequest)
            Yii::app()->end();

        $id = intval($id);
        if (empty($id))
            Yii::app()->end();

        $model = UserVendorCategories::model()->findByAttributes(array('uid' => User::getId(), 'id' => $id));
        if (empty($model))
            Yii::app()->end();

        $this->layout = 'upbox';
        $this->upbox_title = 'Редактирование категории';

        if (isset($_POST['UserVendorCategories']))
        {
            $model->scenario = 'edit';
            $model->attributes = $_POST['UserVendorCategories'];

            if ($model->save()) {
                echo 'ok';
            } else {
                echo CHtml::errorSummary($model);
            }
            die();
        }
        else
        {
            $this->render('_form_category', array(
                'model' => $model
            ));
        }
    }

    public function actionDeleteCategory($id = 0)
    {
        if (!Yii::app()->request->isAjaxRequest)
            Yii::app()->end();

        $id = intval($id);
        if (empty($id))
            Yii::app()->end();

        UserVendorCategories::model()->deleteAllByAttributes(array('uid' => User::getId(), 'id' => $id));
    }

    public function actionGetCategories($vendor_id)
    {
        if (User::isGuest())
            mApi::sayError(1, '<script>alert(\'Необходимо авторизироваться на сайте.\'); $.fancybox.close();</script>');

        $this->layout = 'upbox';
        $this->upbox_title = 'Выберите категорию';

        $categories_data = array();
        $team_data = array();

        if (mApi::getFunId())
        {
            $criteria = new CDbCriteria;
            $criteria->condition = '(t.uid = :uid AND t.'.mApi::getFunType(true).'_id = :fun_id)';
            $criteria->params = array(
                ':uid' => User::getId(),
                ':fun_id' => mApi::getFunId()
            );

            $categories_data = UserVendorCategories::model()->with('Team', 'Team.Vendor')->findAll($criteria);

            // Получаем список компаний и категорий в команде у пользователя, для активного торжества или свадьбы
            $user_vendor_team = UserVendorTeam::model()->findAll($criteria);

            if (!empty($user_vendor_team))
                $team_data = CHtml::listData($user_vendor_team, 'user_cat_id', 'vendor_id');
        }

        $this->render('select_category', array(
            'vendor_id' => $vendor_id,
            'categories_data' => $categories_data,
            'team_data' => $team_data
        ));
    }

    public function actionTeam()
    {
        Yii::app()->name = 'Моя команда - Мои компании - '.Yii::app()->name;

        $categories_data = array();

        if (mApi::getFunId())
        {
            $criteria = new CDbCriteria;
            $criteria->condition = '(t.uid = :uid AND t.'.mApi::getFunType(true).'_id = :fun_id)';
            $criteria->params = array(
                ':uid' => User::getId(),
                ':fun_id' => mApi::getFunId()
            );

            $categories_data = UserVendorCategories::model()->with('Team', 'Team.Vendor')->findAll($criteria);
        }

        $this->render('index', array(
            'categories_data' => $categories_data,
            'action' => 'team'
        ));
    }

    public function actionFavorites()
    {
        Yii::app()->name = 'Сохраненные - Мои компании - '.Yii::app()->name;

        $favorites_data = UserVendorFavorites::model()->with('Vendor')->findAllByAttributes(array('uid' => User::getId()));
        $user_vendor_team = array();

        if (mApi::getFunId())
        {
            // Получаем список компаний в команде у пользователя, для активного торжества или свадьбы (для отображения "в команде")
            $user_vendor_team = UserVendorTeam::model()->findAllByAttributes(array(mApi::getFunType(true).'_id' => mApi::getFunId(), 'uid' => User::getId()));

            if (!empty($user_vendor_team))
                $user_vendor_team = CHtml::listData($user_vendor_team, 'vendor_id', 'id');
        }

        $this->render('index', array(
            'favorites_data' => $favorites_data,
            'user_vendor_team' => $user_vendor_team,
            'action' => 'favorites'
        ));
    }
}