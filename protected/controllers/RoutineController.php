<?php
class RoutineController extends AppController
{
    public $layout = 'planning';
    public $upbox_title = '';

    public function filters()
    {
        return array('accessControl');
    }

    public function accessRules()
    {
        return array(
            array('deny',
                'actions' => array('Index', 'Add', 'Edit', 'Delete'),
                'users' => array('?')
            ),
            array('allow',
                'actions' => array('Index', 'Add', 'Edit', 'Delete'),
                'users' => array('@')
            )
        );
    }

    public function actionIndex()
    {
        Yii::app()->name = 'Распорядок дня - '.Yii::app()->name;

        $routine_data = array();

        if (mApi::getFunId())
            $routine_data = UserRoutine::model()->with('Vendor')->findAllByAttributes(array(
                'uid' => User::getId(),
                mApi::getFunType(true).'_id' => mApi::getFunId()
            ), array(
                'order' => 'time ASC'
            ));

        $this->render('index', array(
            'routine_data' => $routine_data
        ));
    }

    public function actionAdd()
    {
        $this->upbox_title = 'Новое событие';
        $this->layout = 'upbox';

        $model = new UserRoutine('add');
        $vendors_team = UserVendorTeam::model()->with('Vendor')->findAllByAttributes(array('uid' => User::getId(), mApi::getFunType(true).'_id' => mApi::getFunId()));

        if (isset($_POST['UserRoutine']))
        {
            $model->attributes = $_POST['UserRoutine'];
            $model->uid = User::getId();
            $model[mApi::getFunType(true).'_id'] = mApi::getFunId();

            if ($model->save()) {
                echo 'ok';
            } else {
                echo CHtml::errorSummary($model);
            }
        }
        else
        {
            $this->render('_form', array(
                'model' => $model,
                'vendors_team' => $vendors_team
            ));
        }
    }

    public function actionEdit($id = 0)
    {
        $id = intval($id);
        if (empty($id))
            Yii::app()->end();

        $this->upbox_title = 'Редактирование события';
        $this->layout = 'upbox';

        $model = UserRoutine::model()->findByAttributes(array('uid' => User::getId(), 'id' => $id));
        if (empty($model))
            Yii::app()->end();

        $vendors_team = UserVendorTeam::model()->with('Vendor')->findAllByAttributes(array('uid' => User::getId(), mApi::getFunType(true).'_id' => mApi::getFunId()));

        if (isset($_POST['UserRoutine']))
        {
            $model->scenario = 'edit';
            $model->attributes = $_POST['UserRoutine'];

            if ($model->save()) {
                echo 'ok';
            } else {
                echo CHtml::errorSymmary($model);
            }
        }
        else
        {
            $this->render('_form', array(
                'model' => $model,
                'vendors_team' => $vendors_team
            ));
        }
    }

    public function actionDelete($id = 0)
    {
        $id = intval($id);
        if (empty($id))
            Yii::app()->end();

        UserRoutine::model()->deleteAllByAttributes(array('id' => $id, 'uid' => User::getId()));
    }
}