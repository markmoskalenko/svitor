<?php
class PagesController extends AppController
{
    public $pageDescription='На свадебном портале Вы найдете все для свадьбы. Мы поможем Вам ';
    public $pageKeywords='свадьба, торжество, мероприятие, портал, ';
    public function actionIndex($name = '')
    {
        if (empty($name))
            $this->redirect('/',true,301);

        $site_page = Pages::model()->findByAttributes(array('name' => $name));

        if (empty($site_page))
            throw new CHttpException(404, 'Страница не найдена');

        Yii::app()->name = $site_page->title . ' - ' . Yii::app()->name;

        $msg = new UserMessages('new');
        $to_uid = 16;
        $to_vid = 0;

        if(isset($_SESSION["vendor__id"])){
            $to_vid = $_SESSION["vendor__id"];
        }

        if(isset($_POST['UserMessages'])){
            $msg->attributes = $_POST['UserMessages'];
            if(User::getId()){
                $msg->from_uid = User::getId();
            }else{
                $msg->from_vid = $_SESSION["vendor__id"];
            }
        }

        $this->render('index', array(
            'msg' => $msg,
            'to_uid' => $to_uid,
            'to_vid' => $to_vid,
            'site_page' => $site_page
        ));
    }
}