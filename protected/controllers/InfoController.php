<?php
class InfoController extends AppController
{
    public function actionIndex($cat = null, $view = null)
    {
        $cat = strtolower(htmlspecialchars(trim($cat)));
        $category = array();
        $dataProvider = array();
        $posts_data = array();
        $view_post = array();
        $recommendations = array();
        $comments = array();
        $comments_count = 0;
        $type = 'info';

        Yii::app()->name = 'Полезная информация - '.Yii::app()->name;

        // Выборка категории
        if (!empty($cat))
            $category = Category::model()->cache(1000)->findByAttributes(array('visible' => 1, 'name' => $cat));

        if (!empty($category))
            Yii::app()->name = $category->title.' - '.Yii::app()->name;

        if (!empty($view))
        {
            $view_id = intval($view);
            if (empty($view_id))
                $this->redirect(Yii::app()->createUrl('info'));

            $criteria = new CDbCriteria;
            $criteria->condition = 't.published = 1 AND t.status = "verified" AND t.id = :post_id';
            $criteria->order = 'date DESC';
            $criteria->params = array(':post_id' => $view_id);
            $criteria->with = array('Vendor');

            $view_post = InfoPosts::model()->find($criteria);
            if (empty($view_post))
                $this->redirect(Yii::app()->createUrl('info'));

            Yii::app()->name = $view_post->title.' - '.Yii::app()->name;

            // Читайте также
            $criteria = new CDbCriteria;
            $criteria->condition = 't.published = 1 AND t.status = "verified" AND t.cat_id = :cat_id AND t.id != :post_id';
            $criteria->params = array(
                ':cat_id' => $view_post->cat_id,
                ':post_id' => $view_post->id
            );
            $criteria->order = 'date DESC';
            $criteria->limit = 5;
            $criteria->with = array('Category');

            $recommendations = InfoPosts::model()->findAll($criteria);

            // Комментарии к статье
            $criteria = new CDbCriteria;
            $criteria->condition = 'post_id = :post_id AND status = "verified"';
            $criteria->params = array(':post_id' => $view_post->id);
            $criteria->order = 'date ASC';
            $criteria->with = array('User');

            $dataProvider = new CActiveDataProvider('InfoPostComments', array(
                'pagination' => array(
                    'pageSize' => Yii::app()->params['items_per_page'],
                    'pageVar' => 'page',
                    'params' => array_merge($_GET, array('#' => 'comments'))
                ),
                'criteria' => $criteria
            ));

            $comments_count = InfoPostComments::model()->count($criteria);
            $comments = $dataProvider->getData();
        }
        // Иначе показываем список статей
        else
        {
            // Если категория есть, то ищем в ней статьи
            if (!empty($category))
            {
                $criteria = new CDbCriteria;
                $criteria->condition = 't.published = 1 AND t.status = "verified" AND t.cat_id = :cat_id';
                $criteria->order = 'date DESC';
                $criteria->params = array(':cat_id' => $category->id);

                // Если есть что-то в поиске
                if (!empty($_GET['q']))
                {
                    $query = htmlspecialchars(trim($_GET['q']));
                    $query = str_replace('%', '\%', $query);
                    $query = str_replace('_', '\_', $query);
                    $criteria->condition .= ' AND (t.title LIKE "%'.$query.'%" OR t.full_text LIKE "%'.$query.'%")';

                    $log = new LogInfoSearch();
                    $log['ip_address'] = ip2long(CHttpRequest::getUserHostAddress());
                    $log['date'] = time();
                    $log['query_text'] = $query;
                    $log['cat_id'] = $category->id;

                    if (!User::isGuest())
                        $log['uid'] = User::getId();

                    $log->save();
                }

                $dataProvider = new CActiveDataProvider('InfoPosts', array(
                    'pagination' => array(
                        'pageSize' => Yii::app()->params['items_per_page'],
                        'pageVar' => 'page'
                    ),
                    'criteria' => $criteria
                ));
            }
            // Иначе показываем все статьи из всех разделов
            else
            {
                $criteria = new CDbCriteria;
                $criteria->condition = 't.published = 1 AND t.status = "verified"';
                $criteria->order = 'date DESC';
                $criteria->with = array('Category');

                // Если есть что-то в поиске
                if (!empty($_GET['q']))
                {
                    $query = htmlspecialchars(trim($_GET['q']));
                    $query = str_replace('%', '\%', $query);
                    $query = str_replace('_', '\_', $query);
                    $criteria->condition .= ' AND (t.title LIKE "%'.$query.'%" OR t.full_text LIKE "%'.$query.'%")';

                    $log = new LogInfoSearch();
                    $log['ip_address'] = ip2long(CHttpRequest::getUserHostAddress());
                    $log['date'] = time();
                    $log['query_text'] = $query;

                    if (!User::isGuest())
                        $log['uid'] = User::getId();

                    $log->save();
                }

                $dataProvider = new CActiveDataProvider('InfoPosts', array(
                    'pagination' => array(
                        'pageSize' => Yii::app()->params['items_per_page'],
                        'pageVar' => 'page'
                    ),
                    'criteria' => $criteria
                ));
            }

            $posts_data = $dataProvider->getData();
        }

        $this->render('index', array(
            'type' => $type,
            'category' => $category,
            'posts' => $posts_data,
            'dataProvider' => $dataProvider,

            'view_post' => $view_post,
            'recommendations' => $recommendations,
            'comments' => $comments,
            'comments_count' => $comments_count
        ));
    }

    public function actionAddComment()
    {
        if (User::isGuest())
            Yii::app()->end();

        $this->layout = 'ajax';

        $model = new InfoPostComments('add');
        if (isset($_POST['Comment']))
        {
            // Проверим существует ли статья
            $post_id = intval($_POST['Comment']['post_id']);
            if (!$post = InfoPosts::model()->findByPk($post_id))
                Yii::app()->end();

            $model->attributes = $_POST['Comment'];
            $model['post_id'] = $post->id;
            $model['uid'] = User::getId();
            $model['date'] = time();

            if ($model->save()) {
                echo '{"status":"ok"}';
            } else {
                echo '{"status":"error", "msg":"'.str_replace('"', '\"', CHtml::error($model, 'comment')).'"}';
            }
        }
    }

    public function actionRemoveComment($id = 0)
    {
        $id = intval($id);
        if (empty($id))
            Yii::app()->end();

        if (User::isGuest())
            Yii::app()->end();

        $criteria = new CDbCriteria;
        $criteria->condition = 'uid = :uid AND id = :id';
        $criteria->params = array(':uid' => User::getId(), ':id' => $id);

        if (InfoPostComments::model()->updateAll(array('deleted' => 1), $criteria))
            echo 'ok';
    }
}