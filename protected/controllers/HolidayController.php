<?php
class HolidayController extends AppController
{
    public $defaultAction = 'index';
    public $layout = 'planning';
    public $upbox_title = '';

    public function actionIndex()
    {
        if (User::isGuest())
            $this->redirect('/user/login/');

        if (mApi::getFunType() != 1)
            $this->redirect('/dashboard/');

        Yii::app()->name = 'Торжества - '.Yii::app()->name;

        $data = UserHolidays::model()->findAllByAttributes(array('uid' => User::getId()), array('order' => 'date ASC'));
        $this->render('index', array(
            'data' => $data
        ));
    }

    public function actionSelect()
    {
        $this->upbox_title = 'Выберите торжество';
        $this->layout = 'upbox';

        if (User::isGuest())
            $this->redirect('/user/login/');

        $data = UserHolidays::model()->findAllByAttributes(array('uid' => User::getId()), array('order' => 'date ASC'));
        $this->render('select', array('data' => $data));
    }

    public function actionAdd()
    {
        if (User::isGuest() || mApi::getFunType() != 1)
            Yii::app()->end();

        $this->upbox_title = 'Новое торжество';
        $this->layout = 'upbox';

        $holiday = new UserHolidays('add');

        if (isset($_POST['UserHolidays']))
        {
            $holiday->attributes = $_POST['UserHolidays'];
            $holiday->uid = User::getId();

            if ($holiday->validate())
            {
                $date = explode('.', $holiday->date);
                $holiday->date = mktime(00, 00, 00, $date[1], $date[0], $date[2]);

                if ($holiday->date <= time())
                {
                    $holiday->addError('date', 'Дата должна быть старше текущей');
                    echo CHtml::errorSummary($holiday);
                }

                if (!$holiday->hasErrors() && $holiday->save(false))
                {
                    // Создаем сайт торжества
                    $usite = new UserSite('add');
                    $usite['uid'] = User::getId();
                    $usite['title'] = $holiday->title;
                    $usite['holiday_id'] = $holiday->id;
                    $usite['date'] = $holiday->date;
                    $usite->save();

                    // Заполнение категорий (из category_miniature)
                    $criteria = new CDbCriteria;
                    $criteria->condition = '(fun_type = "holiday" OR fun_type = "multi") AND show_in_vendors = 1';
                    $categories = CategoryMiniatures::model()->findAll($criteria);

                    if (!empty($categories))
                    {
                        foreach ($categories as $category)
                        {
                            if (empty($category['title']) || empty($category['name']) || empty($category['icon']))
                                continue;

                            $cat = new UserVendorCategories('add');
                            $cat['name'] = $category->name;
                            $cat['title'] = $category->title;
                            $cat['icon'] = $category->icon;
                            $cat['cat_link'] = $category->cat_link;
                            $cat['uid'] = User::getId();
                            $cat['holiday_id'] = $holiday->id;
                            $cat->save(false);
                        }
                    }

                    echo 'ok';
                }
            }
            else
            {
                echo CHtml::errorSummary($holiday);
            }
        }
        else
        {
            $this->render('_form', array(
                'holiday' => $holiday
            ));
        }
    }

    public function actionEdit($id = 0)
    {
        if (User::isGuest() || mApi::getFunType() != 1)
            Yii::app()->end();

        $this->upbox_title = 'Редактирование торжества';
        $this->layout = 'upbox';

        $id = intval($id);
        if (empty($id))
            Yii::app()->end();

        $holiday = UserHolidays::model()->findByAttributes(array('uid' => User::getId(), 'id' => $id));
        if (empty($holiday))
            Yii::app()->end();

        if (isset($_POST['UserHolidays']))
        {
            $check_willdate = true;
            $old_date = date('d.m.Y', $holiday->date);

            $holiday->scenario = 'edit';
            $holiday->attributes = $_POST['UserHolidays'];

            if ($holiday->validate())
            {
                // Если дата не изменилась, то НЕ проверяем ее (дата должна быть > текущей)
                // потому что дата может быть == текущей (просто наступил этот день)
                if ($old_date == $holiday->date)
                    $check_willdate = false;

                $date = explode('.', $holiday->date);
                $date = mktime(00, 00, 00, $date[1], $date[0], $date[2]);

                if ($check_willdate && $date <= time())
                {
                    $holiday->addError('date', 'Дата должна быть старше текущей');
                    echo CHtml::errorSummary($holiday);
                }

                $holiday->date = $date;

                if (!$holiday->hasErrors() && $holiday->save(false))
                    echo 'ok';

                // Обновим название сайта, в соответствии с названием торжества
                UserSite::model()->updateAll(array('title' => $holiday->title, 'date' => $holiday->date), 'uid = '.User::getId().' AND holiday_id = '.$holiday->id);

                // Если редактировалось активное торжество, то после сохранения обновим данные в сессии
                $active_holiday = mApi::getHoliday();
                if (!empty($active_holiday) && $active_holiday->id == $holiday->id)
                {
                    Yii::app()->user->setState('holiday', $holiday);
                }
            }
            else
            {
                echo CHtml::errorSummary($holiday);
            }
        }
        else
        {
            $this->render('_form', array(
                'holiday' => $holiday
            ));
        }
    }
}