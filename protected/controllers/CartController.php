<?php
/**
 * Created by JetBrains PhpStorm.
 * User: it-Yes
 * Date: 25.07.13
 * Time: 15:44
 * To change this template use File | Settings | File Templates.
 */

class CartController extends AppController
{
    public function filters()
    {
        return array('accessControl');
    }

    public function accessRules()
    {
        return array(
            array(
                'deny',
                'actions' => array('AddCart', 'DeleteCart', 'UpdateCart', 'Orders', 'ViewOrder', 'ViewOrder',
                                   'EditOrderContact', 'EditOrderProduct', 'SaveProduct', 'DeleteProduct', 'ConfirmOrder', 'MyOrders'),
                'users'   => array('?')
            ),
            array(
                'allow',
                'actions' => array('AddCart', 'DeleteCart', 'UpdateCart', 'Orders', 'ViewOrder', 'ViewOrder',
                                   'EditOrderContact', 'EditOrderProduct', 'SaveProduct', 'DeleteProduct', 'ConfirmOrder', 'MyOrders'),
                'users'   => array('@')
            )
        );
    }

    /**
     * Добавить в корзину товар | AJAX |
     *
     * @param $id
     */
    public function actionAddCart($id)
    {
        $id      = intval($id);
        $product = Product::model()->find('id = :id', array(':id' => $id));
        Yii::app()->shoppingCart->put($product);
        echo Yii::app()->shoppingCart->getCount();
        if(!Yii::app()->request->isAjaxRequest) $this->redirect('/cart/orders'); else Yii::app()->end();
    }

    /**
     * Удалить из корзины товар | AJAX |
     *
     * @param $id
     */
    public function actionDeleteCart($id)
    {
        Yii::app()->shoppingCart->remove(  Product::model()->findByPk(intval( $id ))->getId() );
        echo Yii::app()->shoppingCart->getCount();
        if(!Yii::app()->request->isAjaxRequest) $this->redirect('/cart/orders'); else Yii::app()->end();
    }

    public function actionUpdateCart($id, $quantity)
    {
        Yii::app()->shoppingCart->update(  Product::model()->findByPk(intval( $id )), $quantity );

        if(!Yii::app()->request->isAjaxRequest) $this->redirect('/cart/orders'); else Yii::app()->end();
    }

    /**
     * Корзина заказов
     */
    public function actionOrders()
    {
        $dataProvider = $this->normalizeOrders(Yii::app()->shoppingCart->getPositions());
        $this->render(
            'order',
            array(
                 'orders' => $dataProvider
            )
        );
    }

    public function actionViewOrder( $id ){
        $model = Orders::model()->findByPk($id);
        if(count($model) && count($model->bondOrders)){
            $this->render('viewOrder', array(
                     'model' => $model
                )
            );
        }
    }

    /**
     * Оформление заказа
     */
    public function actionCreateOrder( $vendor_id = null )
    {
        $model = new Orders();

        if ( $attributes = Yii::app()->request->getPost( 'Orders' ) ) {

            $model->attributes = $attributes;


            if ($model->validate() && $model->save()) {

                BondOrders::saveOrderPosition( $model->id, $vendor_id );

                $this->redirect($this->createUrl('/cart/confirmOrder', array('order_id'=>$model->id)));

            }
        }

        if( $vendor_id === null || !is_numeric($vendor_id) ) $this->redirect('/cart/orders');

        $model->vendor_id = intval( $vendor_id );

        $this->render( 'createOrder', array(
                'model' => $model
        ));
    }

    public function actionEditOrderContact( $order_id ){

        $model = Orders::model()->my()->notConfirmed()->findByPk( $order_id );

        if( !count($model) ) $this->redirect('/cart/myOrders');

        if ( $attributes = Yii::app()->request->getPost( 'Orders' ) ) {

            $model->attributes = $attributes;

            if ($model->validate() && $model->save()) {

                $this->redirect($this->createUrl('/cart/confirmOrder',array('order_id'=>$order_id)));

            }
        }

        $this->render( 'createOrder', array(
                                           'model' => $model
                                      ));
    }

    public function actionEditOrderProduct( $order_id ){

        $model = Orders::model()->my()->notConfirmed()->findByPk( $order_id );

        if( !count($model) ) $this->redirect('/cart/myOrders');

        $this->render( 'editOrderProduct', array(
                                           'model' => $model
                                      ));
    }

    /**
     * Метод вызывается по средствам AJAX запроса.
     * Реализует безопасное изменения атрибутов товара который заказан, но заказ не подтвержден
     */
    public function actionSaveProduct(){

        if (Yii::app()->request->getPost('order_id') && Yii::app()->request->getPost('product_id') && Yii::app()->request->isAjaxRequest) {

            $order_id   = intval(Yii::app()->request->getPost('order_id'));

            $product_id = intval(Yii::app()->request->getPost('product_id'));

            unset($_POST['order_id']);

            unset($_POST['product_id']);

            unset($_POST['id']);

            if ( !Orders::isEdit( $order_id ) ) { echo 0; Yii::app()->end(); }

            $model             = BondOrders::model()->findByPk($product_id);

            $model->attributes = $_POST;

            $model->save();

        }

        echo CJSON::encode($model->attributes);

        Yii::app()->end();
    }

    public function actionDeleteProduct( $id ){
        $productModel = BondOrders::model()->findByPk( $id );
        if( $productModel->order->user_id == User::getId() && $productModel->order->status == Orders::STATUS_NOT_CONFIRMED ){
            $productModel->delete();
        }
        return false;
    }

    /**
     * Подтверждение заказа
     * @param $order_id
     */
    public function actionConfirmOrder( $order_id, $confirm = false ){

        $model = Orders::model()->my()->notConfirmed()->findByPk( $order_id );

        if( !count($model) ) $this->redirect('/cart/myOrders');

        if( $confirm ){

            $model->status = Orders::STATUS_QUEUED;

            $model->save();

            $this->redirect('/cart/myOrders');

        }

        $this->render( 'confirmOrder', array(
                'model' => $model,
               )
        );
    }

    /**
     * Мои заказы
     */
    public function actionMyOrders(){
        if( isset($_GET['status']) && $_GET['status'] == Orders::STATUS_CLOSED )
            $model = Orders::model()->my()->closed()->findAll();
        else
            $model = Orders::model()->my()->notClosed()->findAll();

        $this->render('myOrder', array(
                                    'model' => $model,
                               ));

    }

    /**
     * Нормализация формата данных для вьюшки
     *
     * Разделяем товары по продовцам
     *
     * @param array $orders
     *
     * @return array
     */
    private function normalizeOrders($orders = array())
    {
        $result = array();
        foreach ($orders as $order) {
            $result[$order->Vendor->id]['vendor']         = $order->Vendor;
            $result[$order->Vendor->id]['price']          = isset($result[$order->Vendor->id]['price']) ? $result[$order->Vendor->id]['price'] + $order->getSumPrice() : $order->getSumPrice();
            $result[$order->Vendor->id]['dataProvider'][] = $order;
        }
        return $result;
    }
}