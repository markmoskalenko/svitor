<?php
class MessagesController extends AppController
{
    public $upbox_title;

    public function filters()
    {
        return array('accessControl');
    }

    public function accessRules()
    {
        return array(
            array('deny',
                'actions' => array('Index', 'View', 'Reply', 'Delete', 'DeleteSelected'),
                'users' => array('?')
            ),
            array('allow',
                'actions' => array('Index', 'View', 'Reply', 'Delete', 'DeleteSelected'),
                'users' => array('@')
            )
        );
    }

    public function actionIndex($folder = 'inbox')
    {
        Yii::app()->name = 'Сообщения - '.Yii::app()->name;

        if ($folder != 'inbox' && $folder != 'outbox' && $folder != 'bid')
            $folder = 'inbox';

        $criteria = new CDbCriteria;

        // Входящие
        if ($folder == 'inbox')
        {
            $criteria->condition = 'is_bid = 0 AND to_uid = :uid AND to_status != "deleted"';
            $criteria->order = 'date DESC';
            $criteria->params = array(':uid' => User::getId());
            $criteria->with = array('FromUser', 'FromVendor');
        }
        // Исходящие
        else if ($folder == 'outbox')
        {
            $criteria->condition = 'is_bid = 0 AND from_uid = :uid AND from_status != "deleted"';
            $criteria->order = 'date DESC';
            $criteria->params = array(':uid' => User::getId());
            $criteria->with = array('ToUser', 'ToVendor');
        }else if ($folder == 'bid')
        {
            $criteria->condition = 'to_uid = :uid AND to_status != "deleted"';
            $criteria->order = 'date DESC';
            $criteria->params = array(':uid' => User::getId());

            if(Yii::app()->request->getParam('bid_id') && Yii::app()->request->getParam('bid_id') != 0){
                $criteria->addCondition('is_bid = :bid_id');
                $criteria->params[':bid_id'] = intval(Yii::app()->request->getParam('bid_id'));
            }else $criteria->addCondition('is_bid != 0');

            $criteria->with = array('FromUser', 'FromVendor');
        }

        $dataProvider = new CActiveDataProvider('UserMessages', array(
            'pagination' => array(
                'pageSize' => Yii::app()->params['items_per_page'],
                'pageVar' => 'page'
            ),
            'criteria' => $criteria
        ));

        $this->render('index', array(
            'messages' => $dataProvider->getData(),
            'dataProvider' => $dataProvider,

            'folder' => $folder
        ));
    }


    public function actionNew()
    {
        $this->layout = 'upbox';
        $this->upbox_title = (Yii::app()->request->getParam('title') ? Yii::app()->request->getParam('title') : 'Новое сообщение');

        $to_uid = 0;
        $to_vid = 0;

        if (User::isGuest() && !isset($_SESSION["vendor__id"]))
            mApi::sayError(1, '<script>$.fancybox.close(); alert("Необходимо авторизироваться на сайте.");</script>');

        if (!empty($_GET['uid']))
            $to_uid = $_GET['uid'];

        if (!empty($_GET['bid']))
            $bid = intval($_GET['bid']);
        else $bid = 0;

        if (!empty($_GET['vid']))
            $to_vid = $_GET['vid'];

        if (empty($to_uid) && empty($to_vid))
            mApi::sayError(1, 'Не указан адресат');

        if (!empty($to_vid)) {
            $to_vid = Vendor::model()->findByPk($to_vid);
        } else {
            $to_uid = User::model()->findByPk($to_uid);
        }

        if (empty($to_uid) && empty($to_vid))
            mApi::sayError(1, 'Не известный адресат');

        $msg = new UserMessages('new');

        if (isset($_POST['UserMessages']))
        {
            $msg->attributes = $_POST['UserMessages'];
            if(isset($_SESSION["vendor__id"])){
                $msg->from_vid = $_SESSION["vendor__id"];
            }else{
                $msg->from_uid = User::getId();
            }
            $msg->to_uid = !empty($to_uid) ? $to_uid->id : 0;

            $msg->to_vid = !empty($to_vid) ? $to_vid->id : 0;

            $msg->is_bid = $bid;

            if ($msg->save())
            {
                $from = User::model()->findByPk( $msg->from_uid );
                // Если у получателя включены уведомления по Email - уведомим его о том, что ему прешло сообщение.
                $to = !empty($to_vid) ? $to_vid : $to_uid;

                if ($to->notify_new_msg){
                    UserMessages::sendEmailNotify($to, $from, $msg);
                }

                echo 'ok';
            }
            else {
                echo CHtml::errorSummary($msg);
            }
        }
        else
        {
            $this->render('_form_new', array(
                'msg' => $msg,

                'to_uid' => $to_uid,

                'bid'    => $bid,

                'to_vid' => $to_vid
            ));
        }
    }

    public function actionReply($id = 0)
    {
        $id = intval($id);

        // Для ответа на входящее
        $reply_msg = UserMessages::model()->findByAttributes(array('to_uid' => User::getId(), 'id' => $id));

        // Для ответа на свое же сообщение т.е. исходящее
        if (empty($reply_msg))
            $reply_msg = UserMessages::model()->findByAttributes(array('from_uid' => User::getId(), 'id' => $id));

        if (empty($reply_msg) || empty($_POST['message']))
            mApi::sayError(1, '0');

        $msg = new UserMessages('new');

        // Ответ пользователю
        if ($reply_msg->to_uid == User::getId() && $reply_msg->from_uid != 0)
        {
            $msg['to_uid'] = $reply_msg->from_uid;
        }
        // Ответ компании
        else if ($reply_msg->to_uid == User::getId() && $reply_msg->from_vid != 0){
            $msg['to_vid'] = $reply_msg->from_vid;
        }
        // Ответ на свое, ранее отправленное сообщение для ПОЛЬЗОВАТЕЛЯ
        else if ($reply_msg->to_uid != 0 && $reply_msg->from_uid == User::getId()) {
            $msg['to_uid'] = $reply_msg->to_uid;
        }
        // Ответ на свое, ранее отправленное сообщение для КОМПАНИИ
        else if ($reply_msg->to_vid != 0 && $reply_msg->from_uid == User::getId()) {
            $msg['to_vid'] = $reply_msg->to_vid;
        }

        $msg['from_uid'] = User::getId();

        if (!preg_match('/^[Re:]/', $reply_msg->title)) {
            $msg['title'] = 'Re: '.$reply_msg->title;
        } else {
            $msg['title'] = $reply_msg->title;
        }

        $msg['message'] = $_POST['message'];
        $msg->is_bid = 0;
        if ($msg->save())
        {
            $from = User::model()->findByPk($msg->from_uid);

            // Если у получателя включены уведомления по Email - уведомим его о том, что ему прешло сообщение.
            if (!empty($msg->to_vid)) {
                $to = Vendor::model()->findByPk($msg->to_vid);
            } else {
                $to = User::model()->findByPk($msg->to_uid);
            }

            if ($to->notify_new_msg)
                UserMessages::sendEmailNotify($to, $from, $msg);

            echo 'ok';
        }
        else {
            echo CHtml::errorSummary($msg);
        }
    }

    public function actionSupport()
    {
        $this->upbox_title = 'Новое сообщение';

        $to_uid = 16;
        $to_vid = 0;
        $msg = new UserMessages('new');
        if(isset($_SESSION["vendor__id"])){
            $to_vid = $_SESSION["vendor__id"];
        }else{
            $to_vid  = User::getId();
        }

        $this->render('_support', array(
            'msg' => $msg,
            'to_uid' => $to_uid,
            'to_vid' => $to_vid
        ));
    }

    public function actionView($id = 0)
    {
        Yii::app()->name = 'Сообщения - '.Yii::app()->name;

        $id = intval($id);

        $criteria = new CDbCriteria;
        $criteria->condition = '(to_uid = :uid OR from_uid = :uid) AND t.id = :id';
        $criteria->params = array(
            ':uid' => User::getId(),
            ':id' => $id
        );
        $criteria->with = array('FromUser', 'FromVendor');

        $message = UserMessages::model()->find($criteria);

        if (empty($message))
            $this->redirect(Yii::app()->createUrl('messages'),true,301);

        // Если сообщение помеченно как "удаленное" то не показываем его
        if ($message->to_uid == User::getId() && $message->to_status == 'deleted' ||
            $message->from_uid == User::getId() && $message->from_status == 'deleted')
        {
            $this->redirect(Yii::app()->createUrl('messages'),true,301);
        }

        // Если это новое сообщение для пользователя, то при меням статус на "прочитано"
        if ($message->to_uid == User::getId() && $message->to_status == 'new')
        {
            $message->to_status = 'readed';
            $message->update();
        }

        $this->render('view', array(
            'message' => $message
        ));
    }

    public function actionDelete($id = 0)
    {
        $id = intval($id);

        $criteria = new CDbCriteria;
        $criteria->condition = '(to_uid = :uid OR from_uid = :uid) AND id = :id';
        $criteria->params = array(':uid' => User::getId(), ':id' => $id);

        $msg = UserMessages::model()->find($criteria);
        if (!empty($msg))
        {
            // Если это "входящее" сообщение
            if ($msg->to_uid == User::getId()) {
                $msg['to_status'] = 'deleted';
            }
            // Если "отправленное"
            else if ($msg->from_uid == User::getId()) {
                $msg['from_status'] = 'deleted';
            }
            $msg->update();
        }
    }

    public function actionDeleteSelected($folder = 'inbox')
    {
        if (empty($_POST['msg']))
            mApi::sayError(1, 'Нечего удалять...');

        if ($folder != 'inbox' && $folder != 'outbox' && $folder != 'bid')
            mApi::sayError(1, 'Ошибка при удалении...');

        $selected_msg = array();
        foreach ($_POST['msg'] as $i => $id)
            $selected_msg[] = $id;

        if ($folder == 'inbox')
        {
            $criteria = new CDbCriteria;
            $criteria->condition = 'to_uid = :uid';
            $criteria->params = array(':uid' => User::getId());
            $criteria->addInCondition('id', $selected_msg);

            UserMessages::model()->updateAll(array('to_status' => 'deleted'), $criteria);
        }
        else if ($folder == 'outbox')
        {
            $criteria = new CDbCriteria;
            $criteria->condition = 'from_uid = :uid';
            $criteria->params = array(':uid' => User::getId());
            $criteria->addInCondition('id', $selected_msg);

            UserMessages::model()->updateAll(array('from_status' => 'deleted'), $criteria);
        }else if ($folder == 'bid')
        {
        $criteria = new CDbCriteria;
        $criteria->condition = 'to_uid = :uid';
        $criteria->params = array(':uid' => User::getId());
        $criteria->addInCondition('id', $selected_msg);

        UserMessages::model()->updateAll(array('to_status' => 'deleted'), $criteria);
    }
    }
}