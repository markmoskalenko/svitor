<?php
class DemoController extends AppController
{
    public function filters()
    {
        return array('accessControl');
    }

    public function accessRules()
    {
        return array(
            array('deny',
                  'actions'=>array('Save'),
                  'users'=>array('?'),
            ),
            array('allow',
                  'actions'=>array('Save'),
                  'users'=>array('testsvitor'),
            ),
            array('deny',
                  'actions'=>array('Save'),
                  'users'=>array('*'),
            ),
        );
    }

    public function actionSave()
    {
        MigrateDB::getInstance()->start();
        $this->redirect('/dashboard');
    }

}
