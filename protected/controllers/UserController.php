<?php
class UserController extends AppController
{
    public $defaultAction = 'profile';

    public function actions()
    {
        return array(
            'captcha' => array(
                'class' => 'CCaptchaAction',
                'backColor' => 0xf0f0f0,
                'foreColor' => 0x545454,
                'width' => 175,
                'height' => 55
            ),
        );
    }

    /*
     * Авторизация через сторонние сайты (vk, facebook, twitter...etc)
     */
    public function actionAuth($name = '')
    {
        if (!User::isGuest())
            $this->redirect(Yii::app()->createUrl('/'));

        $canDoLogin = true;
        $service_name = strtolower($name);
        $duration = 3600*24*30; // 30 days

        if (empty($service_name) || !isset(Yii::app()->eauth->services[$service_name]))
            $this->redirect('/user/login');

        $authIdentity = Yii::app()->eauth->getIdentity($service_name);
        $authIdentity->redirectUrl = Yii::app()->user->returnUrl;
        $authIdentity->cancelUrl = $this->createAbsoluteUrl('user/login');

        try
        {
            if ($authIdentity->authenticate())
            {
                $user = User::model()->findByAttributes(array($service_name . '_id' => $authIdentity->id));

                // Если пользователь не найден — регистрируем.
                if (empty($user))
                {
                    $canDoLogin = false;

                    $user = new User('auth_service');
                    $user->attributes = $authIdentity->getAttributes();

                    // Проверим не занят ли логин
                    if (User::model()->findByAttributes(array('login' => $user->login)))
                        $user['login'] = $user->login . rand(0,10);

                    $user[$service_name . '_id'] = $authIdentity->id;

                    if ($user->save())
                        $canDoLogin = true;
                }

                // Авторизируем
                if ($canDoLogin)
                {
                    $identity = new UserIdentity($user->login, ''); // Пароль не важен

                    if ($identity->auth($user)) {
                        Yii::app()->user->login($identity, $duration);
                        $authIdentity->redirect('/dashboard');
                    }
                    else {
                        Yii::app()->user->setFlash('auth_error', 1);
                        $authIdentity->redirect($authIdentity->cancelUrl);
                    }
                }
            }
        } catch (EAuthException $e) {
            Yii::app()->user->setFlash('auth_error_msg', $e->getMessage());
        }

        Yii::app()->user->setFlash('auth_error', 1);
        $authIdentity->redirect($authIdentity->cancelUrl);
    }

    public function actionLogin()
    {
        if (!User::isGuest())
            $this->redirect('/');

        Yii::app()->name = 'Вход - '.Yii::app()->name;

        $error = '';

        if (!empty($_POST['email']) && !empty($_POST['password']))
        {
            $email = htmlspecialchars(trim($_POST['email']));
            $password = htmlspecialchars(trim($_POST['password']));
            $duration = (!empty($_POST['rememberMe'])) ? 3600*24*30 : 0; // 30 days
            $identity = new UserIdentity($email, $password);

            if ($identity->auth())
            {
                Yii::app()->user->login($identity, $duration);
                $this->redirect('/dashboard/');
            } else {
                $error = 'неверный логин или пароль.';
            }
        }

        $this->render('_form_login', array('error' => $error));
    }

    public function actionLogout()
    {
        if (!User::isGuest())
            Yii::app()->user->logout();

        $this->redirect('/');
    }

    public function actionRegistration()
    {
        if (!User::isGuest())
            $this->redirect('/');

        Yii::app()->name = 'Регистрация - '.Yii::app()->name;

        $user = new User('registration');

        if (isset($_POST['User']))
        {
            $user->attributes = $_POST['User'];

            // Возьмем из сессии реоигнальные настойки, чтобы юзеру не пришлось их опять выбирать после регистрации
            $user->country_id = Yii::app()->user->getState('country_id', null);
            $user->region_id = Yii::app()->user->getState('region_id', null);
            $user->city_id = Yii::app()->user->getState('city_id', null);

            if ($user->save())
            {
                $identity = new UserIdentity($user->email, '');
                if ($identity->auth($user))
                {
                    Yii::app()->user->login($identity, 3600*24*30);
                    $this->redirect('/dashboard/');
                }
            }
        }

        if (Yii::app()->request->isAjaxRequest && !empty($_GET['check_email']))
        {
            $this->layout = 'ajax';
            if (User::model()->find('LOWER(email)=?', array(strtolower($_GET['check_email'])))) {
                echo 1;
            } else {
                echo 0;
            }
            die();
        }

        $this->render('_form_registration', array(
                                                 'user' => $user
                                            ));
    }

    public function actionForgotpassword()
    {
        if (!User::isGuest())
            $this->redirect('/',true,301);

        Yii::app()->name = 'Восстановление пароля - '.Yii::app()->name;

        $error = '';
        if (!empty($_POST['email'])){

            $user = User::model()->find('LOWER(email)=?', array(strtolower($_POST['email'])));

            if ($user) {
                $newPassword = $this->rand_str(7);
                $user->password = hash('md5', $user->salt.strtoupper($newPassword));
                if ($user->save())
                {
                    $postArr = array(
                        "title" => "Ваш новый пароль!",
                        "message" => $newPassword."<br><br>Вы можете поменять присланный пароль на удобный вам. Войдите с новым паролем и перейдите в меню Настройки (правый верхний угол)."
                    );
                    $this->sendMail($postArr,$_POST['email']);
                }
                $this->render('message', array(
                                              'error' => $error
                                         ));

            } else {
                $error = "Пользователя с таким email не существует";
                $this->render('forgotpassword', array(
                                                     'error' => $error
                                                ));
            }
        }else{
            $this->render('forgotpassword', array(
                                                 'error' => $error
                                            ));
        }

    }

    public function actionSettings()
    {
        Yii::app()->name = 'Мои настройки - '.Yii::app()->name;

        $this->render('settings');
    }

    public function actionNotifications()
    {
        Yii::app()->name = 'Уведомления - Мои настройки - '.Yii::app()->name;

        if (User::isGuest())
            $this->redirect('/user/login/');

        $success = '';
        $model = User::getInfo();
        $model->scenario = 'notify';

        if (isset($_POST['User']))
        {
            $model->attributes = $_POST['User'];
            if ($model->save())
                $success = 'Изменения успешно сохранены.';
        }

        $this->render('_form_notification', array(
                                                 'model' => $model,
                                                 'success' => $success
                                            ));
    }

    public function actionPersonalInfo()
    {
        Yii::app()->name = 'Персональная информация - Мои настройки - '.Yii::app()->name;

        if (User::isGuest())
            $this->redirect('/user/login/');

        $success = '';
        $model = User::getInfo();

        if (isset($_POST['User']))
        {
            $old_img_id = $model->img_id;
            $model->scenario = 'edit';
            $model->attributes = $_POST['User'];

            // Если изменили картинку, то старой присвоим статус "удалено"
            if ($old_img_id != $model->img_id)
                FileImages::model()->updateAll(array('is_deleted' => 1), 'id ='.$old_img_id);

            if ($model->save())
            {
                Yii::app()->user->setState('img_id', $model->img_id);
                Yii::app()->user->setState('img_filename', $model->img_filename);
                $success = 'Изменения успешно сохранены.';
            }
        }

        $this->render('_form_personalinfo', array(
                                                 'model' => $model,
                                                 'success' => $success
                                            ));
    }

    public function actionChangePassword()
    {
        Yii::app()->name = 'Изменение пароля - Мои настройки - '.Yii::app()->name;

        if (User::isGuest())
            $this->redirect('/user/login/');

        $success = '';
        $user = User::getInfo();

        if (isset($_POST['User']))
        {
            $user->scenario = 'change_password';
            $user->attributes = $_POST['User'];
            if ($user->validate())
            {
                $user['password'] = $user->new_password;
                if ($user->save(false))
                {
                    $user['old_password'] = '';
                    $user['new_password'] = '';
                    $user['confirm_new_password'] = '';
                    $success = 'Пароль успешно изменен.';
                }
            }
        }

        $this->render('_form_changepassword', array(
                                                   'user' => $user,
                                                   'success' => $success
                                              ));
    }

    public function actionProfile($id = 0)
    {
        $id = intval($id);
        $friend_invite = '';

        // ID не указан, если авторизирован, показываем свой профиль
        if (empty($id) && !User::isGuest())
            $id = User::getId();

        // Если ID не указан и смотрит гость, то редиректим на главную.
        if (empty($id) && User::isGuest())
            $this->redirect('/');

        $profile_info = User::model()->findByPk($id);

        if (empty($profile_info))
            $this->redirect('/');

        // Если просматривается не свой профайл
        if (!User::isGuest() && $profile_info->id != User::getId())
        {
            $friend_status = UserFriends::model()->findByAttributes(array('uid' => User::getId(), 'fid' => $profile_info->id));
            $friend_invite = UserFriends::model()->findByAttributes(array('uid' => $profile_info->id, 'fid' => User::getId()));
        }

        // 12 случайных друзей и общее количество
        $condition = 'uid = ' . $profile_info->id . ' AND status = "friend"';

        $friends = UserFriends::model()->with('User')->findAll(array(
                                                                    'condition' => $condition,
                                                                    'limit' => 12,
                                                               ));

        $total_friends_count = UserFriends::model()->count($condition);

        $this->render('profile', array(
                                      'profile_info' => $profile_info,

                                      'counters' => array(
                                          'total_wedding' => $profile_info['TotalWeddingCount'],
                                          'total_holiday' => $profile_info['TotalHolidayCount'],

                                          'held_wedding' => $profile_info['HeldWeddingCount'],
                                          'held_holiday' => $profile_info['HeldHolidayCount'],

                                          'planning_wedding' => intval($profile_info['TotalWeddingCount'] - $profile_info['HeldWeddingCount']),
                                          'planning_holiday' => intval($profile_info['TotalHolidayCount'] - $profile_info['HeldHolidayCount']),
                                      ),

                                      'friend_status' => !empty($friend_status) ? $friend_status->status : null,
                                      'friend_invite' => $friend_invite,
                                      'friends' => $friends,
                                      'total_friends_count' => $total_friends_count
                                 ));
    }

    private function sendMail($arr,$email) {

        $subject = "Восстановление пароля - svitor.ru";
        $headers = "Content-Type: text/html; charset=UTF-8"."\r\n";
        $headers .= "Subject: {$subject}"."\r\n";
        $headers .= 'From: Василиса Свитрова <noreply@svitor.ru>' . "\r\n" .
          'Reply-To: noreply@svitor.ru' . "\r\n";


        // Заполняем нужными данными
        $body = "<h2>".$arr['title']."</h2>";
        $body .= $arr['message']."<br/>";
        $body .= "С наилучшими пожеланиями, Svitor.ru ";

        mail($email, $subject, $body, $headers);

    }

    private function rand_str($length = 32, $chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890')
    {
        $chars_length = (strlen($chars) - 1);

        $string = $chars{rand(0, $chars_length)};

        for ($i = 1; $i < $length; $i = strlen($string))
        {
            $r = $chars{rand(0, $chars_length)};
            if ($r != $string{$i - 1}) $string .=  $r;
        }
        return $string;
    }
}