<?php
class GuestsController extends AppController
{
    public $defaultAction = 'index';
    public $layout = 'planning';
    public $upbox_title = '';

    public function filters()
    {
        return array('accessControl');
    }

    public function accessRules()
    {
        return array(
            array('deny',
                  'actions' => array('Index', 'Add', 'Edit', 'Delete', 'Notified', 'Seating'),
                  'users' => array('?')
            ),
            array('allow',
                  'actions' => array('Index', 'Add', 'Edit', 'Delete', 'Notified', 'Seating'),
                  'users' => array('@')
            )
        );
    }

    public function actionIndex()
    {
        Yii::app()->name = 'Список гостей - '.Yii::app()->name;

        $data = array();
        $sort = array();
        $all_count = 0;
        $notified_count = 0;

        if (mApi::getFunId())
        {
            $criteria = new CDbCriteria;
            $criteria->condition = 'uid = :uid AND '.mApi::getFunType(true).'_id = '.mApi::getFunId();
            $criteria->params = array(':uid' => User::getId());

            $sort = new MSort;
            $sort->attributes = array(
                'category' => 'guest_cat_id',
                'fullname',
                'email',
                'phone' => 'phone_number',
                'menu' => 'menu_type_id',
                'notified'
            );

            $sort->defaultOrder = 'fullname ASC';
            $sort->defaultLabel = array('fullname', 'asc');
            $sort->applyOrder($criteria);

            $data = UserGuests::model()->with('GuestCategory','GuestMenuTypes')->findAll($criteria);

            $all_count = UserGuests::model()->count($criteria);
            $criteria->condition .= ' AND notified = 1';
            $notified_count = UserGuests::model()->count($criteria);
        }

        $this->render('index', array(
            'data' => $data,
            'sort' => $sort,

            'all_count' => $all_count,
            'notified_count' => $notified_count
        ));
    }

    public function actionAdd()
    {
        $this->layout = 'upbox';
        $this->upbox_title = 'Новый гость';

        $model = new UserGuests('add');

        $criteria = new CDbCriteria;
        $criteria->condition = 'fun_type = "multi" OR fun_type = "'.mApi::getFunType(true).'"';
        $guest_categories = GuestCategories::model()->findAll($criteria);

        $guest_menu_types = GuestMenuTypes::model()->findAll(array('order' => 'sort ASC'));

        if (isset($_POST['UserGuests']))
        {
            $model->attributes = $_POST['UserGuests'];
            $model->uid = User::getId();
            $model[mApi::getFunType(true).'_id'] = mApi::getFunId();

            if ($model->validate())
            {
                if (!empty($model->birthday))
                {
                    $date = explode('.', $model->birthday);
                    $data_time = @mktime(00, 00, 00, $date[1], $date[0], $date[2]);

                    $model->birthday = $data_time;
                }

                if ($model->save(false))
                    echo 'ok';
            }
            else
            {
                echo CHtml::errorSummary($model);
            }
        }
        else
        {
            $this->render('_form', array(
                'model' => $model,
                'guest_categories' => $guest_categories,
                'guest_menu_types' => $guest_menu_types
            ));
        }
    }

    public function actionEdit($id = 0)
    {
        $this->layout = 'upbox';
        $this->upbox_title = 'Редактирование гостя';

        $id = intval($id);
        if (empty($id))
            Yii::app()->end();

        $model = UserGuests::model()->findByAttributes(array('uid' => User::getId(), 'id' => $id));
        if (empty($model))
            Yii::app()->end();

        $criteria = new CDbCriteria;
        $criteria->condition = 'fun_type = "multi" OR fun_type = "'.mApi::getFunType(true).'"';
        $guest_categories = GuestCategories::model()->findAll($criteria);

        $guest_menu_types = GuestMenuTypes::model()->findAll(array('order' => 'sort ASC'));

        if (isset($_POST['UserGuests']))
        {
            $old_img_id = $model->img_id;
            $model->attributes = $_POST['UserGuests'];

            if ($model->validate())
            {
                if (!empty($model->birthday))
                {
                    $date = explode('.', $model->birthday);
                    $data_time = @mktime(00, 00, 00, $date[1], $date[0], $date[2]);

                    $model->birthday = $data_time;
                }

                // Если изменили картинку, то старой присвоим статус "удалено"
                if ($old_img_id != $model->img_id)
                    FileImages::model()->updateAll(array('is_deleted' => 1), 'id ='.$old_img_id);

                if ($model->save(false))
                    echo 'ok';
            }
            else
            {
                echo CHtml::errorSummary($model);
            }
        }
        else
        {
            $this->render('_form', array(
                'model' => $model,
                'guest_categories' => $guest_categories,
                'guest_menu_types' => $guest_menu_types
            ));
        }
    }

    public function actionDelete($id = 0)
    {
        $id = intval($id);
        if (empty($id))
            Yii::app()->end();

        $guest = UserGuests::model()->findByAttributes(array('uid' => User::getId(), 'id' => $id));
        if (empty($guest))
            return;

        if (!empty($guest->img_id))
            FileImages::model()->updateAll(array('is_deleted' => 1), 'id ='.$guest->img_id);

        $guest->delete();
    }

    public function actionNotified($id = 0)
    {
        $id = intval($id);
        if (empty($id))
            Yii::app()->end();

        $model = UserGuests::model()->findByAttributes(array('uid' => User::getId(), 'id' => $id));
        if (empty($model))
            Yii::app()->end();

        $model->notified = $model->notified == 1 ? 0 : 1;

        if ($model->update())
            echo $model->notified;
    }

    public function actionSeating()
    {
        Yii::app()->name = 'Рассадка гостей - '.Yii::app()->name;

        $this->render('seating');
    }

    /**
     * Проверка прав доступа к получению данных для печати.
     */
    public function checkAccess()
    {
        if (User::isGuest())
            mApi::sayError(1, 'Необходимо авторизироваться на сайте.');

        if (!mApi::getFunId())
            mApi::sayError(1, 'Не выбрана свадьба или торжество.');

        return true;
    }


    /**
     * Получение списка гостей (используется для flash-рассадки)
     * @param string $alt определяет тип вывода (XML или JSON)
     */
    public function actionGetAll($alt = '')
    {
        $alt = htmlspecialchars(strtolower($alt));

        if ($alt != 'xml' && $alt != 'json')
            mApi::sayError(1, 'Не получен параметр alt (возможные значения: xml, json)');

        $this->checkAccess();

        $guests_data = UserGuests::model()->with('GuestCategory', 'GuestMenuTypes')->findAllByAttributes(array('uid' => User::getId(), mApi::getFunType(true) . '_id' => mApi::getFunId()));

        if ($alt == 'xml')
        {
            header ("Content-type: text/xml");

            if (empty($guests_data))
                mApi::sayError(1, '<xml></xml>');

            echo '<?xml version="1.0" encoding="utf-8"?>';
            echo '<xml>';

            foreach ($guests_data as $guest)
                echo '<guest guest_id="' . $guest->id . '" table_id="' . $guest->table_id .
                    '" place="' . $guest->place . '" category="' . $guest['GuestCategory']['title'] .
                    '" menu="' . $guest['GuestMenuTypes']['title'] . '">' . $guest->fullname . '</guest>';

            echo '</xml>';
        }

        else if ($alt = 'json')
        {
            if (empty($guests_data))
                mApi::sayError(1, '{}');

            $data = '';
            foreach ($guests_data as $guest)
                $data .= '{"guest_id":' . $guest->id . ', "guest_name":"' . $guest->fullname .
                    '","table_id":' . $guest->table_id . ', "place":' . $guest->place .
                    ', "category":"' . $guest['GuestCategory']['title'] .
                    '" "menu":"' . $guest['GuestMenuTypes']['title'] . '"},';

            echo rtrim($data, ',');
        }
    }

    /**
     * Печать списка гостей, кто за каким столом и на каком месте
     */
    public function actionPrintSeating()
    {
        $this->checkAccess();
        $this->layout = 'print';

        $guests = UserGuests::model()->with('GuestCategory', 'Seating')->findAllByAttributes(
            array(
                'uid' => User::getId(),
                mApi::getFunType(true) . '_id' => mApi::getFunId()
            ), array(
                'order' => 'Seating.title ASC, t.place ASC, t.fullname ASC'
            ));

        if (empty($guests))
            mApi::sayError(1, 'Нет данных для печати.');

        $this->render('print_seating', array(
            'guests' => $guests
        ));
    }
}