<?php
class WeddingController extends AppController
{
    public $layout = 'planning';
    public $upbox_title = '';

    public function actionIndex()
    {
        if (User::isGuest())
            $this->redirect('/user/login/');

        if (mApi::getFunType() != 0)
            $this->redirect('/dashboard/');

        Yii::app()->name = 'Свадьбы - '.Yii::app()->name;

        $wedding = UserWeddings::model()->findAllByAttributes(array('uid' => User::getId()), array('order' => 'date ASC'));
        $this->render('index', array(
            'data' => $wedding
        ));
    }

    public function actionSelect()
    {
        if (User::isGuest())
            Yii::app()->end();

        $this->upbox_title = 'Выберите свадьбу';
        $this->layout = 'upbox';

        $data = UserWeddings::model()->findAllByAttributes(array('uid' => User::getId()), array('order' => 'date ASC'));
        $this->render('select', array('data' => $data));
    }

    public function actionAdd()
    {
        if (User::isGuest() || mApi::getFunType() != 0)
            Yii::app()->end();

        $this->upbox_title = 'Новая свадьба';
        $this->layout = 'upbox';
        $wedding = new UserWeddings('add');

        if (isset($_POST['UserWeddings']))
        {
            $wedding->attributes = $_POST['UserWeddings'];
            $wedding->uid = User::getId();

            if ($wedding->validate())
            {
                $date = explode('.', $wedding->date);
                $wedding->date = mktime(00, 00, 00, $date[1], $date[0], $date[2]);

                if ($wedding->date <= time())
                {
                    $wedding->addError('date', 'Дата свадьбы должна быть старше текущей');
                    echo CHtml::errorSummary($wedding);
                }

                if (!$wedding->hasErrors() && $wedding->save(false))
                {
                    // Создаем сайт свадьбы
                    $usite = new UserSite('add');
                    $usite['uid'] = User::getId();
                    $usite['title'] = $wedding->bride_name.' и '.$wedding->groom_name;
                    $usite['wedding_id'] = $wedding->id;
                    $usite['date'] = $wedding->date;
                    $usite->save();

                    // Заполнение категорий (из category_miniature)
                    $criteria = new CDbCriteria;
                    $criteria->condition = '(fun_type = "wedding" OR fun_type = "multi") AND show_in_vendors = 1';
                    $categories = CategoryMiniatures::model()->findAll($criteria);

                    if (!empty($categories))
                    {
                        foreach ($categories as $category)
                        {
                            if (empty($category['title']) || empty($category['name']) || empty($category['icon']))
                                continue;

                            $cat = new UserVendorCategories('add');
                            $cat['name'] = $category->name;
                            $cat['title'] = $category->title;
                            $cat['icon'] = $category->icon;
                            $cat['cat_link'] = $category->cat_link;
                            $cat['uid'] = User::getId();
                            $cat['wedding_id'] = $wedding->id;
                            $cat->save(false);
                        }
                    }

                    echo 'ok';
                }
            }
            else
            {
                echo CHtml::errorSummary($wedding);
            }
        }
        else
        {
            $this->render('_form', array(
                'wedding' => $wedding
            ));
        }
    }

    public function actionEdit($id = 0)
    {
        if (User::isGuest() || mApi::getFunType() != 0)
            Yii::app()->end();

        $this->upbox_title = 'Редактирование свадьбы';
        $this->layout = 'upbox';

        $id = intval($id);
        if (empty($id))
            Yii::app()->end();

        $wedding = UserWeddings::model()->findByAttributes(array('id' => $id, 'uid' => User::getId()));

        if (empty($wedding))
           Yii::app()->end();

        if (isset($_POST['UserWeddings']))
        {
            $check_willdate = true;
            $old_date = date('d.m.Y', $wedding->date);

            $wedding->scenario  = 'edit';
            $wedding->attributes = $_POST['UserWeddings'];

            if ($wedding->validate())
            {
                // Если дата не изменилась, то НЕ проверяем ее (дата должна быть > текущей)
                // потому что дата может быть == текущей (просто наступил этот день)
                if ($old_date == $wedding->date)
                    $check_willdate = false;

                $date = explode('.', $wedding->date);
                $date = mktime(00, 00, 00, $date[1], $date[0], $date[2]);

                if ($check_willdate && $date <= time())
                {
                    $wedding->addError('date', 'Дата свадьбы должна быть старше текущей');
                    echo CHtml::errorSummary($wedding);
                }

                $wedding->date = $date;

                if (!$wedding->hasErrors() && $wedding->save(false))
                    echo 'ok';

                // Обновим название сайта, в соответствии с именами жениха и невесты
                UserSite::model()->updateAll(array('title' => $wedding->bride_name.' и '.$wedding->groom_name, 'date' => $wedding->date), 'uid = '.User::getId().' AND wedding_id = '.$wedding->id);

                // Если редактировалось активная свадьба, то после сохранения обновим данные в сессии
                $active_wedding = mApi::getWedding();
                if (!empty($active_wedding) && $active_wedding->id == $wedding->id)
                {
                    Yii::app()->user->setState('wedding', $wedding);
                }
            }
            else
            {
                echo CHtml::errorSummary($wedding);
            }
        }
        else
        {
            $this->render('_form', array(
                'wedding' => $wedding
            ));
        }
    }
}