<?php
class MainController extends AppController
{
    public $defaultAction = 'index';
    public $pageTitle='Организация свадеб и проведение торжеств, подбор агентств по организации свадеб, банкетов и праздников на портале Svitor.Ru ';
    public $pageDescription='С порталом Svitor.Ru организация свадеб станет проще, здесь вы узнаете цены на свадебные товары и услуги, расчитаете бюджет и составите план торжества. Проведение торжеств начинается с планирования - с помощью нашего сервиса вы сможете подобрать загс, ресторан, ведущего, найти фотографа и оператора, приобрести необходимые мелочи для праздника!';
    public $pageKeywords='организация свадьбы, проведение свадьбы';

    public function actionIndex ()
    {
        if (!User::isGuest())
            $this->redirect(Yii::app()->createUrl('user/profile'));

    	$this->render('index');
    }

    public function actionError()
    {
        $this->pageTitle = 'Страница не найдена (404 Not Found)';
        if($error=Yii::app()->errorHandler->error)
            $this->render('error', $error);
    }
}