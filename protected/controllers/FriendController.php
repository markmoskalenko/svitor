<?php
class FriendController extends AppController
{
    public $defaultAction = 'index';

    public function actionIndex()
    {
        Yii::app()->name = 'Друзья - ' . Yii::app()->name;

        if (User::isGuest())
            $this->redirect('/user/login/');

        $user_info = array();
        $act = htmlspecialchars(trim(Yii::app()->request->getParam('act')));
        $uid = intval(Yii::app()->request->getParam('uid'));

        if (!empty($uid))
            $user_info = User::model()->findByPk($uid);

        // Если просматривается список друзей какого-то пользователя
        if (!empty($user_info) && $user_info->id != User::getId())
        {
            $dataProvider = new CActiveDataProvider('UserFriends', array(
                'pagination' => array(
                    'pageSize' => Yii::app()->params['items_per_page'],
                    'pageVar' => 'page'
                ),
                'criteria' => array(
                    'with' => 'User',
                    'condition' => 'uid = ' . $user_info->id . ' AND status = "friend"'
                )
            ));
        }
        // Заявки в друзья
        else if ($act == 'requests')
        {
            $dataProvider = new CActiveDataProvider('UserFriends', array(
                'pagination' => array(
                    'pageSize' => Yii::app()->params['items_per_page'],
                    'pageVar' => 'page'
                ),
                'criteria' => array(
                    'with' => 'UserRequests',
                    'condition' => 'fid = ' . User::getId() . ' AND status = "request"'
                )
            ));
        }
        // Все друзья
        else
        {
            $dataProvider = new CActiveDataProvider('UserFriends', array(
                'pagination' => array(
                    'pageSize' => Yii::app()->params['items_per_page'],
                    'pageVar' => 'page'
                 ),
                'criteria' => array(
                    'with' => 'User',
                    'condition' => 'uid = ' . User::getId() . ' AND status = "friend"'
                )
            ));
        }

        $this->render('index', array(
            'act' => $act,
            'user_info' => $user_info,
            'dataProvider' => $dataProvider,
            'friends_requests_count' => UserFriends::getCountRequests(),
        ));
    }

    public function actionAdd($uid = 0)
    {
        $id = intval($uid);

        if (User::isGuest())
            $this->redirect('/user/login/');

        // Нельзя добавить в друзья самого себя
        if ($id == User::getId())
            $this->redirect('/');

        // Друзей может быть не более 300
        if (UserFriends::model()->count('uid = ' . User::getId()) > 300)
            $this->redirect('/?limit_friends=300');

        if ($user_info = User::model()->findByPk($id))
        {
            // Если заявки еще нет, то создаем её
            $condition = 'status = "request" AND (uid = ' . User::getId() . ' AND fid = ' . $user_info->id . ') OR (uid = ' . $user_info->id . ' AND fid = ' . User::getId() . ')';
            if (!UserFriends::model()->find($condition))
            {
                $friend = new UserFriends;
                $friend['uid'] = User::getId();
                $friend['fid'] = $user_info->id;
                $friend['status'] = 'request';
                $friend->save();
            }
            // Если уже есть запрос на добавление, то просто подтверждаем заявку
            else if (UserFriends::model()->findByAttributes(array('fid' => User::getId(), 'uid' => $user_info->id, 'status' => 'request')))
            {
                // Подтверждаем заявку
                UserFriends::model()->updateAll(array('status' => 'friend'), 'uid = ' . $user_info->id . ' AND fid = ' . User::getId());

                // Добавляем в друзья
                $friend = new UserFriends;
                $friend['uid'] = User::getId();
                $friend['fid'] = $user_info->id;
                $friend['status'] = 'friend';
                $friend->save();
            }
            else
            {
                // заявка уже была отправлена или пользователи уже в друзьях друг у друга
            }
        }

        $this->redirect(Yii::app()->createUrl('user/profile', array('id' => $id)));
    }

    public function actionRemove($uid = 0)
    {
        $id = intval($uid);

        if (User::isGuest())
            $this->redirect('/user/login/');

        if ($user_info = User::model()->findByPk($id))
        {
            // Удаление происходит у обоих пользователей, безо всяких следов.
            $condition = '(uid = ' . User::getId() . ' AND fid = ' . $user_info->id . ' AND status = "friend") OR
                          (uid = ' . $user_info->id . ' AND fid = ' . User::getId() . ' AND status = "friend")';

            // Удаляем из друзей
            if (!UserFriends::model()->deleteAll($condition))
            {
                // Если не удалилось, значит пользователь отменяет заявку, а не удаляет из друзей.
                // Так поможем же ему в этом:
                UserFriends::model()->deleteAll('uid = ' . User::getId() . ' AND fid = ' . $user_info->id . ' AND status = "request"');
            }
        }

        $this->redirect(Yii::app()->createUrl('user/profile', array('id' => $user_info->id)));
    }
}