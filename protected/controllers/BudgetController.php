<?php
class BudgetController extends AppController
{
    public $defaultAction = 'index';
    public $layout = 'planning';
    public $upbox_title = '';

    public function filters()
    {
        return array('accessControl');
    }

    public function accessRules()
    {
        return array(
            array('deny',
                  'actions' => array('Index', 'Add', 'Edit', 'Delete'),
                  'users' => array('?')
            ),
            array('allow',
                  'actions' => array('Index', 'Add', 'Edit', 'Delete'),
                  'users' => array('@')
            )
        );
    }

    public function actionIndex($show_by = 'category')
    {
        Yii::app()->name = 'Бюджет - '.Yii::app()->name;

        if ($show_by != 'list' && $show_by != 'category')
            $show_by = 'category';

        $data = array();
        $total_info = array();
        $sort = array();

        if (mApi::getFunId())
        {
            $criteria = new CDbCriteria;
            $criteria->condition = 'uid = :uid AND '.mApi::getFunType(true).'_id = '.mApi::getFunId();
            $criteria->params = array('uid' => User::getId());

            if ($show_by == 'list')
            {
                $sort = new MSort;
                $sort->attributes = array(
                    'title',
                    'plan' => 'amount_plan',
                    'fact' => 'amount_fact',
                    'paid' => 'paid_amount',
                    'left' => 'amount_fact - paid_amount',
                    'date' => array(
                        'asc' => 'date IS NULL, date ASC'
                    )
                );
                $sort->defaultOrder = 'date IS NULL, date ASC';
                $sort->defaultLabel = array('date', 'asc');
                $sort->applyOrder($criteria);

                $data = UserBudget::model()->findAll($criteria);
            }
            else if ($show_by == 'category')
            {
                $data = CategoryMiniatures::model()->with('UserBudget')->findAll($criteria);
            }

            $sql = 'SELECT SUM(amount_plan) AS total_plan, SUM(amount_fact) AS total_fact, SUM(paid_amount) AS total_paid FROM user_budget WHERE uid = :uid AND '.mApi::getFunType(true).'_id = '.mApi::getFunId();
            $userId = User::getId();
            $total_info = Yii::app()->db->createCommand($sql)
                ->bindParam(':uid', $userId, PDO::PARAM_INT)
                ->query()->read();
        }

        $this->render('index', array(
            'data' => $data,
            'sort' => $sort,
            'total_info' => $total_info,

            'show_by' => $show_by
        ));
    }

    public function actionAdd($task_id = 0)
    {
        $this->upbox_title = 'Новая запись';
        $this->layout = 'upbox';

        $model = new UserBudget('add');
        $cat_mins = CategoryMiniatures::model()->findAll('show_in_budget = 1');

        $task_id = intval($task_id);
        if (!empty($task_id)) {
            $task = UserChecklist::model()->findByAttributes(array('uid' => User::getId(), 'id' => $task_id));
            if (!empty($task))
            {
                $model['task_id'] = $task->id;
                $model['cat_min_id'] = $task->cat_min_id;
            }
        }

        if (isset($_POST['UserBudget']))
        {
            $model->uid = User::getId();
            $model[mApi::getFunType(true).'_id'] = mApi::getFunId();

            $model->attributes = $_POST['UserBudget'];

            if ($model->validate())
            {
                if (!empty($model->date))
                {
                    $date = explode('.', $model->date);
                    $model->date = mktime(00, 00, 00, $date[1], $date[0], $date[2]);

                    if ($model->date < time() - 24*60*60)
                    {
                        $model->addError('date', 'Дата "Оплатить до" должна быть старше или равна текущей');
                        echo CHtml::errorSummary($model);
                    }
                }

                if (!$model->hasErrors() && $model->save(false))
                {
                    // Установим связь с задачей, если это необходимо.
                    if (!empty($task))
                    {
                        $task['budget_id'] = $model->id;
                        $task->update();
                    }

                    echo 'ok';
                }

            }
            else
            {
                echo CHtml::errorSummary($model);
            }
        }
        else
        {
            $this->render('_form', array(
                'model' => $model,
                'cat_mins' => $cat_mins
            ));
        }
    }

    public function actionEdit($id = 0, $task = 0)
    {
        $this->upbox_title = 'Редактирование записи';
        $this->layout = 'upbox';

        $id = intval($id);
        if (empty($id))
            Yii::app()->end();

        $model = UserBudget::model()->findByAttributes(array('uid' => User::getId(), 'id' => $id));
        if (empty($model))
            Yii::app()->end();

        $cat_mins = CategoryMiniatures::model()->findAll('show_in_budget = 1');

        if (isset($_POST['UserBudget']))
        {
            $check_willdate = true;
            $old_date = date('d.m.Y', $model->date);

            $model->scenario = 'edit';
            $model->attributes = $_POST['UserBudget'];

            if ($model->validate())
            {
                // Если дата не изменилась, то НЕ проверяем ее (дата должна быть >= текущей)
                // потому что дата может быть < текущей (просто прошел уже этот день)
                if ($old_date == $model->date)
                    $check_willdate = false;

                $date = NULL;

                if (!empty($model->date))
                {
                    $date = explode('.', $model->date);
                    $date = mktime(00, 00, 00, $date[1], $date[0], $date[2]);

                    if ($check_willdate && $date < time() - 24*60*60)
                    {
                        $model->addError('date', 'Дата "Оплатить до" должна быть старше или равна текущей');
                        echo CHtml::errorSummary($model);
                    }
                }

                $model->date = $date;

                if (!$model->hasErrors() && $model->save(false))
                    echo 'ok';
            }
            else
            {
                echo CHtml::errorSummary($model);
            }
        }
        else
        {
            $this->render('_form', array(
                'model' => $model,
                'cat_mins' => $cat_mins
            ));
        }
    }

    public function actionSave_value()
    {
        if (User::isGuest())
            echo 'Изменения не были сохранены т.к. ваша сессия истекла и вам необходимо заново авторизироваться на сайте.';

        if (empty($_POST['id']) || empty($_POST['name']) || !isset($_POST['value']))
            Yii::app()->end();

        $id = intval($_POST['id']);
        if (empty($id))
            Yii::app()->end();

        $name = htmlspecialchars($_POST['name']);
        if ($name != 'title' && $name != 'amount_fact' && $name != 'amount_plan' && $name != 'paid_amount')
            Yii::app()->end();

        $model = UserBudget::model()->findByAttributes(array('uid' => User::getId(), 'id' => $id));
        if (empty($model))
            Yii::app()->end();

        $value = $_POST['value'];

        // Декодируем наименование, иначе символы будут изменены на их коды дважды.
        // Это необходимо, т.к. это значение выбирается из базы и проходит валидацию (+ фильтр htmlspecialchars)
        // и затем записывается обратно в базу
        $model->title = CHtml::decode($model->title);

        $model->$name = $value;

        if ($model->save()) {
            echo 'ok';
        } else {
            if ($name == 'amount_fact') $name = 'paid_amount';
            echo $model->getError($name);
        }
    }

    public function actionDelete($id = 0)
    {
        $id = intval($id);
        if (empty($id))
            Yii::app()->end();

        // удаление возможной связи с задачей
        UserChecklist::model()->updateAll(array('budget_id' => 0), 'uid = :uid AND budget_id = :id', array(':uid' => User::getId(), ':id' => $id));

        // удаление строки
        UserBudget::model()->deleteAllByAttributes(array('uid' => User::getId(), 'id' => $id));
    }
}
?>