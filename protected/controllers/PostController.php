<?php
class PostController extends AppController
{

    public $pageTitle='';//Все для свадьбы и свадебных церемоний на портале svitor.ru
    public $pageDescription='На свадебном портале Вы найдете все для свадьбы. Мы поможем Вам ';
    public $pageKeywords='свадьба, торжество, мероприятие, портал, ';

    public function filters()
    {
        return array('accessControl');
    }

    public function accessRules()
    {
        return array(
            array('deny',
                'actions' => array('Add', 'Edit', 'Delete'),
                'users' => array('?')
            ),
            array('allow',
                'actions' => array('Add', 'Edit', 'Delete'),
                'users' => array('@')
            )
        );
    }

    public function actionIndex($cat = null)
    {
        $cat = strtolower(htmlspecialchars(trim($cat)));
        $active_category = array();
        $view_post = array();
        $recommendations = array();

        Yii::app()->name = 'Обмен опытом';

        if (!empty($cat))
            $active_category = PostCategory::model()->findByAttributes(array('name' => $cat));

        $criteria = new CDbCriteria;
        $criteria->condition = '(t.status = "published" OR t.status = "published_verification")';

        // Если есть активная категория
        if (!empty($active_category))
        {
            Yii::app()->name = $active_category->title;
            $criteria->condition .= ' AND t.post_cat_id = ' . $active_category->id;
        }

        $criteria->order = 'published_date DESC';
        $criteria->with = array('PostCategory');

        // Если есть что-то в поиске
        if (!empty($_GET['q']))
        {
            $query = htmlspecialchars(trim($_GET['q']));
            $query = str_replace('%', '\%', $query);
            $query = str_replace('_', '\_', $query);
            $criteria->condition .= ' AND (t.title LIKE "%'.$query.'%" OR t.content_1 LIKE "%'.$query.'%" OR t.content_2 LIKE "%'.$query.'%")';

            // Пишем в лог только если запрос был от гостя или обычного пользователя
            // запросы администраторов и модераторов не учитываются
            if (User::isGuest() || (!User::isGuest() && User::getSecurityLevel() == 0))
            {
                $log = new LogInfoSearch();
                $log['ip_address'] = ip2long(CHttpRequest::getUserHostAddress());
                $log['date'] = time();
                $log['query_text'] = $query;

                if (!User::isGuest())
                    $log['uid'] = User::getId();

                $log->save();
            }
        }

        $dataProvider = new CActiveDataProvider('Post', array(
            'pagination' => array(
                'pageSize' => Yii::app()->params['items_per_page'],
                'pageVar' => 'page'
            ),
            'criteria' => $criteria
        ));

        // Выбираем категории в которых есть хотябы один опубликованный пост
        $categories = PostCategory::model()->with(array(
            'Post' => array(
                'select' => false
            )))->findAll();

        $this->render('index', array(
            'active_category' => $active_category,
            'categories' => $categories,
            'posts' => $dataProvider->getData(),
            'posts_pagination' => $dataProvider->pagination,

            'view_post' => $view_post,
            'recommendations' => $recommendations
        ));
    }

    public function actionView($id = 0)
    {
        $id = intval($id);
        if (empty($id))
            $this->redirect(Yii::app()->createUrl('post'));

        $criteria = new CDbCriteria;
        $criteria->condition = '(t.status = "published" OR t.status = "published_verification") AND t.id = :post_id';
        $criteria->params = array(':post_id' => $id);
        $criteria->with = array('Vendor', 'User', 'PostCategory');

        $post = Post::model()->find($criteria);
        if (empty($post))
            $this->redirect(Yii::app()->createUrl('post'));

        Yii::app()->name = $post->title;

        // Фото
        $criteria = new CDbCriteria;
        $criteria->condition = 'post_id = :post_id';
        $criteria->params = array(':post_id' => $id);

        $postImages = PostImages::model()->findAll($criteria);

        // Читайте также
        $criteria = new CDbCriteria;
        $criteria->condition = '(t.status = "published" OR t.status = "published_verification") AND t.post_cat_id = :post_cat_id AND t.id != :post_id';
        $criteria->params = array(
            ':post_cat_id' => $post->post_cat_id,
            ':post_id' => $post->id
        );
        $criteria->order = 'published_date DESC';
        $criteria->limit = 5;

        $recommendations = Post::model()->findAll($criteria);

        // Комментарии
        $criteria = new CDbCriteria;
        $criteria->condition = '(t.status = "published") AND post_id = :post_id';
        $criteria->params = array(':post_id' => $post->id);
        $criteria->order = 'date ASC';
        $criteria->with = array('User');

        $dataProvider = new CActiveDataProvider('PostComment', array(
            'pagination' => array(
                'pageSize' => Yii::app()->params['items_per_page'],
                'pageVar' => 'page',
                'params' => array_merge($_GET, array('#' => 'comments'))
            ),
            'criteria' => $criteria
        ));

        $this->render('view', array(
            'postImages' => $postImages,
            'post' => $post,
            'recommendations' => $recommendations,
            'comments_count' => $dataProvider->getTotalItemCount(),
            'comments' => $dataProvider->getData(),
            'comments_pagination' => $dataProvider->pagination
        ));
    }

    public function actionAdd()
    {
        $model = new Post('add');

        if (isset($_POST['Post']))
        {
            $model->attributes = $_POST['Post'];
            $model['created_date'] = time();
            $model['status'] = 'verification';
            $model['author_uid'] = User::getId();

            if ($model->save(false))
                Yii::app()->user->setFlash('add_post_success', 'Спасибо! Ваш пост будет опубликован сразу после проверки - это займет некоторое время ;)');

            // Читайте также
            $criteria = new CDbCriteria;
            $criteria->order = 'id DESC';
            $criteria->limit = 1;

            $lastPost = Post::model()->find($criteria);
            $id = $lastPost->id;

            foreach(json_decode($_POST['images']) as $item){

                $post=new PostImages();
                $post->post_id=(int)$id;
                $post->image_big = $item->image_big;
                $post->image_medium = $item->image_medium;
                $post->save();
            }
        }

        $this->render('_form', array(
            'max_images' => 50,
            'model' => $model,
            'categories' => CHtml::listData(PostCategory::model()->findAll(), 'id', 'title')
        ));
    }

    public function actionAddComment()
    {
        if (User::isGuest())
            Yii::app()->end();

        $this->layout = 'ajax';

        $model = new PostComment('add');
        if (isset($_POST['Comment']))
        {
            // Проверим существует ли статья
            $post_id = intval($_POST['Comment']['post_id']);
            if (!$post = Post::model()->findByPk($post_id))
                Yii::app()->end();

            $model->attributes = $_POST['Comment'];
            $model['post_id'] = $post->id;
            $model['uid'] = User::getId();
            $model['date'] = time();

            if ($model->save()) {
                echo '{"status":"ok"}';
            } else {
                echo '{"status":"error", "msg":"'.str_replace('"', '\"', CHtml::error($model, 'comment')).'"}';
            }
        }
    }

    public function actionDeletephoto()
    {

        if (isset($_POST['path']) && isset($_POST['image']))
        {
            if(file_exists(DOCUMENT_ROOT."/".$_POST['path']."/".$_POST['image'])){
                unlink(DOCUMENT_ROOT."/".$_POST['path']."/".$_POST['image']);
            }
            if(file_exists(DOCUMENT_ROOT."/".$_POST['path']."/medium/".$_POST['image'])){
                unlink(DOCUMENT_ROOT."/".$_POST['path']."/medium/".$_POST['image']);
            }
        }
    }

    public function actionDeleteonephoto()
    {

        $criteria = new CDbCriteria;
        $criteria->condition = 'image_big = :image_big';
        $criteria->params = array(':image_big' => "/uploads_img/16796/".$_POST['image']);

        PostImages::model()->deleteAll($criteria);

        if (isset($_POST['path']) && isset($_POST['image']))
        {
            if(file_exists(DOCUMENT_ROOT."/".$_POST['path']."/".$_POST['image'])){
                unlink(DOCUMENT_ROOT."/".$_POST['path']."/".$_POST['image']);
            }
            if(file_exists(DOCUMENT_ROOT."/".$_POST['path']."/medium/".$_POST['image'])){
                unlink(DOCUMENT_ROOT."/".$_POST['path']."/medium/".$_POST['image']);
            }
        }
    }

    public function actionRemoveComment($id = 0)
    {
        $id = intval($id);
        if (empty($id))
            Yii::app()->end();

        if (User::isGuest())
            Yii::app()->end();

        $criteria = new CDbCriteria;
        $criteria->condition = 'uid = :uid AND id = :id';
        $criteria->params = array(':uid' => User::getId(), ':id' => $id);

        if (PostComment::model()->updateAll(array('deleted' => 1), $criteria))
            echo 'ok';
    }
}