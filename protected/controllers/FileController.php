<?php
class FileController extends AppController
{
    public function actionUploadImg()
    {
        if (empty($_FILES['image']))
            mApi::sayError(1, 'Не удалось загрузить картинку...');

        $imageMgr = new ImageManager($_FILES['image']);

        if ($imageMgr->upload()) {
            echo $imageMgr->getSuccess(); // JSON response with img_id, file_name and path
        } else {
            echo $imageMgr->getError();   // JSON error msg
        }
    }

    public function actionUploadImgUrl()
    {
        if (!Yii::app()->request->getPost('url'))
            mApi::sayError(1, 'Не удалось загрузить картинку...');

        $imageMgr = new ImageManager(Yii::app()->request->getPost('url'));

        if ($imageMgr->uploadImageUrl()) {
            echo $imageMgr->getSuccess(); // JSON response with img_id, file_name and path
        } else {
            echo $imageMgr->getError();   // JSON error msg
        }
    }
}