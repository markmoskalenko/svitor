<?php
class MailerController extends AppController
{
    public $defaulAction = '';

    /**
     * Добавление писем для рассылки всем компаниям
     *
     * @param string $template_name - имя шаблона письма из папки mailer
     */
    public function actionAddMails($template_name = '')
    {
        if (empty($template_name))
            Yii::app()->end();


        switch ($template_name)
        {
            case 'invite_registered_vendors':

                $vendors = Vendor::model()->findAll();
                $content = file_get_contents('/var/www/svitor.ru/protected/views/mailer/invite_registered_vendors.php');

                foreach ($vendors as $vendor)
                {
                    if (empty($vendor->str_password))
                        continue;

                    $title = 'Вы зарегистрированы в каталоге Свадьба и Торжество!';
                    $body = $content;
                    $body = str_replace('{email}', $vendor->email, $body);
                    $body = str_replace('{vendor_id}', $vendor->id, $body);
                    $body = str_replace('{password}', $vendor->str_password, $body);

                    Yii::app()->PearMail->queueMail('info@svitor.ru', $vendor->email, $title, '', $body);
                }

                echo 'Письма добавлены в очередь.';

                break;

            default:
                Yii::app()->end();
                break;
        }
    }

    /**
     * Отправка партии писем из очереди
     */
    public function actionSendMails()
    {
        Yii::app()->PearMail->sendMails();
    }
}