<?php
class CatalogController extends AppController
{
    public $pageTitle=''; //Все для свадьбы и свадебных церемоний на портале svitor.ru
    public $pageDescription='На свадебном портале Вы найдете все для свадьбы. Мы поможем Вам ';
    public $pageKeywords='свадьба, торжество, мероприятие, портал, ';
    public $defaultAction = 'vendors';

    public function actionCc(){
        $criteria = new CDbCriteria();
        $criteria->condition = "cat_id = :cat_id";
        $criteria->params = array(':cat_id'=>184);
        $criteria->addSearchCondition('title', 'бокалы');
        $model = Product::model()->findAll($criteria);
        exit;

    }

    public function actionProducts( $getC = true )
    {
        $cat = strtolower(htmlspecialchars(trim(Yii::app()->request->getParam('cat', null))));
        $view = intval(Yii::app()->request->getParam('view', 0));
        $roots = array();
        $descendants = array();
        $products = array();
        $pages = array();
        $product_view = array();
        $showAttributes = false;
        $category = array();
        $type = 'products';

        Yii::app()->name = 'Товары - '.Yii::app()->name;

        // Выборка категории
        if (!empty($cat))
            $category = Category::model()->findByAttributes(array('visible_prod' => 1, 'name' => $cat));

        // Если категория есть, то ищем в ней товары
        if (!empty($category))
        {
            Yii::app()->name = $category->title.' - '.Yii::app()->name;

            // Root категрия, показываем только ее потомков
            if ($category->isRoot() && !$category->isLeaf() && empty($view))
            {
                $criteria = new CDbCriteria;
                $criteria->order = 'root';
                $criteria->condition = 'visible_prod = 1';

                $descendants = $category->children()->findAll($criteria);
            }
            // Категория содержит товары и не имеет потомков
            else if (empty($view))
            {
                $filter = $_GET; // нет проверки на пустоту, потому что до этого момента не дойдет, если GET будет пуст
                $filter_condition = '';
                if (is_array($filter))
                {
                    $attrInCount = array();
                    foreach ($filter as $attr_id => $attr_values)
                    {
                        $attr_id = intval($attr_id);
                        if (empty($attr_id)) // используем только целые числа
                            continue;

                        if (is_array($attr_values))
                        {
                            $filter_values = '';
                            foreach ($attr_values as $value)
                            {
                                $value = intval($value);
                                if (empty($value)) // используем только целые числа
                                    continue;

                                $attrInCount[] = $value;
                                $filter_values .= $value.', ';
                            }
                            $filter_values = rtrim($filter_values, ', ');
                        }

                        // Если есть значения, то работаем
                        if (!empty($filter_values))
                        {
                            if (empty($filter_condition)) {
                                $filter_condition = '(attr_id = '.$attr_id.' AND attr_value_id IN ('.$filter_values.'))';
                            } else {
                                $filter_condition .= ' OR (attr_id = '.$attr_id.' AND attr_value_id IN ('.$filter_values.'))';
                            }
                        }
                    }
                }

                // Если есть условия для фильтра то...
                if (!empty($filter_condition))
                {
                    // ...выполняем поиск товаров, у которых есть выбранные атрибуты, при условии что $attrInCount != 0
                    if (count($attrInCount) > 0)
                    {
                        $attr_criteria = new CDbCriteria;
                        $attr_criteria->condition = $filter_condition;
                        $attr_criteria->group = 't.product_id';
                        $attr_criteria->having = 'COUNT(*) = :count';
                        $attr_criteria->params = array(
                            ':count' => count($attrInCount) // count должен быть равен == количеству выбранных значений
                        );

                        $hasVendors = ProductAttrValues::model()->findAll($attr_criteria);

                        // Если найдены совпадения с атрибутами, ищем товары
                        if (!empty($hasVendors))
                        {
                            // Подготовим данные для addInCondition (в формате array(0 => product_id1, 1 => product_id2))
                            $product_InCondition = array();
                            foreach ($hasVendors as $product)
                                $product_InCondition[] = $product->product_id;

                            // Ищем товары, отобранные по атрибутам
                            $criteria = new CDbCriteria;
                            $criteria->condition = 't.status = 1 AND t.visible = 1 AND t.cat_id = :cat_id';
                            $criteria->params = array(':cat_id' => $category->id);
                            $criteria->addInCondition('t.id', $product_InCondition);
                            $criteria->order = 'Vendor.rating DESC';

                            $count = Product::model()->published()->count($criteria);

                            $pages = new CPagination($count);
                            $pages->pageSize = Yii::app()->params['items_per_page'];
                            $pages->applyLimit($criteria);

                            $products = Product::model()->published()->with('Vendor', 'FirstImage')->findAll($criteria);
                        }
                        // Нет совпадений, покажем что ничего не найдено
                        else
                        {
                            $products = array();
                        }
                    }
                }
                // Иначе делаем обычный вывод товаров
                else
                {
                    $criteria = new CDbCriteria;
                    $criteria->condition = 't.status = 1 AND t.visible = 1 AND t.cat_id = :cat_id';
                    $criteria->params = array(':cat_id' => $category->id);
                    $criteria->order = 'Vendor.rating DESC';

                    $count = Product::model()->published()->count($criteria);

                    $pages = new CPagination($count);
                    $pages->pageSize = Yii::app()->params['items_per_page'];
                    $pages->applyLimit($criteria);

                    $products = Product::model()->published()->with('Vendor', 'FirstImage')->findAll($criteria);
                }

            }
            // Просматривается конкретный товар
            else if (!empty($view))
            {
                $product_id = intval($view);
                if (empty($product_id))
                    $this->redirect('/catalog/products/cat/'.$category->name);

                $product_view = Product::model()->with('AttrValues', 'Images')->findByAttributes(array('status' => 1, 'visible' => 1, 'cat_id' => $category->id, 'id' => $product_id));

            }
        }
        // Если не выбрана категория, то показываем все root категории
        else
        {
            $criteria = new CDbCriteria;
            $criteria->order = 'root';
            $criteria->condition = 'visible_prod = 1';
            $roots = Category::model()->roots()->cache(500)->findAll($criteria);
        }

        if($getC){
            $this->render('products', array(
                'roots' => $roots,
                'category' => $category,

                'descendants' => $descendants,
                'products' => $products,
                'pages' => $pages,
                'product_view' => $product_view,
                'showAttributes' => $showAttributes,

                'type' => $type
            ));
        }else return $products;
    }

    public function actionVendors()
    {
        $cat = strtolower(htmlspecialchars(trim(Yii::app()->request->getParam('cat', null))));
        $roots = array();
        $descendants = array();
        $vendors = array();
        $pages = array();
        $showAttributes = false;
        $user_vendor_favorite = array();
        $user_vendor_team = array();
        $category = array();
        $type = 'vendors';

        $area_condition = '';
        $country_id = isset($_GET['country_id']) ? intval($_GET['country_id']) : User::getCountryId();
        $region_id = isset($_GET['region_id']) ? intval($_GET['region_id']) : User::getRegionId();
        $city_id = isset($_GET['city_id']) ? intval($_GET['city_id']) : 0;
        $metro_id = isset($_GET['metro_id']) ? intval($_GET['metro_id']) : 0;

        Yii::app()->name = 'Компании - '.Yii::app()->name;

        // Выборка категории
        if (!empty($cat))
            $category = Category::model()->cache(500)->findByAttributes(array('visible_vndr' => 1, 'name' => $cat));

        // Региональные параметры
        if (!empty($country_id))
            $area_condition = 'country_id = '.$country_id;

        if (!empty($country_id) && !empty($region_id))
            $area_condition .= ' AND region_id = '.$region_id;

        if (!empty($country_id) && !empty($region_id) && !empty($city_id))
            $area_condition .= ' AND city_id = '.$city_id;

        if (!empty($country_id) && !empty($region_id) && !empty($city_id) && !empty($metro_id))
            $area_condition .= ' AND metro_id = '.$metro_id;

        // Если в адресе указана только страна. Например если компания работает по всей России
        if (!empty($country_id))
            $area_condition .= ' OR (country_id = '.$country_id.' AND region_id = 0 AND city_id = 0 AND metro_id = 0)';

        // Если категория есть, то ищем в ней компании
        if (!empty($category))
        {
            Yii::app()->name = $category->title;

            // Root категрия, показываем только ее потомков
            if ($category->isRoot() && !$category->isLeaf())
            {
                $criteria = new CDbCriteria;
                $criteria->order = 'root';
                $criteria->addCondition('visible_vndr = 1');

                $descendants = $category->children()->findAll($criteria);

                // Если есть что-то в поиске - поищем компании в подразделах
                if (!empty($_GET['q']))
                {
                    if (!empty($descendants))
                    {
                        $cat_ids = array();
                        foreach ($descendants as $cat)
                            $cat_ids[] = $cat->id;
                    }

                    $criteria = new CDbCriteria;
                    $criteria->order = 'rating DESC';

                    // Будем искать только в подразделах
                    $criteria->join = 'INNER JOIN vendor_category vc ON t.id = vc.vendor_id';
                    $criteria->addInCondition('vc.cat_id', $cat_ids, 'AND');

                    $query = trim($_GET['q']);
                    $query = str_replace('%', '\%', $query);
                    $query = str_replace('_', '\_', $query);
                    $criteria->condition .= ' AND (t.title LIKE "%'.$query.'%" OR t.description LIKE "%'.$query.'%")';

                    $count = Vendor::model()->cache(500)->count($criteria);

                    $pages = new CPagination($count);
                    $pages->pageSize = Yii::app()->params['items_per_page'];
                    $pages->applyLimit($criteria);

                    $vendors = Vendor::model()->with('ReviewsCount')->cache(500)->findAll($criteria);
                }
            }

            // Категория содержит компании и не имеет потомков
            else if (!$category->isRoot() || $category->isLeaf())
            {
                $criteria = new CDbCriteria;
                $criteria->condition = $area_condition;
                $addresses = VendorAddresses::model()->findAll($criteria);

                // Подготовим данные для addInCondition (в формате array(0 => vendor_id1, 1 => vendor_id2))
                $area_InCondition = array();
                foreach ($addresses as $address)
                    $area_InCondition[] = $address->vendor_id;

                $filter = $_GET;
                $filter_condition = '';
                if (is_array($filter))
                {
                    $attrInCount = array();
                    foreach ($filter as $attr_id => $attr_values)
                    {
                        $attr_id = intval($attr_id);
                        if (empty($attr_id)) // используем только целые числа
                            continue;

                        if (is_array($attr_values))
                        {
                            $filter_values = '';
                            foreach ($attr_values as $value)
                            {
                                $value = intval($value);
                                if (empty($value)) // используем только целые числа
                                    continue;

                                $attrInCount[] = $value;
                                $filter_values .= $value.', ';
                            }
                            $filter_values = rtrim($filter_values, ', ');
                        }

                        // Если есть значения, то работаем
                        if (!empty($filter_values))
                        {
                            if (empty($filter_condition)) {
                                $filter_condition = '(cat_id = :cat_id AND attr_id = '.$attr_id.' AND attr_value_id IN ('.$filter_values.'))';
                            } else {
                                $filter_condition .= ' OR (cat_id = :cat_id AND attr_id = '.$attr_id.' AND attr_value_id IN ('.$filter_values.'))';
                            }
                        }
                    }
                }

                // Если есть условия для фильтра то...
                if (!empty($filter_condition))
                {
                    // ...выполняем поиск компаний, у которых есть выбранные атрибуты, при условии что $attrInCount != 0
                    if (count($attrInCount) > 0)
                    {
                        $attr_criteria = new CDbCriteria;
                        $attr_criteria->condition = $filter_condition;
                        $attr_criteria->group = 't.vendor_id';
                        $attr_criteria->having = 'COUNT(*) = :count';
                        $attr_criteria->params = array(
                            ':cat_id' => $category->id,
                            ':count' => count($attrInCount) // count должен быть равен == количеству выбранных значений
                        );

                        $hasVendors = VendorAttrValues::model()->findAll($attr_criteria);

                        // Если найдены совпадения с атрибутами, ищем компании
                        if (!empty($hasVendors))
                        {
                            // Подготовим данные для addInCondition (в формате array(0 => vendor_id1, 1 => vendor_id2))
                            $vendor_InCondition = array();
                            foreach ($hasVendors as $vendor)
                                $vendor_InCondition[] = $vendor->vendor_id;

                            // Ищем компании, отобранные по атрибутам
                            $criteria = new CDbCriteria;
                            $criteria->join = 'INNER JOIN vendor_category vc ON t.id = vc.vendor_id';
                            $criteria->condition = 'vc.cat_id = :cat_id AND t.is_banned = 0';
                            $criteria->params = array(':cat_id' => $category->id);
                            $criteria->addInCondition('t.id', $vendor_InCondition);
                            $criteria->order = 'rating DESC';

                            // Если есть что-то в поиске
                            if (!empty($_GET['q']))
                            {
                                $query = trim($_GET['q']);
                                $query = str_replace('%', '\%', $query);
                                $query = str_replace('_', '\_', $query);
                                $criteria->condition .= ' AND (t.title LIKE "%'.$query.'%" OR t.description LIKE "%'.$query.'%")';
                            }

                            // Учитываем фильтр по региону
                            if (!empty($area_condition))
                                $criteria->addInCondition('t.id', $area_InCondition);

                            $count = Vendor::model()->count($criteria);

                            $pages = new CPagination($count);
                            $pages->pageSize = Yii::app()->params['items_per_page'];
                            $pages->applyLimit($criteria);

                            $vendors = Vendor::model()->with('ReviewsCount')->findAll($criteria);
                        }
                        // Нет совпадений, покажем что ничего не найдено
                        else
                        {
                            $vendors = array();
                        }
                    }
                }
                // Иначе выводим простой список компаний (с возможным отбором по региону)
                else
                {
                    $criteria = new CDbCriteria;
                    $criteria->join = 'INNER JOIN vendor_category vc ON t.id = vc.vendor_id';
                    $criteria->condition = 'vc.cat_id = :cat_id AND t.is_banned = 0';
                    $criteria->params = array(':cat_id' => $category->id);
                    $criteria->order = 'rating DESC';

                    // Если есть что-то в поиске
                    if (!empty($_GET['q']))
                    {
                        $query = trim($_GET['q']);
                        $query = str_replace('%', '\%', $query);
                        $query = str_replace('_', '\_', $query);
                        $criteria->condition .= ' AND (t.title LIKE "%'.$query.'%" OR t.description LIKE "%'.$query.'%")';
                    }

                    // Учитываем фильтр по региону
                    if (!empty($area_condition))
                        $criteria->addInCondition('t.id', $area_InCondition);

                    $count = Vendor::model()->cache(500)->count($criteria);

                    $pages = new CPagination($count);
                    $pages->pageSize = Yii::app()->params['items_per_page'];
                    $pages->applyLimit($criteria);

                    $vendors = Vendor::model()->with('ReviewsCount')->cache(500)->findAll($criteria);
                }


                if (!empty($vendors))
                {
                    // Получаем список сохраненных компаний у пользователя (для корректного отображения кнопки "Сохранить")
                    $user_vendor_favorite = UserVendorFavorites::model()->findAllByAttributes(array('uid' => User::getId()));

                    if (!empty($user_vendor_favorite))
                        $user_vendor_favorite = CHtml::listData($user_vendor_favorite, 'vendor_id', 'id');

                    // Если активно какое-то торжество
                    if (mApi::getFunId())
                    {
                        // Получаем список компаний в команде у пользователя, для активного торжества или свадьбы (для отображения "В команде")
                        $user_vendor_team = UserVendorTeam::model()->findAllByAttributes(array(mApi::getFunType(true).'_id' => mApi::getFunId(), 'uid' => User::getId()));

                        if (!empty($user_vendor_team))
                            $user_vendor_team = CHtml::listData($user_vendor_team, 'vendor_id', 'id');
                    }
                }
            }
        }
        // Если не выбрана категория, то показываем все root категории
        else
        {
            // Если есть что-то в поиске
            if (!empty($_GET['q']))
            {
                $criteria = new CDbCriteria;
                $criteria->condition = 'is_banned = 0';
                $criteria->order = 'rating DESC';

                $query = trim($_GET['q']);
                $query = str_replace('%', '\%', $query);
                $query = str_replace('_', '\_', $query);
                $criteria->condition .= ' AND (t.title LIKE "%'.$query.'%" OR t.description LIKE "%'.$query.'%")';

                $count = Vendor::model()->count($criteria);

                $pages = new CPagination($count);
                $pages->pageSize = Yii::app()->params['items_per_page'];
                $pages->applyLimit($criteria);

                $vendors = Vendor::model()->with('ReviewsCount')->findAll($criteria);
            }
            else
            {
                $criteria = new CDbCriteria;
                $criteria->order = 'root';
                $criteria->condition = 'visible_vndr = 1';
                $roots = Category::model()->roots()->findAll($criteria);
            }
        }

        $this->render('vendors', array(
            'roots' => $roots,
            'category' => $category,
            'country_id' => $country_id,
            'region_id' => $region_id,
            'descendants' => $descendants,
            'vendors' => $vendors,
            'pages' => $pages,
            'showAttributes' => $showAttributes,

            'user_vendor_favorite' => $user_vendor_favorite,
            'user_vendor_team' => $user_vendor_team,
            'type' => $type
        ));
    }


    /*
     *  Для вывода фильтра и списка атрибутов
     *  Сам фильтр тут работает визуально, разбирает запрос $_GET['f'], для отображения фильтра.
     *  Также тут происходит вывод списка всех атрибутов для выбранной категории
     *
     *  @object: $category - AR данные выбранной категории
     *  @string: $type - тип каталога, может быть products или vendors
     *
     */
    public function actionFilters($category, $type)
    {
        if (empty($category) || empty($type) || ($type != 'products' && $type != 'vendors'))
            Yii::app()->end();

        $area_data  = array();
        $country_id = intval(Yii::app()->request->getParam('country_id'));
        $region_id  = intval(Yii::app()->request->getParam('region_id'));
        $city_id    = intval(Yii::app()->request->getParam('city_id'));

        if ($type == 'products') {
            $attributes = $category->cache(500)->AttrsForProduct;
            $originlGET = $_GET;
            foreach($attributes as $attr){
                if($attr->type != 'boolean'){
                    foreach($attr->Values as $v){
                        $_GET = $originlGET;
                        $_GET[$v->attr_id][] = $v->id;
                        $v->countOnFilter .= '('.count($this->actionProducts(false)).')';

                    }
                }else{
                    foreach($attr->Values as $v){
                        if($v->value_boolean==1){
                            $_GET = $originlGET;
                            $_GET[$v->attr_id][] = $v->id;
                            $v->countOnFilter .= '('.count($this->actionProducts(false)).')';
                        }
                    }
                }

            }
            $_GET = $originlGET;
        }
        else if ($type == 'vendors')
        {
            $attributes = $category->cache(500)->AttrsForVendor;
            $area_data['countries'] = Countries::model()->findAll(array('order' => 'sort DESC'));

            if (!empty($country_id))
                $area_data['regions'] = Regions::model()->findAllByAttributes(array('country_id' => $country_id), array('order' => 'sort DESC'));

            if (!empty($country_id) && !empty($region_id))
                $area_data['cities'] = Cities::model()->findAllByAttributes(array('region_id' => $region_id), array('order' => 'sort DESC'));

            if (!empty($country_id) && !empty($region_id) && !empty($city_id))
                $area_data['metro_stantions'] = MetroStantions::model()->findAllByAttributes(array('city_id' => $city_id));
        }
        else {
            Yii::app()->end();
        }

        $this->renderPartial('_filters', array(
            'attributes' => $attributes,
            'category' => $category,
            'type' => $type,

            'area_data' => $area_data
        ));
    }
}