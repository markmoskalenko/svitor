<?php

class BidController extends AppController
{
    public function filters()
    {
        return array('accessControl');
    }

    public function accessRules()
    {
        return array(
            array(
                'deny',
                'actions' => array('Create', 'MyBid'),
                'users'   => array('?')
            ),
            array(
                'allow',
                'actions' => array('Create', 'MyBid'),
                'users'   => array('@')
            )
        );
    }

    /**
     * Регионы по Стране
     */
    public function actionGetRegions()
    {
        $val    = Yii::app()->request->getPost('Bid');
        $models = Regions::getListByCountry($val['country_id']);
        echo CHtml::tag('option', array('value' => ''), '- Выберите регион -', true);
        foreach ($models as $id => $model) {
            $name = $model->name;
            echo CHtml::tag('option', array('value' => $model->region_id), CHtml::encode($name), true);
        }
    }

    /**
     * Получить города по региону
     */
    public function actionGetCities()
    {
        $val    = Yii::app()->request->getPost('Bid');
        $models = Cities::getListByRegion($val['region_id']);
        echo CHtml::tag('option', array('value' => ''), '- Выберите город -', true);
        foreach ($models as $id => $model) {
            $name = $model->name;
            echo CHtml::tag('option', array('value' => $model->city_id), CHtml::encode($name), true);
        }
    }

    /**
     * Создание новой заявки
     */
    public function actionCreate()
    {
        $model = new Bid;

        if (isset($_POST['Bid'])) {
            $model->attributes = $_POST['Bid'];

            if ($model->save()) {

                $model->distribution();

                $this->redirect(array('index'));
            }

        }

        $this->render(
            'create',
            array(
                 'model' => $model,
            )
        );
    }

    /**
     * Список всех заявок
     */
    public function actionIndex()
    {
        $criteria            = new CDbCriteria;

        $criteria->condition = 'Unix_timestamp(date_end) >= CURDATE()';

        $criteria->scopes = array('opened');

        if(Yii::app()->request->getParam('filter')){

            $criteria->compare('description', Yii::app()->request->getParam('filter'), true);

        }

        $criteria->order = 'id DESC';

        $dataProvider        = new CActiveDataProvider('Bid', array(
                                                                   'criteria' => $criteria
                                                              ));
        $this->render(
            'index',
            array(
                 'dataProvider' => $dataProvider,
                 'filter'       => Yii::app()->request->getParam('filter')
            )
        );
    }

    /**
     * Список заявок пользователя
     */
    public function actionMyBid()
    {
        $criteria            = new CDbCriteria;
        $criteria->condition = 'Unix_timestamp(date_end) >= CURDATE()';
        $criteria->addCondition('user_id = :iser_id');
        $criteria->params = array(':iser_id' => User::getId());
        $criteria->with   = array('messages_all');

        $dataProvider = new CActiveDataProvider('Bid', array(
                                                            'criteria' => $criteria
                                                       ));

        $this->render(
            'my_bid',
            array(
                 'dataProvider' => $dataProvider,
            )
        );
    }

    /**
     * Закрыть заявку
     *
     * @param $id
     */
    public function actionClose( $id ){

        Bid::close( $id );

        $this->redirect(array('/bid/myBid'));

    }

    public function loadModel($id)
    {
        $model = Bid::model()->findByPk($id);
        if ($model === null) {
            throw new CHttpException(404, 'The requested page does not exist.');
        }

        return $model;
    }

    /**
     * Performs the AJAX validation.
     *
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'bid-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
}
