<?php
class mApi extends CApplicationComponent
{
    public static function setFunType($type = 0)
    {
        if ($type == 0 || $type == 1)
            Yii::app()->user->setState('fun_type', $type);
    }

    public static function setWedding(UserWeddings $wedding)
    {
        Yii::app()->user->setState('wedding', $wedding);
    }

    public static function setHoliday(UserHolidays $holiday)
    {
        Yii::app()->user->setState('holiday', $holiday);
    }

    public static function getWedding()
    {
        return Yii::app()->user->getState('wedding', false);
    }

    public static function getHoliday()
    {
        return Yii::app()->user->getState('holiday', false);
    }

    public static function getFunType($str = false)
    {
        if (empty(Yii::app()->user->fun_type))
            mApi::setFunType(0);

        if ($str)
            return Yii::app()->user->fun_type == 0 ? 'wedding' : 'holiday';

        return Yii::app()->user->fun_type;
    }

    public static function getFunId()
    {
        $wedding = mApi::getWedding();
        $holiday = mApi::getHoliday();

        if (mApi::getFunType() == 0 && !empty($wedding))
        {
            return $wedding->id;
        }
        else if (mApi::getFunType() == 1 && !empty($holiday))
        {
            return $holiday->id;
        }
        else
        {
            return false;
        }
    }

    public function getFunTypeTitle($id = 0)
    {
        $fun_types = array(
            '0' => 'Свадьбы',
            '1' => 'Торжества',
            '2' => 'Свадьбы и Торжества'
        );

        return $fun_types[$id];
    }

    public static function sayError($type = null, $msg)
    {
        if ($type == 1)
            die($msg);

        return $msg;
    }

    public function getMonthByLocale($number, $type = 0, $locale = null)
    {
        if ($locale == 'ru')
        {
            $month = mApi::getMonth($number, $type);
        } else {
            $month = array('', 'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');
        }

        return $month[$number];
    }

    static public function getMonth($num = 0, $type = 0, $transform = '')
    {
        $months = array('Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь');

        if ($type == 1) {
            $months = array('Января', 'Февраля', 'Марта', 'Апреля', 'Мая', 'Июня', 'Июля', 'Августа', 'Сентября', 'Октября', 'Ноября', 'Декабря');
        }
        else if ($type == 2) {
            $months = array('янв', 'фев', 'мар', 'апр', 'май', 'июн', 'июл', 'авг', 'сен', 'окт', 'ноя', 'дек');
        }

        // Если не указан какой месяц, то возвращаем массив с месяцами
        if (empty($num))
            return $months;

        $num--;
        if ($num < 0)
            return '';

        if ($transform == 'lower')
            return mb_strtolower($months[$num], Yii::app()->charset);

        return $months[$num];
    }

    // Получение даты в формате "1 января" или "1 января 2012", если с годом
    static public function getDateWithMonth($time = 0, $type = 1, $show_year = false)
    {
        $time = intval($time);

        if (empty($time))
            return '--';

        $year = '';

        // Если получен параметр show_year == true
        if ($show_year) {
            $year = ' '.date('Y', $time);
        }
        // Если год != текущему, то показываем его
        else if (date('Y', time()) != date('Y', $time)) {
            $year = ' '.date('Y', $time);
        }

        $date = date('j', $time).' '.mApi::getMonth(date('n', $time), $type, 'lower').$year;

        return $date;
    }

    // Человеко-понятная дата
    public function humanDate($date)
    {
        $diff = time() - $date;

        if ($diff < 0) {
            //для прошлого
            $days = ceil($diff / 86400);
        } else {
            //для будущего
            $days = floor($diff / 86400);
        }

        $hd = '';
        if ($days == 0) {
            //сегодня
            $hd = 'сегодня в ' . date('H:i', $date);
        }
        else
        {
            if ($days > 0)
            {
                //прошлое
                switch ($days)
                {
                    case 1:
                        $hd = 'вчера в ' . date('H:i', $date);
                        break;

                    case 2:
                        $hd = 'позавчера в ' . date('H:i', $date);
                        break;
                }
            }
            else
            {
                //будущее
                switch ($days) {
                    case -1:
                        $hd = 'завтра в ' . date('H:i', $date);
                        break;
                    case -2:
                        $hd = 'послезавтра в ' . date('H:i', $date);
                        break;
                    case -3:
                        $hd = 'через три дня';
                        break;
                }
            }
        }

        if ($hd == '') {
            $hd = date('d.m.Y H:i', $date);
        }

        return $hd;
    }

    // Человеко-понятная, точная дата
    static public function humanDatePrecise($date)
    {
        $d = time() - $date;

        if ($d > 0)
        {
            if ($d < 3600)
            {
                //минут назад
                switch (floor($d / 60))
                {
                    case 0:
                    case 1:
                        return 'только что';
                        break;

                    case 2:
                        return 'только что';
                        break;

                    case 3:
                        return 'три минуты назад';
                        break;

                    case 4:
                        return 'четыре минуты назад';
                        break;

                    case 5:
                        return 'пять минут назад';
                        break;

                    default:
                        return '' . floor($d / 60) . ' мин. назад';
                        break;
                }

            }
            else if ($d < 18000)
            {
                //часов назад
                switch (floor($d / 3600))
                {
                    case 1:
                        return 'час назад';
                        break;

                    case 2:
                        return 'два часа назад';
                        break;

                    case 3:
                        return 'три часа назад';
                        break;

                    case 4:
                        return 'четыре часа назад';
                        break;
                }

            }
            elseif ($d < 172800)
            {
                //сегодня в ...
                if (date('d') == date('d', $date))
                    return 'сегодня в ' . date('H:i', $date) . '';

                if (date('d', time() - 86400) == date('d', $date))
                    return 'вчера в ' . date('H:i', $date) . '';

                if (date('d', time() - 172800) == date('d', $date))
                    return 'позавчера в ' . date('H:i', $date) . '';
            }
        }
        else
        {
            // В будущем
            $d *= -1;

            if ($d < 3600)
            {
                // минут
                switch (floor($d / 60))
                {
                    case 0:
                    case 1:
                        return 'сейчас';
                        break;

                    case 2:
                        return 'через две минуты';
                        break;

                    case 3:
                        return 'через три минуты';
                        break;

                    case 4:
                        return 'через четыре минуты';
                        break;

                    case 5:
                        return 'через пять минут';
                        break;

                    default:
                        return 'через ' . floor($d / 60) . ' мин.';
                        break;
                }
            }
            else if ($d < 18000)
            {
                //часов
                switch (floor($d / 3600))
                {
                    case 1:
                        return 'через час';
                        break;

                    case 2:
                        return 'через два часа';
                        break;

                    case 3:
                        return 'через три часа';
                        break;

                    case 4:
                        return 'через четыре часа';
                        break;
                }
            }
            elseif ($d < 172800)
            {
                //сегодня
                if (date('d') == date('d', $date))
                    return 'сегодня в ' . date('H:i', $date) . '';

                if (date('d', time() - 86400) == date('d', $date))
                    return 'завтра в ' . date('H:i', $date) . '';

                if (date('d', time() - 172800) == date('d', $date))
                    return 'послезавтра в ' . date('H:i', $date) . '';
            }
        }

        return self::getDateWithMonth($date);
    }

    // Возвращает количество оставшихся дней до указанной даты
    static public function getDaysLeft($date)
    {
        $day = date('d', $date);
        $month = date('m', $date);
        $year = date('Y', $date);

        $days_count = ceil((mktime(0,0,0, $month, $day, $year) - time())/86400);

        if ($days_count <= 0)
            return '';

        return $days_count.' '.mApi::plularStr($days_count, 'день', 'дня', 'дней');
    }

    // Множественное число (1 - день, 2 - дня, 5 - дней)
    static public function plularStr($n, $str1, $str2, $str3)
    {
        if ($n % 10 == 1 && $n % 100 != 11) {
            $plular = $str1;
        } else {
            if ($n % 10 >=2 && $n % 10 <= 4 && ($n % 100 < 10 || $n % 100 >= 20)) {
                $plular = $str2;
            } else {
                $plular = $str3;
            }
        }

        return $plular;
    }

    // Обрезание строки сохряняя целые слова
    static public function cutStr($str, $length = 100, $end = '...')
    {
        $str = mb_substr($str, 0, $length+1, Yii::app()->charset);

        if (mb_strlen($str, Yii::app()->charset) > $length)
        {
            $str = wordwrap($str, $length);
            $i = strpos($str, "\n");
            if ($i) {
                $str = mb_substr($str, 0, $i, Yii::app()->charset);
            }

            $str .= $end;
        }

        return $str;
    }

    public static function subStr($str, $start = 0, $end)
    {
        $length = mb_strlen($str, Yii::app()->charset);

        $new_str = mb_substr($str, $start, $end, Yii::app()->charset);
        $new_length = mb_strlen($new_str, Yii::app()->charset);

        if ($length != $new_length)
            $new_str .= '...';

        return $new_str;
    }

    static public function convertFileSize($size)
    {
        $unit = array('', 'KB', 'MB', 'GB', 'TB', 'PB');

        if ($size <= 0) {
            return '0 ' . $unit[1];
        } else {
            $byte_size = $size/pow(1024, ($i=floor(log($size, 1024))));
        }

        if (is_int($byte_size)) {
            return $byte_size . ' ' . $unit[$i];
        } else {
            preg_match('/^[0-9]+\.[0-9]{2}/', $byte_size, $matches);

            if (isset($matches[0]))
                return $matches[0] . ' ' . $unit[$i];
        }

        return '??';
    }

    static public function getCurrencyByCount($count)
    {
        $count = intval($count);

        if ($count <= 0 || $count == '')
            $count = 0;

        $str = Yii::app()->params['currency']['str'];
        if (!empty($str[0]) && !empty($str[1]) && !empty($str[2]))
            return $count.' '.mApi::plularStr($count, $str[0], $str[1], $str[2]);

        return '<div class="msg_error">Отсутствуют множественные варианты для валюты.</div>';
    }

    static public function getCurrency($type = 'char')
    {
        if ($type != 'char' && $type != 'short')
            return;

        if ($type == 'char')
        {
            $char =  Yii::app()->params['currency']['char'];
            if (!empty($char))
                return $char;
        }

        else if ($type == 'short')
        {
            $short = Yii::app()->params['currency']['short'];
            if (!empty($short))
                return $short;
        }

        return '<div class="msg_error">Неверно настроен конфиг валют (не указана буква или короткий вариант)</div>';
    }


    /**
     * @param $aArray
     *
     * @return array|null
     */
    static public function intArray( $aArray )
    {
        if( !is_array($aArray) ) return null;

        foreach( $aArray as &$aItem )
        {
            if( is_array( $aItem ) )
            {
                $aItem = self::intArray( $aItem );
            }
            elseif( is_string( $aItem ) )
            {
                $aItem = intval( $aItem );
            }
        }

        return $aArray;
    }


    public static function translitString($data)
    {
        if (empty($data))
            return '';

        $abc = array(
            ' ' => '_',
            'А' => 'A',
            'Б' => 'B',
            'В' => 'V',
            'Г' => 'G',
            'Д' => 'D',
            'Е' => 'E',
            'Ё' => 'JO',
            'Ж' => 'ZH',
            'З' => 'Z',
            'И' => 'I',
            'Й' => 'JJ',
            'К' => 'K',
            'Л' => 'L',
            'М' => 'M',
            'Н' => 'N',
            'О' => 'O',
            'П' => 'P',
            'Р' => 'R',
            'С' => 'S',
            'Т' => 'T',
            'У' => 'U',
            'Ф' => 'F',
            'Х' => 'KH',
            'Ц' => 'C',
            'Ч' => 'CH',
            'Ш' => 'SH',
            'Щ' => 'SHH',
            'Ъ' => '"',
            'Ы' => 'Y',
            'Ь' => '', // '
            'Э' => 'EH',
            'Ю' => 'JU',
            'Я' => 'JA',
            'а' => 'a',
            'б' => 'b',
            'в' => 'v',
            'г' => 'g',
            'д' => 'd',
            'е' => 'e',
            'ё' => 'jo',
            'ж' => 'zh',
            'з' => 'z',
            'и' => 'i',
            'й' => 'jj',
            'к' => 'k',
            'л' => 'l',
            'м' => 'm',
            'н' => 'n',
            'о' => 'o',
            'п' => 'p',
            'р' => 'r',
            'с' => 's',
            'т' => 't',
            'у' => 'u',
            'ф' => 'f',
            'х' => 'kh',
            'ц' => 'c',
            'ч' => 'ch',
            'ш' => 'sh',
            'щ' => 'shh',
            'ъ' => '', // "
            'ы' => 'y',
            'ь' => '',  // '
            'э' => 'eh',
            'ю' => 'ju',
            'я' => 'ja',
            '\\' => '_',
            '/' => '_',
            ',' => '',
            '.' => '',

        );

        $keys = array_keys($abc);
        $values = array_values($abc);
        $result = str_replace($keys, $values, $data);

        return $result;
    }
}