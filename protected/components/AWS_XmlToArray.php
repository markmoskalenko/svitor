<?php
class AWS_XmlToArray
{
    private $xml;

    public function __construct($xml)
    {
        if (is_file($xml)) {
            $this->xml = file_get_contents($xml);
        } else {
            $this->xml = $xml;
        }
    }

    public function createArray()
    {
        $awsXml      = $this->xml;
        $awsVals     = array();
        $awsIndex    = array();
        $awsRetArray = array();
        $awsParser   = xml_parser_create();
        xml_parser_set_option($awsParser, XML_OPTION_SKIP_WHITE, 1);
        xml_parser_set_option($awsParser, XML_OPTION_CASE_FOLDING, 0);
        xml_parse_into_struct($awsParser, $awsXml, $awsVals, $awsIndex);
        xml_parser_free($awsParser);
        $i                     = 0;
        if(!isset($awsVals[$i]['tag']))return false;
        $awsName               =  $awsVals[$i]['tag'];
        $awsRetArray[$awsName] = isset($awsVals[$i]['attributes']) ? $awsVals[$i]['attributes'] : '';
        $awsRetArray[$awsName] = $this->_struct_to_array($awsVals, $i);

        return $awsRetArray;
    }

    protected function _struct_to_array($awsVals, &$i)
    {
        $awsChild = array();
        if (isset($awsVals[$i]['value'])) array_push($awsChild, $awsVals[$i]['value']);

        while ($i++ < count($awsVals)) {
            if(!isset($awsVals[$i]['type'])) continue;
            switch ($awsVals[$i]['type']) {
                case 'cdata':
                    array_push($awsChild, $awsVals[$i]['value']);
                    break;

                case 'complete':
                    $awsName = $awsVals[$i]['tag'];
                    if (!empty($awsName)) {
                        if ($awsName == 'param' ||  $awsName == 'category') {
                            $awsChild[$awsName][] = array(
                                'value' => (isset($awsVals[$i]['value'])) ? ($awsVals[$i]['value']) : '',
                                'param' => $awsVals[$i]['attributes']
                            );
                        }elseif ($awsName == 'picture') {
                            $awsChild[$awsName][] = isset($awsVals[$i]['value']) ? $awsVals[$i]['value'] : '';
                        } else {
                         /*  $awsChild[$awsName] = array(
                                'value'   =>(isset($awsVals[$i]['value'])) ? ($awsVals[$i]['value']) : '',
                                'attr'    => (isset($awsVals[$i]['attributes'])) ? ($awsVals[$i]['attributes']) : ''
                            )*/
                            $awsChild[$awsName] = (isset($awsVals[$i]['value'])) ? ($awsVals[$i]['value']) : '';
                            if (isset($awsVals[$i]['attributes'])) {
                                $awsChild[$awsName]['attr'] = $awsVals[$i]['attributes'];
                            }
                        }
                    }
                    break;

                case 'open':
                    $awsName = $awsVals[$i]['tag'];

                    $temp = array(
                        'attr' => (isset($awsVals[$i]['attributes'])) ? $awsVals[$i]['attributes'] : '',
                        'data' => $this->_struct_to_array($awsVals, $i)
                    );
                    $awsChild[$awsName][] = $temp;
                    unset($temp);
                    break;

                case 'close':
                    return $awsChild;
                    break;
            }
        }
        return $awsChild;
    }
}

?>