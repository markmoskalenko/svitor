<?php
class UserIdentity extends CUserIdentity
{
    private $_id;

    /*
     * @param: User $user - если он передан, то проверять ничего не нужно — сразу авторизируем.
     */
    public function auth($user = array())
    {
        if (empty($user))
        {
            if (preg_match('/\@/i', $this->username)) {
                $user = User::model()->find('LOWER(email)=?', array(strtolower($this->username)));
            } else {
                $user = User::model()->find('LOWER(login)=?', array(strtolower($this->username)));
            }

            if (!empty($user) && $user->password != hash('md5', $user->salt.strtoupper($this->password)))
                return false;
        }

        if (!empty($user))
        {
            $this->_id = $user->id;
            $this->username = $user->login;

            $this->setState('country_id', $user->country_id);
            $this->setState('region_id', $user->region_id);
            $this->setState('city_id', $user->city_id);
            $this->setState('img_id', $user->img_id);
            $this->setState('img_filename', $user->img_filename);
            $this->setState('security_level', $user->security_level);
            $this->setState('last_activity', time());

            $this->errorCode = self::ERROR_NONE;

            // Update Info
            $user['last_ip'] = Yii::app()->request->getUserHostAddress();
            $user['last_login'] = time();
            $user['last_activity'] = time();
            $user->update();
        }

        return !$this->errorCode;
    }

    public function getId()
    {
        return $this->_id;
    }
}