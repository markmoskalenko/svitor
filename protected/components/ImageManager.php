<?php
class ImageManager extends CApplicationComponent
{
    public $file;

    private $baseDirName = 'uploads_img';
    private $securityCode = 'f@&jkw0Grjk932rf#R@343#FMK57Lf2Q#FLKW12Efwe4fFEl97wef';

    private $success;
    private $error;

    public function ImageManager($file = '')
    {
        if (!empty($file))
            $this->file = $file;
    }

    private function getSecurityCode()
    {
        return $this->securityCode;
    }

    private function addError($msg)
    {
        $this->error = '{"status":"error", "msg":"'.$msg.'"}';
        return false;
    }

    public function getError()
    {
        return $this->error;
    }

    private function addSuccess($img_id, $file_name, $path = '')
    {
        $this->success = '{"status":"ok", "img_id":'.$img_id.', "file_name":"'.$file_name.'", "path":"'.$path.'"}';
    }

    public function getSuccess()
    {
        return $this->success;
    }

    public function getPathByImgId($id)
    {
        $id = intval($id);

        // В каждом sub-каталоге может быть не более 1000 картинок
        $dir_id = intval($id / 1000);
        $dir_name = substr(md5($dir_id), 0, 4).$dir_id;

        $dir_path = $this->baseDirName.'/'.$dir_name;

        return $dir_path;
    }

    private function checkPath($dir_path)
    {
        if (!is_dir($dir_path))
        {
            if (!mkdir($dir_path)) {
                $this->addError('Не удалось создать каталог.');
                return false;
            }

            mkdir($dir_path.'/medium');
            mkdir($dir_path.'/small');
        }

        return true;
    }

    public  function uploadImageUrl( ){

        $file_type = mb_strrchr( $this->file, '.');

        if( $file_type != '.png' && $file_type != '.jpg' && $file_type != '.jpeg' && $file_type != '.gif') {

            $this->addError('Не удается загрузить фото, проверьте url.');

            return false;

        }

        /*$ch = curl_init($this->file);*/

        $type = substr($this->file, strripos( $this->file, '.'), strlen($this->file));

        $fileName = "temp".rand(1,1000).time().$type;

        $path = 'uploads'.DIRECTORY_SEPARATOR.$fileName;

		
		chdir('/var/www/svitor.ru');
		//file_put_contents($path, file_get_contents($this->file));
		
		$ch = curl_init($this->file);
		$fp = fopen($path, 'wb');
        curl_setopt($ch, CURLOPT_FILE, $fp);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_exec($ch);
        curl_close($ch);
        fclose($fp);
		//$to = "/var/www/test.svitor.ru/uploads/";  
		//$from = $this->file;
		//passthru("/usr/bin/wget $from -O $path > /dev/null &", $output); 
        /*$f_o=fopen($file,"a+");
        fwrite($f_o, "\n\r---------------------------------------------------");
        fwrite($f_o, "\n\rВыполняется крон - ".Date('m-d-Y H:i:s'));
        fwrite($f_o, "\n\Ркзультат - ".$res));
        fclose($f_o);*/

        $result = $this->upload( $path );
        if(file_exists($path)){unlink($path);}


        return $result;
    }

    public function upload( $file = false )
    {
        $file = ( $file ? $file : $this->file['tmp_name'] );

        $imageHandler = new CImageHandler;

        if (empty($this->file)) {
            $this->addError('Не выбран файл');
            return false;
        }



        if (!$image = $imageHandler->load($file)) {
            $this->addError('Не удалось загрузить картинку, размер загружаемого файла не может превышать 8 МБ.');
            return false;
        }

        // Делаем запись в базе перед загрузкой, чтобы получить ID, на основе которого
        // мы определим в какой каталог загружать картинку.
        $fileImages = new FileImages('add');
        $fileImages['is_ready'] = 0;
        $fileImages['date'] = time();

/*        if (!User::isGuest())
            $fileImages['uid'] = User::getId();
*/
        if (!$fileImages->save()) {
            $this->addError('Не удалось сохранить картинку');
            return false;
        }

        $directory_path = $this->getPathByImgId($fileImages->id);
        // Обязательно проверим путь, если такого нет,
        // то checkPath позаботится о том, чтобы он появился.
        $this->checkPath($directory_path);

        // Если картинка слишком большая...
        if ($image->getWidth() > '1024' || $image->getHeight() > '1024')
            $image->resize(1024, 1024, true);

        // Поулчаем имя для картинки...
        $fileImages->filename = $this->getNewNameByPath($directory_path, '.jpg');

        // Если есть ошибки, то не идем дальше...
        if ($this->getError())
            return false;

        // Загружаем в каталог
        $large_status = $image->save($directory_path.'/'.$fileImages->filename, false, 80);

        if ($large_status) {
            $medium = $image->reload($file)
                    ->adaptiveThumb(100, 100)
                    ->save($directory_path.'/medium/'.$fileImages->filename, false, 80);

            if ($medium) {
                $small = $image->reload($file)
                    ->adaptiveThumb(50, 50)
                    ->save($directory_path.'/small/'.$fileImages->filename, false, 95);

                if ($small) {
                    $fileImages['path'] = $directory_path;
                    $fileImages['filesize'] = filesize($fileImages->path.'/'.$fileImages->filename);
                    $fileImages['is_ready'] = 1;
                    $fileImages->update();

                    $this->addSuccess($fileImages->id, $fileImages->filename, $fileImages->path);
                    return true; // Обязательно выходим с истиной
                }
            }
        }

        // Если дошло до этого места, значит не все хорошо... :(
        $this->addError('Произошла не известная ошибка.');
        return false;
    }



    private function getNewNameByPath($dir_path, $format = '.jpg')
    {
        $new_name = $this->generateNameByPath($dir_path, $format);

        // На всякий случай...
        if (file_exists($dir_path.'/'.$new_name))
            $new_name = $this->generateNameByPath($dir_path, $format);

        return $new_name;
    }

    private function generateNameByPath($dir_path, $format = '.jpg')
    {
        $name = md5($dir_path.'-'.$this->getSecurityCode().'-'.time());
        $length = mb_strlen($name, Yii::app()->charset);

        $name = mb_substr($name, $length-15, $length).$format;

        return $name;
    }

    public function getUrlById($img_id = 0, $size = 'large', $img_filename = '')
    {
        if ($size != 'small' && $size != 'medium' && $size != 'large')
            throw new ErrorException('Size задан не корректно (возможные варианты: small, medium, large)');

        if (empty($img_id) || empty($img_filename))
        {
            // TODO: сделать заглушку для размера large и убрать этот костыль
            if ($size == 'large')
                $size = 'medium';

            return '/static/images/nophoto_'.$size.'.jpg';
        }

        $img_id = intval($img_id);
        $img_filename = htmlspecialchars($img_filename);

        $url = '/'.$this->getPathByImgId($img_id);

        if ($size != 'large') {
            return $url.'/'.$size.'/'.$img_filename;
        } else {
            return $url.'/'.$img_filename;
        }
    }
}
