<?php
class MSort extends CSort
{
    /* defaultLabel = array('attribute', 'asc or desc');
     * Необходим только для "class" и корректной ссылки (если пустой $_GET['sort']!!!)
     */
    public $defaultLabel;

    public function createUrl($controller, $directions)
    {
        $sorts=array();
        foreach($directions as $attribute=>$descending)
        {
            // Создадим корректную ссылку для атрибута по умолчанию (если пустой $_GET['sort']!!!)
            if (empty($_GET[$this->sortVar]) && isset($this->defaultLabel[0]) && isset($this->defaultLabel[1]) && $attribute == $this->defaultLabel[0])
            {
                if ($this->defaultLabel[1] == 'asc' || empty($this->defaultLabel[1])) {
                    $sorts[] = $attribute.$this->separators[1].$this->descTag;
                } else {
                    $sorts[] = $attribute;
                }
            }
            else {
                $sorts[]=$descending ? $attribute.$this->separators[1].$this->descTag : $attribute;
            }
        }

        $params=$this->params===null ? $_GET : $this->params;
        $params[$this->sortVar] = implode($this->separators[0],$sorts);

        return $controller->createUrl($this->route,$params);
    }

    public function link($attribute,$label=null,$htmlOptions=array())
    {
        // Установим корректный "class" для ссылки, которая == сортировке по умолчанию (если пустой $_GET['sort']!!!)
        if (empty($_GET[$this->sortVar]) && isset($this->defaultLabel[0]) && isset($this->defaultLabel[1]) && $attribute == $this->defaultLabel[0])
        {
            if ($this->defaultLabel[1] == 'asc' || empty($this->defaultLabel[1])) {
                $htmlOptions['class'] = 'asc';
            }
            else if ($this->defaultLabel[1] == 'desc' || empty($this->defaultLabel[1])) {
                $htmlOptions['class'] = 'desc';
            }
        }

        return parent::link($attribute,$label,$htmlOptions);
    }
}