<?php
/**
 * Created by JetBrains PhpStorm.
 * User: it-Yes
 * Date: 16.07.13
 * Time: 22:07
 * To change this template use File | Settings | File Templates.
 */

class DumpDB {

    private $oDb;
    private $path;
    private $fileName;

    private static $instance;

    /**
     * Конструктор
     */
    private function __construct()
    {
        $this->oDb      = Yii::app()->db;
        $this->path     = DOCUMENT_ROOT;
        $this->fileName = 'mask.sql';
    }

    /**
     * @return DumpDB
     */
    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    public function createDump(){
        $command = "mysqldump -u {$this->oDb->username} -p{$this->oDb->password} -f demosvitor > {$this->path}/{$this->fileName}";
        echo exec($command);
    }

    public function importDump(){
        $this->dropDB();
        $command = "mysql -u {$this->oDb->username} -p{$this->oDb->password} demosvitor < {$this->path}/{$this->fileName}";
        exec( $command );
    }

    private function dropDB(){
        $this->oDb->createCommand("DROP DATABASE `demosvitor`")->execute();
        $this->oDb->createCommand("CREATE DATABASE `demosvitor` CHARACTER SET utf8 COLLATE utf8_general_ci")->execute();
    }
}
