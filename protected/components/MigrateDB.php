<?php
/**
 * Created by JetBrains PhpStorm.
 * User: it-Yes
 * Date: 16.07.13
 * Time: 22:07
 * To change this template use File | Settings | File Templates.
 */

class MigrateDB {

    const CHILD_DEMO_USER_ID = 693;

    const ROOT_DEMO_USER_ID  = 666;

    const HOLIDAY = 1;

    const WEDDING = 2;

    //Хранит отношение ROOT_ID => CHILD_ID, таблицы UserVendorCategories
    private $VENDOR_CATEGORY = array();

    //Хранит отношение ROOT_ID => CHILD_ID, таблицы UserGuests
    private $GUEST = array();

    private static $instance;

    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    public function start(){

        $this->clear();

        $this->startHolidayTransaction();

        $this->startWeddingsTransaction();

    }

    private function getHoliday(){
        return UserHolidays::model()->findAll('uid = :uid', array(':uid'=>self::ROOT_DEMO_USER_ID));
    }

    private function getWedding(){
        return UserWeddings::model()->findAll('uid = :uid', array(':uid'=>self::ROOT_DEMO_USER_ID));
    }

    private function getCriteria( $essence, $rootId ){
        $criteria = new CDbCriteria;

        if( $essence == self::WEDDING ) $criteria->condition = 'wedding_id = :id AND uid = :uid'; else
            if( $essence == self::HOLIDAY ) $criteria->condition = 'holiday_id = :id AND uid = :uid';

        $criteria->params = array(':id' => $rootId, ':uid'=>self::ROOT_DEMO_USER_ID);

        return $criteria;
    }

    /**
     * Сущность - Торжество
     * Миграция данных из пользователя TEST в пользователь TEST PUBLIC
     * Миграции таблиц выполняются в определённом порядке. Не рекомендуется менять его.
     */
    private function startHolidayTransaction(){

        $holidays = $this->getHoliday();

        if(!count($holidays)) return false;

        $this->transaction( $holidays, self::HOLIDAY );

    }

    private function startWeddingsTransaction(){

        $wedding = $this->getWedding();

        if(!count($wedding)) return false;

        $this->transaction( $wedding, self::WEDDING );

    }

    private function transaction( $data, $essence ){

        $this->vendorFavoritesInsert();

        foreach($data as $item){
            $rootId = $item->id;

            if( $essence == self::HOLIDAY )
                $childId = $this->holidayInsert( $item ); else
                $childId = $this->weddingsInsert( $item );

            $this->siteInsert( $rootId, $childId, $essence );

            $this->checklistInsert( $rootId, $childId, $essence );

            $this->budgetInsert( $rootId, $childId, $essence );

            $this->guestInsert( $rootId, $childId, $essence );

            $this->vendorCategoriesInsert( $rootId, $childId, $essence );

            $this->vendorTeamInsert( $rootId, $childId, $essence );

            //TODO::ОБРАЩЕНИЕ К ГОСТЯМ (не сохранилось)
            $this->wannaGiftsInsert( $rootId, $childId, $essence );

            $this->routineInsert( $rootId, $childId, $essence );

            $this->photosInsert($rootId, $childId, $essence);

            $this->giftsInsert($rootId, $childId, $essence);
        }
    }

    private function holidayInsert( $item ){

        $childModel = new UserHolidays('add');

        $childModel->attributes = $item->attributes;

        $childModel->uid = self::CHILD_DEMO_USER_ID;

        $childModel->save(false);

        return $childModel->id;
    }

    private function weddingsInsert( $item ){

        $childModel = new UserWeddings('add');

        $childModel->attributes = $item->attributes;

        $childModel->uid = self::CHILD_DEMO_USER_ID;

        $childModel->save(false);

        return $childModel->id;
    }

    private function siteInsert( $rootId, $childId, $essence ){

        $rootModel = UserSite::model()->findAll( $this->getCriteria( $essence, $rootId ) );

        if(count($rootModel)){

          foreach( $rootModel as $item ){

              $childModel             = new UserSite('add');

              $childModel->attributes = $item->attributes;

              $childModel->uid        = self::CHILD_DEMO_USER_ID;

              if( $essence == self::WEDDING ) $childModel->wedding_id = $childId; else
              if( $essence == self::HOLIDAY ) $childModel->holiday_id = $childId;

              $childModel->save(false);
          }

        }
    }

    private function checklistInsert( $rootId, $childId, $essence ){

        $rootModel = UserChecklist::model()->findAll($this->getCriteria( $essence, $rootId ));

        if(count($rootModel)){

            foreach( $rootModel as $item ){

                $childModel             = new UserChecklist('add');

                $childModel->attributes = $item->attributes;

                $childModel->uid        = self::CHILD_DEMO_USER_ID;

                if( $essence == self::WEDDING ) $childModel->wedding_id = $childId; else
                if( $essence == self::HOLIDAY ) $childModel->holiday_id = $childId;

                $childModel->budget_id  = 0;

                $childModel->save(false);

                if( $item->budget_id ){

                    $task_id  = $childModel->id;

                    $budgetId = $this->budgetCopy( $item->budget_id, $childId, $task_id, $essence );

                    $tempModel = UserChecklist::model()->find('id = :id', array(':id' => $task_id));

                    $tempModel->budget_id = $budgetId;

                    $tempModel->save(false);
                }

            }

        }
    }

    private function budgetCopy( $id, $childId, $task_id, $essence ){

        $rootModel              = UserBudget::model()->find('id = :id', array(':id' => $id));

        $childModel             = new UserBudget('add');

        $childModel->attributes = $rootModel->attributes;

        $childModel->uid        = self::CHILD_DEMO_USER_ID;

        if( $essence == self::WEDDING ) $childModel->wedding_id = $childId; else
        if( $essence == self::HOLIDAY ) $childModel->holiday_id = $childId;

        $childModel->task_id    = $task_id;

        $childModel->save(false);

        return $childModel->id;
    }

    private function budgetInsert( $rootId, $childId, $essence ){

        $criteria = $this->getCriteria( $essence, $rootId );

        $criteria -> addCondition('task_id=0');

        $rootModel = UserBudget::model()->findAll($criteria);

        if(count($rootModel)){

            foreach( $rootModel as $item ){

                $childModel             = new UserBudget();

                $childModel->attributes = $item->attributes;

                $childModel->uid        = self::CHILD_DEMO_USER_ID;

                if( $essence == self::WEDDING ) $childModel->wedding_id = $childId; else
                if( $essence == self::HOLIDAY ) $childModel->holiday_id = $childId;

                $childModel->save(false);
            }

        }
    }

    //TODO:: Извещен, галочка не сохраняется
    private function guestInsert( $rootId, $childId, $essence ){

        $rootModel = UserGuests::model()->findAll($this->getCriteria( $essence, $rootId ));

        if(count($rootModel)){

            foreach( $rootModel as $item ){

                $childModel             = new UserGuests('add');

                $childModel->attributes = $item->attributes;

                $childModel->uid        = self::CHILD_DEMO_USER_ID;

                if( $essence == self::WEDDING ) $childModel->wedding_id = $childId; else
                if( $essence == self::HOLIDAY ) $childModel->holiday_id = $childId;

                $childModel->save(false);

                $this->GUEST[$item->id] = $childModel->id;
            }

        }
    }

    /**
     * Мои компании - категории
     * @param $rootId
     * @param $childId
     */
    private function vendorCategoriesInsert( $rootId, $childId, $essence ){

        $rootModel = UserVendorCategories::model()->findAll($this->getCriteria( $essence, $rootId ));

        if(count($rootModel)){

            foreach( $rootModel as $item ){

                $childModel             = new UserVendorCategories();

                $childModel->attributes = $item->attributes;

                $childModel->uid        = self::CHILD_DEMO_USER_ID;

                if( $essence == self::WEDDING ) $childModel->wedding_id = $childId; else
                if( $essence == self::HOLIDAY ) $childModel->holiday_id = $childId;

                $childModel->save(false);

                $this->VENDOR_CATEGORY[$item->id] = $childModel->id;

            }
        }
    }

    private function vendorFavoritesInsert(){

        $rootModel = UserVendorFavorites::model()->findAll('uid = :uid', array(':uid'=>self::ROOT_DEMO_USER_ID));

        if(count($rootModel)){

            foreach( $rootModel as $item ){

                $childModel             = new UserVendorFavorites();

                $childModel->attributes = $item->attributes;

                $childModel->uid        = self::CHILD_DEMO_USER_ID;

                $childModel->save(false);
            }

        }
    }

    private function vendorTeamInsert( $rootId, $childId, $essence ){

        $rootModel = UserVendorTeam::model()->findAll($this->getCriteria( $essence, $rootId ));

        if(count($rootModel)){

            foreach( $rootModel as $item ){

                $childModel              = new UserVendorTeam();

                $childModel->attributes  = $item->attributes;

                $childModel->user_cat_id = $this->VENDOR_CATEGORY[$item->user_cat_id];

                $childModel->uid         = self::CHILD_DEMO_USER_ID;

                if( $essence == self::WEDDING ) $childModel->wedding_id = $childId; else
                if( $essence == self::HOLIDAY ) $childModel->holiday_id = $childId;

                $childModel->save(false);
            }

        }
    }

    private function wannaGiftsInsert( $rootId, $childId, $essence ){

        $rootModel = UserWannaGifts::model()->findAll($this->getCriteria( $essence, $rootId ));

        if(count($rootModel)){

            foreach( $rootModel as $item ){

                $childModel              = new UserWannaGifts('add');

                $childModel->attributes  = $item->attributes;

                $childModel->guest_id    = $this->GUEST[$item->guest_id];

                $childModel->uid         = self::CHILD_DEMO_USER_ID;

                if( $essence == self::WEDDING ) $childModel->wedding_id = $childId; else
                if( $essence == self::HOLIDAY ) $childModel->holiday_id = $childId;

                $childModel->save(false);
            }

        }
    }

    private function routineInsert( $rootId, $childId, $essence ){

        $rootModel = UserRoutine::model()->findAll($this->getCriteria( $essence, $rootId ));

        if(count($rootModel)){

            foreach( $rootModel as $item ){

                $childModel              = new UserRoutine();

                $childModel->attributes  = $item->attributes;

                $childModel->uid         = self::CHILD_DEMO_USER_ID;

                if( $essence == self::WEDDING ) $childModel->wedding_id = $childId; else
                if( $essence == self::HOLIDAY ) $childModel->holiday_id = $childId;

                $childModel->save(false);
            }

        }
    }

    private function photosInsert( $rootId, $childId, $essence ){

        $rootModel = UserImages::model()->findAll($this->getCriteria( $essence, $rootId ));

        if(count($rootModel)){

            foreach( $rootModel as $item ){

                $childModel              = new UserImages();

                $childModel->attributes  = $item->attributes;

                $childModel->uid         = self::CHILD_DEMO_USER_ID;

                if( $essence == self::WEDDING ) $childModel->wedding_id = $childId; else
                if( $essence == self::HOLIDAY ) $childModel->holiday_id = $childId;

                $childModel->save(false);
            }

        }
    }

    private function giftsInsert( $rootId, $childId, $essence ){

        $rootModel = UserGifts::model()->findAll($this->getCriteria( $essence, $rootId ));

        if(count($rootModel)){

            foreach( $rootModel as $item ){

                $childModel              = new UserGifts();

                $childModel->attributes  = $item->attributes;

                $childModel->uid         = self::CHILD_DEMO_USER_ID;

                if( $essence == self::WEDDING ) $childModel->wedding_id = $childId; else
                if( $essence == self::HOLIDAY ) $childModel->holiday_id = $childId;

                $childModel->save(false);
            }

        }
    }

    private function clear(){
        UserHolidays::model()->deleteAll('uid = :uid', array(':uid'=>self::CHILD_DEMO_USER_ID));

        UserWeddings::model()->deleteAll('uid = :uid', array(':uid'=>self::CHILD_DEMO_USER_ID));

        UserSite::model()->deleteAll('uid = :uid', array(':uid'=>self::CHILD_DEMO_USER_ID));

        UserChecklist::model()->deleteAll('uid = :uid', array(':uid'=>self::CHILD_DEMO_USER_ID));

        UserBudget::model()->deleteAll('uid = :uid', array(':uid'=>self::CHILD_DEMO_USER_ID));

        UserGuests::model()->deleteAll('uid = :uid', array(':uid'=>self::CHILD_DEMO_USER_ID));

        UserVendorCategories::model()->deleteAll('uid = :uid', array(':uid'=>self::CHILD_DEMO_USER_ID));

        UserVendorFavorites::model()->deleteAll('uid = :uid', array(':uid'=>self::CHILD_DEMO_USER_ID));

        UserVendorTeam::model()->deleteAll('uid = :uid', array(':uid'=>self::CHILD_DEMO_USER_ID));

        UserWannaGifts::model()->deleteAll('uid = :uid', array(':uid'=>self::CHILD_DEMO_USER_ID));

        UserImages::model()->deleteAll('uid = :uid', array(':uid'=>self::CHILD_DEMO_USER_ID));

        UserGifts::model()->deleteAll('uid = :uid', array(':uid'=>self::CHILD_DEMO_USER_ID));
    }

}
