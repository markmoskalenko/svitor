<?php
class OdnoklassnikiService extends OdnoklassnikiOAuthService
{
    protected function fetchAttributes()
    {
        $info = $this->makeSignedRequest('http://api.odnoklassniki.ru/fb.do', array(
            'query' => array(
                'method' => 'users.getCurrentUser',
                'format' => 'JSON',
                'application_key' => $this->client_public,
                'client_id' => $this->client_id,
            ),
        ));

        $this->attributes['id'] = $info->uid;
        $this->attributes['login'] = mApi::translitString(strtolower($info->name));

        $this->attributes['name'] = $info->first_name;
        $this->attributes['family'] = $info->last_name;

        if (!empty($info->gender))
            $this->attributes['gender'] = strtolower($info->gender) == 'male' ? 0 : 1;
    }
}