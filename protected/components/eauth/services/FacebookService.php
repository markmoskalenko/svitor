<?php
class FacebookService extends FacebookOAuthService
{
    protected function fetchAttributes() {
        $info = (object) $this->makeSignedRequest('https://graph.facebook.com/me', array(
            'query' => array(
                // see more http://developers.facebook.com/docs/reference/api/user/
                'fields' => 'id,name,first_name,last_name,gender',
            )
        ));

        $this->attributes['id'] = $info->id;
        $this->attributes['login'] = mApi::translitString(strtolower($info->name));

        $this->attributes['name'] = $info->first_name;
        $this->attributes['family'] = $info->last_name;

        if (!empty($info->gender))
            $this->attributes['gender'] = strtolower($info->gender) == 'male' ? 0 : 1;
    }
}