<?php
class TwitterService extends TwitterOAuthService
{
    protected function fetchAttributes() {
        $info = $this->makeSignedRequest('https://api.twitter.com/1.1/account/verify_credentials.json');

        $this->attributes['id'] = $info->id;
        $this->attributes['login'] = mApi::translitString(strtolower($info->screen_name));
    }
}