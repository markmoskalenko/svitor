<?php
class VkontakteService extends VKontakteOAuthService
{
    protected function fetchAttributes() {
        $info = (array)$this->makeSignedRequest('https://api.vk.com/method/users.get.json', array(
            'query' => array(
                'uids' => $this->uid,
                // uid, first_name and last_name is always available. See more: http://vk.com/dev/fields
                'fields' => 'first_name, last_name, sex, screen_name',
            ),
        ));

        $info = $info['response'][0];

        $this->attributes['id'] = $info->uid;
        $this->attributes['login'] = mApi::translitString(strtolower($info->screen_name));

        $this->attributes['name'] = $info->first_name;
        $this->attributes['family'] = $info->last_name;

        // Gender: 1 - Family, 2- Male
        if (!empty($info->sex))
            $this->attributes['gender'] = $info->sex == 1 ? 1 : 0;
    }
}