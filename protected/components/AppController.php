<?php
class AppController extends CController
{
    function init()
    {
        // Обновление последней активности пользователя при каждом обращении к сайту
        if (!User::isGuest())
        {
            $u_last_activity = User::getLastActivity();

            if ($u_last_activity == false)
                User::updateLastActivity();

            // Вычисляем сколько секунд прошло с последней активности
            $diff = intval(time() - $u_last_activity);
            $minutes = $diff / 60;

            // Если последняя активность была более чем 5 минут назад
            if ($minutes >= 5)
                User::updateLastActivity();
        }

        // Режим планировщика переключен на "Торжества"
        if (isset($_GET['holiday'])) {
            mApi::setFunType(1);
            Yii::app()->user->setState('wedding', false);
        }
        // Режим планировщика переключен на "Свадьба"
        else if (isset($_GET['wedding'])) {
            mApi::setFunType(0);
            Yii::app()->user->setState('holiday', false);
        }

        // Выбрано торжество (из upBox окна), обновим данные в сессии
        if (!User::isGuest() && !empty($_GET['holiday']) && mApi::getFunType() == 1)
        {
            $data = UserHolidays::model()->findByAttributes(array('uid' => User::getId(), 'id' => intval($_GET['holiday'])));

            if (!empty($data))
                mApi::setHoliday($data);
        }
        // Выбрана свадьба (из upBox окна), обновим данные в сессии
        else if (!User::isGuest() && !empty($_GET['wedding']) && mApi::getFunType() == 0)
        {
            $data = UserWeddings::model()->findByAttributes(array('uid' => User::getId(), 'id' => intval($_GET['wedding'])));

            if (!empty($data)) {
                 mApi::setWedding($data);
            }
        }
    }
}
