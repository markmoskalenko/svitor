<?php
class GuestCategoryController extends AppController
{
    public $fun_type = array(
        'wedding' => 'свадьбы',
        'holiday' => 'торжества',
        'multi' => 'для всех',
    );

    public function actionIndex()
    {
        $criteria = new CDbCriteria;
        $criteria->order = 'id DESC';

        $fun_type = Yii::app()->request->getParam('fun_type');

        if (!empty($fun_type) && ($fun_type == 'wedding' || $fun_type == 'holiday')) {
            $criteria->condition = 'fun_type = :fun_type';
            $criteria->params = array(':fun_type' => $fun_type);
        }

        $categories_data = GuestCategories::model()->findAll($criteria);

        $this->render('index', array(
            'categories_data' => $categories_data
        ));
    }

    public function actionAdd()
    {
        $model = new GuestCategories('add');

        if (isset($_POST['GuestCategories']))
        {
            $model->attributes = $_POST['GuestCategories'];

            if ($model->save())
            {
                $params = isset($_GET['fun_type']) ? '?fun_type='.$_GET['fun_type'] : '';
                $this->redirect('/admin_cp/guestCategory'.$params);
            }
        }

        $this->render('_form', array(
            'model' => $model
        ));
    }

    public function actionEdit($id = 0)
    {
        $id = intval($id);
        if (empty($id))
            $this->redirect(Yii::app()->createUrl('admin_cp/guestCategory'));

        $model = GuestCategories::model()->findByPk($id);
        if (empty($model) || $model->protected == 1)
            $this->redirect(Yii::app()->createUrl('admin_cp/guestCategory'));

        if (isset($_POST['GuestCategories']))
        {
            $old_fun_type = $model->fun_type;
            $model->attributes = $_POST['GuestCategories'];

            // Если изменен "Сайт", то перенесем гостей пользоветелей из этой категории в категорию "Другое"
            // т.к.  редактируемая категория может стать не доступной сайту "Свадьбы" если сайт изменен на "Торжества" и наоборот.
            if ($old_fun_type != $model->fun_type)
            {
                $default_cat = GuestCategories::model()->findByAttributes(array('protected' => 1));
                UserGuests::model()->updateAll(array('guest_cat_id' => $default_cat->id), 'guest_cat_id = '.$model->id);
            }

            if ($model->save())
            {
                $params = isset($_GET['fun_type']) ? '?fun_type='.$_GET['fun_type'] : '';
                $this->redirect('/admin_cp/guestCategory'.$params);
            }
        }

        $this->render('_form', array(
            'model' => $model
        ));
    }

    public function actionDelete($id = 0)
    {
        $id = intval($id);
        if (empty($id))
            $this->redirect(Yii::app()->createUrl('admin_cp/guestCategory'));

        $model = GuestCategories::model()->findByPk($id);

        // Изменяем эту категорию у пользователей на "Другое".
        $default_cat = GuestCategories::model()->findByAttributes(array('protected' => 1));
        UserGuests::model()->updateAll(array('guest_cat_id' => $default_cat->id), 'guest_cat_id = '.$model->id);

        if ($model->delete())
            echo 'ok';
    }
}