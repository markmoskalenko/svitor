<?php
class VendorController extends AppController
{
    public $upbox_title = '';

    public function actionIndex($search = '')
    {
        $search = htmlspecialchars($search);

        $criteria = new CDbCriteria;

        if (!empty($search)) {
            $criteria->condition = 'email LIKE "%'.$search.'%" OR title LIKE "%'.$search.'%" OR description LIKE "%'.$search.'%" OR contact_phone LIKE "%'.$search.'%"';
        }

        // Сортировка
        $sort = new CSort('Vendor');
        $sort->attributes = array('*');
        $sort->defaultOrder = 't.last_activity DESC';
        $sort->applyOrder($criteria);

        // Разбиваем на страницы
        $count = Vendor::model()->count($criteria);

        $pages = new CPagination($count);
        $pages->pageSize = 10;
        $pages->applyLimit($criteria);

        $vendors_data = Vendor::model()->findAll($criteria);

        $this->render('index', array(
            'pages' => $pages,
            'sort' => $sort,
            'count' => $count,
            'vendors_data' => $vendors_data,
            'search' => $search,
        ));
    }

    public function actionUpdateRating()
    {
        $vendors = Vendor::model()->findAll();

        if (empty($vendors))
            return;

        foreach ($vendors as $vendor)
        {
            $criteria = new CDbCriteria;
            $criteria->condition = 'date >= UNIX_TIMESTAMP(DATE_SUB(CURRENT_DATE, INTERVAL 1 MONTH)) AND vendor_id = :vendor_id';
            $criteria->params = array(':vendor_id' => $vendor->id);

            $visits_counts = VendorVisits::model()->count($criteria);

            $new_rating = $visits_counts * 100 / 30;

            $vendor['rating'] = $new_rating;
            $vendor->update();
        }

        echo 'ok';
    }

    public function actionUpdateMessage()
    {

        $vendors = Vendor::model()->find('id=:id', array(':id'=>$_GET['id']));

        if (empty($vendors))
            return;

        $vendors['notify_new_msg'] =  $_GET['notify_new_msg'];
        if($vendors->update()){
            echo 'ok';
        }

    }

    public function actionOptions($id = 0)
    {
        $this->layout = 'upbox';

        $id = intval($id);
        $vendor = Vendor::model()->findByPk($id);

        if (empty($vendor))
            mApi::sayError(1, 'Компания не найдена.');

        $this->upbox_title = 'Свойства \ ' . mApi::cutStr($vendor->title, 50);

        $this->render('_options', array(
            'vendor' => $vendor
        ));
    }

    public function actionBan($id = 0)
    {
        $this->layout = 'upbox';

        $id = intval($id);
        $vendor = Vendor::model()->findByPk($id);

        if (empty($vendor))
            mApi::sayError(1, 'Компания не найдена.');

        $this->upbox_title = 'Блокировка аккаунта \ ' . mApi::cutStr($vendor->title, 50);

        if (isset($_POST['Vendor']))
        {
            $vendor->setScenario('ban');
            $vendor->attributes = $_POST['Vendor'];

            if ($vendor->save()) {
                echo 'ok';
            }
        }
        else
        {
            $this->render('_ban', array(
                'vendor' => $vendor
            ));
        }
    }

    public function actionBanStock($id = 0)
    {
        $this->layout = 'upbox';

        $id = intval($id);
        $vendor = Vendor::model()->findByPk($id);

        if (empty($vendor))
            mApi::sayError(1, 'Компания не найдена.');

        $this->upbox_title = 'Блокировка акций \ ' . mApi::cutStr($vendor->title, 50);

        if (isset($_POST['Vendor']))
        {

            $vendor['is_banned_stock'] = $_POST['Vendor']['is_banned_stock'];

            if($_POST['Vendor']['is_banned_stock'] == 0){
                $vendor['stock_stamp'] = '';
            }else{
                $vendor['stock_stamp'] = time();
            }

            if ($vendor->save()) {
                echo 'ok';
            }
        }
        else
        {
            $this->render('_ban_stock', array(
                'vendor' => $vendor
            ));
        }
    }

    public function actionChangePassword($id = 0)
    {
        $this->layout = 'upbox';

        $id = intval($id);
        $vendor = Vendor::model()->findByPk($id);

        if (empty($vendor))
            mApi::sayError(1, 'Компания не найдена.');

        $this->upbox_title = 'Изменить пароль \ ' . mApi::cutStr($vendor->title, 50);

        if (isset($_POST['Vendor']))
        {
            $vendor->setScenario('admin_change_password');
            $vendor->attributes = $_POST['Vendor'];

            if ($vendor->validate()) {
                $vendor->password = $vendor->new_password;

                if ($vendor->save(false))
                    echo 'ok';
            } else {
                echo CHtml::errorSummary($vendor);
            }
        }
        else
        {
            $this->render('_form_change_password', array(
                'vendor' => $vendor
            ));
        }
    }

    public function actionDelete($id = 0)
    {
        $id = intval($id);
        $vendor = Vendor::model()->findByPk($id);

        if (empty($vendor))
            mApi::sayError(1, 'Компания не найдена.');

        $attrs = array('vendor_id' => $vendor->id);

        VendorAddresses::model()->deleteAllByAttributes($attrs);
        VendorAttrValues::model()->deleteAllByAttributes($attrs);
        VendorCategory::model()->deleteAllByAttributes($attrs);
        VendorCertificates::model()->deleteAllByAttributes($attrs);
        VendorDetails::model()->deleteAllByAttributes($attrs);
        VendorFaq::model()->deleteAllByAttributes($attrs);
        VendorImages::model()->deleteAllByAttributes($attrs);
        VendorReviews::model()->deleteAllByAttributes($attrs);
        VendorVideos::model()->deleteAllByAttributes($attrs);
        Post::model()->deleteAllByAttributes(array('author_vid' => $vendor->id));
        Product::model()->deleteAllByAttributes($attrs);

        UserLogVendors::model()->deleteAllByAttributes($attrs);
        UserVendorFavorites::model()->deleteAllByAttributes($attrs);
        UserVendorTeam::model()->deleteAllByAttributes($attrs);

        if ($vendor->delete()) {
            echo 'ok';
        } else {
            echo 'Не удалось удалить компанию.';
        }
    }
}