<?php
class MainController extends AppController
{
    public $defaultAction = 'index';

    public function actionIndex()
    {
        $this->render('index');
    }

    public function actionClearCache(){
        Yii::app()->cache->flush();
        $this->redirect(array('/admin_cp/main/index'));
    }
}
?>