<?php
class MiniatureController extends AppController
{
    public $fun_types = array(
        'wedding' => 'Свадьбы',
        'holiday' => 'Торжества',
        'multi' => 'На всех'
    );

    public function actionIndex()
    {
        $miniatures_data = CategoryMiniatures::model()->findAll(array('order' => 'id DESC'));

        $this->render('index', array(
            'miniatures_data' => $miniatures_data
        ));
    }

    public function actionAdd()
    {
        $miniature = new CategoryMiniatures;

        if (isset($_POST['CategoryMiniatures']))
        {
            $miniature->attributes = $_POST['CategoryMiniatures'];
            $miniature['name'] = mApi::translitString($miniature->title);

            if ($miniature->save())
                $this->redirect('/admin_cp/miniature/');
        }

        $this->render('_form', array(
            'miniature' => $miniature
        ));
    }

    public function actionEdit($id = 0)
    {
        $id = intval($id);
        if (empty($id))
            $this->redirect('/admin_cp/miniature/');

        $miniature = CategoryMiniatures::model()->findByAttributes(array('id' => $id, 'protected' => 0));
        if (empty($miniature))
            $this->redirect('/admin_cp/miniature/');

        if (isset($_POST['CategoryMiniatures']))
        {
            $old_fun_type = $miniature->fun_type;
            $miniature->attributes = $_POST['CategoryMiniatures'];
            $miniature['name'] = mApi::translitString($miniature->title);

            if ($miniature->validate())
            {
                $default_cat = CategoryMiniatures::model()->findByAttributes(array('name' => 'other', 'protected' => 1));
                if (empty($default_cat))
                    $this->redirect('/admin_cp/miniature/');

                // Изменяем категорию на "по умолчанию" для задач у пользователей,
                // если теперь эта категория не показывается для списка задач
                if ($miniature->show_in_checklist == 0)
                    UserChecklist::model()->updateAll(array('cat_min_id' => $default_cat->id), 'cat_min_id = '.$miniature->id);

                // Изменяем категорию на "по умолчанию" для бюджета у пользователей,
                // если теперь эта категория не показывается для бюджета
                if ($miniature->show_in_budget == 0)
                    UserBudget::model()->updateAll(array('cat_min_id' => $default_cat->id), 'cat_min_id = '.$miniature->id);

                // Убираем привязку к этой категории у пользователей, в разделе "Мои компании".
                // Чтобы не потерялась компания - перенесем ее в сохраненные,
                // если теперь эта категория не показывается для бюджета
                if ($miniature->show_in_vendors == 0)
                    UserVendors::model()->updateAll(array('cat_min_id' => 0, 'in_team' => 0, 'in_favorites' => 1), 'cat_min_id = '.$miniature->id);

                // Если изменен сайт и новый параметр не "для всех"...
                if ($old_fun_type != $miniature->fun_type && $miniature->fun_type != 'multi')
                {
                    $fun_type = '';
                    // Если сайт изменен с "multi" на "wedding" или "holiday", то переносим
                    // только те, где сайт отличается от нового
                    if ($miniature->fun_type == 'wedding' || $miniature->fun_type == 'holiday')
                        $fun_type = 'fun_type != "'.$miniature->fun_type.'" AND ';

                    // ...изменяем категорию на "по умолчанию" для шаблонов списка задач
                    ChecklistTemplate::model()->updateAll(array('cat_min_id' => $default_cat->id), $fun_type.' cat_min_id = '.$miniature->id);
                }

                if ($miniature->save(false))
                    $this->redirect('/admin_cp/miniature/');
            }
        }

        $this->render('_form', array(
            'miniature' => $miniature
        ));
    }

    public function actionDelete($id = 0)
    {
        $id = intval($id);
        if (empty($id))
            mApi::sayError(1, 'Не получен параметр id');

        $miniature = CategoryMiniatures::model()->findByAttributes(array('id' => $id));
        if (empty($miniature))
            mApi::sayError(1, 'Невозможно удалить миниатюру');

        $default_cat = CategoryMiniatures::model()->findByAttributes(array('name' => 'other', 'protected' => 1));
        if (empty($default_cat))
            mApi::sayError(1, 'Не найдена категория "other"');

        // Изменяем категорию на "по умолчанию" для шаблонов списка задач
        ChecklistTemplate::model()->updateAll(array('cat_min_id' => $default_cat->id), 'cat_min_id = '.$miniature->id);

        // Изменяем категорию на "по умолчанию" для задач у пользователей
        UserChecklist::model()->updateAll(array('cat_min_id' => $default_cat->id), 'cat_min_id = '.$miniature->id);

        // Изменяем категорию на "по умолчанию" для бюджета у пользователей
        UserBudget::model()->updateAll(array('cat_min_id' => $default_cat->id), 'cat_min_id = '.$miniature->id);

        // Убираем привязку к этой категории у пользователей, в разделе "Мои компании".
        // Чтобы не потерялась компания - перенесем ее в сохраненные
        UserVendors::model()->updateAll(array('cat_min_id' => 0, 'in_team' => 0, 'in_favorites' => 1), 'cat_min_id = '.$miniature->id);

        if (!$miniature->delete())
            mApi::sayError(1, 'Невозможно удалить миниатюру');
    }
}