<?php
class PageController extends AppController
{
    public function actionIndex()
    {
        $site_pages = Pages::model()->findAll();

        $this->render('index', array(
            'site_pages' => $site_pages
        ));
    }

    public function actionAdd()
    {
        $model = new Pages('add');

        if (isset($_POST['Pages']))
        {
            $model->attributes = $_POST['Pages'];

            if ($model->save())
                $this->redirect('/admin_cp/page/');
        }

        $this->render('_form', array(
            'model' => $model
        ));
    }

    public function actionEdit($id = 0)
    {
        $id = intval($id);
        $model = Pages::model()->findByPk($id);

        if (isset($_POST['Pages']))
        {
            $model->attributes = $_POST['Pages'];

            if ($model->save())
                $this->redirect('/admin_cp/page/');
        }

        $this->render('_form', array(
            'model' => $model
        ));
    }

    public function actionDelete($id = 0)
    {
        if (Pages::model()->deleteByPk($id))
            echo 'ok';
    }
}