<?php
class StatisticController extends AppController
{
    public function actionIndex()
    {
        $img_data_count = FileImages::model()->count();
        $img_data_deleted_count = FileImages::model()->count(array('condition' => 'is_deleted = 1'));
        $img_data_deleted_size = Yii::app()->db->createCommand('SELECT SUM(filesize) as data_size FROM file_images WHERE is_deleted = 1')->queryScalar();
        $img_data_deleted_size = mApi::convertFileSize($img_data_deleted_size);

        $img_data_size = Yii::app()->db->createCommand('SELECT SUM(filesize) as data_size FROM file_images')->queryScalar();
        $img_data_size = mApi::convertFileSize($img_data_size);
        $img_last_five = FileImages::model()->findAll(array('limit' => 10, 'order'=> 'id DESC'));

        // Лог поиска - полезная информация
        $post_search_logs = new CActiveDataProvider('LogInfoSearch', array(
            'pagination' => array(
                'pageSize' => 10,
                'pageVar' => 'log_page'
            ),
            'criteria' => array(
                'limit' => 10,
                'order'=> 'date DESC',
                'with' => array('User', 'Category')
            )
        ));

        $this->render('index', array(
            'img_data_count' => $img_data_count,
            'img_data_deleted_count' => $img_data_deleted_count,
            'img_data_deleted_size' => $img_data_deleted_size,

            'img_data_size' => $img_data_size,
            'img_last_five' => $img_last_five,

            'post_search_logs' => $post_search_logs
        ));
    }

    public function actionFlushLogInfoSearch()
    {
        Yii::app()->db->createCommand('TRUNCATE TABLE log_info_search')->execute();
        $this->redirect('/admin_cp/statistic');
    }
}