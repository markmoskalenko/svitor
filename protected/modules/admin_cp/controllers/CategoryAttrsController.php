<?php
class CategoryAttrsController extends AppController
{
    public function actionIndex($cat = 0)
    {
        $cat_id = intval($cat);
        $category = Category::model()->findByPk($cat_id);

        if (empty($category))
            $this->redirect('/admin_cp/category/');


        $attr_products_count = 0;
        $attr_vendors_count = 0;

        $this->render('index', array(
            'category' => $category,
            'attr_products_count' => $attr_products_count,
            'attr_vendors_count' => $attr_vendors_count
        ));
    }

    public function actionList($cat = 0, $belongs_to = "vendors")
    {
        $cat_id = intval($cat);
        $category = Category::model()->findByPk($cat_id);

        if (empty($category))
            $this->redirect('/admin_cp/categoryAttrs');

        if ($belongs_to != 'vendors' && $belongs_to != 'products')
            $belongs_to = 'products';

        // Привязанные характеристики к выбранному разделу
        $category_attrs = $belongs_to == 'products' ? $category->cache(0)->AttrsForProduct : $category->cache(0)->AttrsForVendor;

        // Доступные характеристики для связки с разделом
        $criteria = new CDbCriteria;
        $criteria->condition = 'belongs_to = :belongs_to AND id NOT IN (SELECT attr_id FROM category_attributes WHERE cat_id = :cat_id)';
        $criteria->params = array(
            ':belongs_to' => $belongs_to,
            ':cat_id' => $category->id
        );
        $attrs_data = Attribute::model()->findAll($criteria);

        // Привязка характеристики к разделу
        $model = new CategoryAttributes('add');

        if (isset($_POST['CategoryAttributes']))
        {
            $model->attributes = $_POST['CategoryAttributes'];
            $model->cat_id = $category->id;

            if ($model->attr_id > 0 && $model->save())
                $this->redirect(Yii::app()->createUrl('admin_cp/categoryAttrs/list/', array('cat' => $category->id, 'belongs_to' => $belongs_to)));
        }

        $this->render('attributes', array(
            'belongs_to' => $belongs_to,

            'category' => $category,
            'attrs_data' => $attrs_data,
            'category_attrs' => $category_attrs
        ));
    }

    public function actionUpdateSort($cat_id = 0)
    {
        $cat_id = intval($cat_id);
        if (empty($cat_id))
            Yii::app()->end();

        if (!empty($_POST['element']))
        {
            foreach ($_POST['element'] as $i => $attr_id)
            {
                CategoryAttributes::model()->updateAll(array('sort' => $i), 'cat_id = '.$cat_id.' AND attr_id = '.$attr_id);
            }
            echo 'ok';
        }
    }

    public function actionDelete($cat_id = 0, $id = 0)
    {
        $attr_id = intval($id);
        $cat_id = intval($cat_id);

        $model = CategoryAttributes::model()->findByAttributes(array('cat_id' => $cat_id, 'attr_id' => $attr_id));
        if (empty($model))
            Yii::app()->end();

        // Удаляем выбранные значения для этой характеристики и раздела
        VendorAttrValues::model()->deleteAllByAttributes(array('cat_id' => $cat_id, 'attr_id' => $attr_id));

        if ($model->delete())
            echo 'ok';
    }
}