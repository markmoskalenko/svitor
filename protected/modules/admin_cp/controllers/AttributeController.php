<?php
class AttributeController extends AppController
{
    public $belongs_to_list = array(
        'products' => 'товарам',
        'vendors' => 'компаниям',
        'exhibitions' => 'выставкам',
        'divination' => 'гаданиям'
    );

    public function actionIndex($belongs_to = '')
    {
        if ($belongs_to == 'products' || $belongs_to == 'vendors' || $belongs_to == 'exhibitions' || $belongs_to == 'divination')
        {
            $attributes_data = Attribute::model()->findAllByAttributes(array('belongs_to' => $belongs_to), array('order' => 'id DESC'));
        } else {
            $attributes_data = Attribute::model()->findAll(array('order' => 'id DESC'));
        }

        $this->render('index', array(
            'attributes_data' => $attributes_data,
            'belongs_to' => $belongs_to
        ));
    }

    public function actionAdd()
    {
        $model = new Attribute('add');

        if (isset($_POST['Attribute']))
        {
            $model->attributes = $_POST['Attribute'];
            if ($model->save())
            {
                // Если тип "Логический", то добавляем 2 значения по умолчанию (1 - да, 0 - нет)
                if ($model->type == 'boolean')
                {
                    // создаем 2 boolean значения
                    $value = new AttrValues;
                    $value['attr_id'] = $model->id;
                    $value['value_boolean'] =  1;
                    $value->save();

                    $value = new AttrValues;
                    $value['attr_id'] = $model->id;
                    $value['value_boolean'] = 0;
                    $value->save();
                }

                $url = isset($_GET['belongs_to']) ? '/admin_cp/attribute/?belongs_to='.$_GET['belongs_to'] : '/admin_cp/attribute/';
                $this->redirect($url);
            }
        }

        $this->render('_form', array(
            'model' => $model
        ));
    }

    public function actionEdit($id = 0)
    {
        $id = intval($id);
        if (empty($id))
            $this->redirect('/admin_cp/attribute/');

        $model = Attribute::model()->findByPk($id);
        if (empty($model))
            $this->redirect('/admin_cp/attribute/');

        $model->scenario = 'edit';

        if (isset($_POST['Attribute']))
        {
            // запомним старый тип и is_multi...
            $old_type = $model->type;
            $old_is_multi = $model->is_multi;

            $model->attributes = $_POST['Attribute'];

            // Если тип был изменен, то удаляем все значения
            if ($old_type != $model->type)
            {
                $values = AttrValues::model()->findAllByAttributes(array('attr_id' => $id));
                // Если есть значения удаляем их
                if (!empty($values))
                {
                    // Удаляем все связи с товарами и компаниями
                    ProductAttrValues::model()->deleteAllByAttributes(array('attr_id' => $id));
                    VendorAttrValues::model()->deleteAllByAttributes(array('attr_id' => $id));

                    // А затем и сами значения
                    AttrValues::model()->deleteAllByAttributes(array('attr_id' => $id));
                }

                // Если тип изменен на "Логический", то добавляем 2 значения по умолчанию
                if ($model->type == 'boolean')
                {
                    // создаем 2 boolean значения
                    $value = new AttrValues;
                    $value['attr_id'] = $model->id;
                    $value['value_boolean'] =  1;
                    $value->save();

                    $value = new AttrValues;
                    $value['attr_id'] = $model->id;
                    $value['value_boolean'] = 0;
                    $value->save();
                }
            }

            // Если параметр is_multi был изменен на 0 (нет возможности выбора нескольких значений),
            // то удаляем все выбранные значения для товаров и компаний. Если был изменен на 1, то ничего страшного
            // т.к. знаечние там останется одно, и будет использоваться мульти-вариантом.
            if ($old_is_multi != $model->is_multi && $model->is_multi == 0)
            {
                // Удаляем все связи с товарами и компаниями
                ProductAttrValues::model()->deleteAllByAttributes(array('attr_id' => $id));
                VendorAttrValues::model()->deleteAllByAttributes(array('attr_id' => $id));
            }

            if ($model->save())
            {
                $url = isset($_GET['belongs_to']) ? '/admin_cp/attribute/?belongs_to='.$_GET['belongs_to'] : '/admin_cp/attribute/';
                $this->redirect($url);
            }
        }

        $this->render('_form', array(
            'model' => $model
        ));
    }

    public function actionDelete($id = 0)
    {
        $id = intval($id);
        if (empty($id))
            mApi::sayError(1, 'Не получен параметр id');

        $attribute = Attribute::model()->findByPk($id);
        if (empty($attribute))
            mApi::sayError(1, 'Нет такой характеристики');

        $values = AttrValues::model()->findAllByAttributes(array('attr_id' => $id));
        // Если есть значения удаляем их
        if (!empty($values))
        {
            // Удаляем все связи c товарами и компаниями
            ProductAttrValues::model()->deleteAllByAttributes(array('attr_id' => $id));
            VendorAttrValues::model()->deleteAllByAttributes(array('attr_id' => $id));

            // А затем и сами значения
            AttrValues::model()->deleteAllByAttributes(array('attr_id' => $id));
        }

        // Удаляем все связи с разделами
        CategoryAttributes::model()->deleteAllByAttributes(array('attr_id' => $id));

        // В итоге удаляем саму характеристику
        if ($attribute->delete())
            echo 'ok';
    }

    public function actionValuesUpdateSort($attr = 0)
    {
        $attr_id = intval($attr);
        if (empty($attr_id))
            Yii::app()->end();

        if (!empty($_POST['element']))
        {
            foreach ($_POST['element'] as $i => $id)
            {
                AttrValues::model()->updateAll(array('sort' => $i), 'id = '.$id.' AND attr_id = '.$attr_id);
            }
            echo 'ok';
        }
    }

    public function actionValues($attr = 0)
    {
        $attr_id = intval($attr);
        $attribute = Attribute::model()->findByPk($attr_id);

        // Если нет такого атрибута или он имеет логический display_type, то прощаемся
        if (empty($attribute) || $attribute->type == 'boolean')
            $this->redirect('/admin_cp/attribute/');

        // Добавление нового значения
        $new_value = new AttrValues('add');

        if (isset($_POST['AttrValues']))
        {
            $new_value->attributes = $_POST['AttrValues'];
            $new_value->attr_id = $attribute->id;

            if ($new_value->save())
            {
                $url = '/admin_cp/attribute/values/attr/'+$attr_id;
                $url .= isset($_GET['belongs_to']) ? '/?belongs_to='.$_GET['belongs_to'] : '';
                $this->redirect($url);
            }
        }

        $values = AttrValues::model()->findAllByAttributes(array('attr_id' => $attr_id), array('order' => 'sort ASC'));

        $this->render('values', array(
            'new_value' => $new_value,
            'values' => $values,
            'attribute' => $attribute
        ));
    }

    public function actionEditValue($id = 0)
    {
        $id = intval($id);
        $value = AttrValues::model()->findByPk($id);

        if (empty($value))
            mApi::sayError(1, 'Нет такого значения');

        if (!empty($_GET['value']))
        {
            $value->scenario = 'edit';
            $value['value_string'] = $_GET['value'];
            if ($value->save())
                echo 'ok';
        }
        else
        {
            mApi::sayError(1, 'Значение не может быть пустым');
        }
    }

    public function actionDeleteValue($id = 0)
    {
        $id = intval($id);
        $value = AttrValues::model()->findByPk($id);

        if (empty($value))
            Yii::app()->end();

        ProductAttrValues::model()->deleteAllByAttributes(array('attr_value_id' => $id));
        VendorAttrValues::model()->deleteAllByAttributes(array('attr_value_id' => $id));

        if ($value->delete())
            echo 'ok';
    }
}
