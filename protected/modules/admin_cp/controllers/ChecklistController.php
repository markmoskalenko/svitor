<?php
class ChecklistController extends AppController
{
    public function actionIndex($for = '', $type_id = 0, $cat_id = 0)
    {
        $private_for = '';
        $type_id = intval($type_id);
        $cat_id = intval($cat_id);
        $holiday_types = array();
        $wedding_types = array();
        $categories = array();
        $criteria = new CDbCriteria;

        if ($for == 'wedding' || $for == 'holiday') {
            $private_for = $for;
            $criteria->condition = 'fun_type = :fun_type';
            $criteria->params = array(':fun_type' => $private_for);

            if (!empty($private_for) && !empty($type_id))
            {
                $criteria->condition .= ' AND '.$private_for.'_type_id = :type_id';
                $criteria->params[':type_id'] = $type_id;

                if (!empty($cat_id))
                {
                    $criteria->condition .= ' AND cat_min_id = :cat_id';
                    $criteria->params[':cat_id'] = $cat_id;
                }
            }
        }
        $templates_data = ChecklistTemplate::model()->findAll($criteria);

        if ($private_for == 'holiday')
            $holiday_types = HolidayTypes::model()->findAll();

        if ($private_for == 'wedding')
            $wedding_types = WeddingTypes::model()->findAll();

        if (!empty($private_for) && !empty($type_id))
        {
            $criteria = new CDbCriteria;
            $criteria->condition = '(fun_type = "multi" OR fun_type = "'.$private_for.'")';
            $categories = CategoryMiniatures::model()->findAll($criteria);
        }

        $this->render('index', array(
            'templates_data' => $templates_data,
            'holiday_types' => $holiday_types,
            'wedding_types' => $wedding_types,
            'categories' => $categories,

            'private_for' => $private_for,
            'type_id' => $type_id
        ));
    }

    public function actionAdd($for = '', $type_id = '', $cat_id = '')
    {
        $model = new ChecklistTemplate('add');
        $holiday_types = HolidayTypes::model()->findAll();
        $wedding_types = WeddingTypes::model()->findAll();

        $criteria = new CDbCriteria;
        $criteria->condition = 'show_in_checklist = 1 AND fun_type = "multi" OR fun_type = "holiday"';

        $holiday_groups = CategoryMiniatures::model()->findAll($criteria);

        $criteria->condition = 'show_in_checklist = 1 AND fun_type = "multi" OR fun_type = "wedding"';
        $wedding_groups = CategoryMiniatures::model()->findAll($criteria);

        $search = '';
        if ($for == 'wedding' || $for == 'holiday') {
            $model->fun_type = $for;

            $search = '?for='.$for;

            if (!empty($type_id)) {
                $type_str = $for.'_type_id';
                $model->$type_str = $type_id;

                $search .= '&type_id='.intval($type_id);

                if (!empty($cat_id)) {
                    $cat_str = $for == 'wedding' ? 'wedding_cat_min_id' : 'holiday_cat_min_id';
                    $model->$cat_str = $cat_id;

                    $search .= '&cat_id='.intval($cat_id);
                }
            }
        }

        if (isset($_POST['ChecklistTemplate']))
        {
            $model->attributes = $_POST['ChecklistTemplate'];
            if ($model->fun_type == 'wedding') {
                $model->cat_min_id = $model->wedding_cat_min_id;
            }
            else if ($model->fun_type == 'holiday') {
                $model->cat_min_id = $model->holiday_cat_min_id;
            }

            if ($model->save())
                $this->redirect('/admin_cp/checklist/'.$search);
        }

        $this->render('_form', array(
            'model' => $model,
            'holiday_types' => $holiday_types,
            'wedding_types' => $wedding_types,
            'holiday_groups' => $holiday_groups,
            'wedding_groups' => $wedding_groups,

            'search' => $search
        ));
    }

    public function actionEdit($id = 0)
    {
        $id = intval($id);
        if (empty($id))
            $this->redirect('/admin_cp/checklist/');

        $model = ChecklistTemplate::model()->findByAttributes(array('entry' => $id));
        if (empty($model))
            $this->redirect('/admin_cp/checklist/');

        if ($model->fun_type == 'wedding') {
            $model->wedding_cat_min_id = $model->cat_min_id;
        }
        else if ($model->fun_type == 'holiday') {
            $model->holiday_cat_min_id = $model->cat_min_id;
        }

        $holiday_types = HolidayTypes::model()->findAll();
        $wedding_types = WeddingTypes::model()->findAll();

        $criteria = new CDbCriteria;
        $criteria->condition = 'show_in_checklist = 1 AND fun_type = "multi" OR fun_type = "holiday"';

        $holiday_groups = CategoryMiniatures::model()->findAll($criteria);

        $criteria->condition = 'show_in_checklist = 1 AND fun_type = "multi" OR fun_type = "wedding"';
        $wedding_groups = CategoryMiniatures::model()->findAll($criteria);

        $search = '';
        if (isset($_GET['for'])) {
            $search = '?for='.$_GET['for'];

            if (isset($_GET['type_id']))
                $search .= '&type_id='.intval($_GET['type_id']);

            if (isset($_GET['cat_id']))
                $search .= '&cat_id='.intval($_GET['cat_id']);
        }

        if (isset($_POST['ChecklistTemplate']))
        {
            $model->scenario = 'edit';
            $model->attributes = $_POST['ChecklistTemplate'];

            if ($model->fun_type == 'wedding') {
                $model->cat_min_id = $model->wedding_cat_min_id;
            }
            else if ($model->fun_type == 'holiday') {
                $model->cat_min_id = $model->holiday_cat_min_id;
            }

            if ($model->save())
                $this->redirect('/admin_cp/checklist/'.$search);
        }

        $this->render('_form', array(
            'model' => $model,
            'holiday_types' => $holiday_types,
            'wedding_types' => $wedding_types,
            'holiday_groups' => $holiday_groups,
            'wedding_groups' => $wedding_groups,

            'search' => $search
        ));
    }

    public function actionDelete($id = 0)
    {
        $id = intval($id);
        if (empty($id))
            mApi::sayError(1, 'Не получен параметр id');

        if (ChecklistTemplate::model()->deleteAllByAttributes(array('entry' => $id)))
            echo 'ok';
    }
}