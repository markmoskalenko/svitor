<?php
class CategoryController extends AppController
{
    public function actionIndex()
    {
        $catalog_data = Category::model()->findAll(array('order' => 'weight'));

        $this->render('index', array(
            'catalog_data' => $catalog_data
        ));
    }

    /**
     * Сохраняем позиции категорий
     * Только AJAX
     */
    public function actionSaveSort(){

        if( !Yii::app()->request->isAjaxRequest ) $this->redirect('/admin_cp/category');

        if( Category::model()->saveSort( Yii::app()->request->getPost('element') ) ){

            echo '1';

        }else echo '0';

        Yii::app()->end();

    }

    public function actionAdd()
    {
        $model = new Category('add');
        $categories_data = Category::model()->roots()->findAll();
        $ancestor = array();

        if (isset($_POST['Category']))
        {
            $ancestor_id = intval($_POST['ancestor_id']);
            $model->attributes = $_POST['Category'];

            // Наименование транслитом (для url)
            $model['name'] = strtolower(mApi::translitString($model->title));

            if (!empty($ancestor_id))
                $ancestor = Category::model()->findByPk($ancestor_id);

            // Если указан родитель, к нему
            if (!empty($ancestor))
            {
                if ($model->appendTo($ancestor))
                    $this->redirect('/admin_cp/category/');
            }
            // Родителя нет, сохраняем как root категорию.
            else
            {
                if ($model->saveNode())
                    $this->redirect('/admin_cp/category/');
            }
        }

        $this->render('_form', array(
            'model' => $model,
            'categories_data' => $categories_data,
            'ancestor' => $ancestor
        ));
    }

    public function actionEdit($id = 0)
    {
        $id = intval($id);

        if (empty($id))
            $this->redirect('/admin_cp/category/');

        $model = Category::model()->findByPk($id);
        if (empty($model))
            $this->redirect('/admin_cp/category/');

        $model->scenario = 'edit';

        $categories_data = Category::model()->roots()->findAll();
        $ancestor = $model->ancestors()->find();
        $descandants = $model->children()->findAll();

        // Если есть родитель, то ID, если нет, то 0 (значит раздел isRoot)
        $old_ancestor = !empty($ancestor['id']) ? $ancestor['id'] : 0;

        if (isset($_POST['Category']))
        {
            $new_ancestor = Yii::app()->request->getPost('ancestor_id');
            $model->attributes = $_POST['Category'];

          // if (empty($_POST['Category']['visible']))
             //   $model['visible'] = 0;
				
			if (empty($_POST['Category']['visible_prod']))
                $model['visible_prod'] = 0;
				
			if (empty($_POST['Category']['visible_vndr']))
                $model['visible_vndr'] = 0;

            // Наименование транслитом (для url)
            $model['name'] = strtolower(mApi::translitString($model->title));

            if ($model->validate())
            {
                // Если старый родитель отличается от нового и категория не имеет дочерних категорий
                if ($old_ancestor != $new_ancestor && empty($descandants))
                {
                    // Делаем раздел root (главный)
                    if ($new_ancestor == 0)
                    {
                        $model->moveAsRoot();
                    }
                    // Делаем раздел подкатегорией
                    else if ($new_ancestor > 0)
                    {
                        $new_cat_data = Category::model()->findByPk($new_ancestor);
                        if (!empty($new_cat_data))
                        {
                            $model->moveAsLast($new_cat_data);
                        }
                    }
                }
            }

            if ($model->saveNode())
                $this->redirect('/admin_cp/category/');
        }

        $this->render('_form', array(
            'model' => $model,
            'categories_data' => $categories_data,
            'ancestor' => $ancestor,
            'descandants' => $descandants,
        ));
    }

    public function actionDelete($id = 0)
    {
        $this->layout = 'ajax';
        $id = intval($id);

        if (empty($id))
            Yii::app()->end();

        $category = Category::model()->findByPk($id);
        if (empty($category))
            mApi::sayError(1, 'Такой категории не существует. Удаление невозможно.');

        //TODO: сделать проверку на наличие компаний и статей в разделе

        $descendants = $category->children()->findAll();
        if (!empty($descendants))
            mApi::sayError(1, 'Удаляемая категория содержит подразделы. Удаление невозможно.');

        $products = Product::model()->findByAttributes(array('cat_id' => $category['id']));
        if (!empty($products))
            mApi::sayError(1, 'В этом разделе есть товары. Удаление невозможно.');

        // Удаляем привязанные характеристики
        CategoryAttributes::model()->deleteAllByAttributes(array('cat_id' => $category->id));

        if ($category->deleteNode())
            echo 1;
    }
}
