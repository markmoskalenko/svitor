<?php
class UserController extends AppController
{
    public function actionIndex($search = '')
    {
        $search_text = htmlspecialchars(trim($search));
        $criteria = new CDbCriteria;

        if (!empty($search_text))
        {
            // Простой поиск (login\email)
            $criteria->condition = 'login LIKE "%'.$search_text.'%" OR email LIKE "%'.$search_text.'%"';
        }

        // Сортировка
        $sort = new CSort('User');
        $sort->attributes = array('*');
        $sort->defaultOrder = 'last_activity DESC';
        $sort->applyOrder($criteria);

        // Разбиваем на страницы
        $count = User::model()->count($criteria);

        $pages = new CPagination($count);
        $pages->pageSize = 10;
        $pages->applyLimit($criteria);

        $users_data = User::model()->findAll($criteria);

        $this->render('index', array(
            'pages' => $pages,
            'sort' => $sort,
            'count' => $count,
            'users_data' => $users_data,
            'search' => $search,
        ));
    }

    public function actionLogin($id = 0)
    {
        $id = intval($id);
        if (empty($id))
            $this->render('/admin_cp/user/');

        $user = User::model()->findByAttributes(array('id' => $id));
        if (empty($user))
            $this->render('/admin_cp/user/');

        $identity = new UserIdentity($user->email, $user->password);

        if ($identity->auth(false))
        {
            Yii::app()->user->setState('holiday', false);
            Yii::app()->user->setState('wedding', false);
            Yii::app()->user->login($identity, 0);

            $this->redirect('/dashboard/');
        }
        else
        {
            $this->redirect('/admin_cp/user/');
        }
    }
}