<?php
class PostCategoryController extends AppController
{
    public function actionIndex()
    {
        $criteria = new CDbCriteria;
        $criteria->order = 'id DESC';

        $this->render('index', array(
            'categories_data' => PostCategory::model()->findAll($criteria)
        ));
    }

    public function actionAdd()
    {
        $model = new PostCategory('add');

        if (isset($_POST['PostCategory']))
        {
            $model->attributes = $_POST['PostCategory'];

            if (empty($model->name))
                $model->name = mApi::translitString($model->title);

            if ($model->save())
                $this->redirect('/admin_cp/postCategory');
        }

        $this->render('_form', array(
            'model' => $model
        ));
    }

    public function actionEdit($id = 0)
    {
        $id = intval($id);
        if (empty($id))
            $this->redirect(Yii::app()->createUrl('admin_cp/postCategory'));

        $model = PostCategory::model()->findByPk($id);

        if (empty($model))
            $this->redirect(Yii::app()->createUrl('admin_cp/postCategory'));

        if (isset($_POST['PostCategory']))
        {
            $model->attributes = $_POST['PostCategory'];

            if (empty($model->name))
                $model->name = mApi::translitString($model->title);

            if ($model->save())
                $this->redirect('/admin_cp/postCategory');
        }

        $this->render('_form', array(
            'model' => $model
        ));
    }

    public function actionDelete($id = 0)
    {
        $id = intval($id);
        if (empty($id))
            $this->redirect(Yii::app()->createUrl('admin_cp/postCategory'));

        $model = PostCategory::model()->findByPk($id);

        // Если эта категория используется - изменим её у всех постов на 0.
        Post::model()->updateAll(array('post_cat_id' => 0), 'post_cat_id = ' . $model->id);

        if ($model->delete())
            echo 'ok';
    }
}