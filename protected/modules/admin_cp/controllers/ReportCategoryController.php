<?php
class ReportCategoryController extends AppController
{
    public function actionIndex()
    {
        $criteria = new CDbCriteria;
        $criteria->order = 'id DESC';

        $categories_data = ReportCategories::model()->findAll($criteria);

        $this->render('index', array(
            'categories_data' => $categories_data
        ));
    }

    public function actionAdd()
    {
        $model = new ReportCategories('add');

        if (isset($_POST['ReportCategories']))
        {
            $model->attributes = $_POST['ReportCategories'];

            if (empty($model->name))
                $model->name = mApi::translitString($model->title);

            if ($model->save())
                $this->redirect('/admin_cp/reportCategory');
        }

        $this->render('_form', array(
            'model' => $model
        ));
    }

    public function actionEdit($id = 0)
    {
        $id = intval($id);
        if (empty($id))
            $this->redirect(Yii::app()->createUrl('admin_cp/reportCategory'));

        $model = ReportCategories::model()->findByPk($id);

        if (empty($model))
            $this->redirect(Yii::app()->createUrl('admin_cp/reportCategory'));

        if (isset($_POST['ReportCategories']))
        {
            $model->attributes = $_POST['ReportCategories'];

            if (empty($model->name))
                $model->name = mApi::translitString($model->title);

            if ($model->save())
                $this->redirect('/admin_cp/reportCategory');
        }

        $this->render('_form', array(
            'model' => $model
        ));
    }

    public function actionDelete($id = 0)
    {
        $id = intval($id);
        if (empty($id))
            $this->redirect(Yii::app()->createUrl('admin_cp/reportCategory'));

        $model = ReportCategories::model()->findByPk($id);

        // Если эта категория используется в пользовательских отчетах - изменим её на 0.
        UserReports::model()->updateAll(array('report_cat_id' => 0), 'report_cat_id = ' . $model->id);

        if ($model->delete())
            echo 'ok';
    }
}