<?php
class GalleryCategoryController extends AppController
{
    public function actionIndex()
    {
        $categories = GalleryCategory::model()->findAll(array('order' => 'id DESC'));

        $this->render('index', array(
            'categories' => $categories
        ));
    }

    public function actionAdd()
    {
        $model = new GalleryCategory('add');

        if (!empty($_POST['GalleryCategory']))
        {
            $model->attributes = $_POST['GalleryCategory'];

            if (empty($model->name))
                $model['name'] = mApi::translitString($model->title);

            if ($model->save())
                $this->redirect('/admin_cp/galleryCategory');
        }

        $this->render('_form', array(
            'model' => $model
        ));
    }

    public function actionEdit($id)
    {
        $model = GalleryCategory::model()->findByPk($id);
        if (empty($model))
            $this->redirect(Yii::app()->createUrl('admin_cp/galleryCategory'));

        if (!empty($_POST['GalleryCategory']))
        {
            $model->attributes = $_POST['GalleryCategory'];

            if (empty($model->name))
                $model['name'] = mApi::translitString($model->title);

            if ($model->save())
                $this->redirect('/admin_cp/galleryCategory');
        }

        $this->render('_form', array(
            'model' => $model
        ));
    }

    public function actionDelete($id)
    {
        $model = GalleryCategory::model()->findByPk($id);

        if ($model->delete()) {
            Gallery::model()->updateAll(array('cat_id' => 0), 'cat_id = ' . $model->id);
            echo 'ok';
        }
    }
}