<?php
class GuestMenuTypeController extends AppController
{
    public function actionIndex()
    {
        $criteria = new CDbCriteria;
        $criteria->order = 'sort ASC';

        $menu_types_data = GuestMenuTypes::model()->findAll($criteria);
        $model = new GuestMenuTypes('add');

        if (isset($_POST['GuestMenuTypes']))
        {
            $model->attributes = $_POST['GuestMenuTypes'];
            if ($model->save())
                $this->redirect('/admin_cp/guestMenuType');
        }

        $this->render('index', array(
            'menu_types_data' => $menu_types_data,
            'model' => $model
        ));
    }

    public function actionEditValue($id = 0, $value = '')
    {
        $id = intval($id);
        if (empty($id))
            mApi::sayError(1, 'Не получен параметр id');

        $value = GuestMenuTypes::model()->findByPk($id);
        if (empty($value))
            mApi::sayError(1, 'Нет такого значения');

        if (!empty($_GET['value']))
        {
            $value->scenario = 'edit';
            $value['title'] = $_GET['value'];
            if ($value->save()) {
                echo 'ok';
            } else {
                mApi::sayError(1, $value->getError('title'));
            }
        }
        else
        {
            mApi::sayError(1, 'Значение не может быть пустым');
        }
    }

    public function actionUpdateSort()
    {
        if (!empty($_POST['element']))
        {
            foreach ($_POST['element'] as $i => $id)
            {
                GuestMenuTypes::model()->updateAll(array('sort' => $i), 'id = '.$id);
            }
            echo 'ok';
        }
    }

    public function actionDelete($id = 0)
    {
        $id = intval($id);
        if (empty($id))
            mApi::sayError(1, 'Не получен параметр id');

        // Сбрасываем выбор у пользователей (для этого типа меню)
        UserGuests::model()->updateAll(array('menu_type_id' => 0), 'menu_type_id = '.$id);

        // Удаляем тип меню
        if (GuestMenuTypes::model()->deleteAllByAttributes(array('id' => $id)))
            echo 'ok';
    }
}