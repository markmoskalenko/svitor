<?php
class GalleryPhotoController extends AppController
{
    public function actionIndex($status = 'verification')
    {
        if ($status != 'verification' && $status != 'rejected' && $status != 'published')
            $status = 'verification';

        $photos = new CActiveDataProvider('VendorImages', array(
            'pagination' => array(
                'pageSize' => 54,
                'pageVar' => 'page'
            ),
            'criteria' => array(
                'order' => 'added_date DESC',
                'with' => array(
                    'FileImage' => array('condition' => 'FileImage.is_ready = 1 AND FileImage.is_deleted = 0')
                ),
                'condition' => 't.status_gallery = "' . $status . '"'
            )
        ));

        $this->render('index', array(
            'status' => $status,
            'photos' => $photos->getData(),
            'photos_pagination' => $photos->pagination
        ));
    }

    public function actionCheck($id)
    {
        $photo = VendorImages::model()->findByAttributes(array('img_id' => $id));
        $categories = GalleryCategory::model()->findAll(array('order'=> 'title ASC'));
        $model = Gallery::model()->findByAttributes(array('img_id' => $id));

        if (empty($model))
            $model = new Gallery();

        if (!empty($_POST['Gallery']))
        {
            $model->attributes = $_POST['Gallery'];
            $model['vendor_id'] = $photo->vendor_id;
            $model['img_id'] = $photo->img_id;
            $model['img_filename'] = $photo->img_filename;
            $model['added_date'] = time();

            if ($model->save())
            {
                $photo['status_gallery'] = 'published';
                $photo->update();

                $this->redirect(Yii::app()->createUrl('admin_cp/galleryPhoto/check', array(
                    'id' => self::getRandomNextId()
                )));
            }
        }

        $this->render('_check', array(
            'model' => $model,
            'photo' => $photo,
            'categories' => $categories
        ));
    }

    public function actionReject($id)
    {
        Gallery::model()->deleteAllByAttributes(array('img_id' => $id));

        $photo = VendorImages::model()->findByAttributes(array('img_id' => $id));
        $photo['status_gallery'] = 'rejected';
        $photo->update();

        $this->redirect(Yii::app()->createUrl('admin_cp/galleryPhoto/check', array(
            'id' => self::getRandomNextId()
        )));
    }

    public static function getRandomNextId($status = 'verification')
    {
        $photo = VendorImages::model()->with(array(
                'FileImage' => array(
                    'condition' => 'FileImage.is_ready = 1 AND FileImage.is_deleted = 0')
            )
        )->findByAttributes(array('status_gallery' => $status), array('order' => 'added_date DESC'));

        return $photo->img_id;
    }
}