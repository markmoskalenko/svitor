<h1>Список гостей &rarr; <a href="/admin_cp/guestCategory">Категории</a> &rarr; <?php echo $model->isNewRecord ? 'Новая' : '&laquo;'.$model['title'].'&raquo;'; ?></h1>
<?php echo CHtml::beginForm(); ?>
<?php echo CHtml::errorSummary($model); ?>
<table class="default_table">
    <tr>
        <td>
            <span class="title">Сайт <span class="red_text">*</span></span>
            <?php
            $fun_type = Yii::app()->request->getParam('fun_type');
            if (isset($fun_type) && empty($model->fun_type))
                $model->fun_type = $fun_type;

            echo CHtml::activeRadioButtonList($model, 'fun_type', array(
                'wedding' => 'Свадьба',
                'holiday' => 'Торжества'
            ),
            array(
                'separator' => '&nbsp;&nbsp;&nbsp;&nbsp;'
            ));
            ?>
        </td>
    </tr>
    <tr>
        <td>
            <span class="title">Название <span class="red_text">*</span></span>
            <?php echo CHtml::activeTextField($model, 'title'); ?>
        </td>
    </tr>
</table>

<br><br>
<div class="button_main">
    <button type="submit"><?php echo $model->isNewRecord ? 'Добавить' : 'Сохранить'; ?></button>
</div>
<?php echo CHtml::endForm(); ?>