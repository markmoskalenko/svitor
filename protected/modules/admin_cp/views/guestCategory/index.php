<h1>Список гостей &rarr; Категории</h1>
<div class="list_data_head">
    <div class="button_main">
        <button type="button" onclick="listdata.addNew();">Добавить</button>
    </div>
    или показать
    <?php
    echo CHtml::dropDownList('fun_type', Yii::app()->request->getParam('fun_type'), array(
        '' => '-- все --',
        'wedding' => 'для свадеб',
        'holiday' => 'для торжеств'
    ),
    array(
        'onchange' => 'listdata.applyFilter(this);'
    ));
    ?>
</div>
<div class="list_data scrolled">
    <?php
    if (!empty($categories_data))
    {
        foreach ($categories_data as $cat)
        {
            if ($cat['protected'] == 1) {
                echo '<div class="element" id="element_'.$cat['id'].'">';
            } else {
                echo '<div class="element active" id="element_'.$cat['id'].'" onclick="listdata.editItem(this, '.$cat['id'].');">';
            }
            echo '<nobr>'.$cat['title'].'</nobr>';

            echo ' <span class="info">&rarr; '.$this->fun_type[$cat['fun_type']].'</span>';

            if ($cat['protected'] == 1)
                echo ' <span class="info">(Protected)</span>';

            echo '<div class="edit_links">';
                if ($cat['protected'] != 1)
                    echo '<a onclick="listdata.deleteItem(this, '.$cat['id'].'); return false;">Удалить</a>';
            echo '</div>';
            echo '</div>';
        }
    } else {
        echo '<div class="element empty">Нет ни одной категории...</div>';
    }
    ?>
</div>
<div id="trash-dialog" style="display: none;" title="Удалить категорию">Категория будет удалена без возможности восстановления. <br><br>Вы действительно хотите продолжить?</div>
<script src="/static/admin/js/e_listdata.js"></script>
<script>
    $(document).ready(function() {
        listdata.init({
            addUrl : '/admin_cp/guestCategory/add',
            deleteUrl : '/admin_cp/guestCategory/delete',
            editUrl : '/admin_cp/guestCategory/edit',
        });
    });
</script>