<h1><?php echo $miniature->isNewRecord ? 'Новая миниатюра' : 'Миниатюра &laquo;'.$miniature->title.'&raquo;'; ?></h1>
<?php echo CHtml::beginForm(); ?>
<?php echo CHtml::errorSummary($miniature); ?>
<table class="default_table">
    <tr>
        <td>
            <span class="title">Наименование <span class="red_text">*</span></span>
            <?php echo CHtml::activeTextField($miniature, 'title'); ?>
        </td>
    </tr>
    <tr>
        <td>
            <span class="title">Имя иконки <span class="red_text">*</span></span>
            <?php echo CHtml::activeTextField($miniature, 'icon'); ?> <span class="note">&larr; из директории /static/images/miniatures/</span>
        </td>
    </tr>
    <tr>
        <td>
            <span class="title">Ссылка</span>
            <?php echo CHtml::activeTextField($miniature, 'cat_link', array(
                'style' => 'width: 300px;'
            )); ?> <br><span class="note">(будет использоваться кнопкой "Найти компанию", в разделе "Исполнители" у пользователей. Если ссылки нет, оставьте значение пустым.)</span>
        </td>
    </tr>
    <tr>
        <td>
            <span class="title">Сайт <span class="red_text">*</span></span>
            <?php echo CHtml::activeRadioButtonList($miniature, 'fun_type', $this->fun_types, array(
            'separator' => '&nbsp;&nbsp;',
            'required' => 'required'
        )); ?>
        </td>
    </tr>
    <tr>
        <td>
            <span class="title">Где используется?</span>
            <label><?php echo CHtml::activeCheckbox($miniature, 'show_in_checklist'); ?> Список задач</label>
        </td>
    </tr>
    <tr>
        <td>
            <label><?php echo CHtml::activeCheckbox($miniature, 'show_in_budget'); ?> Бюджет</label>
        </td>
    </tr>
    <tr>
        <td>
            <label><?php echo CHtml::activeCheckbox($miniature, 'show_in_vendors'); ?> Исполнители</label>
        </td>
    </tr>
</table>
<br><br>
<div class="button_main">
    <button type="submit"><?php echo $miniature->isNewRecord ? 'Добавить' : 'Сохранить'; ?></button>
</div>
или <a onclick="window.history.go(-1); return false;">вернуться назад</a>
<?php echo CHtml::endForm(); ?>