<h1>Категории-Миниатюры</h1>
<div class="button_main">
    <button onclick="location.assign('/admin_cp/miniature/add/');">Добавить</button>
</div>
<br><br>
<table class="list_table">
    <thead>
        <th width="40px"></th>
        <th class="al_l"><b>Наименование</b></th>
        <th width="100px"><b>Сайт</b></th>
        <th width="260px" class="al_c"><b>Где показывается?</b></th>
        <th width="180px"></th>
    </thead>
<?php
if (!empty($miniatures_data))
{
    foreach ($miniatures_data as $min)
    {
        $show_in_checklist = $min['show_in_checklist'] == 1 ? 'Список задач | ' : '';
        $show_in_bugdet = $min['show_in_budget'] == 1 ? 'Бюджет | ' : '';
        $show_in_vendors = $min['show_in_vendors'] == 1 ? 'Исполнители' : '';

        echo '<tr id="element_'.$min['id'].'">';
        $icon = $min['icon'] ? $min['icon'] : 'нет иконки';

        echo '<td width="30px"><img src="/static/images/miniatures/'.$icon.'" width="30px"></td>';
        echo '<td><b>'.$min['title'].'</b>';

        if (!empty($min['cat_link']))
            echo '<br>Ссылка: <a href="'.$min['cat_link'].'" target="_blank">'.mApi::cutStr($min['cat_link'], 60).'</a>';

        echo '</td>';

        echo '<td class="al_c">'.$this->fun_types[$min['fun_type']].'</td>';

            echo '<td class="al_c">'.$show_in_checklist.$show_in_bugdet.$show_in_vendors.'</td>';

            if ($min->protected == 0) {
                echo '<td class="al_r">';
                    echo '<div class="edit_links"><a href="/admin_cp/miniature/edit/id/'.$min['id'].'/">Редактировать</a> | <a onclick="action.deleteMiniature(this, '.$min['id'].'); return false;">Удалить</a></div>';
                echo '</td>';
            } else {
                echo '<td class="al_r" style="color: #777;">Protected</td>';
            }
        echo '</tr>';
    }
}
else
{
    echo '<tr><td colspan="3" class="al_c">Нет ни одной миниатюры...</td></tr>';
}
?>
</table>
<div id="trash-miniature-dialog" style="display: none;" title="Удалить миниатюру?">Будут удалены все связи с этой миниатюрой у пользователей которые используют эту миниатюру для создания списка задач, бюджета и команды.<br><br>Вы действительно хотите удалить миниатюру?</div>
<script>
    var action = {
        deleteMiniature : function (link, id) {
            $('#element_'+id).css('background-color', '#eda1a1');
            $('#trash-miniature-dialog').dialog({
                'modal'   : true,
                'draggable' : true,
                'width' : 400,
                'resizable' : false,
                'buttons' : {
                    "Да" : function() {
                        $('#trash-miniature-dialog').dialog("close");
                        $.get('/admin_cp/miniature/delete/id/'+id, null, function (re) {
                            if (!re) {
                                $('#element_'+id).fadeOut(500);
                            } else {
                                alert(re);
                            }
                        });
                    },
                    "Отмена" : function() {
                        $('#trash-miniature-dialog').dialog("close");
                    }
                },
                'close' : function () {
                    $('#element_'+id).css('background-color', '');
                }
            });
            return false;
        }
    };
</script>
