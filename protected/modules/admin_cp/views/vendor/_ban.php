<div class="upbox_content">
    <br>
    Выберите статус для аккаунта...
    <br><br>
    <form onsubmit="return false;">
        <table class="default_table">
            <tr>
                <td>
                    <?php
                    echo CHtml::activeRadioButtonList($vendor, 'is_banned', array(
                        '0' => 'Активен',
                        '1' => 'Заблокирована (не показывается в каталоге)'
                    ), array(
                        'separator' => '<br><br>'
                    ));
                    ?>
                </td>
            </tr>
        </table>
    </form>
</div>
<div class="upbox_buttons">
    <div class="button_main">
        <button onclick="upBox.sendForm(this, '/admin_cp/vendor/ban/id/<?php echo $vendor->id; ?>', $('.upbox_content form').serialize(), false);">Сохранить</button>
    </div>
    <div class="button_grey">
        <button onclick="upBox.ajaxLoad('/admin_cp/vendor/options/id/<?php echo $vendor->id; ?>');">Отменить</button>
    </div>
</div>
