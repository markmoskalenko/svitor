<h1>Компании</h1>
<div class="button_main r_fl">
    <button onclick="vendor.updateRating(this);">Пересчитать рейтинг</button>
</div>

<div class="b-search">
    <?php echo CHtml::beginForm('/admin_cp/vendor/index/', 'GET'); ?>
    <input type="text" name="search" value="<?php echo $search; ?>" placeholder="email, название или слово для поиска в описании...">
    <div class="button_main">
        <button onclick="submit">Найти</button>
    </div>
    <?php echo CHtml::endForm(); ?>
</div>


<?php echo !empty($search) ? 'По вашему запросу найдено: '.$count.' компаний.' : 'На данный момент зарегистрированно: '.$count.' компаний.'; ?>

<div class="r_fl paginator">
    <?php
    $this->widget('CLinkPager', array(
        'pages' => $pages,
        'htmlOptions' => array('class' => 'b-default_pager'),
        'firstPageLabel' => '',
        'lastPageLabel' => '',
        'prevPageLabel' => '&larr; Назад',
        'nextPageLabel' => 'Далее &rarr;',
        'header' => ''
    ));
    ?>
</div>
<br><br>
<table class="list_table">
    <thead>
    <tr>
        <th width="30px"></th>
        <th width="220px" class="al_l"><?php echo $sort->link('title', 'Название'); ?></th>
        <th width="180px" class="al_l"><?php echo $sort->link('email', 'E-Mail'); ?></th>
        <th class="al_c"><?php echo $sort->link('last_activity', 'Посл. активность'); ?></th>
        <th class="al_c"><?php echo $sort->link('reg_date', 'Дата рег.'); ?></th>
        <th class="al_c"><?php echo $sort->link('rating', 'Рейтинг'); ?></th>
        <th class="al_c"><?php echo $sort->link('is_banned', 'Блок.'); ?></th>
        <th class="al_c"><?php echo $sort->link('notify_new_msg', 'Сообщения'); ?></th>
        <th width="100px" class="al_l last_r"></th>
    </tr>
    </thead>
    <?php
    if (!empty($vendors_data))
    {
        foreach ($vendors_data as $vendor)
        {
            echo '<tr id="vendor_' . $vendor->id . '">';
            echo '<td class="al_c"><a href="' . Yii::app()->createUrl('vendor/profile', array('id' => $vendor['id'])) . '/" target="_blank"><img src="' . Yii::app()->ImgManager->getUrlById($vendor->img_id, 'small', $vendor->img_filename) . '" width="30px"></a></td>';

            echo '<td class="al_l"><a href="' . Yii::app()->createUrl('vendor/profile', array('id' => $vendor['id'])) . '/" target="_blank"><b>' . mApi::cutStr(CHtml::decode($vendor['title']), 40) . '</b></a></td>';

            echo '<td class="al_l"><span title="' . $vendor['email'] . '">' . mApi::cutStr($vendor['email'], 25) . '</span></td>';

            echo '<td class="al_c"><span title="вход: ' . mApi::HumanDatePrecise($vendor['last_login']) . '">' . mApi::HumanDatePrecise($vendor['last_activity']) . '</span></td>';

            echo '<td class="al_c">' . mApi::HumanDatePrecise($vendor['reg_date']) . '</td>';

            echo '<td class="al_c">';
            echo round($vendor->rating, 2);
            echo '</td>';

            echo '<td class="al_c">';
            echo $vendor->is_banned == 0 ? 'Нет' : '<b>Есть</b>';
            echo '</td>';

            echo '<td class="al_c">';
            echo $vendor->notify_new_msg == 1 ? '<input sel="' . $vendor->id . '" class="message" type="checkbox" checked>' : '<input sel="' . $vendor->id . '" class="message" type="checkbox">';
            echo '</td>';

            echo '<td class="al_c">';
            echo '<div class="button_main">';
            echo '<button onclick="upBox.ajaxLoad(\'/admin_cp/vendor/options/id/' . $vendor['id'] . '\');">Свойства</button>';
            echo '</div> ';

            /*echo '<div class="button_main" style="margin-left: 5px;">';
            echo '<button onclick="window.open(\'http://' . Yii::app()->params['vendor_cp_domain'] . '/user/login/id/' . $vendor['id'] . '\');">Войти</button>';
            echo '</div>';*/

            echo '</td>';

            echo '</tr>';
        }
    }
    else
    {
        echo '<tr><td colspan="7" class="al_c">Ничего не найдено...</td></tr>';
    }
    ?>
</table>
<br>
<div class="r_fl paginator">
    <?php
    $this->widget('CLinkPager', array(
        'pages' => $pages,
        'htmlOptions' => array('class' => 'b-default_pager'),
        'firstPageLabel' => '',
        'lastPageLabel' => '',
        'prevPageLabel' => '&larr; Назад',
        'nextPageLabel' => 'Далее &rarr;',
        'header' => ''
    ));
    ?>
</div>

<div class="rating_updater" style="display: none;"><b>Производится пересчет рейтинга компаний. Не закрывайте и не сворачивайте эту страницу. По завершению пересчета, страница будет автоматически перезагружена.</b></div>

<div id="trash-dialog" style="display: none;" title="Удалить?">Компания будет уничтожена, без возможности восстановления. <br><br>Вы действительно хотите удалить эту компанию?</div>

<script>
    var vendor = {
        delete : function (trigger, id) {
            $.fancybox.close();
            $('#vendor_'+id).css('background-color', '#ffd38e');
            $('#trash-dialog').dialog({
                'modal' : true,
                'width' : 400,
                'z-index' : 9999,
                'resizable' : false,
                'buttons' : {
                    "Да" : function() {
                        $(trigger).hide();
                        $.fancybox.showLoading();
                        $.get('/admin_cp/vendor/delete/', 'id='+id, function(re) {
                            if (re == 'ok') {
                                $('#vendor_'+id).fadeOut(300);
                            } else {
                                alert(re);
                                $(trigger).show();
                            }
                            $.fancybox.hideLoading();
                        });
                        $('#trash-dialog').dialog("close");
                    },
                    "Отмена" : function() {
                        $('#trash-dialog').dialog("close");
                    }
                },
                'close' : function () {
                    $('#vendor_'+id).css('background-color', '');
                }
            });
            return false;
        },
        updateRating : function (trigger) {
            $('.paginator').hide();
            $('.list_table').hide();
            $('.rating_updater').show();

            var old_button_content = $(trigger).html();
            $(trigger).html($.ajaxLoader()).attr('disabled', true);

            $.get('/admin_cp/vendor/updateRating/', function (re) {
                if (re == 'ok') {
                    location.reload();
                } else {
                    alert('Произошла неизвестная ошибка при пересчете рейтинга. Попробуйте еще раз...');
                    $(trigger).html(old_button_content).attr('disabled', false);
                    $('.paginator').show();
                    $('.list_table').show();
                    $('.rating_updater').hide();
                }
            });
        }
    };

    $(".message").change(function(){

        var notify_new_msg = $(this).attr("checked");
        var id = $(this).attr("sel");
        if(notify_new_msg == undefined){
            notify_new_msg = 0;
        }else if(notify_new_msg == "checked"){
            notify_new_msg = 1;
        }

        $.get('/admin_cp/vendor/updateMessage/id/'+id+'/notify_new_msg/'+notify_new_msg, function (re) {
            if (re == 'ok') {} else {}
        });

    });
</script>