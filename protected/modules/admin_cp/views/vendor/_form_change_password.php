<div class="upbox_content">
    <div id="msg"></div>
    <form onsubmit="return false;">
    <table class="default_table">
        <tr>
            <td>
                <span class="title">Новый пароль</span>
                <?php echo CHtml::activePasswordField($vendor, 'new_password', array('style' => 'width: 350px;')); ?>
            </td>
        </tr>
        <tr>
            <td><span class="title">Новый пароль еще раз</span>
                <?php echo CHtml::activePasswordField($vendor, 'confirm_new_password', array('style' => 'width: 350px;')); ?>
            </td>
        </tr>
    </table>
    </form>
</div>
<div class="upbox_buttons">
    <div class="button_main">
        <button onclick="upBox.sendForm(this, '/admin_cp/vendor/changePassword/id/<?php echo $vendor->id; ?>', $('.upbox_content form').serialize(), false);">Сохранить</button>
    </div>
    <div class="button_grey">
        <button onclick="upBox.ajaxLoad('/admin_cp/vendor/options/id/<?php echo $vendor->id; ?>');">Отменить</button>
    </div>
</div>
