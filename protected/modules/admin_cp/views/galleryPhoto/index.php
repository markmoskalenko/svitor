<h1>Галерея &rarr; Фотографии</h1>
<div class="gallery-filter">
    <select onchange="location.assign('<?= Yii::app()->createUrl('admin_cp/galleryPhoto/index'); ?>/status/'+ this.value);">
        <option value="verification"<?= ($status == 'verification') ? ' selected' : ''; ?>>На проверку</option>
        <option value="rejected"<?= ($status == 'rejected') ? ' selected' : ''; ?>>Отклоненные</option>
        <option value="published"<?= ($status == 'published') ? ' selected' : ''; ?>>Опубликованные</option>
    </select>
</div>
<div class="gallery-photos">
    <? $imageManager = new ImageManager; ?>
    <? if ($photos): ?>
        <? foreach ($photos as $photo): ?>
            <a href="<?= Yii::app()->createUrl('admin_cp/galleryPhoto/check', array('id' => $photo->img_id)); ?>"><img src="<?= Yii::app()->ImgManager->getUrlById($photo->img_id, 'medium', $photo->img_filename); ?>"></a>
        <? endforeach; ?>
    <? else: ?>
        Нет фотографий для проверки...
    <? endif; ?>
</div>
<?
$this->widget('CLinkPager', array(
    'pages' => $photos_pagination,
    'htmlOptions' => array('class' => 'b-default_pager'),
    'firstPageLabel' => '',
    'lastPageLabel' => '',
    'prevPageLabel' => '&larr; Назад',
    'nextPageLabel' => 'Далее &rarr;',
    'header' => '',
    'cssFile' => false
));
?>