<h1>Галерея &rarr; <a href="/admin_cp/galleryPhoto">Фотографии</a> &rarr; Проверка фотографии</h1>
<?= CHtml::beginForm(); ?>
<?= CHtml::errorSummary($model); ?>
<table class="default_table">
    <tr>
        <td>
            <b>Загружено:</b> компанией «<a href="<?= Yii::app()->createUrl('vendor/profile', array('id' => $photo->vendor_id)); ?>" target="_blank"><?= $photo['Vendor']['title']; ?></a>»
        </td>
    </tr>
    <tr>
        <td class="gallery-img-wrap">
            <img src="<?= Yii::app()->ImgManager->getUrlById($photo->img_id, 'large', $photo->img_filename); ?>" style="max-width: 100%;">
        </td>
    </tr>
    <tr>
        <td>
            <span class="title">Категория</span>
            <?=
            CHtml::activeDropDownList($model, 'cat_id', CHtml::listData($categories, 'id', 'title'), array(
                'empty' => '-- не выбрано --'
            ));
            ?>
        </td>
    </tr>
    <tr>
        <td>
            <span class="title">Описание</span>
            <?=
            CHtml::activeTextArea($model, 'description', array(
                'style' => 'width: 500px; height: 40px;'
            ));
            ?>
        </td>
    </tr>
</table>

<br><br>
<div class="button_main">
    <button type="submit"><?= $model->isNewRecord ? 'Добавить в галерею' : 'Сохранить изменения'; ?> и показать следующую &rarr;</button>
</div>
<div class="button_grey r_fl">
    <button type="button" onclick="location.assign('<?= Yii::app()->createUrl('admin_cp/galleryPhoto/reject', array('id' => $photo->img_id)); ?>');"><?= $model->isNewRecord ? 'Не добавлять в галерею' : 'Убрать из галереи'; ?> и показать следующую &rarr;</button>
</div>
<?= CHtml::endForm(); ?>