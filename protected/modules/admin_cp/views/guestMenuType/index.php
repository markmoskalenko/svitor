<h1>Список гостей &rarr; Виды меню</h1>

<div class="list_data_head">
    <?php echo CHtml::beginForm(); ?>
    <?php echo CHtml::errorSummary($model); ?>

    <?php
    echo CHtml::activeTextField($model, 'title', array(
        'placeholder' => 'Введите наименование...',
        'style' => 'width: 250px;'
    ));
    ?>
    <div class="button_main">
        <button type="submit">Добавить</button>
    </div>
    <?php echo CHtml::endForm(); ?>

    <div id="guestmenu_sort_actionbar" style="display: none; padding-top: 15px;">
        Вы изменили порядок элементов&nbsp;
        <div class="button_main save_button">
            <button onclick="listdata_sortable.saveData(this);">Сохранить</button>
        </div>
        <div class="button_grey reset_button">
            <button onclick="location.reload();">Отменить</button>
        </div>
    </div>
</div>
<div class="list_data scrolled" id="guestmenu_data">
    <?php
    if (!empty($menu_types_data))
    {
        foreach ($menu_types_data as $mtype)
        {
            echo '<div class="element active" id="element_'.$mtype['id'].'" onclick="listdata.editValue(this, '.$mtype['id'].');">';
            echo '<span class="sortable_trigger"></span>';

            echo '<nobr>'.$mtype['title'].'</nobr>';

            echo '<div class="edit_links">';
                echo '<a onclick="listdata.deleteItem(this, '.$mtype['id'].'); return false;">Удалить</a>';
            echo '</div>';
            echo '</div>';
        }
    } else {
        echo '<div class="element empty">Нет ни одной записи...</div>';
    }
    ?>
</div>
<div id="trash-dialog" style="display: none;" title="Удалить запись">Запись будет удалена без возможности восстановления. <br><br>Вы действительно хотите продолжить?</div>
<script src="/static/admin/js/e_listdata.js"></script>
<script src="/static/admin/js/e_listdata_sortable.js"></script>
<script>
$(document).ready(function() {
    listdata.init({
        deleteUrl : '/admin_cp/guestMenuType/delete',
        editValueUrl : '/admin_cp/guestMenuType/editValue'
    });

    listdata_sortable.init({
        listdata : $('#guestmenu_data'),
        actionbar : $('#guestmenu_sort_actionbar'),
        saveUrl : '/admin_cp/guestMenuType/updateSort'
    });
});
</script>