<h1>Обмен опытом &rarr; <a href="/admin_cp/postCategory">Категории</a> &rarr; <?php echo $model->isNewRecord ? 'Новая' : '&laquo;'.$model['title'].'&raquo;'; ?></h1>
<?php echo CHtml::beginForm(); ?>
<?php echo CHtml::errorSummary($model); ?>
<table class="default_table">
    <tr>
        <td>
            <span class="title">Название <span class="red_text">*</span></span>
            <?php echo CHtml::activeTextField($model, 'title'); ?>
        </td>
    </tr>
    <tr>
        <td>
            <span class="title">Name</span>
            <?php echo CHtml::activeTextField($model, 'name'); ?> &larr; для URL (можно оставить пустым - будет сгенерировано автоматически)
        </td>
    </tr>
</table>

<br><br>
<div class="button_main">
    <button type="submit"><?php echo $model->isNewRecord ? 'Добавить' : 'Сохранить'; ?></button>
</div>
<?php echo CHtml::endForm(); ?>