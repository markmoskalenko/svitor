<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>ADMIN: Svitor</title>
    <link rel="stylesheet" href="/static/admin/styles.css">
    <script src="/static/js/jquery-1.8.0.min.js"></script>
    <script src="/static/js/jquery.placeholder.js"></script>
    <script src="/static/js/main.js"></script>

    <script src="/static/js/m_tooltip.js"></script>
    <link rel="stylesheet" href="/static/m_tooltip.css">

    <link rel="stylesheet" href="/static/fancybox/jquery.fancybox.css">
    <script src="/static/fancybox/jquery.fancybox.pack.js"></script>

    <link rel="stylesheet" href="/static/humanity/jquery-ui-1.8.23.custom.css">
    <script src="/static/js/jquery-ui-1.8.23.custom.min.js"></script>
</head>
<body>
<div class="container">
    <div class="head_userbar">
        <b><?php echo Yii::app()->user->name; ?></b> (<a href="/user/logout/" class="logout">выход</a>)
        <br>
        <a href="/" class="links">Перейти на сайт &rarr;</a>
        <div style="margin-top: 5px;"><a href="/mod_cp/" class="links">Модерация &rarr;</a></div>
    </div>
    <div class="head" onclick="location.assign('/admin_cp/');">
        Система <span>*-*</span> управления
    </div>
    <div class="head_menu">
        <ul>
            <li class="first"><a href="/admin_cp/statistic/">Статистика</a></li>
            <li><a href="/admin_cp/user/">Пользователи</a></li>
            <li><a href="/admin_cp/vendor/">Компании</a></li>
            <li class="main"><a class="main arrow-down" onclick="return false;">Каталог</a>
                <div>
                    <ul>
                        <li><a href="/admin_cp/category/">Категории</a></li>
                        <li><a href="/admin_cp/attribute/">Характеристики</a></li>
                    </ul>
                </div>
            </li>
            <li class="main"><a class="main arrow-down" onclick="return false;">Галерея</a>
                <div>
                    <ul>
                        <li><a href="/admin_cp/galleryCategory/">Категории</a></li>
                        <li><a href="/admin_cp/galleryPhoto/">Фотографии</a></li>
                    </ul>
                </div>
            </li>
            <li class="main"><a class="main arrow-down" onclick="return false;">Обмен опытом</a>
                <div>
                    <ul>
                        <li><a href="/admin_cp/postCategory/">Категории</a></li>
                    </ul>
                </div>
            </li>
            <li class="main"><a class="main arrow-down" onclick="return false;">Список гостей</a>
                <div>
                    <ul>
                        <li><a href="/admin_cp/guestCategory/">Категории</a></li>
                        <li><a href="/admin_cp/guestMenuType/">Виды меню</a></li>
                    </ul>
                </div>
            </li>
            <li class="main"><a class="main arrow-down" onclick="return false;">Разное</a>
                <div>
                    <ul>
                        <li><a href="/admin_cp/checklist/">Список задач (шаблоны)</a></li>
                        <li><a href="/admin_cp/miniature/">Категории-миниатюры</a></li>
                        <li><a href="/admin_cp/page/">Страницы сайта</a></li>
                    </ul>
                </div>
            </li>
            <li class="main last"><a href="/admin_cp/main/clearCache/">Очистить кэш</a></li>
        </ul>
    </div>
    <div class="content">
        <?php echo $content; ?>
    </div>
    <div class="footer">&copy; Created by Dmitry Marinin</div>
</div>
</body>
</html>