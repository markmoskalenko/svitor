<h1>Статистика</h1>
<div>
    <br>
    <h2>Обмен опытом: поисковые запросы</h2>
    <p>
        Всего <b><?php echo number_format($post_search_logs->totalItemCount); ?></b> <?php echo mApi::plularStr($post_search_logs->totalItemCount, 'запрос', 'запроса', 'запросов'); ?>
        <?php if ($post_search_logs->totalItemCount > 0): ?>
            <a href="/admin_cp/statistic/flushLogInfoSearch">(Очистить лог)</a>
        <?php endif; ?>
        <?php if ($post_search_logs->totalItemCount > 100000) echo ' <span style="color: red"><b>&larr; Рекомендуется очистить лог!</b></span>'; ?>
    </p>
    <?php $post_search_log_data = $post_search_logs->getData(); ?>
    <?php if (!empty($post_search_log_data)): ?>
    В таблице показаны последние 10 запросов.
    <table class="stats_data">
        <thead>
        <th class="l_al">Запрос</th>
        <th class="l_al">Категория</th>
        <th class="l_al">Пользователь\IP</th>
        <th class="l_al">Дата</th>
        </thead>
        <?php foreach ($post_search_log_data as $var): ?>
        <tr>
            <td class="l_al" width="250px">
                <a href="http://<?php echo Yii::app()->request->getServerName() . '/post/?q=' . urlencode($var->query_text); ?>" target="_blank"><?php echo mApi::cutStr(CHtml::decode($var->query_text), 50); ?></a>
            </td>
            <td class="l_al" width="100px">
                <?php
                if (!empty($var['Category'])) {
                    echo '<a href="http://' . Yii::app()->request->getServerName() . '/post/' . $var['Category']['name'] . '" target="_blank">' . $var['Category']['title'] . '</a>';
                } else {
                    echo '--';
                }
                ?>
            </td>
            <td class="l_al" width="130px">
                <?php
                if (!empty($var['User'])) {
                    echo '<a href="/admin_cp/user/index/?search=' . $var['User']['login'] . '">' . $var['User']['login'] . '</a>';
                } else {
                    echo long2ip($var->ip_address);
                }
                ?>
            </td>
            <td class="l_al" width="200px">
                <?php echo mApi::getDateWithMonth($var->date).' в '.date('H:i:s', $var->date); ?>
            </td>
        </tr>
        <?php endforeach; ?>
    </table>
    <br>
    <div class="r_fl">
        <?php
        $this->widget('CLinkPager', array(
            'pages' => $post_search_logs->pagination,
            'htmlOptions' => array('class' => 'b-default_pager'),
            'firstPageLabel' => '',
            'lastPageLabel' => '',
            'prevPageLabel' => '&larr; Назад',
            'nextPageLabel' => 'Далее &rarr;',
            'header' => ''
        ));
        ?>
    </div>
    <?php endif; ?>
</div>
    <br>
<div>
    <h2>Изображения</h2>
    <p>Сейчас на сервере: <b><?php echo number_format($img_data_count); ?></b>
    <?php echo mApi::plularStr($img_data_count, 'картинка', 'картинки', 'картинок'); ?> (объем ~<b><?php echo $img_data_size; ?></b>)</p>

    <p>Из них ожидает удаления: <b><?php echo number_format($img_data_deleted_count); ?></b>
    <?php echo mApi::plularStr($img_data_deleted_count, 'картинка', 'картинки', 'картинок'); ?> (обьем ~<b><?php echo $img_data_deleted_size; ?></b>)</p>

    <?php if (!empty($img_last_five)): ?>
    В таблице показано 10 последних картинок.
    <table class="stats_data">
        <thead>
            <th></th>
            <th class="l_al">Картинка</th>
            <th class="l_al">Размер файла</th>
            <th class="l_al">Дата</th>
        </thead>
        <?php foreach ($img_last_five as $img): ?>
        <?php $url = Yii::app()->ImgManager->getUrlById($img->id, 'large', $img->filename); ?>
            <tr>
                <td width="20px"><a href="<?php echo $url; ?>" target="_blank"><img src="<?php echo Yii::app()->ImgManager->getUrlById($img->id, 'small', $img->filename); ?>" width="20px"></a></td>
                <td class="l_al"><a href="<?php echo $url; ?>" target="_blank"><?php echo $img->filename; ?></a></td>
                <td class="l_al"><?php echo mApi::convertFileSize($img->filesize);?></td>
                <td class="l_al"><?php echo mApi::getDateWithMonth($img->date).' в '.date('H:i', $img->date); ?></td>
            </tr>
        <?php endforeach; ?>
    </table>
    <?php endif; ?>
</div>