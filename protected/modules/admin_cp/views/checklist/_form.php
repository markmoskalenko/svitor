<h1><?php echo $model->isNewRecord ? 'Новый шаблон' : 'Редактирование шаблона'; ?></h1>
<?php echo CHtml::beginForm(); ?>
<?php echo CHtml::errorSummary($model); ?>
<?php
    $wedding_type_id = '';
    $holiday_type_id = '';

    $for = Yii::app()->request->getParam('for');
    $type_id = Yii::app()->request->getParam('type_id');

    if ($for == 'wedding') {
        $wedding_type_id = $type_id;
    }
    else if ($for == 'holiday') {
        $holiday_type_id = $type_id;
    }

    $fun_types = array(
        'wedding' => 'Свадьбы',
        'holiday' => 'Торжества'
    );
?>
    <table class="default_table">
        <tr>
            <td><span class="title">Сайт <span class="red_text">*</span></span></td>
            <td>
                <?php echo CHtml::activeRadioButtonList($model, 'fun_type', $fun_types, array(
                    'onchange' => 'checklist.selectFunType(this);',
                    'separator' => '&nbsp;&nbsp;',
                    'required' => 'required'
                )); ?>
            </td>
        </tr>
        <tr id="holiday_types"<?php echo empty($model->holiday_type_id) ? ' style="display: none;"' : ''; ?>>
            <td><span class="title">Тип торжества <span class="red_text">*</span></span></td>
            <td>
                <?php
                $types = CHtml::listData($holiday_types, 'id', 'title');
                echo CHtml::activeDropDownList($model, 'holiday_type_id', $types, array(
                    'empty' => '-- не выбрано --'
                ));
                ?>
            </td>
        </tr>
        <tr id="wedding_types"<?php echo empty($model->wedding_type_id) ? ' style="display: none;"' : ''; ?>>
            <td><span class="title">Тип свадьбы <span class="red_text">*</span></span></td>
            <td>
                <?php
                $types = CHtml::listData($wedding_types, 'id', 'title');
                echo CHtml::activeDropDownList($model, 'wedding_type_id', $types, array(
                    'empty' => '-- не выбрано --'
                ));
                ?>
            </td>
        </tr>
        <tr id="groups_selectors"<?php echo empty($model->fun_type) ? ' style="display: none;"' : ''; ?>>
            <td><span class="title">Категория <span class="red_text">*</span></span></td>
            <td>
                <div id="holiday_groups"<?php echo $model->fun_type == 'wedding' ? ' style="display: none;"' : ''; ?>>
                    <?php echo CHtml::activeDropDownList($model, 'holiday_cat_min_id', CHtml::listData($holiday_groups, 'id', 'title'), array(
                        'empty' => '-- не выбрано --'
                    )); ?>
                </div>
                <div id="wedding_groups"<?php echo $model->fun_type == 'holiday' ? ' style="display: none;"' : ''; ?>>
                    <?php echo CHtml::activeDropDownList($model, 'wedding_cat_min_id', CHtml::listData($wedding_groups, 'id', 'title'), array(
                        'empty' => '-- не выбрано --'
                    )); ?>
                </div>
            </td>
        </tr>
        <tr>
            <td class="t_va">
                <span class="title">Ссылка</span>
            </td>
            <td>
                <?php
                echo CHtml::activeTextField($model, 'link', array(
                    'style' => 'width: 250px;'
                ));
                ?><br>
                <span class="note">(если указана ссылка, то текст задачи будет обернут в нее при выводе в таблице у пользователя)</span>
            </td>
        </tr>
        <tr>
            <td class="t_va">
                <span class="title">Текст задачи <span class="red_text">*</span></span>
            </td>
            <td>
                <?php
                $model->task = CHtml::decode($model->task);
                echo CHtml::activeTextarea($model, 'task', array(
                    'style' => 'width: 600px; height: 90px;'
                ));
                ?>
            </td>
        </tr>
        <tr>
            <td class="t_va">
                <span class="title">Наименование для бюджета</span>
            </td>
            <td>
                <?php
                echo CHtml::activeTextField($model, 'budget_title', array(
                    'style' => 'width: 450px;'
                ));
                ?><br>
                <span class="note">(если заполнено это поле, то будет установлена связь с бюджетом, <br>т.е. у пользователя будет также создана строка в бюджете и она будет связана с этой задачей)</span>
            </td>
        </tr>
    </table>

    <br><br>
    <div class="button_main">
        <button type="submit"><?php echo $model->isNewRecord ? 'Добавить' : 'Сохранить'; ?></button>
    </div>
    или <a onclick="window.history.go(-1); return false;">вернуться назад</a>
<?php echo CHtml::endForm(); ?>

<script src="/static/admin/js/e_checklist.js"></script>
<script>
$(document).ready(function () {
    checklist.init({
        fun_type : '<?php echo $model->fun_type; ?>'
    });
});
</script>