<h1>Шаблоны списка задач</h1>
<div class="list_data_head">
    <div class="button_main">
        <button onclick="listdata.addNew();">Добавить</button>
    </div>
    или показать
    <?php
        echo CHtml::dropDownList('for', $private_for, array(
            'wedding' => 'для свадеб',
            'holiday' => 'для торжеств'
        ), array(
            'empty' => '-- все --',
            'onchange' => 'listdata.applyFilter(this, 1);'
        ));

        $active_type = Yii::app()->request->getParam('type_id');
        if (!empty($holiday_types) && !empty($private_for)) {
            $types = CHtml::listData($holiday_types, 'id', 'title');
            echo ' с типом: '.CHtml::dropDownList('type_id', $active_type, $types, array(
                'empty' => '-- все --',
                'onchange' => 'listdata.applyFilter(this, 2);'
            ));
        }
        else if (!empty($wedding_types) && !empty($private_for)) {
            $types = CHtml::listData($wedding_types, 'id', 'title');
            echo ' с типом: '.CHtml::dropDownList('type_id', $active_type, $types, array(
                'empty' => '-- все --',
                'onchange' => 'listdata.applyFilter(this, 2);'
            ));
        }

        $active_option = Yii::app()->request->getParam('cat_id');
        if (!empty($type_id) && (!empty($wedding_types) || !empty($holiday_types))) {
            $categories = CHtml::listData($categories, 'id', 'title');
            echo ' в категории: '.CHtml::dropDownList('cat_id', $active_option, $categories, array(
                'empty' => '-- все --',
                'onchange' => 'listdata.applyFilter(this, 3);'
            ));
        }
    ?>
</div>
<div class="list_data scrolled">
    <?php
    if (!empty($templates_data))
    {
        foreach ($templates_data as $template)
        {
            echo '<div class="element active" id="element_'.$template['entry'].'" onclick="listdata.editItem(this, '.$template['entry'].');">';
            //echo '<span class="sortable_trigger"></span>';

            echo '<nobr><b>'.mApi::cutStr($template['task'], 100).'</b></nobr>';

            if (!empty($private_for) && empty($type_id))
            {
                echo ' <span class="info">&larr; '.$types[$template[$private_for.'_type_id']].'</span>';
            }
            else if (empty($private_for) && !empty($template['wedding_type_id']))
            {
                echo ' <span class="info">&larr; Свадьбы</span>';
            }
            else if (empty($private_for) && !empty($template['holiday_type_id']))
            {
                echo ' <span class="info">&larr; Торжества</span>';
            }

            if (!empty($type_id) && isset($categories[$template['cat_min_id']])) {
                echo ' <span class="info">&larr; '.$categories[$template['cat_min_id']].'</span>';
            }

            echo ' <span class="info">';
            if (!empty($template['link']))
                echo ' (есть ссылка)';

            if (!empty($template['budget_title']))
                echo ' (связь с бюджетом)';

            echo '</span>';

            echo '<div class="edit_links">';
            echo '<a onclick="listdata.deleteItem(this, '.$template['entry'].'); return false;">Удалить</a>';
            echo '</div>';
            echo '</div>';
        }
    } else {
        echo '<div class="element empty">Нет шаблонов для этого типа...</div>';
    }
    ?>
</div>
<div id="trash-dialog" style="display: none;" title="Удалить шаблон">Шаблон будет удален, и более не будет использоваться для заполнения списка задач по умолчанию у пользователей.<br><br>Вы действительно хотите продолжить?</div>
<script>
/*$(document).ready(function() {
    $('.list_data').sortable({
        scroll : false,
        opacity : 0.6
    });
    $('.list_data').sortable('option', 'handle', 'span[class^=sortable_trigger]');
}); */
</script>
<script src="/static/admin/js/e_listdata.js"></script>
<script>
    $(document).ready(function () {
        listdata.init({
            addUrl : '/admin_cp/checklist/add/',
            editUrl : '/admin_cp/checklist/edit',
            deleteUrl : '/admin_cp/checklist/delete'
        });
    });
</script>