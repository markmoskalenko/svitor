<h1><a href="/admin_cp/page/">Страницы сайта</a> &rarr; <?php echo $model->isNewRecord ? 'новая' : '&laquo;'.$model->name.'&raquo;'; ?></h1>
<?php echo CHtml::beginForm(); ?>
<?php echo CHtml::errorSummary($model); ?>
    <table class="default_table">
        <tr>
            <td>
                <span class="title">Имя <span class="red_text">*</span></span>
                http://<?php echo Yii::app()->request->getServerName(); ?>/page/ <?php echo CHtml::activeTextField($model, 'name'); ?> <span class="note">(например: about, contacts, terms)</span>
            </td>
        </tr>
        <tr>
            <td>
                <span class="title">Ключевые слова (SEO)</span>
                <?php echo CHtml::activeTextField($model, 'keywords', array('style' => 'width: 550px;')); ?> <span class="note">&larr; через запятую</span>
            </td>
        </tr>
        <tr>
            <td>
                <span class="title">Краткое описание (SEO)</span>
                <?php echo CHtml::activeTextArea($model, 'description', array('style' => 'height: 60px; width: 550px;')); ?>
            </td>
        </tr>
        <tr>
            <td>
                <span class="title">Заголовок H1<span class="red_text">*</span></span>
                <?php echo CHtml::activeTextField($model, 'title', array('style' => 'width: 550px;')); ?>
            </td>
        </tr>
        <tr>
            <td>
                <span class="title">Заголовок<span class="red_text">*</span></span>
                <?php echo CHtml::activeTextField($model, 'title_h1', array('style' => 'width: 550px;')); ?>
            </td>
        </tr>
        <tr>
            <td>
                <span class="title">Содержимое (Text, HTML, JavaScript)</span>
                <?php echo CHtml::activeTextArea($model, 'content', array('style' => 'height: 170px; width: 550px;')); ?>
            </td>
        </tr>
        <tr>
            <td>
                <label><?php echo CHtml::activeCheckbox($model, 'visible'); ?> Страница доступна по URL</label>
            </td>
        </tr>
    </table>

    <br><br>
    <div class="button_main">
        <button type="submit"><?php echo $model->isNewRecord ? 'Добавить' : 'Сохранить'; ?></button>
    </div>
    или <a href="/admin_cp/page/">вернуться к списку</a>
<?php echo CHtml::endForm(); ?>