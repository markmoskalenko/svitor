<h1>Страницы сайта</h1>
<div class="list_data_head">

    <div class="button_main">
        <button onclick="listdata.addNew();">Добавить</button>
    </div>

</div>
<div class="list_data scrolled" id="guestmenu_data">
    <?php
    if (!empty($site_pages))
    {
        foreach ($site_pages as $page)
        {
            echo '<div class="element active" id="element_'.$page['id'].'" onclick="listdata.editItem(this, '.$page['id'].');">';

            echo '<nobr><b>'.$page['name'].'</b> &rarr; '.mApi::cutStr($page['title'], 100).'</nobr>';

            echo '<div class="edit_links">';
            echo '<a href="http://' . Yii::app()->request->getServerName() . '/page/' . $page['name'] . '" onclick="listdata.data.goEditEvent = false;" onmouseout="listdata.data.goEditEvent = true;" target="_blank">&uarr; Открыть на сайте</a> | ';
            echo '<a onclick="listdata.deleteItem(this, '.$page['id'].'); return false;">Удалить</a>';
            echo '</div>';
            echo '</div>';
        }
    } else {
        echo '<div class="element empty">Нет ни одной страницы...</div>';
    }
    ?>
</div>
<div id="trash-dialog" style="display: none;" title="Удалить страницу">Страница будет удалена без возможности восстановления и не будет доступна на сайте. <br><br>Вы действительно хотите продолжить?</div>
<script src="/static/admin/js/e_listdata.js"></script>
<script>
    $(document).ready(function() {
        listdata.init({
            addUrl : '/admin_cp/page/add',
            editUrl : '/admin_cp/page/edit',
            deleteUrl : '/admin_cp/page/delete'
        });
    });
</script>