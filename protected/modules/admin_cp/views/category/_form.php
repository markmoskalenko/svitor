<h1><a href="/admin_cp/category/">Каталог</a> &rarr; <?php echo $model->isNewRecord ? 'новая категория' : 'категория &laquo;'.$model['title'].'&raquo;'; ?></h1>
<?php echo CHtml::beginForm(); ?>
<?php echo CHtml::errorSummary($model); ?>
    <table class="default_table">
        <?php
        // Если редактируется категория, у которой имеются дочерние разделы, то убираем из формы "Родитель" и "Сайты".
        if (empty($descandants)): ?>
            <tr>
                <td class="ancestor_id"><div class="title">Родитель</div>
                    <select name="ancestor_id" onchange="catalog.selectAncestor(this);">
                        <option value="0">-- нет родителя --</option>
                        <?php
                        foreach ($categories_data as $cat)
                        {
                            $active_attribute = '';
                            if (isset($ancestor['id']) && $ancestor['id'] == $cat['id'])
                                $active_attribute = ' selected="selected"';

                            if ($model['id'] != $cat['id'])
                                echo '<option value="'.$cat['id'].'"'.$active_attribute.'>'.$cat['title'].'</option>';
                        }
                        ?>
                    </select>
                </td>
            </tr>
        <?php endif; ?>
        <tr>
            <td>
                <span class="title">Название <span class="red_text">*</span></span>
                <?php echo CHtml::activeTextField($model, 'title'); ?>
            </td>
        </tr>
        <tr>
            <td><span class="title">Имя иконки</span>
                <?php echo CHtml::activeTextField($model, 'icon'); ?> <span class="note">&larr; из директории /static/images/categories/</span>
            </td>
        </tr>
         <!--<tr>
            <td><label><?php echo CHtml::activeCheckbox($model, 'visible'); ?> Показывать в каталоге</label></td>
        </tr>-->
		<tr>
            <td><label><?php echo CHtml::activeCheckbox($model, 'visible_vndr'); ?> Показывать в компаниях</label></td>
        </tr>
		<tr>
            <td><label><?php echo CHtml::activeCheckbox($model, 'visible_prod'); ?> Показывать в товарах</label></td>
        </tr>
<!--        S E O        -->
        <tr class="catalog_seo"><td><h2>Компании</h2></td></tr>
        <tr class="catalog_seo">
            <td><span class="title">Заголовок</span>
                <?php echo CHtml::activeTextField($model, 'title_catalog'); ?>
            </td>
        </tr>
        <tr class="catalog_seo">
            <td><span class="title">Ключевые слова</span>
                <?php echo CHtml::activeTextField($model, 'keywords_catalog'); ?>
            </td>
        </tr>
        <tr class="catalog_seo">
            <td><span class="title">Описание</span>
                <?php echo CHtml::activeTextArea($model, 'description_catalog', array('style'=>'width: 500px;height: 40px;')); ?>
            </td>
        </tr>
        <tr class="catalog_seo">
            <td><span class="title">Текст 1</span>
                <?php echo CHtml::activeTextArea($model, 'text_header_catalog', array('style'=>'width: 500px;height: 40px;')); ?>
            </td>
        </tr>
        <tr class="catalog_seo">
            <td><span class="title">Текст 2</span>
                <?php echo CHtml::activeTextArea($model, 'text_footer_catalog', array('style'=>'width: 500px;height: 40px;')); ?>
            </td>
        </tr>
        <tr class="product_seo"><td><h2>Товары</h2></td></tr>
        <tr class="product_seo">
            <td><span class="title">Заголовок</span>
                <?php echo CHtml::activeTextField($model, 'title_product'); ?>
            </td>
        </tr>
        <tr class="product_seo">
            <td><span class="title">Ключевые слова</span>
                <?php echo CHtml::activeTextField($model, 'keywords_product'); ?>
            </td>
        </tr>
        <tr class="product_seo">
            <td><span class="title">Описание</span>
                <?php echo CHtml::activeTextArea($model, 'description_product', array('style'=>'width: 500px;height: 40px;')); ?>
            </td>
        </tr>
        <tr class="product_seo">
            <td><span class="title">Текст 1</span>
                <?php echo CHtml::activeTextArea($model, 'text_header_product', array('style'=>'width: 500px;height: 40px;')); ?>
            </td>
        </tr>
        <tr class="product_seo">
            <td><span class="title">Текст 2</span>
                <?php echo CHtml::activeTextArea($model, 'text_footer_product', array('style'=>'width: 500px;height: 40px;')); ?>
            </td>
        </tr>
    </table>

    <br><br>
    <div class="button_main">
        <button type="submit"><?php echo $model->isNewRecord ? 'Добавить категорию' : 'Сохранить'; ?></button>
    </div>
    или <a onclick="window.history.go(-1); return false;">вернуться назад</a>
<?php echo CHtml::endForm(); ?>

<script src="/static/ckeditor/ckeditor.js"></script>
<script src="/static/ckeditor/adapters/jquery.js"></script>

<script src="/static/admin/js/e_catalog.js"></script>

<script type="text/javascript">
    $(function(){
        $('#Category_visible_vndr').change(function(){
            if(this.checked)
                $('.catalog_seo').show(200);
            else
                $('.catalog_seo').hide(150);

        });
        $('#Category_visible_prod').change(function(){
            if(this.checked)
                $('.product_seo').show(200);
            else
                $('.product_seo').hide(150);

        });

        if( ! $('#Category_visible_vndr').is(':checked') ){

            $('.catalog_seo').hide(0);

        }

        if( ! $('#Category_visible_prod').is(':checked') ){

            $('.product_seo').hide(0);

        }
    });

</script>