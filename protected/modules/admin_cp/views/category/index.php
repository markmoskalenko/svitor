<h1>Каталог</h1>
<table class="category_editor">
<tr>
<td class="left">
	<div class="filter">
        <div class="button_main" style="float:left;">
            <button type="button" onclick="location.assign('/admin_cp/category/add/');">Добавить</button>
        </div>
        <div id="loader" style="float:left;padding: 7px 0px 0px 15px; "></div>
        <div style="clear: both"></div>
    </div>
    <div id="remove_errors"></div>
    <div id="list" class="cats_list">
        <?php
        if (count($catalog_data) >= 1)
        {
            $chars = array(
                0 => '&larr; скрыт',
                1 => '',
            );

            foreach($catalog_data as $n => $category)
            {
                // Если главный раздел
                if ($category->lft == 1)
                {
                    echo '<div class="root" id="element_'.$category->id.'" >';
                    echo '<span class="sortable_trigger trigger"></span>';

                    echo '<b onclick="catalog.editCat('.$category['id'].');">'.$category->title.'</b>';
                    echo '<b class="cat_info">'.$chars[$category['visible']].'</b>';

                    echo '<div class="edit_links" id="edit_'.$category->id.'">';

                    // Показываем только у категорий, содержащих товары\компании
                    if ($category->isRoot() && $category->isLeaf())
                    {
                        echo '<a href="/admin_cp/categoryAttrs/index/cat/'.$category->id.'/">Редактировать характеристики</a> | ';
                        echo '<a onclick="catalog.removeCat('.$category['id'].', this); return false;">Удалить</a>';
                    }

                    echo '</div>';


                    foreach ($catalog_data as $s => $sub_cat)
                    {
                        if ($sub_cat->isRoot() || $sub_cat->root != $category->root)
                            continue;

                        echo '<div class="children element active" id="element_'.$sub_cat->id.'" onclick="catalog.editCat('.$sub_cat['id'].');">';
                        echo '<span class="sortable_trigger trigger2"></span>';

                        echo $sub_cat->title;
                        echo ' <b class="cat_info">'.$chars[$sub_cat['visible']].'</b>';

                        echo '<div class="edit_links" id="edit_'.$sub_cat->id.'">';

                        echo '<a href="/admin_cp/categoryAttrs/index/cat/'.$sub_cat->id.'/">Редактировать характеристики</a> | ';

                        echo '<a onclick="catalog.removeCat('.$sub_cat['id'].', this); return false;">Удалить</a></div>';
                        echo '</div>';
                    }
                    echo '</div>';
                }
            }
        }
        else
        {
            echo 'Нет категорий...';
        }
        ?>
    </div>
</td>
</table>
<div id="trash-dialog" style="display: none;" title="Удалить?">Вы действительно хотите удалить эту категорию?</div>
<script src="/static/admin/js/e_catalog.js"></script>
<script>
$(document).ready(function(){
  //  catalog.init();
});
</script>

<script>
    $(document).ready(function() {
        $('.root').sortable({
            scroll : false,
            opacity : 0.6,
            handle : '.trigger2',
            update : function (event, ui) {
                $('#loader').html($.ajaxLoader());
                var data = $(this).sortable('serialize');
                $.post('/admin_cp/category/saveSort', data, function (re) {
                    if( re == '1' ){
                        $('#loader').html('');
                    }else{
                        $('#loader').html('');
                    }
                });
                console.log($(this).sortable('serialize'))
            }

        });

        $('#list').sortable({
            scroll : false,
            opacity : 0.6,
            handle : '.trigger',
            update : function (event, ui) {
                $('#loader').html($.ajaxLoader());
                var data = $(this).sortable('serialize');
                $.post('/admin_cp/category/saveSort', data, function (re) {
                    if( re == '1' ){
                        $('#loader').html('');
                    }else{
                        $('#loader').html('');
                    }
                });
                console.log($(this).sortable('serialize'))
            }

        });
    });
</script>
