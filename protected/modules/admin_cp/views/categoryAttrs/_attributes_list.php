<?php
$count = 0;

foreach ($data as $attr)
{
    $count++;
    echo '<div class="element" id="element_'.$attr['id'].'">';

    echo '<span class="sortable_trigger"></span>';

    echo '<nobr>'.$attr['title'].'</nobr> ';

    if ($attr['type'] == 'boolean') {
        echo '<span class="info">&larr; <b>'.$attr->types_list[$attr['type']].'</b><span>';
    } else {
        echo '<span class="info">&larr; '.$attr->types_list[$attr['type']].'<span>';
    }

    echo '<div class="edit_links">';
        echo '<a href="/admin_cp/attribute/edit/id/'.$attr['id'].'/">Редактировать</a>';
        echo ' | ';

    if ($attr['type'] != 'boolean')
        echo '<a href="/admin_cp/attribute/values/attr/'.$attr['id'].'/">Редактировать значения</a> | ';

    echo '<a onclick="listdata.deleteItem(this, '.$attr['id'].'); return false;">Открепить</a>';
    echo '</div>';
    echo '</div>';
}

if ($count == 0)
    echo '<div class="element empty">К разделу не привязано ни одной характеристики...</div>';

?>