<h1>
    <a href="<?php echo Yii::app()->createUrl('admin_cp/category'); ?>">Каталог</a>
    &rarr; &laquo;<a href="<?php echo Yii::app()->createUrl('admin_cp/category/edit', array('id' => $category->id)); ?>"><?php echo $category->title; ?></a>&raquo;
    &rarr; <a href="<?php echo Yii::app()->createUrl('admin_cp/categoryAttrs/index', array('cat' => $category->id)); ?>">Характеристики</a>
    &rarr; Для <?php echo $belongs_to == 'products' ? 'товаров' : 'компаний'; ?>
</h1>

<div class="list_data_head">
    <?php echo CHtml::beginForm(); ?>
    <?php
    $data = !empty($attrs_data) ? CHtml::listData($attrs_data, 'id', 'title') : array();
    echo CHtml::dropDownList('CategoryAttributes[attr_id]', '', $data, array(
        'empty' => '-- выберите характеристику --',
        'style' => 'width: 300px'
    ));
    ?>
    <div class="button_main">
        <button type="submit">Прикрепить</button>
    </div>
    <?php echo CHtml::endForm(); ?>

    <div id="<?php echo $belongs_to; ?>_sort_actionbar" style="display: none; padding-top: 15px;">
        Вы изменили порядок элементов&nbsp;
        <div class="button_main save_button">
            <button onclick="listdata_sortable.saveData(this);">Сохранить</button>
        </div>
        <div class="button_grey reset_button">
            <button onclick="location.reload();">Отменить</button>
        </div>
    </div>
</div>
<div class="list_data scrolled" id="<?php echo $belongs_to; ?>_data">
    <?php
    if (!empty($category_attrs))
        $this->renderPartial('_attributes_list', array('data' => $category_attrs));
    ?>
</div>

<div id="trash-dialog" style="display: none;" title="Открепить характеристику">Характеристика будет откреплена от этого раздела. Все выбранные значения для этой характеристики у компаний и товаров будут не доступны. <br><br>Вы действительно хотите продолжить?</div>
<script src="/static/admin/js/e_listdata.js"></script>
<script src="/static/admin/js/e_listdata_sortable.js"></script>
<script>
$(document).ready(function () {
    listdata.init({
        deleteUrl : '/admin_cp/categoryAttrs/delete/cat_id/<?php echo $category->id; ?>'
    });

    listdata_sortable.init({
        listdata : $('#<?php echo $belongs_to; ?>_data'),
        actionbar : $('#<?php echo $belongs_to; ?>_sort_actionbar'),
        saveUrl : '/admin_cp/categoryAttrs/updateSort/cat_id/<?php echo $category->id; ?>'
    });
});
</script>
