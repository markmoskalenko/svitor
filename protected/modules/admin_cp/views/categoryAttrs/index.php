<h1>
    <a href="<?php echo Yii::app()->createUrl('admin_cp/category'); ?>">Каталог</a>
    &rarr; &laquo;<a href="<?php echo Yii::app()->createUrl('admin_cp/category/edit', array('id' => $category->id)); ?>"><?php echo $category->title; ?></a>&raquo;
    &rarr; Характеристики
</h1>
Какие характеристики вы хотите редактировать?
<ul class="default nav">
    <li>
        <a href="<?php echo Yii::app()->createUrl('admin_cp/categoryAttrs/list/', array('cat' => $category->id, 'belongs_to' => 'products')); ?>">Для товаров</a>
    </li>
    <li>
        <a href="<?php echo Yii::app()->createUrl('admin_cp/categoryAttrs/list/', array('cat' => $category->id, 'belongs_to' => 'vendors')); ?>">Для компаний</a>
    </li>
</ul>