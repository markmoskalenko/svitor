<h1><?php echo $model->isNewRecord ? 'Новая характеристика' : 'Редактирование характеристики &laquo;<a href="/admin_cp/attribute/edit/id/'.$model->id.'">'.$model->title.'</a>&raquo;'; ?></h1>
<?php echo CHtml::beginForm(); ?>
<?php echo CHtml::errorSummary($model); ?>
<table class="attributes_editor">
    <tr>
        <td class="left">
            <table class="default_table">
                <tr>
                    <td><span class="title">Название <span class="red_text">*</span></span>
                        <?php echo CHtml::activeTextField($model, 'title'); ?>
                    </td>
                </tr>

                <tr>
                    <td><label><?php echo CHtml::activeCheckbox($model, 'required'); ?> Обязательна к заполнению</label></td>
                </tr>

                <tr>
                    <td>
                        <span class="title">Тип <span class="red_text">*</span></span>
                        <?php
                        echo CHtml::activeDropDownList($model, 'type', $model->types_list, array(
                            'empty' => '',
                            'onchange' => 'attribute.selectType(this);'
                        ));
                        ?>
                    </td>
                </tr>

                <tr id="is_multi"<?php echo $model->type == 'boolean' || $model->type == '' ? ' style="display:none;"' : ''; ?>>
                    <td>
                        <label><?php echo CHtml::activeCheckbox($model, 'is_multi'); ?> Может быть несколько значений</label>
                    </td>
                </tr>

                <tr>
                    <td>
                        <?php if ($model->isNewRecord):

                                if ($model->isNewRecord && isset($_GET['belongs_to']))
                                    $model->belongs_to = $_GET['belongs_to'] ? 'vendors' : 'products';
                        ?>
                            <span class="title">Относится к...</span>
                            <?php echo CHtml::activeDropDownList($model, 'belongs_to', $this->belongs_to_list); ?>
                        <?php else: ?>
                            <b>Относится к</b> <?php echo $this->belongs_to_list[$model->belongs_to]; ?>
                        <?php endif; ?>
                    </td>
                </tr>

                <tr>
                    <td colspan="2"><br>
                        <div class="button_main">
                            <button type="submit"><?php echo $model->isNewRecord ? 'Добавить' : 'Сохранить'; ?></button>
                        </div>
                        или <a onclick="window.history.go(-1); return false;">вернуться назад</a>
                    </td>
                </tr>
            </table>
        </td>
        <td class="right">
            <div class="help_title">Типы</div>
            <?php if (!$model->isNewRecord): ?>
                <div style="padding: 0 10px 10px 10px;">
                    <span class="red_text">При изменении типа, будут удалены все значения!</span>
                    <br>Например: если раньше у характеристики был тип "строковый", то при изменении типа на "логический" будут удалены все добавленные значения,
                    следовательно у товаров и компаний они также будут удалены.
                </div>
            <?php endif; ?>
                <b>Логический</b>
                <ul class="help">
                    <li>Значения: "Да", "Нет" (нельзя добавлять другие значения)</li>
                    <li>При фильтрации в каталоге можно использовать только значение "Да"</li>
                </ul>
                <b>Строковый</b>
                <ul class="help">
                    <li>Значения: добавляются отдельно</li>
                    <li>При фильтрации в каталоге, можно выбрать только 1 значение.</li>
                </ul>
                <!-- <b>Число</b>
                <ul class="help">
                    <li>Значения: добавляются отдельно</li>
                    <li>При фильтрации в каталоге, можно использовать интервал числовых значений (от ... до ...).</li>
                    <li>В дальнейшем нельзя будет изменить параметр "Относится к..."</li>
                </ul>  -->

            <div id="is_multi_help" <?php echo $model->type == 'boolean' || $model->type == '' ? ' style="display:none;"' : ''; ?>><br>
                <div class="help_title">Может быть несколько значений</div>
                <div style="padding: 0 10px 10px 10px;">
                    При фильтрации в каталоге, добавлении товара или компании можно выбрать нескольно значений для этой характеристики. При изменении этого параметра могут быть удалены все связи в этой характеристикой у товаров и компаний.
                </div>
            </div>

            <?php if ($model->isNewRecord): ?>
                <br>
                <div class="help_title">Относится к...</div>
                <div style="padding: 0 10px 10px 10px;">
                    Этот параметр в дальнейшем НЕЛЬЗЯ будет изменить.
                </div>
            <?php endif; ?>
        </td>
    </tr>
</table>

<?php echo CHtml::endForm(); ?>
<script src="/static/admin/js/e_attribute.js"></script>