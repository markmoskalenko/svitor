<h1>Характеристики</h1>
<div class="list_data_head">
    <div class="button_main">
        <button onclick="listdata.addNew();">Добавить</button>
    </div>
    или показать
    <?php
    echo CHtml::dropDownList('belongs_to', $belongs_to, array(
        '' => '-- все --',
        'products' => 'для товаров',
        'vendors' => 'для компаний',
        'exhibitions' => 'для выставок',
        'divination' => 'для гаданий'
    ), array(
        'onchange' => 'listdata.applyFilter(this);'
    ));
    ?>
</div>
<div class="list_data scrolled">
    <?php
    if (!empty($attributes_data))
    {
        foreach ($attributes_data as $attr)
        {
            echo '<div class="element active" id="element_'.$attr['id'].'" onclick="listdata.editItem(this, '.$attr['id'].');">';
                echo '<nobr><b>'.$attr['title'].'</b></nobr> ';

                if ($attr['required'] == 1)
                    echo '<span class="red_text">*</span>';

                echo ' <span class="info">&larr; '.$attr->types_list[$attr['type']].'</span>';

                echo '<div class="edit_links">';
                    if ($attr->type != 'boolean')
                        echo '<a href="/admin_cp/attribute/values/attr/'.$attr['id'].'">Редактировать значения</a> | ';

                    echo '<a onclick="listdata.deleteItem(this, '.$attr['id'].'); return false;">Удалить</a>';
                echo '</div>';
            echo '</div>';
        }
    } else {
        echo '<div class="element empty">Нет ни одной характеристики...</div>';
    }
    ?>
</div>
<div id="trash-dialog" style="display: none;" title="ВНИМАНИЕ !!!">Характеристика будет удалена у всех разделов каталога, у компаний и товаров, а также все ее значения будут удалены без возможности восстановления. <br><br>Вы действительно хотите продолжить?</div>
<script src="/static/admin/js/e_listdata.js"></script>
<script>
$(document).ready(function () {
    listdata.init({
        addUrl : '/admin_cp/attribute/add/',
        editUrl : '/admin_cp/attribute/edit',
        deleteUrl : '/admin_cp/attribute/delete'
    });
});
</script>