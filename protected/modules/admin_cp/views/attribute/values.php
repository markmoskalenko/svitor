<h1>
    <a href="<?php echo Yii::app()->createUrl('admin_cp/attribute'); ?>">Характеристики</a>
    &rarr; &laquo;<a href="<?php echo Yii::app()->createUrl('admin_cp/attribute/edit', array('id' => $attribute->id)); ?>"><?php echo $attribute->title; ?></a>&raquo;
    &rarr; Значения
</h1>

<div class="list_data_head">
    <?php echo CHtml::beginForm(); ?>
    <?php echo CHtml::errorSummary($new_value); ?>
        <?php
        echo CHtml::activeTextField($new_value, 'value_string', array(
            'placeholder' => 'Введите наименование...',
            'style' => 'width: 250px;'
        ));
        ?>

        <div class="button_main">
            <button type="submit">Добавить</button>
        </div>
    <?php echo CHtml::endForm(); ?>

    <div id="values_sort_actionbar" style="display: none; padding-top: 15px;">
        Вы изменили порядок элементов&nbsp;
        <div class="button_main save_button">
            <button onclick="listdata_sortable.saveData(this);">Сохранить</button>
        </div>
        <div class="button_grey reset_button">
            <button onclick="location.reload();">Отменить</button>
        </div>
    </div>
</div>
<div class="list_data scrolled" id="values_data">
    <?php
    if (!empty($values))
    {
        foreach ($values as $val)
        {
            echo '<div class="element active" id="element_'.$val['id'].'" onclick="listdata.editValue(this, '.$val['id'].');">';
            echo '<span class="sortable_trigger"></span>';
            echo '<nobr>'.$val['value_string'].'</nobr>';
            echo '<div class="edit_links"><a onclick="listdata.deleteItem(this, '.$val['id'].'); return false;">Удалить</a></div>';
            echo '</div>';
        }
    }
    else
    {
        echo '<div class="element">Пока нет ни одного значения...</div>';
    }
    ?>
</div>
<div id="trash-dialog" style="display: none;" title="ВНИМАНИЕ !!!">Все возможные связи с этим значением будут уничтожены без возможности восстановления! Если вы хотите изменить значение, просто кликните по его названию. <br><br>Вы действительно хотите удалить это значение?</div>
<script src="/static/admin/js/e_listdata.js"></script>
<script src="/static/admin/js/e_listdata_sortable.js"></script>
<script>
$(document).ready(function() {
    listdata.init({
        deleteUrl : '/admin_cp/attribute/deleteValue/',
        editValueUrl : '/admin_cp/attribute/editValue/'
    });

    listdata_sortable.init({
        listdata : $('#values_data'),
        actionbar : $('#values_sort_actionbar'),
        saveUrl : '/admin_cp/attribute/ValuesUpdateSort/attr/<?php echo $attribute->id; ?>'
    });
});
</script>

