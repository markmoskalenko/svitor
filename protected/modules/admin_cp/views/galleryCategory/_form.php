<h1>Галерея &rarr; <a href="/admin_cp/galleryCategory">Категории</a> &rarr; <?= $model->isNewRecord ? 'Новая' : '&laquo;' . $model['title'] . '&raquo;'; ?></h1>
<?= CHtml::beginForm(); ?>
<?= CHtml::errorSummary($model); ?>
<table class="default_table">
    <tr>
        <td>
            <span class="title">Название <span class="red_text">*</span></span>
            <?= CHtml::activeTextField($model, 'title'); ?>
        </td>
    </tr>
</table>

<br><br>
<div class="button_main">
    <button type="submit"><?= $model->isNewRecord ? 'Добавить' : 'Сохранить'; ?></button>
</div>
<?= CHtml::endForm(); ?>