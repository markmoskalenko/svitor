<h1>Галерея &rarr; Категории</h1>
<div class="list_data_head">
    <div class="button_main">
        <button type="button" onclick="listdata.addNew();">Добавить</button>
    </div>
</div>
<div class="list_data scrolled">
    <?
    if (!empty($categories))
    {
        foreach ($categories as $cat)
        {
            echo '<div class="element active" id="element_'.$cat['id'].'" onclick="listdata.editItem(this, '.$cat['id'].');">';
            echo '<nobr>'.$cat['title'].'</nobr>';

            echo '<div class="edit_links">';
            echo '<a onclick="listdata.deleteItem(this, '.$cat['id'].'); return false;">Удалить</a>';
            echo '</div>';
            echo '</div>';
        }
    } else {
        echo '<div class="element empty">Нет ни одной категории...</div>';
    }
    ?>
</div>
<div id="trash-dialog" style="display: none;" title="Удалить категорию">Категория будет удалена без возможности восстановления. <br><br>Вы действительно хотите продолжить?</div>
<script src="/static/admin/js/e_listdata.js"></script>
<script>
    $(document).ready(function() {
        listdata.init({
            addUrl : '/admin_cp/galleryCategory/add',
            deleteUrl : '/admin_cp/galleryCategory/delete',
            editUrl : '/admin_cp/galleryCategory/edit'
        });
    });
</script>