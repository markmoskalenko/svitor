<h1>Пользователи</h1>
<div class="b-search">
    <?php echo CHtml::beginForm('/admin_cp/user/index/', 'GET'); ?>
        <input type="text" name="search" value="<?php echo $search; ?>" placeholder="логин или email...">
        <div class="button_main">
            <button onclick="submit">Найти</button>
        </div>
    <?php echo CHtml::endForm(); ?>
</div>

<?php echo !empty($search) ? 'По вашему запросу найдено: '.$count.' пользователей.' : 'На данный момент зарегистрированно: '.$count.' пользователей.'; ?>
<div class="r_fl">
    <?php
    $this->widget('CLinkPager', array(
        'pages' => $pages,
        'htmlOptions' => array('class' => 'b-default_pager'),
        'firstPageLabel' => '',
        'lastPageLabel' => '',
        'prevPageLabel' => '&larr; Назад',
        'nextPageLabel' => 'Далее &rarr;',
        'header' => ''
    ));
    ?>
</div>
<br><br>
<table class="list_table">
    <thead>
        <tr>
            <th width="30px"></th>
            <th width="250px" class="al_l"><?php echo $sort->link('login', 'Логин'); ?></th>
            <th width="200px" class="al_c"><?php echo $sort->link('email', 'E-Mail'); ?></th>
            <th class="al_c"><?php echo $sort->link('last_activity', 'Посл. активность'); ?></th>
            <th class="al_c last_r"><?php echo $sort->link('reg_date', 'Дата рег.'); ?></th>

            <!-- <th width="80px" class="al_c last_r"></th> -->
        </tr>
    </thead>
<?php
if (!empty($users_data))
{
    foreach ($users_data as $user)
    {
        echo '<tr>';
            echo '<td class="al_c"><a href="' . Yii::app()->createUrl('user/profile', array('id' => $user['id'])) . '/" target="_blank"><img src="' . Yii::app()->ImgManager->getUrlById($user->img_id, 'medium', $user->img_filename) . '" width="30px"></a></td>';

            $security = $user['security_level'] ? ' <b class="red_text">(админ)</b>' : '';
            echo '<td class="al_l"><a href="' . Yii::app()->createUrl('user/profile', array('id' => $user['id'])) . '/" target="_blank"><b>' . $user['login'] . '</b>' . $security . '</a></td>';

            echo '<td class="al_c">' . $user['email'] . '</td>';

            echo '<td class="al_c"><span title="вход: ' . mApi::HumanDatePrecise($user['last_login']) . '">' . mApi::HumanDatePrecise($user['last_activity']) . '</span></td>';

            echo '<td class="al_c">' . mApi::HumanDatePrecise($user['reg_date']) . '</td>';

        /*echo '<td class="al_c">';
            echo '<div class="button_main"><button onclick="location . assign(\'/admin_cp/user/login/id/' . $user['id'] . '\');">войти</button></div>';
        echo '</td>';*/

        echo '</tr>';
    }
}
else
{
    echo '<tr><td colspan="7" class="al_c">Ничего не найдено...</td></tr>';
}
?>
</table>
<br>
<div class="r_fl">
    <?php
    $this->widget('CLinkPager', array(
        'pages' => $pages,
        'htmlOptions' => array('class' => 'b-default_pager'),
        'firstPageLabel' => '',
        'lastPageLabel' => '',
        'prevPageLabel' => '&larr; Назад',
        'nextPageLabel' => 'Далее &rarr;',
        'header' => ''
    ));
    ?>
</div>