<?php
class Admin_cpModule extends CWebModule
{
	public $defaultController = 'Main';
	public $layout = 'main';

	public function init()
	{
		if (User::isGuest())
			Yii::app()->request->redirect('/');

         $user = User::model()->findByPk(User::getId());

        if ($user->security_level != 1){
            Yii::app()->request->redirect('/');
        }elseif($user->security_level == 2){
            Yii::app()->request->redirect('/mod_cp');
        }
	}
}