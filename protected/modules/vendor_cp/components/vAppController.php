<?php
class vAppController extends CController
{
    public function filters()
    {
        return array('accessControl');
    }

    public function accessRules()
    {
        return array(
            array('deny',
                'controllers' => array('LoadIm', 'Bid', 'Orders', 'Dashboard', 'Restore', 'Company', 'Faq', 'Posts', 'Products', 'Reviews', 'Video', 'Notices', 'Messages', 'Stock'),
                'users' => array('?')
            ),
            array('allow',
                'controllers' => array('LoadIm', 'Bid', 'Orders', 'Dashboard', 'Restore', 'Company', 'Faq', 'Posts', 'Products', 'Reviews', 'Video', 'Notices', 'Messages', 'Stock'),
                'users' => array('@')
            )
        );
    }

    public function init()
    {
        if (!Vendor::isGuest())
        {
            // Обновление последней активности пользователя при каждом обращении к сайту
            $u_last_activity = Vendor::getLastActivity();

            if ($u_last_activity == false)
                Vendor::updateLastActivity();

            // Вычисляем сколько секунд прошло с последней активности
            $diff = intval(time() - $u_last_activity);
            $minutes = $diff / 60;

            // Если последняя активность была более чем 5 минут назад
            if ($minutes >= 5)
                Vendor::updateLastActivity();

            // Учитываем посещения раз в день. Пкоа именно так (каждый раз запрос в базу), чтобы не могли накрутить.
            $vendor_visit = VendorVisits::model()->findByAttributes(array('vendor_id' => Vendor::getId()), array('limit' => 1, 'order' => 'date DESC'));

            if (empty($vendor_visit) || date('d.m.Y', $vendor_visit->date) != date('d.m.Y', time()))
            {
                $visit = new VendorVisits();
                $visit['vendor_id'] = Vendor::getId();
                $visit['ip_address'] = ip2long(Yii::app()->request->getUserHostAddress());
                $visit['date'] = time();
                $visit['user_agent'] = Yii::app()->request->getUserAgent();

                $visit->save();
            }
        }
    }
}