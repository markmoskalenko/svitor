<?php
class VendorIdentity extends CUserIdentity
{
    private $_id;

    public function auth($check_password = true)
    {
        $data = Vendor::model()->find('LOWER(email)=?', array(strtolower($this->username)));

        if ($data)
        {
            if ($check_password && $data->password != hash('md5', $data->salt.strtoupper($this->password)))
                return false;

            $this->_id = $data->id;
            $this->username = $data->email;

            $this->setState('img_id', $data->img_id);
            $this->setState('img_filename', $data->img_filename);
            $this->setState('last_activity', time());

            $this->errorCode = self::ERROR_NONE;

            // Обновим информацию (если авторизируется пользователь, а не админ)
            if ($check_password)
            {
                $data['last_ip'] = Yii::app()->request->getUserHostAddress();
                $data['last_login'] = time();
                $data['last_activity'] = time();

                $data->update();
            }
        }
        return !$this->errorCode;
    }

    public function getId()
    {
        return $this->_id;
    }
}