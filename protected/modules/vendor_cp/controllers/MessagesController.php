<?php
class MessagesController extends vAppController
{
    public $upbox_title;

    public function filters()
    {
        return array('accessControl');
    }

    public function accessRules()
    {
        return array(
            array('deny',
                'actions' => array('Index', 'New', 'View', 'Reply', 'Delete', 'DeleteSelected'),
                'users' => array('?')
            ),
            array('allow',
                'actions' => array('Index', 'New', 'View', 'Reply', 'Delete', 'DeleteSelected'),
                'users' => array('@')
            )
        );
    }

    public function actionIndex($folder = 'inbox')
    {
        Yii::app()->name = 'Сообщения — '.Yii::app()->name;

        if ($folder != 'inbox' && $folder != 'outbox')
            $folder = 'inbox';

        $criteria = new CDbCriteria;

        // Входящие
        if ($folder == 'inbox')
        {
            $criteria->condition = 'is_bid = 0 AND to_vid = :vid AND to_status != "deleted"';
            $criteria->order = 'date DESC';
            $criteria->params = array('vid' => Vendor::getId());
            $criteria->with = array('FromUser', 'FromVendor');
        }
        // Исходящие
        else if ($folder == 'outbox')
        {
            $criteria->condition = 'is_bid = 0 AND from_vid = :vid AND from_status != "deleted"';
            $criteria->order = 'date DESC';
            $criteria->params = array('vid' => Vendor::getId());
            $criteria->with = array('ToUser', 'ToVendor');
        }


        $dataProvider = new CActiveDataProvider('UserMessages', array(
            'pagination' => array(
                'pageSize' => Yii::app()->params['items_per_page'],
                'pageVar' => 'page'
            ),
            'criteria' => $criteria
        ));

        $this->render('index', array(
            'messages' => $dataProvider->getData(),
            'dataProvider' => $dataProvider,

            'folder' => $folder
        ));
    }

    public function actionNew()
    {
        $this->layout = 'upbox';
        $this->upbox_title = 'Новое сообщение';

        $to_uid = 0;
        $to_vid = 0;

        if (!empty($_GET['bid']))
            $bid = intval($_GET['bid']);
        else $bid = 0;

        if (!empty($_GET['uid']))
            $to_uid = $_GET['uid'];

        if (!empty($_GET['vid']))
            $to_vid = $_GET['vid'];

        if (empty($to_uid) && empty($to_vid))
            mApi::sayError(1, 'Не указан адресат');

        if (!empty($to_uid)) {
            $to_uid = User::model()->findByPk($to_uid);
        } else {
            $to_vid = Vendor::model()->findByPk($to_vid);
        }

        if (empty($to_uid) && empty($to_vid))
            mApi::sayError(1, 'Не известный адресат');

        $msg = new UserMessages('new');

        if (isset($_POST['UserMessages']))
        {
            $msg->attributes = $_POST['UserMessages'];
            $msg->from_vid = Vendor::getId();
            $msg->to_uid = !empty($to_uid) ? $to_uid->id : 0;
            $msg->to_vid = !empty($to_vid) ? $to_vid->id : 0;
            $msg->is_bid = $bid;


            if ($msg->save())
            {
                $from = Vendor::model()->findByPk($msg->from_vid);

                // Если у получателя включены уведомления по Email - уведомим его о том, что ему прешло сообщение.
                $to = !empty($to_vid) ? $to_vid : $to_uid;

                if ($to->notify_new_msg)
                    UserMessages::sendEmailNotify($to, $from, $msg, 'vendor');

                echo 'ok';
                Yii::app()->end();
            }
            else {
                echo CHtml::errorSummary($msg);
            }
        }
        else
        {
            $this->render('_form_new', array(
                'msg' => $msg,

                'to_uid' => $to_uid,
                'to_vid' => $to_vid,
                'bid'    => $bid,
            ));
        }
    }

    public function actionSupport()
    {
        $this->upbox_title = 'Новое сообщение';

        $to_uid = 16;
        $to_vid = 0;
        $msg = new UserMessages('new');
        if(isset($_SESSION["vendor__id"])){
            $to_vid = $_SESSION["vendor__id"];
        }else{
            $to_vid  = User::getId();
        }

        $this->render('_support', array(
            'msg' => $msg,
            'to_uid' => $to_uid,
            'to_vid' => $to_vid
        ));
    }

    public function actionReply($id = 0)
    {
        $id = intval($id);

        $reply_msg = UserMessages::model()->findByAttributes(array('to_vid' => Vendor::getId(), 'id' => $id));

        if (empty($reply_msg))
            $reply_msg = UserMessages::model()->findByAttributes(array('from_vid' => Vendor::getId(), 'id' => $id));

        if (empty($reply_msg) || empty($_POST['message']))
            mApi::sayError(1, '0');

        $msg = new UserMessages('new');

        // Ответ пользователю
        if ($reply_msg->to_vid == Vendor::getId() && $reply_msg->from_uid != 0)
        {
            $msg['to_uid'] = $reply_msg->from_uid;
        }
        // Ответ компании
        else if ($reply_msg->to_vid == Vendor::getId() && $reply_msg->from_vid != 0){
            $msg['to_vid'] = $reply_msg->from_vid;
        }
        // Ответ на свое, ранее отправленное сообщение для ПОЛЬЗОВАТЕЛЯ
        else if ($reply_msg->to_uid != 0 && $reply_msg->from_vid == Vendor::getId()) {
            $msg['to_uid'] = $reply_msg->to_uid;
        }
        // Ответ на свое, ранее отправленное сообщение для КОМПАНИИ
        else if ($reply_msg->to_vid != 0 && $reply_msg->from_vid == Vendor::getId()) {
            $msg['to_vid'] = $reply_msg->to_vid;
        }

        $msg['from_vid'] = Vendor::getId();

        if (!preg_match('/^[Re:]/', $reply_msg->title)) {
            $msg['title'] = 'Re: '.$reply_msg->title;
        } else {
            $msg['title'] = $reply_msg->title;
        }

        $msg['message'] = $_POST['message'];

        if ($msg->save())
        {
            $from = Vendor::model()->findByPk($msg->from_vid);

            // Если у получателя включены уведомления по Email - уведомим его о том, что ему прешло сообщение.
            if (!empty($msg->to_vid)) {
                $to = Vendor::model()->findByPk($msg->to_vid);
            } else {
                $to = User::model()->findByPk($msg->to_uid);
            }

            if ($to->notify_new_msg)
                UserMessages::sendEmailNotify($to, $from, $msg, 'vendor');

            echo 'ok';
        } else {
            echo CHtml::errorSummary($msg);
        }
    }

    public function actionView($id = 0)
    {
        Yii::app()->name = 'Сообщения — '.Yii::app()->name;

        $id = intval($id);

        $criteria = new CDbCriteria;
        $criteria->condition = '(to_vid = :vid OR from_vid = :vid) AND t.id = :id';
        $criteria->params = array(
            ':vid' => Vendor::getId(),
            ':id' => $id
        );
        $criteria->with = array('FromUser', 'FromVendor');

        $message = UserMessages::model()->find($criteria);

        if (empty($message))
            $this->redirect(Yii::app()->createUrl('messages'));

        // Если сообщение помеченно как "удаленное" то не показываем его
        if ($message->to_vid == Vendor::getId() && $message->to_status == 'deleted' ||
            $message->from_vid == Vendor::getId() && $message->from_status == 'deleted')
        {
            $this->redirect(Yii::app()->createUrl('messages'));
        }

        // Если это новое сообщение для компании, то при меням статус на "прочитано"
        if ($message->to_vid == Vendor::getId() && $message->to_status == 'new')
        {
            $message->to_status = 'readed';
            $message->update();
        }

        $this->render('view', array(
            'message' => $message
        ));
    }

    public function actionDelete($id = 0)
    {
        $id = intval($id);

        $criteria = new CDbCriteria;
        $criteria->condition = '(to_vid = :vid OR from_vid = :vid) AND id = :id';
        $criteria->params = array(':vid' => Vendor::getId(), ':id' => $id);

        $msg = UserMessages::model()->find($criteria);
        if (!empty($msg))
        {
            // Если это "входящее" сообщение
            if ($msg->to_vid == Vendor::getId()) {
                $msg['to_status'] = 'deleted';
            }
            // Если "отправленное"
            else if ($msg->from_vid == Vendor::getId()) {
                $msg['from_status'] = 'deleted';
            }
            $msg->update();
        }
    }

    public function actionDeleteSelected($folder = 'inbox')
    {
        if (empty($_POST['msg']))
            mApi::sayError(1, 'Нечего удалять...');

        if ($folder != 'inbox' && $folder != 'outbox')
            mApi::sayError(1, 'Ошибка при удалении...');

        $selected_msg = array();
        foreach ($_POST['msg'] as $i => $id)
            $selected_msg[] = $id;

        if ($folder == 'inbox')
        {
            $criteria = new CDbCriteria;
            $criteria->condition = 'to_vid = :vid';
            $criteria->params = array(':vid' => Vendor::getId());
            $criteria->addInCondition('id', $selected_msg);

            UserMessages::model()->updateAll(array('to_status' => 'deleted'), $criteria);
        }
        else if ($folder == 'outbox')
        {
            $criteria = new CDbCriteria;
            $criteria->condition = 'from_vid = :vid';
            $criteria->params = array(':vid' => Vendor::getId());
            $criteria->addInCondition('id', $selected_msg);

            UserMessages::model()->updateAll(array('from_status' => 'deleted'), $criteria);
        }
    }
}