<?php
class FaqController extends vAppController
{
    public function actionIndex()
    {
        $model = VendorFaq::model()->findAllByAttributes(array('vendor_id' => Vendor::getId()), array('order' => 'last_edit DESC'));
        $this->render('index', array('model' => $model));
    }

    public function actionAdd()
    {
        $model = new VendorFaq('add');
        if (isset($_POST['VendorFaq']))
        {
            $time = time();
            $model->attributes = $_POST['VendorFaq'];
            $model->vendor_id = Vendor::getId();
            $model->last_edit = $time;
            $model->added_date = $time;

            if ($model->save())
                $this->redirect('/vendor_cp/faq');
        }
        $this->render('_form', array('model' => $model));
    }

    public function actionEdit($id = 0)
    {
        $id = intval($id);
        if (empty($id))
            $this->redirect('/vendor_cp/faq');

        $model = VendorFaq::model()->findByAttributes(array('id' => $id, 'vendor_id' => Vendor::getId()));
        if (empty($model))
            $this->redirect('/vendor_cp/faq');

        if (isset($_POST['VendorFaq']))
        {
            $model->scenario = 'edit';
            $model->attributes = $_POST['VendorFaq'];
            $model->last_edit = time();

            if ($model->save())
                $this->redirect('/vendor_cp/faq');
        }
        $this->render('_form', array('model' => $model));
    }

    public function actionDelete($id = 0)
    {
        $id = intval($id);
        if (empty($id))
            $this->redirect('/vendor_cp/faq');

        $model = VendorFaq::model()->findByAttributes(array('id' => $id, 'vendor_id' => Vendor::getId()));
        if (empty($model))
            $this->redirect('/vendor_cp/faq');

        $model->delete();
        $this->redirect('/vendor_cp/faq');
    }
}
?>