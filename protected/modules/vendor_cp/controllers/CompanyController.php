<?php
class CompanyController extends vAppController
{
	public $defaultAction = 'generalInfo';
    public $upbox_title;

    /************
     *  Общая информация
     ************/
	public function actionGeneralInfo()
	{
        $success = '';
		$vendor = Vendor::model()->findByAttributes(array('id' => Vendor::getId()));
        $vendor->scenario = 'edit';

        // Сохранение информации
		if (isset($_POST['Vendor']))
		{
            $old_img_id = $vendor->img_id;
            $vendor->attributes = $_POST['Vendor'];

            // Если изменили картинку, то старой присвоим статус "удалено"
            if ($old_img_id != $vendor->img_id)
                FileImages::model()->updateAll(array('is_deleted' => 1), 'id ='.$old_img_id);

            if ($vendor->save())
                $success = 'Все изменения успешно сохранены.';
        }

	    $this->render('general_info', array(
            'vendor' => $vendor,
            'success' => $success
        ));
	}



    /************
     *  Разделы каталога
     ************/
    public function actionCategories()
    {
        $act = $_GET;
        $max_count = Yii::app()->params['vendor_max_category'];

        $vendor_category = VendorCategory::model()->with('Category')->findAllByAttributes(array('vendor_id' => Vendor::getId()));
        $cat_count = count($vendor_category);

        if (isset($act['a_get_attributes']) && !empty($act['cat_id']))
        {
            $cat_id = intval($act['cat_id']);
            $this->widget('application.modules.vendor_cp.widgets.AttrsByCatId', array(
                'cat_id' => $cat_id,
                'vendor_id' => Vendor::getId()
            ));
        }

        // Добавляем не больше, чем разрешено max_count
        else if (isset($act['add']) && $cat_count < $max_count)
        {
            $model = new VendorCategory;

            if (isset($_POST['VendorCategory']))
            {
                $model->scenario = 'add';
                $model->vendor_id = Vendor::getId();
                $model->attributes = $_POST['VendorCategory'];

                if ($model->save())
                {
                    Vendor::saveFeaturesForCategory($model->cat_id, $_POST['Vendor']['Features'], $model->getScenario());

                    $this->redirect('/vendor_cp/company/categories/');
                }
            }

            $this->render('_form_category', array(
                'model' => $model
            ));
        }


        // Редактирование характеристик для раздела
        else if (isset($act['edit']) && !empty($act['id']))
        {
            $id = intval($act['id']);

            $model = VendorCategory::model()->with('Category')->findByAttributes(array('id' => $id, 'vendor_id' => Vendor::getId()));
            if (empty($model))
                $this->redirect('/vendor_cp/company/categories/');

            $model->scenario = 'edit';

            if (isset($_POST['Vendor']['Features']))
            {
                // Сохраняем характеристики
                Vendor::saveFeaturesForCategory($model->cat_id, $_POST['Vendor']['Features'], $model->getScenario());

                $this->redirect('/vendor_cp/company/categories/');
            }

            $this->render('_form_attributes', array(
                'model' => $model
            ));
        }

        // Удаление раздела (и характеристик к нему)
        else if (isset($act['delete']) && !empty($act['id']))
        {
            $id = intval($act['id']);
            if (empty($id))
                mApi::sayError(1, 'Не получен параметр id');

            $vendor_id = Vendor::getId();
            $cat = VendorCategory::model()->findByAttributes(array('id' => $id, 'vendor_id' => $vendor_id));

            if (!empty($cat))
            {
                // Удаляем характеристики для этого раздела
                VendorAttrValues::model()->deleteAllByAttributes(array('cat_id' => $cat['cat_id'], 'vendor_id' => $vendor_id));

                // И затем сам раздел
                if ($cat->delete()) {
                    echo $cat_count - 1;
                    die();
                } else {
                    mApi::sayError(1, 'Не удалось удалить раздел. Попробуйте еще раз...');
                }
            }
            else
            {
                mApi::sayError(1, 'У этой компании нет такого раздела');
            }
        }


        else
        {
            $this->render('categories', array(
                'vendor_category' => $vendor_category,
                //'cats_data' => $cats_data,

                'cat_count' => $cat_count,
                'max_count' => $max_count
            ));
        }
    }

    /************
     *  Адреса
     ************/
    public function actionAddresses()
    {
        $act = $_GET;
        $addresses_data = VendorAddresses::model()->with('Country', 'Region', 'City', 'Metro')->findAllByAttributes(array('vendor_id' => Vendor::getId()));
        $count = count($addresses_data);
        $max_count = Yii::app()->params['vendor_max_address'];

        $countries = Countries::model()->findAll(array('order' => 'sort DESC, name ASC'));


        // Добавляем не больше чем разрешено
        if (isset($act['add']) && $count < $max_count)
        {
            $address = new VendorAddresses('add');

            if (isset($_POST['VendorAddresses']))
            {
                $address->attributes = $_POST['VendorAddresses'];
                $address->vendor_id = Vendor::getId();

                if (empty($address->metro_id))
                    $address->metro_id = 0;

                if ($address->save())
                    $this->redirect('/vendor_cp/company/addresses/');
            }

            $this->render('_form_address', array(
                'address' => $address,
                'countries' => $countries,
            ));
        }


        // Редактирование адреса
        else if (isset($act['edit']) && !empty($act['id']))
        {
            $id = intval($act['id']);

            $address = VendorAddresses::model()->findByAttributes(array('id' => $id, 'vendor_id' => Vendor::getId()));
            if (empty($address))
                $this->redirect('/vendor_cp/company/addresses/');

            if (isset($_POST['VendorAddresses']))
            {
                $address->scenario = 'edit';
                $address->attributes = $_POST['VendorAddresses'];

                if (empty($address->metro_id))
                    $address->metro_id = 0;

                if ($address->save())
                    $this->redirect('/vendor_cp/company/addresses/');
            }

            $this->render('_form_address', array(
                'address' => $address,
                'countries' => $countries,
            ));
        }


        // Удаление адреса
        else if (isset($act['delete']) && !empty($act['id']))
        {
            $id = intval($act['id']);
            $address = VendorAddresses::model()->findByAttributes(array('id' => $id, 'vendor_id' => Vendor::getId()));
            if (empty($address))
                Yii::app()->end();

            if ($address->delete())
            {
                echo $count - 1;
                die();
            }
            else
            {
                mApi::sayError(1, 'Ошибка при удалении адреса. Попробуйте еще раз...');
            }
        }


        // Список адресов
        else
        {
            $this->render('addresses', array(
                'count' => $count,
                'max_count' => $max_count,

                'addresses_data' => $addresses_data
            ));
        }
    }

    /************
     *  Реквизиты
     *  добавление / редактирование
     *
     ************/
    public function actionDetails()
    {
        $success = '';
        $details = VendorDetails::model()->findByAttributes(array('vendor_id' => Vendor::getId()));

        if (empty($details))
        {
            $details = new VendorDetails();
            $details->vendor_id = Vendor::getId();
        }

        if (isset($_POST['VendorDetails']))
        {
            $details->attributes = $_POST['VendorDetails'];
            if ($details->save())
                $success = 'Все изменения успешно сохранены.';
        }

        $this->render('details', array(
            'details' => $details,
            'success' => $success
        ));
    }

    /************
     *  Фото и Видео
     *  просмотр / добавление / редактирование / ajax-загрузка изображения
     *
     ************/
    public function actionMultimedia()
    {
        $images = VendorImages::model()->findAllByAttributes(array('vendor_id' => Vendor::getId()));
        $videos = VendorVideos::model()->findAllByAttributes(array('vendor_id' => Vendor::getId()));

        $images_count = count($images);
        $videos_count = count($videos);

        $max_images = Yii::app()->params['vendor_max_image'];
        $max_videos = Yii::app()->params['vendor_max_video'];

        // Загружаем только если кол-во изображений меньше vendor_max_image
        if (isset($_POST['img_id']) && $images_count < $max_images)
        {
            $image_info = $_POST;

            if ($image_info['status'] != 'ok')
                die();

            $model = new VendorImages;
            $model['vendor_id'] = Vendor::getId();
            $model['img_id'] = $image_info['img_id'];
            $model['img_filename'] = $image_info['file_name'];
            $model['added_date'] = time();

            if ($model->save())
            {
                $images_count++;

                $image_info['id'] = $model->id;
                $image_info['img_count'] = $images_count;

                echo json_encode($image_info);
                return;
            }
        }

        // Редактирование изображения
        else if (isset($_GET['edit_image']))
        {
            $id = intval($_GET['edit_image']);
            if (empty($id))
                Yii::app()->end();

            $image = VendorImages::model()->findByAttributes(array('id' => $id, 'vendor_id' => Vendor::getId()));
            if (empty($image))
                Yii::app()->end();

            if (!empty($_POST['VendorImages']))
            {
                $image->attributes = $_POST['VendorImages'];
                if ($image->save())
                {
                    echo 'ok';
                } else {
                    echo CHtml::errorSummary($image);
                }
            }
            else
            {
                $this->layout = 'upbox';
                $this->upbox_title = 'Редактирование описания';

                $this->render('_form_image', array(
                    'image' => $image
                ));
            }
        }

        else if (isset($_GET['video']))
        {
            $this->layout = 'upbox';
            $this->upbox_title = 'Новое видео';

            $this->render('_form_video', array(
                'model' => new VendorImages
            ));
        }

        // Сохраняем только если кол-во видео меньше установленного в настройках параметра
        else if (isset($_POST['video_id']) && $videos_count < $max_videos && empty($_GET['edit_video']))
        {
            $model = new VendorVideos('add');

            $model['vendor_id'] = Vendor::getId();
            $model['video_provider'] = Yii::app()->request->getPost('video_provider');
            $model['video_id'] = Yii::app()->request->getPost('video_id');
            $model['title'] = Yii::app()->request->getPost('title');
            $model['description'] = Yii::app()->request->getPost('description');
            $model['added_date'] = time();
            $model['thumbnail_url'] = Yii::app()->request->getPost('thumbnail_url');

            if ($model->save())
            {
                $videos_count++;
                $video_url = $model->video_provider == 'vimeo' ? 'http://vimeo.com/' : 'http://www.youtube.com/watch?v=';

                $video_data = array();
                $video_data['error'] = '';
                $video_data['id'] = $model->id;
                $video_data['video_url'] = $video_url . $model->video_id;
                $video_data['thumbnail_url'] = $model->thumbnail_url;
                $video_data['video_count'] = $videos_count;
                $video_data['title'] = $model->title;

                echo json_encode($video_data);
                die();
            }
            else
            {
                $title_error = $model->getError('title');
                echo !empty($title_error) ? '{"error":"' . $title_error . '"}"' : '{"error":"' . $model->getError('description') . '"}';
                die();
            }
        }

        // Редактирование видео
        else if (isset($_GET['edit_video']))
        {
            $id = intval($_GET['edit_video']);
            if (empty($id))
                Yii::app()->end();

            $model = VendorVideos::model()->findByAttributes(array('id' => $id, 'vendor_id' => Vendor::getId()));
            $model->scenario = 'edit';

            if (!empty($_POST))
            {
                $model['title'] = Yii::app()->request->getPost('title');
                $model['description'] = Yii::app()->request->getPost('description');

                if ($model->save())
                {
                    echo 'ok';
                    die();
                } else {
                    $title_error = $model->getError('title');
                    echo !empty($title_error) ? $title_error : $model->getError('description');
                    die();
                }
            }
            else
            {
                $this->layout = 'upbox';
                $this->upbox_title = 'Редактирование видеозаписи';

                $this->render('_form_video', array(
                    'model' => $model
                ));
            }
        }

        // Удаление изображения или видео
        else if (isset($_GET['delete']) && !empty($_GET['id']))
        {
            $id = intval($_GET['id']);
            if (empty($id))
                Yii::app()->end();

            if ($_GET['delete'] == 'image')
            {
                $image = VendorImages::model()->findByAttributes(array('id' => $id, 'vendor_id' => Vendor::getId()));
                if (empty($image))
                    return;

                // Установим статус "удалено" для картинки
                FileImages::model()->updateAll(array('is_deleted' => 1), 'id = '.$image->img_id);

                // Удаляем картинку у компании
                $image->delete();
            }
            else if ($_GET['delete'] == 'video')
            {
                VendorVideos::model()->deleteAllByAttributes(array('id' => $id, 'vendor_id' => Vendor::getId()));
            }
            die('1');
        }

        else
        {
            $this->render('multimedia', array(
                'images' => $images,
                'videos' => $videos,

                'images_count' => $images_count,
                'videos_count' => $videos_count,

                'max_images' => $max_images,
                'max_videos' => $max_videos
            ));
        }
    }

    /************
     *  Сертификаты и Лицензии
     *  просмотр / добавление / редактирование / ajax-загрузка изображения
     *
     ************/
    public function actionCertificates()
    {
        $act = $_GET;

        $max_count = Yii::app()->params['vendor_max_certificates'];
        $certificates_count = VendorCertificates::model()->count('vendor_id='.Vendor::getId());

        // Добавляем не больше, чем разрешено max_certificates
        if (isset($act['add']) && $certificates_count < $max_count)
        {
            $model = new VendorCertificates;
            if (isset($_POST['VendorCertificates']))
            {
                $model->attributes = $_POST['VendorCertificates'];
                $model->vendor_id = Vendor::getId();
                $model->added_date = time();

                if ($model->save())
                    $this->redirect('/vendor_cp/company/certificates/');
            }
            $this->render('_form_certificate', array('model' => $model));
        }


        else if (isset($act['edit']) && !empty($act['id']))
        {
            $id = intval($act['id']);

            $model = VendorCertificates::model()->findByAttributes(array('id' => $id, 'vendor_id' => Vendor::getId()));
            if (empty($model))
                $this->redirect('/vendor_cp/company/certificates/');

            if (isset($_POST['VendorCertificates']))
            {
                $old_img_id = $model->img_id;
                $model->attributes = $_POST['VendorCertificates'];

                // Если изменили картинку, то старой присвоим статус "удалено"
                if ($old_img_id != $model->img_id)
                    FileImages::model()->updateAll(array('is_deleted' => 1), 'id ='.$old_img_id);

                if ($model->save())
                    $this->redirect('/vendor_cp/company/certificates/');
            }

            $this->render('_form_certificate', array('model' => $model));
        }


        else if (isset($act['delete']) && !empty($act['id']))
        {
            $id = intval($act['id']);
            $certificate = VendorCertificates::model()->findByAttributes(array('id' => $id, 'vendor_id' => Vendor::getId()));
            if (!empty($certificate))
            {
                // Установим статус "удалено" для картинки
                FileImages::model()->updateAll(array('is_deleted' => 1), 'id = '.$certificate->img_id);

                // Удалим "сертификат"
                $certificate->delete();

                echo $certificates_count - 1;
            }
            else
            {
                mApi::sayError(1, 'Не удалось удалить. Попробуйте еще раз...');
            }
        }

        else
        {
            $model = VendorCertificates::model()->findAllByAttributes(array('vendor_id' => Vendor::getId()));
            $this->render('certificates', array(
                'model' => $model,
                'certificates_count' => $certificates_count,
                'max_count' => $max_count
            ));
        }
    }
}