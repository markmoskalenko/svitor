<?php
class MainController extends vAppController
{
	public $defaultAction = 'index';

	public function actionIndex()
	{
        if (!Vendor::isGuest())
            $this->redirect(Yii::app()->createUrl('vendor_cp/company'));

		$this->render('index');
	}
}