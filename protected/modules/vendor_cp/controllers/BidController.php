<?php

class BidController extends vAppController
{
	public function actionIndex( $type = 'opened' )
	{
        $criteria = new CDbCriteria;

        if( $type == 'opened' ){
            $criteria->condition = 'Unix_timestamp(date_end) >= CURDATE()';
        }elseif( $type == 'closed' ) {
            $criteria->condition = 'Unix_timestamp(date_end) <= CURDATE()';
        }

        $criteria->scopes = array('opened');

        $criteria->addInCondition('cat_id', VendorCategory::getCurrVendorCategory());

        $dataProvider = new CActiveDataProvider('Bid',array(
                                                         'criteria'=>$criteria
                                                    ));
        Bid::updateLastBid();

        $this->render('index',array(
                                'dataProvider' => $dataProvider,
                                'opened'       => $type,
                                'bid'          => (Yii::app()->request->getParam('bid') ? intval( Yii::app()->request->getParam('bid') ) : 0),
                                'uid'          => (Yii::app()->request->getParam('uid') ? intval( Yii::app()->request->getParam('uid') ) : 0)
		));
	}


	public function loadModel($id)
	{
		$model=Bid::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='bid-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
