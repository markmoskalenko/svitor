<?php
class PostsController extends vAppController
{
    public function actionIndex()
    {
        $criteria = new CDbCriteria;
        $criteria->condition = 't.author_vid = :vendor_id';
        $criteria->params = array(':vendor_id' => Vendor::getId());
        $criteria->order = 'created_date DESC';

        $dataProvider = new CActiveDataProvider('Post', array(
            'pagination' => array(
                'pageSize' => Yii::app()->params['items_per_page'],
                'pageVar' => 'page'
            ),
            'criteria' => $criteria
        ));

        $this->render('index', array(
            'dataProvider' => $dataProvider,
            'posts' => $dataProvider->getData(),
            'posts_count' => $dataProvider->getTotalItemCount()
        ));
    }

    public function actionAdd()
    {
        $model = new Post('add');

        if (isset($_POST['Post']))
        {
            $model->attributes = $_POST['Post'];
            $model['author_vid'] = Vendor::getId();
            $model['created_date'] = time();
            $model['status'] = 'verification';

            if ($model->save())
                $this->redirect(Yii::app()->createUrl('vendor_cp/posts'));
        }

        $this->render('_form', array(
            'model' => $model,
            'categories' => PostCategory::model()->findAll()
        ));
    }

    public function actionEdit($id = 0)
    {
        $id = intval($id);
        if (empty($id))
            $this->redirect(Yii::app()->createUrl('vendor_cp/posts'));

        $model = Post::model()->findByAttributes(array('author_vid' => Vendor::getId(), 'id' => $id));

        if (empty($model) || $model->status == 'published')
            $this->redirect(Yii::app()->createUrl('posts'));

        if (isset($_POST['Post']))
        {
            $model->attributes = $_POST['Post'];
            $model['edited_date'] = time();
            $model['status'] = 'verification';

            if ($model->save())
                $this->redirect(Yii::app()->createUrl('vendor_cp/posts'));
        }


        $this->render('_form', array(
            'model' => $model,
            'categories' => PostCategory::model()->findAll()
        ));
    }

    public function actionDelete($id = 0)
    {

    }
}