<?php
class NoticesController extends vAppController
{
    public function actionIndex()
    {
        $criteria = new CDbCriteria();
        $criteria->condition = 'vendor_id = :vendor_id';
        $criteria->params = array(':vendor_id' => Vendor::getId());
        $criteria->order = 't.date DESC';
        $criteria->with = array('User', 'UserCategory', 'UserWedding', 'UserHoliday');

        $dataProvider = new CActiveDataProvider('UserLogVendors', array(
            'pagination' => array(
                'pageSize' => Yii::app()->params['items_per_page'],
                'pageVar' => 'page'
            ),
            'criteria' => $criteria
        ));

        // Отметим все уведомления как просмотренные
        UserLogVendors::model()->updateAll(array('status' => 'viewed'), 'vendor_id=' . Vendor::getId());

        $this->render('index', array(
            'notices' => $dataProvider->getData(),
            'dataProvider' => $dataProvider
        ));
    }
}