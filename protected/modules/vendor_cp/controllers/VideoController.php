<?php
class VideoController extends vAppController
{
    public $defaultAction = 'checkUrl';

    private $_error;

    const UNKNOWN_PROVIDER = 100;
    const INCORRECT_URL = 101;
    const VIDEO_NOT_FOUND = 102;

    public function actionCheckUrl($url = '')
    {
        $url = trim($url);
        if (empty($url))
            Yii::app()->end();

        // Youtube.com
        if (preg_match('/^(http|https)\:\/\/(www\.){0,1}youtube.com/', $url))
        {
            preg_match('/v=([a-z0-9\-\_]{11})/is', $url, $youtube_id);

            if (isset($youtube_id[1])) {
                $video_data = $this->getVideoData('youtube', $youtube_id[1]);
            } else {
                $this->_error = self::INCORRECT_URL;
            }
        }
        // Youtu.be
        else if (preg_match('/^(http|https)\:\/\/(www\.){0,1}youtu.be/', $url))
        {
            preg_match('/.be\/([a-z0-9\-\_]{11})/is', $url, $youtube_id);

            if (isset($youtube_id[1])) {
                $video_data = $this->getVideoData('youtube', $youtube_id[1]);
            } else {
                $this->_error = self::INCORRECT_URL;
            }
        }
        // Vimeo.com
        else if (preg_match('/^(http|https)\:\/\/(www\.){0,1}vimeo.com/', $url))
        {
            preg_match('/([0-9]+)/i', $url, $vimeo_id);

            if (isset($vimeo_id[1])) {
                $video_data = $this->getVideoData('vimeo', $vimeo_id[1]);
            } else {
                $this->_error = self::INCORRECT_URL;
            }
        }
        else
        {
            $this->_error = self::UNKNOWN_PROVIDER;
        }

        // Если были какие-то ошибки...
        if (!empty($this->_error))
        {
            echo '{"error":"' . $this->getErrorMsg($this->_error) . '"}';
        }
        // Если ошибок нет, то выводим JSON данные видео
        else
        {
            echo json_encode($video_data);
        }
    }

    public function getVideoData($provider, $video_id)
    {
        $video_data = array();

        if ($provider == 'youtube')
        {
            $youtube_video_data = json_decode(@file_get_contents('http://gdata.youtube.com/feeds/api/videos/' . $video_id . '?alt=json'), true);

            // Удостоверимся, что это то, что нам нужно
            if (isset($youtube_video_data['entry']))
            {
                $video_data['video_id'] = $video_id;
                $video_data['title'] = $youtube_video_data['entry']['title']['$t'];
                $video_data['description'] = $youtube_video_data['entry']['content']['$t'];
                $video_data['video_provider'] = 'youtube';
                $video_data['thumbnail_url'] = $youtube_video_data['entry']['media$group']['media$thumbnail']['1']['url'];
            }
        }
        else if ($provider == 'vimeo')
        {
            $vimeo_video_data = json_decode(@file_get_contents('http://vimeo.com/api/oembed.json?url=http://vimeo.com/' . $video_id), true);

            // Удостоверимся, что это то, что нам нужно
            if (isset($vimeo_video_data['video_id']))
            {
                $video_data['video_id'] = $video_id;
                $video_data['title'] = $vimeo_video_data['title'];
                $video_data['description'] = $vimeo_video_data['description'];
                $video_data['video_provider'] = 'vimeo';
                $video_data['thumbnail_url'] = $vimeo_video_data['thumbnail_url'];
            }
        }

        // Если ничего не вышло...
        if (empty($video_data)) {
            $this->_error = self::VIDEO_NOT_FOUND;
        }

        return $video_data;
    }

    public function getErrorMsg($error_code)
    {
        switch ($error_code)
        {
            case self::UNKNOWN_PROVIDER:
                return 'Видеосервис не поддерживается.';
                break;

            case self::INCORRECT_URL:
                return 'Некорректная ссылка.';
                break;

            case self::VIDEO_NOT_FOUND:
                return 'Не удалось получить данные видео.';
                break;

            default:
                return 'Неизвестная ошибка.';
                break;
        }
    }
}