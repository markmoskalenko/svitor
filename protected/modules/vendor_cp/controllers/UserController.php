<?php
class UserController extends vAppController
{
    public $defaultAction = 'login';

    public function actions()
    {
        return array(
            'captcha' => array(
                'class' => 'CCaptchaAction',
                'backColor' => 0xf0f0f0,
                'foreColor' => 0x545454,
                'width' => 175,
                'height' => 55
            ),
        );
    }

    public function actionLogin()
    {
        if (!Vendor::isGuest())
            $this->redirect('/vendor_cp');

        Yii::app()->name = 'Вход - '.Yii::app()->name;

        if (!empty($_POST['email']) && !empty($_POST['password']))
        {
            $email = htmlspecialchars(trim($_POST['email']));
            $password = htmlspecialchars(trim($_POST['password']));

            $duration = (!empty($_POST['rememberMe'])) ? 3600*24*30 : 0; // 30 days
            $identity = new VendorIdentity($email, $password);

            if ($identity->auth())
            {
                Yii::app()->user->login($identity, $duration);
                $this->redirect('/vendor_cp/company/');
            }
            else {
                Yii::app()->user->setFlash('login_error', 'неверный Email или пароль.');
            }
        }

        $this->render('_form_login');
    }

    public function actionLogout()
    {
        if (!Vendor::isGuest())
            Yii::app()->user->logout();

        $this->redirect('/vendor_cp');
    }

    public function actionForgotpassword()
    {
        if (!Vendor::isGuest())
            $this->redirect('/vendor_cp',true,301);

        Yii::app()->name = 'Восстановление пароля - '.Yii::app()->name;

        $error = '';
        if (!empty($_POST['email'])){

            $user = Vendor::model()->find('LOWER(email)=?', array(strtolower($_POST['email'])));

            if ($user) {
                $newPassword = $this->rand_str(7);
                $user->password = hash('md5', $user->salt.strtoupper($newPassword));
                if ($user->save())
                {
                    $postArr = array(
                        "title" => "Ваш новый пароль!",
                        "message" => $newPassword."<br><br>Вы можете поменять присланный пароль на удобный вам. Войдите с новым паролем и перейдите в меню Настройки (правый верхний угол)."
                    );
                    $this->sendMail($postArr,$_POST['email']);
                }
                $this->render('message', array(
                    'error' => $error
                ));

            } else {
                $error = "Пользователя с таким email не существует";
                $this->render('forgotpassword', array(
                    'error' => $error
                ));
            }
        }else{
            $this->render('forgotpassword', array(
                'error' => $error
            ));
        }
    }

    public function actionRegistration()
    {
        if (!Vendor::isGuest())
            $this->redirect('/vendor_cp');

        Yii::app()->name = 'Регистрация - '.Yii::app()->name;

        $vendor = new Vendor('registration');

        if (isset($_POST['Vendor']))
        {
            $vendor->attributes = $_POST['Vendor'];

            if ($vendor->save())
                $this->redirect('/vendor_cp/user/login');
        }

        // Проверим уникальность E-Mail
        if (Yii::app()->request->isAjaxRequest && !empty($_GET['check_email']))
        {
            $this->layout = 'ajax';
            if (Vendor::model()->find('LOWER(email)=?', array(strtolower($_GET['check_email'])))) {
                echo 1;
            } else {
                echo 0;
            }
            die();
        }

        $this->render('_form_registration', array(
            'vendor' => $vendor
        ));
    }

    public function actionSettings()
    {
        if (Vendor::isGuest())
            $this->redirect('vendor_cp/user/login');

        $vendor = Vendor::model()->findByPk(Vendor::getId());
        $success = array();

        if (isset($_POST['Vendor']))
        {
            if (isset($_POST['Notifications']))
            {
                $vendor->attributes = $_POST['Vendor'];
                if ($vendor->save())
                    $success[0] = 'Изменения успешно сохранены.';
            }
            else if (isset($_POST['ChangePassword']))
            {
                $vendor->scenario = 'change_password';
                $vendor->attributes = $_POST['Vendor'];

                if ($vendor->validate()) {
                    $vendor['password'] = $vendor->new_password;

                    if ($vendor->save(false))
                    {
                        $vendor['old_password'] = '';
                        $vendor['new_password'] = '';
                        $vendor['confirm_new_password'] = '';

                        $success[1] = 'Изменения успешно сохранены.';
                    }
                }
            }
        }

        $this->render('settings', array(
            'vendor' => $vendor,
            'success' => $success,
        ));
    }

    private function sendMail($arr,$email) {

        $subject = "Восстановление пароля - svitor.ru";
        $headers = "Content-Type: text/html; charset=UTF-8"."\r\n";
        $headers .= "Subject: {$subject}"."\r\n";
        $headers .= 'From: Василиса Свитрова <noreply@svitor.ru>' . "\r\n" .
            'Reply-To: noreply@svitor.ru' . "\r\n";


        // Заполняем нужными данными
        $body = "<h2>".$arr['title']."</h2>";
        $body .= $arr['message']."<br/>";
        $body .= "С наилучшими пожеланиями, Svitor.ru ";

        mail($email, $subject, $body, $headers);

    }

    private function rand_str($length = 32, $chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890')
    {
        $chars_length = (strlen($chars) - 1);

        $string = $chars{rand(0, $chars_length)};

        for ($i = 1; $i < $length; $i = strlen($string))
        {
            $r = $chars{rand(0, $chars_length)};
            if ($r != $string{$i - 1}) $string .=  $r;
        }
        return $string;
    }
}