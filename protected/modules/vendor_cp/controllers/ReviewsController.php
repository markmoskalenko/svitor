<?php
class ReviewsController extends vAppController
{
    public function actionIndex()
    {
        $model = VendorReviews::model()->with('User')->findAllByattributes(array('vendor_id' => Vendor::getId()));
        $this->render('index', array('model' => $model));
    }
}