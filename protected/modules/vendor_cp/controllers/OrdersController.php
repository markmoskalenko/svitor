<?php
/**
 * @author Москаленко Марк.
 */
class OrdersController extends vAppController
{
    /**
     * Ajax изменения параметров заказа
     */
    public function actionSaveAjaxValue()
    {
        $id    = Yii::app()->request->getPost('id');
        $name  = Yii::app()->request->getPost('name');
        $value = Yii::app()->request->getPost('value');

        if( empty($id) || empty($name) || empty($value)  || $value == 0 ) $this->ajaxError();

        if( $name == 'delivery' ){
            $result = Orders::saveDelivery( $id, $value );
        }else{
            $result = BondOrders::saveAjaxParam( $id, $name, $value );
        }

        if( !$result ) $this->ajaxError();

        // r - result, s - summ, d - delivery
        if( $name == 'delivery' ){
            echo CJSON::encode(array('r' => 'ok', 's' => ($result->sum() + $result->delivery), 'd'=>$result->delivery));
        }else{
            echo CJSON::encode(array('r' => 'ok', 's' => ($result->order->sum() + $result->order->delivery), 'd'=>$result->order->delivery));
        }

        Yii::app()->end();

    }

    /**
     * Завершаем AJAX запрос не удачей
     */
    private function ajaxError(){

        echo CJSON::encode( array( 'result' => 'error' ) );

        Yii::app()->end();

    }

    /**
     * Добавление продукта к заказу
     * Заказ может быть изменен только если он не закрыт, подтвержден и адресован компании
     *
     * @param $id - Номер заказа ( order id )
     */
    public function actionAddProduct( $id ){

        if( Orders::model()->notClosed()->notSend()->confirmed()->myVendor()->exists('id = :id', array(':id'=>(int)$id)) ){

            $model = Product::model()->published()->myVendor()->visible()->findAll();

            if( Yii::app()->request->getPost('products_ids') ){

                BondOrders::addProduct( $id, Yii::app()->request->getPost('products_ids') );

                $this->redirect($this->createUrl('/vendor_cp/orders/view',array('id'=>$id)));

            }

            $this->render('products', array(
                    'model'     => $model,
                    'orderId'   => $id
            ));

        }else
            $this->redirect('/vendor_cp/orders/index');
    }

    /**
     * @param $orderId
     * @param $status
     */
    public function actionChangeStatus( $orderId, $status ){

        Orders::changeStatus( $orderId, $status );

        $this->redirect($this->createUrl('/vendor_cp/orders/view', array('id'=>$orderId)));

    }

    /**
     * @return bool
     */
    public function actionChangeComment(){

        if( !Yii::app()->request->getPost('orderId') || !Yii::app()->request->getPost('vendor_comment')  ) return false;

        Orders::changeVendorComment((int)Yii::app()->request->getPost('orderId'), Yii::app()->request->getPost('vendor_comment'));

        $this->redirect($this->createUrl('/vendor_cp/orders/view', array('id'=>Yii::app()->request->getPost('orderId'))));

    }

    /**
     * @param $id
     */
    public function actionGetSum( $id ){

        $model = Orders::model()->myVendor->findByPk($id)->sum();

        echo $model;

        Yii::app()->end();

    }

    public function actionIndex(){
        $model = Orders::model()->myVendor()->confirmed()->findAll();

        $this->render('index', array(
                                    'model'=>$model
                               ));
    }

    public function actionView( $id ){

        $model = Orders::model()->myVendor()->confirmed()->findByPk($id);

        if(!$model) $this->redirect('/vendor_cp/orders/index');

        if( $model->status == Orders::STATUS_QUEUED ){

            $model->status = Orders::STATUS_TREATED;

            $model->save();
        }


        $this->render('view', array(
                                    'model'=>$model
                               ));
    }

}