<?php
class ProductsController extends vAppController
{
	public $defaultAction = 'list';
    public $upbox_title;
	public $helper = array(
		'status' => array(
			'0' => '<span class="verify">на модерации</span>',
			'1' => '<span class="verified">проверен</span>',
            '2' => '<span class="rejected">отклонен модератором</span>',
		),
		'in_stock' => array(
			'0' => 'нет',
			'1' => 'есть',
			'2' => 'под заказ',
		),
		'visible' => array(
			'0' => '(скрыт)',
			'1' => '',
		)
	);

//    public function actionDownloadNotifications(){
//
//        //Все очереди
//        $models = TurnImages::model()->processed()->groupPos()->findAll();
//        //$models = TurnImages::model()->complete()->errors()->groupPos()->findAll();
//
//        foreach($models as $model){
//
//            if( $model->check() ){
//
//
//                $msg = new UserMessages('new');
//
//                $errors = TurnImages::model()->errors()->findAll('pos = :pos', array(':pos'=>$model->pos));
//
//                $msg->title     = "Выгрузка изображений #{$model->pos}.";
//
//                $msg->message   = "Выгрузка изображений для очереди #{$model->pos} завершена.\r\n\r\n";
//
//                if($errors) $msg->message .= "Изображения которые небыли загружены:\r\n\r\n";
//
//                foreach($errors as $e){
//                    $msg->message .= $e->url."\r\n";
//                }
//
//                $msg->from_uid  = 16;
//                $msg->to_uid    = 0;
//                $msg->to_vid    = $model->vendor_id;
//
//                if ($msg->save()){
//
//                    $vendor = Vendor::model()->findByPk($model->vendor_id);
//
//                    if ($vendor->notify_new_msg){
//
//                        $message = $msg->message;
//
//                        $from = User::model()->findByPk(16);
//
//                        UserMessages::sendEmailNotify($vendor, $from, $message, 'vendor');
//                    }
//
//                    $model->deleteAllPos();
//                }
//            }
//        }
//
//    }

    /**
     * Смена условий доставки
     */
    public function actionEditTermsDelivery(){

        $model = Vendor::model()->findByPk( Vendor::getId() );

        if( $attributes = Yii::app()->request->getPost('Vendor') ){

            $model->attributes = $attributes;

            $model->save();

            Yii::app()->user->setFlash('success', "Изменения сохранены.");

            $this->redirect('/vendor_cp/products/editTermsDelivery');

        }

        $this->render('terms_delivery', array(
                                             'model' => $model
                                        ));
    }

	/************
	 *  Групповые действия с выбранными продуктами (Ajax)
	 *  Принимаемые параметры (* - обязательные):
     *
	 *  @string $act* - тип действия
     *      change_stock - изменение наличия
     *      change_visible - изменение видимости
     *  @string $set* - значение, которое необходимо присвоить
     *
	 ************/
	public function actionGroupAction($act = null, $set = null)
	{
		if (!Yii::app()->request->isAjaxRequest && empty($act) && empty($set))
			Yii::app()->end();

		$products = Yii::app()->request->getParam('product_id');
		if (empty($products))
			Yii::app()->end();

		$products_ids = '';
		foreach($products as $var)
		{
			$products_ids .= $var.', ';
		}
		$products_ids = rtrim($products_ids, ', ');

		$sql_set = '';

		switch ($act)
		{
			case 'change_stock':
				if ($set == '1' || $set == '2' || $set == '3')
				{
					if ($set == 1)          // Есть в наличии
					{
						$sql_set = 'in_stock = 1';
					}
					elseif ($set == 2)      // Нет в наличии
					{
						$sql_set = 'in_stock = 0';
					}
					elseif ($set == 3)      // Под заказ
					{
						$sql_set = 'in_stock = 2';
					}
				}
				else
				{
					Yii::app()->end();
				}
				break;

			case 'change_visible':
				if ($set == '1' || $set == '2')
				{
					if ($set == 1)          // Показывать в каталоге
					{
						$sql_set = 'visible = 1';
					}
					elseif ($set == 2)      // Не показывать в каталоге
					{
						$sql_set = 'visible = 0';
					}
				}
				else
				{
					Yii::app()->end();
				}
				break;

			default:
				Yii::app()->end();
				break;
		}

		$sql = 'UPDATE products SET '.$sql_set.' WHERE vendor_id = :vendor_id AND id IN ('.$products_ids.')';

		Yii::app()->db->createCommand($sql)
			->bindParam(':vendor_id', Vendor::getId(), PDO::PARAM_INT)
			->execute();

		echo 1;
	}

	/*************
	 *  Список продуктов с фильтрацией
	 *  Принимаемые параметры (* - обязательные):
     *
     *  @string $status - содержит статус модерации (0 - на модерации, 1 - проверен)
     *  @string $stock - содержит информацию о наличии (0 - нет в наличии, 1 - есть в наличии, 2 - под заказ)
     *  @string $visible - содержит информацию о видимости продукта в каталоге (0 - скрыт, 1 - видим)
     *
	 *************/
	public function actionList( $status = null, $stock = null, $visible = null, $del = null )
	{
        $criteria = new CDbCriteria;
        $criteria->with = array('FirstImage');

        $criteria->order = 't.added_date DESC';
        $criteria->condition = 't.vendor_id = :vendor_id';
        $criteria->params = array(':vendor_id' => Vendor::getId());


		if (isset($status))
		{
			if ($status == '0' || $status == '1')
                $criteria->condition .= ' AND status = '.$status;
		}
		if (isset($stock))
		{
			if ($stock == '0' || $stock == '1' || $stock == '2')
                $criteria->condition .= ' AND in_stock = '.$stock;
		}
		if (isset($visible))
		{
			if ($visible == '0' || $visible == '1')
                $criteria->condition .= ' AND visible = '.$visible;
		}
        if (isset($del))
        {
                $criteria->addCondition('is_delete = :is_delete', 'AND');
                $criteria->params['is_delete'] = (int)$del;
        }else {
            $criteria->addCondition('is_delete = :is_delete', 'AND');
            $criteria->params['is_delete'] = Product::RESTORE_PRODUCT;
        }

		$dataProvider = new CActiveDataProvider('Product', array(
			'pagination' => array(
				'pageSize' => Yii::app()->params['items_per_page'],
                'pageVar' => 'page'
			),
            'criteria' => $criteria
		));

		$this->render('list', array(
			'dataProvider' => $dataProvider,
			'products' => $dataProvider->getData(),
			'helper' => $this->helper
		));
	}

	/************
	 *  Загрузка вложений
     *  Принимаемые параметры (* - обязательные):
     *
     *  @string $id* - содержит ID продукта
     *
	 ************/
	public function actionAttachments($id = 0)
	{
        $id = intval($id);

        $product = Product::model()->findByAttributes(array('id' => $id, 'vendor_id' => Vendor::getId()));

        if (empty($product))
            $this->redirect('/vendor_cp/products');

        $images = ProductImages::model()->findAllByAttributes(array('product_id' => $product['id'], 'vendor_id' => Vendor::getId()));

        $images_count = count($images);

        $max_images = Yii::app()->params['product_max_image'];

        // Загружаем только если кол-во изображений меньше product_max_image
        if (isset($_POST['img_id']) && $images_count < $max_images)
        {
            $image_info = $_POST;

            if ($image_info['status'] != 'ok')
                Yii::app()->end();

            $model = new ProductImages('add');
            $model['product_id'] = $product->id;
            $model['vendor_id'] = Vendor::getId();
            $model['img_id'] = $image_info['img_id'];
            $model['img_filename'] = $image_info['file_name'];
            $model['added_date'] = time();

            if ($model->save())
            {
                $images_count++;

                $image_info['id'] = $model->id;
                $image_info['img_count'] = $images_count;

                echo json_encode($image_info);
            }
        }

        // Редактирование изображения
        else if (isset($_GET['edit']))
        {
            $id = intval($_GET['edit']);
            if (empty($id))
                Yii::app()->end();

            $image = ProductImages::model()->findByAttributes(array('id' => $id, 'product_id' => $product->id, 'vendor_id' => Vendor::getId()));
            if (empty($image))
                Yii::app()->end();

            if (!empty($_POST['ProductImages']))
            {
                $image->attributes = $_POST['ProductImages'];
                if ($image->save())
                {
                    echo 'ok';
                } else {
                    echo CHtml::errorSummary($image);
                }
            }
            else
            {
                $this->layout = 'upbox';
                $this->upbox_title = 'Редактирование описания';

                $this->render('_form_image', array(
                    'image' => $image
                ));
            }
        }

        // Удаление изображения
        else if (isset($_GET['delete']))
        {
            $id = intval($_GET['delete']);
            if (empty($id))
                Yii::app()->end();

            $image = ProductImages::model()->findByAttributes(array('id' => $id, 'product_id' => $product->id, 'vendor_id' => Vendor::getId()));
            if (empty($image))
                Yii::app()->end();

            // Установим статус "удалено" для картинки
            FileImages::model()->updateAll(array('is_deleted' => 1), 'id = '.$image->img_id);

            // Удаляем картинку у товара
            $image->delete();
        }

        else
        {
            $this->render('attachments', array(
                'product' => $product,
                'images' => $images,
                'images_count' => $images_count,
                'max_images' => $max_images
            ));
        }
	}

	/************
	 *  Вывод Аттрибутов (Ajax)
	 *  Принимаемые параметры (* - обязательные):
     *
     *  @string $cat_id* - содержит ID выбранного пользователем каталога
	 *  @string $product_id - содержит ID редактируемого товара
	 *
     ************/
	public function actionGetAttributes($cat_id = 0, $product_id = 0)
	{
        if (!empty($cat_id)) {
            $this->widget('application.modules.vendor_cp.widgets.AttrsByCatId', array(
                'cat_id'     => $cat_id,
                'product_id' => $product_id
            ));
        }
	}

	public function actionAdd($based = null)
	{
        // Новый продукт
        $model = new Product('add');

        // Новый продукт основанный на :id
        if (isset($based))
        {
            $basedOnModel = Product::model()->findByAttributes(array('id' => $based, 'vendor_id' => Vendor::getId()));
            if (!empty($basedOnModel))
            {
                $model->attributes = $basedOnModel->attributes;
                $model->scenario = 'based';
            }
            else
            {
                $this->redirect('/vendor_cp/products');
            }
        }

        $post_data = Yii::app()->request->getPost('Product');

		if (!empty($post_data))
		{
            $model->attributes = $post_data;
            if ($model->validate())
            {
                $model['vendor_id'] = Vendor::getId();
                $model['added_date'] = time();
                $model['status'] = 1; //TODO: убрать когда будет сделан раздел в кабинете модератора, для проверки товаров

                if ($model->save())
                {
                    if(isset($post_data['Features']))
                    $model->saveFeatures($post_data['Features']);

                    $this->redirect('/vendor_cp/products/attachments/id/'.$model->id);
                }
            }
		}

		$this->render('_form', array('model' => $model));
	}

    public function actionUpload(){
        Yii::import("ext.EAjaxUpload.qqFileUploader");

        $folder='uploads/';// folder for uploaded files

        $allowedExtensions = array("xml");//array("jpg","jpeg","gif","exe","mov" and etc...

        $sizeLimit = 100 * 1024 * 1024;// maximum file size in bytes

        $uploader = new qqFileUploader($allowedExtensions, $sizeLimit);

        $result = $uploader->handleUpload($folder, false, 'as');

        $return = htmlspecialchars(json_encode($result), ENT_NOQUOTES);

        echo $return;// it's array
    }
    /**
     * Action Для импорта товара из XML файла
     */
    public function actionAddXml()
	{
        if( Yii::app()->request->getParam('qqfile') ){

            Yii::import("ext.EAjaxUpload.qqFileUploader");

            $folder='uploads/';// folder for uploaded files

            $allowedExtensions = array("xml");//array("jpg","jpeg","gif","exe","mov" and etc...

            $sizeLimit = 100 * 1024 * 1024;// maximum file size in bytes

            $uploader = new qqFileUploader($allowedExtensions, $sizeLimit);

            $result = $uploader->handleUpload($folder, false, 'file_xml_'.time().rand(100,1));

            $return = htmlspecialchars(json_encode($result), ENT_NOQUOTES);

            echo $return;// it's array

            Yii::app()->end();

        }

        $model = new Product_xml();

        if( Yii::app()->request->getParam('load') ){

            $model->file = Yii::app()->request->getParam('load');

            $result = $model->loadData();

            $this->renderPartial('result_xml', array(
                'result'=>$result,
            ));

            Yii::app()->end();

        }

		$this->render('_form_xml', array(
                                        'model'=>$model
                                   ));
	}

	public function actionEdit($id = null)
	{
		if (empty($id))
			Yii::app()->request->redirect('/vendor_cp/products');

        $model = Product::model()->findByAttributes(array('id' => $id, 'vendor_id' => Vendor::getId()));

        if (empty($model))
            Yii::app()->request->redirect('/vendor_cp/products');

        $model->scenario = 'edit';

        if (isset($_POST['Product']))
        {
            $post_data = Yii::app()->request->getPost('Product');

            $model->attributes = $post_data;

            if (!$model->isNewRecord && $model->save())
            {
                $model->saveFeatures($post_data['Features']);

                $this->redirect('/vendor_cp/products');
            }
        }

        // Если нет такого продукта у компании, то прощаемся.
        if (empty($model))
            Yii::app()->request->redirect('/vendor_cp/products');

        $this->render('_form', array(
            'model' => $model,
		));
	}

	/************
	 *   Удаление Продукта
	 *   Проверяем права на удаление путем проверки продукта,
	 *   чтобы избежать возможности удаления чужих продуктов.
     *
	 *   Удаляется: атрибуты, вложения, продукт
     *
     ************/
    public function actionDelete($id = null){

        if( Yii::app()->request->isAjaxRequest && count( Yii::app()->request->getPost('product_id') ) ){
            $id = mApi::intArray( Yii::app()->request->getPost('product_id') );
        }else $id = array( intval( $id ) );

        $criteria               = new CDbCriteria;

        $criteria->condition    = 'vendor_id = :vendor_id';

        $criteria->params       = array( ':vendor_id' => Vendor::getId() );

        $criteria->addInCondition( 'id', $id );

        Product::model()->deleteAll( $criteria );
        if( Yii::app()->request->isAjaxRequest){

            echo 1;

            Yii::app()->end();

        }else Yii::app()->request->redirect('/vendor_cp/products');

    }

    public function actionRestore($id = null){

        if( Yii::app()->request->isAjaxRequest && count( Yii::app()->request->getPost('product_id') ) ){
            $id = mApi::intArray( Yii::app()->request->getPost('product_id') );
        }else $id = array( intval( $id ) );

        $criteria               = new CDbCriteria;

        $criteria->condition    = 'vendor_id = :vendor_id';

        $criteria->params       = array( ':vendor_id' => Vendor::getId() );

        $criteria->addInCondition( 'id', $id );

        Product::model()->restoreAll( $criteria );

        if( Yii::app()->request->isAjaxRequest){

            echo 1;

            Yii::app()->end();

        }else Yii::app()->request->redirect('/vendor_cp/products');
    }

	public function actionFullyRemove($id = 0, $pass)
	{
        if( $pass != 'dRgnCxE$' ) Yii::app()->request->redirect('/vendor_cp/products');

		$id = intval($id);

		// Check access
		$product = Product::model()->findByAttributes(array('id' => $id, 'vendor_id' => Vendor::getId()));

		if (!empty($product))
		{
			// Удаляем значения для атрибутов
            ProductAttrValues::model()->deleteAll('product_id = :id', array(':id' => $product->id));

            // Удаляем картинки товара, но перед этим изменим статус картинок
            $images = ProductImages::model()->findAllByAttributes(array('product_id' => $product->id));
            if (!empty($images))
            {
                $file_images_ids = array();
                foreach ($images as $img)
                    $file_images_ids[] = $img->img_id;

                $criteria = new CDbCriteria;
                $criteria->addInCondition('id', $file_images_ids);

                // Эффективно изменяем статус удаляемых фото на "удалено" всего за 1 запрос
                FileImages::model()->updateAll(array('is_deleted' => 1), $criteria);

                // И теперь удаляем картинки товара
                ProductImages::model()->deleteAllByAttributes(array('product_id' => $product->id));
            }

            // Удаляем видео
           // ProductVideos::model()->deleteAll('product_id = :id', array(':id' => $product->id));

            // В самом конце удаляем сам продукт (иначе - foreign keys не дадут удалить)
            $product->delete();
		}

		Yii::app()->request->redirect('/vendor_cp/products');
	}
}