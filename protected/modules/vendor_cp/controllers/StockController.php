<?php
class StockController extends vAppController
{

    public function actionSendingEndStock()
    {
        $sendTime    = '01:00';
        $currentDate = date('Y-m-d');
        $yestDate    = strtotime($currentDate) - 60 * 60 * 24;

        echo strtotime($currentDate);

        $criteria            = new CDbCriteria;
        $criteria->condition = 'date_to = :yestDate';
        $criteria->params    = array(
            ':yestDate' => $yestDate
        );

        $vendorStock = VendorStock::model()->with('Vendor')->findAll($criteria);
        $res = count($vendorStock);
        if (count($vendorStock)) {
            $email = array();
            foreach ($vendorStock as $item) {
                $email[] = $item->Vendor->email;
            }

            $oMail = new UniSenderApi( Yii::app()->params['uni_sender_key'] );

            $res   = $oMail->createCampaign(
                array(
                     'message_id' => Yii::app()->params['uni_sender_message_id'],
                     'contacts'   => implode(',', $email),
                     'start_time' => $currentDate.' '.$sendTime,
                )
            );
        }

        echo  '<hr>ss'. $res;
        Yii::app()->end();
    }

    public function actionIndex()
    {
        $vendor =  Vendor::getInfo();

        $criteria=new CDbCriteria;
        $criteria->condition= 'vendor_id = :vendor_id AND date_to>:date_to';
        $criteria->params=array(':date_to'=>time(), ':vendor_id' => Vendor::getId());

        $dataProvider = new CActiveDataProvider('VendorStock', array(
            'pagination' => array(
                'pageSize' => Yii::app()->params['items_per_page'],
                'pageVar' => 'page'
            ),
            'criteria' => $criteria
        ));

        $criteriaOld=new CDbCriteria;
        $criteriaOld->condition='vendor_id = :vendor_id AND date_to<:date_to';
        $criteriaOld->params=array(':date_to'=>time(), ':vendor_id' => Vendor::getId());

        $dataProviderOld = new CActiveDataProvider('VendorStock', array(
            'pagination' => array(
                'pageSize' => Yii::app()->params['items_per_page'],
                'pageVar' => 'page'
            ),
            'criteria' => $criteriaOld
        ));

        $this->render('index', array(
            'vendor' => $vendor,
            'dataProvider' => $dataProvider,
            'posts' => $dataProvider->getData(),
            'posts_count' => $dataProvider->getTotalItemCount(),
            'dataProviderOld' => $dataProviderOld,
            'postsOld' => $dataProviderOld->getData(),
            'posts_countOld' => $dataProviderOld->getTotalItemCount()
        ));
    }

    public function actionAdd()
    {
        $error = array();
        $criteria=new CDbCriteria;
        $criteria->condition= 'vendor_id = :vendor_id AND date_to>:date_to';
        $criteria->params=array(':date_to'=>time(), ':vendor_id' => Vendor::getId());
        $posts = VendorStock::model()->findAll($criteria);
        if (!empty($posts)){
            $this->redirect(Yii::app()->createUrl('vendor_cp/stock'));
        }

        $model = new VendorStock();

        if (isset($_POST['VendorStock']))
        {
            $model['vendor_id'] = Vendor::getId();

            if(isset($_POST['VendorStock']['date_from'])){
                $date_from = explode("-",$_POST['VendorStock']['date_from']);
                if(!isset($date_from[1]) || $date_from[0]>31 || $date_from[1]>12  || $date_from[2]>2019 ){
                    $error['date_from'] = "Введите корректную дату старта, например: ".date("d-m-Y",time());
                    $_POST['VendorStock']['date_from'] = time();
                }else{
                    $_POST['VendorStock']['date_from'] = mktime(0, 0, 0, $date_from[1], $date_from[0], $date_from[2]);
                    $model['date_from'] = $_POST['VendorStock']['date_from'];
                }
            }

            if(isset($_POST['VendorStock']['date_to'])){
                $date_to = explode("-",$_POST['VendorStock']['date_to']);

                if (!isset($date_to[1]) || $date_to[0]>31 || $date_to[1]>12  || $date_to[2]>2019 ) {
                    $error['date_to'] = "Введите корректную дату окончания, например: " . date("d-m-Y", $_POST['VendorStock']['date_from'] + 86400);
                    $_POST['VendorStock']['date_to'] = time();
                } elseif ($_POST['VendorStock']['date_from'] >= mktime(22, 0, 0, $date_to[1], $date_to[0], $date_to[2])) {
                    $error['date_to'] = "Введите корректную дату окончания (должна быть больше даты старта)";
                    $_POST['VendorStock']['date_to'] =  $_POST['VendorStock']['date_from'] + 86400;
                } else {
                    $_POST['VendorStock']['date_to'] = mktime(22, 0, 0, $date_to[1], $date_to[0], $date_to[2]);
                    $model['date_to'] = $_POST['VendorStock']['date_to'];
                }
            }
			
			if ($model['date_to']-$model['date_from']>2678400){
				$error['date_to'] = "Максимальная продолжительность акции - 1 месяц.";
			}

            $model->attributes = $_POST['VendorStock'];

            if(empty($error)){
                if ($model->save())
                $this->redirect(Yii::app()->createUrl('vendor_cp/stock'));
            }
        }

        $this->render('_form', array(
            'model' => $model,
            'error' => $error,
        ));
    }

    public function actionEdit($id = 0)
    {
        $error = array();
        $id = intval($id);
        if (empty($id))
            $this->redirect(Yii::app()->createUrl('vendor_cp/stock'));

        $model = VendorStock::model()->findByAttributes(array('stock_id' => $id));

        if ($model['date_to'] < time()){
            $this->redirect(Yii::app()->createUrl('vendor_cp/stock'));
        }

        if (isset($_POST['VendorStock']))
        {
            $model['vendor_id'] = Vendor::getId();
            if(isset($_POST['VendorStock']['date_from'])){
                $date_from = explode("-",$_POST['VendorStock']['date_from']);
                if(!isset($date_from[1]) || $date_from[0]>31 || $date_from[1]>12  || $date_from[2]>2019 ){
                    $error['date_from'] = "Введите корректную дату старта, например: ".date("d-m-Y",time());
                    $_POST['VendorStock']['date_from'] = time();
                }else{
                    $_POST['VendorStock']['date_from'] = mktime(0, 0, 0, $date_from[1], $date_from[0], $date_from[2]);
                    $model['date_from'] = $_POST['VendorStock']['date_from'];
                }
            }

            if(isset($_POST['VendorStock']['date_to'])){
                $date_to = explode("-",$_POST['VendorStock']['date_to']);

                if (!isset($date_to[1]) || $date_to[0]>31 || $date_to[1]>12  || $date_to[2]>2019 ) {
                    $error['date_to'] = "Введите корректную дату окончания, например: " . date("d-m-Y", $_POST['VendorStock']['date_from'] + 86400);
                    $_POST['VendorStock']['date_to'] = time();
                } elseif ($_POST['VendorStock']['date_from'] >= mktime(22, 0, 0, $date_to[1], $date_to[0], $date_to[2])) {
                    $error['date_to'] = "Введите корректную дату окончания (должна быть больше даты старта)";
                    $_POST['VendorStock']['date_to'] =  $_POST['VendorStock']['date_from'] + 86400;
                } else {
                    $_POST['VendorStock']['date_to'] = mktime(22, 0, 0, $date_to[1], $date_to[0], $date_to[2]);
                    $model['date_to'] = $_POST['VendorStock']['date_to'];
                }
            }

            $model->attributes = $_POST['VendorStock'];
            if(empty($error)){
            if ($model->save())
                $this->redirect(Yii::app()->createUrl('vendor_cp/stock'));
            }
        }


        $this->render('_form', array(
            'model' => $model,
            'error' => $error,
        ));
    }
}