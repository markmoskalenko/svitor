<?php $product_edit_url = Yii::app()->createUrl('vendor_cp/products/edit', array('id' => $product->id)); ?>
<h1>
    <a href="<?php echo Yii::app()->createUrl('products'); ?>">Товары</a>
    &rarr; &laquo;<a href="<?php echo $product_edit_url; ?>"><?php echo mApi::cutStr($product['title'], 20); ?></a>&raquo;
    &rarr; Изображения <a href="<?php echo $product_edit_url; ?>" class="head_tab">Основная информация</a>
</h1>

    <div class="b-main_tabs_title">Изображения <span id="images_counter"><span><?php echo $images_count; ?></span> / <?php echo $max_images; ?></span></div>
    <div class="b-main_tabs_box">
        <div class="add_attach_button" id="addImgButton" <?php echo $images_count >= $max_images ? 'style="display: none;"' : ''; ?>>
            URL адрес изображения <input type="text" id="urlImage"/>
            <div class="button_main">
                <button id="add_img_url">Загрузить</button>
            </div><br>
            <a id="add_img">+ Добавить изображение с компьютера</a>
            <span id="loader"></span>
        </div>
        <div class="b-attach-thumbs" id="images_thumbs">
            <?php
            if (!empty($images))
                foreach ($images as $img)
                {
                    echo '<div class="img" id="img_'.$img['id'].'">
                        <a class="upbox-button" rel="upbox-images" href="'.Yii::app()->ImgManager->getUrlById($img->img_id, 'large', $img->img_filename).'" title="' . $img->description . '">
                        <img src="'.Yii::app()->ImgManager->getUrlById($img->img_id, 'medium', $img->img_filename).'"></a>
                        <a class="delete magic_button" onclick="attachments.remove(this, '.$img['id'].'); return false;" onmouseover="tooltip.show(this, \'Удалить\', {position:\'top-right\'});"></a>
                        <br><a onclick="attachments.edit(' . $img->id . '); return false;">редактировать</a>
                        </div>';
                }
            ?>
        </div>
    </div>

    <div class="b-vendor-default_form">
        <div class="footer" style="margin-top: 30px; margin-bottom: 10px;">
            <span id="save_loader"></span>
            <div class="button_main">
                <button onclick="location.assign('<?php echo Yii::app()->createUrl('vendor_cp/products'); ?>');">Готово</button>
            </div>
            или <a href="<?php echo Yii::app()->createUrl('vendor_cp/products/edit', array('id' => $product['id'])); ?>">вернуться к основной информации</a>
        </div>
    </div>
<script>
$(document).ready(function () {
    attachments.init(<?php echo $product['id']; ?>, <?php echo $images_count; ?>, <?php echo $max_images; ?>);
    attachments.uploadImg($('#add_img'));
    attachments.uploadImgUrl($('#add_img_url'));
});
</script>
<link rel="stylesheet" type="text/css" href="/static/fancybox/helpers/jquery.fancybox-buttons.css?<?php echo Yii::app()->params['version']['css']; ?>">
<script type="text/javascript" src="/static/fancybox/helpers/jquery.fancybox-buttons.js?<?php echo Yii::app()->params['version']['js']; ?>"></script>


<script src="/static/js/ajaxupload.js?<?php echo Yii::app()->params['version']['js']; ?>"></script>
<script src="/static/vendor/js/v_product_attachments.js?<?php echo Yii::app()->params['version']['js']; ?>"></script>
