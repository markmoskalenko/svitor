<div class="b-main_tabs">
    <ul class="tabs_button">
        <li><?php echo CHtml::link('Список товаров', $this->createUrl('/vendor_cp/products')); ?></li>
        <li class="active"><?php echo CHtml::link('Условия доставки', $this->createUrl('/vendor_cp/products/EditTermsDelivery')); ?></li>
    </ul>
</div>

<h1>Условия доставки</h1>
<div class="main_content">
    <?php
    foreach(Yii::app()->user->getFlashes() as $key => $message) {
        echo '<span class="msg_success">' . $message . "</span>\n";
    }
    ?>
	<p class="infotext">Здесь Вы можете указать Ваши условия доставки товаров.</p>
    <div class="form">
    <?php $form=$this->beginWidget('CActiveForm', array(
                                                       'id'=>'company-form',
                                                       'enableAjaxValidation'=>false,
                                                  )); ?>
    <?php echo $form->errorSummary($model); ?>

    <div class="row">
        <?php echo $form->textArea($model,'terms_delivery',array('rows'=>12,'style'=>'width:100%;')); ?>
        <?php echo $form->error($model,'terms_delivery'); ?>
    </div>
	<br/>
    <div class="row">
        <div class="button_main"><?php echo CHtml::submitButton('Сохранить', array('type'=>'button','class'=>'button')); ?></div>
    </div>
<br/>
    <?php $this->endWidget(); ?>
    </div>
</div>
