<style type="text/css">
    .progress {display: none;}
    .progress #load-file{display: none;}
    .progress .progress-bar{width: 300px; height: 20px; margin: 10px 0;}
    .progress .progress-bar .ui-widget-header{background: url("/pbar-ani.gif");}
    .progress p{margin: 0 0 10px 0; font-weight: bold;}
</style>

<h1>
        <a href="<?php echo Yii::app()->createUrl('vendor_cp/products'); ?>">Товары</a> &rarr; Импорт из файла *.xml
</h1>

<div class="progress">
    <p>Загрузка файла на сервер.</p>
    <div id="progressbar" class="progress-bar"></div>

    <div id="load-file">
        <p>Импорт товаров на сайт.</p>
        <div id="progressbar-import" class="progress-bar"></div>
    </div>
    <div class="clear"></div>
</div>

<script>
    var totalSize = 0;
    var totalComplete = 0;
    var progress = function( ){
        totalComplete = totalComplete + 8000;
        var p = (( totalComplete * 100 ) / totalSize);
        $( "#progressbar-import" ).progressbar({
            value: p
        });
    }

    $(function() {
        $( "#progressbar" ).progressbar({
            value: 0
        });
        $( "#progressbar-import" ).progressbar({
            value: 0
        });
    });
</script>
<div style="margin-bottom: 10px">
<b>Внимание!</b> Импорт изображений - это длительный процесс и происходит в фоновом режиме.
<br>Отображение изображений для товаров происходит постепенно и полная загрузка может затянуться до 5 часов.
</div>
<?php $this->widget('ext.EAjaxUpload.EAjaxUpload',
    array(
         'id'=>'uploadFile',
         'config'=>array(
             'action'=>Yii::app()->createUrl('/vendor_cp/products/AddXml'),
             'allowedExtensions'=>array("xml"),
             'sizeLimit'=>100*1024*1024,
             'minSizeLimit'=>1,
             'multiple'=>false,
             'onSubmit'=>"js:function(id, fileName){
                $('#uploadFile').fadeOut(100,function(){
                    $('.progress').fadeIn(300);
                });
             }",
             'onComplete'=>"js:function(id, fileName, responseJSON){
                 console.log(totalSize);
                 if( responseJSON.success == true ){
                    $('#load-file').fadeIn(300);
                    setInterval('progress()', 1000)

                    $.ajax({
                      url: '/vendor_cp/products/AddXml',
                      type: 'POST',
                      data: { 'load' : responseJSON.filename },
                      success: function(data){
                      $('.b-content').fadeOut(200,function(){
                            $('.b-content').html(data);
                        });
                        $('.b-content').fadeIn(200);
                      }
                    });
                     $.ajax({
                      url: '/vendor_cp/products',
                      type: 'POST',
                    });
                 }
             }",
             'onProgress'=>"js:function(id, fileName, loaded, total){
                totalSize = total;
                var p = ( loaded * 100 ) /  total;
                $( '#progressbar' ).progressbar({
                    value: p
                });
                if( p == 100 ){
                    $('#progressbar .ui-widget-header').css({'background':'url(\"/pg-stop.jpg\")'});
                }
             }"
         )
    )); ?>