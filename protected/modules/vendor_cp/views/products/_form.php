<h1><a href="<?php echo Yii::app()->createUrl('vendor_cp/products'); ?>">Товары</a>
    <?php if (!$model->isNewRecord): ?>
        &rarr; редактирование &laquo;<?php echo mApi::cutStr($model['title'], 20); ?>&raquo;
        <a href="<?php echo Yii::app()->createUrl('vendor_cp/products/attachments', array('id' => $model->id)); ?>" class="head_tab">Изображения</a>
    <?php else: ?>
        &rarr;
        <?php if ($model->getScenario() == 'based'): ?>
            новый товар, основанный на &laquo;<a href="<?php echo Yii::app()->createUrl('vendor_cp/products/edit', array('id' => $_GET['based'])); ?>"><?php echo mApi::cutStr($model->title, 50); ?></a>&raquo;
         <?php else: ?>
            новый товар
         <?php endif; ?>
    <?php endif; ?>
</h1>

<div class="b-vendor-default_form main_content">
<?php echo CHtml::beginForm(); ?>
    <table>
        <tr>
            <td class="left top"><span class="red_text">*</span> Название:</td>
            <td class="right">
                <?php
                $model->title = CHtml::decode($model->title);
                echo CHtml::activeTextField($model, 'title', array(
                    'style' => 'width: 385px;'
                ));
                ?>
                <?php echo CHtml::error($model, 'title'); ?>
            </td>
        </tr>
        <tr>
            <td class="left top"><span class="red_text">*</span> ID (артикул):</td>
            <td class="right">
                <?php
                $model->external_id = CHtml::decode($model->external_id);
                echo CHtml::activeTextField($model, 'external_id', array(
                    'style' => 'width: 385px;'
                ));
                ?>
                <?php echo CHtml::error($model, 'external_id'); ?>
            </td>
        </tr>
        <tr id="categories">
            <td class="left top"><span class="red_text">*</span> <label for="category_options">Раздел каталога:</label></td>
            <td class="right">
                <select name="Product[cat_id]" id="category_options" onchange="productEditor.loadAttributes($(this).val());">
                    <?php $this->widget('application.modules.vendor_cp.widgets.AllCats', array('model' => $model,'type'=>'products')); ?>
                </select>
                <span id="cat_loader"></span>
                <?php echo CHtml::error($model, 'cat_id'); ?>
            </td>
        </tr>
        <tr>
            <td class="left top"></td>
            <td class="right">
                <label><?php echo CHtml::activeCheckbox($model, 'visible'); ?> показывать в каталоге<label>
                <?php echo CHtml::error($model, 'visible'); ?>
            </td>
        </tr>
        <tr>
            <td class="left top"></td>
            <td class="right">
                <label><?php echo CHtml::activeCheckbox($model, 'is_delete'); ?> удален<label>
                        <?php echo CHtml::error($model, 'is_delete'); ?>
            </td>
        </tr>
        <tr id="stock">
            <td class="left top">Наличие:</td>
            <td class="right">
                <?php
                echo CHtml::activeDropDownList($model, 'in_stock', array(
                        '1' => 'есть в наличии',
                        '0' => 'нет в наличии',
                        '2' => 'под заказ',
                    ));
                ?>
                <?php echo CHtml::error($model, 'in_stock'); ?>
            </td>
        </tr>
        <tr>
            <td class="left top"><span class="red_text">*</span> Цена:</td>
            <td class="right">
                <?php
                echo CHtml::activeTextField($model, 'price', array(
                    'style' => 'width: 100px;'
                ));
                ?> руб.
                <?php echo CHtml::error($model, 'price'); ?>
            </td>
        </tr>
        <tr>
            <td class="left" valign="top">
                <span class="red_text">*</span> Описание
            </td>
            <td class="right" id="decription">
                <?php
                $model->description = CHtml::decode($model->description);
                echo CHtml::activeTextArea($model, 'description', array(
                    'style' => 'height: 100px; width: 750px;',
                    'onkeyup' => 'formHelper.updateCounter(this, "decription", 1000);',
                    'onchange' => 'formHelper.updateCounter(this, "decription", 1000);',
                    'onclick' => 'formHelper.updateCounter(this, "decription", 1000);'
                ));
                ?>
                <span class="note"><span class="counter"></span></span>
                <?php echo CHtml::error($model, 'description'); ?>
            </td>
        </tr>
    </table>

    <div id="attributes" style="<?php echo empty($model->cat_id) ? 'display: none;' : ''; ?>margin-top: 15px;">
        <div id="attributes_data">
            <?php $this->actionGetAttributes($model->cat_id, $model->id); ?>
        </div>
    </div>

    <div class="footer" style="margin-top: 15px;">
        <span id="save_loader"></span>
        <div class="button_main">
                <button type="submit"><?php echo ($model->getScenario() == 'edit') ? 'Сохранить' : 'Продолжить'; ?></button>
        </div>
        или <a href="<?php echo Yii::app()->createUrl('vendor_cp/products'); ?>">Вернуться назад</a>
    </div>
    <?php echo CHtml::endForm(); ?>
</div>
<script src="/static/vendor/js/v_product_editor.js?<?php echo Yii::app()->params['version']['js']; ?>"></script>