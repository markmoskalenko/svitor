<style type="text/css">
    .b-vendor-default_form .left{
        width: 207px;
    }
</style>
<h1>
    <a href="<?php echo Yii::app()->createUrl('vendor_cp/products'); ?>">Товары</a> &rarr; Страница результата импортирования каталога товаров.
</h1>

<div class="b-vendor-default_form main_content">
    <table>
        <tbody>
        <tr>
            <td class="left top">Количество товаров к импорту:</td>
            <td class="right">
                <div style="margin-top: 5px;">
                    <?php echo $result['count']; ?>
                </div>
            </td>
        </tr>
        <tr>
            <td class="left top">Количество удачных импортов:</td>
            <td class="right">
                <div style="margin-top: 5px;">
                    <?php echo $result['success']; ?>
                </div>
            </td>
        </tr>
        <tr>
            <td class="left top">Количество не удачных импортов:</td>
            <td class="right">
                <div style="margin-top: 5px;">
                    <?php echo $result['error']['count']; ?>
                </div>
            </td>
        </tr>
        </tbody>
    </table>
    <?php if( is_array($result['error']['model']) && count($result['error']['model']) ): ?>
        <?php foreach( $result['error']['model'] as $model ): ?>
            <br>
            <hr>
            <br>
            Товар с ID - <b><?php echo (empty($model->external_id)) ? 'НЕ УКАЗАНО' : $model->external_id; ?></b>
            <table class="xml_error_table list_table">
            <tr>
                <td class="head l">Поле</td>
                <td class="head">Валидность</td>
            </tr>
            <tr>
                <td>name (<?php echo $model->getAttributeLabel('name'); ?>)</td>

                <td class="<?php echo ($model->getError('title')) ? 'error' : 'success';?>">
                    <?php if( $model->getError('title')): ?>
                        <?php echo $model->getError('title'); ?>
                    <?php else: ?>
                    Да
                    <?php endif; ?>
                </td>
            </tr>
            <tr>
                <td>categoryId (<?php echo $model->getAttributeLabel('categoryId'); ?>)</td>
                <td class="<?php echo ($model->getError('cat_id')) ? 'error' : 'success';?>">
                    <?php if( $model->getError('cat_id')): ?>
                        <?php echo $model->getError('cat_id'); ?>
                    <?php else: ?>
                        Да
                    <?php endif; ?>
                </td>
            </tr>
            <tr>
                <td>price (<?php echo $model->getAttributeLabel('price'); ?>)</td>
                <td class="<?php echo ($model->getError('price')) ? 'error' : 'success';?>">
                    <?php if( $model->getError('price')): ?>
                        <?php echo $model->getError('price'); ?>
                    <?php else: ?>
                        Да
                    <?php endif; ?>
                </td>
            </tr>
            <tr>
                <td>description (<?php echo $model->getAttributeLabel('description'); ?>)</td>
                <td class="<?php echo ($model->getError('description')) ? 'error' : 'success';?>">
                    <?php if( $model->getError('description')): ?>
                        <?php echo $model->getError('description'); ?>
                    <?php else: ?>
                        <?php if( empty($model->description) ): ?>
                            <span style="color:#E4EE00;">Нет</span>
                        <?php else: ?>
                            Да
                        <?php endif; ?>
                    <?php endif; ?>
                </td>
            </tr>
            <tr>
                <td>available (<?php echo $model->getAttributeLabel('available'); ?>)</td>
                <td class="<?php echo ($model->getError('in_stock')) ? 'error' : 'success';?>">
                    <?php if( $model->getError('in_stock')): ?>
                        <?php echo $model->getError('in_stock'); ?>
                    <?php else: ?>
                        <?php if( empty($model->description) ): ?>
                            <span style="color:#E4EE00;">Наличие не указано, автоматически установлено в значение «true»</span>
                        <?php else: ?>
                            Да
                        <?php endif; ?>
                    <?php endif; ?>
                </td>
            </tr>
            <tr>
                <td>currencyId (<?php echo $model->getAttributeLabel('currencyId'); ?>)</td>
                <td class="<?php echo ($model->getError('currencyId')) ? 'error' : 'success';?>">
                    <?php if( $model->getError('currencyId')): ?>
                        <?php echo $model->getError('currencyId'); ?>
                    <?php else: ?>
                        Да
                    <?php endif; ?>
                </td>
            </tr>
            <tr>
                <td>picture (<?php echo $model->getAttributeLabel('picture'); ?>)</td>
                <td class="<?php echo ($model->getError('picture')) ? 'error' : 'success';?>">
                    <?php if( $model->getError('picture')): ?>
                        <?php echo $model->getError('picture'); ?>
                    <?php else: ?>
                        Да
                    <?php endif; ?>
                </td>
            </tr>
            <tr>
                <td>id (<?php echo $model->getAttributeLabel('id'); ?>)</td>
                <td class="<?php echo ($model->getError('external_id')) ? 'error' : 'success';?>">
                    <?php if( $model->getError('external_id')): ?>
                        <?php echo $model->getError('external_id'); ?>
                    <?php else: ?>
                        Да
                    <?php endif; ?>
                </td>
            </tr>
            <tr>
                <td>country_of_origin (<?php echo $model->getAttributeLabel('country_of_origin'); ?>)</td>
                <td class="<?php echo ($model->getError('country_of_origin')) ? 'error' : 'success';?>">
                    <?php if( $model->getError('country_of_origin')): ?>
                        <?php echo $model->getError('country_of_origin'); ?>
                    <?php else: ?>
                        <?php if( empty($model->description) ): ?>
                            <span style="color:#E4EE00;">Нет</span>
                        <?php else: ?>
                            Да
                        <?php endif; ?>
                    <?php endif; ?>
                </td>
            </tr>
            </table>
        <?php endforeach; ?>
    <?php endif; ?>
    <div style="margin: 10px 0;">Можете продолжать работу с сайтом. Мы Вас уведомим об окончании импорта изображений.</div>

    <div class="cart button_main">
        <a class="button" href="/vendor_cp/products">В каталог</a>
    </div>
</div>