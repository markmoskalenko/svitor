<!--<div class="b-main_tabs">-->
<!--    <ul class="tabs_button">-->
<!--        <li class="active">--><?php //echo CHtml::link('Список товаров', $this->createUrl('/vendor_cp/products')); ?><!--</li>-->
<!--        <li >--><?php //echo CHtml::link('Условия доставки', $this->createUrl('/vendor_cp/products/editTermsDelivery')); ?><!--</li>-->
<!--    </ul>-->
<!--</div>-->

<h1>Товары</h1>
<div class="main_content">
	<p class="infotext">Если у вас есть товары - добавьте.</p>
    <div class="button_main">
        <button onclick="location.assign('<?php echo Yii::app()->createUrl('vendor_cp/products/add'); ?>');">Добавить</button>
    </div>
    <div class="button_main">
        <button onclick="location.assign('<?php echo Yii::app()->createUrl('vendor_cp/products/addXml'); ?>');">Добавить в виде XML</button>
    </div>

    <div class="b-products-filter">
        <b>Фильтр:</b>
        <ul>
            <li>Наличие
                <div>
                    <ul>
                        <li><a href="?stock=0">Нет</a></li>
                        <li><a href="?stock=1">Есть</a></li>
                        <li><a href="?stock=2">Под заказ</a></li>
                    </ul>
                </div>
            </li>
            <li>Статус
                <div>
                    <ul>
                        <li><a href="?status=1">Проверенные</a></li>
                        <li><a href="?status=0">На модерации</a></li>
                        <li><a href="?visible=0">Скрытые</a></li>
                        <li><a href="?visible=1">Видимые</a></li>
                    </ul>
                </div>
            </li>
            <?php echo CHtml::link('Активные',  Yii::app()->createUrl('/vendor_cp/products/list', array('del'=>Product::RESTORE_PRODUCT))); ?> |
            <?php echo CHtml::link('Удаленные', Yii::app()->createUrl('/vendor_cp/products/list', array('del'=>Product::DELETE_PRODUCT))); ?>
        </ul>
        <?php
        if (isset($_GET['type']) || isset($_GET['stock']) || isset($_GET['status']) || isset($_GET['visible']) || isset($_GET['del']))
        {
            if (isset($_GET['page']))
            {
                echo '<a href="' . Yii::app()->createUrl('vendor_cp/products/list', array('page' => intval($_GET['page']))) . '" class="reset">&larr; сбросить</a>';
            }
            else
            {
                echo '<a href="' . Yii::app()->createUrl('vendor_cp/products') . '" class="reset">&larr; сбросить</a>';
            }
        }
        ?>
    </div>

    <div class="b-products-groupaction" id="group_actions">
        <b class="selected_counter"></b>
        отмеченные
        <select name="change_stock" onchange="productsList.groupAction(this)">
            <option value="" style="color: #888888 !important;">Изменить наличие</option>
            <option value="1">- в наличии</option>
            <option value="2">- нет в наличии</option>
            <option value="3">- под заказ</option>
        </select>
        <select name="change_visible" onchange="productsList.groupAction(this)">
            <option value="" style="color: #888888 !important;">Изменить видимость</option>
            <option value="1">- показывать в каталоге</option>
            <option value="2">- не показывать в каталоге</option>
        </select>
        <select name="change_del" onchange="productsList.groupAction(this)">
            <option value="" style="color: #888888 !important;">Восстановить / Удалить</option>
            <option value="<?php echo Product::RESTORE_PRODUCT; ?>">- Восстановить</option>
            <option value="<?php echo Product::DELETE_PRODUCT; ?>">- Удалить</option>
        </select>
        <span id="group_action_msg"></span>
    </div>

    <div id="delete-dialog" style="display: none;" title="Удалить товар">Товар будет удален с возможностью восстановления.<br><br>Вы действительно хотите продолжить?</div>
    <div id="restore-dialog" style="display: none;" title="Восстановить товар">Товар будет восстановлен и доступен на ветрине сайта.<br><br>Вы действительно хотите продолжить?</div>

    <table class="b-vendor_products list_table" id="products_list">
        <thead>
            <th><input type="checkbox" name="select_all" onclick="productsList.selectAll(this)"></th>
            <th width="35px"></th>
            <th class="al_l"><b>ID (артикул)</b></th>
            <th class="al_l"><b>Название</b></th>
            <th class="al_c"><b>Цена</b></th>
            <th class="al_c"><b>Наличие</b></th>
            <th class="al_c"><b>Статус</b></th>
            <th width="220px" class="last_r"></th>
        </thead>
        <tbody>
        <?php if (!empty($products)): ?>
        <?php foreach ($products as $var): ?>
            <tr <?php echo ($var['is_delete'] == 1 ? 'class="delete"' : '') ?> id="product_<?php echo $var['id']; ?>">
                <td class="al_c"><input type="checkbox" name="product_id[]" value="<?php echo $var['id']; ?>" onclick="productsList.selectTr('product_<?php echo $var['id']; ?>')"></td>

                <td width="35px"><img width="35px" src="<?php echo Yii::app()->ImgManager->getUrlById($var['FirstImage']['img_id'], 'medium', $var['FirstImage']['img_filename']); ?>"></td>
                <td width="35px"><?php echo $var['external_id']; ?></td>

                <td width="30%" class="left">
                    <b><?php echo mApi::cutStr($var['title'], 30); ?></b> <?php echo $helper['visible'][$var['visible']]; ?>
                    <br><?php echo mApi::cutStr($var['description'], 50); ?>
                </td>

                <td class="al_c"><?php echo $var['price']; ?>р</td>

                <td class="al_c"><?php echo $helper['in_stock'][$var['in_stock']]; ?></td>

                <td class="al_c"><?php echo $helper['status'][$var['status']]; ?></td>

                <td class="links">
                    <div>
                        <a href="<?php echo Yii::app()->createUrl('vendor_cp/products/edit', array('id' => $var['id'])); ?>">изменить</a>
                        | <a href="<?php echo Yii::app()->createUrl('vendor_cp/products/add', array('based' => $var['id'])); ?>">копировать</a>
                        <?php if( $var['is_delete'] == 1 ): ?>
                        | <a onclick="productsList.restoreItem(<?php echo $var['id']; ?>); return false">восстановить</a>
                        <?php else: ?>
                        | <a onclick="productsList.deleteItem(<?php echo $var['id']; ?>); return false">удалить</a>
                        <?php endif; ?>
                    </div>
                </td>
            </tr>
            <?php endforeach; ?>
        <?php else: ?>
        <tr>
            <td colspan="9" align="center">Вы пока не добавили ни одного товара...</td>
        </tr>
        <?php endif; ?>
        </tbody>
    </table>
    <br>
    <div class="l_fl">
    <?php
    $this->widget('CLinkPager', array(
        'pages' => $dataProvider->pagination,
        'htmlOptions' => array('class' => 'b-default_pager'),
        'firstPageLabel' => '',
        'lastPageLabel' => '',
        'prevPageLabel' => '&larr; Назад',
        'nextPageLabel' => 'Далее &rarr;',
        'header' => '',
        'cssFile' => false
    ));
    ?>
    </div>
</div>
<script>
$(document).ready(function () {
    productsList.init();
});
</script>
<script src="/static/vendor/js/v_products_list.js?<?php echo Yii::app()->params['version']['js']; ?>"></script>