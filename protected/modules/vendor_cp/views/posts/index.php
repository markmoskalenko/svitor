<h1>Статьи</h1>
<div class="main_content">
	<p class="infotext">Написание статей увеличивает вашу <a href='<?php echo Yii::app()->createUrl('page/help_company#rating'); ?>' title=''>Свитосилу</a> и создает положительные впечатления о вашей компании. В каждой написанной статье будет ссылка на автора, т.е. вашу компанию. Содержание статей не должно противоречить <a href='/page/publication-rules' title='Правила размещения информации'>"Правилам размещения информации на портале"</a></p>
    <div class="button_main">
        <button onclick="location.assign('<?php echo Yii::app()->createUrl('vendor_cp/posts/add'); ?>');">Добавить</button>
    </div>
    <br><br>
    <table class="list_table">
        <thead>
            <th width="40px"><b>Дата</b></th>
            <th class="al_l"><b>Название</b></th>
            <th width="200px"><b>Статус</b></th>
            <th width="110px" class="last_r"></th>
        </thead>

        <?php if (!empty($posts)): ?>

            <?php foreach ($posts as $post): ?>
                <tr id="element_<?php echo $post['id']; ?>">

                    <td>
                        <span class="info c_default" onmouseover="tooltip.show(this, 'Отредактировано <?php echo date('d.m.Y в H:i', $post['edited_date']); ?>', {position:'top-right', special_class:'white'});"><?php echo date('d.m.Y', $post['created_date']); ?></span>
                    </td>

                    <td><?php echo $post['title']; ?></td>

                    <td class="al_c c_default">
                        <?php if ($post['status'] == 'published'): ?>
                            <span class="verified">
                                <a href="<?php echo Yii::app()->createUrl('post/view', array('id' => $post['id'])); ?>" target="_blank">опубликовано</a>
                            </span>
                        <?php elseif ($post['status'] == 'verification'): ?>
                            <span class="verify">на модерации</span>
                        <?php elseif ($post['status'] == 'rejected'): ?>
                            <span class="rejected">отклонено модератором</span>
                        <?php endif; ?>
                    </td>

                    <td>
                    <?php if ($post['status'] == 'verification' || $post['status'] == 'rejected'): ?>
                        <div class="edit_links">
                        <a href="<?php echo Yii::app()->createUrl('vendor_cp/posts/edit', array('id' => $post['id'])); ?>" class="edit">редактировать</a>
                        </div>
                    <?php endif; ?>
                    </td>
                </tr>
            <?php endforeach; ?>

        <?php else: ?>
            <tr>
                <td colspan="5" class="al_c">Вы пока не добавили ни одной статьи...</td>
            </tr>
        <?php endif; ?>
    </table>
    <br>
    <div class="l_fl">
        <?php
        $this->widget('CLinkPager', array(
            'pages' => $dataProvider->pagination,
            'htmlOptions' => array('class' => 'b-default_pager'),
            'firstPageLabel' => '',
            'lastPageLabel' => '',
            'prevPageLabel' => '&larr; Назад',
            'nextPageLabel' => 'Далее &rarr;',
            'header' => ''
        ));
        ?>
    </div>
    <br>
</div>