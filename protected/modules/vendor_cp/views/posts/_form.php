<h1><?php echo $model->isNewRecord ? 'Новая статья' : 'Редактирование статьи'; ?></h1>
<div class="b-vendor-default_form main_content">
    <?php echo CHtml::beginForm(); ?>
    <table>
        <tr>
            <td class="left top">Раздел:</td>
            <td class="right top">
                <?php
                $list_data = CHtml::listData($categories, 'id', 'title');
                echo CHtml::activeDropDownList($model, 'post_cat_id', $list_data, array(
                    'empty' => '-- не выбрано --',
                    'style' => 'width: 400px;',
                ));
                ?>
                <div class="note"><span class="counter"></span></div>
                <?php echo CHtml::error($model, 'post_cat_id'); ?>
            </td>
        </tr>
        <tr>
            <td class="left top">Название:</td>
            <td class="right top" id="title">
                <?php
                $model->title = CHtml::decode($model->title);
                echo CHtml::activeTextField($model, 'title', array(
                    'style' => 'width: 400px;',
                    'onkeyup' => 'formHelper.updateCounter(this, "title", 200);',
                    'onchange' => 'formHelper.updateCounter(this, "title", 200);',
                    'onclick' => 'formHelper.updateCounter(this, "title", 200);'
                ));
                ?>
                <div class="note"><span class="counter"></span></div>
                <?php echo CHtml::error($model, 'title'); ?>
            </td>
        </tr>
        <tr>
            <td class="left top"></td>
            <td class="right top" id="full_text">
                <?php echo CHtml::activeTextArea($model, 'content_2'); ?>
                <div class="note"><span class="counter"></span></div>
                <?php echo CHtml::error($model, 'content_2'); ?>
            </td>
        </tr>
        <tr>
            <td class="left top">Теги:</td>
            <td class="right top">
                <?php
                echo CHtml::activeTextField($model, 'tags', array(
                    'style' => 'width: 400px;'
                ));
                ?>
                <div class="note">(ключевые слова должны разделяться запятыми, например: тег1, тег2, тег3)</div>
                <?php echo CHtml::error($model, 'tags'); ?>
            </td>
        </tr>
    </table>
    <br>

    <div class="footer">
        <div class="button_main">
            <button type="submit"><?php echo $model->isNewRecord ? 'Добавить' : 'Сохранить'; ?></button>
        </div>
        или <a href="<?php echo Yii::app()->createUrl('vendor_cp/posts'); ?>">Отменить</a>
    </div>
    <?php echo CHtml::endForm(); ?>
</div>

<script src="/static/ckeditor/ckeditor.js?<?php echo Yii::app()->params['version']['js']; ?>"></script>
<script src="/static/ckeditor/adapters/jquery.js?<?php echo Yii::app()->params['version']['js']; ?>"></script>
<script>
$(document).ready(function () {
    $('#Post_content_2').ckeditor({width : '790px'});
});
</script>