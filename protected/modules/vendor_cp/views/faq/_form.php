<h1><?php echo $model->isNewRecord ? 'Новый F.A.Q' : 'Редактирование F.A.Q'; ?></h1>
<div class="b-vendor-default_form main_content">
    <?php echo CHtml::beginForm(); ?>
    <table>
        <tr id="question">
            <td class="left top">Вопрос:</td>
            <td class="right top">
                <?php
                    echo CHtml::activeTextField($model, 'question', array(
                        'style' => 'width: 400px; padding: 5px;',
                        'onkeyup' => 'formHelper.updateCounter(this, "question", 200);',
                        'onchange' => 'formHelper.updateCounter(this, "question", 200);',
                        'onclick' => 'formHelper.updateCounter(this, "question", 200);'
                    ));
                ?>
                <div class="note"><span class="counter"></span></div>
                <?php echo CHtml::error($model, 'question'); ?>
            </td>
        </tr>
        <tr id="response">
            <td class="left top">Ответ:</td>
            <td class="right top">
                <?php
                    echo CHtml::activeTextArea($model, 'response', array(
                        'style' => 'width: 520px; height: 190px; padding: 5px;',
                        'onkeyup' => 'formHelper.updateCounter(this, "response", 500);',
                        'onchange' => 'formHelper.updateCounter(this, "response", 500);',
                        'onclick' => 'formHelper.updateCounter(this, "response", 500);'
                    ));
                ?>
                <div class="note"><span class="counter"></span></div>
                <?php echo CHtml::error($model, 'response'); ?>
            </td>
        </tr>
    </table>
    <br>
    <div class="footer">
        <div class="button_main">
            <button type="submit"><?php echo $model->isNewRecord ? 'Добавить' : 'Сохранить'; ?></button>
        </div>
        или <a href="<?php echo Yii::app()->createUrl('vendor_cp/faq'); ?>">Отменить</a>
    </div>
    <?php echo CHtml::endForm(); ?>
</div>