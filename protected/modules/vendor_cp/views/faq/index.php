<h1>Часто задаваемые вопросы</h1>
<div class="main_content">
	<p class="infotext">Если к вам часто обращаются с одними и теми же вопросами, подготовьте на них ответы и опубликуйте в этом разделе.</p>
    <div class="button_main">
        <button onclick="location.assign('<?php echo Yii::app()->createUrl('vendor_cp/faq/add'); ?>');">Добавить</button>
    </div>
    <br><br>
    <?php
    if (count($model) > 0)
    {
        foreach ($model as $faq)
        {
            echo '<div><b>'.$faq['question'].'</b></div>';
            echo '<div>'.$faq['response'].'</div>';
            echo '<div><a href="' . Yii::app()->createUrl('vendor_cp/faq/edit', array('id' => $faq['id'])) . '">Изменить</a>';
            echo '| <a href="' . Yii::app()->createUrl('vendor_cp/faq/delete', array('id' => $faq['id'])) . '">Удалить</a></div>';
            echo '<br><br>';
        }
    }
    else
    {
        echo 'Вы пока не добавили ни одного вопроса...';
    }
    ?>
</div>