<div class="view">
    <div class="left">
Ищу исполнителя.
	<b><?php echo $data->getAttributeLabel('cat_id'); ?>:</b>
	<?php echo CHtml::link($data->cat->title, Yii::app()->createUrl('/catalog/vendors', array('cat' => $data->cat->name))); ?>
	<br />

	<b><?php echo $data->getAttributeLabel('user_id'); ?>:</b>
    <?php echo CHtml::link(CHtml::encode($data->user->login), $this->createUrl( '/user/profile', array('id'=>$data->user_id))); ?></span>

    <?php if(isset($data->cities->name) && !empty($data->cities->name)): ?>
    <b><?php echo $data->getAttributeLabel('cities_id'); ?>:<b>
            <?php echo CHtml::encode($data->cities->name); ?>
    <?php endif; ?>
    <b><?php echo $data->getAttributeLabel('date_created'); ?>:</b>
    <?php echo Date('d-m-Y', strtotime($data->date_created)); ?>
    <br />

    <div class="description-min">
        <?php echo CHtml::encode(mb_substr($data->description, 0, 45, 'UTF-8')); ?>... <?php echo CHtml::link('Подробнее','javascript:void(0)',array('class'=>'show-description','desc_id'=> $data->id)) ?>
    </div>
    <div class="description_full" style="display: none;" id="description_<?php echo $data->id; ?>">
        <?php echo CHtml::encode($data->description); ?>
    </div>

	<b><?php echo CHtml::encode($data->getAttributeLabel('type_holiday')); ?>:</b>
	<?php echo CHtml::encode($data->type_holiday); ?>
	<br />

    </div>
    <?php if(!empty($data->budget)): ?>
    <div class="right"><?php echo CHtml::encode($data->budget); ?>  руб.</div>
    <?php endif; ?>
    <div class="clear"></div>
</div>