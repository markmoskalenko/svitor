<div class="content">
    <div class="row">
        <h2>Заявки и объявления</h2>
    </div>
    <div class="row sorty">
        <?php echo CHtml::link('Открытые', $this->createUrl('/vendor_cp/bid/index',array('type'=>'opened')), array('class'=>((isset($_GET['type']) && $_GET['type']   == 'opened' ) || !isset($_GET['type']))? 'active' : 'not_active')); ?>
        <?php echo CHtml::link('Закрытые', $this->createUrl('/vendor_cp/bid/index',array('type'=>'closed')), array('class'=>isset($_GET['type']) && $_GET['type'] == 'closed' ? 'active' : 'not_active')); ?>
    </div>

    <div class="row">
        <?php if( $opened == 'opened' ): ?>
        <?php $this->widget('zii.widgets.CListView', array(
                                                          'dataProvider' => $dataProvider,
                                                          'itemView'     => '_viewOpened',
                                                     )); ?>
        <?php else: ?>
        <?php $this->widget('zii.widgets.CListView', array(
                                                          'dataProvider' => $dataProvider,
                                                          'itemView'     => '_viewClosed',
                                                     )); ?>
        <?php endif; ?>
    </div>
</div>
<script type="text/javascript">
    if(window.location.hash.substring(1) == 'message'){
        upBox.ajaxLoad('/vendor_cp/messages/new?bid=' + <?php echo $bid; ?> + '&uid=' + <?php echo $uid; ?>  + '&title='+encodeURIComponent("Ответ по заявке")  );
    }
    $(function(){
        $('.reply').click(function(){
            var uid = $(this).attr('id');
            var bid = $(this).attr('bid');
            upBox.ajaxLoad('/vendor_cp/messages/new?bid=' + bid + '&uid=' + uid + '&title='+encodeURIComponent("Ответ по заявке")  );
        })
        $('.show-description').click(function(){
            var id = $(this).attr('desc_id');
            $('.description-min').show();
            $('.description_full').hide();
            $(this).parent().hide();
            $("#description_"+id).show();
        });
    });
</script>