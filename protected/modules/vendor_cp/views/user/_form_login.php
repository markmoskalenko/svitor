<h1>Вход</h1>
<?php
if (Yii::app()->user->hasFlash('login_error'))
    echo '<div class="errorSummary">Ошибка: ' . Yii::app()->user->getFlash('login_error') . '</div>';
?>

<?php echo CHtml::beginForm(); ?>
<table class="b-vendor-default_form">
    <tr>
        <td class="left">E-mail</td>
        <td class="right"><input type="text" name="email" value="<?php if (!empty($_POST['email'])) echo $_POST['email']; ?>"></td>
    </tr>
    <tr>
        <td class="left">Пароль</td>
        <td class="right"><input type="password" name="password" value=""></td>
    </tr>
    <tr>
        <td class="left"></td>
        <td class="right">
            <div><a href="/vendor_cp/user/forgotpassword">Забыли пароль?</a></div>
        </td>
    </tr>
    <tr>
        <td></td>
        <td class="right"><label><input type="checkbox" name="rememberMe" value="1" checked="checked"> Запомнить</label></td>
    <tr>
    <tr>
        <td></td>
        <td class="right">
           <br>
            <div class="button_main">
                <button type="submit">Войти</button>
            </div>
        </td>
    <tr>
</table>
<?php echo CHtml::endForm(); ?>
