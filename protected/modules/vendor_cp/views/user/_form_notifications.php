<?php echo CHtml::beginForm(); ?>
<?php echo CHtml::hiddenField('Notifications'); ?>
<table class="default">
    <tr>
        <td class="top">
            <label><?php echo CHtml::activeCheckbox($vendor, 'notify_site_news'); ?> Получать новости сайта</label>
            <?php echo CHtml::error($vendor, 'notify_site_news'); ?>
        </td>
    </tr>
    <tr>
        <td class="top">
            <label><?php echo CHtml::activeCheckbox($vendor, 'notify_new_reviews'); ?> Получать уведомления о новых отзывах</label>
            <?php echo CHtml::error($vendor, 'notify_new_reviews'); ?>
        </td>
    </tr>
    <tr>
        <td class="top">
            <label><?php echo CHtml::activeCheckbox($vendor, 'notify_new_msg'); ?> Получать уведомления о новых сообщениях</label>
            <?php echo CHtml::error($vendor, 'notify_new_msg'); ?>
        </td>
    </tr>
    <tr>
        <td>
            <br>
            <div class="button_main">
                <button type="submit">Сохранить</button>
            </div>
        </td>
    </tr>
</table>
<?php echo CHtml::endForm(); ?>