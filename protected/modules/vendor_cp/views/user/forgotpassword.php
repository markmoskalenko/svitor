<h1>Восстановление пароля</h1>
<div>Введите свой e-mail, чтобы получить новый пароль</div>

<?php echo CHtml::beginForm(); ?>
<table class="b-vendor-default_form">
    <tr>
        <td class="left">E-mail</td>
        <td class="right"><input type="text" name="email" value="<?php if (!empty($_POST['email'])) echo $_POST['email']; ?>"></td>
    </tr>
    <span style="color:red;"><?php if (isset($error)) echo $error; ?></span>
    <tr>
        <td></td>
        <td class="right">
            <br>
            <div class="button_main">
                <button type="submit">Восстановить</button>
            </div>
        </td>
    <tr>
</table>
<?php echo CHtml::endForm(); ?>