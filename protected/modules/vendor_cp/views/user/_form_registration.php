<h1>Регистрация компании</h1>

<?php echo CHtml::beginForm(); ?>
<table class="b-vendor-default_form">
    <tr>
        <td class="left">Название</td>
        <td class="right">
            <?php echo CHtml::activeTextField($vendor, 'title'); ?>
            <?php echo CHtml::error($vendor, 'title'); ?>
        </td>
    </tr>
    <tr>
        <td class="left">E-mail</td>
        <td class="right" id="email">
            <?php echo CHtml::activeTextField($vendor, 'email'); ?> <span id="email_loader"></span>
            <?php echo CHtml::error($vendor, 'email'); ?>
            <?php echo empty($_POST) ? '<div class="errorMessage" style="display: none;"></div>' : ''; ?>
        </td>
    </tr>
    <tr>
        <td class="left">Пароль</td>
        <td class="right">
            <?php echo CHtml::activePasswordField($vendor, 'password'); ?>
            <?php echo CHtml::error($vendor, 'password'); ?>
        </td>
    </tr>
    <tr>
        <td class="left">Пароль еще раз</td>
        <td class="right">
            <?php echo CHtml::activePasswordField($vendor, 'confirm_password'); ?>
            <?php echo CHtml::error($vendor, 'confirm_password'); ?>
        </td>
    </tr>
<!--    <tr>
        <td class="left"></td>
        <td class="right"><?php $this->widget('CCaptcha', array('buttonLabel' => '&larr; обновить')); ?> </td>
    </tr>
    <tr>
        <td class="left">Код с картинки</td>
        <td class="right">
            <?php echo CHtml::activeTextField($vendor, 'captcha_code'); ?>
            <?php echo CHtml::error($vendor, 'captcha_code'); ?>
        </td>
    </tr> -->
    <tr>
        <td class="left"></td>
        <td class="right">
            <label>
                <?php echo CHtml::activeCheckbox($vendor, 'accept_terms'); ?>
                Я соглашаюсь с <a href="<?php echo Yii::app()->createUrl('pages/index', array('name' => 'terms-of-use')); ?>" target="_blank">пользовательским соглашением</a> и <a href="<?php echo Yii::app()->createUrl('pages/index', array('name' => 'privacy-policy')); ?>" target="_blank">политикой конфиденциальности</a>.
                <?php echo CHtml::error($vendor, 'accept_terms', array('id' => 'accept_terms_error')); ?>
            </label>
        </td>
    </tr
    <tr>
        <td></td>
        <td class="right">
            <br>
            <div class="button_main">
                <button type="submit">Зарегистрироваться</button>
            </div>
        </td>
    </tr>
</table>
<?php echo CHtml::endForm(); ?>
<script src="/static/vendor/js/v_registration.js"></script>