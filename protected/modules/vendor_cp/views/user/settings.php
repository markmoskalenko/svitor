<h1>Настройки аккаунта</h1>
<div class="main_content">

    <h2>Уведомления</h2>
    <?php if (!empty($success[0])) echo '<div class="msg_success">'.$success[0].'</div>'; ?>
    <?php $this->renderPartial('_form_notifications', array('vendor' => $vendor)); ?>

    <br><br>
    <h2>Пароль</h2>
    <?php if (!empty($success[1])) echo '<div class="msg_success">'.$success[1].'</div>'; ?>
    <?php $this->renderPartial('_form_changepassword', array('vendor' => $vendor)); ?>

</div>