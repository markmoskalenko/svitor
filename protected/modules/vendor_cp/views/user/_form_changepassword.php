<?php echo CHtml::beginForm(); ?>
<?php echo CHtml::hiddenField('ChangePassword'); ?>
<table class="default">
    <tr>
        <td class="left">Текущий пароль:</td>
        <td>
            <?php echo CHtml::activePasswordField($vendor, 'old_password'); ?>
            <?php echo CHtml::error($vendor, 'old_password'); ?>
        </td>
    </tr>

    <tr>
        <td class="left">Новый пароль:</td>
        <td>
            <?php echo CHtml::activePasswordField($vendor, 'new_password'); ?>
            <?php echo CHtml::error($vendor, 'new_password'); ?>
        </td>
    </tr>

    <tr>
        <td class="left">Еще раз новый:</td>
        <td>
            <?php echo CHtml::activePasswordField($vendor, 'confirm_new_password'); ?>
            <?php echo CHtml::error($vendor, 'confirm_new_password'); ?>
        </td>
    </tr>
</table>
<br>
<div class="button_main">
    <button type="submit">Сохранить</button>
</div>
<?php echo CHtml::endForm(); ?>