<h1>Что такое кабинет компании?</h1>
<div class="main_content svitor_about">
    <div class="main_desc">
        Это место, где вы вы можете управлять информацией о своей компании, которая отображается на основном сайте <a href="/">svitor.ru</a>, общаться с пользователями, отслеживать уведомления и многое другое!


        <div class="go_to_registration">
            <button onclick="location.assign('<?php echo Yii::app()->createUrl('vendor_cp/user/registration'); ?>');">Зарегистрировать компанию (бесплатно)</button>
            или <a href="<?php echo Yii::app()->createUrl('vendor_cp/user/login'); ?>">Войти</a> для управления уже зарегистрированной.
        </div>
    </div>
</div>