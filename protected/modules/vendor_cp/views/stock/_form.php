<h1>Новая акция</h1>
<div class="b-vendor-default_form main_content">
    <?php echo CHtml::beginForm(); ?>
    <table>
        <tr>
            <td class="left top"><span class="title">Название: <span class="red_text">*</span></span></td>
            <td class="right top" id="title">
                <?php
                $model->title = CHtml::decode($model->title);
                echo CHtml::activeTextField($model, 'title', array(
                    'style' => 'width: 400px;',
                ));
                ?>
                <div class="note"><span class="counter"></span></div>
                <?php echo CHtml::error($model, 'title'); ?>
            </td>
        </tr>
        <tr>
            <td class="left top"><span class="title">Период: <span class="red_text">*</span></span> </td>
            <td class="right top" id="date">
                <span style="color: #808080;">с </span><?php

                if($model->date_from){
                    $date = $model->date_from;
                } else {
                    $date = time()+86400;
                }
                $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                'name'=>"VendorStock[date_from]",
                'value'=>date("d-m-Y",$date),
                    'htmlOptions' => array(
                        'size' => '10',
                        'maxlength' => '10',
                        'style'=> 'width: 155px; margin-left: 15px; margin-right: 15px;'
                    ),
                    'options' => array(
                        'dateFormat' => 'dd-mm-yy',     // format of "2012-12-25"
                        'minDate' => date("d-m-Y",time()),      // minimum date
                        'maxDate' =>  date("d-m-Y",time()+2678400),      // maximum date
                    ),
                ));
                ?>
                <span style="color: #808080;">по </span><span id="lastDate">
                    <?php
                if($model->date_to){
                    $date = $model->date_to;
                } else {
                    $date = time()+691200;
                }
                $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                'name'=>"VendorStock[date_to]",
                'value'=>date("d-m-Y",$date),
                    'htmlOptions' => array(
                        'size' => '10',         // textField size
                        'maxlength' => '10',
                        'style'=> 'width: 155px; margin-left: 15px; margin-right: 15px;'
                    ),
                    'options' => array(
                        'dateFormat' => 'dd-mm-yy',     // format of "2012-12-25"
                        'minDate' => date("d-m-Y",time()-12678400),      // minimum date
                        'maxDate' => date("d-m-Y",time()+12678400),      // maximum date   2678400
                    ),
                ));
                    ?>
                </span>
                <div class="note"><span class="counter"></span></div>
                <div style="color:red;"><?php if(isset($error['date_from'])){ echo $error['date_from'];}elseif(isset($error['date_to'])){ echo $error['date_to'];}?></div>
                <span style="margin-left: 25px; float: left;"><?php echo CHtml::error($model, 'date_from'); ?></span>
                <span style="margin-right: 57px; float: right;"><?php echo CHtml::error($model, 'date_to'); ?></span>
            </td>
        </tr>
        <tr>
            <td class="left top"><span class="title">Описание:</span></td>
            <td class="right top" id="full_text">
                <?php echo CHtml::activeTextArea($model, 'description', array(
                        'style' => 'width: 400px; height: 150px;',
                    )
                ); ?>
                <div class="note"><span class="counter"></span></div>
                <?php echo CHtml::error($model, 'description'); ?>
            </td>
        </tr>
    </table>
    <br>

    <div class="footer">
        <div class="button_main">
            <button type="submit"><?php echo $model->isNewRecord ? 'Добавить' : 'Сохранить'; ?></button>
        </div>
        или <a href="<?php echo Yii::app()->createUrl('vendor_cp/stock'); ?>">Отменить</a>
    </div>
    <?php echo CHtml::endForm(); ?>
</div>

<script src="/static/ckeditor/adapters/jquery.js?<?php echo Yii::app()->params['version']['js']; ?>"></script>