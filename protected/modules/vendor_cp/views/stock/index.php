<h1>Акции</h1>
<div class="main_content">
	<p class="infotext">Если вы хотите проинформировать пользователей о действующих у вас скидках или
        акциях – добавьте.</p>
    <?php if($vendor["is_banned_stock"] == 1):?>
    <p class="infotext">В связи с нарушением условий размещения Акций, вам заблокирована возможность использования услуги на срок до <?php echo date("Y-m-d", $vendor["stock_stamp"]+2678400);?></p>
    <?php elseif(!empty($posts)):?>
    <p class="infotext">Добавление новой акции невозможно до тех пор, пока остались действующие акции.</p>
    <?php else: ?>
    <div class="button_main">
        <button onclick="location.assign('<?php echo Yii::app()->createUrl('vendor_cp/stock/add'); ?>');">Добавить</button>
    </div>
    <?php endif;?>
    <br><br>
</div>
<h1>Действующая акция</h1>
<div class="main_content">
    <table class="list_table">
        <thead>
            <th width="250px"><b>Название</b></th>
            <th class="al_l"><b>Период</b></th>
            <th width="110px" class="last_r"></th>
        </thead>
        <?php if (!empty($posts)): ?>
            <?php foreach ($posts as $post): ?>
                <tr id="element_<?php echo $post['stock_id']; ?>" class="editStock">
                    <td><?php echo $post['title']; ?></td>
                    <td>
                        c <?php echo date("d-m-Y",$post['date_from']); ?> по <?php echo date("d-m-Y",$post['date_to']); ?>
                    </td>
                    <td>
                        <a style="display: none;" href="<?php echo Yii::app()->createUrl('vendor_cp/stock/edit', array('id' => $post['stock_id'])); ?>" class="edit">изменить</a>
                    </td>
                </tr>
            <?php endforeach; ?>

        <?php else: ?>
            <tr>
                <td colspan="5" class="al_c">Вы пока не добавили ни одной акции...</td>
            </tr>
        <?php endif; ?>
    </table>
    <br>
    <div class="l_fl">
        <?php
        $this->widget('CLinkPager', array(
            'pages' => $dataProvider->pagination,
            'htmlOptions' => array('class' => 'b-default_pager'),
            'firstPageLabel' => '',
            'lastPageLabel' => '',
            'prevPageLabel' => '&larr; Назад',
            'nextPageLabel' => 'Далее &rarr;',
            'header' => ''
        ));
        ?>
    </div>
    <br>
</div>
<h1>Завершенные акции</h1>
<div class="main_content">
    <table class="list_table">
        <thead>
        <th width="250px"><b>Название</b></th>
        <th class="al_l"><b>Период</b></th>
        </thead>
        <?php if (!empty($postsOld)): ?>
        <?php foreach ($postsOld as $post): ?>
            <tr id="element_<?php echo $post['stock_id']; ?>" class="editStock">
                <td><?php echo $post['title']; ?></td>
                <td>
                    c <?php echo date("d-m-Y",$post['date_from']); ?> по <?php echo date("d-m-Y",$post['date_to']); ?>
                </td>
            </tr>
            <?php endforeach; ?>

        <?php else: ?>
        <tr>
            <td colspan="5" class="al_c">Вы пока не добавили ни одной акции...</td>
        </tr>
        <?php endif; ?>
    </table>
    <br>
    <div class="l_fl">
        <?php
        $this->widget('CLinkPager', array(
            'pages' => $dataProviderOld->pagination,
            'htmlOptions' => array('class' => 'b-default_pager'),
            'firstPageLabel' => '',
            'lastPageLabel' => '',
            'prevPageLabel' => '&larr; Назад',
            'nextPageLabel' => 'Далее &rarr;',
            'header' => ''
        ));
        ?>
    </div>
    <br>
</div>
<script src="/static/ckeditor/adapters/jquery.js?<?php echo Yii::app()->params['version']['js']; ?>"></script>
<script type="text/javascript">
    $(".editStock").mouseover(function(){
        $(this).find("a").css("display","block");
    });
    $(".editStock").mouseout(function(){
        $(this).find("a").css("display","none");
    });
</script>
