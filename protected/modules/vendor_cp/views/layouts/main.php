<!DOCTYPE html>
<html>
<head>
	<title><?php echo Yii::app()->name; ?></title>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="/static/vendor/global.css?<?php echo Yii::app()->params['version']['css']; ?>" />
    <link rel="stylesheet" type="text/css" href="/static/vendor/global2.css?<?php echo Yii::app()->params['version']['css']; ?>" />
    <link rel="stylesheet" type="text/css" href="/static/m_tooltip.css?<?php echo Yii::app()->params['version']['css']; ?>" />
    <?php Yii::app()->getClientScript()->registerCoreScript( 'jquery' ); ?>

    <script type="text/javascript" src="/static/js/jquery.placeholder.js?<?php echo Yii::app()->params['version']['js']; ?>"></script>
	<script type="text/javascript" src="/static/js/main.js?<?php echo Yii::app()->params['version']['js']; ?>"></script>
    <script type="text/javascript" src="/static/js/m_tooltip.js?<?php echo Yii::app()->params['version']['js']; ?>"></script>
	<link rel="stylesheet" type="text/css" href="/static/fancybox/jquery.fancybox.css?<?php echo Yii::app()->params['version']['css']; ?>">
	<script src="/static/fancybox/jquery.fancybox.pack.js"></script>

    <link rel="stylesheet" type="text/css" href="/static/humanity/jquery-ui-1.8.23.custom.css?<?php echo Yii::app()->params['version']['css']; ?>">
    <script src="/static/js/jquery-ui-1.8.23.custom.min.js"></script>
</head>
<body>
<div class="layout">
    <div class="b-header">
        <div class="left-tape"></div>
        <div class="right-tape"></div>
        <div class="b-title"><a href="/vendor_cp">Кабинет компании</a></div>
        <div class="main_site_link"><a href="/">&larr; перейти на основной сайт</a></div>
        <div class="b-userpanel"><?php $this->widget('application.modules.vendor_cp.widgets.UserPanel'); ?></div>
    </div>
    <div class="b-container_wrap">
        <div class="b-container">
            <?php $this->widget('application.modules.vendor_cp.widgets.mainMenu'); ?>
            <div class="b-content">
                <?php echo $content; ?>
            </div>
        </div>
	</div>
    <div class="layout_footer">
        <?php $this->renderPartial('//layouts/_footer'); ?>
    </div>
</div>
</body>
</html>
