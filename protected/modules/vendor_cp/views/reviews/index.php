<h1>Отзывы от пользователей</h1>
<p class="infotext">Здесь вы увидите все отзывы оставленные пользователями о вас. Обращаем внимание, что отзывы влияют на вашу <a href="<?php echo Yii::app()->createUrl('page/help_company#rating'); ?>" title=''>Свитосилу</a>.</p>
<div class="main_content">
<?php if (!empty($model)): ?>
    <?php foreach ($model as $var): ?>
    <?php $profile_url = Yii::app()->createUrl('user/profile', array('id' => $var['User']['id'])); ?>
        <table width="100%">
            <td valign="top" width="55px">
                <a href="<?php echo $profile_url; ?>"><img src="<?php echo Yii::app()->ImgManager->getUrlById($var['User']['img_id'], 'small', $var['User']['img_filename']); ?>"></a>
            </td>
            <td valign="top" align="left">
                <b><a href="<?php echo $profile_url; ?>"><?php echo $var['User']['login']; ?></a></b><br>
                <?php echo $var['comment']; ?>
                <br>
                <b>Оценка:</b> <?php echo $var['quality']; ?> из 5
            </td>
        </table><br>
    <?php endforeach; ?>

<?php else: ?>
   Нет ни одного отзыва...
<?php endif; ?>
</div>