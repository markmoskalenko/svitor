<?php if (!empty($success)) echo '<span class="msg_success">'.$success.'</span>'; ?>
<div class="main_content">
<h2>
    Основное
</h2>
<?php echo CHtml::beginForm(); ?>
    <div class="b-main_tabs_box b-vendor-default_form">
            <table>
                <tr>
                    <td class="left top">Адрес профайла:</td>
                    <td class="right">
                        <div style="margin-top: 5px;">
                            <?php $profile_url = Yii::app()->createUrl('vendor/profile', array('id' => Vendor::getId())); ?>
                            <a href="<?php echo $profile_url; ?>" target="_blank">http://<?php echo Yii::app()->request->getServerName() . $profile_url; ?></a>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="left top"><span class="red_text">*</span> Название:</td>
                    <td class="right">
                        <?php
                        $vendor->title = CHtml::decode($vendor->title);
                        echo CHtml::activeTextField($vendor, 'title'); ?>
                        <?php echo CHtml::error($vendor, 'title'); ?>
                    </td>
                </tr>
                <tr>
                    <td class="left top"><span class="red_text">*</span> О компании:</td>
                    <td class="right" id="description">
                        <?php
                        $vendor->description = CHtml::decode($vendor->description);
                        echo CHtml::activeTextarea($vendor, 'description', array(
                            'style' => 'width: 650px; height: 100px;',
                            'onclick' => 'formHelper.updateCounter(this, \'description\', 3000);',
                            'onchange' => 'formHelper.updateCounter(this, \'description\', 3000);',
                            'onkeyup' => 'formHelper.updateCounter(this, \'description\', 3000);',
                        ));
                        ?>
                        <?php echo CHtml::error($vendor, 'description'); ?>
                        <span class="note"><span class="counter"></span></span>
                    </td>
                </tr>
                <tr>
                    <td class="left top">Фото или Логотип:</td>
                    <td class="right">
                        <div id="image_thumbs">
                            <?php
                            if (!empty($vendor['img_id']))
                                echo '<img src="'.Yii::app()->ImgManager->getUrlById($vendor['img_id'], 'medium', $vendor['img_filename']).'">
                                  <img src="'.Yii::app()->ImgManager->getUrlById($vendor['img_id'], 'small', $vendor['img_filename']).'">';
                            ?>
                        </div>
                        <br>
                        <a id="upload_image">Выбрать файл &rarr;</a><br>
                        <span class="note">(поддерживаются форматы JPG, PNG и GIF, размер не более 2мб)</span>
                        <input type="hidden" name="Vendor[img_id]" id="img_id" value="<?php echo $vendor['img_id']; ?>">
                        <input type="hidden" name="Vendor[img_filename]" id="img_filename" value="<?php echo $vendor['img_filename']; ?>">
                    </td>
                </tr>
            </table>
    </div>
        <br>
    <h2>
        Контакты
    </h2>
    <div class="b-main_tabs_box b-vendor-default_form">
        <table>
            <tr>
                <td class="left top"><span class="red_text">*</span> <?php echo (empty($vendor->contact_phone2) && empty($vendor->contact_phone3)) ? 'Телефон' : 'Телефоны'; ?></td>
                <td class="right">
                    <div>
                        <?php echo CHtml::activeTextField($vendor, 'contact_phone'); ?>
                        <?php if (empty($vendor->contact_phone2) && empty($vendor->contact_phone3)): ?>
                            <a onclick="general_info.morePhones(this); return false;">+ добавить еще</a>
                        <?php endif; ?>
                        <?php echo CHtml::error($vendor, 'contact_phone'); ?>
                    </div>
                    <div class="more_contact_phones" <?php if (empty($vendor->contact_phone2) && empty($vendor->contact_phone3)) echo 'style="display:none;"'; ?>>
                        <div><?php echo CHtml::activeTextField($vendor, 'contact_phone2', array('placeholder' => 'дополнительный телефон')); ?></div>
                        <div><?php echo CHtml::activeTextField($vendor, 'contact_phone3', array('placeholder' => 'дополнительный телефон')); ?></div>
                    </div>
                </td>
            </tr>
            <tr>
                <td class="left top">Сайт компании:</td>
                <td class="right">
                    <?php echo CHtml::activeTextField($vendor, 'contact_site'); ?>
                    <?php echo CHtml::error($vendor, 'contact_site'); ?>
                </td>
            </tr>
            <tr>
                <td class="left top">Skype:</td>
                <td class="right">
                    <?php
                    $vendor->contact_skype = CHtml::decode($vendor->contact_skype);
                    echo CHtml::activeTextField($vendor, 'contact_skype'); ?>
                    <?php echo CHtml::error($vendor, 'contact_skype'); ?>
                </td>
            </tr>
            <tr>
                <td class="left top">Twitter:</td>
                <td class="right">
                    <?php
                    $vendor->contact_twitter = CHtml::decode($vendor->contact_twitter);
                    echo CHtml::activeTextField($vendor, 'contact_twitter'); ?>
                    <?php echo CHtml::error($vendor, 'contact_twitter'); ?>
                </td>
            </tr>
            <tr>
                <td class="left top">Вконтакте:</td>
                <td class="right">
                    <?php
                    $vendor->contact_vk = CHtml::decode($vendor->contact_vk);
                    echo CHtml::activeTextField($vendor, 'contact_vk'); ?>
                    <?php echo CHtml::error($vendor, 'contact_vk'); ?>
                </td>
            </tr>
            <tr>
                <td class="left top">Facebook:</td>
                <td class="right">
                    <?php
                    $vendor->contact_fb = CHtml::decode($vendor->contact_fb);
                    echo CHtml::activeTextField($vendor, 'contact_fb'); ?>
                    <?php echo CHtml::error($vendor, 'contact_fb'); ?>
                </td>
            </tr>
        </table>
        <br>
        <div class="footer">
            <div class="button_main">
                <button type="submit">Сохранить</button>
            </div>
        </div>
    </div>
<?php echo CHtml::endForm(); ?>
</div>
<script>
$(document).ready(function() {
	general_info.uploadLogo($('#upload_image'));

    $('form').FormNav('У вас есть не сохраненные изменения. Если вы хотите их сохранить, нажмите кнопку «Сохранить» в нижней части формы.');
});
</script>

<script src="/static/js/ajaxupload.js?<?php echo Yii::app()->params['version']['js']; ?>"></script>
<script src="/static/js/jquery.form_nav.js?<?php echo Yii::app()->params['version']['js']; ?>"></script>
<script src="/static/vendor/js/v_general_info.js?<?php echo Yii::app()->params['version']['js']; ?>"></script>