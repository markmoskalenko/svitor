<div class="b-main_tabs_box">
	<?php $infotext = '<p class="infotext">Если ваша деятельность предполагает лицензирование или вы являетесь победителем каких-либо конкурсов, то в этом разделе вы можете разместить сканированные изображения этих документов.</p>'; 
?>
    <div class="b-media-thumbs_title">
        Загружено <span id="certificateCounter"><?php echo $certificates_count; ?></span> / <?php echo $max_count; ?>
    </div>
    <div class="b-media-thumbs_box">
            <div class="b-media-thumbs">
                <?php foreach ($model as $var): ?>

                    <div class="element" id="element_<?php echo $var['id']; ?>">
                        <a class="upbox-button" rel="upbox-certificates" href="<?php echo Yii::app()->ImgManager->getUrlById($var['img_id'], 'large', $var['img_filename']); ?>" title="<?php echo $var['description']; ?>"><img src="<?php echo Yii::app()->ImgManager->getUrlById($var['img_id'], 'medium', $var['img_filename']); ?>"></a>
                        <br>
                        <a href="<?php echo Yii::app()->createUrl('vendor_cp/company/certificates', array('id' => $var['id'])); ?>/edit">редактировать</a>
                        <a onclick="certificate.deleteItem(this, <?php echo $var['id']; ?>); return false;" class="delete magic_button" onmouseover="tooltip.show(this, 'Удалить', {position:'top-right'});"></a>
                    </div>

                <?php endforeach; ?>
            </div>
			<?php echo $infotext; ?>
            <div class="button_main" id="addCertificateButton" <?php if ($certificates_count >= $max_count) echo 'style="display:none;"'; ?>>
                <button onclick="location.assign('<?php echo Yii::app()->createUrl('vendor_cp/company/certificates'); ?>/add');">Добавить</button>
            </div>
    </div>
</div>
<div id="trash-dialog" title="Удалить" style="display: none;">Удаление происходит без возможности восстановления. <br><br>Вы действительно продолжить?</div>
<script>
$(document).ready(function () {
    certificate.init(<?php echo $certificates_count.', '.$max_count; ?>);
});
</script>
<script src="/static/vendor/js/v_certificate.js?<?php echo Yii::app()->params['version']['js']; ?>"></script>