<div class="b-main_tabs_title">
    <?php echo $model->isNewRecord ? 'Новый скриншот' : 'Редактирование'; ?>
</div>
<div class="b-vendor-default_form b-main_tabs_box">
    <?php echo CHtml::beginForm(); ?>
    <table>
        <tr>
            <td class="left top">Изображение</td>
            <td class="right">

                <div id="image_thumbs">
                    <?php if (!empty($model->img_id)): ?>
                    <img src="<?php echo Yii::app()->ImgManager->getUrlById($model->img_id, 'medium', $model->img_filename); ?>">
                    <?php endif; ?>
                </div>
                <input type="hidden" name="VendorCertificates[img_id]" id="img_id" value="<?php echo $model->img_id; ?>">
                <input type="hidden" name="VendorCertificates[img_filename]" id="img_filename" value="<?php echo $model->img_filename; ?>">
                <br>
                <a id="upload_image">Выбрать файл &rarr;</a>
                <span class="note">(поддерживаются форматы JPG, PNG и GIF, размер не более 2мб)</span>
                <?php echo CHtml::error($model, 'img_filename'); ?>
                <br>
            </td>
        </tr>
        <tr>
            <td class="left top"><span class="red_text">*</span> Описание</td>
            <td class="right">
                <?php
                $model->description = CHtml::decode($model->description);
                echo CHtml::activeTextArea($model, 'description', array('style' => 'width: 650px; height: 80px;')); ?>
                <?php echo CHtml::error($model, 'description'); ?>
            </td>
        </tr>
    </table>
    <br>
    <div class="footer">
        <div class="button_main">
            <button type="submit"><?php echo $model->isNewRecord ? 'Добавить' : 'Сохранить'; ?></button>
        </div>
        или <a href="<?php echo Yii::app()->createUrl('vendor_cp/company/certificates'); ?>">Отменить</a>
    </div>
    <?php echo CHtml::endForm(); ?>
</div>

<script>
$(document).ready(function () {
    certificate.uploadImg($('#upload_image'));
    <?php if (!$model->isNewRecord): ?>$('form').FormNav('У вас есть не сохраненные изменения. Если вы хотите их сохранить, нажмите кнопку «Сохранить» в нижней части формы.');<?php endif; ?>
});
</script>

<script src="/static/js/ajaxupload.js?<?php echo Yii::app()->params['version']['js']; ?>"></script>
<script src="/static/vendor/js/v_certificate.js?<?php echo Yii::app()->params['version']['js']; ?>"></script>
<script src="/static/js/jquery.form_nav.js?<?php echo Yii::app()->params['version']['js']; ?>"></script>