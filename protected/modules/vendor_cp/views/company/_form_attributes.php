<div class="b-main_tabs_title">
    Характеристики для раздела &laquo;<?php echo $model['Category']['title']; ?>&raquo;
</div>
<?php echo CHtml::beginForm(); ?>
    <div class="b-vendor-default_form b-main_tabs_box">
        <div id="attributes_data">
            <?php
            if (!empty($model->cat_id))
                $this->widget('application.modules.vendor_cp.widgets.AttrsByCatId', array(
                    'cat_id' => $model->cat_id,
                    'vendor_id' => Vendor::getId()
                ));
            ?>
        </div>
    <div class="footer" style="margin-top: 15px;">
        <div class="button_main">
            <button type="submit">Сохранить</button>
        </div>
        или <a href="<?php echo Yii::app()->createUrl('vendor_cp/company/categories'); ?>">вернуться к списку разделов</a>
    </div>
</div>
<?php echo CHtml::endForm(); ?>
<script src="/static/js/jquery.form_nav.js?<?php echo Yii::app()->params['version']['js']; ?>"></script>
<script>
$(document).ready(function() {
    $('form').FormNav('У вас есть не сохраненные изменения. Если вы хотите их сохранить, нажмите кнопку «Сохранить» в нижней части формы.');
})
</script>