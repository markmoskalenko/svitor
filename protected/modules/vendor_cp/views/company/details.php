<div class="b-main_tabs_box" style="margin-top: 10px;">
	<?php $infotext = '<p class="infotext">Заполненные в этом разделе данные используются для выставления счетов на оплату услуг svitor.ru</p>'; 
		echo $infotext;
?>
    <?php if (!empty($success)) echo '<span class="msg_success">'.$success.'</span>'; ?>
    <div id="comp_info" class="b-vendor-default_form">
        <?php echo CHtml::beginForm(); ?>
        <table>
            <tr>
                <td class="left top"><span class="red_text">*</span> Наименование юр. лица:</td>
                <td class="right">
                    <?php
                    $details->legal_name = html_entity_decode($details->legal_name);
                    echo CHtml::activeTextField($details, 'legal_name'); ?>
                    <?php echo CHtml::error($details, 'legal_name'); ?>
                </td>
            </tr>
            <tr>
                <td class="left top"><span class="red_text">*</span> Юридический адрес:</td>
                <td class="right">
                    <?php
                    $details->legal_address = html_entity_decode($details->legal_address);
                    echo CHtml::activeTextField($details, 'legal_address'); ?>
                    <?php echo CHtml::error($details, 'legal_address'); ?>
                </td>
            </tr>
            <tr>
                <td class="left top">ФИО руководителя:</td>
                <td class="right">
                    <?php echo CHtml::activeTextField($details, 'head_fullname'); ?>
                    <?php echo CHtml::error($details, 'head_fullname'); ?>
                </td>
            </tr>
            <tr>
                <td class="left top">Должность руководителя:</td>
                <td class="right">
                    <?php
                    $details->head_post = html_entity_decode($details->head_post);
                    echo CHtml::activeTextField($details, 'head_post'); ?>
                    <?php echo CHtml::error($details, 'head_post'); ?>
                </td>
            </tr>
            <tr>
                <td class="left top">ФИО главного бухгалтера:</td>
                <td class="right">
                    <?php echo CHtml::activeTextField($details, 'accountant_fullname'); ?>
                    <?php echo CHtml::error($details, 'accountant_fullname'); ?>
                </td>
            </tr>
            <tr>
                <td class="left top"><span class="red_text">*</span> ИНН:</td>
                <td class="right">
                    <?php echo CHtml::activeTextField($details, 'inn'); ?>
                    <?php echo CHtml::error($details, 'inn'); ?>
                </td>
            </tr>
            <tr>
                <td class="left top">КПП:</td>
                <td class="right">
                    <?php echo CHtml::activeTextField($details, 'kpp'); ?>
                    <?php echo CHtml::error($details, 'kpp'); ?>
                </td>
            </tr>
            <tr>
                <td class="left top"><span class="red_text">*</span> Расчетный счет:</td>
                <td class="right">
                    <?php echo CHtml::activeTextField($details, 'current_account'); ?>
                    <?php echo CHtml::error($details, 'current_account'); ?>
                </td>
            </tr>
            <tr>
                <td class="left top"><span class="red_text">*</span> Наименование банка:</td>
                <td class="right">
                    <?php
                    $details->bank_name = html_entity_decode($details->bank_name);
                    echo CHtml::activeTextField($details, 'bank_name'); ?>
                    <?php echo CHtml::error($details, 'bank_name'); ?>
                </td>
            </tr>
            <tr>
                <td class="left top"><span class="red_text">*</span> Корреспондентский счет:</td>
                <td class="right">
                    <?php echo CHtml::activeTextField($details, 'correspondent_account'); ?>
                    <?php echo CHtml::error($details, 'correspondent_account'); ?>
                </td>
            </tr>
            <tr>
                <td class="left top"><span class="red_text">*</span> БИК:</td>
                <td class="right">
                    <?php echo CHtml::activeTextField($details, 'bic'); ?>
                    <?php echo CHtml::error($details, 'bic'); ?>
                </td>
            </tr>
            <tr>
                <td class="left top">Система налогообложения:</td>
                <td class="right">
                    <?php
                    $details->taxation_system = html_entity_decode($details->taxation_system);
                    echo CHtml::activeTextField($details, 'taxation_system'); ?>
                    <?php echo CHtml::error($details, 'taxation_system'); ?>
                </td>
            </tr>
        </table>
        <br>
        <div class="footer">
            <div class="button_main">
                <button type="submit">Сохранить</button>
            </div>
            или <a href="<?php echo Yii::app()->createUrl('vendor_cp/company/details'); ?>">Отменить</a>
        </div>
        <?php echo CHtml::endForm(); ?>
    </div>
</div>