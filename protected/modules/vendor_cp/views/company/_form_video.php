    <div class="content b-attach-video_form" id="addVideoForm">
        <form onsubmit="return false;">
            <?php if ($model->isNewRecord): ?>
                <div class="b-title">Ссылка на видеоролик (YouTube или Vimeo) <span class="red_text">*</span></div>
                <input type="text" name="video_url" value="" style="width: 97%">
                <div align="center" style="margin-top: 15px;">
                    <div class="button_main">
                        <button onclick="multimedia.checkVideo(this, $('input[name=video_url]')); return false;">Найти видео</button>
                    </div>
                </div>
                <div id="link_error" class="msg_error" style="display: none"></div>
            <?php endif; ?>

            <div id="video_info"<?php echo isset($model->video_id) ? '' : ' style="display: none"'; ?>>
                <?php if ($model->isNewRecord): ?>
                <div class="b-title">Изображение</div>
                <img src="<?php echo isset($model->thumbnail_url) ? $model->thumbnail_url : '/static/images/nophoto_medium.jpg'; ?>" width="120" height="90">
                <?php endif; ?>

                <div class="b-title">Название <span class="red_text">*</span></div>
                <input type="text" name="title" value="<?php echo isset($model->title) ? $model->title : ''; ?>" style="width: 98%;">

                <div class="b-title">Описание</div>
                <textarea name="description" style="width: 98%;"><?php echo isset($model->description) ? $model->description : ''; ?></textarea>
            </div>
            <input type="hidden" name="video_id" value="">
            <input type="hidden" name="video_provider" value="">
            <input type="hidden" name="thumbnail_url" value="">
        </form>
    </div>
    <div class="buttons">

        <?php if (!$model->isNewRecord): ?>
        <div class="button_main" id="save_video_button">
            <button type="button" onclick="multimedia.updateVideo(this, <?php echo $model->id; ?>);">Сохранить</button>
        </div>
        <?php else: ?>
        <div class="button_main" id="save_video_button" style="display: none">
            <button type="button" onclick="multimedia.saveVideo(this);">Добавить</button>
        </div>
        <?php endif; ?>
        <div class="button_grey">
            <button type="button" onclick="$.fancybox.close();">Отменить</button>
        </div>
    </div>