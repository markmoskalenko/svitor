<div class="b-main_tabs_title">
    <?php echo $address->isNewRecord ? 'Новый адрес' : 'Редактирование адреса'; ?>
</div>
<?php echo CHtml::beginForm(); ?>
<div class="b-vendor-default_form b-main_tabs_box">
    <table>
        <tr id="address_country">
            <td class="left top"><span class="red_text">*</span> Страна:</td>
            <td class="right">
                <?php
                echo CHtml::dropDownList('VendorAddresses[country_id]', '', CHtml::listData($countries, 'country_id', 'name'), array(
                    'empty' => '-- не выбрано --',
                    'onchange' => 'addresses.selectCountry(this);',
                    'class' => 'fixed'
                ));
                ?> <span id="address_region_loader"></span>
                <?php echo CHtml::error($address, 'country_id'); ?>
            </td>
        </tr>
        <tr id="address_region" style="display: none;">
            <td class="left top">Регион:</td>
            <td class="right">
                <?php
                echo CHtml::activeDropDownList($address, 'region_id', array(), array(
                    'empty' => '-- не выбрано --',
                    'onchange' => 'addresses.selectRegion(this);',
                    'class' => 'fixed'
                ));
                ?> <span id="address_city_loader"></span>
                <?php echo CHtml::error($address, 'region_id'); ?>
            </td>
        </tr>
        <tr id="address_city" style="display: none;">
            <td class="left top">Город:</td>
            <td class="right">
                <?php
                echo CHtml::activeDropDownList($address, 'city_id', array(), array(
                    'empty' => '-- не выбрано --',
                    'onchange' => 'addresses.selectCity(this);',
                    'class' => 'fixed'
                ));
                ?> <span id="address_metro_loader"></span>
                <?php echo CHtml::error($address, 'city_id'); ?>
            </td>
        </tr>
        <tr id="address_metro" style="display: none;">
            <td class="left top">Станция метро:</td>
            <td class="right">
                <?php
                echo CHtml::dropDownList('VendorAddresses[metro_id]', '', array(), array(
                    'empty' => '-- не выбрано --',
                    'class' => 'fixed'
                ));
                ?>
            </td>
        </tr>
        <tr>
            <td class="left top">Адрес:</td>
            <td class="right">
                <?php
                $address->address = CHtml::decode($address->address);
                echo CHtml::activeTextField($address, 'address', array('placeholder' => 'пример: ул. Ленина, дом 10')); ?>
                <?php echo CHtml::error($address, 'address'); ?>
            </td>
        </tr>
        <tr>
            <td class="left top">Описание <a onclick="return false" onmouseover="tooltip.show(this, 'Укажите любую полезную информацию о местонахождении вашей компании: торговый центр, номер павильона, название вывески т.д.', {position: 'default', special_class:'white'});">(?)</a>:</td>
            <td class="right">
                <?php
                $address->description = CHtml::decode($address->description);
                echo CHtml::activeTextField($address, 'description', array('placeholder' => '')); ?>
                <?php echo CHtml::error($address, 'description'); ?>
            </td>
        </tr>
        <tr>
            <td class="left top">Время работы:</td>
            <td class="right">
                <?php
                $address->work_time = CHtml::decode($address->work_time);
                echo CHtml::activeTextField($address, 'work_time', array('placeholder' => 'пример: с 9:00 до 21:00')); ?>
                <?php echo CHtml::error($address, 'work_time'); ?>
            </td>
        </tr>
        <tr>
            <td class="left top">Телефон:</td>
            <td class="right">
                <?php
                $address->phone = CHtml::decode($address->phone);
                echo CHtml::activeTextField($address, 'phone', array('placeholder' => 'пример: +7 (916) 123 45 67')); ?>
                <?php echo CHtml::error($address, 'phone'); ?>
            </td>
        </tr>
    </table>
    <div class="footer" style="margin-top: 15px;">
        <div class="button_main">
            <button type="submit"><?php echo $address->isNewRecord ? 'Добавить' : 'Сохранить'; ?></button>
        </div>
        или <a href="<?php echo Yii::app()->createUrl('vendor_cp/company/addresses') ?>">вернуться к списку адресов</a>
    </div>
</div>
<?php echo CHtml::endForm(); ?>
<script src="/static/vendor/js/v_addresses.js?<?php echo Yii::app()->params['version']['js']; ?>"></script>
<script src="/static/js/jquery.form_nav.js?<?php echo Yii::app()->params['version']['js']; ?>"></script>
<script>
$(document).ready(function () {
    addresses.init({
        country : <?php echo !empty($address->country_id) ? $address->country_id : Yii::app()->user->getState('country_id', Yii::app()->params['default_country']); ?>,
        region : <?php echo !empty($address->region_id) ? $address->region_id : 0; ?>,
        city : <?php echo !empty($address->city_id) ? $address->city_id : 0; ?>,
        metro : <?php echo !empty($address->metro_id) ? $address->metro_id : 0; ?>
    });
    <?php if (!$address->isNewRecord): ?>$('form').FormNav('У вас есть не сохраненные изменения. Если вы хотите их сохранить, нажмите кнопку «Сохранить» в нижней части формы.');<?php endif; ?>
});
</script>