<div class="b-main_tabs_title">
    Добавлено адресов <span id="addressCounter"><?php echo $count; ?></span> / <?php echo $max_count; ?>
</div>
<div class="b-main_tabs_box">
    <div class="list_data">
        <?php
			$infotext = '<p class="infotext">Адрес используется при выборе пользователем компании по характеристикам. Если адрес не заполнен, пользователь , в большинстве случаев, не найдет Вас в каталоге.<br/>
Если вы работайте по всей России вам достаточно указать страну без дальнейшей детализации адреса. Обратите внимание, адрес, это не то, где вы находитесь (хотя в случае с офисами или магазинами это именно так), а там где вы ГОТОВЫ находиться, работать.<br/>
Примеры допустимых адресов:<br/>
1. Россия<br/>
2. Россия - Московская область - Балашиха<br/>
3. Россия - Московская область - Балашиха - ул. Ленина 34</p>';
        if (!empty($addresses_data))
        {
            foreach ($addresses_data as $address)
            {
                echo '<div class="element active" id="element_'.$address['id'].'">';

                echo $address['Country']['name'];

                if (!empty($address->region_id))
                    echo ', ' . $address['Region']['name'];

                if (!empty($address->city_id))
                    echo ', ' . $address['City']['name'];

                if (!empty($address->metro_id))
                    echo ', м. '.$address['Metro']['name'];

                if (!empty($address->address))
                    echo ', '.$address['address'];

                if (!empty($address->description))
                    echo ', '.$address['description'];

                echo '<div class="edit_links">
                    <a href="' . Yii::app()->createUrl('vendor_cp/company/addresses', array('id' => $address['id'])) . '/edit/">Редактировать</a> |
                    <a onclick="addresses.deleteItem(this, '.$address['id'].'); return false;">Удалить</a></div>';
                echo '</div>';
            }
			echo $infotext;
        }
        else
        {
            echo '<div class="element empty">Вы пока не добавили ни одного адреса...</div>';
			echo $infotext;
        }
        ?>
    </div>
    <div class="button_main" id="addAddressButton" <?php if ($count >= $max_count) echo 'style="display:none;"'; ?>>
        <button onclick="location.assign('<?php echo Yii::app()->createUrl('vendor_cp/company/addresses'); ?>/add');">Добавить</button>
    </div>
</div>
<div id="trash-dialog" title="Удалить адрес?" style="display: none;">Вы действительно хотите удалить этот адрес?</div>
<script>
$(document).ready(function () {
    addresses.init({
        count : <?php echo $count; ?>,
        max_count : <?php echo $max_count; ?>
    });
});
</script>
<script src="/static/vendor/js/v_addresses.js?<?php echo Yii::app()->params['version']['js']; ?>"></script>