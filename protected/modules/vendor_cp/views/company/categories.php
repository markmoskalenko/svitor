<div class="b-main_tabs_title">
    Выбрано разделов <span id="categoryCounter"><?php echo $cat_count; ?></span> / <?php echo $max_count; ?>
</div>
<div class="b-main_tabs_box">
    <div class="list_data">
    <?php
		$infotext = '<p class="infotext">Правильный выбор разделов и характеристик позволит пользователям найти именно вас, среди множества компаний представленных в каталоге.</p>';
        if (!empty($vendor_category))
        {
            foreach ($vendor_category as $cat)
            {
                echo '<div class="element active" id="element_'.$cat['id'].'">';
                echo $cat['Category']['title'];
                echo '<div class="edit_links">
                    <a href="' . Yii::app()->createUrl('vendor_cp/company/categories', array('id' => $cat['id'])) . '/edit/">Редактировать характеристики</a> |
                    <a onclick="categories.removeCat(this, '.$cat['id'].'); return false;">Удалить</a></div>';
                echo '</div>';
            }
			echo $infotext;
        }
        else
        {
            echo '<div class="element empty">Вы не выбрали ни одного раздела...</div>';
			echo $infotext;
        }
		
    ?>
    </div>
    <div class="button_main" id="addCategoryButton" <?php if ($cat_count >= $max_count) echo 'style="display:none;"'; ?>>
        <button onclick="location.assign('<?php echo Yii::app()->createUrl('vendor_cp/company/categories'); ?>/add');">Добавить</button>
    </div>
</div>
<div id="trash-dialog" title="Удалить раздел?" style="display: none;">Ваша компания не будет показываться в этом разделе после его удаления.<br><br>Вы действительно хотите удалить этот раздел?</div>
<script>
    $(document).ready(function () {
        categories.init(<?php echo $cat_count.', '.$max_count; ?>);
    });
</script>
<script src="/static/vendor/js/v_categories.js?<?php echo Yii::app()->params['version']['js']; ?>"></script>