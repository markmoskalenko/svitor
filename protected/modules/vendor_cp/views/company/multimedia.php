<div class="b-main_tabs_box">
	<?php $infotext = '<p class="infotext">Лучше 1 раз увидеть, чем сто раз услышать. Если вам есть, что показать пользователям - сделайте это. Желательно размещать фотографии в разрешении не меньше чем 200*200px, также рекомендуется размещать фотографии одного размера - это позволит пользователям наслаждаться просмотром.</p>'; 
		echo $infotext;
?>
    <div class="b-media-thumbs_title">Изображения <span id="images_counter"><span><?php echo $images_count; ?></span> / <?php echo $max_images; ?></span></div>
    <div class="b-media-thumbs_box">
        <div class="b-media-thumbs" id="images_thumbs">
            <?php
            if (!empty($images))
                foreach ($images as $img)
                {
                    echo '<div class="img" id="img_'.$img['id'].'">
                    <a class="upbox-button" rel="upbox-button" href="'.Yii::app()->ImgManager->getUrlById($img->img_id, 'large', $img->img_filename).'" title="' . $img->description . '">
                    <img src="'.Yii::app()->ImgManager->getUrlById($img->img_id, 'medium', $img->img_filename).'"></a>
                    <a class="delete magic_button" onclick="multimedia.remove(\'image\', this, '.$img['id'].'); return false;" onmouseover="tooltip.show(this, \'Удалить\', {position:\'top-right\'});"></a>
                    <br><a onclick="multimedia.editImage(' . $img->id . '); return false;">редактировать</a>
                    </div>';
                }
            ?>
        </div>
        <div class="b-default_button_box" id="addImgButton" <?php echo $images_count >= $max_images ? 'style="display: none;"' : ''; ?>>
            <div class="button_main">
                <button type="button" id="add_img">Добавить</button>
            </div>
            <span id="loader"></span>
        </div>
    </div>

    <div class="b-media-thumbs_title">Видео <span id="video_counter"><span><?php echo $videos_count; ?></span> / <?php echo $max_videos; ?></span></div>
    <div class="b-media-thumbs_box">
        <div class="b-media-thumbs" id="videos_thumbs">
            <?php
            if (!empty($videos))
                foreach ($videos as $video)
                {
                    $video_url = '';

                    echo '<div class="video" id="video_' . $video['id'] . '">';

                    if ($video['video_provider'] == 'youtube') {
                        $video_url = 'http://www.youtube.com/watch?v=' . $video['video_id'];
                    }
                    else if ($video['video_provider'] == 'vimeo') {
                        $video_url = 'http://vimeo.com/' . $video['video_id'];
                    }

                    echo '<a class="upbox-media" title="' . $video['title'] . '" href="' . $video_url . '">';
                    echo '<img src="' . $video['thumbnail_url'] . '" width="120" height="90">';
                    echo '</a><br>';
                    echo '<a onclick="multimedia.editVideo(' . $video['id'] . '); return false;">редактировать</a>
                          <a class="delete magic_button" onclick="multimedia.remove(\'video\', this, ' . $video['id'] . '); return false;" onmouseover="tooltip.show(this, \'Удалить\', {position:\'top-right\'});"></a>';
                    echo '</div>';
                }
            ?>
        </div>
        <div class="b-default_button_box" id="addVideoButton" <?php echo $videos_count >= $max_videos ? 'style="display: none;"' : ''; ?>>
            <div class="button_main">
                <button type="button" onclick="multimedia.videoAddForm(); return false;">Добавить</button>
            </div>
        </div>
    </div>

</div>
<script>
$(document).ready(function () {
    multimedia.init(<?php echo Vendor::getId(); ?>, <?php echo $images_count; ?>, <?php echo $videos_count; ?>, <?php echo $max_images; ?>, <?php echo $max_videos; ?>);
    multimedia.uploadImg($('#add_img'));
});
</script>
<link rel="stylesheet" type="text/css" href="/static/fancybox/helpers/jquery.fancybox-buttons.css?<?php echo Yii::app()->params['version']['css']; ?>">
<script type="text/javascript" src="/static/fancybox/helpers/jquery.fancybox-buttons.js?<?php echo Yii::app()->params['version']['js']; ?>"></script>
<script type="text/javascript" src="/static/fancybox/helpers/jquery.fancybox-media.js?<?php echo Yii::app()->params['version']['js']; ?>"></script>

<script src="/static/js/ajaxupload.js?<?php echo Yii::app()->params['version']['js']; ?>"></script>
<script src="/static/vendor/js/v_multimedia.js?<?php echo Yii::app()->params['version']['js']; ?>"></script>
