<?php echo CHtml::beginForm(); ?>
    <div class="b-main_tabs_title">
        Новый раздел
    </div>
    <div class="b-vendor-default_form b-main_tabs_box">
        <table>
            <tr id="categories">
                <td class="left top"><span class="red_text">*</span> Раздел:</td>
                <td class="right" colspan="3">
                    <select name="VendorCategory[cat_id]" id="category_options" class="fixed" onchange="categories.loadAttributes($(this).val())">
                        <?php $this->widget('application.modules.vendor_cp.widgets.AllCats', array('model' => $model,'type'=>'vendors')); ?>
                    </select>
                    <span id="cat_loader"></span>
                    <?php echo CHtml::error($model, 'cat_id'); ?>
                </td>
            </tr>
        </table>
    </div>

    <div id="attributes" style="<?php echo empty($model->cat_id) || $model->fun_type == '' ? 'display: none; ' : ''; ?>margin-top: 15px;">
        <div class="b-main_tabs_title">
            Характеристики
        </div>
        <div class="b-vendor-default_form b-main_tabs_box">
            <div id="attributes_data">
                <?php
                    if (!empty($model->cat_id))
                    $this->widget('application.modules.vendor_cp.widgets.AttrsByCatId', array(
                        'cat_id' => $model->cat_id,
                        'vendor_id' => Vendor::getId()
                    ));
                ?>
            </div>
        </div>
    </div>

    <div class="b-vendor-default_form b-main_tabs_box" style="margin-top: 15px;">
        <div class="footer">
            <div class="button_main">
                <button type="submit">Добавить</button>
            </div>
            или <a href="<?php echo Yii::app()->createUrl('vendor_cp/company/categories'); ?>">вернуться к списку разделов</a>
        </div>
    </div>
<?php echo CHtml::endForm(); ?>
<script>
$(document).ready(function() {
    categories.initForm(<?php echo !empty($model->cat_id) ? $model->cat_id : 'null'; ?>);
});
</script>
<script src="/static/vendor/js/v_categories.js?<?php echo Yii::app()->params['version']['js']; ?>"></script>