<?php foreach ($messages as $message): ?>
<tr>
    <td class="actions">
        <label><input type="checkbox" name="msg[]" value="<?php echo $message['id']; ?>" onchange="messages.select(this);"></label>
    </td>

    <?php if (!empty($message['to_vid'])): ?>
        <td class="avatar">
            <a href="<?php echo Yii::app()->createUrl('vendor/profile', array('id' => $message->to_vid)); ?>">
                <img src="<?php echo Yii::app()->ImgManager->getUrlById($message['ToVendor']['img_id'], 'small', $message['ToVendor']['img_filename']); ?>">
            </a>
        </td>
        <td class="author">
        <?php
            $author = mApi::cutStr($message['ToVendor']['title'], 15);

            echo '<a href="' . Yii::app()->createUrl('vendor/profile', array('id' => $message->to_vid)) . '" title="'.$message['ToVendor']['title'].'">';
            echo /*$message->to_status == "new" ? '<b>' . $author . '</b>' :*/ $author;
            echo '</a>';
        ?>
        </td>

    <?php else : ?>

        <td class="avatar">
            <a href="<?php echo Yii::app()->createUrl('user/profile', array('id' => $message->to_uid)); ?>">
                <img src="<?php echo Yii::app()->ImgManager->getUrlById($message['ToUser']['img_id'], 'small', $message['ToUser']['img_filename']); ?>">
            </a>
        </td>
        <td class="author">
        <?php
            $author = mApi::cutStr($message['ToUser']['login'], 15);

            echo '<a href="' . Yii::app()->createUrl('user/profile', array('id' => $message->to_uid)) . '" title="' . $message['ToUser']['login'] . '">';
            echo /*$message->to_status == "new" ? '<b>'.$author.'</b>' :*/ $author;
            echo '</a>';
        ?>
        </td>
    <?php endif; ?>

    <td class="msg">
        <?php
        //if ($message->to_status == 'new') {
            //echo '<a href="'.Yii::app()->createUrl('vendor_cp/messages/view', array('id' => $message->id)).'"><b>'.$message['title'].'</b></a>';
        //} else {
            echo '<a href="'.Yii::app()->createUrl('vendor_cp/messages/view', array('id' => $message->id)).'">'.$message['title'].'</a>';
        //}
        ?>
    </td>
    <td class="date" onclick="location.assign('<?php echo Yii::app()->createUrl('vendor_cp/messages/view', array('id' => $message->id)); ?>');">
        <span onmouseover="tooltip.show(this, 'Отправлено в <?php echo date('H:i', $message['date']); ?>', {position:'top-right'});"><?php echo mApi::getDateWithMonth($message['date'], 1); ?></span>
    </td>
</tr>
<?php endforeach; ?>