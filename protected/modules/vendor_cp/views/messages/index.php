<h1>Сообщения</h1>

<div class="simple_folder_tabs">
    <ul>
        <li<?php if ($folder == 'inbox') echo ' class="active"'; ?>>
            <a href="<?php echo Yii::app()->createUrl('vendor_cp/messages/index', array('folder' => 'inbox')); ?>">Входящие</a>
        </li>
        <li<?php if ($folder == 'outbox') echo ' class="active"'; ?>>
            <a href="<?php echo Yii::app()->createUrl('vendor_cp/messages/index', array('folder' => 'outbox')); ?>">Отправленные</a>
        </li>
        <li<?php if (Yii::app()->controller->action == 'support') echo ' class="active"'; ?> style="float: right;">
            <a class="help" href="<?php echo Yii::app()->createUrl('vendor_cp/messages/support'); ?>">Получить помощь</a>
        </li>
    </ul>
</div>

<div class="messages_action_buttons">
    <div class="delete">
        <div class="counter">Выделено <span class="selected_msg_count"><b>0</b> сообщений</span></div>
        <div class="button_main">
            <button onclick="messages.deleteSelected(this, '<?php echo $folder; ?>');">Удалить</button>
        </div>
    </div>
</div>

<?php if (!empty($messages)): ?>

    <table class="messages_table">
        <tr>
            <th>
                <input type="checkbox" name="select_all" value="" onclick="$(':checkbox').attr('checked', $(this).is(':checked')); messages.select(this);">
            </th>
            <th><?php echo $folder == 'inbox' ? 'От' : 'Кому'; ?></th>
            <th></th>
            <th>Тема</th>
            <th>Дата</th>
        </tr>
        <?php $this->renderPartial('_'.$folder, array('messages' => $messages)); ?>
    </table>

    <div class="messages_pagination">
        <?php
        $this->widget('CLinkPager', array(
            'pages' => $dataProvider->pagination,
            'htmlOptions' => array('class' => 'b-default_pager'),
            'firstPageLabel' => '',
            'lastPageLabel' => '',
            'prevPageLabel' => '&larr; Назад',
            'nextPageLabel' => 'Далее &rarr;',
            'header' => '',
            'cssFile' => false
        ));
        ?>
    </div>
    <script src="/static/vendor/js/v_messages.js?<?php echo Yii::app()->params['version']['js']; ?>"></script>
<?php else: ?>
    <div class="messages_empty">Нет сообщений...</div>
<?php endif; ?>