<h2>Сообщения</h2>

<div class="simple_folder_tabs">
    <ul>
        <li>
            <a href="<?php echo Yii::app()->createUrl('vendor_cp/messages/index', array('folder' => 'inbox')); ?>">Входящие</a>
        </li>
        <li>
            <a href="<?php echo Yii::app()->createUrl('vendor_cp/messages/index', array('folder' => 'outbox')); ?>">Отправленные</a>
        </li>
        <li class="active" style="float: right;">
            <a href="<?php echo Yii::app()->createUrl('vendor_cp/messages/support'); ?>">Получить помощь</a>
        </li>
    </ul>
</div>
<br/>
<table class="simple_table messages_table">
    <tbody><tr>
        <th>&nbsp;</th>
    </tr>
    </tbody>
</table>
<div class="content" id="newMsgForm">
    <div id="msg"></div>
    <form onsubmit="return false;">
        <div class="label_name">Тема <span style="color: red;">*</span></div>
        <div class="field">
            <?php
            $msg->title = CHtml::decode($msg->title);
            echo CHtml::activeTextField($msg, 'title', array(
                'style' => 'width: 45%;',
                'required' => 'required'
            ));
            ?>
        </div>
        <br/>
        <div class="field">
            <div class="label_name">Отправка сообщения в тех. поддержку <span style="color: red;">*</span></div>
            <div class="field">
                <?php
                echo CHtml::activeTextArea($msg, 'message', array('style' => 'width: 55%; height: 70px;'));
                ?>
            </div>
        </div>
    </form>
</div>
<br/>
<div class="buttons">
    <div class="button_main">
        <button type="button"
                onclick="msg.send(this, <?php echo $to_uid; ?>, <?php echo $to_vid; ?>);">
            Отправить
        </button>
    </div>
</div>

<script>
    var msg = {
        send:function (trigger, to_uid, to_vid) {
            var msg = $('#msg');
            var url = '/messages/new/';
            var old_button_content = $(trigger).html();
            $(trigger).html($.ajaxLoader()).attr('disabled', true);

            if (to_uid != 0) {
                url += 'uid/' + to_uid;
            } else {
                url += 'vid/' + to_vid;
            }

            var data = $('#newMsgForm form').serialize();

            $.post(url, data, function (re) {
                if (re == 'ok') {
                    document.location = '/vendor_cp/messages/';
                } else {
                    $(trigger).html(old_button_content).removeAttr('disabled');
                    msg.html(re).fadeIn(200);
                }
            });
        }
    };
</script>