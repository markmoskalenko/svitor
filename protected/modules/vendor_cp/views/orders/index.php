<div class="main_content notices">
    <?php
    $this->widget(
        'zii.widgets.grid.CGridView',
        array(
             //         'filter' => $model,
             'dataProvider' => new CArrayDataProvider($model),
             'htmlOptions'  => array('class' => 'grid-view'),
             'columns'      => array(
                 array(
                     'name'        => 'id',
                     'header'      => 'Заказы:',
                     'type'        => 'raw',
                     'value'       => 'CHtml::link("№".$data->id,Yii::app()->createUrl("/vendor_cp/orders/view", array("id"=>$data->id)))',
                     'htmlOptions' => array(
                         'width' => '55px',
                     ),
                 ),
                 array(
                     'header'      => 'Покупатель:',
                     'htmlOptions' => array(
                         'width' => '300px',
                     ),
                     'type'        => 'raw',
                     'value'       => 'CHtml::link($data->user->login, Yii::app()->createUrl("/user/profile",array("id"=>$data->user_id)))',
                 ),
                 array(
                     'header' => 'Контакты:',
                     'type'   => 'raw',
                     'value'  => '$data->surname." ".$data->name."<br>".$data->phone',
                 ),
                 array(
                     'header' => 'Статус:',
                     'name'   => 'status',
                     'type'   => 'raw',
                     'value'  => 'Orders::getStatusTitle($data->status).($data->status == Orders::STATUS_NOT_CONFIRMED ? " - ".CHtml::link("Подтвердить",Yii::app()->createUrl("/cart/confirmOrder", array("order_id"=>$data->id))) : "")',
                     'filter' => Orders::getStatusOnFilter(),
                 ),
             ),
        )
    );
    ?>

</div>
