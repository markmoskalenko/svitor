<?php
Yii::app()->clientScript->registerScriptFile('/static/js/m_cart.js', CClientScript::POS_HEAD); ?>

<?php
function template($data, $column){
    return '
    <span class="'.$column.'_active_value" onclick="cart.editVal(this, '.$data->id.', \'int\');"
    onmouseover="tooltip.show(this, \'Кликните для быстрого редактирования\', {position :\'top-right\', delay : 2000});">'.$data->{$column}.' '.($column != 'delivery' ? ('/ шт.') : '').'</span>
    <div class="input_shadow">'.
      CHtml::textField($column,$data->{$column},array("onkeyup"=>"cart.checkIntValue(this);", "class"=>"edit_input","style"=>"display:none; width:50px;"))
    .'</div>';
}
?>

<div class="row">Покупатель: <?php echo CHtml::link($model->user->login, $this->createUrl('/user/profile', array('id'=>$model->user->id))); ?></div>
<div class="row"><?php echo $model->surname." ".$model->name; ?>, тел.: <?php echo $model->phone; ?></div>
<div class="main_content notices">
    <?php
    $this->widget('zii.widgets.grid.CGridView', array(
         'dataProvider' => new CArrayDataProvider($model->bondOrders),
         'htmlOptions'  => array('class' => 'grid-view'),
         'id'=>'grid-view',
         'columns' => array(
             array(
                 'header'      => 'Товары:',
                 'type'        => 'raw',
                 'value'       => '!empty($data->product->FirstImage->img_id) ? CHtml::image(Yii::app()->ImgManager->getUrlById($data->product->FirstImage->img_id, "medium", $data->product->FirstImage->img_filename)) : ""',
                 'htmlOptions' => array('width' => '100px')
             ),
             array(
                 'header' => '',
                 'value'  => '$data->product->title'
             ),
             array(
                 'header' => 'Количество:',
                 'type'=>'raw',
                 'value'  =>'$data->order->status != Orders::STATUS_CLOSED && $data->order->status != Orders::STATUS_SENT ? template($data, "count") : $data->count." шт."',
                 'htmlOptions' => array('width' => '50px', 'class'=>'ajax')
             ),
             array(
                 'header' => 'Цена, руб.:',
                 'type'=>'raw',
                 'value'  => '$data->order->status != Orders::STATUS_CLOSED && $data->order->status != Orders::STATUS_SENT ? template($data, "price") : $data->price." / шт."',
                 'htmlOptions' => array('width' => '100px', 'class'=>'ajax')
             ),
             array(
                 'header' => 'Сумма, руб.:',
                 'value'  => '$data->price * $data->count',
                 'htmlOptions' => array('width' => '100px')

             ),
         ),
    ));
    ?>
    <div id="delivery_wrapper" style="border: 1px solid #000; padding: 20px 0 20px 112px; <?php echo $model->delivery==0 ? "display:none;" : ""; ?>">
        Доставка
        <div style="float: right;width:100px;text-align:center" class="ajax">
            <?php echo ($model->status != Orders::STATUS_CLOSED  && $model->status != Orders::STATUS_SENT) ? template($model, 'delivery') : 20; ?>
        </div>
    </div>

    <?php if( $model->status != Orders::STATUS_CLOSED  && $model->status != Orders::STATUS_SENT ): ?>
	<div style="margin-top:10px">

        <div style="float:right" id="sum">Сумма, руб.:<?php echo $model->sum+$model->delivery;?></div>
        <div class="cart button_main"><?php echo CHtml::link('Добавить товар', $this->createUrl('/vendor_cp/orders/addProduct', array('id'=>$model->id)),array('class'=>'button')); ?></div>

        <?php if($model->delivery == 0): ?>
            <div class="button_main"><?php echo CHtml::link('Добавить в заказ доставку', 'javascript:void(0)', array('id'=>'add-delivery','class'=>'button')); ?></div>
            <div id="redirect-dialog" style="display: none;" title="Доставка товара"><br>Вы уверены что хотите добавить доставку товара?</div>
			</div>
        <?php endif; ?>


        <!--end form-->
    <?php endif; ?> <!-- $model->status != Orders::STATUS_CLOSED-->

    <?php if( $model->status != Orders::STATUS_CLOSED ): ?>
    <!--begin form-->
    <?php echo CHtml::form('/vendor_cp/orders/changeStatus', 'get'); ?>
    <?php echo CHtml::hiddenField('orderId',$model->id) ?>

    <div>Статус:
        <?php echo CHtml::dropDownList(
            'status',
            $model->status,
            Orders::getStatusOnFilter(),
            array('onchange' => 'this.form.submit()', 'options'=>array(Orders::STATUS_NOT_CONFIRMED => array('disabled'=>'disabled'), Orders::STATUS_QUEUED => array('disabled'=>'disabled'))));
        ?>
    </div>

    <?php echo CHtml::endForm(); ?>

    <?php endif; ?>

    <div class="row">
            <p>Комментарии к заказу:</p>
            <?php echo CHtml::form('/vendor_cp/orders/changeComment'); ?>
        <div>
            <?php echo CHtml::hiddenField('orderId',$model->id) ?>
            <?php echo CHtml::textArea('vendor_comment',$model->vendor_comment, array('style'=>'width:500px;')); ?>
        </div>
        <div>
           <div class="button_main"> <?php echo CHtml::submitButton('Сохранить',array('class'=>'button')) ?></div>
        </div>
            <?php echo CHtml::endForm(); ?>
    </div>
    <!--end form-->
</div>

<?php if($model->delivery == 0): ?>
<script type="text/javascript">
    $(function(){
        $('#add-delivery').click(function(){
            $('#redirect-dialog').dialog({
                'modal' : true,
                'resizable' : false,
                'width' : 450,
                'buttons' : [
                    {
                        text : 'Да',
                        click : function () {
                            $.post('/vendor_cp/orders/saveAjaxValue/',{ id: <?php echo $model->id; ?>, name: "delivery", value: 20 },
                              function (response) {
                                    $("#delivery").show();
                                    var obj = jQuery.parseJSON(response);
                                    if(obj.r = 'ok'){
                                        $('#sum').text('Сумма, руб.:'+obj.s);
                                        $('#delivery .ajax .delivery_active_value').text(obj.d);
                                    }
                                    $.fn.yiiGridView.update('grid-view');
                              }
                            );
                            $(this).dialog('close');
                        }
                    },
                    {
                        text : 'Нет',
                        click : function () {
                            $(this).dialog('close');
                        }
                    }
                ]
            });
        });
    });
</script>
<?php endif; ?>