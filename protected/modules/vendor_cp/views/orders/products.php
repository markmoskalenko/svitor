<h1>Товары</h1>
<p>Добавить товары к заказу</p>
<div class="main_content notices">
    <?php echo CHtml::form();  ?>
   <div class="button_main"><?php echo CHtml::submitButton('Добавить',array('class'=>'button'));  ?></div>
    <?php $this->widget('zii.widgets.grid.CGridView', array(
         'dataProvider' => new CArrayDataProvider($model),
         'htmlOptions'  => array('class' => 'grid-view'),
         'columns' => array(
             array(
                 'header'=>CHtml::checkBox('all',false,array(
                                                            'onclick'=>'
                                                            if($(this).is(":checked")){
                                                                $(".ch").attr("checked",true).trigger("change");
                                                            }else{
                                                                $(".ch").attr("checked",false).trigger("change");

                                                            }
                                                        '
                                                       )),
                 'type' => 'raw',
                 'value' => 'CHtml::checkBox("products_ids[]",false,array("value"=>$data->id,"class"=>"ch"))',
                 'htmlOptions'=>array('width'=>'16px')
             ),
             array(
                 'header'      => 'Название',
                 'type'        => 'raw',
                 'value'       => '!empty($data->FirstImage->img_id) ? CHtml::image(Yii::app()->ImgManager->getUrlById($data->FirstImage->img_id, "medium", $data->FirstImage->img_filename)) : ""',
                 'htmlOptions' => array('width' => '100px')
             ),
             array(
                 'header' => '',
                 'value'  => '$data->title'
             ),
             array(
                 'header' => 'Цена, руб.',
                 'type'=>'raw',
                 'value'  => '$data->price',
                 'htmlOptions' => array('width' => '100px', 'class'=>'ajax')
             ),
             array(
                 'header' => 'Наличие',
                 'type'=>'raw',
                 'value'  => '$data->in_stock == 0 ? "нет" : $data->in_stock == 1 ? "есть" : "под заказ"',
                 'htmlOptions' => array('width' => '100px', 'class'=>'ajax')
             ),
         ),
    ));
    echo CHtml::endForm();
    ?>

</div>
