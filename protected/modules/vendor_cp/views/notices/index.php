<h1>Уведомления</h1>

<div class="main_content notices">

<?php if (!empty($notices)): ?>

    <?php foreach ($notices as $notice): ?>
    <?php
        if (date('d.m.Y', $notice->date) == date('d.m.Y', time())) {
            $date = 'Сегодня в ' . date('H:i', $notice->date);
        } else {
            $date = mApi::getDateWithMonth($notice->date) . ' в ' . date('H:i', $notice->date);
        }
    ?>
        <div class="notice">
            <span class="date"><?php echo $date; ?></span>
            <?php UserLogVendors::getEventInfo($notice); ?>
        </div>

    <?php endforeach; ?>

    <div class="l_fl">
        <?php
        $this->widget('CLinkPager', array(
            'pages' => $dataProvider->pagination,
            'htmlOptions' => array('class' => 'b-default_pager'),
            'firstPageLabel' => '',
            'lastPageLabel' => '',
            'prevPageLabel' => '&larr; Назад',
            'nextPageLabel' => 'Далее &rarr;',
            'header' => ''
        ));
        ?>
    </div>

<?php else: ?>
    Нет уведомлений...
<?php endif; ?>

</div>