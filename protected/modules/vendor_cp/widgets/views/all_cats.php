<option value="">--- выберите раздел ---</option>
<?php
if (!empty($categories_data))
{
    foreach ($categories_data as $category)
    {
        $select = '';
		$haschild = false;
		$children = '';
        if ($model->cat_id == $category['id'])
            $select = ' selected="selected"';

        if ($category->lft == 1)
        {
            if (!$category->isLeaf() && $category->isRoot())
            {
               
                foreach ($categories_data as $sub_category)
                {
					$subselect = '';
                    if ($category['root'] == $sub_category['root'] && $category['id'] != $sub_category['id']){
						if ($model->cat_id == $sub_category['id'])
							$subselect = ' selected="selected"';
                        $children .= '<option value="'.$sub_category['id'].'"'.$subselect.'>- '.$sub_category['title'].'</option>';
						$haschild = true;
						}
                }
				if ($haschild){
				 echo '<optgroup label="'.$category['title'].'">';
				 echo $children;
                echo '</optgroup>';
				}
				else{
				echo '<option value="'.$category['id'].'"'.$select.'>'.$category['title'].'</option>';
					}
            }
            else
            {
                echo '<option value="'.$category['id'].'"'.$select.'>'.$category['title'].'</option>';
            }
        }
    }
}
else
{
    echo '<option value="" disabled="disabled">нет категорий...</option>';
}
?>