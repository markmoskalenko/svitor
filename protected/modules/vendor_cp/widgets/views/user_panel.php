<ul>
<?php if (Vendor::isGuest()): ?>

    <li>
        <a href="<?php echo Yii::app()->createUrl('vendor_cp/user/login'); ?>">Войти</a>
    </li>
    <li>
        <a href="<?php echo Yii::app()->createUrl('vendor_cp/user/registration'); ?>">Зарегистрировать компанию</a>
    </li>

<?php else: ?>
    <?php $msg_count = UserMessages::getCountInbox('vendor_cp'); ?>
    <?php $notice_count = UserLogVendors::getCountNewNotices(); ?>

    <li>
        <b><?php echo Yii::app()->user->name; ?></b>
    </li>
    <li>
        <a href="<?php echo Yii::app()->createUrl('vendor_cp/notices'); ?>">Уведомления <?php if (!empty($notice_count)) echo '<b>+'.$notice_count.'</b>'; ?></a>
    </li>
    <li>
        <a href="<?php echo Yii::app()->createUrl('vendor_cp/messages'); ?>">Сообщения <?php if (!empty($msg_count)) echo '<b>+'.$msg_count.'</b>'; ?></a>
    </li>
    <li>
        <a href="<?php echo Yii::app()->createUrl('vendor_cp/user/settings'); ?>">Настройки</a>
    </li>
    <li class="magic_button">
        <a href="<?php echo Yii::app()->createUrl('vendor_cp/user/logout'); ?>">Выход</a>
    </li>

<?php endif; ?>
</ul>