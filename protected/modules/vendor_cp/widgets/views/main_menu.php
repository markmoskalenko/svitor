<div class="b-vendor_mainmenu">
    <ul>
        <li<?php if ($controller_name == 'company') echo ' class="active"'; ?>>
            <a href="<?php echo Yii::app()->createUrl('vendor_cp/company'); ?>">Информация о компании</a>
        </li>
        <li<?php if ($controller_name == 'stock') echo ' class="active"'; ?>>
            <a href="<?php echo Yii::app()->createUrl('vendor_cp/stock'); ?>" style="color: red;">Акции</a>
        </li>
        <li<?php if ($controller_name == 'posts') echo ' class="active"'; ?>>
            <a href="<?php echo Yii::app()->createUrl('vendor_cp/posts'); ?>">Статьи</a>
        </li>
        <li<?php if ($controller_name == 'faq') echo ' class="active"'; ?>>
            <a href="<?php echo Yii::app()->createUrl('vendor_cp/faq'); ?>">FAQ</a>
        </li>
        <li<?php if ($controller_name == 'reviews') echo ' class="active"'; ?>>
            <a href="<?php echo Yii::app()->createUrl('vendor_cp/reviews'); ?>">Отзывы</a>
        </li>
        <li<?php if ($controller_name == 'products') echo ' class="active"'; ?>>
            <a href="<?php echo Yii::app()->createUrl('vendor_cp/products'); ?>">Товары</a>
        </li>
<!--        <li--><?php //if ($controller_name == 'orders') echo ' class="active"'; ?><!-->
<!--            --><?php //echo CHtml::link('Заказы'.' (<b> '.(Orders::getCountNewOrders() ? '+'.Orders::getCountNewOrders() : '0').' </b>)', Yii::app()->createUrl('/vendor_cp/orders')); ?>
<!--        </li>-->
        <li<?php if ($controller_name == 'bid') echo ' class="active"'; ?>>
            <?php echo CHtml::link('Заявки'.' (<b> '.(Bid::countNewBid() ? '+'.Bid::countNewBid() : '0').' </b>)', Yii::app()->createUrl('/vendor_cp/bid')); ?>
        </li>
    </ul>
</div>

<?php if ($controller_name == 'company'):?>
<div class="b-main_tabs">
    <ul class="tabs_button">
        <li<?php if ($action_name == 'generalInfo') echo ' class="active"'; ?>>
            <a href="<?php echo Yii::app()->createUrl('vendor_cp/company/generalInfo'); ?>">Общая информация</a>
        </li>
        <li<?php if ($action_name == 'categories') echo ' class="active"'; ?>>
            <a href="<?php echo Yii::app()->createUrl('vendor_cp/company/categories'); ?>">Разделы каталога</a>
        </li>
        <li<?php if ($action_name == 'addresses') echo ' class="active"'; ?>>
            <a href="<?php echo Yii::app()->createUrl('vendor_cp/company/addresses'); ?>">Адреса</a>
        </li>
        <li<?php if ($action_name == 'multimedia') echo ' class="active"'; ?>>
            <a href="<?php echo Yii::app()->createUrl('vendor_cp/company/multimedia'); ?>">Фото и Видео</a>
        </li>
        <li<?php if ($action_name == 'certificates') echo ' class="active"'; ?>>
            <a href="<?php echo Yii::app()->createUrl('vendor_cp/company/certificates'); ?>">Сертификаты и Лицензии</a>
        </li>
        <li<?php if ($action_name == 'details') echo ' class="active"'; ?>>
            <a href="<?php echo Yii::app()->createUrl('vendor_cp/company/details'); ?>" >Реквизиты</a>
        </li>
    </ul>
</div>
<?php endif; ?>