<?php
$empty_error = '<span class="note">В выбранном разделе нет характеристик.</span><input type="hidden" name="'.$model_name.'[Features]" value=""><br>';

if (!empty($category_attrs))
{
    $html = '';

    foreach ($category_attrs as $attr)
    {
        // Выводим только характеритики для товаров, которые имеют значения
        if ($attr['belongs_to'] != $belongs_to || empty($attr['Values']))
            continue;

        $post_params = Yii::app()->request->getPost($model_name);

        // Значение по умолчанию (после отправки формы, запоминаем выбор)
        $select_value = '';
        if (isset($post_params['Features'][$attr['id']]))
            $select_value = $post_params['Features'][$attr['id']];

        // Обязательная для заполнения?
        $required_html = $attr->required == 1 ? '<span class="red_text">*</span> ' : '';

        $html .= '<tr>';
            $html .= '<td class="left top">'.$required_html. $attr['title'].':</td>';
            $html .= '<td class="right">'.AttrValues::renderByDisplayType($model_name.'[Features]', $attr, $attr['Values'], $select_value, $data_attr_values).'</td>';
        $html .= '</tr>';
    }

    echo !empty($html) ? '<table>'.$html.'</table>' : $empty_error;
}
else
{
    echo $empty_error;
}
?>