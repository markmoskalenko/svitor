<?php
class mainMenu extends CWidget
{
	function init()
	{
        $controller_name = Yii::app()->controller->getId();
        $action_name = Yii::app()->controller->action->getId();


        if (Vendor::isGuest()) {
            $this->render('guest_main_menu', array(
                'controller_name' => $controller_name,
                'action_name' => $action_name
            ));
        }
        else {
		    $this->render('main_menu', array(
                'controller_name' => $controller_name,
                'action_name' => $action_name
            ));
        }
	}
}