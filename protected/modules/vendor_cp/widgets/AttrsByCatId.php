<?php
class AttrsByCatId extends CWidget
{
    public $cat_id;
    public $product_id;
    public $vendor_id;

    private $_belongs_to = 0;
    private $_model_name = 'Product';

    public function init()
    {
        if (empty($this->cat_id))
            throw new ErrorException('Не получен параметр cat_id');
    }

    // input attr-name construction = $_barent_name[Features][$attr_id] = $value_id
    public function run()
    {
        $cat_id = intval($this->cat_id);
        $product_id = intval($this->product_id);
        $vendor_id = intval($this->vendor_id);
        $data_attr_values = array();
        $category_attrs = array();

        $category = Category::model()->findByPk($cat_id);
        if (empty($category))
            Yii::app()->end();

        if (!empty($product_id) && empty($vendor_id))
        {
            $this->_belongs_to = 'products';
            $this->_model_name = 'Product';
            $attr_values = ProductAttrValues::model()->findAllByAttributes(array('product_id' => $product_id));
            $category_attrs = $category->AttrsForProduct;
        }
        else if (!empty($vendor_id) && empty($product_id))
        {

            $this->_belongs_to = 'vendors';
            $this->_model_name = 'Vendor';
            $attr_values = VendorAttrValues::model()->findAllByAttributes(array('cat_id' => $cat_id, 'vendor_id' => $vendor_id));
            $category_attrs = $category->AttrsForVendor;
        }
        else
        {
            $category_attrs = $category->AttrsForProduct;
            $attr_values = array();
        }

        if (!empty($attr_values))
        {
            foreach ($attr_values as $var)
                $data_attr_values[$var['attr_id'].$var['attr_value_id']] = '';
        }

        $this->render('attrs_by_cat_id', array(
            'category' => $category,
            'category_attrs' => $category_attrs,

            'belongs_to' => $this->_belongs_to,
            'model_name' => $this->_model_name,

            'data_attr_values' => $data_attr_values
        ));
    }
}