<?php
class AllCats extends CWidget
{
    public $model;
	public $type;

    public function run()
    {
		if ($this->type == 'products') {
        $attr = array('visible_prod' => 1);	
		}
        else if ($this->type == 'vendors'){
		$attr = array('visible_vndr' => 1);		
		}
		
        $categories_data = Category::model()->findAllByAttributes($attr, array('order' => 'root'));

        $this->render('all_cats', array(
            'categories_data' => $categories_data,
            'model' => $this->model
        ));
    }
}