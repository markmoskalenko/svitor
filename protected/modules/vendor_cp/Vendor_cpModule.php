<?php
class Vendor_cpModule extends CWebModule
{
    public $defaultController = 'Main';
    public $layout = 'main';

    public function init()
    {
        Yii::app()->name = 'Кабинет компании — ' . Yii::app()->name;
        
        Yii::app()->setComponents(array(
            'user' => array(
                'class' => 'CWebUser',
                'stateKeyPrefix' => 'vendor',
                'loginUrl' => Yii::app()->createUrl($this->getId() . '/user/login'),
            ),
        ));
    }
}