<?php
class ModSettings extends CApplicationComponent
{
    public $version = '1.0';
    public $version_str = '1.0 (beta) 02.10.2012';
    public $version_css_styles = '2.15';

    public function init()
    {

    }

    public static  function get($setting)
    {
        $data = new ModSettings;
        return $data->$setting;
    }
}