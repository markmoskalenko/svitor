<h1>Комментарии к статьям</h1>
<?php
if (!empty($comments))
{
    //TODO: привести код к нормальному виду...
    foreach ($comments as $comment)
    {
        $user_profile = Yii::app()->createUrl('user/profile', array('id' => $comment['User']['id']));
        $special_class = '';
        if ($comment->deleted == 1)
            $special_class = ' magic_button';

        echo 'Статья: <b><a href="/post/'.$comment['Post']['id'].'" target="_blank">'.$comment['Post']['title'].'</a></b>';

        echo '<table id="comment_'.$comment['id'].'" class="comment_table'.$special_class.'">';
        echo '<tr>';
        echo '<td valign="top" width="55px">
                  <a href="'.$user_profile.'"><img src="'.Yii::app()->ImgManager->getUrlById($comment['User']['img_id'], 'small', $comment['User']['img_filename']).'"></a>
             </td>';

        echo '<td valign="top" style="padding: 0 5px 5px;">';
        echo '<b><a href="'.$user_profile.'">'.$comment['User']['login'].'</a></b>';

        if ($comment->deleted == 0) {
            echo '<div class="comment_text">'.nl2br($comment['comment']).'</div>';
            echo '<div class="comment_info">'.mApi::getDateWithMonth($comment['date']).' в '.date('H:i', $comment['date']);
            echo '</div>';
        }
        else {
            echo '<div class="comment_deleted">Пользователь удалил свой комментарий...</div>';
        }

        echo '</td>';

        echo '<td style="width: 210px; vertical-align: top;">';

            echo '<div class="button_main">';
                echo '<button onclick="location.assign(\'/mod_cp/comment/approve/id/'.$comment['id'].'\');">Опубликовать</button>';
            echo '</div>';

            echo ' <div class="button_grey">';
                echo '<button onclick="location.assign(\'/mod_cp/comment/reject/id/'.$comment['id'].'\');">Отклонить</button>';
            echo '</div>';

        echo '</td>';

        echo '</table>';
    }
}
else
{
    echo '<div style="margin-top: 15px;">Нет комментариев для проверки...</div>';
}
?>
<br>
<?php
$this->widget('CLinkPager', array(
    'pages' => $dataProvider->pagination,
    'htmlOptions' => array('class' => 'b-default_pager'),
    'firstPageLabel' => '',
    'lastPageLabel' => '',
    'prevPageLabel' => '&larr; Назад',
    'nextPageLabel' => 'Далее &rarr;',
    'header' => ''
));
?>