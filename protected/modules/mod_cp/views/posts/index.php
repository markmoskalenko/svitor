<h1>Необходимо проверить (<?php echo $verify_posts_count; ?>)</h1>
Статьи, написанные компаниями, которые <b>ожидают проверки</b>.
<?php
$this->renderPartial('_posts', array(
    'posts' => $verify_posts,
    'dataProvider' => $verify_dataProvider,
    'type' => 1
));
?>
<br>
<h1>Статьи (<?php echo $posts_count; ?>)</h1>
Это список статей, которые добавлены модераторами. Вы можете написать статью от лица "редакции" сайта.<br><br>
<div class="button_main">
    <button onclick="location.assign('/mod_cp/posts/add/');">Написать</button>
</div>
<?php
$this->renderPartial('_posts', array(
    'posts' => $posts,
    'dataProvider' => $dataProvider,
    'type' => 2
));
?>
