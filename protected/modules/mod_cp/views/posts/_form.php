<?php
$h1_text = 'Новая статья';
$button_text = 'Добавить';

if (!$model->isNewRecord)
{
    $h1_text = ($model->status == 'verified') ? 'Редактирование статьи' : 'Проверка статьи';
    $button_text = ($model->status == 'verified') ? 'Сохранить' : 'Опубликовать';
}
?>

<h1><?php echo $h1_text; ?></h1>
<?php echo CHtml::beginForm(); ?>
<table class="default_table">
    <tr>
        <td class="title">Раздел:</td>
        <td>
            <?php
            $list_data = CHtml::listData($categories, 'id', 'title');
            echo CHtml::activeDropDownList($model, 'cat_id', $list_data, array(
                'empty' => '-- не выбрано --',
                'style' => 'width: 200px;',
            ));
            ?>
            <div class="note"><span class="counter"></span></div>
            <?php echo CHtml::error($model, 'cat_id'); ?>
        </td>
    </tr>
    <tr>
        <td class="title">Название:</td>
        <td id="title">
            <?php
            $model->title = CHtml::decode($model->title);
            echo CHtml::activeTextField($model, 'title', array(
                'style' => 'width: 98%;',
                'onkeyup' => 'formHelper.updateCounter(this, "title", 200);',
                'onchange' => 'formHelper.updateCounter(this, "title", 200);',
                'onclick' => 'formHelper.updateCounter(this, "title", 200);'
            ));
            ?>
            <div class="note"><span class="counter"></span></div>
            <?php echo CHtml::error($model, 'title'); ?>
        </td>
    </tr>
    <tr>
        <td class="title"></td>
        <td>
            <?php echo CHtml::activeTextArea($model, 'full_text'); ?>
            <div class="note"><span class="counter"></span></div>
            <?php echo CHtml::error($model, 'full_text'); ?>
        </td>
    </tr>
    <tr>
        <td class="left top">Изображение</td>
        <td class="right">

            <div id="image_thumbs">
                <?php if (!empty($model->img_id)): ?>
                <img src="<?php echo Yii::app()->ImgManager->getUrlById($model->img_id, 'medium', $model->img_filename); ?>">
                <?php endif; ?>
            </div>
            <input type="hidden" name="InfoPosts[img_id]" id="img_id" value="<?php echo $model->img_id; ?>">
            <input type="hidden" name="InfoPosts[img_filename]" id="img_filename" value="<?php echo $model->img_filename; ?>">
            <br>
            <a id="upload_image">Выбрать файл &rarr;</a>
            <span class="note">(поддерживаются форматы JPG, PNG и GIF, размер не более 2мб)</span>
            <?php echo CHtml::error($model, 'img_filename'); ?>
            <br>
        </td>
    </tr>
    <tr>
        <td class="title">Теги:</td>
        <td>
            <?php
            echo CHtml::activeTextField($model, 'tags', array(
                'style' => 'width: 400px;'
            ));
            ?>
            <div class="note">(ключевые слова должны разделяться запятыми, например: тег1, тег2, тег3)</div>
            <?php echo CHtml::error($model, 'tags'); ?>
        </td>
    </tr>
</table>
<br>
<div class="button_main">
    <button type="submit"><?php echo $button_text; ?></button>
</div>

<?php if (!$model->isNewRecord && $model->status == 'verify'): ?>
    <div class="button_main r_fl">
        <button type="button" onclick="location.assign('/mod_cp/posts/reject/id/<?php echo $model->id; ?>');">Отклонить</button>
    </div>
<?php endif; ?>

<?php if (!$model->isNewRecord && $model->status == 'verified'): ?>
    <a href="/mod_cp/posts/delete/id/<?php echo $model->id; ?>" class="r_fl" onclick="if (!confirm('Вы действительно хотите удалить эту статью?')) return false;">Удалить статью</a>
<?php endif; ?>

    или <a href="/mod_cp/posts/">вернуться к списку</a>
<?php echo CHtml::endForm(); ?>

<script src="/static/ckeditor/ckeditor.js"></script>
<script src="/static/ckeditor/adapters/jquery.js"></script>
<script src="/static/js/ajaxupload.js"></script>
<script src="/static/admin/js/e_infoposts.js"></script>
<script>
$(document).ready(function () {
    $('#InfoPosts_full_text').ckeditor({width : '98%'});
    infopost.uploadImg($('#upload_image'));
});
</script>