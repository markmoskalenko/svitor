<br><br>
<table class="list_table">
    <thead>
    <th width="40px"><b>Дата</b></th>
    <th class="al_l"><b>Название</b></th>
    <th width="200px" class="last_r"><b><?php echo $type == 1 ? 'Автор' : ''; ?></b></th>
    </thead>
    <?php
    if (!empty($posts))
    {
        foreach ($posts as $post) {
            echo '<tr id="element_'.$post['id'].'">';

            echo '<td>
                      <span class="info c_default" onmouseover="tooltip.show(this, \'Отредактировано '.date('d.m.Y в H:i', $post['edited_date']).'\', {position:\'top-right\', special_class:\'white\'});">'.date('d.m.Y', $post['created_date']).'</span>
                  </td>';

            echo '<td><b><a href="'.Yii::app()->createUrl('mod_cp/post/edit', array('id' => $post['id'])).'">'.$post['title'].'</a></b></td>';

            if (!empty($post['User'])) {
                echo '<td class="al_c">&laquo;<a href="'.Yii::app()->createUrl('user/profile', array('id' => $post['User']['id'])).'">'.mApi::cutStr($post['User']['login'], 15).'</a>&raquo;</td>';
            }
            else if (!empty($post['Vendor'])) {
                echo '<td class="al_c">&laquo;<a href="'.Yii::app()->createUrl('vendor/profile', array('id' => $post['Vendor']['id'])).'">'.mApi::cutStr($post['Vendor']['title'], 15).'</a>&raquo;</td>';
            }
            else {
                echo '<td class="al_c"><a href="/post/'.$post['id'].'" target="_blank">посмотреть на сайте &rarr;</a></td>';
            }

            echo '</tr>';
        }
    }
    else
    {
        echo '<tr><td colspan="4" class="al_c">Нет статей для проверки...</td></tr>';
    }
    ?>
</table>
<br>
<div class="r_fl">
    <?php
    $this->widget('CLinkPager', array(
        'pages' => $posts_pagination,
        'htmlOptions' => array('class' => 'b-default_pager'),
        'firstPageLabel' => '',
        'lastPageLabel' => '',
        'prevPageLabel' => '&larr; Назад',
        'nextPageLabel' => 'Далее &rarr;',
        'header' => ''
    ));
    ?>
</div>