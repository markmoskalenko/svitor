<h1>Необходимо проверить (<?php echo $verify_posts_count; ?>)</h1>
Статьи, написанные пользователями или компаниями, которые <b>ожидают проверки</b>.
<?php
$this->renderPartial('_posts', array(
    'posts' => $verify_posts,
    'posts_pagination' => $verify_posts_pagination,
    'type' => 1
));
?>
<br>
<h1>Статьи (<?php echo $posts_count; ?>)</h1>
Это список статей, которые добавлены модераторами. Вы можете написать статью от лица "редакции" сайта.<br><br>
<div class="button_main">
    <button onclick="location.assign('/mod_cp/post/add/');">Написать</button>
</div>
<?php
$this->renderPartial('_posts', array(
    'posts' => $posts,
    'posts_pagination' => $posts_pagination,
    'type' => 2
));
?>
