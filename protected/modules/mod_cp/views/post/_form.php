<?php
$h1_text = 'Новая статья';
$button_text = 'Добавить';

if (!$model->isNewRecord)
{
    $h1_text = ($model->status == 'published') ? 'Редактирование статьи' : 'Проверка статьи';
    $button_text = ($model->status == 'published') ? 'Сохранить' : 'Опубликовать';
}
?>

<h1><?php echo $h1_text; ?></h1>
<?php echo CHtml::beginForm(); ?>
<table class="default_table">
    <tr>
        <td class="title">Раздел:</td>
        <td>
            <?php
            $list_data = CHtml::listData($categories, 'id', 'title');
            echo CHtml::activeDropDownList($model, 'post_cat_id', $list_data, array(
                'empty' => '-- не выбрано --',
                'style' => 'width: 200px;',
            ));
            ?>
            <div class="note"><span class="counter"></span></div>
            <?php echo CHtml::error($model, 'post_cat_id'); ?>
        </td>
    </tr>
    <tr>
        <td class="title">Название:</td>
        <td id="title">
            <?php
            $model->title = CHtml::decode($model->title);
            echo CHtml::activeTextField($model, 'title', array(
                'style' => 'width: 98%;',
                'onkeyup' => 'formHelper.updateCounter(this, "title", 200);',
                'onchange' => 'formHelper.updateCounter(this, "title", 200);',
                'onclick' => 'formHelper.updateCounter(this, "title", 200);'
            ));
            ?>
            <div class="note"><span class="counter"></span></div>
            <?php echo CHtml::error($model, 'title'); ?>
        </td>
    </tr>
    <tr>
        <td class="title t_va">Контент 1 <br> (превью):</td>
        <td>
            <?php echo CHtml::activeTextArea($model, 'content_1'); ?>
            <div class="note"><span class="counter"></span></div>
            <?php echo CHtml::error($model, 'content_1'); ?>
        </td>
    </tr>
    <tr>
        <td class="title t_va">Контент 2 <br> (продолжение):</td>
        <td>
            <?php echo CHtml::activeTextArea($model, 'content_2'); ?>
            <div class="note"><span class="counter"></span></div>
            <?php echo CHtml::error($model, 'content_2'); ?>
        </td>
    </tr>
    <tr>
        <td class="title">Теги:</td>
        <td>
            <?php
            echo CHtml::activeTextField($model, 'tags', array(
                'style' => 'width: 400px;'
            ));
            ?>
            <div class="note">(ключевые слова должны разделяться запятыми, например: тег1, тег2, тег3)</div>
            <?php echo CHtml::error($model, 'tags'); ?>
        </td>
    </tr>
    <tr>
        <td class="title">Загрузка фото:</td>
        <td>
            <br>
            <div class="button_main">
                <button type="submit" id="upload_image">Загрузить фотографии &rarr;</button>
            </div>
            <br>
            <br>
            <div class="item">
                <div class="b-media-thumbs_title">Изображения <span id="images_counter"><span><?php if(isset($postImages)){ echo count($postImages);}else{echo 0;}?></span> / 50</div>
                <div class="b-media-thumbs" id="images_thumbs">
                    <?php if(isset($postImages)) foreach($postImages as $image):?>
                    <div class="img" id="img_<?php echo $image["id"];?>">
                        <a class="upbox-button" rel="upbox-button" href="<?php echo $image["image_big"];?>"><img src="<?php echo $image["image_medium"];?>"></a>
                        <a class="delete magic_button"  onclick="post_add_admin.remove('uploads_img/16796','<?php $img = explode("/",$image["image_big"]); echo $img[3];?>','<?php echo $image["id"];?>'); return false;"></a>
                    </div>
                    <?php endforeach; ?>
                </div>
                <span id="loader"></span>
                <input id="imagePost" type="hidden" name="images" value="">
            </div>
        </td>
    </tr>
</table>
<br>
<div class="button_main">
    <button type="submit"><?php echo $button_text; ?></button>
</div>

<?php if ($model->status == 'verification'): ?>
<div class="button_main r_fl">
    <button type="button" onclick="location.assign('/mod_cp/post/reject/id/<?php echo $model->id; ?>');">Отклонить</button>
</div>
<?php endif; ?>

<?php if ($model->status == 'published' || $model->status == 'published_verification'): ?>
<a href="/mod_cp/post/delete/id/<?php echo $model->id; ?>" class="r_fl" onclick="if (!confirm('Вы действительно хотите удалить эту статью?')) return false;">Удалить статью</a>
<?php endif; ?>

или <a href="/mod_cp/post/">вернуться к списку</a>
<?php echo CHtml::endForm(); ?>

<script src="/static/ckeditor/ckeditor.js"></script>
<script src="/static/ckeditor/adapters/jquery.js"></script>
<script src="/static/js/m_post_add_admin.js"></script>
<script src="/static/js/ajaxupload.js"></script>
<script>
    $(document).ready(function () {
        $('#Post_content_1, #Post_content_2').ckeditor({width : '98%'});
    });
    $(document).ready(function () {
        $('#Post_content_2').ckeditor();
        post_add_admin.uploadImg($('#upload_image'));

        var arr = "[";
        $( ".img" ).each(function( index ) {
            if(index == 0){
                arr = arr+'{'+
                        '"image_medium":"'+ $(this).find("img").attr("src")+'",'+
                        '"image_big":"'+ $(this).find(".upbox-button").attr("href")+'"'+
                        '}';
            }else{
                arr = arr+',{'+
                        '"image_medium":"'+ $(this).find("img").attr("src")+'",'+
                        '"image_big":"'+ $(this).find(".upbox-button").attr("href")+'"'+
                        '}';
            }
        });
        arr = arr+']';
        $("#imagePost").attr("value",arr);
    });
</script>