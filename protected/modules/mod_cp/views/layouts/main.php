<!DOCTYPE html>
<html>
<head>
    <title>MOD: iMarinin</title>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <?php Yii::app()->getClientScript()->registerCoreScript( 'jquery' ); ?>
    <link rel="stylesheet" type="text/css" href="/static/admin/styles.css?<?php echo ModSettings::get('version_css_styles'); ?>" />
    <script type="text/javascript" src="/static/js/jquery.placeholder.js"></script>
    <script type="text/javascript" src="/static/js/main.js"></script>

    <script type="text/javascript" src="/static/js/m_tooltip.js"></script>
    <link rel="stylesheet" type="text/css" href="/static/m_tooltip.css" />

    <link rel="stylesheet" type="text/css" href="/static/fancybox/jquery.fancybox.css">
    <script type="text/javascript" src="/static/fancybox/jquery.fancybox.pack.js"></script>

    <link rel="stylesheet" type="text/css" href="/static/humanity/jquery-ui-1.8.23.custom.css">
    <script type="text/javascript" src="/static/js/jquery-ui-1.8.23.custom.min.js"></script>
    <script>
        var svitor = {
            mainDomain:'<?php echo Yii::app()->request->getServerName(); ?>'
        };
    </script>
</head>
<body>
<div class="container">
    <div class="head_userbar">
        <b><?php echo Yii::app()->user->name; ?></b> (<a href="/user/logout/" class="logout">выход</a>)
        <br />
        <a href="/" class="links">Перейти на сайт &rarr;</a>
        <?php if (User::getSecurityLevel() == 1): ?>
        <div style="margin-top: 5px;"><a href="/admin_cp/" class="links">Система управления &rarr;</a></div>
        <?php endif; ?>
    </div>
    <div class="head" onclick="location.assign('/mod_cp/');">
        Кабинет <span><?php echo ModSettings::get('version'); ?></span> Модератора
    </div>
    <div class="head_menu">
        <ul>
            <!-- <li class="first"><a href="/mod_cp/products">Товары</a></li> -->
            <li class="main"><a class="main" onclick="return false;">Обмен опытом</a>
                <div>
                    <ul>
                        <a href="/mod_cp/post">Статьи</a>
                        <li><a href="/mod_cp/comment">Комментарии</a></li>
                    </ul>
                </div>
            </li>
            <li class="main last"><a class="main arrow-down" onclick="return false;">События</a>
                <div>
                    <ul>
                        <li><a href="/mod_cp/exhibitions/">Выставки</a></li>
                        <li><a href="/mod_cp/divination/">Гадания</a></li>
                    </ul>
                </div>
            </li>
        </ul>
    </div>
    <div class="content">
        <?php echo $content; ?>
    </div>
    <div class="footer">Created by Dmitry Marinin.</div>
</div>
</body>
</html>