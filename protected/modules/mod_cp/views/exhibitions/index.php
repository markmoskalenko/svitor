<h1>Выставки</h1>
<table class="category_editor">
    <tr>
        <td class="left">
            <div class="filter">
                <div class="button_main">
                    <button type="button" onclick="location.assign('/mod_cp/exhibitions/add/');">Добавить</button>
                </div>
            </div>
            <table class="list_table">
                </thead>
                <?php
                if (!empty($exhibitions))
                {
                    foreach ($exhibitions as $exhibition)
                    {
                        echo '<tr id="'.$exhibition["event_id"].'" onmouseover="exhibitions.focus(this);" onmouseout="exhibitions.outfocus(this);">';
                        echo '<td class="al_c" style=" width: 200px;text-align: left;">c '.date("d-m-Y",$exhibition["date_from"]).' по '.date("d-m-Y",$exhibition["date_to"]).'</td>';

                        echo '<td class="al_c" style="text-align: left; width: 500px;">'.$exhibition["title"].'</td>';

                        echo '<td class="al_c" style="text-align: right;"><span class="action" style="display: none;"><a href="/mod_cp/exhibitions/edit/id/'.$exhibition["event_id"].'">Редактировать</a> | <a href="/mod_cp/exhibitions/delete/id/'.$exhibition["event_id"].'">Удалить</a></span></td>';
                        echo '</tr>';
                    }
                }
                else
                {
                    echo '<tr><td colspan="7" class="al_c">Ничего не найдено...</td></tr>';
                }
                ?>
            </table>
            <div class="r_fl">
                <?php
                $this->widget('CLinkPager', array(
                    'pages' => $exhibitions_pagination,
                    'htmlOptions' => array('class' => 'b-default_pager'),
                    'firstPageLabel' => '',
                    'lastPageLabel' => '',
                    'prevPageLabel' => '&larr; Назад',
                    'nextPageLabel' => 'Далее &rarr;',
                    'header' => ''
                ));
                ?>
            </div>
        </td>
</table>
<script src="/static/admin/js/e_exhibitions.js"></script>