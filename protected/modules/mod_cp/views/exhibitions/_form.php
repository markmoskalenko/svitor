<h1>Создать выставку</h1>
<?php echo CHtml::beginForm(); ?>
<?php echo CHtml::errorSummary($model); ?>
    <table class="default_table">
        <tr>
            <td>
                <span class="title">Название <span class="red_text">*</span></span>
                <?php echo CHtml::activeTextField($model, 'title'); ?>
            </td>
        </tr>
        <?php
        // Если редактируется категория, у которой имеются дочерние разделы, то убираем из формы "Родитель" и "Сайты".
        if (!empty($countries)): ?>
            <tr>
                <td class="ancestor_id"><div class="title">Страна <span class="red_text">*</span></div>
                    <select style="width: 212px;" id="country" name="country" onchange="exhibitions.addRegion(this);">
                        <option value="0">--- не выбрано ---</option>
                        <?php foreach ($countries as $cat):?>
                            <option <?php if($cat['country_id'] == $model["country"]){echo "selected";} ?> value="<?php echo $cat['country_id'];?>"><?php echo $cat['name'];?></option>
                        <?php endforeach;?>
                    </select>
                </td>
            </tr>
        <?php endif; ?>

            <tr <?php if (empty($regions)){ echo "style='display:none;'";} ?>>
                <td class="ancestor_id"><div class="title">Регион </div>
                    <select style="width: 212px;" id="region" name="region" onchange="exhibitions.addSity();">
                        <option value="0">--- не выбрано ---</option>
                    <?php if (isset($regions)): ?>
                        <?php foreach ($regions as $reg):?>
                            <option <?php if($reg['region_id'] == $model["region"]){echo "selected";} ?> value="<?php echo $reg['region_id'];?>"><?php echo $reg['name'];?></option>
                        <?php endforeach;?>
                    <?php endif; ?>
                    </select>
                </td>
            </tr>

            <tr <?php if (empty($cities)){ echo "style='display:none;'";}?>>
                <td class="ancestor_id"><div class="title">Город</div>
                    <select style="width: 212px;" id="sity" name="sity">
                        <option value="0">--- не выбрано ---</option>
                        <?php if (isset($cities)): ?>
                        <?php foreach ($cities as $cit):?>
                            <option <?php if($cit['city_id'] == $model["sity"]){echo "selected";} ?> value="<?php echo $cit['city_id'];?>"><?php echo $cit['name'];?></option>
                        <?php endforeach;?>
                        <?php endif; ?>
                    </select>
                </td>
            </tr>
            <tr>
                <td class="ancestor_id"><div class="title">Адрес</div>
                    <?php echo CHtml::activeTextField($model, 'addres', array('placeholder' => 'пример: ул. Ленина, дом 10')); ?>
                </td>
            </tr>
        <tr>
            <td class="ancestor_id"><div class="title">Описание</div>
                <?php echo CHtml::activeTextField($model, 'description_small'); ?>
            </td>
        </tr>
        <tr>
            <td><span class="title">Дата <span class="red_text">*</span></span>
                с <?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'name'=>"Exhibitions[date_from]",
                    'value'=>date("d-m-Y",$model->date_from),
                    'htmlOptions' => array(
                        'size' => '10',
                        'maxlength' => '10',
                        'style'=> 'width: 78px;'
                    ),
                    'options' => array(
                        'dateFormat' => 'dd-mm-yy',     // format of "2012-12-25"
                        'minDate' => date("d-m-Y",time()),      // minimum date
                        'maxDate' => '2099-12-31',      // maximum date
                    ),
                ));
                ?>
                по <span id="lastDate">
                    <?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'name'=>"Exhibitions[date_to]",
                    'value'=>date("d-m-Y",$model->date_to),
                    'htmlOptions' => array(
                        'size' => '10',         // textField size
                        'maxlength' => '10',
                        'style'=> 'width: 78px;'
                    ),
                    'options' => array(
                        'dateFormat' => 'dd-mm-yy',     // format of "2012-12-25"
                        'minDate' => date("d-m-Y",time()),      // minimum date
                        'maxDate' => '2099-12-31',      // maximum date
                    ),
                ));
                    ?>
                </span>
                <div style="color:red;"><?php if(isset($error['date_from'])){ echo $error['date_from'];}elseif(isset($error['date_to'])){ echo $error['date_to'];}?></div>
            </td>
        </tr>
        <tr>
            <td><span class="title">Описание 2</span>
                <?php echo CHtml::activeTextArea($model, 'description'); ?>
            </td>
        </tr>
        <tr>
            <td><span class="title">Характеристики </span>
                <?php foreach($attributes_data as $attributes):?>
                        <?php if($attributes->Values): ?>
                        <div>
                            <div style="color: grey; float: left;width: 100px; text-align: right;"><?php echo $attributes->title;?></div>
                            <div style="margin-left: 120px;">
                            <?php foreach($attributes->Values as $item):?>
                                <?php if($item->value_boolean): ?>
                                    <input
                                        <?php
                                        if(isset($events)):
                                            foreach($events as $event){
                                                if($event['attr_val_id'] == $item->id && $event['attr_id'] == $attributes->id){
                                                    echo "checked=''";
                                                }
                                            }
                                        endif;
                                        ?> type="radio" name="attribute[<?php echo $item->id;?>]" value="<?php echo $attributes->id;?>"> <?php echo $item->value_boolean;?><Br>
                                <?php else: ?>
                                    <?php
                                    ?>
                                    <input
                                        <?php
                                        if(isset($events)):
                                        foreach($events as $event){
                                            if($event['attr_val_id'] == $item->id && $event['attr_id'] == $attributes->id){
                                                echo "checked=''";
                                            }
                                        }
                                        endif;
                                        ?>
                                    name="attribute[<?php echo $item->id;?>]" type="checkbox" value="<?php echo $attributes->id;?>"> <?php echo $item->value_string;?><br>
                                <?php endif; ?>
                            <?php endforeach;?>
                            </div>
                        </div><br style="clear: both;">
                        <?php endif; ?>
                <?php endforeach; ?>
            </td>
        </tr>
    </table>
    <br><br>
    <div class="button_main">
        <button type="submit">Опубликовать</button>
    </div>
<?php echo CHtml::endForm(); ?>
<script src="/static/admin/js/e_exhibitions.js"></script>
<script src="/static/ckeditor/ckeditor.js"></script>
<script src="/static/ckeditor/adapters/jquery.js"></script>
<script>
    $(document).ready(function () {
        $('#Exhibitions_description').ckeditor({width : '98%'});
    });
</script>