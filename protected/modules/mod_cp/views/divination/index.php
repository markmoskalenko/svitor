<h1>Гадания</h1>
<table class="category_editor">
    <tr>
        <td class="left">
            <div class="filter">
                <div class="button_main">
                    <button type="button" onclick="location.assign('/mod_cp/divination/add/');">Добавить</button>
                </div>
            </div>
            <table class="list_table">
                </thead>
                <?php
                if (!empty($divination))
                {
                    foreach ($divination as $divination)
                    {
                        if($divination["date_from"]){ $date_from = 'c '.date("d-m-Y",$divination["date_from"]); }else{ $date_from = 'без даты';}
                        if($divination["date_to"]){ $date_to = 'по '.date("d-m-Y",$divination["date_to"]); }else{ $date_to = '';}
                        echo '<tr id="'.$divination["event_id"].'" onmouseover="divination.focus(this);" onmouseout="divination.outfocus(this);">';
                        echo '<td class="al_c" style=" width: 200px;text-align: left;">'.$date_from.' '.$date_to.'</td>';

                        echo '<td class="al_c" style="text-align: left; width: 500px;">'.$divination["title"].'</td>';

                        echo '<td class="al_c" style="text-align: right;"><span class="action" style="display: none;"><a href="/mod_cp/divination/edit/id/'.$divination["event_id"].'">Редактировать</a> | <a href="/mod_cp/divination/delete/id/'.$divination["event_id"].'">Удалить</a></span></td>';
                        echo '</tr>';
                    }
                }
                else
                {
                    echo '<tr><td colspan="7" class="al_c">Ничего не найдено...</td></tr>';
                }
                ?>
            </table>
            <div class="r_fl">
                <?php
                $this->widget('CLinkPager', array(
                    'pages' => $divination_pagination,
                    'htmlOptions' => array('class' => 'b-default_pager'),
                    'firstPageLabel' => '',
                    'lastPageLabel' => '',
                    'prevPageLabel' => '&larr; Назад',
                    'nextPageLabel' => 'Далее &rarr;',
                    'header' => ''
                ));
                ?>
            </div>
        </td>
</table>
<script src="/static/admin/js/e_divination.js"></script>