<?php
class PostsController extends AppController
{
    public function loadCategories()
    {
        $criteria = new CDbCriteria;
        $criteria->order = 'root';
        $criteria->condition = 't.visible = 1';

        $categories = Category::model()->roots()->findAll($criteria);

        return $categories;
    }

    public function actionIndex()
    {
        Yii::app()->user->setReturnUrl(Yii::app()->request->url);

        // Статьи от "редакции" сайта
        $criteria = new CDbCriteria;
        $criteria->condition = 't.status = "verified" AND t.published = 1 AND author_vid = 0';
        $criteria->order = 'date DESC';

        $dataProvider = new CActiveDataProvider('InfoPosts', array(
            'pagination' => array(
                'pageSize' => 15,
                'pageVar' => 'page'
            ),
            'criteria' => $criteria
        ));

        $posts_count = InfoPosts::model()->count($criteria);

        // Статьи для подтверждения
        $criteria = new CDbCriteria;
        $criteria->condition = 't.status = "verify" AND t.published = 1';
        $criteria->order = 'date ASC, last_edit DESC';
        $criteria->with = array('Vendor');

        $verify_dataProvider = new CActiveDataProvider('InfoPosts', array(
            'pagination' => array(
                'pageSize' => 15,
                'pageVar' => 'v_page'
            ),
            'criteria' => $criteria
        ));

        $verify_posts_count = InfoPosts::model()->count($criteria);

        $this->render('index', array(
            'dataProvider' => $dataProvider,
            'posts' => $dataProvider->getData(),
            'posts_count' => $posts_count,

            'verify_dataProvider' => $verify_dataProvider,
            'verify_posts' => $verify_dataProvider->getData(),
            'verify_posts_count' => $verify_posts_count
        ));
    }

    public function actionAdd()
    {
        $model = new InfoPosts('add');
        $categories = $this->loadCategories();

        if (isset($_POST['InfoPosts']))
        {
            $model->attributes = $_POST['InfoPosts'];
            $model['date'] = time();
            $model['last_edit'] = time();
            $model['status'] = 'verified';

            if ($model->save())
                $this->redirect(Yii::app()->createUrl('mod_cp/posts'));
        }

        $this->render('_form', array(
            'model' => $model,
            'categories' => $categories
        ));
    }

    public function actionEdit($id = 0)
    {
        $id = intval($id);
        if (empty($id))
            $this->redirect(Yii::app()->createUrl('mod_cp/posts'));

        $model = InfoPosts::model()->findByPk($id);
        $categories = $this->loadCategories();

        if (isset($_POST['InfoPosts']))
        {
            $model->attributes = $_POST['InfoPosts'];
            $model['last_edit'] = time();
            $model['status'] = 'verified';

            if ($model->save()) {
                $url = Yii::app()->user->getReturnUrl();
                Yii::app()->request->redirect($url);
            }
        }

        $this->render('_form', array(
            'model' => $model,
            'categories' => $categories
        ));
    }

    public function actionReject($id = 0)
    {
        $id = intval($id);
        if (empty($id))
            $this->redirect(Yii::app()->createUrl('mod_cp/posts'));

        $model = InfoPosts::model()->findByPk($id);
        $model['status'] = 'rejected';
        $model->update();

        $this->redirect(Yii::app()->createUrl('mod_cp/posts'));
    }

    public function actionDelete($id = 0)
    {
        $id = intval($id);
        if (empty($id))
            $this->redirect(Yii::app()->createUrl('mod_cp/posts'));

        if (InfoPosts::model()->deleteByPk($id))
        {
            InfoPostComments::model()->deleteAllByAttributes(array('post_id' => $id));
            $this->redirect(Yii::app()->createUrl('mod_cp/posts'));
        }
        else
        {
            mApi::sayError(1, 'Не удалось удалить статью...');
        }
    }
}