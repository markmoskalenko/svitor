<?php
class CommentsController extends AppController
{
    public function actionIndex()
    {
        $criteria = new CDbCriteria;
        $criteria->condition = 't.status = "verify"';
        $criteria->order = 't.date ASC';
        $criteria->with = array('Post', 'User');

        $dataProvider = new CActiveDataProvider('InfoPostComments', array(
            'pagination' => array(
                'pageSize' => Yii::app()->params['items_per_page'],
                'pageVar' => 'page',
                'params' => array_merge($_GET, array('#' => 'comments'))
            ),
            'criteria' => $criteria
        ));

        $comments_count = InfoPostComments::model()->count($criteria);
        $comments = $dataProvider->getData();

        $this->render('index', array(
            'dataProvider' => $dataProvider,
            'comments_count' => $comments_count,
            'comments' => $comments
        ));
    }

    public function actionApprove($id = 0)
    {
        $id = intval($id);
        if (empty($id))
            $this->redirect(Yii::app()->createUrl('mod_cp/comments'));

        $model = InfoPostComments::model()->findByPk($id);
        $model['status'] = 'verified';
        $model->update();

        $this->redirect(Yii::app()->createUrl('mod_cp/comments'));
    }

    public function actionReject($id = 0)
    {
        $id = intval($id);
        if (empty($id))
            $this->redirect(Yii::app()->createUrl('mod_cp/posts'));

        $model = InfoPostComments::model()->findByPk($id);
        $model['status'] = 'rejected';
        $model->update();

        $this->redirect(Yii::app()->createUrl('mod_cp/comments'));
    }

    public function actionBan()
    {
        // i wanna ban you!
    }
}