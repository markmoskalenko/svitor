<?php
class CommentController extends AppController
{
    public function actionIndex()
    {
        $criteria = new CDbCriteria;
        $criteria->condition = 't.status = "verification"';
        $criteria->order = 't.date ASC';
        $criteria->with = array('Post', 'User');

        $dataProvider = new CActiveDataProvider('PostComment', array(
            'pagination' => array(
                'pageSize' => Yii::app()->params['items_per_page'],
                'pageVar' => 'page',
                'params' => array_merge($_GET, array('#' => 'comment'))
            ),
            'criteria' => $criteria
        ));

        $comments_count = $dataProvider->getTotalItemCount();
        $comments = $dataProvider->getData();

        $this->render('index', array(
            'dataProvider' => $dataProvider,
            'comments_count' => $comments_count,
            'comments' => $comments
        ));
    }

    public function actionApprove($id = 0)
    {
        $id = intval($id);
        if (empty($id))
            $this->redirect(Yii::app()->createUrl('mod_cp/comment'));

        $model = PostComment::model()->findByPk($id);
        $model['status'] = 'published';
        $model->update();

        $this->redirect(Yii::app()->createUrl('mod_cp/comment'));
    }

    public function actionReject($id = 0)
    {
        $id = intval($id);
        if (empty($id))
            $this->redirect(Yii::app()->createUrl('mod_cp/post'));

        $model = PostComment::model()->findByPk($id);
        $model['status'] = 'rejected';
        $model->update();

        $this->redirect(Yii::app()->createUrl('mod_cp/comment'));
    }

    public function actionBan()
    {
        // i wanna ban you!
    }
}