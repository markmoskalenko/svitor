<?php
class MainController extends AppController
{
    public $defaultAction = 'index';

    public function actionIndex()
    {
        $this->render('index');
    }
}