<?php
class ExhibitionsController extends AppController
{
    public $defaultAction = 'index';

    public function actionIndex()
    {
//        $exhibitions = Exhibitions::model()->findAll();

        $criteria = new CDbCriteria;
        $criteria->order = 'date_from DESC';


        $dataProvider = new CActiveDataProvider('Exhibitions', array(
            'pagination' => array(
                'pageSize' => 15,
                'pageVar' => 'page'
            ),
            'criteria' => $criteria
        ));

        $this->render('index', array(
            'exhibitions_pagination' => $dataProvider->pagination,
            'exhibitions' => $dataProvider->getData(),
            'exhibitions_count' => $dataProvider->getTotalItemCount()
        ));
    }

    public function actionAdd()
    {
        $countries = Countries::model()->findAll();

        $attributes_data = Attribute::model()->findAllByAttributes(array('belongs_to' => 'exhibitions'), array('order' => 'id DESC'));

        $model = new Exhibitions();

        $error = '';

        if (isset($_POST['Exhibitions']))
        {
            if(isset($_POST['country'])){
                $_POST['Exhibitions']['country'] = $_POST['country'];
                $model['country'] = $_POST['country'];
            }

            if(isset($_POST['region'])){
                $_POST['Exhibitions']['region'] = $_POST['region'];
                $model['region'] = $_POST['region'];
            }

            if(isset($_POST['sity'])){
                $_POST['Exhibitions']['sity'] = $_POST['sity'];
                $model['sity'] = $_POST['sity'];
            }

            if(isset($_POST['Exhibitions']['description'])){
                $model['description'] = $_POST['Exhibitions']['description'];
            }

            if(isset($_POST['Exhibitions']['description_small'])){
                $model['description_small'] = $_POST['Exhibitions']['description_small'];
            }

            if(isset($_POST['Exhibitions']['date_from'])){
                $date_from = explode("-",$_POST['Exhibitions']['date_from']);
                if(!isset($date_from[1]) || intval($date_from[0]) == 0 || intval($date_from[1]) == 0 || intval($date_from[2]) == 0 || $date_from[0]>31 || $date_from[1]>12  || $date_from[2]>2019 ){
                    $error['date_from'] = "Введите корректную дату старта, например: ".date("d-m-Y",time());
                    $_POST['Exhibitions']['date_from'] = time();
                }else{
                    $_POST['Exhibitions']['date_from'] = mktime(0, 0, 0, $date_from[1], $date_from[0], $date_from[2]);
                    $model['date_from'] = $_POST['Exhibitions']['date_from'];
                }
            }

            if(isset($_POST['Exhibitions']['date_to'])){
                $date_to = explode("-",$_POST['Exhibitions']['date_to']);

                if (!isset($date_to[1]) || intval($date_to[0]) == 0 || intval($date_to[1]) == 0 || intval($date_to[2]) == 0 || $date_to[0]>31 || $date_to[1]>12  || $date_to[2]>2019 ) {
                    $error['date_to'] = "Введите корректную дату окончания, например: " . date("d-m-Y", $_POST['Exhibitions']['date_from'] + 86400);
                    $_POST['Exhibitions']['date_to'] = time();
                } elseif ($_POST['Exhibitions']['date_from'] >= mktime(22, 0, 0, $date_to[1], $date_to[0], $date_to[2])) {
                    $error['date_to'] = "Введите корректную дату окончания (должна быть больше даты старта)";
                    $_POST['Exhibitions']['date_to'] =  $_POST['Exhibitions']['date_from'] + 86400;
                } else {
                    $_POST['Exhibitions']['date_to'] = mktime(22, 0, 0, $date_to[1], $date_to[0], $date_to[2]);
                    $model['date_to'] = $_POST['Exhibitions']['date_to'];
                }
            }

            $model->attributes = $_POST['Exhibitions'];

            if(empty($error)){
                if($model->save()){

                    $event_id = Yii::app()->db->getLastInsertId();

                    if(isset($_POST['attribute'])){

                        foreach($_POST['attribute'] as $key => $attribute){
                            $events = new Events();
                            $events['event_id'] =  $event_id;
                            $events['attr_id'] =  $attribute;
                            $events['attr_val_id'] = $key;
                            $events->save();
                        }
                    }

                    $this->redirect(Yii::app()->createUrl('mod_cp/exhibitions'));
                }
            }
        }

        $this->render('_form', array(
            'model' => $model,
            'countries' => $countries,
            'error' => $error,
            'attributes_data' => $attributes_data
        ));
    }

    public function actionEdit($id = 0)
    {
        $id = intval($id);
        $countries = Countries::model()->findAll();
        $attributes_data = Attribute::model()->findAllByAttributes(array('belongs_to' => 'exhibitions'), array('order' => 'id DESC'));

        $events = Events::model()->findAll('event_id=:event_id', array(':event_id' =>$id));

        $model = Exhibitions::model()->findByPk($id);

        $error = array();

        if(!empty($model->region) ){
            $cities = Cities::model()->findAll('region_id=:region_id', array(':region_id'=>$model->region));
            $regions = Regions::model()->findAll('country_id=:country_id', array(':country_id'=>$model->country));
        }else{
            $cities =array();
            $regions =array();
        }

        if (isset($_POST['Exhibitions']))
        {

            if(isset($_POST['country'])){
                $_POST['Exhibitions']['country'] = $_POST['country'];
                $model['country'] = $_POST['country'];
            }

            if(isset($_POST['region'])){
                $_POST['Exhibitions']['region'] = $_POST['region'];
                $model['region'] = $_POST['region'];
            }

            if(isset($_POST['sity'])){
                $_POST['Exhibitions']['sity'] = $_POST['sity'];
                $model['sity'] = $_POST['sity'];
            }

            if(isset($_POST['Exhibitions']['description'])){
                $model['description'] = $_POST['Exhibitions']['description'];
            }

            if(isset($_POST['Exhibitions']['description_small'])){
                $model['description_small'] = $_POST['Exhibitions']['description_small'];
            }

            if(isset($_POST['Exhibitions']['date_from'])){
                $date_from = explode("-",$_POST['Exhibitions']['date_from']);
                if(!isset($date_from[1]) || $date_from[0]>31 || $date_from[1]>12  || $date_from[2]>2019 ){
                    $error['date_from'] = "Введите корректную дату старта, например: ".date("d-m-Y",time());
                    $_POST['Exhibitions']['date_from'] = time();
                }else{
                    $_POST['Exhibitions']['date_from'] = mktime(0, 0, 0, $date_from[1], $date_from[0], $date_from[2]);
                    $model['date_from'] = $_POST['Exhibitions']['date_from'];
                }
            }

            if(isset($_POST['Exhibitions']['date_to'])){
                $date_to = explode("-",$_POST['Exhibitions']['date_to']);

                if (!isset($date_to[1]) || $date_to[0]>31 || $date_to[1]>12  || $date_to[2]>2019 ) {
                    $error['date_to'] = "Введите корректную дату окончания, например: " . date("d-m-Y", $_POST['Exhibitions']['date_from'] + 86400);
                    $_POST['Exhibitions']['date_to'] = time();
                } elseif ($_POST['Exhibitions']['date_from'] >= mktime(22, 0, 0, $date_to[1], $date_to[0], $date_to[2])) {
                    $error['date_to'] = "Введите корректную дату окончания (должна быть больше даты старта)";
                    $_POST['Exhibitions']['date_to'] =  $_POST['Exhibitions']['date_from'] + 86400;
                } else {
                    $_POST['Exhibitions']['date_to'] = mktime(22, 0, 0, $date_to[1], $date_to[0], $date_to[2]);
                    $model['date_to'] = $_POST['Exhibitions']['date_to'];
                }
            }

            $model->attributes = $_POST['Exhibitions'];

            if(empty($error)){
                if($model->save()){

                    if(isset($_POST['attribute'])){

                        Events::model()->deleteAllByAttributes(array('event_id' =>$id));

                        foreach($_POST['attribute'] as $key => $attribute){
                            $events = new Events();
                            $events['event_id'] =  $id;
                            $events['attr_id'] =  $attribute;
                            $events['attr_val_id'] = $key;
                            $events->save();
                        }
                    }

                    $this->redirect(Yii::app()->createUrl('mod_cp/exhibitions'));
                }
            }
        }
        $this->render('_form', array(
            'model' => $model,
            'countries' => $countries,
            'regions' => $regions,
            'cities' => $cities,
            'events' => $events,
            'error' => $error,
            'attributes_data' => $attributes_data
        ));
    }

    public function actionRegions()
    {
        $this->layout = 'ajax';
        $coutry_id = $_POST['country'];
        $coutry_id = intval($coutry_id);
        $model = Regions::model()->findAll('country_id=:country_id', array(':country_id'=>$coutry_id));
        $arr = array();
        foreach($model as $item){
            $arr[$item['region_id']] = $item['name'];
        }

        echo json_encode($arr);
    }

    public function actionSity()
    {
        $this->layout = 'ajax';
        $region_id = $_POST['sity'];
        $region_id = intval($region_id);
        $model = Cities::model()->findAll('region_id=:region_id', array(':region_id'=>$region_id));
        $arr = array();
        foreach($model as $item){
            $arr[$item['city_id']] = $item['name'];
        }

        echo json_encode($arr);
    }

    public function actionDelete($id = 0)
    {
        $this->layout = 'ajax';
        $id = intval($id);

        if (empty($id))
            Yii::app()->end();

        // Удаляем привязанные характеристики
        Exhibitions::model()->deleteAllByAttributes(array('event_id' =>$id));

        $this->redirect(Yii::app()->createUrl('mod_cp/exhibitions'));

    }
}
?>