<?php
class DivinationController extends AppController
{
    public $defaultAction = 'index';

    public function actionIndex()
    {
        $criteria = new CDbCriteria;
        $criteria->order = 'date_from DESC';


        $dataProvider = new CActiveDataProvider('Divination', array(
            'pagination' => array(
                'pageSize' => 15,
                'pageVar' => 'page'
            ),
            'criteria' => $criteria
        ));

        $this->render('index', array(
            'divination_pagination' => $dataProvider->pagination,
            'divination' => $dataProvider->getData(),
            'divination_count' => $dataProvider->getTotalItemCount()
        ));
    }

    public function actionAdd()
    {
        $countries = Countries::model()->findAll();

        $attributes_data = Attribute::model()->findAllByAttributes(array('belongs_to' => 'divination'), array('order' => 'id DESC'));

        $model = new Divination();

        $error = '';

        if (isset($_POST['Divination']))
        {
            if(isset($_POST['country'])){
                $_POST['Divination']['country'] = $_POST['country'];
                $model['country'] = $_POST['country'];
            }

            if(isset($_POST['region'])){
                $_POST['Divination']['region'] = $_POST['region'];
                $model['region'] = $_POST['region'];
            }

            if(isset($_POST['sity'])){
                $_POST['Divination']['sity'] = $_POST['sity'];
                $model['sity'] = $_POST['sity'];
            }

            if(isset($_POST['Divination']['description'])){
                $model['description'] = $_POST['Divination']['description'];
            }

            if($_POST['Divination']['date_from'] == '01-01-1970'){
                $_POST['Divination']['date_from'] = 0;
                $model['date_from'] = $_POST['Divination']['date_from'];
            } else{
                if(isset($_POST['Divination']['date_from'])){
                    $date_from = explode("-",$_POST['Divination']['date_from']);
                    if(!isset($date_from[1]) || intval($date_from[0]) == 0 || intval($date_from[1]) == 0  || intval($date_from[2]) == 0 || $date_from[0]>31 || $date_from[1]>12  || $date_from[2]>2019 ){
                        $error['date_from'] = "Введите корректную дату старта, например: ".date("d-m-Y",time());
                        $_POST['Divination']['date_from'] = time();
                    }else{
                        $_POST['Divination']['date_from'] = mktime(0, 0, 0, $date_from[1], $date_from[0], $date_from[2]);
                        $model['date_from'] = $_POST['Divination']['date_from'];
                    }
                }
            }

            if($_POST['Divination']['date_to'] == '01-01-1970'){
                $_POST['Divination']['date_to'] = 0;
                $model['date_to'] = $_POST['Divination']['date_to'];
            } else{

                if(isset($_POST['Divination']['date_to'])){
                    $date_to = explode("-",$_POST['Divination']['date_to']);

                    if (!isset($date_to[1]) || intval($date_to[0]) == 0 || intval($date_to[1]) == 0 || intval($date_to[2]) == 0 || $date_to[0]>31 || $date_to[1]>12  || $date_to[2]>2019 ) {
                        $error['date_to'] = "Введите корректную дату окончания, например: " . date("d-m-Y", $_POST['Divination']['date_from'] + 86400);
                        $_POST['Divination']['date_to'] = time();
                    } elseif ($_POST['Divination']['date_from'] >= mktime(22, 0, 0, $date_to[1], $date_to[0], $date_to[2])) {
                        $error['date_to'] = "Введите корректную дату окончания (должна быть больше даты старта)";
                        $_POST['Divination']['date_to'] =  $_POST['Divination']['date_from'] + 86400;
                    } else {
                        $_POST['Divination']['date_to'] = mktime(22, 0, 0, $date_to[1], $date_to[0], $date_to[2]);
                        $model['date_to'] = $_POST['Divination']['date_to'];
                    }
                }
            }

            $model->attributes = $_POST['Divination'];

            if(empty($error)){
                if($model->save()){

                    $event_id = Yii::app()->db->getLastInsertId();

                    if(isset($_POST['attribute'])){

                        foreach($_POST['attribute'] as $key => $attribute){
                            $events = new Events();
                            $events['event_id'] =  $event_id;
                            $events['attr_id'] =  $attribute;
                            $events['attr_val_id'] = $key;
                            $events->save();
                        }
                    }

                    $this->redirect(Yii::app()->createUrl('mod_cp/divination'));
                }
            }
        }

        $this->render('_form', array(
            'model' => $model,
            'countries' => $countries,
            'error' => $error,
            'attributes_data' => $attributes_data
        ));
    }

    public function actionEdit($id = 0)
    {
        $id = intval($id);
        $countries = Countries::model()->findAll();
        $attributes_data = Attribute::model()->findAllByAttributes(array('belongs_to' => 'divination'), array('order' => 'id DESC'));

        $events = Events::model()->findAll('event_id=:event_id', array(':event_id' =>$id));

        $model = Divination::model()->findByPk($id);

        $error = '';

        if(!empty($model->region) ){
            $cities = Cities::model()->findAll('region_id=:region_id', array(':region_id'=>$model->region));
            $regions = Regions::model()->findAll('country_id=:country_id', array(':country_id'=>$model->country));
        }else{
            $cities =array();
            $regions =array();
        }

        if (isset($_POST['Divination']))
        {
            if(isset($_POST['country'])){
                $_POST['Divination']['country'] = $_POST['country'];
                $model['country'] = $_POST['country'];
            }

            if(isset($_POST['region'])){
                $_POST['Divination']['region'] = $_POST['region'];
                $model['region'] = $_POST['region'];
            }

            if(isset($_POST['sity'])){
                $_POST['Divination']['sity'] = $_POST['sity'];
                $model['sity'] = $_POST['sity'];
            }

            if(isset($_POST['Divination']['description'])){
                $model['description'] = $_POST['Divination']['description'];
            }

            if($_POST['Divination']['date_from'] == '01-01-1970'){
                $_POST['Divination']['date_from'] = 0;
                $model['date_from'] = $_POST['Divination']['date_from'];
            } else{
                if(isset($_POST['Divination']['date_from'])){
                    $date_from = explode("-",$_POST['Divination']['date_from']);
                    if(!isset($date_from[1]) || $date_from[0]>31 || $date_from[1]>12  || $date_from[2]>2019 ){
                        $error['date_from'] = "Введите корректную дату старта, например: ".date("d-m-Y",time());
                        $_POST['Divination']['date_from'] = time();
                    }else{
                        $_POST['Divination']['date_from'] = mktime(0, 0, 0, $date_from[1], $date_from[0], $date_from[2]);
                        $model['date_from'] = $_POST['Divination']['date_from'];
                    }
                }
            }

            if($_POST['Divination']['date_to'] == '01-01-1970'){
                $_POST['Divination']['date_to'] = 0;
                $model['date_to'] = $_POST['Divination']['date_to'];
            } else{

                if(isset($_POST['Divination']['date_to'])){
                    $date_to = explode("-",$_POST['Divination']['date_to']);

                    if (!isset($date_to[1]) || $date_to[0]>31 || $date_to[1]>12  || $date_to[2]>2019 ) {
                        $error['date_to'] = "Введите корректную дату окончания, например: " . date("d-m-Y", $_POST['Divination']['date_from'] + 86400);
                        $_POST['Divination']['date_to'] = time();
                    } elseif ($_POST['Divination']['date_from'] >= mktime(22, 0, 0, $date_to[1], $date_to[0], $date_to[2])) {
                        $error['date_to'] = "Введите корректную дату окончания (должна быть больше даты старта)";
                        $_POST['Divination']['date_to'] =  $_POST['Divination']['date_from'] + 86400;
                    } else {
                        $_POST['Divination']['date_to'] = mktime(22, 0, 0, $date_to[1], $date_to[0], $date_to[2]);
                        $model['date_to'] = $_POST['Divination']['date_to'];
                    }
                }
            }

            $model->attributes = $_POST['Divination'];

            if(empty($error)){
                if($model->save()){

                    if(isset($_POST['attribute'])){

                        Events::model()->deleteAllByAttributes(array('event_id' =>$id));

                        foreach($_POST['attribute'] as $key => $attribute){
                            $events = new Events();
                            $events['event_id'] =  $id;
                            $events['attr_id'] =  $attribute;
                            $events['attr_val_id'] = $key;
                            $events->save();
                        }
                    }

                    $this->redirect(Yii::app()->createUrl('mod_cp/divination'));
                }
            }
        }
        $this->render('_form', array(
            'model' => $model,
            'countries' => $countries,
            'regions' => $regions,
            'cities' => $cities,
            'events' => $events,
            'error' => $error,
            'attributes_data' => $attributes_data
        ));
    }

    public function actionRegions()
    {
        $this->layout = 'ajax';
        $coutry_id = $_POST['country'];
        $coutry_id = intval($coutry_id);
        $model = Regions::model()->findAll('country_id=:country_id', array(':country_id'=>$coutry_id));
        $arr = array();
        foreach($model as $item){
            $arr[$item['region_id']] = $item['name'];
        }

        echo json_encode($arr);
    }

    public function actionSity()
    {
        $this->layout = 'ajax';
        $region_id = $_POST['sity'];
        $region_id = intval($region_id);
        $model = Cities::model()->findAll('region_id=:region_id', array(':region_id'=>$region_id));
        $arr = array();
        foreach($model as $item){
            $arr[$item['city_id']] = $item['name'];
        }

        echo json_encode($arr);
    }

    public function actionDelete($id = 0)
    {
        $this->layout = 'ajax';
        $id = intval($id);

        if (empty($id))
            Yii::app()->end();

        // Удаляем привязанные характеристики
        Divination::model()->deleteAllByAttributes(array('event_id' =>$id));

        $this->redirect(Yii::app()->createUrl('mod_cp/divination'));

    }
}
?>