<?php
class PostController extends AppController
{
    public function actionIndex()
    {
        Yii::app()->user->setReturnUrl(Yii::app()->request->url);

        // Статьи от "редакции" сайта
        $criteria = new CDbCriteria;
        $criteria->condition = '(t.status = "published" OR t.status = "published_verification") AND author_vid = 0 AND author_uid = 0';
        $criteria->order = 'created_date DESC';

        $dataProvider = new CActiveDataProvider('Post', array(
            'pagination' => array(
                'pageSize' => 15,
                'pageVar' => 'page'
            ),
            'criteria' => $criteria
        ));

        // Статьи для подтверждения
        $criteria = new CDbCriteria;
        $criteria->condition = 't.status = "verification" OR t.status = "published_verification"';
        $criteria->order = 'created_date ASC, edited_date DESC';
        $criteria->with = array('Vendor', 'User');

        $verify_dataProvider = new CActiveDataProvider('Post', array(
            'pagination' => array(
                'pageSize' => 15,
                'pageVar' => 'v_page'
            ),
            'criteria' => $criteria
        ));

        $this->render('index', array(
            'posts_pagination' => $dataProvider->pagination,
            'posts' => $dataProvider->getData(),
            'posts_count' => $dataProvider->getTotalItemCount(),

            'verify_posts_pagination' => $verify_dataProvider->pagination,
            'verify_posts' => $verify_dataProvider->getData(),
            'verify_posts_count' => $verify_dataProvider->getTotalItemCount()
        ));
    }

    public function actionAdd()
    {
        $model = new Post('verification');

        if (isset($_POST['Post']))
        {
            $model->attributes = $_POST['Post'];

            $model['created_date'] = time();
            $model['published_date'] = time();
            $model['status'] = 'published';

            if($model->save()){

                $criteria = new CDbCriteria;
                $criteria->order = 'id DESC';
                $criteria->limit = 1;

                $lastPost = Post::model()->find($criteria);
                $id = $lastPost->id;

                $criteria = new CDbCriteria;
                $criteria->condition = 'post_id = :post_id';
                $criteria->params = array(':post_id' => $id);

                PostImages::model()->deleteAll($criteria);

                foreach(json_decode($_POST['images']) as $item){

                    $post=new PostImages();
                    $post->post_id=(int)$id;
                    $post->image_big = $item->image_big;
                    $post->image_medium = $item->image_medium;
                    $post->save();
                }
                $this->redirect(Yii::app()->createUrl('mod_cp/post'));
            }
        }

        $this->render('_form', array(
            'model' => $model,
            'categories' => PostCategory::model()->findAll()
        ));
    }

    public function actionEdit($id = 0)
    {

        $id = intval($id);
        if (empty($id))
            $this->redirect(Yii::app()->createUrl('mod_cp/post'));

        $model = Post::model()->findByPk($id);

        $criteria = new CDbCriteria;
        $criteria->condition = 'post_id = :post_id';
        $criteria->params = array(':post_id' => $id);

        $postImages = PostImages::model()->findAll($criteria);

        if (isset($_POST['Post']))
        {
            $model->setScenario('verification');
            $model->attributes = $_POST['Post'];

            if ($model->validate())
            {
                $model->status = 'published';
                $model->author_uid = 0;

                if (empty($model->published_date))
                    $model->published_date = time();

                $model->save(false);

                PostImages::model()->deleteAll($criteria);

                foreach(json_decode($_POST['images']) as $item){

                    $post=new PostImages();
                    $post->post_id=(int)$id;
                    $post->image_big = $item->image_big;
                    $post->image_medium = $item->image_medium;
                    $post->save();
                }

                $url = Yii::app()->user->getReturnUrl();
                Yii::app()->request->redirect($url);

            }
        }

        $this->render('_form', array(
            'model' => $model,
            'postImages' => $postImages,
            'categories' => PostCategory::model()->findAll()
        ));
    }

    public function actionReject($id = 0)
    {
        $id = intval($id);
        if (empty($id))
            $this->redirect(Yii::app()->createUrl('mod_cp/post'));

        $model = Post::model()->findByPk($id);
        $model['status'] = 'rejected';
        $model->update();

        $this->redirect(Yii::app()->createUrl('mod_cp/post'));
    }

    public function actionDelete($id = 0)
    {
        $id = intval($id);
        if (empty($id))
            $this->redirect(Yii::app()->createUrl('mod_cp/post'));

        if (Post::model()->deleteByPk($id))
        {
            PostComment::model()->deleteAllByAttributes(array('post_id' => $id));
            $this->redirect(Yii::app()->createUrl('mod_cp/post'));
        }
        else
        {
            mApi::sayError(1, 'Не удалось удалить статью...');
        }
    }
}