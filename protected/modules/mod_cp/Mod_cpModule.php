<?php
class Mod_cpModule extends CWebModule
{
    public $defaultController = 'Main';
    public $layout = 'main';

    public function init()
    {
        if (User::isGuest())
            Yii::app()->request->redirect('/');

        $user = User::model()->findByPk(User::getId());

        if (empty($user) || $user->security_level <= 0)
            Yii::app()->request->redirect('/');
    }
}