<?php

class m130909_140858_pages_add_column_seo extends CDbMigration
{
	public function up()
	{
        $this->addColumn('pages', 'title_h1', 'TEXT');
    }

	public function down()
	{
		echo "m130909_140858_pages_add_column_seo does not support migration down.\n";
		return false;
	}
}