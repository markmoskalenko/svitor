<?php

class m130909_105125_add_column_seo_category extends CDbMigration
{
	public function up()
	{
        $this->addColumn('category', 'title_catalog', 'VARCHAR(255)');
        $this->addColumn('category', 'title_product', 'VARCHAR(255)');
        $this->addColumn('category', 'description_catalog', 'TEXT');
        $this->addColumn('category', 'description_product', 'TEXT');
        $this->addColumn('category', 'keywords_catalog', 'VARCHAR(255)');
        $this->addColumn('category', 'keywords_product', 'VARCHAR(255)');
        $this->addColumn('category', 'text_header_catalog', 'TEXT');
        $this->addColumn('category', 'text_footer_catalog', 'TEXT');
        $this->addColumn('category', 'text_header_product', 'TEXT');
        $this->addColumn('category', 'text_footer_product', 'TEXT');
	}

	public function down()
	{
		echo "m130909_105125_add_column_seo_category does not support migration down.\n";
		return false;
	}
}