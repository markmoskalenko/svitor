<?php

class Bid extends CActiveRecord
{
    const OPENED = 1;
    const CLOSED = 0;
    /**
     * Returns the static model of the specified AR class.
     *
     * @param string $className active record class name.
     *
     * @return Bid the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'bid';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('description, type_holiday, budget','filter','filter'=>array($obj=new Purifier(),'rest')),

            array('cat_id, country_id, region_id, type_holiday, description', 'required'),

            array('type_holiday, description, budget', 'filter', 'filter' => 'trim'),
            array('type_holiday, description, budget', 'filter', 'filter' => 'htmlspecialchars'),

            array('user_id, opened, country_id, cities_id', 'numerical', 'integerOnly' => true),
            array('description', 'length', 'min' => 20),
            array('cat_id, region_id', 'length', 'max' => 11),
            array('budget', 'length', 'max' => 25),
            array('date_created, date_end', 'date'),
            array('type_holiday', 'length', 'max' => 20),
            array('id_last_bid, opened','safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array(
                'id, cat_id, user_id, country_id, region_id, cities_id, type_holiday, budget, description',
                'safe',
                'on' => 'search'
            ),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        if(isset($_SESSION["vendor__id"])){

            $rel = array(self::HAS_MANY, 'UserMessages', 'is_bid', 'condition'=>'from_vid='.$_SESSION["vendor__id"] );

        } else $rel = array(self::HAS_MANY, 'UserMessages', 'is_bid');

        return array(
            'cat'     => array(self::BELONGS_TO, 'Category', 'cat_id'),
            'cities'  => array(self::BELONGS_TO, 'Cities', 'cities_id'),
            'country' => array(self::BELONGS_TO, 'Countries', 'country_id'),
            'region'  => array(self::BELONGS_TO, 'Regions', 'region_id'),
            'user'    => array(self::BELONGS_TO, 'User', 'user_id'),
            'messages'=> $rel,
            'messages_all'=> array(self::HAS_MANY, 'UserMessages', 'is_bid')
        );
    }

    public function defaultScope(){
        return array(
            'order'=>"id DESC"
        );
    }

    public function scopes(){
        return array(
            'myUser'=>array(
                'condition' => 'user_id = '.User::getId()
            ),
            'opened'=>array(
                'condition' => 'opened = '.self::OPENED
            ),
            'closed'=>array(
                'condition' => 'opened = '.self::CLOSED
            )
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id'           => 'ID',
            'cat_id'       => 'Раздел',
            'user_id'      => 'Пользователь',
            'country_id'   => 'Страна',
            'region_id'    => 'Регион',
            'cities_id'    => 'Город',
            'type_holiday' => 'Вид праздника',
            'budget'       => 'Бюджет, рублей',
            'description'  => 'Описание',
            'date_created' => 'Дата публикации',
        );
    }

    public function beforeSave()
    {
        if ($this->isNewRecord) {
            $this->date_created  = Date('Y-m-d');
            $this->date_end      = Date('Y-m-d', (strtotime($this->date_created)+86400*7)); //+7 дней
            $this->user_id       = User::getId();
        }

        return parent::beforeSave();
    }


    public function distribution(){

        $path = Yii::getPathOfAlias('application.views._mailing.newBid');

        //Поиск получателей
        $users = Vendor::model()
          ->with(array(
                      'Categories' => array( 'condition' => 'cat_id = '.$this->cat_id )
                 ))
          ->findAll('`t`.`email` IS NOT NULL');

        if( !count( $users ) ) return false;

        $email = array();
        foreach ( $users as $user ) {
            $email[] = $user->email;
        }

        unset( $users );

        //Темплейт письма
        $template = Yii::app()->getController()->renderInternal( $path, array( 'bid' => $this ), true );

        //API Unisender
        $oMail = new UniSenderApi( Yii::app()->params['uni_sender_key'] );

        //Отправка шаблона
        $res = $oMail->createEmailMessage(
            array(
                 'sender_name'   => Yii::app()->params['sender_name'],
                 'sender_email'  => Yii::app()->params['sender_email'],
                 'subject'       => 'Заявка на услуги от пользователя Svitor.ru',
                 'body'          => $template,
                 'list_id'       => Yii::app()->params['uni_sender_company_list'],
            )
        );

        $res = CJSON::decode( $res );

        if( isset( $res['error'] ) || !isset( $res['result']['message_id'] ) ) return false;

        $oMail->createCampaign(
            array(
                 'message_id' => $res['result']['message_id'],
                 'contacts'   => implode(',', $email)
            )
        );

//        $oMail->deleteMessage(
//            array(
//                 'message_id'   => $res['result']['message_id'],
//            )
//        );

    }

    public static function updateLastBid(){

        $criteria = new CDbCriteria;

        $criteria->condition = 'Unix_timestamp(date_end) >= CURDATE()';

        $criteria->addInCondition('cat_id', VendorCategory::getCurrVendorCategory());

        $criteria->scopes = array('opened');

        $criteria->order = 'id DESC';

        $model = self::model()->find($criteria);

        if( !$model ) return false;

        $vendor = Vendor::model()->findByPk( $_SESSION["vendor__id"] );

        if( !$vendor ) return false;

        $vendor->id_last_bid = $model->id;

        $vendor->save();

    }

    public static function countNewBid(){

        $vendor = Vendor::model()->findByPk( $_SESSION["vendor__id"] );

        if( !$vendor ) return false;

        $criteria = new CDbCriteria;

        $criteria->condition = 'Unix_timestamp(date_end) >= CURDATE()';

        $criteria->scopes = array('opened');

        if( is_numeric($vendor->id_last_bid) && $vendor->id_last_bid > 0){

            $criteria->addCondition('id > :id');

            $criteria->params = array( ':id' => $vendor->id_last_bid );

        }

        $criteria->addInCondition('cat_id', VendorCategory::getCurrVendorCategory());

        $criteria->order = 'id DESC';

        $res = self::model()->findAll( $criteria );

        return count($res);
    }

    public static function close( $id ){

        $model = self::model()->myUser()->findByPk( $id );

        if( !$model ) return false;

        $model->opened = self::CLOSED;

        $model->save( false );

        return true;

    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id, true);
        $criteria->compare('cat_id', $this->cat_id, true);
        $criteria->compare('user_id', $this->user_id);
        $criteria->compare('country_id', $this->country_id);
        $criteria->compare('region_id', $this->region_id, true);
        $criteria->compare('cities_id', $this->cities_id);
        $criteria->compare('type_holiday', $this->type_holiday, true);
        $criteria->compare('budget', $this->budget);
        $criteria->compare('description', $this->description, true);

        return new CActiveDataProvider($this, array(
                                                   'criteria' => $criteria,
                                              ));
    }
}