<?php
class Countries extends CActiveRecord
{
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'area_countries';
    }

    public function getNameById($id = 0)
    {
        if (empty($id))
            return;

        return self::model()->cache(3000)->findByAttributes(array('country_id' => $id))->name;
    }

    public static function getList(){
        $model  = self::model()->cache(3000)->findAll();
        $result = array();
        foreach ($model as $item) {
            $result[$item->country_id] = $item->name;
        }

        return $result;
    }
}