<?php
class Regions extends CActiveRecord
{
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'area_regions';
    }

    public function getNameById($id = 0)
    {
        if (empty($id))
            return;

        return self::model()->cache(3000)->findByAttributes(array('region_id' => $id))->name;
    }

    public static function getListByCountry( $id ){
        return self::model()->cache(3000)->findAllByAttributes(array('country_id' => intval($id)));
    }

    public static function getListDDByCountry( $id ){
        $model  = self::model()->findAllByAttributes( array('country_id' => intval($id)) );
        $result = array();
        foreach ($model as $item) {
            $result[$item->region_id] = $item->name;
        }

        return $result;
    }
}