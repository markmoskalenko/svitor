<?php
class Cities extends CActiveRecord
{
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'area_cities';
    }

    public function getNameById($id = 0)
    {
        if (empty($id))
            return;

        return self::model()->cache(3000)->findByAttributes(array('city_id' => $id))->name;
    }

    public static function getListByRegion( $id ){

        return self::model()->cache(3000)->findAllByAttributes(array( 'region_id' => intval($id) ));

    }

    public static function getListDDByRegion( $id ){
        $model  = self::model()->findAllByAttributes( array('region_id' => intval($id)) );
        $result = array();
        foreach ($model as $item) {
            $result[$item->city_id] = $item->name;
        }

        return $result;
    }

}