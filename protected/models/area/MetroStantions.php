<?php
class MetroStantions extends CActiveRecord
{
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'area_metro_stantions';
    }

    public function getNameById($id = 0)
    {
        if (empty($id))
            return;

        return self::model()->cache(3000)->findByAttributes(array('id' => $id))->name;
    }
}