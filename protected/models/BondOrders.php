<?php
class BondOrders extends CActiveRecord
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'bond_orders';
    }

//    Разрешенные поля для изменения ajax запросом.
    private static $ajaxColumn = array('count','price');

    public function rules()
    {
        return array(
            array('order_id, product_id, count, price', 'required'),
            array('order_id, product_id, count, price', 'numerical', 'integerOnly' => true),
            array('order_id, product_id', 'length', 'max' => 20),
            array('id, order_id, product_id, count, price', 'safe', 'on' => 'search'),
        );
    }

    public function relations()
    {
        return array(
            'product' => array(self::BELONGS_TO, 'Product', 'product_id'),
            'order'   => array(self::BELONGS_TO, 'Orders', 'order_id'),
        );
    }

    public function attributeLabels()
    {
        return array(
            'id'         => 'ID',
            'order_id'   => 'Order',
            'product_id' => 'Product',
            'count'      => 'Количество',
            'price'      => 'Price',
        );
    }

    /**
     * Сохранение товаров заказа из корзины
     *
     * @param $orderId
     * @param $vendorId
     *
     * @return bool
     */
    public static function saveOrderPosition( $orderId,  $vendorId ){

        if( !is_numeric($orderId) || !is_numeric($vendorId) ) return false;

        $products = Yii::app()->shoppingCart->getPositions();

        foreach($products as $product){

            if( $product->Vendor->id === $vendorId ) {

                $model = new self;

                $model->order_id    = $orderId;

                $model->product_id  = $product->id;

                $model->count = $product->getQuantity();

                $model->price = $product->price;

                $model->save();

                Yii::app()->shoppingCart->remove( Product::model()->findByPk( $product->id )->getId() );

            }
        }
        return true;
    }

    public static function addProduct( $orderId, $productsIds ){

        $orderId = intval($orderId);

        $productsIds = mApi::intArray( $productsIds );

        if( !count($productsIds) ){ return false; }

        foreach($productsIds as $id){

            $modelProduct = Product::model()->myVendor()->published()->visible()->find('id = :id',array(':id'=>$id));

            $modelBondOrders = self::model()->find( 'order_id = :order_id AND product_id = :product_id', array(
                                                                                                                ':order_id'   => $orderId,
                                                                                                                ':product_id' => $id
                                                                                                           ) );
            if( $modelProduct && !$modelBondOrders ){

                $model             = new self;

                $model->order_id   = $orderId;

                $model->product_id = $modelProduct->id;

                $model->count      = 1;

                $model->price      = $modelProduct->price;

                $model->save();

            }elseif( $modelBondOrders ){

                $modelBondOrders->count ++;

                $modelBondOrders->save();

            }
        }
    }

    /**
     * @param $id
     * @param $column
     * @param $value
     *
     * @return bool|CActiveRecord
     */
    public static function saveAjaxParam( $id, $column, $value ){

        $model = self::model()->find('id=:id', array(':id' => intval($id)));

        if( !$model                                                 ||
            $model->order->status == Orders::STATUS_CLOSED          ||
            $model->order->status == Orders::STATUS_NOT_CONFIRMED   ||
            $model->order->status == Orders::STATUS_SENT            ||
            !in_array( $column, self::$ajaxColumn )
        ){
            return false;
        }

        $model->{$column} = intval( $value );

        if( $model->save() ) return $model;

        return false;
    }
}