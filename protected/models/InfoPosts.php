<?php
class InfoPosts extends CActiveRecord
{
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'info_posts';
    }

    public function relations()
    {
        return array(
            'Category' => array(self::BELONGS_TO, 'Category', 'cat_id'),
            'Vendor' => array(self::BELONGS_TO, 'Vendor', 'author_vid')
        );
    }

    public function rules()
    {
        return array(
            array('img_filename, title, full_text, tags', 'filter', 'filter' => 'trim'),
            array('img_filename, title, tags', 'filter', 'filter' => 'htmlspecialchars'),

            array('img_id', 'filter', 'filter' => 'intval'),

            array('full_text', 'filter', 'filter' => array('InfoPosts', 'postStripTags')),

            array('title, full_text', 'required',
                'message' => '&uarr; заполните это поле'),

            array('img_filename', 'required',
                'message' => '&uarr; загрузите изображение'),

            array('cat_id', 'required',
                'message' => '&uarr; выберите раздел'),

            array('title', 'length', 'min' => 5, 'max' => 200,
            'tooShort' => '&uarr; слишком короткое название',
            'tooLong' => '&uarr; слишком длинное название (максимум {max} символов)'),

            array('tags', 'safe'),

            array('published', 'in', 'range' => array(0, 1))
        );
    }

    public function postStripTags($value)
    {
        return strip_tags($value, '<a><p><span><strong><b><strike><ol><ul><li><img><iframe><table><tbody><th><tr><td><hr>');
    }

    public function reducePost($value, $size = 500)
    {
        $value = strip_tags($value);

        return mApi::subStr($value, 0, $size);
    }
}