<?php
class Events extends CActiveRecord
{
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'events';
    }

    public function relations()
    {
        return array(
            'Divination' => array(self::BELONGS_TO, 'Divination', 'event_id'),
            'Exhibitions' => array(self::BELONGS_TO, 'Exhibitions', 'event_id'),
            'Values' => array(self::BELONGS_TO, 'AttrValues', 'attr_val_id'),
        );
    }

    public function rules()
    {
        return array();
    }
}