<?php
class Attribute extends CActiveRecord
{
    public $checkedOnFilter = 0;

    public $types_list = array(
        'string' => 'строковый',
        'boolean' => 'логический',
        //'integer' => 'число'
    );
    public $countOnFilter;

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function tableName()
	{
		return 'attributes';
	}

    public function relations()
    {
        return array(
            'Values' => array(self::HAS_MANY, 'AttrValues', 'attr_id',
                'order' => 'Values.sort ASC, Values.id ASC'),

            'CatAttrs' => array(self::BELONGS_TO, 'CategoryAttributes', 'id')
        );
    }

    public function rules()
    {
        return array(
            array('title', 'filter', 'filter' => 'htmlspecialchars'),
            array('title', 'filter', 'filter' => 'trim'),

            array('title', 'required',
                'message' => 'Введите название'),

            array('title', 'uniqueAttrTitle',
                'message' => 'Наименование должно быть уникальным'),

            array('title', 'length', 'min' => 2, 'max' => 250,
                'tooLong' => 'Слишком длинное название (максимум допустимо 250 символов)',
                'tooShort' => 'Слишком короткое название (минимум 2 символа)'),

            array('type', 'required', 'message' => 'Выберите тип'),
            array('type', 'in', 'range' => array('string', 'boolean', 'integer')),
            array('is_multi, required', 'filter', 'filter' => 'intval'),

            array('belongs_to', 'in', 'range' => array('products', 'vendors', 'exhibitions', 'divination')),

            array('required, belongs_to', 'required')
        );
    }


    public function uniqueAttrTitle ($attribute, $params)
    {
        if(!$this->hasErrors())
        {
            $params['criteria']= array(
                'condition' => 'belongs_to = :belongs_to',
                'params' => array(':belongs_to' => $this->belongs_to)
            );

            $validator = CValidator::createValidator('unique', $this, $attribute, $params);
            $validator->validate($this, $attribute);
        }
    }
}
?>