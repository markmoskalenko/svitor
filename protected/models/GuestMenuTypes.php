<?php
class GuestMenuTypes extends CActiveRecord
{
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'guest_menu_types';
    }

    public function rules()
    {
        return array(
            array('title', 'filter', 'filter' => 'trim'),
            array('title', 'filter', 'filter' => 'htmlspecialchars'),

            array('title', 'required', 'message' => 'Введите наименование'),

            array('title', 'length', 'min' => 2, 'max' => 255,
                'tooShort' => 'Слишком короткое наименование',
                'tooLong' => 'Слишком длинное наименование (максимум {max} символов)')
        );
    }
}