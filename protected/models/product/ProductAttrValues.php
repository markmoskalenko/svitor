<?php
class ProductAttrValues extends CActiveRecord
{
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function tableName()
	{
		return 'product_attribute_values';
	}

    public function relations()
    {
        return array(
            'Values' => array(self::BELONGS_TO, 'AttrValues', 'attr_value_id')
        );
    }

    public function rules()
    {
        return array(
            array('product_id, attr_id, attr_value_id', 'required'),
            array('product_id, attr_id, attr_value_id', 'numerical'),
        );
    }
}
?>