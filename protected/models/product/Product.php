<?php
class Product extends CActiveRecord implements IECartPosition
{
    const DELETE_PRODUCT  = 1;
    const RESTORE_PRODUCT = 0;

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    public function tableName()
    {
        return 'products';
    }

    public function relations()
    {
        return array(
            'Vendor' => array(self::BELONGS_TO, 'Vendor', 'vendor_id'),

            'Images' => array(self::HAS_MANY, 'ProductImages', 'product_id'),

            'FirstImage' => array(self::HAS_ONE, 'ProductImages', 'product_id',
                'order' => 'FirstImage.id ASC'
            ),

            'AttrValues' => array(self::HAS_MANY, 'ProductAttrValues', 'product_id', 'with' => 'Values')
        );
    }

	public function rules()
	{
		return array(
            //ДЛЯ XML

            array( 'title', 'required', 'on' => 'addXML, editXML',
                  'message' => 'Необходимо указать название товара'
            ),

            array( 'external_id', 'required', 'on' => 'addXML, editXML',
                  'message' => 'Необходимо указатьтег «id»'
            ),

            array( 'price', 'required', 'on' => 'addXML, editXML',
                   'message' => 'Необходимо указать цену товара'
            ),

            array( 'currencyId', 'required', 'on' => 'addXML, editXML',
                   'message' => 'Необходимо указать тег «currencyId»'
            ),

            array( 'cat_id', 'required', 'on' => 'addXML, editXML',
                  'message' => 'Необходимо указать тег - «categoryId»'
            ),

            array( 'cat_id', 'numerical', 'on' => 'addXML, editXML',
                   'message' => '"categoryId" может быть только целым числом'
            ),

            array( 'cat_id', 'exist', 'on'=>'addXML, editXML', 'attributeName'=>'id', 'className'=>'Category',
                  'message' => 'Такого номера категории на сайте не существует'),

            array( 'cat_id', 'categoryProduct', 'on'=>'addXML, editXML',
                  'message' => 'Такого номера категории на сайте не существует'),


            // КОНЕЦ ДЛЯ XML

			array('external_id, price, title, in_stock, description', 'required', 'on' => 'add, edit, based',
				'message' => '&uarr; это поле необходимо заполнить'
			),

            array('country_of_origin', 'length', 'max' => 50, 'on' => 'add, edit, based, addXML, editXML',
                'message' => '&uarr; максимум допустимо {max} символов'
            ),

            array('external_id', 'length', 'max' => 255, 'on' => 'add, edit, based, addXML, editXML',
                'message' => '&uarr; максимум допустимо {max} символов'
            ),

            array('external_id', 'uniqueExternalAndVendor', 'on' => 'add, edit, based, addXML, editXML'),


			array('cat_id', 'required', 'on' => 'add, edit, based',
				'message' => '&uarr; выберите категорию'
			),

            array('is_delete', 'numerical', 'on' => 'add, edit, based, addXML, editXML',
                  'message' => '&uarr; число должно быть целым'
            ),

			array('description', 'length', 'max' => 1000, 'on' => 'add, edit, based, addXML, editXML',
                'tooLong' => '&uarr; максимум допустимо {max} символов'),

            array('description, title', 'filter', 'filter' => 'htmlspecialchars', 'on' => 'add, edit, based, addXML, editXML'),

			array('visible, in_stock, price', 'numerical', 'on' => 'add, edit, based, addXML, editXML',
                'message' => '&uarr; некорректное значение'
            ),
		);
	}

    public function categoryProduct($attribute,$params){
        $r = Category::model()->exists('id = :id AND visible_prod = 1', array( ':id' => (int)$this->cat_id ));
        if( !$r )
            $this->addError($attribute, 'Такого номера категории на сайте не существует');
    }


    function getId(){
        return 'Product'.$this->id;
    }

    function getPrice(){
        return $this->price;
    }

    public function scopes(){
        return array(
            'published'=>array(
                'condition'=>'is_delete=0',
            ),
            'myVendor'=>array(
                'condition'=>'vendor_id='.Vendor::getId(),
            ),
            'visible'=>array(
                'condition'=>'visible=1',
            ),
            'no_published'=>array(
                'condition'=>'is_delete=1',
            ),
        );
    }

    /**
     * Проверяем на уникальность external_id в паре с vendor_id
     * @param $attribute
     * @param $params
     */
    public function uniqueExternalAndVendor($attribute,$params)
    {
        if(!$this->isNewRecord){
            $if = Product::model()->exists("
            external_id = :external_id AND vendor_id = :vendor_id AND id != :id", array(
                                                   ':external_id'=> $this->external_id,
                                                   ':vendor_id'  => Vendor::getId(),
                                                   ':id'         => $this->id
                                              ));
        }else {
            $if = Product::model()->exists("
            external_id = :external_id AND vendor_id = :vendor_id", array(
                                                                       ':external_id'=> $this->external_id,
                                                                       ':vendor_id'  => Vendor::getId(),
                                                                  ));
        }
        if($if)
            $this->addError('external_id','Артикул должен быть уникальный.');
    }

    public function attributeLabels()
    {
        return array(
            'name'              => 'Заголовок', // Заголовок
            'categoryId'        => 'ID категории товара', // ID категории товара
            'price'             => 'Цена', // цена
            'description'       => 'Описание', // описание
            'available'         => 'Наличие', //true (В наличии), false (Нет в наличии), order (Под заказ)
            'currencyId'        => 'currencyId', //TODO: Игнорируем (Пока)
            'picture'           => 'Изображение', // Скачать и сохранить у нас на сервере
            'id'                => 'ID товара', // указывает id товара из своей БД
            'country_of_origin' => 'Производитель', // Производитель
        );
    }

    /************
     *  Функция сохраяющая атрибуты (характеристики) товара.
     *  Принимаемые параметры:
     *
     *  @array $data - содержит информацию о выбранных пользователем значениях для атрибутов
     *
     ************/
    public function saveFeatures($data = null)
    {
        if (self::getScenario() == 'edit' ||  self::getScenario() == 'editXML')
            $featureValues = ProductAttrValues::model()->findAllByAttributes(array('product_id' => $this->id));

        // Удаляем старые атрибуты, если товар редактируется
        if (!empty($featureValues))
            ProductAttrValues::model()->deleteAll('product_id = :product_id', array(':product_id' => $this->id));

        if (empty($data))
            return false;

        // Пишем новые атрибуты
        foreach ($data as $featureId => $featureValue)
        {
            // Несколько значений у одного атрибута
            if (is_array($featureValue))
            {
                foreach ($featureValue as $value)
                {
                    if ($value == '')
                        continue;

                    $newProductFeature = new ProductAttrValues;
                    $newProductFeature['product_id'] = $this->id;
                    $newProductFeature['attr_id'] = intval($featureId);
                    $newProductFeature['attr_value_id'] = intval($value);
                    $newProductFeature->save();
                }
            }
            // Одно значение у одного атрибута
            else
            {
                if ($featureValue == '')
                    continue;

                $newProductFeature = new ProductAttrValues;
                $newProductFeature['product_id'] = $this->id;
                $newProductFeature['attr_id'] = intval($featureId);
                $newProductFeature['attr_value_id'] = intval($featureValue);
                $newProductFeature->save();
            }
        }

        return true;
    }

    /************
     *  Функция для установки значений по умолчанию (обычно после отправки формы, или при редактировании)
     *  ВАЖНА ТЕМ, ЧТО ЕСЛИ ЗНАЧЕНИЯ НЕТ, ТО УСТАНАВЛИВАЕТСЯ - NULL, а не пустота
     *
     *  Принимаемые значения (* - обязательные параметры):
     *
     *  @string $attribute* - строка, содержащая имя аттрибута
     *  @array  $post* - массив данных, содержащий информацию из $_POST (имя аттрибута => значение)
     *  @array  $model - массив данных, содержащий информацию из CActiveRecord (имя аттрибута => значение)
     *  @array  $on - массив данных, содержащий имена сценариев, в которых следует присвоить значения
     *
     ************/
    public function setDefaultValue($attribute, $post = array(), $model = array(), $on = array())
    {
        if (empty($attribute) || empty($post) && empty($model))
           return 'null';

        $var = 'null';

        // Берем данные из $_POST
        if (isset($post[$attribute]))
        {
            if ($post[$attribute] != '')
                $var = $post[$attribute];
        }
        // Берем данные из модели, но сначала проверим сценарий
        else if (in_array($model->getScenario(), $on))
        {
            if (isset($model[$attribute]))
                $var = $model[$attribute];
        }

        return $var;
    }

    public function deleteAll($condition = '', $params = Array()){

        $product = Product::model()->findAll( $condition );

        if( !count( $product ) ) return false;

        foreach( $product as $item ){

            $item->is_delete = self::DELETE_PRODUCT;

            $item->save();

        }

    }

    public function restoreAll( $condition = '' ){

        $product = Product::model()->findAll( $condition );

        if( !count( $product ) ) return false;

        foreach( $product as $item ){

            $item->is_delete = self::RESTORE_PRODUCT;

            $item->save();

        }

    }

    public function delete(){
        $this->is_delete = self::DELETE_PRODUCT;
        $this->save();
    }

    public function restore(){
        $this->is_delete = self::RESTORE_PRODUCT;
        $this->save();
    }
}
?>