<?php
class ProductImages extends CActiveRecord
{
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'product_images';
    }

    public function rules()
    {
        return array(
            array('img_filename, title, description', 'filter', 'filter' => 'trim'),
            array('img_filename, title, description', 'filter', 'filter' => 'htmlspecialchars'),

            array('source_url', 'safe'),

            array('product_id, vendor_id, img_id, added_date', 'filter', 'filter' => 'intval'),

            array('title', 'length', 'min' => 2, 'max' => 250,
                'tooShort' => '&uarr; слишком короткое название (минимум {min} символов)',
                'tooLong' => '&uarr; слишком длинное название (максимум 200 символов)', 'on' => 'edit'),

            array('description', 'length', 'max' => 1000,
                'tooLong' => '&uarr; слишком длинное описание (максимум допустимо {max} символов)', 'on' => 'edit')
        );
    }

    public static function updateImg( $id, $filename, $imgId ){

        $model = self::model()->findByPk( $id );

        $model->img_filename = $filename;

        $model->img_id = $imgId;

        $model->save();

    }
}