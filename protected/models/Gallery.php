<?php
class Gallery extends CActiveRecord
{
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'gallery';
    }

    public function relations()
    {
        return array(
            'Category' => array(self::BELONGS_TO, 'GalleryCategory', 'cat_id'),
            'Vendor' => array(self::BELONGS_TO, 'Vendor', 'vendor_id')
        );
    }

    public function rules()
    {
        return array(
            array('description, img_filename', 'filter', 'filter' => 'trim'),
            array('description, img_filename', 'filter', 'filter' => 'htmlspecialchars'),

            array('cat_id', 'exist', 'attributeName' => 'id', 'className' => 'GalleryCategory',
                'message' => 'Вы пытаетесь добавить фотографию в несуществующую категорию.'),

            array('cat_id', 'required',
                'message' => 'Пожалуйста, выберите категорию'),

            array('description', 'length', 'max' => 500,
                'tooLong' => 'Слишком длинное описание (максимум допустимо {max} символов)'),

            array('vendor_id, user_id, cat_id, img_id, added_date, views_count', 'filter', 'filter' => 'intval'),

            // Unsafe attributes
            array('vendor_id, user_id, added_date, views_count', 'unsafe')
        );
    }
}