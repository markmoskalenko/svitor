<?php
class ReportCategories extends CActiveRecord
{
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'report_categories';
    }

    public function relations()
    {
        return array(
            'UserReports' => array(self::HAS_MANY, 'UserReports', 'report_cat_id',
                'condition' => 'UserReports.status = "published" OR UserReports.status = "published_verification"')
        );
    }

    public function rules()
    {
        return array(
            array('title, name', 'filter', 'filter' => 'trim'),
            array('title, name', 'filter', 'filter' => 'htmlspecialchars'),

            array('name', 'filter', 'filter' => 'strtolower'),

            array('title', 'required', 'message' => 'Введите наименование'),

            array('title', 'length', 'min' => 2, 'max' => 100,
                'tooShort' => 'Слишком короткое наименование',
                'tooLong' => 'Слишком длинное наименование (максимум {max} символов)'),

            array('name', 'length', 'max' => 100)
        );
    }
}