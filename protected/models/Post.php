<?php
class Post extends CActiveRecord
{
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'posts';
    }

    public function relations()
    {
        return array(
            'PostCategory' => array(self::BELONGS_TO, 'PostCategory', 'post_cat_id'),
            'Vendor' => array(self::BELONGS_TO, 'Vendor', 'author_vid'),
            'User' => array(self::BELONGS_TO, 'User', 'author_uid')
        );
    }

    public function rules()
    {
        return array(
            array('title, content_1, content_2, tags', 'filter', 'filter' => 'trim'),
            array('title, tags', 'filter', 'filter' => 'htmlspecialchars'),

            array('content_1, content_2', 'filter', 'filter' => array('Post', 'postStripTags')),

            array('title, content_2', 'required',
                'message' => '&uarr; заполните это поле'),

            array('content_1', 'required', 'on' => 'verification',
                'message' => '&uarr; заполните это поле'),

            array('post_cat_id', 'required',
                'message' => '&uarr; выберите раздел'),

            array('title', 'length', 'min' => 5, 'max' => 200,
            'tooShort' => '&uarr; слишком короткое название',
            'tooLong' => '&uarr; слишком длинное название (максимум {max} символов)'),

            array('tags', 'safe'),
            array('status', 'unsafe'),
            array('status', 'in', 'range' => array('published', 'published_verification', 'verification', 'rejected', 'draft'))
        );
    }

    public static function postStripTags($value)
    {
        $value = strip_tags($value, '<h1><h2><h3><h4><h5><h6><a><p><strong><b><strike><ol><ul><li><img><iframe><table><tbody><th><tr><td><hr>');

        $value = preg_replace('/font\-family\:([\d\w\s,\'-]*)\;/i', '', $value);
        $value = preg_replace('/font\:([\d\w\s,\'-]*)\;/i', '', $value);

        return $value;
    }
}