<?php
class AttrValues extends CActiveRecord
{

    public $countOnFilter;
    
    public $checkedOnFilter = 0;

    public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function tableName()
	{
		return 'attribute_values';
	}

    public function rules()
    {
        return array(
            array('value_string', 'filter', 'filter' => 'trim', 'on' => 'add, edit'),
            array('value_string', 'filter', 'filter' => 'htmlspecialchars', 'on' => 'add, edit'),

            array('attr_id', 'required',
                'message' => 'Выберите характеристику к которой будет принадлежать это значение', 'on' => 'add, edit'),

            array('value_string', 'required',
                'message' => 'Введите наименование значения', 'on' => 'add, edit'),

            array('value_string', 'uniqueAttrValueString',
                'message' => 'Значение должно быть уникальным', 'on' => 'add, edit'),

            array('value_string', 'length', 'min' => 2, 'max' => 150,
                'tooShort' => 'Слишком короткое значение (минимум 2 символа)',
                'tooLong' => 'Слишком длинное значение (максимум допустимо 255 символов)', 'on' => 'add, edit'),

            array('value_boolean', 'boolean')
        );
    }

    public function uniqueAttrValueString ($attribute, $params)
    {
        if(!$this->hasErrors())
        {
            $params['criteria']= array(
                'condition' => 'attr_id = :attr_id',
                'params' => array(':attr_id' => $this->attr_id)
            );

            $validator = CValidator::createValidator('unique', $this, $attribute, $params);
            $validator->validate($this, $attribute);
        }
    }

    /************
     *   Функция формирует значения атрибутов в зависимости от display_type атрибута (select, radio, checkbox)
     *   Принимаемые параметры (* - обазятальные параметры):
     *
     *   @string $name - имя для атрибута name="" у input-полей (Product или Vendor)
     *   @array  $attribute* - содержит информацию об одном атрибуте (модель Attribute)
     *   @array  $values_data* - сожержит информацию о значениях атрибута (модель AttrValues)
     *   @string $select_value - сожержит значение, которые необходимо "выбрать" по умолчанию
     *   @array  $product_attr_values - содержит значения атрибутов у определенного продукта или компании (attr_id => attr_value_id)
     *
     ************/

    static public function renderByDisplayType($name = 'Product[Features]', $attr_data, $values_data, $select_value = '', $data_attr_values = array())
    {
        if (empty($attr_data) && empty($values_data))
            return;

        if ($name != 'Product[Features]' && $name != 'Vendor[Features]')
            return;

        switch ($attr_data['type'])
        {
            case 'boolean':

                $html_data = '';
                foreach ($values_data as $i => $value)
                {
                    $title = $value['value_boolean'] == 1 ? 'Да' : 'Нет';

                    $select_attr = '';
                    if (isset($select_value) && $select_value == $value['id'] || (empty($select_value) && isset($data_attr_values[$attr_data['id'].$value['id']])))
                    {
                        $select_attr = ' checked="checked"';
                    }
                    else if (empty($select_value) && empty($data_attr_values))
                    {   // По умолчанию выбираем значение "Нет"
                        if ($i == 1)
                            $select_attr = ' checked="checked"';
                    }

                    $html_data .= '<label><input type="radio" name="'.$name.'['.$attr_data['id'].']" value="'.$value['id'].'" '.$select_attr.'> '.$title.'</label>';
                }
                break;


            case 'string':

                // Если можно выбирать нескольно значений, то checkbox
                if ($attr_data['is_multi'] == 1)
                {
                    $html_data = '';
                    foreach ($values_data as $i => $value)
                    {
                        $select_attr = '';
                        if (isset($select_value[$i]) && $select_value[$i] == $value['id'] || (empty($select_value) && isset($data_attr_values[$attr_data['id'].$value['id']])))
                            $select_attr = ' checked="checked"';

                        $html_data .= '<label><input type="checkbox" name="'.$name.'['.$attr_data['id'].']['.$i.']" value="'.$value['id'].'" '.$select_attr.'> '.$value['value_string'].'</label><br>';
                    }
                }
                // Если можно выбирать только одно значение, то radio
                else
                {
                    $html_data = '';
                    foreach ($values_data as $i => $value)
                    {
                        $select_attr = '';
                        if ($select_value == $value['id'] || (empty($select_value) && isset($data_attr_values[$attr_data['id'].$value['id']])))
                        {
                            $select_attr = ' checked="checked"';
                        }
                        else if (empty($select_value))
                        {   // По умолчанию выбираем первое значение
                            if ($i == 0)
                                $select_attr = ' checked="checked"';
                        }

                        $html_data .= '<label><input type="radio" name="'.$name.'['.$attr_data['id'].']" value="'.$value['id'].'" '.$select_attr.'> '.$value['value_string'].'</label><br>';
                    }

                    $html_data .= '</select>';
                }
                break;

            default:
                return;
        }

        return $html_data;
    }
}
?>