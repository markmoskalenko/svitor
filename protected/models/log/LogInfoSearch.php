<?php
class LogInfoSearch extends CActiveRecord
{
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'log_info_search';
    }

    public function rules()
    {
        return array(
            array('uid, ip_address, date', 'filter', 'filter' => 'intval'),

            array('query_text', 'filter', 'filter' => 'trim'),
            array('query_text', 'filter', 'filter' => 'htmlspecialchars')
        );
    }

    public function relations()
    {
        return array(
            'User' => array(self::BELONGS_TO, 'User', 'uid'),
            'Category' => array(self::BELONGS_TO, 'Category', 'cat_id')
        );
    }
}