<?php
class Pages extends CActiveRecord
{
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'pages';
    }

    public function rules()
    {
        return array(
            array('name, title, title_h1, keywords, description', 'filter', 'filter' => 'trim'),
            array('name, title, title_h1, keywords, description', 'filter', 'filter' => 'htmlspecialchars'),

            array('name', 'match', 'pattern' => '/[a-z0-9-_]/i',
                'message' => 'Имя страницы должно содержать только латинские символы (также допустимы знаки "-" и "_")'),

            array('name', 'match', 'pattern' => '/[-_]{2,}/i', 'not' => true,
                'message' => 'В имени страницы не должно быть более одного символа "_" или "-" подряд'),


            array('name', 'required', 'message' => 'Введите имя страницы'),
            array('title, title_h1', 'required', 'message' => 'Введите заголовок страницы'),

            array('content', 'safe'),
            array('visible', 'in', 'range' => array(0, 1))
        );
    }
}