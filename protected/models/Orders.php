<?php
class Orders extends CActiveRecord
{
    /* Статусы заказа */
    const STATUS_NOT_CONFIRMED       = -1;
    const STATUS_NOT_CONFIRMED_TITLE = 'Не подтвержден';

    const STATUS_QUEUED       = 0;
    const STATUS_QUEUED_TITLE = 'В очереди';

    const STATUS_TREATED       = 1;
    const STATUS_TREATED_TITLE = 'Рассматривается';

    const STATUS_WORK          = 2;
    const STATUS_WORK_TITLE    = 'Принят в работу';

    const STATUS_SENT          = 3;
    const STATUS_SENT_TITLE    = 'Отправлен';

    const STATUS_CLOSED        = 4;
    const STATUS_CLOSED_TITLE  = 'Закрыт';

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'orders';
    }

    public function rules()
    {
        return array(
            array('name, phone, surname, middle_name, address, vendor_comment','filter','filter'=>array($obj=new Purifier(),'rest')),

            array('name, phone, user_id, vendor_id', 'required'),
            array('status, delivery', 'numerical', 'integerOnly' => true),
            array('surname, name, middle_name, phone, user_id, vendor_id', 'length', 'max' => 20),
            array('address', 'length', 'max' => 200),
            array('vendor_comment', 'safe'),
            array(
                'id, surname, name, middle_name, phone, address, user_id, status, delivery, vendor_comment, vendor_id',
                'safe',
                'on' => 'search'
            ),
        );
    }

    public function relations()
    {
        return array(
            'bondOrders' => array(self::HAS_MANY, 'BondOrders', 'order_id'),
            'vendor'     => array(self::BELONGS_TO, 'Vendor', 'vendor_id'),
            'user'       => array(self::BELONGS_TO, 'User', 'user_id'),
            'sum'        => array(self::STAT, 'BondOrders', 'order_id', 'select' => 'SUM(`price` * `count`)')
        );
    }

    public function defaultScope(){
        return array(
            'order'=>"id DESC"
        );
    }

    public function scopes(){
        return array(
            'my'=>array(
                'condition' => 'user_id = '.User::getId()
            ),
            'myVendor'=>array(
                'condition' => 'vendor_id = '.(isset($_SESSION["vendor__id"]) ? $_SESSION["vendor__id"] : -1)
            ),
            'notConfirmed'=>array(
                'condition' => 'status = '.self::STATUS_NOT_CONFIRMED
            ),
            'confirmed'=>array(
                'condition' => 'status != '.self::STATUS_NOT_CONFIRMED
            ),
            'notClosed'=>array(
                'condition' => 'status != '.self::STATUS_CLOSED
            ),
            'notSend'=>array(
                'condition' => 'status != '.self::STATUS_SENT
            ),
            'closed'=>array(
                'condition' => 'status = '.self::STATUS_CLOSED
            ),
            'queued'=>array(
                'condition' => 'status = '.self::STATUS_QUEUED
            )
        );
    }

    public function beforeValidate(){
        if($this->isNewRecord){
            $this->user_id = User::getId();
            $this->status  = self::STATUS_NOT_CONFIRMED;
        }

        return parent::beforeValidate();
    }

    public function attributeLabels()
    {
        return array(
            'id'             => 'Заказ №:',
            'surname'        => 'Фамилия',
            'name'           => 'Имя',
            'middle_name'    => 'Отчество',
            'phone'          => 'Контактный телефон',
            'address'        => 'Адрес',
            'user_id'        => 'Заказчик',
            'status'         => 'Статус исполнения',
            'delivery'       => 'Стоимость доствки',
            'vendor_comment' => 'Комментарий',
            'vendor_id'      => 'Исполнитель',
        );
    }

    /**
     * Количество новых (не просмотренных) заказов у компании
     * @return mixed
     */
    public static function getCountNewOrders(){

        return self::model()->myVendor()->queued()->count();

    }

    /**
     * Смена статуса заказ из кабинета компании
     * Смена стасу продавцом возможна только если заказ не закрыт и подтвержден покупателем.
     *
     * @param $orderID
     * @param $status
     *
     * @return bool
     */
    public static function changeStatus( $orderID, $status ){

        if( $status != self::STATUS_QUEUED  &&
            $status != self::STATUS_TREATED &&
            $status != self::STATUS_WORK    &&
            $status != self::STATUS_SENT    &&
            $status != self::STATUS_CLOSED
        )   return false;


        $model = self::model()->notClosed()->confirmed()->myVendor()->findByPk( $orderID );

        if( $model ){

            // Пристатусе отправлен заказ моно только сохранить
            if( $model->status == self::STATUS_SENT && $status != self::STATUS_CLOSED ) return false;

            $model->status = $status;

            if( $model->save() ) return true;

        }

        return false;
    }

    /**
     * Сохранение комментария продавца к заказу.
     *
     * @param $orderID
     * @param $comment
     *
     * @return bool
     */
    public static function changeVendorComment( $orderID, $comment ){

        if( !is_int( $orderID ) ) return false;

        $model = self::model()->confirmed()->myVendor()->findByPk( $orderID );

        if( $model ){

            $model->vendor_comment = $comment;

            if( $model->save() ) return true;

        }

        return false;
    }

    /**
     * Возвращает title статуса
     * @param $statusCode - Код статуса
     *
     * @return bool|string
     */
    public static function getStatusTitle( $statusCode ){

        switch ($statusCode) {
            case self::STATUS_NOT_CONFIRMED:
                return self::STATUS_NOT_CONFIRMED_TITLE;// Не подтвержден
            case self::STATUS_QUEUED:
                return self::STATUS_QUEUED_TITLE;       // В очереди
            case self::STATUS_TREATED:
                return self::STATUS_TREATED_TITLE;      // Рассматривается
            case self::STATUS_WORK:
                return self::STATUS_WORK_TITLE;         // Принят в работу
            case self::STATUS_SENT:
                return self::STATUS_SENT_TITLE;         // Отправлен
            case self::STATUS_CLOSED:
                return self::STATUS_CLOSED_TITLE;       // Закрыт
        }

        return false;
    }

    /**
     * Массив статусов код => заголовок
     * Сформирован для dropdown
     * @return array
     */
    public static function getStatusOnFilter( ){
        return array(
            self::STATUS_NOT_CONFIRMED => self::STATUS_NOT_CONFIRMED_TITLE,
            self::STATUS_QUEUED        => self::STATUS_QUEUED_TITLE,
            self::STATUS_TREATED       => self::STATUS_TREATED_TITLE,
            self::STATUS_WORK          => self::STATUS_WORK_TITLE,
            self::STATUS_SENT          => self::STATUS_SENT_TITLE,
            self::STATUS_CLOSED        => self::STATUS_CLOSED_TITLE,
        );
    }

    /**
     * Изменение состояния доставки.
     * 0 - нет доставки, Больше 0 - это стоимость доставки.
     * @param $id    - id заказа
     * @param $value - значение
     *
     * @return bool|CActiveRecord
     */

    public static function saveDelivery( $id, $value ){

        $model = self::model()->notClosed()->myVendor()->notSend()->confirmed()->findByPk($id);

        if( !$model ) return false;

        $model->delivery = intval($value);

        if($model->save()) return $model;

        return false;

    }

    /**
     *
     * @param $order_id
     *
     * @return mixed
     *
     * Редактирование для пользователя.
     *
     * notConfirmed() Указывает что заказ еще не у продовца (НЕ ПОДТВЕРЖДЕН)
     *
     */
    public static function isEdit( $order_id ){
        return self::model()->my()->notConfirmed()->exists('id = :order_id', array(':order_id' => $order_id));
    }

    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id, true);
        $criteria->compare('surname', $this->surname, true);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('middle_name', $this->middle_name, true);
        $criteria->compare('phone', $this->phone, true);
        $criteria->compare('address', $this->address, true);
        $criteria->compare('user_id', $this->user_id, true);
        $criteria->compare('status', $this->status);
        $criteria->compare('delivery', $this->delivery);
        $criteria->compare('vendor_comment', $this->vendor_comment, true);
        $criteria->compare('vendor_id', $this->vendor_id, true);

        return new CActiveDataProvider($this, array(
                                                   'criteria' => $criteria,
                                              ));
    }
}