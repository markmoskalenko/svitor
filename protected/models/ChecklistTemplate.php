<?php
class ChecklistTemplate extends CActiveRecord
{
    public $wedding_cat_min_id;
    public $holiday_cat_min_id;

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'checklist_templates';
    }

    public function relations()
    {
        return array(
            'Cat' => array(self::BELONGS_TO, 'CategoryMiniatures', 'category_id')
        );
    }

    public function rules()
    {
        return array(
            array('fun_type', 'required', 'message' => 'Необходимо выбрать сайт'),
            array('wedding_type_id, holiday_type_id', 'typeIdValidate', 'message' => 'Необходимо выбрать тип'),

            array('wedding_cat_min_id, holiday_cat_min_id', 'catMinIdValidate', 'message' => 'Необходимо выбрать категорию'),

            array('wedding_cat_min_id, holiday_cat_min_id', 'filter', 'filter' => 'intval'),
            array('cat_min_id', 'filter', 'filter' => 'intval'),

            array('task, budget_title, link', 'filter', 'filter' => 'trim'),
            array('task, budget_title, link', 'filter', 'filter' => 'htmlspecialchars'),

            array('task', 'required', 'message' => 'Введите текст для задачи'),

            array('task', 'length', 'max' => '255', 'on' => 'add, edit',
                'tooLong' => 'Текст задачи не должен превышать размер в 255 символов'),

            array('budget_title', 'length', 'max' => '255', 'on' => 'add, edit',
                'tooLong' => 'Наименование для бюджета не должено превышать размер в 255 символов'),

            array('link', 'url', 'message' => 'Указанная ссылка не является правильным URL'),

            array('fun_type', 'in', 'range' => array('wedding', 'holiday', 'multi')),
            array('wedding_type_id, holiday_type_id', 'default', 'setOnEmpty' => true, 'value' => 0)
        );
    }

    public function typeIdValidate($attribute, $params)
    {
        if (!$this->getError('holiday_type_id') && $this->fun_type == 'holiday' && empty($this->holiday_type_id))
        {
            $this->addError('holiday_type_id', $params['message']);
        }
        else if (!$this->getError('wedding_type_id') && $this->fun_type == 'wedding' && empty($this->wedding_type_id))
        {
            $this->addError('wedding_type_id', $params['message']);
        }
    }

    public function catMinIdValidate ($attribute, $params)
    {
        if (!$this->getError('holiday_cat_min_id') && $this->fun_type == 'holiday' && empty($this->holiday_cat_min_id))
        {
            $this->addError('holiday_cat_min_id', $params['message']);
        }
        else if (!$this->getError('wedding_cat_min_id') &&$this->fun_type == 'wedding' && empty($this->wedding_cat_min_id))
        {
            $this->addError('wedding_cat_min_id', $params['message']);
        }
    }
}
?>