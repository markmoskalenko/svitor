<?php
class GalleryCategory extends CActiveRecord
{
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'gallery_categories';
    }

    public function rules()
    {
        return array(
            array('title, name', 'filter', 'filter' => 'trim'),
            array('title, name', 'filter', 'filter' => 'htmlspecialchars'),
            array('name', 'filter', 'filter' => 'strtolower'),
            array('name', 'length', 'max' => 120),
            array('title', 'required', 'message' => 'Введите название'),
            array('title', 'length', 'min' => 2, 'max' => 100,
                'tooShort' => 'Слишком короткое название',
                'tooLong' => 'Слишком длинное название (максимум {max} символов)')
        );
    }
}