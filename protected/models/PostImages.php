<?php
class PostImages extends CActiveRecord
{
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'posts_image';
    }

    public function relations()
    {
        return array(
            'Post' => array(self::HAS_MANY, 'Post', 'id',
                'condition' => '(Post.id = PostImages.post_id)')
        );
    }

    public function rules()
    {
        return array();
    }
}