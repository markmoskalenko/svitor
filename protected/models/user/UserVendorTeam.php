<?php
class UserVendorTeam extends CActiveRecord
{
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'user_vendor_team';
    }

    public function relations()
    {
        return array(
            'Vendor' => array(self::BELONGS_TO, 'Vendor', 'vendor_id')
        );
    }

    public function rules()
    {
        return array(
            array('note', 'filter', 'filter' => 'trim'),
            array('note', 'filter', 'filter' => 'htmlspecialchars'),
            array('user_cat_id, vendor_id', 'required'),
            array('note', 'filter', 'filter' => 'htmlspecialchars'),

            array('note', 'length', 'max' => 500,
                'tooLong' => 'Слишком длинная заметка (максимум {max} символов)')
        );
    }
}