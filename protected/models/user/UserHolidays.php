<?php
class UserHolidays extends CActiveRecord
{
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'user_holidays';
    }

    public function rules()
    {
        return array(
            array('title, description', 'filter', 'filter' => 'trim'),
            array('title, description', 'filter', 'filter' => 'htmlspecialchars'),

            array('title', 'required', 'message' => 'Введите название', 'on' => 'add, edit'),

            array('uid', 'required', 'on' => 'add, edit'),

            array('date', 'required', 'message' => 'Укажите дату', 'on' => 'add, edit'),

            array('title', 'length', 'max' => 100,
                'tooLong' => 'Слишком длинное название (допустимо {max} символов)', 'on' => 'add, edit'),

            array('description', 'length', 'max' => 500,
                'tooLong' => 'Слишком длинное описание (допустимо {max} символов)', 'on' => 'add, edit'),

            array('date', 'date', 'format' => 'dd.MM.yyyy',
                'message' => 'Неверный формат даты', 'on' => 'add, edit'),

            array('seating_room_width, seating_room_height', 'default', 'value' => 10, 'on' => 'add, edit'),
            array('seating_room_width, seating_room_height', 'filter', 'filter' => 'intval', 'on' => 'set_room_size'),
        );
    }
}