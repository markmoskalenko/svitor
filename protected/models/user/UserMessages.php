<?php
class UserMessages extends CActiveRecord
{
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'user_messages';
    }

    public function relations()
    {
        return array(
            'FromUser' => array(self::BELONGS_TO, 'User', 'from_uid'),
            'ToUser' => array(self::BELONGS_TO, 'User', 'to_uid'),
            'FromVendor' => array(self::BELONGS_TO, 'Vendor', 'from_vid'),
            'ToVendor' => array(self::BELONGS_TO, 'Vendor', 'to_vid'),
        );
    }

    public function rules()
    {
        return array(
            array('date', 'default', 'value' => time()),

            array('is_bid, to_uid, to_vid, from_uid, from_vid, date', 'filter', 'filter' => 'intval'),
            array('title, message', 'filter', 'filter' => 'trim'),
            array('title, message', 'filter', 'filter' => 'htmlspecialchars'),

            array('title', 'required', 'message' => 'Заполните поле "Тема"'),
            array('message', 'required', 'message' => 'Сообщение не может быть пустым'),

            array('title', 'length', 'max' => 100,
                'tooLong' => 'Слишком длинная тема (допустимо {max} символов)'),

            array('message', 'length', 'max' => 3000,
                'tooLong' => 'Слишком длинное сообщение (допустимо {max} символов)'),

            array('to_status', 'in', 'range' => array('new','readed','deleted')),
            array('from_status', 'in', 'range' => array('sended','deleted')),
        );
    }



    public static function getCountInbox($alt = '')
    {
        if (Yii::app()->user->isGuest)
            return 0;

        if ($alt == 'vendor_cp')
            return self::model()->count('to_vid = ' . Vendor::getId() . ' AND to_status = "new"');

        return self::model()->count('to_uid = ' . User::getId() . ' AND to_status = "new"');
    }

    public static function getCountBid()
    {
        if (Yii::app()->user->isGuest)
            return 0;
        return self::model()->count('to_uid = ' . User::getId() . ' AND to_status = "new" AND is_bid != 0');
    }

    public static function sendEmailNotify($to, $from, $msg, $alt = 'user')
    {
        if (empty($to) || empty($from) || empty($msg))
            return false;

        if ($alt != 'user' && $alt != 'vendor')
            $alt = 'user';

        $mail = Yii::app()->PearMail;

        // Письмо от пользователя компании/пользователю
        if ($alt == 'user')
        {
            $inbox_url = 'http://' . Yii::app()->request->getServerName() . Yii::app()->createUrl('vendor_cp/messages/index');
            $from_name = '<a href="http://' . Yii::app()->request->getServerName() . Yii::app()->createUrl('user/profile', array('id' => $from->id)) . '">' . $from->login . '</a>';

            $subject = 'У вас есть новое личное сообщение от ' . $from->login . ': ' . $msg->title;
        }
        // Письмо от компании пользователю
        else if ($alt == 'vendor')
        {
            $inbox_url = 'http://' . Yii::app()->request->getServerName() . Yii::app()->createUrl('messages/index');
            $from_name = '<a href="http://' . Yii::app()->request->getServerName() . Yii::app()->createUrl('vendor/profile', array('id' => $from->id)) . '">' . mApi::cutStr($from->title, 50) . '</a>';

            $subject = 'У вас есть новое личное сообщение от ' . mApi::cutStr($from->title, 20) . ': ' . $msg->title;
        }

        $headers["Content-Type"] = "text/html; charset=UTF-8";
        $headers['Subject'] = $subject;
        $headers['From'] = 'noreply@svitor.ru';

        // Берём шаблон
        $body = file_get_contents(Yii::app()->params['email_templates_path'] . '/notifications/notify_new_msg.php');

        // Заполняем нужными данными
        $body = str_replace('{from_name}', $from_name, $body);
        $body = str_replace('{msg_date}', mApi::getDateWithMonth($msg->date), $body);
        $body = str_replace('{msg_title}', $msg->title, $body);
        $body = str_replace('{msg}', nl2br($msg->message), $body);
        $body = str_replace('{messages_inbox_url}', $inbox_url, $body);

        // Отправляем
        return $mail->send($to->email, $headers, $body);
    }
}