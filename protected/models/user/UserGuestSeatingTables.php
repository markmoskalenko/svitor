<?php
class UserGuestSeatingTables extends CActiveRecord
{
    public static function model($className =__CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'user_guest_seating_tables';
    }

    public function relations()
    {
        return array(
            'Guests' => array(self::HAS_MANY, 'UserGuests', 'table_id', 'order' => 't.title ASC, Guests.fullname ASC', 'with' => 'GuestCategory')
        );
    }

    public function rules()
    {
        return array(
            array('uid, wedding_id, holiday_id, type, width, height, places, diameter', 'filter', 'filter' => 'intval'),
            array('rotation, position_x, position_y', 'filter', 'filter' => 'floatval'),

            array('note, title', 'filter', 'filter' => 'trim'),
            array('note, title', 'filter', 'filter' => 'htmlspecialchars'),
            array('note', 'length', 'max' => 500),

            array('title', 'length', 'max' => 150)
        );
    }
}