<?php
class UserRoutine extends CActiveRecord
{
    public static function model($className =__CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'user_routine';
    }

    public function relations()
    {
        return array(
            'Vendor' => array(self::BELONGS_TO, 'Vendor', 'vendor_id')
        );
    }

    public function rules()
    {
        return array(
            array('time, event, address, note', 'filter', 'filter' => 'trim'),
            array('time, event, address, note', 'filter', 'filter' => 'htmlspecialchars'),
            array('vendor_id', 'filter', 'filter' => 'intval'),

            array('time', 'required',
                'message' => 'Выберите время'),

            array('event', 'required',
                'message' => 'Введите информацию в поле "Событие"'),

            array('event', 'length', 'max' => '255',
                'tooLong' => 'Поле "Событие" не должно превышать размер в {max} символов'),

            array('address', 'length', 'max' => '255',
                'tooLong' => 'Поле "Адрес" не должно превышать размер в {max} символов'),

            array('note', 'length', 'max' => '255',
                'tooLong' => 'Примечание не должно превышать размер в {max} символов'),

            array('time', 'date', 'format' => 'mm:ss',
                'message' => 'Неверный формат времени')
        );
    }
}