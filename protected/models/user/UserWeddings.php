<?php
class UserWeddings extends CActiveRecord
{
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'user_weddings';
    }

    public function rules()
    {
        return array(
            array('bride_name, groom_name, date', 'filter', 'filter' => 'trim'),
            array('bride_name, groom_name, date', 'filter', 'filter' => 'htmlspecialchars'),

            array('bride_name', 'required',
                'message' => 'Введите имя невесты', 'on' => 'add, edit'),

            array('groom_name', 'required',
                'message' => 'Введите имя жениха', 'on' => 'add, edit'),

            array('date', 'required',
                'message' => 'Укажите дату свадьбы', 'on' => 'add, edit'),

            array('bride_name', 'length', 'min' => 2, 'max' => 110,
                'tooShort' => 'Слишком короткое имя невесты',
                'tooLong' => 'Слишком длинное имя невесты', 'on' => 'add, edit'),

            array('groom_name', 'length', 'min' => 2, 'max' => 110,
                'tooShort' => 'Слишком короткое имя жениха',
                'tooLong' => 'Слишком длинное имя жениха', 'on' => 'add, edit'),

            array('date', 'date', 'format' => 'dd.MM.yyyy',
                'message' => 'Неверный формат даты', 'on' => 'add, edit'),

            array('seating_room_width, seating_room_height', 'default', 'value' => 10, 'on' => 'add, edit'),
            array('seating_room_width, seating_room_height', 'filter', 'filter' => 'intval', 'on' => 'set_room_size'),
        );
    }
}