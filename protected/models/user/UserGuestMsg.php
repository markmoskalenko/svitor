<?php
class UserGuestMsg extends CActiveRecord
{
    public static function model($className =__CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'user_guest_msg';
    }

    public function rules()
    {
        return array(
            array('content', 'filter', 'filter' => 'trim'),
            array('content', 'filter', 'filter' => 'htmlspecialchars'),

            array('content', 'length', 'max' => 500,
                'tooLong' => '&uarr; слишком длинное обращение')
        );
    }
}