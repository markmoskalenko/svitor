<?php
class UserFriends extends CActiveRecord
{
    public static function model($className = __CLASS__)
    {
    	return parent::model($className);
    }

    public function tableName()
    {
    	return 'user_friends';
    }

    public function relations()
    {
        return array(
            'User' => array(self::BELONGS_TO, 'User', 'fid'),
            'UserRequests' => array(self::BELONGS_TO, 'User', 'uid')
        );
    }

    public function rules()
    {
        return array(
            array('uid, fid', 'filter', 'filter' => 'intval'),
            array('status', 'in', 'range' => array('request', 'friend'))
        );
    }

    public static function getCountRequests()
    {
        if (Yii::app()->user->isGuest)
            return 0;

        return self::model()->count('fid = ' . User::getId() . ' AND status = "request"');
    }
}