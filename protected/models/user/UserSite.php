<?php
class UserSite extends CActiveRecord
{
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'user_site';
    }

    public function relations()
    {
        return array(
            'Template' => array(self::BELONGS_TO, 'UsiteTemplates', 'template_id'),
            'TemplateStyle' => array(self::BELONGS_TO, 'UsiteTemplateStyles', 'template_style_id')
        );
    }

    public function rules()
    {
        return array(
            array('title, content', 'filter', 'filter' => 'trim'),
            array('wedding_id, holiday_id, template_id, template_style_id, show_routine, show_guests_list, show_guests_seating, show_vendors, show_wanna_gifts, show_gifts, show_photos, is_available', 'filter', 'filter' => 'intval'),
            array('content', 'filter', 'filter' => array('UserSite', 'postStripTags')),

            array('title', 'required',
                'message' => 'Введите название сайта'),

            array('show_routine, show_guests_list, show_guests_seating, show_vendors, show_wanna_gifts, show_gifts, show_photos, is_available', 'in', 'range' => array(0, 1))
        );
    }

    public static function postStripTags($value) {
        return strip_tags($value, '<a><p><span><strong><b><strike><ol><ul><li><img><iframe><table><tbody><th><tr><td><hr>');
    }
}