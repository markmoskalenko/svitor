<?php
class UserDialogs extends CActiveRecord
{
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function relations()
    {
        return array(
            'User' => array(self::BELONGS_TO, 'User', 'who_uid'),
        );
    }

    public function tableName()
    {
        return 'user_dialogs';
    }
}
?>