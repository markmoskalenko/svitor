<?php
class UserLogVendors extends CActiveRecord
{
    public static function model($className =__CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'user_log_vendors';
    }

    public function relations()
    {
        return array(
            'User' => array(self::BELONGS_TO, 'User', 'uid'),
            'UserCategory' => array(self::BELONGS_TO, 'UserVendorCategories', 'user_cat_id'),
            'UserWedding' => array(self::BELONGS_TO, 'UserWeddings', 'wedding_id'),
            'UserHoliday' => array(self::BELONGS_TO, 'UserHolidays', 'holiday_id'),
            'Vendor' => array(self::BELONGS_TO, 'Vendor', 'vendor_id'),
        );
    }

    public function rules()
    {
        return array(
            array('date', 'default', 'value' => time()),
            array('status', 'default', 'value' => 'now'),

            array('uid, vendor_id, date, user_cat_id', 'numerical'),

            array('event', 'in', 'range' => array(
                'add_to_team',
                'add_to_favorites',
                'remove_from_team',
                'remove_from_favorites'
            )),
            array('status', 'in', 'range' => array(
                'new',
                'viewed'
            ))
        );
    }

    public static function getCountNewNotices()
    {
        return self::model()->count('vendor_id = ' . Vendor::getId() . ' AND status = "new"');
    }

    public static function getEventInfo($notice)
    {
        $notice_text = '';
        $user_profile_url = '/user/' . $notice['User']['id'];

        switch ($notice->event)
        {
            case 'add_to_team':
                if (!empty($notice['UserWedding'])) {
                    $notice_text = 'Пользователь <b><a href="' . $user_profile_url . '" target="_blank">' . $notice['User']['login'] . '</a></b> добавил вас в свою свадебную команду (категория &laquo;' . $notice['UserCategory']['title']  . '&raquo;)';
                }else {
                    $notice_text = 'Пользователь <b><a href="' . $user_profile_url . '" target="_blank">' . $notice['User']['login'] . '</a></b> добавил вас в свою торжественную команду (категория &laquo;' . $notice['UserCategory']['title']  . '&raquo;)';
                }
                break;

            case 'remove_from_team':
                $notice_text = 'Пользователь <b><a href="' . $user_profile_url . '" target="_blank">' . $notice['User']['login'] . '</a></b> удалил вас из своей команды';
                break;

            case 'add_to_favorites':
                $notice_text = 'Пользователь <b><a href="' . $user_profile_url . '" target="_blank">' . $notice['User']['login'] . '</a></b> добавил вашу компанию в избранные';
                break;

            case 'remove_from_favorites':
                $notice_text = 'Пользователь <b><a href="' . $user_profile_url . '" target="_blank">' . $notice['User']['login'] . '</a></b> удалил вашу компанию из избранных';
                break;
        }

        echo $notice_text;
    }
}