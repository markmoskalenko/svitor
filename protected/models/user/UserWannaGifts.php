<?php
class UserWannaGifts extends CActiveRecord
{
    public static function model($className =__CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'user_wanna_gifts';
    }

    public function relations()
    {
        return array(
            'Guest' => array(self::BELONGS_TO, 'UserGuests', 'guest_id')
        );
    }

    public function rules()
    {
        return array(
            array('title, description', 'filter', 'filter' => 'trim'),
            array('title, description', 'filter', 'filter' => 'htmlspecialchars'),

            array('title', 'required',
                'message' => 'Введите название'),

            array('title', 'length', 'max' => 100,
                'tooLong' => 'Слишком длинное название'),

            array('description', 'length', 'max' => 255,
                'tooLong' => 'Слишком длинное описание'),

            array('guest_id', 'filter', 'filter' => 'intval'),

            array('link', 'url',
                'message' => 'Указанная вами ссылка не является правильным URL')
        );
    }
}