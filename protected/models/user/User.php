<?php
class User extends CActiveRecord
{
    // Registration
    public $confirm_password;
    public $captcha_code;
    public $accept_terms;

    // Change Password
    public $old_password;
    public $new_password;
    public $confirm_new_password;

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function relations()
    {
        $held_time = time() - (3600 * 24);

        return array(
            'Friends' => array(self::HAS_MANY, 'UserFriends', 'id'),
            'Messages' => array(self::HAS_MANY, 'UserMessages', 'id'),
            'Dialogs' => array(self::HAS_MANY, 'UserDialogs', 'id'),

            // Всего свадеб и торжеств
            'TotalWeddingCount' => array(self::STAT, 'UserWeddings', 'uid'),
            'TotalHolidayCount' => array(self::STAT, 'UserHolidays', 'uid'),

            // Состоявшиеся свадьбы и торжества
            'HeldWeddingCount' => array(self::STAT, 'UserWeddings', 'uid',
                                        'condition' => 'date < ' . $held_time),
            'HeldHolidayCount' => array(self::STAT, 'UserHolidays', 'uid',
                                        'condition' => 'date < ' . $held_time),
        );
    }

    public function tableName()
    {
        return 'users';
    }

    public function rules()
    {
        return array(
            array('gender', 'in', 'range' => array(0,1)),

            array('gender', 'default', 'value' => 0),
            array('last_ip', 'default', 'value' => Yii::app()->request->userHostAddress),
            array('user_agent', 'default', 'value' => Yii::app()->request->userAgent),

            // ONLY FOR SELECT LOCATION
            array('country_id, region_id, city_id', 'numerical', 'on' => 'edit_area'),

            // ONLY FOR CHANGE PASSWORD
            array('old_password, new_password, confirm_new_password', 'required',
                  'message' => '&uarr; заполните это поле', 'on' => 'change_password'),

            array('old_password', 'oldPassword', 'on' => 'change_password'),

            array('new_password', 'length', 'min' => 6, 'max' => 70,
                  'tooShort' => '&uarr; слишком короткий пароль (минимум {min} символов)',
                  'tooLong' => '&uarr; слишком длинный пароль (максимум {max} символов)', 'on' => 'change_password'),

            array('new_password', 'compare', 'compareAttribute' => 'old_password', 'strict' => true, 'operator' => '!=',
                  'message' => '&uarr; новый пароль должен отличаться от старого', 'on' => 'change_password'),

            array('confirm_new_password', 'compare', 'compareAttribute' => 'new_password', 'strict' => true,
                  'message' => '&uarr; пароли не совпадают', 'on' => 'change_password'),

            // ONLY FOR EDIT
            array('img_filename', 'filter', 'filter' => 'trim', 'on' => 'edit'),
            array('img_filename', 'filter', 'filter' => 'htmlspecialchars', 'on' => 'edit'),

            array('img_id', 'filter', 'filter' => 'intval', 'on' => 'edit'),

            array('email', 'email', 'message' => '&uarr; некорректный E-Mail', 'on' => 'edit'),
            array('email', 'unique', 'message' => '&uarr; такой E-Mail уже кем-то используется', 'on' => 'edit'),

            // ONLY FOR REGISTRATION
            array('login, vkontakte_id, facebook_id, twitter_id, odnoklassniki_id, name, family', 'filter', 'filter' => 'trim', 'on' => 'registration, auth_service'),
            array('login, vkontakte_id, facebook_id, twitter_id, odnoklassniki_id, name, family', 'filter', 'filter' => 'htmlspecialchars', 'on' => 'registration, auth_service'),

            array('login', 'length', 'min' => 3, 'max' => 30,
                  'tooShort' => '&uarr; слишком короткий логин (минимум {min} символа)',
                  'tooLong' => '&uarr; слишком длинный логин (максимум {max} символов)', 'on' => 'registration, auth_service'),

            array('login', 'match', 'pattern' => '/^[a-z0-9-_]+$/i',
                  'message' => '&uarr; логин может содержать только латинские буквы и цифры (допустимы также знаки "-", "_")', 'on' => 'registration, auth_service'),

            array('login', 'match', 'pattern' => '/[-_]{2,}/i', 'not' => true,
                  'message' => '&uarr; логин не должен содержать более одного знака  "-" или "_" подряд', 'on' => 'registration, auth_service'),

            array('login', 'unique',
                  'message' => '&uarr; такой логин уже кем-то используется', 'on' => 'registration, auth_service'),

            array('email', 'email', 'message' => '&uarr; некорректный E-Mail', 'on' => 'registration'),
            array('email', 'unique', 'message' => '&uarr; такой E-Mail уже кем-то используется', 'on' => 'registration'),

            array('accept_terms', 'in', 'range' => array('1'),
                  'message' => '&uarr; необходимо ваше согласие', 'on' => 'registration'),

            //array('captcha_code', 'captcha', 'captchaAction' => 'captcha', 'caseSensitive' => true,
            //'message' => '&uarr; не правильно указан код', 'on' => 'registration'),

            array('email, login, password, confirm_password', 'required',
                  'message' => '&uarr; заполните это поле', 'on' => 'registration'),

            array('password', 'length', 'min' => 6, 'max' => 70,
                  'tooShort' => '&uarr; слишком короткий пароль (минимум {min} символов)',
                  'tooLong' => '&uarr; слишком длинный пароль (максимум {max} символов)', 'on' => 'registration'),

            array('confirm_password', 'compare', 'compareAttribute' => 'password', 'strict' => true,
                  'message' => '&uarr; пароли не совпадают', 'on' => 'registration'),

            array('reg_date', 'default', 'value' => time(), 'on' => 'registration, auth_service'),

            array('id, salt, last_activity', 'unsafe'),
            array('last_activity', 'filter', 'filter' => 'intval'),
            array('notify_site_news, notify_new_msg', 'filter', 'filter' => 'intval', 'on' => 'notify'),
            array('notify_site_news, notify_new_msg', 'in', 'range' => array('0','1'), 'on' => 'notify')
        );
    }

    public function oldPassword($attribute, $params)
    {
        $old_password = hash('md5', $this->salt.strtoupper($this->$attribute));
        if ($this->password != $old_password)
            $this->addError($attribute, '&uarr; текущий пароль введен не верно');
    }

    public function beforeSave()
    {
        if ($this->getScenario() == 'registration' || $this->getScenario() == 'change_password')
        {
            $this->salt = $this->generateSalt();
            $this->password = hash('md5', $this->salt.strtoupper($this->password));
        }
        return true;
    }

    public function generateSalt($max = 30)
    {
        $salt = '';
        $chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%&*?';
        for ($i = 0; $i < $max; $i++)
            $salt .= $chars[mt_rand(0, strlen($chars) - 1)];

        return $salt;
    }


    static public function updateLastActivity()
    {
        if (self::isGuest())
            return false;

        $user = self::model()->findByPk(User::getId());
        $user->last_ip = Yii::app()->request->getUserHostAddress();
        $user->last_activity = time();

        if ($user->update()) {
            // Обновим состояние
            Yii::app()->user->setState('last_activity', $user->last_activity);

            return true;
        } else {
            return false;
        }
    }

    public static function getLastActivity()
    {
        return Yii::app()->user->getState('last_activity', false);
    }

    static public function getId()
    {
        return Yii::app()->user->getId();
    }

    public static function getInfo($id = 0)
    {
        if (empty($id))
        {
            $id = self::getId();
            if (empty($id))
                return false;
        }

        return User::model()->findByAttributes(array('id' => $id));
    }

    public function getLocale($id = null)
    {
        // TODO: брать значение из базы
        return Yii::app()->session['_lang'];
    }

    static public function getSecurityLevel()
    {
        return Yii::app()->user->getState('security_level', 0);
    }

    static public function getCountryId()
    {
        return Yii::app()->user->getState('country_id', 0);
    }

    static public function getRegionId()
    {
        return Yii::app()->user->getState('region_id', 0);
    }

    static public function getCityId()
    {
        return Yii::app()->user->getState('city_id', 0);
    }

    public static function isGuest()
    {
        return Yii::app()->user->isGuest;
    }
}