<?php
class UserBudget extends CActiveRecord
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'user_budget';
    }

    public function rules()
    {
        return array(
            array('cat_min_id', 'required',
                'message' => 'Выберите группу'),

            array('title, note', 'filter', 'filter' => 'trim'),
            array('title, note', 'filter', 'filter' => 'htmlspecialchars'),
            array('amount_plan, amount_fact, paid_amount', 'filter', 'filter' => 'intval'),

            array('title', 'required',
                'message' => 'Введите наименование'),

            array('title', 'length', 'max' => '255',
                'tooLong' => 'Наименование не должно превышать размер в {max} символов'),

            array('note', 'length', 'max' => '255',
                'tooLong' => 'Примечание не должно превышать размер в {max} символов'),

            array('paid_amount', 'compare', 'compareAttribute' => 'amount_fact', 'operator' => '<=',
                'message' => 'Оплаченная сумма не должна превышать факт. сумму'),

            array('date', 'date', 'format' => 'dd.MM.yyyy', 'on' => 'add, edit',
                'message' => 'Неверный формат даты')
        );
    }
}