<?php
class UserImages extends CActiveRecord
{
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'user_images';
    }

    public function rules()
    {
        return array(
            array('img_filename, description', 'filter', 'filter' => 'trim'),
            array('img_filename, description', 'filter', 'filter' => 'htmlspecialchars'),
            array('holiday_id, uid', 'safe'),

            array('img_id, added_date', 'filter', 'filter' => 'intval'),

            array('description', 'length', 'max' => 500,
                'tooLong' => 'Cлишком длинное описание (максимум допустимо {max} символов)')
        );
    }
}