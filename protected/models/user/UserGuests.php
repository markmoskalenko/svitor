<?php
class UserGuests extends CActiveRecord
{
    public static function model($className =__CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
    	return 'user_guests';
    }

    public function relations()
    {
        return array(
            'GuestCategory' => array(self::BELONGS_TO, 'GuestCategories', 'guest_cat_id'),
            'GuestMenuTypes' => array(self::BELONGS_TO, 'GuestMenuTypes', 'menu_type_id'),

            'Seating' => array(self::BELONGS_TO, 'UserGuestSeatingTables', 'table_id')
        );
    }

    public function rules()
    {
        return array(
            array('guest_cat_id', 'required',
                'message' => 'Выберите категорию'),

            array('img_id, guest_cat_id, place, table_id', 'filter', 'filter' => 'intval'),

            array('img_filename, fullname, phone_number', 'filter', 'filter' => 'trim'),
            array('img_filename, fullname, phone_number', 'filter', 'filter' => 'htmlspecialchars'),

            array('fullname', 'required',
                'message' => 'Введите имя'),

            array('gender', 'required',
                'message' => 'Выберите пол'),

            array('gender', 'in', 'range' => array('male', 'female')),

            array('birthday', 'date', 'format' => 'dd.MM.yyyy',
                'message' => 'Некорректный формат даты рождения'),

            array('email', 'email',
                'message' => 'Некорректный E-Mail'),

            array('menu_type_id', 'filter', 'filter' => 'intval')
        );
    }
}