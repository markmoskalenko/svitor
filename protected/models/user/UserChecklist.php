<?php
class UserChecklist extends CActiveRecord
{
    public $time;

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'user_checklist';
    }

    public function relations()
    {
        return array(
            'CatMiniatures' => array(self::BELONGS_TO, 'CategoryMiniatures', 'cat_min_id')
        );
    }

    public function rules()
    {
        return array(
            array('cat_min_id', 'required', 'on' => 'add, edit',
                'message' => 'Выберите категорию'),

            array('cat_min_id, budget_id', 'numerical'),

            array('task, note', 'filter', 'filter' => 'trim', 'on' => 'add, edit'),
            array('task, note', 'filter', 'filter' => 'htmlspecialchars', 'on' => 'add, edit'),

            array('task', 'required', 'on' => 'add, edit',
                'message' => 'Введите описание задачи'),

            array('task', 'length', 'max' => '255', 'on' => 'add, edit',
                'tooLong' => 'Описание задачи не должно превышать размер в {max} символов'),

            array('note', 'length', 'max' => '255', 'on' => 'add, edit',
                'tooLong' => 'Примечание не должно превышать размер в {max} символов'),

            array('date', 'date', 'format' => 'dd.MM.yyyy',
                'message' => 'Неверный формат даты'),

            array('time', 'date', 'format' => 'mm:ss',
                'message' => 'Неверный формат времени'),

            array('link', 'url', 'message' => 'Указанная ссылка не является правильным URL'),
        );
    }

    public static function getTaskDate($task_date)
    {
        $date_string = '--';

        if (!empty($task_date) && (date('H:i', $task_date) != '00:00'))
        {
            $date_string = mApi::getDateWithMonth($task_date, 1).' в '.date('H:i', $task_date);
        }
        else if (!empty($task_date))
        {
            $date_string = mApi::getDateWithMonth($task_date, 1);
        }

        return $date_string;
    }
}
?>