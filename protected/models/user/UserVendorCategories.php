<?php
class UserVendorCategories extends CActiveRecord
{
    public $fun_type;

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'user_vendor_categories';
    }

    public function relations()
    {
        return array(
            'Team' => array(self::HAS_ONE, 'UserVendorTeam', array(
                'user_cat_id',
                'uid' => 'uid',
                'wedding_id' => 'wedding_id',
                'holiday_id' => 'holiday_id'
            ))
        );
    }

    public function rules()
    {
        return array(
            array('title, icon, icon', 'filter', 'filter' => 'trim'),
            array('title, icon, icon', 'filter', 'filter' => 'htmlspecialchars'),
            array('name', 'filter', 'filter' => 'strtolower'),

            array('title', 'required',
                'message' => 'Необходимо ввести наименование')
        );
    }
}