<?php
class UserGifts extends CActiveRecord
{
    public static function model($className =__CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'user_gifts';
    }

    public function rules()
    {
        return array(
            array('img_filename, title', 'filter', 'filter' => 'trim'),
            array('img_filename, title', 'filter', 'filter' => 'htmlspecialchars'),
            array('img_id', 'filter', 'filter' => 'intval'),

            array('title', 'required',
                'message' => 'Введите название'),

            array('title', 'length', 'max' => 100,
                'tooLong' => 'Слишком длинное название')
        );
    }
}