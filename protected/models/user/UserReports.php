<?php
class UserReports extends CActiveRecord
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'user_reports';
    }

    public function relations()
    {
        return array(
            'ReportCategory' => array(self::BELONGS_TO, 'ReportCategories', 'report_cat_id'),
            'User' => array(self::BELONGS_TO, 'User', 'uid')
        );
    }

    public function rules()
    {
        return array(
            array('title, content', 'filter', 'filter' => 'trim'),
            array('title', 'filter', 'filter' => 'htmlspecialchars'),
            array('published_date, created_date, edited_date', 'filter', 'filter' => 'intval'),

            array('title', 'required',
                'message' => 'введите название'),

            array('report_cat_id', 'required',
                'message' => 'выберите раздел'),

            array('content', 'required',
                'message' => 'необходимо заполнить это поле'),

            array('content', 'filter', 'filter' => array('UserReports', 'postStripTags')),

            array('content', 'length', 'min' => 150, 'max' => 10000,
                'tooShort' => 'отчет не может быть таким коротким, постарайтесь описать все более подробно',
                'tooLong' => 'отчет не должен превышать размер в {max} символов'),

            array('title', 'length', 'max' => '200',
                'tooLong' => 'название не должно превышать размер в {max} символов'),

            array('status', 'unsafe'),
            array('status', 'in', 'range' => array('published', 'published_verification', 'verification', 'rejected'))
        );
    }

    public function postStripTags($value)
    {
        return strip_tags($value, '<a><p><span><strong><b><strike><ol><ul><li><img><iframe><table><tbody><th><tr><td><hr>');
    }
}