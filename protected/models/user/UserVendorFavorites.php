<?php
class UserVendorFavorites extends CActiveRecord
{
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function rules()
    {
        return array(
            array('vendor_id', 'required')
        );
    }

    public function tableName()
    {
        return 'user_vendor_favorites';
    }

    public function relations()
    {
        return array(
            'Vendor' => array(self::BELONGS_TO, 'Vendor', 'vendor_id')
        );
    }
}