<?php
class Exhibitions extends CActiveRecord
{
    public $checkedOnFilter = 0;

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'events_exhibitions';
    }

    public function rules()
    {
        return array(
            array('title', 'required', 'message' => 'Введите название'),
            array('country', 'required', 'message' => 'Введите страну'),
            array('date_from', 'required', 'message' => 'Введите дату начала'),
            array('date_to', 'required', 'message' => 'Введите дату окончания'),
            array('addres', 'length', 'max' => 255,
                'tooLong' => '&uarr; поле может содержать максимум {max} символов'),

        );
    }

    /**
     * @param array       $attr
     * @param CDbCriteria $criteria
     *
     * @return array
     */
    public static function getByAttr( $attr = array(), CDbCriteria $criteria = null ){

        $r = array();

        foreach($attr as $key=>$a){

            if(!is_array($a) || !count($a)) continue;

            $criteriaEvents = new CDbCriteria();

            $criteriaEvents->condition = 'attr_id = :attr_id';

            $criteriaEvents->params = array( ':attr_id' => $key );

            $criteriaEvents->addInCondition('attr_val_id', $a);

            $modelEvent = Events::model()->findAll( $criteriaEvents );

            if( !count($modelEvent) ) continue;

            $ids = array();

            foreach( $modelEvent as $event ) $ids[] = $event->event_id;

            unset($modelEvent);

            if( $criteria == null ){
                $criteria = new CDbCriteria();
            }
            $criteria->addInCondition('event_id', $ids);

            $modelExhibitions = self::model()->findAll($criteria);

            if( count($modelExhibitions) ) {
                foreach( $modelExhibitions as $m ) $r[] = $m;
            }

        }
        return $r;
    }
}