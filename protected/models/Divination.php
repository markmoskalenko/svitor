<?php
class Divination extends CActiveRecord
{

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'events_divination';
    }

    public function rules()
    {
        return array(
            array('title', 'required', 'message' => 'Введите название'),
            array('country', 'required', 'message' => 'Введите страну'),

        );
    }

    /**
     * @param array       $attr
     * @param CDbCriteria $criteria
     *
     * @return array
     */
    public static function getByAttr( $attr = array(), CDbCriteria $criteria = null ){

        $r = array();

        foreach($attr as $key=>$a){

            if(!is_array($a) || !count($a)) continue;

            $criteriaEvents = new CDbCriteria();

            $criteriaEvents->condition = 'attr_id = :attr_id';

            $criteriaEvents->params = array( ':attr_id' => $key );

            $criteriaEvents->addInCondition('attr_val_id', $a);

            $modelEvent = Events::model()->findAll( $criteriaEvents );

            if( !count($modelEvent) ) continue;

            $ids = array();

            foreach( $modelEvent as $event ) $ids[] = $event->event_id;

            unset($modelEvent);

            if( $criteria == null ){
                $criteria = new CDbCriteria();
            }
            $criteria->addInCondition('event_id', $ids);

            $modelDivination = self::model()->findAll($criteria);

            if( count($modelDivination) ) {
                foreach( $modelDivination as $m ) $r[] = $m;
            }

        }
        return $r;
    }
}