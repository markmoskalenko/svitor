<?php
class GuestCategories extends CActiveRecord
{
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'guest_categories';
    }

    public function rules()
    {
        return array(
            array('fun_type', 'required', 'message' => 'Необходимо выбрать сайт'),
            array('fun_type', 'in', 'range' => array('wedding', 'holiday', 'multi')),

            array('title', 'filter', 'filter' => 'trim'),
            array('title', 'filter', 'filter' => 'htmlspecialchars'),

            array('title', 'required', 'message' => 'Введите наименование'),

            array('title', 'length', 'min' => 2, 'max' => 255,
                'tooShort' => 'Слишком короткое наименование',
                'tooLong' => 'Слишком длинное наименование (максимум {max} символов)')
        );
    }
}