<?php
class WeddingTypes extends CActiveRecord
{
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'wedding_types';
    }

    public function rules()
    {
        return array(
            array('title', 'filter', 'filter' => 'trim'),
            array('title', 'filter', 'filter' => 'htmlspecialchars'),

            array('title', 'required'),

            array('title', 'length', 'min' => 2, 'max' => 255,
                'tooShort' => '&uarr; слишком короткое наименование',
                'tooLong' => '&uarr; слишком длинное наименование (максимум {max} символов)')
        );
    }

}