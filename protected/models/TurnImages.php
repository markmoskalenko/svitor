<?php

class TurnImages extends CActiveRecord
{
    const STATUS_QUEUE      = 1; // Новые
    const STATUS_LOCKED     = 2; // Заблокировананно
    const STATUS_COMPLETED  = 3;
    const STATUS_ERROR      = 4;

    const MAX_TRIES         = 3;

    const MAX_LOAD          = 30;

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'turn_images';
    }

    public function beforeValidate()
    {
        if( $this->isNewRecord ){

            $this->status = self::STATUS_QUEUE;

            $this->vendor_id = $_SESSION["vendor__id"];

            $this->number_tries = 0;

        }

        return parent::beforeValidate();
    }

    public function rules()
    {
        return array(
            array('number_tries, pos, img_id, vendor_id, url, status', 'required'),
            array('number_tries, locked_time, pos, img_id, vendor_id, status', 'numerical', 'integerOnly' => true),
            array('url', 'length', 'max' => 255),
        );
    }

    public function relations()
    {
        return array();
    }


    public function scopes(){
        return array(
            'queue'=>array(
                'condition' => 'status = '.self::STATUS_QUEUE,
                'limit'     => self::MAX_LOAD,
                'order'     => 'pos, id'
            ),
            'locked'=>array(
                'condition' => 'status = '.self::STATUS_LOCKED.' AND ( locked_time+300 ) <'.time(),
            ),
            'complete'=>array(
                'condition' => 'status = '.self::STATUS_COMPLETED
            ),
            'errors'=>array(
                'condition' => 'status = '.self::STATUS_ERROR
            ),

            'onTheGo' =>array(
                'condition' => 'status = '.self::STATUS_QUEUE.' OR status = '.self::STATUS_LOCKED
            ),

            'processed'=>array(
                'condition' => 'status = '.self::STATUS_ERROR.' OR status = '.self::STATUS_COMPLETED
            ),
            'groupPos'=>array(
                'group' => 'pos'
            )
        );
    }

    public function attributeLabels()
    {
        return array(
            'id'           => 'ID',
            'number_tries' => 'Number Tries',
            'url'          => 'Url',
            'status'       => 'Status',
        );
    }

    public function completed(){

        $this->status       = self::STATUS_COMPLETED;

        $this->locked_time  = time();

        $this->save(false);

    }

    public function lock(){

        $this->status       = self::STATUS_LOCKED;

        $this->locked_time  = time();

        $this->save(false);

    }

    public function unlock(){

        $this->number_tries = $this->number_tries + 1;

        $this->status       = self::STATUS_QUEUE;

        $this->locked_time  = 0;

        $this->save(false);

    }

    public function error(){

        $this->status = self::STATUS_ERROR;

        $this->save(false);

    }

    /**
     * Проверям очередь на предмет завершенности
     *
     * @return bool
     */
    public function check(){

        $r = self::model()->onTheGo()->exists('pos = :pos',array(':pos'=>$this->pos));

        return $r ? false : true;

    }

    /**
     * Удаляем всю очередь
     */
    public function deleteAllPos(){

        self::model()->deleteAll('pos = :pos',array(':pos'=>$this->pos));

    }

    public static function getPosition(){
        $maxOrderNumber = Yii::app()->db->createCommand()
              ->select('max(pos) as max')
              ->from('turn_images')
              ->queryScalar();
        return $maxOrderNumber ? $maxOrderNumber : 0;
    }

    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id, true);
        $criteria->compare('number_tries', $this->number_tries);
        $criteria->compare('url', $this->url, true);
        $criteria->compare('status', $this->status);

        return new CActiveDataProvider($this, array(
                                                   'criteria' => $criteria,
                                              ));
    }
}