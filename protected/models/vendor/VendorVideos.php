<?php
class VendorVideos extends CActiveRecord
{
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'vendor_videos';
    }

    public function rules()
    {
        return array(
            array('title, description, video_id', 'filter', 'filter' => 'trim'),
            array('title, description', 'filter', 'filter' => 'htmlspecialchars'),

            array('thumbnail_url', 'url'),

            array('vendor_id, added_date', 'filter', 'filter' => 'intval'),

            array('video_provider', 'in', 'range' => array('youtube', 'vimeo')),

            array('title', 'required', 'message' => 'Введите название', 'on' => 'add, edit'),

            array('title', 'length', 'min' => 2, 'max' => 250,
                'tooShort' => 'Слишком короткое название (минимум {min} символов)',
                'tooLong' => 'Слишком длинное название (максимум 200 символов)', 'on' => 'add, edit'),

            array('description', 'length', 'max' => 3000,
                'tooLong' => 'Слишком длинное описание (максимум допустимо {max} символов)', 'on' => 'add, edit')
        );
    }
}