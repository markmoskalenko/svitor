<?php
class VendorCategory extends CActiveRecord
{
    public $fun_type;

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'vendor_category';
    }

    public function relations()
    {
        return array(
            'Category' => array(self::BELONGS_TO, 'Category', 'cat_id'),
            'Vendor'   => array(self::BELONGS_TO, 'Vendor', 'vendor_id'),
        );
    }

    public function rules()
    {
        return array(
            array('cat_id', 'required',
                'message' => '&uarr; выберите раздел'),

            array('cat_id', 'uniqueCategory',
                'message' => '&uarr; у вас уже добавлен этот раздел'),

            array('vendor_id', 'required'),
            array('vendor_id, cat_id', 'numerical'),
        );
    }

    public static function getCurrVendorCategory(){
        $criteria            = new CDbCriteria;
        $criteria->condition = 'vendor_id = :vendor_id';
        $criteria->select    = 'cat_id';
        $criteria->params    = array(':vendor_id' => Vendor::getId());
        $categories          = VendorCategory::model()->findAll($criteria);
        $result              = array();
        if (count($categories)) {
            foreach ($categories as $category) {
                $result[] = $category->cat_id;
            }
        }

        return $result;
    }

    public function uniqueCategory ($attribute, $params)
    {
        if(!$this->hasErrors())
        {
            $params['criteria']= array(
                'condition' => 'vendor_id = :vendor_id',
                'params' => array(':vendor_id' => $this->vendor_id)
            );

            $validator = CValidator::createValidator('unique', $this, $attribute, $params);
            $validator->validate($this, $attribute);
        }
    }
}
?>