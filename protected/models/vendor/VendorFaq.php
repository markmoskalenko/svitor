<?php
class VendorFaq extends CActiveRecord
{
    public static function model ($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function rules()
    {
        return array(
            array('question, response', 'required', 'on' => 'add, edit',
                'message' => '&uarr; это поле необходимо заполнить'),

            array('question, response', 'filter', 'filter' => 'htmlspecialchars', 'on' => 'add, edit'),

            array('question', 'length', 'max' => 200, 'on' => 'add, edit',
                'tooLong' => '&uarr; максимум допустимо {max} символов'),

            array('response', 'length', 'max' => 500, 'on' => 'add, edit',
                'tooLong' => '&uarr; максимум допустимо {max} символов'),
        );
    }

    public function tableName ()
    {
        return 'vendor_faq';
    }
}
?>