<?php
class Vendor extends CActiveRecord
{
    // Registration
    public $confirm_password;
    public $captcha_code;
    public $accept_terms;

    // Change Password
    public $old_password;
    public $new_password;
    public $confirm_new_password;

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'vendors';
    }

    public function relations()
    {
        return array(
            'Reviews' => array(self::HAS_MANY, 'VendorReviews', 'vendor_id', 'with' => 'User'),
            'ReviewsCount' => array(self::STAT, 'VendorReviews', 'vendor_id'),
            'Categories' => array(self::HAS_MANY, 'VendorCategory', 'vendor_id', 'with' => 'Category'),
            'Videos' => array(self::HAS_MANY, 'VendorVideos', 'vendor_id'),
            'Images' => array(self::HAS_MANY, 'VendorImages', 'vendor_id'),
            'Faq' => array(self::HAS_MANY, 'VendorFaq', 'vendor_id'),
            'Address' => array(
                self::HAS_MANY,
                'VendorAddresses',
                'vendor_id',
                'with' => array('Country', 'Region', 'City', 'Metro')
            ),
            'Certificates' => array(self::HAS_MANY, 'VendorCertificates', 'vendor_id'),
            'Stock'        => array(self::HAS_MANY, 'VendorStock', 'vendor_id'),
            'PhotoInGalleryCount' => array(self::STAT, 'Gallery', 'vendor_id')
        );
    }

    public function rules()
    {
        return array(
            array('last_ip', 'default', 'value' => Yii::app()->request->userHostAddress),
            array('user_agent', 'default', 'value' => Yii::app()->request->userAgent),

            array('title', 'length', 'min' => 3, 'max' => 100,
                'tooShort' => '&uarr; слишком короткое название (минимум {min} символа)',
                'tooLong' => '&uarr; слишком длинное название (максимум {max} символов)'),

            array('terms_delivery', 'filter', 'filter' => 'trim'),
            array('terms_delivery', 'filter', 'filter' => 'htmlspecialchars'),

            // ONLY FOR EDIT INFORMATION
            array('img_filename, title, description, contact_phone, contact_phone2, contact_phone3, contact_site, contact_skype, contact_twitter, contact_vk, contact_fb',
                'filter', 'filter' => 'trim', 'on' => 'edit'),

            array('img_filename, title, description, contact_phone, contact_phone2, contact_phone3, contact_site, contact_skype, contact_twitter, contact_vk, contact_fb',
                'filter', 'filter' => 'htmlspecialchars', 'on' => 'edit'),

            array('img_id', 'filter', 'filter' => 'intval', 'on' => 'edit'),

            array('title, description, contact_phone', 'required',
                'message' => '&uarr; необходимо заполнить это поле', 'on' => 'edit'),

            array('description', 'length', 'min' => 6, 'max' => 3000,
                'tooShort' => '&uarr; слишком короткое описание (минимум {min} символов)',
                'tooLong' => '&uarr; слишком длинное описание (максимум {max} символов)', 'on' => 'edit'),

            array('terms_delivery', 'length', 'max' => 3000,
                'tooLong' => '&uarr; слишком длинное описание (максимум {max} символов)'),

            array('contact_skype, contact_twitter, contact_vk, contact_fb', 'length', 'max' => 60,
                'tooLong' => '&uarr; поле может содержать максимум {max} символов', 'on' => 'edit'),

            array('contact_site', 'url',
                'message' => '&uarr; указанный адрес не является правильным URL (пример: http://example.com)', 'on' => 'edit'),

            // ONLY FOR CHANGE PASSWORD
            array('old_password, new_password, confirm_new_password', 'required',
                'message' => '&uarr; заполните это поле', 'on' => 'change_password'),

            array('old_password', 'oldPassword', 'on' => 'change_password'),

            array('new_password', 'length', 'min' => 6, 'max' => 70,
                'tooShort' => '&uarr; слишком короткий пароль (минимум {min} символов)',
                'tooLong' => '&uarr; слишком длинный пароль (максимум {max} символов)', 'on' => 'change_password, admin_change_password'),

            array('new_password', 'compare', 'compareAttribute' => 'old_password', 'strict' => true, 'operator' => '!=',
                'message' => '&uarr; новый пароль должен отличаться от старого', 'on' => 'change_password, admin_change_password'),

            array('confirm_new_password', 'compare', 'compareAttribute' => 'new_password', 'strict' => true,
                'message' => '&uarr; пароли не совпадают', 'on' => 'change_password, admin_change_password'),

            // ONLY FOR REGISTRATION
            array('email, title, password, confirm_password', 'required',
                'message' => '&uarr; заполните это поле', 'on' => 'registration'),

            array('email', 'email', 'message' => '&uarr; некорректный E-Mail', 'on' => 'registration'),
            array('email', 'unique', 'message' => '&uarr; такой E-Mail уже кем-то используется', 'on' => 'registration'),

            array('accept_terms', 'in', 'range' => array(1),
                'message' => '&uarr; необходимо ваше согласие', 'on' => 'registration'),

            /*array('captcha_code', 'captcha', 'captchaAction' => 'captcha', 'caseSensitive' => true,
                'message' => '&uarr; не правильно указан код', 'on' => 'registration'),*/

            array('password', 'length', 'min' => 6, 'max' => 70,
                'tooShort' => '&uarr; слишком короткий пароль (минимум {min} символов)',
                'tooLong' => '&uarr; слишком длинный пароль (максимум {max} символов)', 'on' => 'registration'),

            array('confirm_password', 'compare', 'compareAttribute' => 'password', 'strict' => true,
                'message' => '&uarr; пароли не совпадают', 'on' => 'registration'),

            array('reg_date', 'default', 'value' => time(), 'on' => 'registration'),

            array('salt', 'unsafe'),
            array('notify_site_news, notify_new_reviews, notify_new_msg', 'filter', 'filter' => 'intval'),
            array('notify_site_news, notify_new_reviews, notify_new_msg', 'in', 'range' => array(0,1)),

            array('is_banned', 'in', 'range' => array(0,1))
        );
    }

    public function oldPassword($attribute, $params)
    {
        $old_password = hash('md5', $this->salt.strtoupper($this->$attribute));
        if ($this->password != $old_password)
            $this->addError($attribute, '&uarr; текущий пароль введен не верно');
    }

    public function beforeSave()
    {
        if ($this->getScenario() == 'registration' || $this->getScenario() == 'change_password' || $this->getScenario() == 'admin_change_password')
        {
            $this->salt = $this->generateSalt();
            $this->password = hash('md5', $this->salt.strtoupper($this->password));
        }
        return true;
    }

    public function generateSalt($max = 30)
    {
        $salt = '';
        $chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%&*?';
        for ($i = 0; $i < $max; $i++)
            $salt .= $chars[mt_rand(0, strlen($chars) - 1)];

        return $salt;
    }

    static public function updateLastActivity()
    {
        if (self::isGuest())
            return false;

        $vendor = self::model()->findByPk(self::getId());
        $vendor->last_ip = Yii::app()->request->getUserHostAddress();
        $vendor->last_activity = time();

        if ($vendor->update()) {
            // Обновим состояние
            Yii::app()->user->setState('last_activity', $vendor->last_activity);

            return true;
        } else {
            return false;
        }
    }

    public static function getLastActivity()
    {
        return Yii::app()->user->getState('last_activity', false);
    }

    public static function getInfo()
    {
        return self::model()->findByPk(self::getId());
    }

    public static function getId()
    {
        return Yii::app()->user->getId();
    }

    public static function isGuest()
    {
        return Yii::app()->user->isGuest;
    }

    /************
     *  Функция сохраяющая атрибуты (характеристики) компании.
     *  Принимаемые параметры:
     *
     *  @array $data - содержит информацию о выбранных пользователем значениях для атрибутов
     *
     ************/
    public static  function saveFeaturesForCategory($cat_id = 0, $data = null, $scenario = 'add')
    {
        if (empty($cat_id))
            return false;

        $vendor_id = self::getId();

        if ($scenario == 'edit')
            $featureValues = VendorAttrValues::model()->findAllByAttributes(array('cat_id' => $cat_id, 'vendor_id' => $vendor_id));

        // Удаляем старые атрибуты, если редактируется раздел
        if (!empty($featureValues))
            VendorAttrValues::model()->deleteAllByAttributes(array('cat_id' => $cat_id, 'vendor_id' => $vendor_id));

        if (empty($data))
            return false;

        // Пишем новые атрибуты
        foreach ($data as $featureId => $featureValue)
        {
            // Несколько значений у одного атрибута
            if (is_array($featureValue))
            {
                foreach ($featureValue as $value)
                {
                    if ($value == '')
                        continue;

                    $newProductFeature = new VendorAttrValues;
                    $newProductFeature['vendor_id'] = $vendor_id;
                    $newProductFeature['cat_id'] = $cat_id;
                    $newProductFeature['attr_id'] = intval($featureId);
                    $newProductFeature['attr_value_id'] = intval($value);
                    $newProductFeature->save();
                }
            }
            // Одно значение у одного атрибута
            else
            {
                if ($featureValue == '')
                    continue;

                $newProductFeature = new VendorAttrValues;
                $newProductFeature['vendor_id'] = $vendor_id;
                $newProductFeature['cat_id'] = $cat_id;
                $newProductFeature['attr_id'] = intval($featureId);
                $newProductFeature['attr_value_id'] = intval($featureValue);
                $newProductFeature->save();
            }
        }

        return true;
    }
}