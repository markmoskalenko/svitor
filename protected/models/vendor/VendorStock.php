<?php
class VendorStock extends CActiveRecord
{
    public static function model ($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function relations()
    {
        return array(
            'Vendor' => array(self::BELONGS_TO, 'Vendor', 'vendor_id')
        );
    }

    public function rules()
    {
        return array(
            array('title, description, date_to, date_from', 'filter', 'filter' => 'trim'),
            array('title, description', 'filter', 'filter' => 'htmlspecialchars'),

            array('title, description, date_to, date_from', 'required',
                'message' => '&uarr; заполните это поле'),

            array('title', 'length', 'min' => 5, 'max' => 200,
                'tooShort' => '&uarr; слишком короткое название',
                'tooLong' => '&uarr; слишком длинное название (максимум {max} символов)'),

        );
    }

    /**
     * Выборка Акций по категориям
     *
     * @param array $ids - Массив id категорий
     * @param string $condition - Дополнительное условие выборки
     *
     * @return array|bool
     */
    public static function getByCategories( $ids = array(), CDbCriteria $criteria = null, $country = null, $city = null, $region = null ){

        $ids = mApi::intArray($ids);

        if( !is_array($ids) || !count($ids)) return false;

        $criteriaCat = new CDbCriteria();

        $criteriaCat->select = 'vendor_id';

        $criteriaCat->addInCondition('cat_id', $ids);

        $modelCategory = VendorCategory::model()->findAll($criteriaCat);

        if( !count($modelCategory) ) return false;

        $vendorIDS = array();

        foreach( $modelCategory as $category ){
            $criteriaAddress = new CDbCriteria();
            $criteriaAddress -> condition = 'vendor_id = :vendor_id';
            $criteriaAddress -> params = array(':vendor_id' => $category->vendor_id);
            if( $country != null ) {
                $criteriaAddress->addCondition('country_id = :country');
                $criteriaAddress->params[':country'] = (int)$country;
            }
            if( $city != null ) {
                $criteriaAddress->addCondition('city_id = :city');
                $criteriaAddress->params[':city'] = (int)$city;
            }
            if( $region != null ) {
                $criteriaAddress->addCondition('region_id = :region');
                $criteriaAddress->params[':region'] = (int)$region;
            }
            if(VendorAddresses::model()->exists($criteriaAddress))
                $vendorIDS[] = $category->vendor_id;
        }

        unset($modelCategory);

        if( $criteria == null ){
            $criteria = new CDbCriteria();
        }

        $criteria->addInCondition('t.vendor_id', $vendorIDS);

        $modelStock = self::model()->findAll($criteria);

        return count( $modelStock ) ? $modelStock : array();

    }

    public function tableName()
    {
        return 'vendor_stock';
    }
}