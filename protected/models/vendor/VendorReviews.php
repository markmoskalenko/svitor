<?php
class VendorReviews extends CActiveRecord
{
    public static function model ($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function tableName ()
    {
        return 'vendor_reviews';
    }

    public function relations()
    {
        return array(
            'User' => array(self::BELONGS_TO, 'User', 'uid')
        );
    }

    public function rules()
    {
        return array(
            array('comment', 'filter', 'filter' => 'trim'),
            array('comment', 'filter', 'filter' => 'htmlspecialchars'),

            array('quality', 'required',
                'message' => 'Оцените качество услуг этой компании'),

            array('comment', 'required',
                'message' => 'Пожалуйста, прокомментируйте свою оценку'),

            array('comment', 'length', 'min' => 10, 'max' => 500,
                'tooShort' => 'Пожалуйста, прокомментируйте свою оценку',
                'tooLong' => 'Слишком длинный комментарий'),

            array('quality', 'in', 'range' => array(1,2,3,4,5),
                'message' => 'Компания оценивается по пятибалльной шкале')
        );
    }
}