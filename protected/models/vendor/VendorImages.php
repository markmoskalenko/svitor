<?php
class VendorImages extends CActiveRecord
{
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'vendor_images';
    }

    public function relations()
    {
        return array(
            'Vendor' => array(self::BELONGS_TO, 'Vendor', 'vendor_id'),
            'FileImage' => array(self::BELONGS_TO, 'FileImages', 'img_id')
        );
    }

    public function rules()
    {
        return array(
            array('img_filename, description', 'filter', 'filter' => 'trim'),
            array('img_filename, description', 'filter', 'filter' => 'htmlspecialchars'),

            array('vendor_id, img_id, added_date', 'filter', 'filter' => 'intval'),

            array('description', 'length', 'max' => 500,
                'tooLong' => 'Слишком длинное описание (максимум допустимо {max} символов)'),

            array('status_gallery', 'in', 'range' => array('published', 'verification', 'rejected')),

            // Unsafe attributes
            array('status_gallery', 'unsafe')
        );
    }
}