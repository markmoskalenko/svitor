<?php
class VendorDetails extends CActiveRecord
{
    public static function model ($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function rules()
    {
        $all_message = '&uarr; значение должно содержать {min} цифр';

        return array(
            array('legal_name, legal_address, head_fullname, head_post, accountant_fullname, inn,
                   kpp, bank_name, current_account, correspondent_account, bic, taxation_system', 'filter', 'filter' => 'trim'),

            array('legal_name, legal_address, head_fullname, head_post, accountant_fullname, inn,
                   kpp, bank_name, current_account, correspondent_account, bic, taxation_system', 'filter', 'filter' => 'htmlspecialchars'),

            array('legal_name, legal_address, inn, bank_name, current_account, correspondent_account, bic', 'required',
                'message' => '&uarr; это поле необходимо заполнить'),

            array('inn, kpp, current_account, correspondent_account, bic', 'numerical',
                'message' => '&uarr; значение должно состоять из цифр'),

            array('inn', 'length', 'min' => 10, 'max' => 12,
                'tooShort' => '&uarr; значение должно содержать минимум {min} цифр',
                'tooLong' => '&uarr; значение должно содержать минимум {max} цифр'),

            array('kpp', 'length', 'min' => 9, 'max' => 9,
                'tooShort' => $all_message,
                'tooLong' => $all_message),

            array('current_account, correspondent_account', 'length', 'min' => 20, 'max' => 20,
                'tooShort' => $all_message,
                'tooLong' => $all_message),

            array('bic', 'length', 'min' => 9, 'max' => 9,
                'tooShort' => $all_message,
                'tooLong' => $all_message)
        );
    }

    public function tableName ()
    {
        return 'vendor_details';
    }
}
?>