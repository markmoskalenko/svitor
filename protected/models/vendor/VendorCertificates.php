<?php
class VendorCertificates extends CActiveRecord
{
    public static function model ($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function tableName ()
    {
        return 'vendor_certificates';
    }

    public function rules()
    {
        return array(
            array('img_filename, description', 'filter', 'filter' => 'htmlspecialchars'),
            array('img_id', 'filter', 'filter' => 'intval'),

            array('img_id, img_filename', 'required',
                'message' => '&uarr; загрузите изображение'),

            array('description', 'required',
                'message' => '&uarr; это поле необходимо заполнить'),
        );
    }
}
?>