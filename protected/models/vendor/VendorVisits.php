<?php
class VendorVisits extends CActiveRecord
{
    public static function model ($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function rules()
    {
        return array(
            array('vendor_id, date, ip_address', 'filter', 'filter' => 'intval')
        );
    }

    public function tableName()
    {
        return 'vendor_visits';
    }
}