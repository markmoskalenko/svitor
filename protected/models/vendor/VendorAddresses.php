<?php
class VendorAddresses extends CActiveRecord
{
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'vendor_addresses';
    }

    public function relations()
    {
        return array(
            'Country' => array(self::BELONGS_TO, 'Countries', 'country_id'),
            'Region' => array(self::BELONGS_TO, 'Regions', 'region_id'),
            'City' => array(self::BELONGS_TO, 'Cities', 'city_id'),
            'Metro' => array(self::BELONGS_TO, 'MetroStantions', 'metro_id'),
        );
    }

    public function rules()
    {
        return array(
            array('country_id, region_id, city_id, metro_id', 'filter', 'filter' => 'intval'),

            array('address, description, work_time, phone', 'filter', 'filter' => 'trim', 'on' => 'add, edit'),
            array('address, description, work_time, phone', 'filter', 'filter' => 'htmlspecialchars', 'on' => 'add, edit'),

            array('country_id', 'required', 'message' => '&uarr; выберите страну', 'on' => 'add, edit'),

            array('address, description, work_time, phone', 'length', 'max' => 255,
                'tooLong' => '&uarr; поле может содержать максимум {max} символов', 'on' => 'add, edit'),
        );
    }
}