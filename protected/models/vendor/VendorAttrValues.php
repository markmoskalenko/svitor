<?php
class VendorAttrValues extends CActiveRecord
{
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'vendor_attribute_values';
    }

    public function relations()
    {
        return array(
            'Values' => array(self::BELONGS_TO, 'AttrValues', 'attr_value_id')
        );
    }

    public function rules()
    {
        return array(
            array('vendor_id, cat_id, attr_id, attr_value_id', 'required'),
            array('vendor_id, cat_id, attr_id, attr_value_id', 'numerical'),
        );
    }
}