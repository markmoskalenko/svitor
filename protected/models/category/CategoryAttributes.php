<?php
class CategoryAttributes extends CActiveRecord
{
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    public function tableName()
    {
        return 'category_attributes';
    }

    public function relations()
    {
        return array(
            'Attributes' => array(self::BELONGS_TO, 'Attribute', 'attr_id')
        );
    }

    public function rules()
    {
        return array(
            array('cat_id, attr_id', 'filter', 'filter' => 'intval'),
            array('cat_id, attr_id', 'required'),

            array('attr_id', 'exist', 'attributeName' => 'id', 'className' => 'Attribute')
        );
    }
}
?>