<?php
class CategoryMiniatures extends CActiveRecord
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'category_miniatures';
    }

    public function relations()
    {
        return array(
            'UserChecklist' => array(self::HAS_MANY, 'UserChecklist', 'cat_min_id'),
            'UserBudget' => array(self::HAS_MANY, 'UserBudget', 'cat_min_id'),
            'ChecklistTemplates' => array(self::HAS_MANY, 'ChecklistTemplate', 'cat_min_id')
        );
    }

    public function rules()
    {
        return array(
            array('title, icon', 'filter', 'filter' => 'trim'),
            array('title, icon', 'filter', 'filter' => 'htmlspecialchars'),
            array('name', 'filter', 'filter' => 'strtolower'),

            array('title', 'required',
                'message' => 'Необходимо ввести наименование'),

            array('icon', 'required',
                'message' => 'Необходимо ввести имя иконки'),

            array('fun_type', 'required', 'message' => 'Необходимо выбрать сайт'),

            array('cat_link', 'url', 'message' => 'Указанная вами ссылка не является правильным URL'),

            array('show_in_checklist, show_in_budget, show_in_vendors', 'numerical'),

            array('fun_type', 'in', 'range' => array('wedding', 'holiday', 'multi'))
        );
    }
}