<?php
class Category extends CActiveRecord
{
    public $checkedOnFilter = false;

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'category';
    }

    public function defaultScope()
    {
        return array(
            'order' => "weight",
        );
    }

    public function relations()
    {
        return array(
            'Attrs'           => array(
                self::MANY_MANY,
                'Attribute',
                'category_attributes(cat_id, attr_id)',
                'order' => 'Attrs.type DESC, Attrs_Attrs.sort ASC',
                'with'  => 'Values'
            ),
            'AttrsForProduct' => array(
                self::MANY_MANY,
                'Attribute',
                'category_attributes(cat_id, attr_id)',
                'condition' => 'AttrsForProduct.belongs_to = "products"',
                'order'     => 'AttrsForProduct.type DESC, AttrsForProduct_AttrsForProduct.sort ASC',
                'with'      => 'Values'
            ),
            'AttrsForVendor'  => array(
                self::MANY_MANY,
                'Attribute',
                'category_attributes(cat_id, attr_id)',
                'condition' => 'AttrsForVendor.belongs_to = "vendors"',
                'order'     => 'AttrsForVendor.type DESC, AttrsForVendor_AttrsForVendor.sort ASC',
                'with'      => 'Values'
            )
        );
    }

    public function behaviors()
    {
        return array(
            'NestedSetBehavior' => array(
                'class'          => 'ext.behaviors.trees.NestedSetBehavior',
                'leftAttribute'  => 'lft',
                'rightAttribute' => 'rgt',
                'levelAttribute' => 'level',
                'hasManyRoots'   => true,
            ),
        );
    }

    public function rules()
    {
        return array(
            array('title', 'filter', 'filter' => 'htmlspecialchars', 'on' => 'edit, add'),
            array('title', 'filter', 'filter' => 'trim', 'on' => 'edit, add'),
            array(
                'title',
                'required',
                'message' => 'Введите название',
                'on'      => 'edit, add'
            ),
            array(
                'title',
                'unique',
                'message' => 'Наименование должно быть уникальным'
            ),
            array(
                'title, title_catalog, title_product, keywords_catalog, keywords_product',
                'length',
                'min'      => 3,
                'max'      => 255,
                'tooLong'  => 'Слишком длинное название (допустимо {max} символов)',
                'tooShort' => 'Слишком короткое название (необходимо минимум {min} символа)',
                'on'       => 'edit, add'
            ),
            array(
                'title_catalog, title_product, keywords_catalog, keywords_product',
                'length',
                'max'     => 255,
                'tooLong' => 'Слишком длинное название (допустимо {max} символов)'
            ),
            array(
                'description_catalog, description_product, text_header_catalog, text_footer_catalog, text_header_product, text_footer_product',
                'length',
                'max' => 5000
            ),
            array('visible, weight, visible_vndr, visible_prod', 'numerical', 'on' => 'edit, add'),
            array('icon, name', 'safe', 'on' => 'edit, add')
        );
    }


    /**
     * Массив значений для списка
     * @return array
     */
//    public static function getDataDropDown(){
//        $model  = self::findAll();
//        $result = array();
//        foreach ($model as $item) {
//            $result[$item->id] = $item->title;
//        }
//
//        return $result;
//    }

    /**
     * Сохраняем позиции категорий
     *
     * @param $data
     *
     * @return bool
     */
    public function saveSort($data)
    {

        if (!is_array($data) || !count($data)) {
            return false;
        }

        $data = mApi::intArray($data);

        $weight = 1;

        foreach ($data as $id) {

            $model = self::model()->find('id = :id', array(':id' => $id));

            if (!$model) {
                continue;
            }

            $model->weight = $weight++;

            $model->saveNode();

        }

        return true;

    }

    public function attributeLabels()
    {
        return array(
            'title'               => 'Наименование',
            'visible'             => 'Показывать в каталоге',
            'visible_prod'        => 'Показывать в товарах',
            'visible_vndr'        => 'Показывать в компаниях',
            'description_catalog' => 'Описание (Компании)',
            'description_product' => 'Описание (Товары)',
            'text_header_catalog' => 'Текст 1 (Компании)',
            'text_footer_catalog' => 'Текст 2 (Компании)',
            'text_header_product' => 'Текст 1 (Товары)',
            'text_footer_product' => 'Текст 2 (Товары)',
            'title_catalog'       => 'Заголовок (Компании)',
            'title_product'       => 'Заголовок (Товары)',
            'keywords_catalog'    => 'Ключевые слова (Компании)',
            'keywords_product'    => 'Ключевые слова (Товары)',
        );
    }
}