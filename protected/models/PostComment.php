<?php
class PostComment extends CActiveRecord
{
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'post_comments';
    }

    public function relations()
    {
        return array(
            'Post' => array(self::BELONGS_TO, 'Post', 'post_id'),
            'User' => array(self::BELONGS_TO, 'User', 'uid')
        );
    }

    public function rules()
    {
        return array(
            array('comment', 'filter', 'filter' => 'trim'),
            array('comment', 'filter', 'filter' => 'htmlspecialchars'),

            array('comment', 'required',
                'message' => '&darr; комментарий не может быть пустым'),

            array('comment', 'length', 'min' => 5, 'max' => 1000,
                'tooShort' => '&darr; комментарий должен содердать как минимум пару слов',
                'tooLong' => '&darr; комментарий привышает максимальную длинну ({max} символов)'),
        );
    }
}