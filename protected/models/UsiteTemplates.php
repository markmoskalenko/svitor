<?php
class UsiteTemplates extends CActiveRecord
{
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'usite_templates';
    }

    public function relations()
    {
        return array(
            'Styles' => array(self::HAS_MANY, 'UsiteTemplateStyles', 'template_id')
        );
    }
}