<?php
/**
 * Class Product_xml
 *
 * @Author Mark Moskalenko
 * @Version 1.0
 *
 */

class Product_xml extends CFormModel  {

    const AVAILABLE_DELETE_PRODUCT = 3;

    public $file;

    private $path;

    private $data;

    private $fileContent = NULL;

    private $otherCategories;

    private $turnPosition;

    private $result = array(
          'count'   => 0,
          'error'   => array(
              'count'       => 0,
              'model'       => array()
          ),
          'success' => 0
    );


    public function init(){
        $this->path = DOCUMENT_ROOT.DIRECTORY_SEPARATOR.'uploads'.DIRECTORY_SEPARATOR;

        $this->turnPosition = time().rand(10, 90);
    }

    /**
     * Стартовая функция загрузки данных из XML файла
     */
    public function loadData(){

        if( !$this->fileContent = $this->openFile() ) return false;

        $aXml = $this->parseProduct();

        $c = count($aXml);

        if( is_array($aXml) && $c ){

            $this->result['count'] = $c;

            foreach( $aXml as $item ){

                $this->data = $this->normalizeData( $item );

                if( $this->data['in_stock'] == self::AVAILABLE_DELETE_PRODUCT ){

                    $this->deleteProduct($this->data['external_id']);

                    $this->result['success'] += 1;

                    continue;

                }

                $exists = Product::model()->exists('external_id = :external_id AND vendor_id = :vendor_id', array(
                    'external_id' => $this->data['external_id'],
                    'vendor_id'   => Vendor::getId()
                ));

                if( $exists ){
                    $model = Product::model()->findByAttributes(array('external_id' => $this->data['external_id'], 'vendor_id' => Vendor::getId()));
                    $model->scenario = 'editXML';
                    $model->restore();
                }else {
                    $model = new Product('addXML');
                    $model['vendor_id']  = Vendor::getId();
                    $model['added_date'] = time();
                    $model['status']     = 1;
                }

                $params = $this->data['param'];

                unset($this->data['param']);

                $model->attributes = $this->data;

                //TODO: Заглушка на время.
                if( $model->currencyId )
                    $model->currencyId = 0;

                if( $model->validate() && $model->save() ) {

                    $this->result['success'] += 1;

                    $params = $this->normalizeParams( $params, $this->data['cat_id'] );

                    if( $params ) $model->saveFeatures($params);

                    $this->attacheImage( $this->data['picture'], $model->id );

                }else $this->error( $model );
            }
        }
        return $this->result;
    }

    private function deleteProduct( $id ){
        // Check access
        $product = Product::model()->findByAttributes(array('external_id' => $id, 'vendor_id' => Vendor::getId()));

        if (!empty($product))
        {
            $product->delete();
        }
    }

    /**
     * Счетчик ошибок (товаров)
     * @param $data
     */
    private function error( $model ){

       $this->result['error']['count']  += 1;

       $this->result['error']['model'][] = $model;

    }

    /**
     * Открываем файл для парсинга
     * @return bool
     */
    private function openFile(){

        if( !file_exists($this->path.$this->file) ) return false;

        $fileContent = file_get_contents( $this->path.$this->file );

        if( empty( $fileContent ) ) return false;

        unlink( $this->path.$this->file );

        return $fileContent;
    }

    /**
     * Привязка изображения к товару
     * Поддерживает мультизагрузку не более 5
     *
     * @param $imageUrl - array count max 5
     * @param $productId
     */
    private function attacheImage( $imageUrl, $productId ){

        if(!is_array($imageUrl) || !count($imageUrl)){
            ProductImages::model()->deleteAll('product_id = :product_id AND vendor_id = :vendor_id', array(
                                                                                                        'product_id' =>  $productId,
                                                                                                        'vendor_id'  =>  Vendor::getId(),
                                                                                                   ));
            return false;
        }

        $images = ProductImages::model()->findAll('product_id = :product_id AND vendor_id = :vendor_id', array(
                                                                                                      'product_id' =>  $productId,
                                                                                                      'vendor_id'  =>  Vendor::getId(),
                                                                                                 ));
        if(count($imageUrl)>5){
            $imageUrl = array_slice($imageUrl, 0, 5);
        }

        if(count( $images )){

            //Пройдемся по текущим img
            foreach( $images as $image ){

                $exist = false;

                //Если не закончились загружаемые картинки
                if( count($imageUrl) ){

                    foreach( $imageUrl as $key=>$iUrl ){

                        if( $image->source_url == $iUrl ){
                            $exist = true;
                            //Не надо ее грузить (она уже есть в бд)
                            unset($imageUrl[$key]);
                        }
                    }
                }
                if(!$exist){
                    $image->delete();
                }
            }
        }

        if( count($imageUrl) ){
            foreach($imageUrl as $url){

                $model                 = new ProductImages('add');
                $model['product_id']   = $productId;
                $model['vendor_id']    = Vendor::getId();
                $model['img_id']       = 0;
                $model['img_filename'] = 'loading';
                $model['added_date']   = time();
                $model['source_url']   = $url;

                $model->save();

                //НОВАЯ ОЧЕРЕДЬ
                $turn = new TurnImages();

                $turn->url = $url;

                $turn->img_id = $model->id;

                $turn->pos = $this->turnPosition;

                $turn->save();
            }
        }
    }

    /**
     * Приводит атрибуты товара к правильному формату ID_ATTR : ID_VAL
     * @param      $params
     * @param null $catId
     *
     * @return array|bool
     */
    private function normalizeParams( $params, $catId = null ){

        if( !is_array( $params ) || !count( $params ) || $catId == null) return false;

        $catId    = (int)$catId;
        $category = Category::model()->findByPk( $catId );

        if ( count($category) == 0 ){ return false; }

        $category_attrs = $category->AttrsForProduct;

        if( empty( $category_attrs ) ){ return false; }

        $result = array();

        foreach( $params as $param ){
            foreach($category_attrs as $attrs){
                if( $attrs->title == $param['param']['name'] ){
                    switch( $attrs->type ){
                        case 'string'  : $column = 'value_string';  break;
                        case 'integer' : $column = 'value_integer'; break;
                        case 'boolean' : $column = 'value_boolean'; break;
                    }
                    foreach($attrs->Values as $value){
                        switch( $param['value'] ){
                            case 'Да'  : $pv = 1;  break;
                            case 'Нет' : $pv = 0; break;
                            default : $pv = $param['value'];
                        }
                        if( $value->{$column} == $pv ){

                            if($attrs->is_multi){
                                $result[$attrs->id][] = $value->id;
                            }else $result[$attrs->id] = $value->id;
                            break;
                        }

                    }
                }
            }
        }
        return $result;
    }

    /**
     * Нормализация данных после парсинга xml
     *
     * @param $aXml
     *
     * @return array|bool
     */
    private function normalizeData( $aXml ){
        if( !is_array( $aXml ) || !count( $aXml ) ) return false;

        $data = array(
            'title'         => isset($aXml['data']['name'])        ? $aXml['data']['name']          : '',  // Заголовок
            'cat_id'        => isset($aXml['data']['categoryId'])  ? $aXml['data']['categoryId']    : '',  // ID категории товара
            'visible'       => 1,                                                                          // видимость
            'price'         => isset($aXml['data']['price'])       ? $aXml['data']['price']         : '',  // цена
            'description'   => isset($aXml['data']['description']) ? $aXml['data']['description']   : '',  // описание
            'in_stock'      => isset($aXml['attr']['available'])   ? $aXml['attr']['available']     : (isset($aXml['available']) ? $aXml['available'] : ''),  // true (В наличии), false (Нет в наличии), order (Под заказ)
            'currencyId'    => isset($aXml['data']['currencyId'])  ? $aXml['data']['currencyId']    : '',  // TODO: Игнорируем (Пока)
            'picture'       => isset($aXml['data']['picture'])     ? $aXml['data']['picture']       : '',  // Скачать и сохранить у нас на сервере
            'param'         => isset($aXml['data']['param'])       ? $aXml['data']['param'] : '',
            'external_id'   => isset($aXml['attr']['id'])          ? $aXml['attr']['id']            : (isset($aXml['id']) ? $aXml['id'] : ''),  // указывает id товара из своей БД
            'country_of_origin' => isset($aXml['data']['country_of_origin'])   ? $aXml['data']['country_of_origin'] : '' // Производитель
        );
        if($data['in_stock'] == 'del')   $data['in_stock'] = self::AVAILABLE_DELETE_PRODUCT; else
        if($data['in_stock'] == 'true')  $data['in_stock'] = 1; else
        if($data['in_stock'] == 'false') $data['in_stock'] = 2; else
        if($data['in_stock'] == 'no')    $data['in_stock'] = 0; else
        if($data['in_stock'] == '') $data['in_stock'] = 1;
        return $data;
    }


    /**
     * Парсим XML
     *
     * @return bool
     */
    private function parseProduct(){

        $oXml = new AWS_XmlToArray( $this->fileContent );

        $aXml = $oXml->createArray();

        if(isset($aXml['offers']['offer'])){
            $aXml = $aXml['offers']['offer'];
        } else $aXml = false;

        return $aXml;
    }
}