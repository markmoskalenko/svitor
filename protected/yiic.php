<?php

/* setup default time zone */
date_default_timezone_set('UTC');

/* change dir to root */
chdir(dirname(__FILE__) . '/..');

/* change to set debug mode */
// defined('YII_DEBUG') or define('YII_DEBUG',true);

$config = 'protected/configs/cron.php';

require_once('../framework/yii.php');


require_once('../framework/yiic.php');

exit;