/* Dmitry Marinin 2012 */

var checklist = {
    init : function (params) {
        if (!empty(params.fun_type))
            checklist.selectFunType('#'+params.fun_type+'_types select', {fun_type : params.fun_type});
    },
    selectFunType : function (link, params) {
        var value;

        if (!empty(params) && !empty(params.fun_type)) {
            value = params.fun_type;
        } else {
            value = $(link).val();
        }
        $('#groups_selectors').show();

        var holiday_types = $('#holiday_types');
        var holiday_groups = $('#holiday_groups');
        var wedding_types = $('#wedding_types');
        var wedding_groups = $('#wedding_groups');

        if (value == 'wedding') {
            holiday_types.find('select option[value=""]').attr('selected', true);
            holiday_types.hide();
            holiday_groups.find('select option[value=""]').attr('selected', true);
            holiday_groups.hide();

            wedding_types.show();
            wedding_groups.show();
        }
        else if (value == 'holiday') {
            wedding_types.find('select option[value=""]').attr('selected', true);
            wedding_types.hide();
            wedding_groups.find('select option[value=""]').attr('selected', true);
            wedding_groups.hide();

            holiday_types.show();
            holiday_groups.show();
        }
    }
};