/* Dmitry Marinin 2012 */

var exhibitions = {
    data : {
        id : 0,
        info : '',
        edit_event : true
    },
    addRegion : function() {
        var country = $("#country option:selected").val();
        if(country == 0){
            $("#region").parents("tr").css("display","none");
            $("#sity").parents("tr").css("display","none");
        }else{
            $.ajax({
                type: "POST",
                url: "/mod_cp/exhibitions/regions",
                data: "country=" +country,
                success: function(result){
                    var data = $.parseJSON(result);
                    var str = '<option value="0">--- не выбрано ---</option>';
                    for(p in data) {
                        str += '<option value="'+p+'">'+data[p]+'</option>';
                    }
                    $("#region").html(str);
                    $("#region").parents("tr").css("display","block");
                    $("#sity").html('<option value="0">--- не выбрано ---</option>');
                    $("#sity").parents("tr").css("display","none");
                }
            })
        }
    },
    addSity : function() {
        var sity = $("#region option:selected").val();
        if(sity == 0){
            $("#sity").parents("tr").css("display","none");
        }else{
            $.ajax({
                type: "POST",
                url: "/mod_cp/exhibitions/sity",
                data: "sity=" +sity,
                success: function(result){
                    var data = $.parseJSON(result);
                    var str = '<option value="0">--- не выбрано ---</option>';
                    for(p in data) {
                        str += '<option value="'+p+'">'+data[p]+'</option>';
                    }
                    $("#sity").html(str);
                    $("#sity").parents("tr").css("display","block");
                }
            })
        }
    },
    focus : function (item)
    {
        var id = item.getAttribute("id");
        $("#"+id).find(".action").css("display","block");
    }  ,
    outfocus : function (item)
    {
        var id = item.getAttribute("id");
        $("#"+id).find(".action").css("display","none");
    }
};