/* Dmitry Marinin 2012 */

var params_data;

listdata_sortable = {
    init : function (params) {

        if (!params.listdata) {
            alert('listdata_sortable.js: I wanna "lisdata" parameter!');
            return false;
        }

        if (!params.actionbar) {
            alert('listdata_sortable.js: I wanna "actionbar" parameter!');
            return false;
        }

        if (!params.saveUrl) {
            alert('listdata_sortable.js: I wanna "saveUrl" parameter!');
            return false;
        }

        params_data = params;

        var listdata = params_data.listdata;
        var actionbar = params_data.actionbar;

        listdata.sortable({
            scroll : false,
            opacity : 0.6,
            update : function (event, ui) {
                actionbar.slideDown(200);
            }
        });
        listdata.sortable('option', 'handle', 'span[class^=sortable_trigger]');
    },
    saveData : function (trigger) {
        var data = params_data.listdata.sortable('serialize');
        var button_save_text = $(trigger).text();
        var button_reset = $('.reset_button');

        button_reset.hide();
        $(trigger).html($.ajaxLoader());

        $.post(params_data.saveUrl, data, function (re) {
            if (re == 'ok') {
                params_data.actionbar.slideUp(200);
            } else {
                alert('Не удалось сохранить... Обновите страницу и попробуйте еще раз.')
            }
            $(trigger).html(button_save_text);
            button_reset.show();
        });
    },
    resetData : function (trigger) {

    }
};