/* Dmitry Marinin 2012 */

var catalog = {
    data : {
        id : 0,
        info : '',
        edit_event : true
    },
    removeCat : function(id, data) {
        catalog.data.edit_event = false;
        $('#trash-dialog').dialog({
            'modal' : true,
            'width' : 400,
            'resizable' : false,
            'buttons' : {
                "Да" : function() {
                    $.get('/admin_cp/category/delete/', 'id='+id, function(re) {
                        if (re == 1) {
                            $('#cat_'+id).slideUp(300);
                        } else {
                            $('#remove_errors').html('<span class="msg_error_block">'+re+'</span').fadeIn(100);
                            setTimeout(function() {
                                $('#remove_errors').slideUp(500).html('');
                            }, 5000);
                        }
                    });
                    $('#trash-dialog').dialog("close");
                },
                "Отмена" : function() {
                   $('#trash-dialog').dialog("close");
                }
            },
            'close' : function () {
                catalog.data.edit_event = true;
            }
        });
        return false;
    },
    editCat : function (id)
    {
        if (catalog.data.edit_event)
            location.assign('/admin_cp/category/edit/id/'+id);
    }
};

$(function(){
    $('#Category_text_footer_catalog, #Category_text_header_catalog, #Category_text_header_product, #Category_text_footer_product').ckeditor({width : '98%'});

    $('#Category_visible_vndr').change(function(){
        if(this.checked)
            $('.catalog_seo').show(200);
        else
            $('.catalog_seo').hide(150);

    });
    $('#Category_visible_prod').change(function(){
        if(this.checked)
            $('.product_seo').show(200);
        else
            $('.product_seo').hide(150);

    });

    if( ! $('#Category_visible_vndr').is(':checked') ){
        $('.catalog_seo').hide(0);
    }
    if( ! $('#Category_visible_prod').is(':checked') ){
        $('.product_seo').hide(0);
    }
});