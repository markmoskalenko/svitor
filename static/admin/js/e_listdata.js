/* Dmitry Marinin 2012 */

var listdata = {
    data : {
        goEditEvent : true,
        editValueUrl : '',
        editUrl : '',
        addUrl : ''
    },

    init : function (params) {
        if (!empty(params.editValueUrl))
            listdata.data.editValueUrl = params.editValueUrl;

        if (!empty(params.addUrl))
            listdata.data.addUrl = params.addUrl;

        if (!empty(params.editUrl))
            listdata.data.editUrl = params.editUrl;

        if (!empty(params.deleteUrl))
            listdata.data.deleteUrl = params.deleteUrl;
    },

    addNew : function () {
        location.assign(listdata.data.addUrl + location.search);
    },

    editItem : function (trigger, id) {
        if (listdata.data.goEditEvent)
            location.assign(listdata.data.editUrl + '/id/' + id + location.search);
    },

    deleteItem : function (trigger, id) {
        if (listdata.data.goEditEvent){
            listdata.data.goEditEvent = false;

            var element = $('#element_'+id);
            var warningBox = $('#trash-dialog');
            var default_html = element.html();

            element.css('background-color', '#eda1a1');
            warningBox.dialog({
                'modal'   : true,
                'width' : 400,
                'resizable' : false,
                'buttons' : {
                    "Да" : function() {
                        warningBox.dialog("close");
                        element.find('.edit_links').remove();
                        element.find('nobr').append(' '+$.ajaxLoader()+ ' ');

                        $.get(listdata.data.deleteUrl, 'id='+id, function (re) {
                            if (re == 'ok') {
                                element.fadeOut(300, function () {
                                    $(this).remove();
                                });
                            } else {
                                alert('Не удалось удалить...');
                                element.html(default_html);
                            }
                            listdata.data.goEditEvent = true;
                        });
                    },
                    "Отмена" : function() {
                        warningBox.dialog("close");
                        listdata.data.goEditEvent = true;
                    }
                },
                'close' : function () {
                    element.css('background-color', '');
                }
            });
            return false;
        }
    },

    editValue : function (trigger, id) {
        var value = $(trigger).find('nobr');

        if (listdata.data.goEditEvent)
        {
            value.hide();
            value.parent().append('<input type="text" class="edit_input" value="'+value.text()+'">');

            var edit_input = $(trigger).find('.edit_input');
            var end = edit_input.val().length;

            edit_input.focus().selectRange(end, end);
            edit_input.one('blur', function () {
                listdata.saveValue(edit_input, id);
            });
            edit_input.keyup(function (event) {
                if (event.keyCode == 13 || event.keyCode == 27)
                    edit_input.blur();
            });
            listdata.data.goEditEvent = false;
        }
    },

    editInputHide : function (id) {
        listdata.data.goEditEvent = true;
        $('#element_'+id+' input').hide().remove();
        $('#element_'+id+' nobr').show();
    },

    saveValue : function (trigger, id) {
        var new_value = $(trigger).val();
        var element_value = $('#element_'+id+' nobr');
        var old_value = element_value.text();

        element_value.text(new_value);
        listdata.editInputHide(id);

        if (old_value != new_value)
        {
            $.get(listdata.data.editValueUrl, 'id='+id+'&value=' + new_value, function (re) {
                if (re != 'ok') {
                    alert(re);
                    element_value.text(old_value);
                }
            });
        }
    },

    applyFilter : function (trigger, dependent_filter) {

        // Main filter always has ID == 1
        if (!dependent_filter)
            dependent_filter = 1;

        var input = $(trigger);
        var url_query = input.attr('name') + '=' + input.val();

        var base_url = location.pathname;
        var search_string = location.search;

        if (!search_string || dependent_filter == 1) {
            if (input.val() != '') {
                url_query = '?' + url_query;
                location.assign(base_url + url_query);
            } else {
                location.assign(base_url);
            }
        }
        else {
            url_query = '&' + url_query;

            var reg = new RegExp('[?&]' + input.attr('name') + '=[A-Za-z0-9_-]+');

            if (search_string.match(reg) && input.val() == '') {
                search_string = search_string.replace(reg, '');
            }
            else if (search_string.match(reg)) {
                search_string = search_string.replace(reg, url_query);
            } else {
                search_string += url_query;
            }

            var url = base_url + search_string;
            url = url.replace(new RegExp(base_url + '[&]'), base_url + '?');

            location.assign(url);
        }
    }
};