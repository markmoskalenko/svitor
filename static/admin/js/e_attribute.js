/* Dmitry Marinin 2012 */

var attribute = {
    selectType : function (trigger) {
        var is_multi = $('#is_multi');
        var is_multi_help = $('#is_multi_help');

        if ($(trigger).val() != 'boolean' && $(trigger).val() != '')
        {
            is_multi.fadeIn(300);
            is_multi_help.fadeIn(300);
        } else {
            is_multi.fadeOut(300);
            is_multi_help.fadeOut(300);
            is_multi.find('input').removeAttr('checked');
        }
    }
};