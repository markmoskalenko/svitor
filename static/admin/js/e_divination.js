/* Dmitry Marinin 2012 */

var divination = {
    data : {
        id : 0,
        info : '',
        edit_event : true
    },
    addRegion : function() {
        var country = $("#country option:selected").val();
        if(country == 0){
            $("#region").parents("tr").css("display","none");
            $("#sity").parents("tr").css("display","none");
        }else{
            $.ajax({
                type: "POST",
                url: "/mod_cp/divination/regions",
                data: "country=" +country,
                success: function(result){
                    var data = $.parseJSON(result);
                    var str = '<option value="0">--- не выбрано ---</option>';
                    for(p in data) {
                        str += '<option value="'+p+'">'+data[p]+'</option>';
                    }
                    $("#region").html(str);
                    $("#region").parents("tr").css("display","block");
                }
            })
        }
    },
    addSity : function() {
        var sity = $("#region option:selected").val();
        if(sity == 0){
            $("#sity").parents("tr").css("display","none");
        }else{
            $.ajax({
                type: "POST",
                url: "/mod_cp/divination/sity",
                data: "sity=" +sity,
                success: function(result){
                    var data = $.parseJSON(result);
                    var str = '<option value="0">--- не выбрано ---</option>';
                    for(p in data) {
                        str += '<option value="'+p+'">'+data[p]+'</option>';
                    }
                    $("#sity").html(str);
                    $("#sity").parents("tr").css("display","block");
                }
            })
        }
    },
    editCat : function (id)
    {
        if (catalog.data.edit_event)
            location.assign('/admin_cp/category/edit/id/'+id);
    } ,
    focus : function (item)
    {
        var id = item.getAttribute("id");
        $("#"+id).find(".action").css("display","block");
    }  ,
    outfocus : function (item)
    {
        var id = item.getAttribute("id");
        $("#"+id).find(".action").css("display","none");
    },
    check: function(item){
        if(!item){
            $("#data").css("display","none");
            $("#Divination_date_from").attr("value",'01-01-1970');
            $("#Divination_date_to").attr("value",'01-01-1970');
        } else{
            $("#data").css("display","block");
        }
    }
};