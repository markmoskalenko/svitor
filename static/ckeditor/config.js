﻿/*
Copyright (c) 2003-2012, CKSource - Frederico Knabben. All rights reserved.
For licensing, see LICENSE.html or http://ckeditor.com/license
*/


CKEDITOR.editorConfig = function( config )
{
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	//config.uiColor = '#AADC6E';
    config.extraPlugins = 'MediaEmbed';
    config.toolbar = 'Full';
    config.toolbar_Full = [

        { name: 'clipboard', items : ['Undo','Redo','-','PasteText'] },
        { name: 'styles', items : [ 'Format' ] },
        { name: 'basicstyles', items : [ 'Bold','Italic','Strike','-','RemoveFormat' ] },
        { name: 'paragraph', items : [ 'NumberedList','BulletedList','-','Outdent','Indent',
            '-','JustifyLeft','JustifyCenter','JustifyRight'/*,'JustifyBlock' */] },
        { name: 'insert', items : [ 'Image','MediaEmbed','-','Table','HorizontalRule','SpecialChar'] },
        { name: 'links', items : [ 'Link','Unlink'] },
        { name: 'document', items : [ 'Source'] },
    ]
};
