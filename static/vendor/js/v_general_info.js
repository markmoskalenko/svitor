/* Dmitry Marinin 2012 */

var general_info = {
    morePhones : function (link) {
        $('.more_contact_phones').slideDown(100, function () {
            $(link).hide();
        });
    },
	uploadLogo : function (button) {
        var old_thumbs = $('#image_thumbs').html();

        new AjaxUpload(button, {
            action : '/file/uploadImg',
            name : 'image',
            onSubmit : function (file, ext) {
                $('#image_thumbs').html($.ajaxLoader());
                button.hide();
            },
            onComplete : function (file, response) {
                var img = $.parseJSON(response);

                if (img.status) {
                    if (img.status == 'ok') {
                        $('#img_id').val(img.img_id);
                        $('#img_filename').val(img.file_name);
                        $('#image_thumbs').html('<img src="/'+img.path+'/medium/'+img.file_name+'"> <img src="/'+img.path+'/small/'+img.file_name+'">');
                        old_thumbs = $('#image_thumbs').html();
                        $('#image_thumbs').append('<br><b class="warning">Не забудьте сохранить изменения. Нажмите кнопку «Сохранить» в нижней части формы.</b>');
                    } else {
                        alert(img.msg);
                        $('#image_thumbs').html(old_thumbs);
                    }
                } else {
                    alert(response);
                }
                button.show();
            }
        });
	}
};