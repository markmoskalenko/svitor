/* Dmitry Marinin 2012 */

var categories = {
    data : {
        cat_id : null,
        count : 0,
        max_count : 0
    },
    init : function (count, max_count) {
        categories.data.count = count;
        categories.data.max_count = max_count;
    },
    initForm : function (cat_id) {
        if (cat_id != null)
            categories.data.cat_id = cat_id;

        if (categories.data.cat_id != 0 && categories.data.cat_id != null)
            categories.loadAttributes(categories.data.cat_id);

    },
    loadAttributes : function (category, vendor) {
        if (category != '') {

            var vendor_data = '';
            if (vendor)
                vendor_data = '&vendor_id='+vendor;

            $('#attributes').hide().fadeIn(300);
            $('#attributes_data').html($.ajaxLoader()).show();
            $.get('/vendor_cp/company/categories/a_get_attributes/', 'cat_id='+category+vendor_data, function (re) {
                if (re != 0) {
                    $('#attributes_data').html(re);
                    $('#attributes').fadeIn(200);
                } else {
                    $('#attributes_data').hide(200);
                    $('#attributes').slideUp(200);
                }
            });
        } else {
            $('#attributes').hide(200);
        }
    },
    attributesReset : function () {
        $('#attributes_data').html('');
        $('#attributes').hide();
    },
    removeCat : function (link, id) {
        $('#element_'+id).addClass('selected');
        $('#trash-dialog').dialog({
            'modal'   : true,
            'width' : 400,
            'resizable' : false,
            'buttons' : {
                "Удалить" : function() {
                    $('#trash-dialog').dialog("close");
                    $.get('/vendor_cp/company/categories/id/'+id+'/delete/', function (re) {
                        if (re.length < 5) {
                            categories.data.count = re;
                            categories.updateCounter();
                            $('#element_'+id).slideUp(200);
                        } else {
                            alert(re);
                        }
                    });
                },
                "Отмена" : function() {
                    $('#trash-dialog').dialog("close");
                }
            },
            'close' : function () {
                $('#element_'+id).removeClass('selected');
            }
        });
        return false;
    },
    updateCounter : function () {
        if (categories.data.count < categories.data.max_count) {
            $('#addCategoryButton').show();
        } else {
            $('#addCategoryButton').hide();
        }
        $('#categoryCounter').text(categories.data.count)
    }
};