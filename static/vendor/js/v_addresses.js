/* Dmitry Marinin 2012 */

var addresses = {
    data : {
        count : 0,
        max_count : 0,
        country_id : 0,
        city_id : 0,
        region_id : 0,
        metro_id : 0
    },
    init : function (params) {
        if (!empty(params.count))
            addresses.data.count = params.count;

        if (!empty(params.max_count))
            addresses.data.max_count = params.max_count;

        if (!empty(params.country)) {
            addresses.data.country_id = params.country;
            $('#address_country select option[value='+addresses.data.country_id+']').attr('selected', true);
            addresses.getRegions(addresses.data.country_id);
        }

        if (!empty(params.region)) {
            addresses.data.region_id = params.region;
            addresses.getCities(addresses.data.region_id);
        }

        if (!empty(params.city)) {
            addresses.data.city_id = params.city;
            addresses.getMetro(addresses.data.city_id);
        }

        if (!empty(params.metro))
            addresses.data.metro_id = params.metro;

        addresses.updateData();
    },
    select : function () {
        upBox.ajaxLoad('/area/select/', true);
    },
    getRegions : function (id, select) {
        $('#address_region').fadeIn(200);
        $('#address_region_loader').html($.ajaxLoader());
        $.get('/area/get_regions/country/'+id, function (response) {
            $('#address_region select').html(response);
            if (addresses.data.region_id != 0 && addresses.data.region_id != '')
                $('#address_region select option[value='+addresses.data.region_id+']').attr('selected', true);

            $('#address_region_loader').html('');
        });
    },
    getCities : function (id) {
        $('#address_city').fadeIn(200);
        $('#address_city_loader').html($.ajaxLoader());
        $.get('/area/get_cities/region/'+id, function (response) {
            $('#address_city select').html(response);
            if (addresses.data.city_id != 0 && addresses.data.city_id != '')
                $('#address_city select option[value='+addresses.data.city_id+']').attr('selected', true);

            $('#address_city_loader').html('');
        });
    },
    getMetro : function (id) {

        $('#address_metro_loader').html($.ajaxLoader());
        $.get('/area/get_metro_stantions/city/'+id, function (response) {
            if (response != 0) {
                $('#address_metro').fadeIn(200);
                $('#address_metro select').html(response);
                if (addresses.data.metro_id != 0 && addresses.data.metro_id != '')
                    $('#address_metro select option[value='+addresses.data.metro_id+']').attr('selected', true);

            } else {
                addresses.hideMetro();
            }
            $('#address_metro_loader').html('');
        });
    },
    selectCountry : function (link) {
        addresses.data.country_id = $(link).val();
        addresses.hideCity();
        addresses.hideMetro();
        addresses.data.region_id = 0;
        addresses.getRegions(addresses.data.country_id);
        addresses.updateData();
    },
    selectRegion : function (link) {
        addresses.data.region_id = $(link).val();
        addresses.hideMetro();
        addresses.data.city_id = 0;
        addresses.getCities(addresses.data.region_id);
        addresses.updateData();
    },
    selectCity : function (link) {
        addresses.data.city_id = $(link).val();
        addresses.getMetro(addresses.data.city_id);
        addresses.updateData();
    },
    hideRegion : function () {
        addresses.data.region_id = 0;
        $('#address_region select option[value=""]').attr('selected', true);
        $('#address_region').fadeOut(200);
    },
    hideCity : function () {
        addresses.data.city_id = 0;
        $('#address_city select option[value=""]').attr('selected', true);
        $('#address_city').fadeOut(200);
    },
    hideMetro : function () {
        addresses.data.metro_id = 0;
        $('#address_metro select option[value=""]').attr('selected', true);
        $('#address_metro').fadeOut(200);
    },
    updateData : function () {
        if (addresses.data.country_id == '' || addresses.data.country_id == 0)
            addresses.hideRegion();

        if (addresses.data.region_id == 0 || addresses.data.region_id == '')
            addresses.hideCity();

        if (addresses.data.city_id == 0 || addresses.data.city_id == '')
            addresses.hideMetro();
    },
    deleteItem : function (link, id) {
        $('#element_'+id).addClass('selected');
        $('#trash-dialog').dialog({
            'modal'   : true,
            'width' : 400,
            'resizable' : false,
            'buttons' : {
                "Удалить" : function() {
                    $('#trash-dialog').dialog("close");
                    $.get('/vendor_cp/company/addresses/id/'+id+'/delete/', function (re) {
                        if (re.length < 5) {
                            addresses.data.count = re;
                            addresses.updateCounter();
                            $('#element_'+id).slideUp(200);
                        } else {
                            alert(re);
                        }
                    });
                },
                "Отмена" : function() {
                    $('#trash-dialog').dialog("close");
                }
            },
            'close' : function () {
                $('#element_'+id).removeClass('selected');
            }
        });
        return false;
    },
    updateCounter : function () {
        if (addresses.data.count < addresses.data.max_count) {
            $('#addAddressButton').show();
        } else {
            $('#addAddressButton').hide();
        }
        $('#addressCounter').text(addresses.data.count)
    }
};