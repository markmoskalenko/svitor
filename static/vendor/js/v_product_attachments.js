/* Dmitry Marinin 2012 */

var attachments = {
    data : {
        product_id : 0,
        img_count : 0,
        max_images : 0
    },

    init : function (product_id, img_count, max_images) {
        if (img_count != '')
            attachments.data.img_count = img_count;

        if (product_id != '')
            attachments.data.product_id = product_id;

        attachments.data.max_images = max_images;
    },

    uploadImg : function (button) {
        new AjaxUpload(button, {
            action : '/file/uploadImg',
            name : 'image',
            onSubmit : function (file, ext) {
                $('#loader').html($.ajaxLoader());
            },
            onComplete : function (file, response) {

                var img = $.parseJSON(response);

                if (img.status) {
                    if (img.status == 'ok') {
                        $.post('/vendor_cp/products/attachments/id/'+attachments.data.product_id, img, function(response) {
                            var reAttach = $.parseJSON(response);

                            $('#images_thumbs').append('<div class="img" id="img_'+reAttach.id+'">' +
                                '<a class="upbox-button" rel="upbox-images" href="/'+img.path+'/'+img.file_name+'"><img src="/'+img.path+'/medium/'+img.file_name+'"></a>' +
                                '<a class="delete magic_button" onclick="attachments.remove(this, '+reAttach.id+'); return false;" onmouseover="tooltip.show(this, \'Удалить\', {position:\'top-right\'});"></a>' +
                                '<br><a onclick="attachments.edit(' + reAttach.id + '); return false;">редактировать</a>' +
                                '</div>');

                            $('#img_'+reAttach.id).hide().show(200);
                            attachments.data.img_count = reAttach.img_count;
                            attachments.updateCounter();

                            $('#loader').html('');
                        });
                    } else {
                        alert(img.msg);
                    }
                } else {
                    alert(response);
                }
            }
        });
    },

    uploadImgUrl : function (button) {
        button.click(function(){
            if($('#urlImage').val() == '') {alert('Вставте в поле URL адрес изображения'); return false;}
        $.ajax({
            type: "POST",
            url: '/file/uploadImgUrl',
            data: ({ 'url': $('#urlImage').val() }),
            beforeSend : function () {
                $('#loader').html($.ajaxLoader());
            },
            success: function( response ){
                var img = $.parseJSON(response);
                console.log(response);
                if (img.status) {
                    if (img.status == 'ok') {
                        $.post('/vendor_cp/products/attachments/id/'+attachments.data.product_id, img, function(response) {
                            var reAttach = $.parseJSON(response);

                            $('#images_thumbs').append('<div class="img" id="img_'+reAttach.id+'">' +
                                '<a class="upbox-button" rel="upbox-images" href="/'+img.path+'/'+img.file_name+'"><img src="/'+img.path+'/medium/'+img.file_name+'"></a>' +
                                '<a class="delete magic_button" onclick="attachments.remove(this, '+reAttach.id+'); return false;" onmouseover="tooltip.show(this, \'Удалить\', {position:\'top-right\'});"></a>' +
                                '<br><a onclick="attachments.edit(' + reAttach.id + '); return false;">редактировать</a>' +
                                '</div>');

                            $('#img_'+reAttach.id).hide().show(200);
                            attachments.data.img_count = reAttach.img_count;
                            attachments.updateCounter();

                            $('#loader').html('');
                            $('#urlImage').val('');
                        });
                    } else {
                        alert(img.msg);
                        $('#loader').html('');
                        $('#urlImage').val('');
                    }
                } else {
                    alert(response);
                    $('#loader').html('');
                    $('#urlImage').val('');
                }
            }

        });
        });
    },

    save : function (trigger, id) {
        var data = $('#imageEdit form').serialize(),
            button_old_content = $(trigger).html();

        $(trigger).html($.ajaxLoader()).attr('disabled', true);

        $.post('/vendor_cp/products/attachments/id/'+attachments.data.product_id+'/edit/'+id, data, function(response){
            if (response == 'ok') {
                var desc = $('#imageEdit form textarea').val();
                $('#img_'+id).find('.upbox-button').attr('title', desc);
                $.fancybox.close();
            } else {
                $('#imageEdit #msg').html(response);
                $(trigger).html(button_old_content).attr('disabled', false);
            }
        });
    },

    edit : function (id) {
        upBox.ajaxLoad('/vendor_cp/products/attachments/id/'+attachments.data.product_id+'/edit/'+id);
    },

    remove: function (trigger, id) {
        $('#img_'+id).hide(200, function () {
            $('#img_'+id).remove();
        });
        attachments.data.img_count--;

        attachments.updateCounter();
        $.get('/vendor_cp/products/attachments/id/'+attachments.data.product_id+'/?delete='+id);
    },

    updateCounter : function () {
        $('#images_counter span').html(attachments.data.img_count);

        if (attachments.data.img_count == attachments.data.max_images)
            $('#addImgButton').hide();

        if (attachments.data.img_count < attachments.data.max_images)
            $('#addImgButton').show();
    }
};