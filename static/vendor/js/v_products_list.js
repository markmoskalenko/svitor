/* Dmitry Marinin 2012 */

var productsList = {
    data : {
        count : 0,
        selected : 0,
        select : true
    },
	init : function () {
		$('#group_actions select').attr('disabled', 'disabled');
		$('tbody tr').bind('click', function () {
			productsList.selectTr($(this).attr('id'));
		});
	},
	selectTr : function (id) {
		if (productsList.data.select == true) {
			var product = '#'+id;

			if (!$(product+' input[type=checkbox]').attr('checked')) {
				$(product+' input[type=checkbox]').attr('checked', 'checked');
				$(product).addClass('selected');
			} else {
				$(product+' input[type=checkbox]').removeAttr('checked');
				$(product).removeClass('selected');
			}

			productsList.updateChecked();
			productsList.data.count = $('tbody input[type=checkbox]').length;
			if (productsList.data.count == productsList.data.selected)
			{
				$('input[name=select_all]').attr('checked', 'checked');
			} else {
				$('input[name=select_all]').removeAttr('checked');
			}
		}
	},
	selectAll : function (param) {
		if ($('input[name=select_all]').attr('checked') || param == 1) {
			$('tbody input[type=checkbox]').attr('checked', 'checked');
			$('tbody tr').addClass('selected');
		} else {
			$('tbody input[type=checkbox]').removeAttr('checked');
			$('tbody tr').removeClass('selected');
		}
		productsList.updateChecked();
	},
	groupAction : function (link) {
		var data   = $('#products_list tbody input[type=checkbox]').serialize();
		var action = $(link).attr('name');
		var value  = $(link).val();

        if(action == "change_del"){
            var url = "/vendor_cp/products/delete";
            if( value == 0 ) url = "/vendor_cp/products/restore";
            $('#group_action_msg').html($.ajaxLoader());
            $.post(url, data, function (re) {
                if (re == 1) {
                    $('#group_action_msg').html('');
                    window.location = '/vendor_cp/products';
                } else {
                    $('#group_action_msg').addClass('msg_error').html(re);
                }
            });
            return false;
        }

		if (data != '' && action != '') {
			$('#group_action_msg').html($.ajaxLoader());
			$.post('/vendor_cp/products/groupAction/act/'+action+'/set/'+value, data, function (re) {
				if (re == 1) {
					$('#group_action_msg').html('');
					document.location.reload();
				} else {
					$('#group_action_msg').addClass('msg_error').html(re);
				}
			});
		}
	},
	updateChecked : function () {
		productsList.data.selected = $('tbody input[type=checkbox]:checked').length;
		if (productsList.data.selected > 0) {
			$('.selected_counter').text(productsList.data.selected);
			$('#group_actions select').removeAttr('disabled');
		} else {
			$('.selected_counter').text('');
			$('#group_actions select').attr('disabled', 'disabled');
		}
	},
	deleteItem : function (id) {
		if (id != '') {
			productsList.data.select = false;
			$('#delete-dialog').dialog({
				'modal' : true,
				'resizable' : false,
				'width' : 450,
				'buttons' : [
					{
						text : 'Да',
						click : function () {
							productsList.data.select = true;
							document.location.assign('/vendor_cp/products/delete/id/'+id);
						}
					},
					{
						text : 'Нет',
						click : function () {
							productsList.data.select = true;
							$(this).dialog('close');
						}
					}
				],
				'close' : function () {
					productsList.data.select = true;
				}
			});
		}
	},
    restoreItem : function (id) {
        if (id != '') {
            productsList.data.select = false;
            $('#restore-dialog').dialog({
                'modal' : true,
                'resizable' : false,
                'width' : 450,
                'buttons' : [
                    {
                        text : 'Да',
                        click : function () {
                            productsList.data.select = true;
                            document.location.assign('/vendor_cp/products/restore/id/'+id);
                        }
                    },
                    {
                        text : 'Нет',
                        click : function () {
                            productsList.data.select = true;
                            $(this).dialog('close');
                        }
                    }
                ],
                'close' : function () {
                    productsList.data.select = true;
                }
            });
        }
    }
};