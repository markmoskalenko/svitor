/* Dmitry Marinin 2012 */

var certificate = {
    data : {
        count : 0,
        max_count : 0
    },
    init : function (count, max_count) {
        certificate.data.count = count;
        certificate.data.max_count = max_count;
    },
    deleteItem : function (link, id) {
        $('#trash-dialog').dialog({
            'modal'   : true,
            'width' : 400,
            'resizable' : false,
            'buttons' : {
                "Да" : function() {
                    $('#trash-dialog').dialog("close");
                    $.get('/vendor_cp/company/certificates/', 'delete=1&id='+id, function (re) {
                        if (re.length < 5) {
                            certificate.data.count = re;
                            certificate.updateCounter();
                            $('#element_'+id).hide(200, function () {
                                $('#element_'+id).remove();
                            });
                        } else {
                            alert(re);
                        }
                    });
                },
                "Отмена" : function() {
                    $('#trash-dialog').dialog("close");
                }
            }
        });
        return false;
    },
    uploadImg : function (button) {
        var old_thumbs = $('#image_thumbs').html();

        new AjaxUpload(button, {
            action : '/file/uploadImg',
            name : 'image',
            onSubmit : function (file, ext) {
                $('#image_thumbs').html($.ajaxLoader());
                button.hide();
            },
            onComplete : function (file, response) {
                var img = $.parseJSON(response);

                if (img.status) {
                    if (img.status == 'ok') {
                        $('#img_id').val(img.img_id);
                        $('#img_filename').val(img.file_name);
                        $('#image_thumbs').html('<img src="/'+img.path+'/medium/'+img.file_name+'">');
                        old_thumbs = $('#image_thumbs').html();
                    } else {
                        alert(img.msg);
                        $('#image_thumbs').html(old_thumbs);
                    }
                } else {
                    alert(response);
                }
                button.show();
            }
        });
    },
    updateCounter : function () {
        if (certificate.data.count < certificate.data.max_count) {
            $('#addCertificateButton').show();
        } else {
            $('#addCertificateButton').hide();
        }
        $('#certificateCounter').text(certificate.data.count)
    }
};