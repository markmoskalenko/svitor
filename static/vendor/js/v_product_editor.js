/* Dmitry Marinin 2012 */

var productEditor = {
	loadAttributes : function (category, product) {
        var attributes = $('#attributes');
        var attributes_data = $('#attributes_data');

		if (category != '') {
			var product_data = '';

			if (product)
				product_data = '&product_id='+product;

            attributes.hide().fadeIn(300);
            attributes_data.html($.ajaxLoader()).show();
			$.get('/vendor_cp/products/getAttributes/', 'cat_id='+category+product_data, function (re) {
                if (re != 0) {
                    attributes_data.html(re);
                    attributes.fadeIn(200);
                } else {
                    attributes_data.hide(200);
                    attributes.slideUp(200);
                }
			});
		} else {
            attributes.hide(200);
		}
	},
	attributesReset : function () {
		$('#attributes_data').html('');
		$('#attributes').hide();
	},
	checkRequiredAttr : function (href) {
        alert('Выберите значения для характеристик, помеченных как обязательные.');
        return true;
	}
};