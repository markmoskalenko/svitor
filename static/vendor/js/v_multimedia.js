/* Dmitry Marinin 2012 */

var multimedia = {
    data : {
        vendor_id : 0,
        img_count : 0,
        video_count: 0,
        max_images : 0,
        max_videos : 0
    },

    init : function (vendor_id, img_count, video_count, max_images, max_videos) {
        multimedia.data.img_count = img_count;
        multimedia.data.video_count = video_count;
        multimedia.data.vendor_id = vendor_id;
        multimedia.data.max_images = max_images;
        multimedia.data.max_videos = max_videos;
    },

    videoAddForm : function ()
    {
        upBox.ajaxLoad('/vendor_cp/company/multimedia/video/');
    },

    checkVideo : function (trigger, link) {

        if ($(link).val() != '') {

            var button_old_content = $(trigger).html();

            $(trigger).html($.ajaxLoader()).attr('disabled', true);
            $(link).attr('disabled', true);
            $('#video_info').hide();
            $('.b-upbox .buttons .button_main').hide();

            $.getJSON('/vendor_cp/video/checkUrl/?url=' + encodeURIComponent($(link).val()), function(json) {
                if (!json['error']) {
                    $('#link_error').hide();
                    $('#video_info').show();
                    $('#addVideoForm input[name=video_id]').val(json['video_id']);
                    $('#addVideoForm input[name=video_provider]').val(json['video_provider']);
                    $('#addVideoForm input[name=title]').val(json["title"]);
                    $('#addVideoForm input[name=thumbnail_url]').val(json['thumbnail_url']);
                    $('#addVideoForm textarea[name=description]').val(json['description']);
                    $('#addVideoForm img').attr('src', json['thumbnail_url']);
                    $('.b-upbox .buttons .button_main').show();
                } else {
                    $('#link_error').html(json['error']).show();
                    $('#video_info').hide();
                }

                $(trigger).html(button_old_content).attr('disabled', false);
                $(link).attr('disabled', false);
            });

        } else {
            $(link).focus();
            $('#video_info').hide();
            $('.b-upbox .buttons .button_main').hide();
        }
    },

    saveVideo : function (trigger) {
        var data = $('#addVideoForm form').serialize();

        var text = $(trigger).text();
        $(trigger).html($.ajaxLoader()).attr('disabled','disabled');

        $.post('/vendor_cp/company/multimedia/', data, function (response) {

            var re = $.parseJSON(response);

            if (re.error == '') {
                $('#videos_thumbs').append('<div class="video" id="video_'+re.id+'">' +
                    '<a class="upbox-media" href="'+re.video_url+'" title="'+re.title+'">' +
                    '<img src="'+re.thumbnail_url+'" width="120" height="90"></a><br>' +
                    '<a onclick="multimedia.editVideo('+ re.id +'); return false;">редактировать</a>' +
                    '<a class="delete magic_button" onclick="multimedia.remove(\'video\', this, ' + re.id + '); return false;" onmouseover="tooltip.show(this, \'Удалить\', {position:\'top-right\'});"></a></div>');

                $('#video_'+re.id).hide().show(100);
                multimedia.data.video_count = re.video_count;
                multimedia.updateCounter('video');

                $.fancybox.close()
            } else {
                $(trigger).html(text).removeAttr('disabled');
                alert(re.error);
            }
        });
    },

    saveImage : function (trigger, id) {
        var data = $('#imageEdit form').serialize(),
            button_old_content = $(trigger).html();

        $(trigger).html($.ajaxLoader()).attr('disabled', true);

        $.post('/vendor_cp/company/multimedia/edit_image/'+id, data, function(response){
            if (response == 'ok') {
                var desc = $('#imageEdit form textarea').val();
                $('#img_'+id).find('.upbox-button').attr('title', desc);
                $.fancybox.close();
            } else {
                $('#imageEdit #msg').html(response);
                $(trigger).html(button_old_content).attr('disabled', false);
            }
        });
    },

    uploadImg : function (button) {
        new AjaxUpload(button, {
            action : '/file/uploadImg',
            name : 'image',
            onSubmit : function (file, ext) {
                $('#loader').html($.ajaxLoader());
            },
            onComplete : function (file, response) {
                var img = $.parseJSON(response);

                if (img.status) {
                    if (img.status == 'ok') {
                        $.post('/vendor_cp/company/multimedia', img, function (response) {
                            var reMedia = $.parseJSON(response);

                            $('#images_thumbs').append('<div class="img" id="img_'+reMedia.id+'">' +
                                '<a class="upbox-button" rel="upbox-button" href="/'+img.path+'/'+img.file_name+'"><img src="/'+img.path+'/medium/'+img.file_name+'"></a>' +
                                '<a class="delete magic_button" onclick="multimedia.remove(\'image\', this, '+reMedia.id+'); return false;" onmouseover="tooltip.show(this, \'Удалить\', {position:\'top-right\'});"></a>' +
                                '<br><a onclick="multimedia.editImage(' + reMedia.id + '); return false;">редактировать</a>' +
                                '</div>');

                            $('#img_'+reMedia.id).hide().show(200);
                            multimedia.data.img_count = reMedia.img_count;
                            multimedia.updateCounter('image');

                            $('#loader').html('');
                        });

                    } else {
                        alert(img.msg);
                    }
                } else {
                    alert(response);
                }
            }
        });
    },

    editVideo : function (id) {
        upBox.ajaxLoad('/vendor_cp/company/multimedia/edit_video/'+id);
    },

    editImage : function (id) {
        upBox.ajaxLoad('/vendor_cp/company/multimedia/edit_image/'+id);
    },

    updateVideo : function (trigger, id) {
        var data = $('#addVideoForm form').serialize();

        var text = $(trigger).text();
        $(trigger).html($.ajaxLoader()).attr('disabled','disabled');
        $.post('/vendor_cp/company/multimedia/edit_video/'+id, data, function (re) {
            if (re == 'ok') {
                $.fancybox.close();
            } else {
                $(trigger).html(text).removeAttr('disabled');
                alert(re);
            }
        });
    },

    remove: function (type, link, id) {
        if (type == 'image') {
            $('#img_'+id).hide(200, function () {
                $('#img_'+id).remove();
            });
            multimedia.data.img_count--;
        } else if (type == 'video') {
            $('#video_'+id).hide(200, function () {
                $('#video_'+id).remove();
            });
            multimedia.data.video_count--;
        }
        multimedia.updateCounter(type);
        $.get('/vendor_cp/company/multimedia/delete/'+type+'/id/'+id);
    },

    updateCounter : function (type) {
        if (type == 'image') {
            $('#images_counter span').html(multimedia.data.img_count);

            if (multimedia.data.img_count == multimedia.data.max_images)
                $('#addImgButton').hide();

            if (multimedia.data.img_count < multimedia.data.max_images)
                $('#addImgButton').show();

        } else if (type == 'video') {
            $('#video_counter span').html(multimedia.data.video_count);

            if (multimedia.data.video_count == multimedia.data.max_videos)
                $('#addVideoButton').hide();

            if (multimedia.data.video_count < multimedia.data.max_videos)
                $('#addVideoButton').show();
        }
    }
};