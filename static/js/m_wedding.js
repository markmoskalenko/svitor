/* Dmitry Marinin 2012 */

var wedding = {
    addNew : function () {
        upBox.ajaxLoad('/wedding/add/');
    },
    goEdit : function (id) {
        upBox.ajaxLoad('/wedding/edit/id/'+id, true);
    },
    goEdit : function (id) {
        upBox.ajaxLoad('/wedding/edit/id/'+id, true);
    },
    setSelected : function (id) {
        location.assign('/dashboard/?wedding='+id);
    },
    sendForm : function (link, act) {
        var data = $('#weddingForm form').serialize();
        upBox.sendForm(link, '/wedding/' + act, data);
    }
}