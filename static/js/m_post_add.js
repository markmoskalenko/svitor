/* Dmitry Marinin 2012 */

var post_add = {

    data : {
        img_count : 0,
        max_images : 50
    },

    uploadImg : function (button) {
        new AjaxUpload(button, {
            action : '/file/uploadImg',
            name : 'image',
            onSubmit : function (file, ext) {
                $('#loader').html($.ajaxLoader());
            },
            onComplete : function (file, response) {
                var img = $.parseJSON(response);

                if (img.status) {
                    if (img.status == 'ok') {
                        var path = "'"+img.path+"'";
                        var file_name = "'"+img.file_name+"'";

                        $('#images_thumbs').append('<div class="img" id="img_'+img.img_id+'">' +
                            '<a class="upbox-button" rel="upbox-button" href="/'+img.path+'/'+img.file_name+'"><img src="/'+img.path+'/medium/'+img.file_name+'"></a>' +
                            '<a class="delete magic_button"  onclick="post_add.remove('+path+','+file_name+','+img.img_id+'); return false;"></a>' +
                            '</div>');

                        $('#img_'+img.img_id).hide().show(200);
                        post_add.data.img_count = +$('#images_counter span').html()+1;
                        post_add.updateCounter();
                        post_add.allimage();

                        $('#loader').html('');

                    } else {
                        alert(img.msg);
                    }
                } else {
                    alert(response);
                }
            }
        });
    },

    remove: function (path, image, id) {

        $('#img_' + id).hide(200, function () {
            $('#img_' + id).remove();
        });
        post_add.data.img_count--;
        post_add.updateCounter();
        post_add.allimage();

        $.ajax({
            type: "POST",
            url: '/post/deletephoto',
            data: 'path='+path+'&image='+image
        });
        post_add.allimage();
    } ,

    allimage: function () {

        var arr = "[";
        $( ".img" ).each(function( index ) {
            if(index == 0){
                arr = arr+'{'+
                    '"image_medium":"'+ $(this).find("img").attr("src")+'",'+
                    '"image_big":"'+ $(this).find(".upbox-button").attr("href")+'"'+
                    '}';
            }else{
                arr = arr+',{'+
                    '"image_medium":"'+ $(this).find("img").attr("src")+'",'+
                    '"image_big":"'+ $(this).find(".upbox-button").attr("href")+'"'+
                    '}';
            }
        });
        arr = arr+']';
        $("#imagePost").attr("value",arr);
    } ,

    updateCounter : function () {
        $('#images_counter span').html(post_add.data.img_count);

        if (post_add.data.img_count == post_add.data.max_images)
            $('#upload_image').hide();

        if (post_add.data.img_count < post_add.data.max_images)
            $('#upload_image').show();
    }
};