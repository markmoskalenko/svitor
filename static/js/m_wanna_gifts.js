wanna_gifts = {
    addNew : function () {
        upBox.ajaxLoad('/wannaGifts/add');
    },
    goEdit : function (id) {
        upBox.ajaxLoad('/wannaGifts/edit/id/'+id);
    },
    sendForm : function (trigger, act) {
        var data = $('#giftForm form').serialize();
        upBox.sendForm(trigger, '/wannaGifts/' + act, data);
    },
    deleteItem : function (id) {
        var item = $('#item_'+id);
        item.addClass('active');
        $('#trash-dialog').dialog({
            'modal'   : true,
            'width' : 400,
            'resizable' : false,
            'buttons' : {
                "Да" : function() {
                    $('#trash-dialog').dialog("close");
                    item.fadeOut(200, function () {
                        $(this).remove();
                        wanna_gifts.updateTable();
                    })
                    $.get('/wannaGifts/delete/id/'+id);
                },
                "Отмена" : function() {
                    $('#trash-dialog').dialog("close");
                    item.removeClass('active');
                }
            },
            'close' : function () {
                item.removeClass('active');
            }
        });
        return false;
    },
    updateTable : function () {
        var items = $('.gifts_table tr[class^=item]');

        if (items.size() == 0) {
            $('.thead').hide();
            $('.js_empty_item').show();
        }
    },
    hideMessage : function () {
        setTimeout(function () {
            $('#announce_msg').slideUp(300, 'easeInQuad');
        }, 1000);
    }
};