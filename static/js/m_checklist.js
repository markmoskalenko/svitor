/* Dmitry Marinin 2012 */

var checklist = {
    counter : {
        all : 0,
        done : 0,
        todo : 0,
        trash: 0
    },
    init : function (active_folder) {
        checklist.counter.all = parseInt($('.all_counter').eq(0).text());
        checklist.counter.done = parseInt($('.done_counter').eq(0).text());
        checklist.counter.todo = parseInt($('.todo_counter').eq(0).text());
        checklist.counter.trash = parseInt($('.trash_counter').eq(0).text());

        checklist.active_folder = active_folder;
    },
    changeFolder : function (link, id, new_folder) {
        if (new_folder == 'todo' || new_folder == 'done')
        {
            var folder = 'done';
            if ($(link).val() == 'done')
                folder = 'todo';

            $.get('/checklist/setStatus/', 'id='+ id +'&folder=' + folder);

            if (folder == 'done') {
                $(link).attr('checked', true);
                $(link).val(folder);
                $('#task_'+id+' .task_desc').css('text-decoration', 'line-through');
                checklist.counter.done++;
                checklist.counter.todo--;
            } else {
                $(link).attr('checked', false);
                $(link).val(folder);
                $('#task_'+id+' .task_desc').css('text-decoration', 'none');
                checklist.counter.done--;
                checklist.counter.todo++;
            }
            checklist.updateCounters();
        }
        else
        {
            if (new_folder == 'restore')
                new_folder = 'todo';

            $('#task_'+id).addClass('active');
            $('#'+new_folder+'-dialog').dialog({
                'modal'   : true,
                'width' : 400,
                'resizable' : false,
                'buttons' : {
                    "Да" : function() {
                        if ($('#task_'+id+' input[type=checkbox]').val() == 'done')
                            checklist.counter.done--;

                        if ($('#task_'+id+' input[type=checkbox]').val() == 'todo')
                            checklist.counter.todo--;

                        if (new_folder == 'trash') {
                            checklist.counter.all--;
                            checklist.counter.trash++;
                        }
                        else if (new_folder == 'todo') {
                            checklist.counter.all++;
                            checklist.counter.todo++;
                            checklist.counter.trash--;
                        }
                        $('#'+new_folder+'-dialog').dialog("close");
                        $('#task_'+id+' td').animate({"border-top": "0", "border-bottom": "0"}, 200);
                        $('#task_'+id).fadeOut(200, function () {
                            checklist.updateFolders();
                            $('#task_'+id).remove();
                        });
                        $.get('/checklist/setStatus/id/'+id+'/folder/'+new_folder);

                        checklist.hideCategoryIfEmpty(id);
                        checklist.updateCounters();
                    },
                    "Отмена" : function() {
                       $('#'+new_folder+'-dialog').dialog("close");
                       $('#task_'+id).removeClass('active');
                     }
                },
                'close' : function () {
                    $('#task_'+id).removeClass('active');
                }
            });
            return false;
        }
    },
    deleteTask : function (link, id) {
        checklist.counter.trash--;
        checklist.updateCounters();

        $('#task_'+id+' td').animate({"border-top": "0", "border-bottom": "0"}, 200);
        $('#task_'+id).fadeOut(200, function () {
            checklist.updateFolders();
            $('#task_'+id).remove();
        });

        $.get('/checklist/delete/id/'+id);
        checklist.hideCategoryIfEmpty(id);
    },
    cleanTrash : function (link) {
        $(link).hide();
        checklist.counter.trash = 0;
        checklist.updateCounters();

        var show_empty_text = true;
        $('.task td').animate({"border-top": "0", "border-bottom": "0"}, 200);
        $('.task').fadeOut(200, function () {
            if (show_empty_text) {
                checklist.updateFolders();
                show_empty_text = false;
            }
        });

        $.get('/checklist/cleanTrash/');
        $('.checklist_table tr[class^=thead]').hide().remove();
    },
    addTask : function () {
	    upBox.ajaxLoad('/checklist/add/');
    },
	editTask : function (id) {
		upBox.ajaxLoad('/checklist/edit/id/'+id);
	},
    updateCounters : function() {
        $('.all_counter').text(checklist.counter.all);
        $('.done_counter').text(checklist.counter.done);
        $('.todo_counter').text(checklist.counter.todo);
        $('.trash_counter').text(checklist.counter.trash);
    },
    updateFolders : function () {
        var active_folder = checklist.active_folder;

        if (active_folder == 'all' && checklist.counter.done == 0 && checklist.counter.todo == 0) {
            $('.js_empty_item').show();
        }

        else if (active_folder == 'done' && checklist.counter.done == 0) {
            $('.js_empty_item').show();
        }

        else if (active_folder == 'todo' && checklist.counter.todo == 0) {
            $('.js_empty_item').show();
        }

        else if (active_folder == 'trash' && checklist.counter.trash == 0) {
            $('.js_empty_item').show();
            $('.checklist_clean_trash').hide();
        }
    },
    hideCategoryIfEmpty : function (id) {
        var link = $('#task_'+id);

        var next = link.next('tr[class^=task]');
        var prev = link.prev('tr[class^=task]');

        if (next.size() == 0 && prev.size() == 0)  {
            link.prev('tr[class^=thead]').find('th').animate({"border-top": "0", "border-bottom": "0"}, 100);
            link.prev('tr[class^=thead]').fadeOut(200, function () {
                link.prev('tr[class^=thead]').remove();
            });
        }
    },
    showTemplates : function () {
        upBox.ajaxLoad('/checklist/templates');
    },
    loadTemplates : function (trigger) {
        var type_id = $(trigger).val();

        if (type_id != '') {
            $('#tempLoader').html($.ajaxLoader());
            $.post('/checklist/templates/type_id/'+type_id, function (re) {
                $('#templates').html(re);
                $('#tempLoader').html('');
                $.fancybox.update();
            });
        } else {
            $('#templates').html('');
        }
        $.fancybox.update();
    },
    selectTempTask : function (trigger) {
        var count = $('.b-upbox').find(':checkbox:checked').not('input[name=select_all]').length;
        var button = $('#add_selected_task button');
        if (count > 0) {
            button.attr('disabled', false);
        } else {
            button.attr('disabled', true);
        }
        $('#selected_task_count span').html(plural_str(count, 'Выбрана', 'Выбрано', 'Выбрано') + ' <b>' + count + '</b> ' + plural_str(count, 'задача', 'задачи', 'задач'));
    },
    sendTempForm : function (trigger) {
        var data = $('.checklist_templates :checkbox').not(':input[name=select_all]').serialize();
        var button_old_content = $(trigger).html();

        $(trigger).html($.ajaxLoader()).attr('disabled', true);
        $('.checklist_templates :checkbox').attr('disabled', true);
        $.ajax({
			type: "GET",
			url: "/checklist/addByTemplates",
			data: data
			}).done(function( msg ) {
			if (msg == 'ok') {
                location.reload();
				} else {
                alert('Не удалось добавить выбранные задачи. Попробуйте еще раз.');
                $(trigger).html(button_old_content).attr('disabled', false);
                $('.checklist_templates :checkbox').attr('disabled', false);
			}
		});
        /*$.post('/checklist/addByTemplates/', { 'template': 3 }, function(re){
            if (re == 'ok') {
                location.reload();
				} else {
				alert(re);
                alert('Не удалось добавить выбранные задачи. Попробуйте еще раз.');
                $(trigger).html(button_old_content).attr('disabled', false);
                $('.checklist_templates :checkbox').attr('disabled', false);
			}
		});*/
    }
};