/* Dmitry Marinin 2012 */

var checklist_form = {

    sendForm : function (trigger, act) {
        var data = $('#checklistForm form').serialize();

        var button_text = $(trigger).html();
        $(trigger).html($.ajaxLoader()).attr('disabled', true);

        if (data == '')
            data = $('.upbox .content').serialize();

        $.post('/checklist/' + act, data, function (re) {

            var response = $.parseJSON(re);

            if (!empty(response.msg) && response.msg == 'ok') {
                if (!empty(response.task_id)) {
                    upBox.ajaxLoad('/budget/add/task_id/' + response.task_id);
                } else {
                    location.reload();
                }
            }
            else if (!empty(response.msg)) {
                $('#msg').html(response.msg).fadeIn(200);
            }
            $(trigger).html(button_text).attr('disabled', false);
        });
    },
    noteSwitch : function (trigger) {
        var display = $('#box_note').css('display');
        if (display == 'none') {
            $(trigger).text('Скрыть примечание');
            $('#box_note').fadeIn(200);
        } else{
            $(trigger).text('Показать примечание');
            $('#box_note').hide();
        }
    }
};