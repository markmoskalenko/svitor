var budget_form = {
    data : {
        amount_plan : 0,
        amount_fact : 0,
        paid_amount : 0,
        currency : 'р'
    },
    init : function (params) {
        budget_form.data.amount_plan = parseInt(params.plan);
        budget_form.data.amount_fact = parseInt(params.fact);
        budget_form.data.paid_amount = parseInt(params.paid);
        budget_form.data.currency = params.currency;
        budget_form.updateCounters();
    },
    setAmountPlan : function (trigger) {
        budget_form.data.amount_plan = budget_form.filterValue(trigger);
        budget_form.updateCounters();
    },
    setAmountFact : function (trigger) {
        budget_form.data.amount_fact = budget_form.filterValue(trigger);
        budget_form.updateCounters();
        budget_form.checkPaidAmount();
    },
    setPaidAmount : function (trigger) {
        budget_form.data.paid_amount = budget_form.filterValue(trigger);
        budget_form.updateCounters();
        budget_form.checkPaidAmount();
    },
    selectSum : function (trigger) {
        if ($(trigger).val() == '0')
            $(trigger).val('');
    },
    checkPaidAmount : function () {
        if (budget_form.data.paid_amount > budget_form.data.amount_fact) {
            $('#UserBudget_paid_amount').addClass('error');
            $('#paid_error').slideDown(200);
            $('#send_button').attr('disabled', true);
        } else {
            $('#UserBudget_paid_amount').removeClass('error');
            $('#paid_error').slideUp(200);
            $('#send_button').attr('disabled', false);
        }
    },
    filterValue : function (trigger) {
        var new_value = $(trigger).val();
        if (new_value != '0') {
            var new_value = new_value.replace(/\D+/, '').replace(/^0{1,}/, '');
            $(trigger).val(new_value);

            if (empty(new_value))
                new_value = 0;
        }
        return parseInt(new_value);
    },
    updateCounters : function () {
        var balance = Math.round(budget_form.data.amount_fact - budget_form.data.paid_amount);
        if (balance < 0)
            balance = 0;

        $('#balance_counter').text(balance);

        var result = Math.round(budget_form.data.amount_plan - budget_form.data.amount_fact);

        if ((budget_form.data.amount_plan != 0 || budget_form.data.amount_fact != 0) && budget_form.data.amount_plan != budget_form.data.amount_fact) {
            if (result >= 0) {
                if (result == 0) {
                    $('#result_counter').removeClass('budget_result_minus').removeClass('budget_result_plus');
                } else {
                    $('#result_counter').removeClass('budget_result_minus').addClass('budget_result_plus');
                }
                $('#result_counter').html('экономия <b>' + result + '</b>' + budget_form.data.currency);
            }
            else if (result < 0) {
                $('#result_counter').removeClass('budget_result_plus').addClass('budget_result_minus').html('перерасход <b>' + result + '</b>' + budget_form.data.currency);
            }
        }
        // default
        else {
            $('#result_counter').removeClass('budget_result_plus').removeClass('budget_result_minus').html('экономия <b>0</b>' + budget_form.data.currency);
        }
    },
    noteSwitch : function (trigger) {
        var display = $('#box_note').css('display');
        if (display == 'none') {
            $(trigger).text('Скрыть примечание');
            $('#box_note').fadeIn(200);
        } else{
            $(trigger).text('Показать примечание');
            $('#box_note').hide();
        }
    },
    sendData : function (trigger, act) {
        var data = $('#budgetForm form').serialize();
        upBox.sendForm(trigger, '/budget/' + act, data);
    }
};