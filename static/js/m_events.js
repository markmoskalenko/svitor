/* Dmitry Marinin 2012 */

var events = {
    url: document.location,
    check : function (item,param,event) {
        var url = this.url + '';
        var urlVar = url.indexOf('/products/');
        if(urlVar != -1){
            urlVar = url.indexOf('/'+param+event+'/');
            if(urlVar != -1){
                urlVar = urlVar+4;
                if(item){
                    document.location = url.slice(0,urlVar)+'0'+url.slice(urlVar+1);
                } else {
                    document.location = url.slice(0,urlVar)+'1'+url.slice(urlVar+1);
                }
            }else{
                if(item){
                    document.location = url+'/'+param+event+'/0';
                } else {
                    document.location = url+'/'+param+event+'/1';
                }
            }
        }else{
            document.location = url+'/products/'+param+event+'/1';
        }
    },
    checkGeo : function (param,event) {
        var url = this.url + '';

        var urlVar = url.indexOf('/products/');
        if(urlVar != -1){
            var par = '/'+param;
            urlVar = url.indexOf(par);
            var next = url.indexOf('/',urlVar+4);
            if(urlVar != -1){
                if(Number(url.slice(next)) === NaN){
                    document.location = url.slice(0,urlVar)+'/'+param+'/'+event+url.slice(next);
                }else{
                    document.location = url.slice(0,urlVar)+'/'+param+'/'+event;
                }
            }else{
                document.location = url+'/'+param+'/'+event;
            }
        }else{
            document.location = url+'/products/'+param+'/'+event;
        }
    },
    checkOther : function (item,param) {
        var url = this.url + '';
        var urlVar = url.indexOf('/products/');
        if(urlVar != -1){
            document.location = url+'/'+param+'/'+item;
        }else{
            document.location = url+'/products/'+param+'/'+item;
        }
    },
    removeFilter:  function (item) {
        var url = this.url + '';
        item = encodeURI(item);
        var urlVar = url.indexOf(item);
        if(urlVar != -1){
            var newUrl = url.slice(0,urlVar)+url.slice(urlVar+item.length);
            if(newUrl == 'http://'+this.url.host+'/event/products'){
                document.location = 'http://'+this.url.host+'/event';
            }else{
                document.location = newUrl;
            }
        }
    },
    removeAllFilter:  function () {
        var url = this.url.host;
        document.location = 'http://'+url+'/event';
    },
    showMulty : function (id) {
        $('#'+id).find('.apply_multy').hide();
        $('#'+id).find('.show_multy').show();
        $('.attr :input[type=checkbox]').attr('checked', false).hide();
        $('#'+id+' :input[type=checkbox]').fadeIn(100);

        $('#'+id+' .show_multy').hide();
        $('#'+id+' .apply_multy').show();
    },
    applyMulty : function (id) {
        var  data = '';
        $('#'+id+' :input[type=checkbox]:checked').each(function(){
            data += '/'+$(this).attr("name")+'/'+$(this).attr("value");
        });
        if (data == '')
            return false;

        var url = this.url + '';
        var urlVar = url.indexOf('/products/');
        if(urlVar != -1){
           document.location = url+data;
        }else{
            document.location = url+'/products'+data;
        }
    },
    addRegion : function() {

        var country = $("#country option:selected").val();
        this.checkGeo('c',country);
    },
    addSity : function() {
        var sity = $("#region option:selected").val();
        this.checkGeo('g',sity);
    },
    addISity : function() {
        var sity = $("#sity option:selected").val();
        this.checkGeo('i',sity);
    }
};