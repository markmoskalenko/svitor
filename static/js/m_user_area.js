/* Dmitry Marinin 2012 */

var  user_area = {
    data : {
        id : '#user',
        country_id : 0,
        city_id : 0,
        region_id : 0,
        metro_id : 0
    },
    init : function (params) {
        if (!empty(params.id)) {
            user_area.data.id = params.id;
        }

        if (!empty(params.country)) {
            user_area.data.country_id = params.country;
            $(user_area.data.id+'_country select option[value='+user_area.data.country_id+']').attr('selected', true);
            user_area.getRegions(user_area.data.country_id);
        }

        if (!empty(params.region)) {
            user_area.data.region_id = params.region;
            user_area.getCities(user_area.data.region_id);
        }

        if (!empty(params.city)) {
            user_area.data.city_id = params.city;
            user_area.getMetro(user_area.data.city_id);
        }

        if (!empty(params.metro))
            user_area.data.metro_id = params.metro;

        user_area.updateData();
    },
    select : function () {
        upBox.ajaxLoad('/area/select/', true);
    },
    getRegions : function (id, select) {
        $(user_area.data.id+'_region').show();
        $(user_area.data.id+'_region_loader').html($.ajaxLoader());
        $.get('/area/get_regions/country/'+id, function (response) {
            $(user_area.data.id+'_region select').html(response);
            if (user_area.data.region_id != 0 && user_area.data.region_id != '')
                $(user_area.data.id+'_region select option[value='+user_area.data.region_id+']').attr('selected', true);

            $(user_area.data.id+'_region_loader').html('');
        });
    },
    getCities : function (id) {
        $(user_area.data.id+'_city').show();
        $(user_area.data.id+'_city_loader').html($.ajaxLoader());
        $.get('/area/get_cities/region/'+id, function (response) {
            $(user_area.data.id+'_city select').html(response);
            if (user_area.data.city_id != 0 && user_area.data.city_id != '')
                $(user_area.data.id+'_city select option[value='+user_area.data.city_id+']').attr('selected', true);

            $(user_area.data.id+'_city_loader').html('');
        });
    },
    getMetro : function (id) {
        $(user_area.data.id+'_metro_loader').html($.ajaxLoader());
        $.get('/area/get_metro_stantions/city/'+id, function (response) {
            if (response != 0) {
                $(user_area.data.id+'_metro').show();
                $(user_area.data.id+'_metro select').html(response);
                if (user_area.data.metro_id != 0 && user_area.data.metro_id != '')
                    $(user_area.data.id+'_metro select option[value='+user_area.data.metro_id+']').attr('selected', true);

            } else {
                user_area.hideMetro();
            }
            $(user_area.data.id+'_metro_loader').html('');
        });
    },
    selectCountry : function (link) {
        user_area.data.country_id = $(link).val();
        user_area.hideCity();
        user_area.hideMetro();
        user_area.data.region_id = 0;
        user_area.getRegions(user_area.data.country_id);
        user_area.updateData();
    },
    selectRegion : function (link) {
        user_area.data.region_id = $(link).val();
        user_area.hideMetro();
        user_area.data.city_id = 0;
        user_area.getCities(user_area.data.region_id);
        user_area.updateData();
    },
    selectCity : function (link) {
        user_area.data.city_id = $(link).val();
        user_area.getMetro(user_area.data.city_id);
        user_area.updateData();
    },
    hideRegion : function () {
        user_area.data.region_id = 0;
        $(user_area.data.id+'_region select option[value=""]').attr('selected', true);
        $(user_area.data.id+'_region').hide();
    },
    hideCity : function () {
        user_area.data.city_id = 0;
        $(user_area.data.id+'_city select option[value=""]').attr('selected', true);
        $(user_area.data.id+'_city').hide();
    },
    hideMetro : function () {
        user_area.data.metro_id = 0;
        $(user_area.data.id+'_metro select option[value=""]').attr('selected', true);
        $(user_area.data.id+'_metro').hide();
    },
    updateData : function () {
        if (user_area.data.country_id == '' || user_area.data.country_id == 0)
            user_area.hideRegion();

        if (user_area.data.region_id == 0 || user_area.data.region_id == '')
            user_area.hideCity();

        if (user_area.data.city_id == 0 || user_area.data.city_id == '')
            user_area.hideMetro();
    },
    saveInfo : function (link) {
        var data = $('.b-upbox form').serialize();
        var text = $(link).text();
        $(link).html($.ajaxLoader()).attr('disabled', true);
        $.post('/area/update/', data, function (response) {
            if (response == 'ok') {
                location.reload();
            } else {
                $(link).html(text).removeAttr('disabled');
                alert(response);
            }
        })
    }
};