var usite = {
    data : {
        site_id : 0,
        template_id : 0,
        style_id : 0
    },

    init : function (params) {
        usite.data.site_id = params.site_id;
        usite.data.template_id = params.template_id;
        usite.data.style_id = params.style_id;

        $('#'+usite.data.template_id+'_'+usite.data.style_id).click();
    },

    hideMessage : function () {
        setTimeout(function () {
            $('#msg').slideUp(300, 'easeInQuad');
        }, 1000);
    },

    showPreview : function (trigger, template, style, selected) {
        $(trigger).parent('div').find('img').removeClass('active');
        $(trigger).addClass('active');
        var preview_img = '/static/usite/'+template['name']+'/'+style['name']+'/preview.jpg';
        var priview_link = '/s'+usite.data.site_id+'?template='+template['id']+'&style='+style['id'];
        var actions = '<a href="'+priview_link+'" class="preview_link" target="_blank">посмотреть</a>  | ';

        if (usite.data.template_id == template['id'] && usite.data.style_id == style['id']) {
            actions += '<b class="selected">выбрано</b><a style="display: none;" class="select" onclick="usite.selectTemplate(this, '+template['id']+', '+style['id']+'); return false;">выбрать</a>';
        } else {
            actions += '<b style="display: none;" class="selected">выбрано</b><a class="select" onclick="usite.selectTemplate(this, '+template['id']+', '+style['id']+'); return false;">выбрать</a>';
        }

        $('#tmp_'+template['id']+ ' img').attr('src', preview_img);
        $('#tmp_'+template['id']+ ' .u_site_design_preview_actionbar').html(actions);
    },

    selectTemplate : function (trigger, template, style) {
        usite.data.template_id = template;
        usite.data.style_id = style;

        $('.u_site_design_preview_actionbar .selected').hide();
        $('.u_site_design_preview_actionbar .select').show();
        $(trigger).hide().prev('b').show();

        $.post('/site/design', '&UserSite[template_id]='+template+'&UserSite[template_style_id]='+style, function (re) {
            if (re == 'ok') {
                $('#msg').html('<div class="msg_success">Изменения успешно сохранены.</div><br>').slideDown(300);
                usite.hideMessage();
            } else {
                alert('Не удалось выбрать этот шаблон.');
            }
        });
    },



    addGift : function () {
        upBox.ajaxLoad('/site/addGift/');
    },
    editGift : function (id) {
        upBox.ajaxLoad('/site/editGift/id/'+id);
    },
    sendGiftForm : function (trigger, act) {
        var data = $('#giftForm form').serialize();
        upBox.sendForm(trigger, '/site/' + act, data);
    },
    uploadGiftImg : function (button) {
        var old_thumbs = $('#image_thumbs').html();

        new AjaxUpload(button, {
            action : '/file/uploadImg',
            name : 'image',
            onSubmit : function (file, ext) {
                $('#image_thumbs').html($.ajaxLoader());
                button.hide();
            },
            onComplete : function (file, response) {
                var img = $.parseJSON(response);

                if (img.status) {
                    if (img.status == 'ok') {
                        $('#img_id').val(img.img_id);
                        $('#img_filename').val(img.file_name);
                        $('#image_thumbs').html('<img src="/'+img.path+'/medium/'+img.file_name+'"> <img src="/'+img.path+'/small/'+img.file_name+'">');
                        old_thumbs = $('#image_thumbs').html();
                    } else {
                        alert(img.msg);
                        $('#image_thumbs').html(old_thumbs);
                    }
                } else {
                    alert(response);
                }
                button.show();
            }
        });
    },
    deleteGift : function (id) {
        var item = $('#item_'+id);
        item.addClass('active');
        $('#trash-dialog').dialog({
            'modal'   : true,
            'width' : 400,
            'resizable' : false,
            'buttons' : {
                "Да" : function() {
                    $('#trash-dialog').dialog("close");
                    item.fadeOut(200, function () {
                        $(this).remove();
                        usite.updateGiftsTable();
                    })
                    $.get('/site/deleteGift/id/'+id);
                },
                "Отмена" : function() {
                    $('#trash-dialog').dialog("close");
                    item.removeClass('active');
                }
            },
            'close' : function () {
                item.removeClass('active');
            }
        });
        return false;
    },
    updateGiftsTable : function () {
        var items = $('.u_site_gifts_table tr[class^=item]');

        if (items.size() == 0) {
            $('.thead').hide();
            $('.js_empty_item').show();
        }
    }
};