/* Dmitry Marinin 2012 */

var catalog = {
    showMulty : function (id) {
        $('.apply_multy').hide();
        $('.show_multy').show();
        $('.attr :input[type=checkbox]').attr('checked', false).hide();
        $('#attr_'+id+' :input[type=checkbox]').fadeIn(100);

        $('#attr_'+id+' .show_multy').hide();
        $('#attr_'+id+' .apply_multy').show();
    },
    applyMulty : function (trigger, id) {
        var query_string = location.search;
        var data = decodeURIComponent($('#attr_'+id+' :input[type=checkbox]:checked').serialize());

        if (data == '')
            return false;

        if (!empty(query_string))
            location.assign(location.pathname + query_string + '&' + data);
        else
            location.assign(location.pathname + '?' + data);
    },
    moreAttrs : function (trigger) {
        $(trigger).contents('123');
        var box = $('#more_attributes');
        var display = box.css('display');

        if (display == 'none') {
            $(trigger).html('Скрыть дополнительные параметры');
            box.slideDown(500, 'easeInQuad');
        } else{
            $(trigger).html('Показать еще параметры...');
            box.slideUp(300, 'easeInQuad');
        }
    }
};

$(function(){
    $('a.cart').click(function() {
        var $this = $(this);
        if($this.attr('action') == 'remove'){
            $.get('/cart/deleteCart', { 'id': $this.attr('id') }, function(data) {

                var $cart_link = $('.cart_link');

                $cart_link.find('span').text(data);

                if(data == 0){
                    var display = $cart_link.css('display');
                    if (display != 'none') {
                        $cart_link.hide();
                    }
                }
                $this.text('В корзину');
                $this.attr('action', 'add');
            });
        }else if($this.attr('action') == 'add'){
            $.get('/cart/addCart', { 'id': $this.attr('id') }, function(data) {

                if(data > 0){

                    var $cart_link = $('.cart_link');

                    $cart_link.find('span').text(data);

                    var display = $cart_link.css('display');
                    if (display == 'none') {
                        $cart_link.show();
                    }
                }

                $this.text('Удалить из корзины');
                $this.attr('action', 'remove');
            });
        }
        return false;
    });
});