/* Dmitry Marinin 2012 */

var vendor = {
    addCat : function () {
        upBox.ajaxLoad('/vendors/addCategory/');
    },
    editCat : function (id) {
        upBox.ajaxLoad('/vendors/editCategory/id/'+id);
    },
    deleteCat : function (id) {
        $('#delete-dialog').dialog({
            'modal'   : true,
            'width' : 400,
            'resizable' : false,
            'buttons' : {
                "Да" : function() {
                    $('#cat_'+id).parent('.category').hide(300, function () { $(this).remove(); });
                    $('#delete-dialog').dialog("close");
                    $.get('/vendors/deleteCategory/id/'+id);
                },
                "Отмена" : function() {
                    $('#delete-dialog').dialog("close");
                }
            }
        });
        return false;
    },
    getCategories : function (trigger, id) {
        upBox.ajaxLoad('/vendors/getCategories/vendor_id/'+id);
    },
    addToTeam : function (trigger, id, cat_id) {
        $.fancybox.close();

        // from profile
        var profile_buttons = $('.user_profile .buttons .add_toteam .add_vendor');
        var old_profile_buttons = profile_buttons.html();

        if (profile_buttons.length > 0) {

            profile_buttons.html($.ajaxLoader());

            $.get('/vendor/team/', 'act=add&id='+id+'&cat_id='+cat_id, function (re) {
                if (re == 'ok') {
                    profile_buttons.html('<div class="status">Эта компания в вашей команде (<a onclick="vendor.removeFromTeam(this, ' + id + '); return false;">удалить из команды</a>)</div>');
                } else {
                    profile_buttons.html(old_profile_buttons);
                    alert(re);
                }
            });
        }
        // from user vendors_favorites or products
        else {
            var box = $('#vendor_'+id+ ' .add_toteam');
            var old_box = box.html();

            box.html($.ajaxLoader());

            $.get('/vendor/team/', 'act=add&id='+id+'&cat_id='+cat_id, function (re) {
                if (re == 'ok') {
                    box.fadeOut(200, function () {
                        box.html('<b>(в команде)</b>').fadeIn(200);
                    });
                } else {
                    box.html(old_box);
                    alert(re);
                }
            });
        }
    },
    removeFromTeam : function (trigger, id, cat_id) {
        // from user vendors_team
        if (parseInt(cat_id) > 0)
        {
            $('#removeFromTeam-dialog').dialog({
                'modal' : true,
                'width' : 400,
                'resizable' : false,
                'buttons' : {
                    "Да" : function() {
                        $('#removeFromTeam-dialog').dialog("close");

                        var box = $('#vendor_'+id),
                            old_box = box.html(),
                            cat = box.prev('div[class^=category_info]');

                        box.html($.ajaxLoader());

                        $.get('/vendor/team/', 'act=remove&id='+id+'&cat_id='+cat_id, function (re) {
                            if (re == 'ok') {
                                box.fadeOut(200, function () {
                                    cat.fadeIn(200);
                                });
                                box.remove();
                            } else {
                                box.prev('div[class^=category_info]').hide();
                                box.html(old_box).show();
                                alert(re);
                            }
                        });
                    },
                    "Отмена" : function() {
                        $('#removeFromTeam-dialog').dialog("close");
                    }
                }
            });
            return false;
        }
        // from profile
        else {
            if (!confirm('Вы уверены?'))
                return false;

            var profile_vendor_status = $('.status'),
                old_profile_vendor_status = profile_vendor_status.html();

            profile_vendor_status.html($.ajaxLoader());

            $.get('/vendor/team/', 'act=remove&id='+id, function (re) {
                if (re == 'ok') {
                    profile_vendor_status.parent('.add_vendor').html('<div class="button_main">' +
                                                                    '<button type="button" onclick="vendor.getCategories(this, '+id+');">Добавить в команду</button>' +
                                                                    '</div>');
                    profile_vendor_status.remove();
                } else {
                    profile_vendor_status.html(old_profile_vendor_status);
                    alert(re);
                }
            });
        }
    },
    addToFavorite : function (trigger, id) {
        var html_data = $(trigger).html();
	    $(trigger).html($.ajaxLoader());
        $.get('/vendor/favorite/', 'act=add&id='+id, function (re) {
            if (re == 'ok') {
	            $(trigger).html('<span class="vendor-saved vendor_action_links">В избранном</span>');
	            $(trigger).contents().unwrap();
            } else {
                $(trigger).html(html_data);
	            alert(re);
            }
        });
    },
    removeFromFavorite : function (id) {
        $('#removeFromFavorite-dialog').dialog({
            'modal' : true,
            'width' : 400,
            'resizable' : false,
            'buttons' : {
                "Да" : function() {
                    $('#removeFromFavorite-dialog').dialog("close");
                    var box = $('#vendor_'+id).parent('div[class^=category]');
                    box.hide(300);
                    $.get('/vendor/favorite', 'act=remove&id='+id, function (re) {
                        if (re == 'ok') {
                            box.remove();
                        } else {
                            box.stop(true, true).show();
                            alert(re);
                        }
                    });
                },
                "Отмена" : function() {
                    $('#removeFromFavorite-dialog').dialog("close");
                }
            }
        });
        return false;
    },
    editNote : function (trigger, id) {
	    upBox.ajaxLoad('/vendor/userNote/id/'+id);
    },
    saveNote : function (trigger, id) {
        var note_text = $('#UserVendorTeam_note').val();
        var user_note = $('#vendor_'+id+ ' .user_note');
        if (!empty(note_text)) {
            user_note.text('*');
        } else {
            user_note.text('');
        }
        upBox.sendForm(trigger, '/vendor/userNote/id/'+id, 'UserVendorTeam[note]='+note_text, false);
    },
    newMsg : function (trigger, vendor, title) {
	    upBox.ajaxLoad('/messages/new?vid='+vendor + (typeof title == 'undefined' ? '' : '&title='+encodeURIComponent(title))  );
    },
    writeReview : function (trigger, id) {
        upBox.ajaxLoad('/vendor/userReview/id/'+id);
    },
    saveReview : function (trigger, id, reload) {
        var data = $('#userReviewForm form').serialize();
        upBox.sendForm(trigger, '/vendor/userReview/id/'+id, data, reload);
    },
    showTooltip : function (trigger) {
        var tooltip = $(trigger).find('.vendor_tooltip');

        $(trigger).bind('mouseout', function () {
            tooltip.hide();
        });

        tooltip.show();
    }
};