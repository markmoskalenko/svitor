var info = {
    sendComment : function (trigger, post_id) {
        var comment = $('#comment').val();
        if (comment != '') {
            var data = 'Comment[comment]=' + comment + '&Comment[post_id]=' + parseInt(post_id);

            $.post('/post/addComment', data, function (re) {
                var response = $.parseJSON(re);

                if (response.status == 'ok') {
                    $('#user_comment').fadeOut(200, function () {
                        $(this).html('<span class="msg_success">Спасибо! Ваш комментарий будет опубликован после проверки модератором.</span><br>').fadeIn(200);
                    });
                } else {
                    $('#comment_error').html(response.msg);
                }
            });
        } else {
            $('#comment_error').html('<div class="errorMessage">&darr; комментарий не может быть пустым</div>');
        }
    },
    removeComment : function (id) {
        $('#trash-comment-dialog').dialog({
            'modal'   : true,
            'width' : 400,
            'resizable' : false,
            'buttons' : {
                "Да" : function() {
                    $('#trash-comment-dialog').dialog("close");
                    $('#comment_'+id).addClass('magic_button');
                    $('#comment_'+id+' .comment_text').html('<div class="comment_deleted">Пользователь удалил свой комментарий...</div>');
                    $('#comment_'+id+' .comment_info').remove();
                    $.get('/post/removeComment/id/'+id);
                },
                "Отмена" : function() {
                    $('#trash-comment-dialog').dialog("close");
                }
            }
        });
        return false;
    }
};