/* Dmitry Marinin 2012 */

var messages = {
    select : function (trigger) {
        var count = $(':checkbox:checked').not('input[name=select_all]').length;
        if (count > 0) {
            $('.messages_action_buttons').slideDown(200);
        } else {
            $('.messages_action_buttons').slideUp(200);
        }
        $('.selected_msg_count').html('<b>'+count+'</b> '+plural_str(count, 'сообщение', 'сообщения', 'сообщений'));
    },
    sendReply : function (trigger, msg_id) {
        if ($('#reply_text_'+msg_id).val() == '') {
            $('#reply_text_'+msg_id).focus();
            return false;
        }

        var old_button_content = $(trigger).html();
        $(trigger).html($.ajaxLoader()).attr('disabled', true);

        $('#error_box').slideUp(200);

        $.post('/messages/reply/id/'+msg_id, 'message='+$('#reply_text_'+msg_id).val(), function (re) {
            if (re == 'ok') {
                location.assign('/messages');
            } else {
                $(trigger).html(old_button_content).removeAttr('disabled');
                $('#error_box').html(re).slideDown(300);
            }
        });
    },
    deleteSelected : function (trigger, folder) {
        $(trigger).html($.ajaxLoader()).attr('disabled', true);
        var data = $(':checkbox').not('input[name=select_all]').serialize();

        $.post('/messages/deleteSelected/folder/'+folder, data, function (re) {
            location.reload();
        })
    },
    delete : function (trigger, id) {
        $(trigger).html($.ajaxLoader());
        $.get('/messages/delete/id/'+id, function (re) {
            location.assign('/messages');
        });
    }
};