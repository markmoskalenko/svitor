/* Dmitry Marinin 2012 */

var budget = {
    data : {
        currency : ''
    },

    init : function (params) {
        budget.data.currency = params.currency;
    },

	addNew : function () {
		upBox.ajaxLoad('/budget/add/');
	},

	editItem : function (id) {
		upBox.ajaxLoad('/budget/edit/id/'+id);
	},

	deleteItem : function (id) {
		$('#item_'+id).addClass('active');
		$('#trash-dialog').dialog({
			'modal'   : true,
			'width' : 400,
			'resizable' : false,
			'buttons' : {
				"Да" : function() {
					$('#trash-dialog').dialog("close");
                    $('#item_'+id+' td').animate({"border-top": "0", "border-bottom": "0"}, 200);
                    $('#item_'+id).fadeOut(200, function () {
                        $(this).remove();
                        budget.updateTable();
                        budget.updateTotalInfo();
                    });
                    budget.hideCategoryIfEmpty(id);
					$.get('/budget/delete/id/'+id);
				},
				"Отмена" : function() {
					$('#trash-dialog').dialog("close");
					$('#item_'+id).removeClass('active');
				}
			},
			'close' : function () {
				$('#item_'+id).removeClass('active');
			}
		});

		return false;
    },

    hideCategoryIfEmpty : function (id) {
        var link = $('#item_'+id);

        var next = link.next('tr[class^=item]');
        var prev = link.prev('tr[class^=item]');

        if (next.size() == 0 && prev.size() == 0)  {
            link.prev('tr[class^=thead]').find('th').animate({"border-top": "0", "border-bottom": "0"}, 100);
            link.prev('tr[class^=thead]').fadeOut(200, function () {
                link.prev('tr[class^=thead]').remove();
            });
        }
    },

    updateTable : function () {
        var items = $('.budget_table tr[class^=item]');

        if (items.size() == 0) {
            $('.thead_help').hide();
            $('.js_empty_item').show();
        }
    },

    editVal : function (trigger, id, value_type) {
        var item_id = id;
        if (isNaN(item_id))
            return false;

        $(trigger).hide();
        var input = $(trigger).parents('td').eq(0).find('input');
        var old_value = input.val();

        var end = input.val().length;
        input.fadeIn(200).focus().selectRange(end, end);

        $(input).keyup(function (event) {
            if (event.keyCode == 13 || event.keyCode == 27)
                input.blur();
        });

        $(input).one('blur', function () {
            budget.saveValue(trigger, item_id, input, old_value, value_type);
        });
    },

    saveValue : function (trigger, item_id, input, old_value, value_type) {
        input.hide();
        var new_value = input.val().replace(/^\s+/, '').replace(/\s+$/, '');

        if (value_type == 'int' && new_value != '0') {
            old_value = old_value.replace(/\D+/, '').replace(/^0{1,}/, '');
            new_value = new_value.replace(/\D+/, '').replace(/^0{1,}/, '');

            if (new_value > 2147483647) // 32bit
                new_value = 2147483647;
        }

        if (empty(old_value))
            old_value = '0';

        if (new_value == '') {
            new_value = old_value;
        }

        input.val(new_value);
        $(trigger).text(new_value).show();

        if (new_value != old_value) {
            var data = 'id='+item_id+'&name='+input.attr('name')+'&value='+new_value;
            $.post('/budget/save_value/', data, function (response) {
                if (response != 'ok')
                {
                    input.val(old_value);
                    $(trigger).text(old_value);
                    alert(response);
                }
                else
                {
                    $(trigger).append('<span class="budget_saved_info">Сохранено</span>');
                    $('.budget_saved_info').one('mouseover', function () {
                        $('.budget_saved_info').hide().remove();
                    });

                    setTimeout(function () {
                        $('.budget_saved_info').fadeOut(100, function() { $(this).remove(); });
                    }, 700);

                    old_value = input.val();

                    budget.updateTotalInfo();
                    budget.updateLeftCounter(item_id);
                }
            });
        }
    },

    checkIntValue : function (trigger) {
        var keyCode = window.event.keyCode;   //left           //up             //right          //down           // delete        //backspace     //numpad 0      //enter
        if ((keyCode < 48 || keyCode > 57) && keyCode != 37 && keyCode != 38 && keyCode != 39 && keyCode != 40 && keyCode != 46 && keyCode != 8 && keyCode != 96 && keyCode != 13)
            $(trigger).val(budget.filterIntValue(trigger));
    },

    filterIntValue : function (trigger) {
        var new_value = $(trigger).val().replace(/\D+/, '').replace(/^0{1,}/, '');
        $(trigger).val(new_value);

        new_value = parseInt(new_value);
        if (isNaN(new_value))
            new_value = '';

        return new_value;
    },

    updateLeftCounter : function (id) {
        var item = $('#item_'+id);

        var paid_amount = parseInt(item.find('input[name^=paid_amount]').val());
        var amount_fact = parseInt(item.find('input[name^=amount_fact]').val());

        var value = Math.round(amount_fact - paid_amount);
        item.find('.left').html(value);
    },

    updateTotalInfo : function () {
        var plan_inputs = $('.budget_table :input[name^=amount_plan]');

        var total_plan = 0;
        for (var i = 0; i < plan_inputs.size(); i++)
            total_plan += parseInt(plan_inputs.eq(i).val());

        var fact_inputs = $('.budget_table :input[name^=amount_fact]');
        var total_fact = 0;
        for (var i = 0; i < fact_inputs.size(); i++)
            total_fact += parseInt(fact_inputs.eq(i).val());

        var paid_inputs = $('.budget_table :input[name^=paid_amount]');
        var total_paid = 0;
        for (var i = 0; i < paid_inputs.size(); i++)
            total_paid += parseInt(paid_inputs.eq(i).val());

        if (isNaN(total_plan))
            total_plan = 0;

        if (isNaN(total_fact))
            total_fact = 0;

        if (isNaN(total_paid))
            total_paid = 0;

        $('#total_plan').text(total_plan);
        $('#total_fact').text(total_fact);
        $('#total_paid').text(total_paid);

        var left_value;
        if ((total_plan != 0 || total_fact != 0) && total_plan != total_fact) {
            var result = Math.round(total_plan - total_fact);

            if (result > 0) {
                left_value = '<td>Экономия:</td><td class="r_al"><b class="budget_result_plus">'+result+'</b> ' + budget.data.currency + '.</td>';
            }
            else if (result <= 0) {
                left_value = '<td>Перерасход:</td><td class="r_al"><b class="budget_result_minus">'+result+'</b> ' + budget.data.currency + '.</td>';
            }
        }
        else {
            left_value = '<td>Перерасход:</td><td class="r_al"><b>0</b> ' + budget.data.currency + '.</td>';
        }

        $('#total_left').html(left_value);
    }
}