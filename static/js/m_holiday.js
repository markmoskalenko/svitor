/* Dmitry Marinin 2012 */

var holiday = {
    addNew : function () {
        upBox.ajaxLoad('/holiday/add/');
    },
    goEdit : function (id) {
        upBox.ajaxLoad('/holiday/edit/id/'+id, true);
    },
    setSelected : function (id) {
        location.assign('/dashboard/?holiday='+id);
    },
    sendForm : function (link, act) {
        var data = $('#holidayForm form').serialize();
        upBox.sendForm(link, '/holiday/' + act, data);
    }
}