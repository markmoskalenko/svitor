/* Dmitry Marinin 2012 */

var tab = {
    init : function (params) {
        var name = window.location.hash.substring(1);

        if (!name)
            name = params['default'];

        tab.openTab(name);
    },
    openTab : function (name) {
        tab.closeAllTab();
        window.location.hash = name;
        $('#box_'+name).show();
        $('#tab_'+name).addClass('active');
    },
    closeAllTab : function () {
        $('.b-small_tabs').children('div').hide();
        $('.tabs_button li a').removeClass('active');
    }
};