/* Dmitry Marinin 2012 */

var photos = {

    img_count : 0,
    max_images : 0,

    init : function (img_count, max_images) {
        photos.img_count = img_count;
        photos.max_images = max_images;
        photos.uploadImg($('#addImgButton'));
    },

    uploadImg : function (button) {
        new AjaxUpload(button, {
            action : 'http://'+svitor.mainDomain + '/file/uploadImg',
            name : 'image',
            onSubmit : function (file, ext) {
                $('#loader').html($.ajaxLoader());
            },
            onComplete : function (file, response) {

                var img = $.parseJSON(response);

                if (img.status) {
                    if (img.status == 'ok') {
                        $.post('/photos/', img, function(response) {
                            var user_img = $.parseJSON(response);

                            $('#photos_thumbs').append('<div class="img" id="img_'+user_img.id+'">' +
                                '<a class="upbox-button" rel="upbox-images" href="http://'+svitor.mainDomain+'/'+img.path+'/'+img.file_name+'"><img src="http://'+svitor.mainDomain+'/'+img.path+'/medium/'+img.file_name+'"></a>' +
                                '<a class="delete magic_button" onclick="photos.remove(this, '+user_img.id+'); return false;" onmouseover="tooltip.show(this, \'Удалить\', {position:\'top-right\'});"></a>' +
                                '<br><a onclick="photos.edit(' + user_img.id + '); return false;">редактировать</a>' +
                                '</div>');

                            $('#img_'+user_img.id).hide().show(200);
                            photos.img_count = user_img.img_count;
                            photos.updateCounter();

                            $('#loader').html('');
                        });
                    } else {
                        alert(img.msg);
                    }
                } else {
                    alert(response);
                }
            }
        });
    },

    remove : function (trigger, id) {
        $('#img_'+id).hide(200, function () {
            $('#img_'+id).remove();
        });
        photos.img_count--;

        photos.updateCounter();
        $.get('/photos/delete/id/'+id);
    },

    edit : function (id) {
        upBox.ajaxLoad('/photos/edit/id/'+id);
    },

    save : function (trigger, id) {
        var data = $('#userPhotoEdit form').serialize(),
            button_old_content = $(trigger).html();

        $(trigger).html($.ajaxLoader()).attr('disabled', true);

        $.post('/photos/edit/id/'+id, data, function(response){
            if (response == 'ok') {
                var desc = $('#userPhotoEdit form textarea').val();
                $('#img_'+id).find('.upbox-button').attr('title', desc);
                $.fancybox.close();
            } else {
                $('#userPhotoEdit #msg').html(response);
                $(trigger).html(button_old_content).attr('disabled', false);
            }
        });
    },

    updateCounter : function () {
        $('#images_counter span').html(photos.img_count);

        if (photos.img_count == photos.max_images)
            $('#addImgButton').hide();

        if (photos.img_count < photos.max_images)
            $('#addImgButton').show();
    }
};