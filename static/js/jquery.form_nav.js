﻿var global_formNavigate = true;

(function($){
    $.fn.FormNav = function(message) {
        window.onbeforeunload = confirmExit;
        function confirmExit( event ) {
            if (global_formNavigate == true) {  event.cancelBubble = true;  }  else  { return message;  }
        }
        $(this).find('input').change(function(){
            global_formNavigate = false;
        });
        //to handle back button
        $(this).find('textarea').keyup(function(){
            global_formNavigate = false;
        });
        $(this).find('submit').click(function(){
            global_formNavigate = true;
        });
        $(this).find('button').click(function(){
            global_formNavigate = true;
        });
    }
})(jQuery);