/* Dmitry Marinin 2012 */

var authServicePopup = {
    popup : '',

    show : function (trigger, options) {
        var url, redirect_uri = url = $(trigger).attr('href');

        options = $.extend({
            width: 450,
            height: 380
        }, options);

        var leftPosition = (window.screen.width - options.width) / 2,
            topPosition = (window.screen.height - options.height) / 2;

        if (authServicePopup.popup !== undefined && authServicePopup.popup != '')
            authServicePopup.popup.close();

        url += url.indexOf('?') >= 0 ? '&' : '?';
        if (url.indexOf('redirect_uri=') === -1)
            url += 'redirect_uri=' + encodeURIComponent(redirect_uri) + '&';
        url += 'js';

        authServicePopup.popup = window.open(url, 'auth_services', 'width=' + options.width +
            ',height=' + options.height +
            ',left=' + leftPosition +
            ',top=' + topPosition +
            ',resizable=yes,location=no,directories=no,status=yes,scrollbars=no,toolbar=no,menubar=no');
        authServicePopup.popup.focus();

        return false;
    }
};

$.ajaxLoader = function () {
    return '<img src="/static/images/ajax-loader.gif" alt="Loading..." id="ajaxLoader">';
}

var upBox = {
	ajaxLoad : function (href, modal) {
        if (modal != '')
            modal = false;

        $.fancybox({
            'type' : 'ajax',
            'href' : href,
            'padding' : 0,
            'topRatio' : 0.2,
            'openEffect' : 'none',
            'closeEffect' : 'none',
            'scrolling' : false,
            'modal' : modal,
            'helpers' : {
                overlay : {
                    opacity : 0.3,
                    speedOut : 0
                }
            }
		});
		return false;
	},
    showContent : function (content) {
        $.fancybox({
            'content' : content,
            'padding' : 0,
            'topRatio' : 0.3,
            'openEffect' : 'none',
            'closeEffect' : 'none',
            'helpers' : {
                overlay : {
                    opacity : 0.3,
                    speedOut   : 0
                }
            }
        });
    },
	sendForm : function (button, href, data, reload) {

        if (reload !== false)
            reload = true;

		var old_button_content = $(button).html();
        $(button).html($.ajaxLoader()).attr('disabled', true);

		if (data == '')
			data = $('.upbox form').serialize();

        var msg = $('#msg');

		$.post(href, data, function (re) {
			if (re == 1 || re == 'ok') {
                msg.hide();
				$.fancybox.close();
                if (reload)
				    location.reload();
			} else {
                $(button).html(old_button_content).removeAttr('disabled');
                msg.html(re).fadeIn(200);
			}
		});
	}
};

var selectFun = {
    holiday : function () {
        upBox.ajaxLoad('/holiday/select/');
    },
    wedding : function () {
        upBox.ajaxLoad('/wedding/select/');
    }
};

$(document).ready(function() {
    $('.upbox-button').fancybox({
        prevEffect : 'none',
        nextEffect : 'none',
        closeEffect : 'none',
        openEffect : 'none',
        topRatio : 0.3,
        maxWidth : 916,
        helpers		: {
            title	: { type : 'inside' },
            buttons	: {},
            overlay : { speedOut : 0 }
        }
    });

    $('.upbox-media').fancybox({
        openEffect  : 'none',
        closeEffect : 'none',
        closeEffect : 'none',
        openEffect : 'none',
        topRatio : 0.3,
        padding: 0,
        maxHeight: 460,
        helpers : {
            media : {},
            overlay : { speedOut : 0 }
        }
    });

    $(".upbox-various").fancybox({
        maxWidth	: 800,
        maxHeight	: 500,
        topRatio : 0.3,
        fitToView	: false,
        width		: '70%',
        height		: '70%',
        padding: 0,
        autoSize	: false,
        closeClick	: false,
        openEffect	: 'none',
        closeEffect	: 'none',
        helpers : {
            overlay : {
                opacity : 0.3,
                speedOut : 0
            }
        }
    });

    $('input[type=text], input[type=password], input[type=checkbox], input[type=radio], textarea, select').change(function(){
        $(this).removeClass('error')
        $(this).parent().find('div[class^=errorMessage]').slideUp(200);
    });

    $('[placeholder]').placeholder();

    $('.magic_button').css('opacity','0.5');
    $('.magic_button').hover(function(){
        $(this).stop().animate({opacity: 1}, 300);
    }, function () {
        $(this).stop().animate({opacity: 0.5}, 200);
    });
	
	//slider Carusel
	jQuery("#images_thumbs .slider_center").jCarouselLite({
		btnNext: ".s_next",
		btnPrev: ".s_prev"
	});	
	
	//accordeon
	$('div.nav_wrap h3').append('<span></span>');
	
	$('div.nav_wrap .sub_nav').hide();
	
	if((location.href).search('rating')>0){
		$('div.nav_wrap h3:eq(1)').addClass('active');
		$('div.nav_wrap .sub_nav:eq(1)').show();
	}	

	$('div.nav_wrap h3').click(function() {
		$(this).next('.sub_nav').slideToggle(300).siblings(".sub_nav:visible").slideUp(300);
		$(this).toggleClass("active");
        $(this).siblings("h3").removeClass("active");
	});
	
});


$.fn.selectRange = function(start, end) {
    return this.each(function() {
        if (this.setSelectionRange) {
            this.focus();
            this.setSelectionRange(start, end);
        } else if (this.createTextRange) {
            var range = this.createTextRange();
            range.collapse(true);
            range.moveEnd('character', end);
            range.moveStart('character', start);
            range.select();
        }
    });
};

function empty (mixed_var) {
	var key;

	if (mixed_var === "" || mixed_var === 0 || mixed_var === "0" || mixed_var === null || mixed_var === false || typeof mixed_var === 'undefined') {
		return true;
	}

	if (typeof mixed_var == 'object') {
		for (key in mixed_var) {
			return false;
		}
		return true;
	}

	return false;
}

function plural_str(i, str1, str2, str3) {
	function plural (a){
		if ( a % 10 == 1 && a % 100 != 11 ) {
			return 0
		} else if ( a % 10 >= 2 && a % 10 <= 4 && ( a % 100 < 10 || a % 100 >= 20)) {
			return 1
		} else {
			return 2;
		}
	}

	switch (plural(i)) {
		case 0: return str1;
		case 1: return str2;
		default: return str3;
	}
}

var formHelper = {
    updateCounter : function (trigger, name, max_count) {
        var text = $(trigger).val();
        var text_length = text.length;
        var live_counter = max_count - text_length;

        var textarea = $('#'+name+' input[type=text], #'+name+' textarea');
        var counter = $('#'+name+' .counter');

        if (live_counter >= 0) {
            textarea.removeClass('error');
            counter.html(plural_str(live_counter, 'Остался', 'Осталось', 'Осталось')+' <b>'+live_counter+'</b> '+plural_str(live_counter, 'символ', 'символа', 'символов'));
        } else {
            live_counter = live_counter*(-1);
           textarea.addClass('error');
            counter.html('Размер привышен на <b style="color: red;">'+live_counter+'</b> '+plural_str(live_counter, 'символ', 'символа', 'символов'));
        }
    }
};





(function(jQuery) {                                          
jQuery.fn.jCarouselLite = function(o) {
    o = jQuery.extend({
        btnPrev: null,
        btnNext: null,
        btnGo: null,
        mouseWheel: false,
        auto: false,

        speed: 1500,
        easing: null,

        vertical: false,
        circular: true,
        visible: 7,
        start: 0,
        scroll: 1,

        beforeStart: null,
        afterEnd: null
    }, o || {});

    return this.each(function() {                          

        var running = false, animCss=o.vertical?"top":"left", sizeCss=o.vertical?"height":"width";
        var div = jQuery(this), ul = jQuery("ul", div), tLi = jQuery("li", ul), tl = tLi.size(), v = o.visible;

        if(o.circular) {
            ul.prepend(tLi.slice(tl-v-1+1).clone())
              .append(tLi.slice(0,v).clone());
            o.start += v;
        }

        var li = jQuery("li", ul), itemLength = li.size(), curr = o.start;
        div.css("visibility", "visible");

        li.css({overflow: "hidden", float: o.vertical ? "none" : "left"});
        ul.css({margin: "0", padding: "0", position: "relative", "list-style-type": "none", "z-index": "1"});
        div.css({overflow: "hidden", position: "relative"});

        var liSize = o.vertical ? height(li) : width(li);   
        var ulSize = liSize * itemLength;                   
        var divSize = liSize * v;                           

        li.css({width: li.width(), height: li.height()});
        ul.css(sizeCss, ulSize+"px").css(animCss, -(curr*liSize));

        div.css(sizeCss, divSize+"px");                    

        if(o.btnPrev)
            jQuery(o.btnPrev).click(function() {
                return go(curr-o.scroll);
            });

        if(o.btnNext)
            jQuery(o.btnNext).click(function() {
                return go(curr+o.scroll);
            });

        if(o.btnGo)
            jQuery.each(o.btnGo, function(i, val) {
                jQuery(val).click(function() {
                    return go(o.circular ? o.visible+i : i);
                });
            });

        if(o.mouseWheel && div.mousewheel)
            div.mousewheel(function(e, d) {
                return d>0 ? go(curr-o.scroll) : go(curr+o.scroll);
            });

        if(o.auto)
            setInterval(function() {
                go(curr+o.scroll);
            }, o.auto+o.speed);

        function vis() {
            return li.slice(curr).slice(0,v);
        };

        function go(to) {
            if(!running) {

                if(o.beforeStart)
                    o.beforeStart.call(this, vis());

                if(o.circular) {            
                    if(to<=o.start-v-1) {          
                        ul.css(animCss, -((itemLength-(v*2))*liSize)+"px");
                        curr = to==o.start-v-1 ? itemLength-(v*2)-1 : itemLength-(v*2)-o.scroll;
                    } else if(to>=itemLength-v+1) { 
                        ul.css(animCss, -( (v) * liSize ) + "px" );
                        curr = to==itemLength-v+1 ? v+1 : v+o.scroll;
                    } else curr = to;
                } else {                    
                    if(to<0 || to>itemLength-v) return;
                    else curr = to;
                }                           

                running = true;

                ul.animate(
                    animCss == "left" ? { left: -(curr*liSize) } : { top: -(curr*liSize) } , o.speed, o.easing,
                    function() {
                        if(o.afterEnd)
                            o.afterEnd.call(this, vis());
                        running = false;
                    }
                );
                if(!o.circular) {
                    jQuery(o.btnPrev + "," + o.btnNext).removeClass("disabled");
                    jQuery( (curr-o.scroll<0 && o.btnPrev)
                        ||
                       (curr+o.scroll > itemLength-v && o.btnNext)
                        ||
                       []
                     ).addClass("disabled");
                }

            }
            return false;
        };
    });
};

function css(el, prop) {
    return parseInt(jQuery.css(el[0], prop)) || 0;
};
function width(el) {
    return  el[0].offsetWidth + css(el, 'marginLeft') + css(el, 'marginRight');
};
function height(el) {
    return el[0].offsetHeight + css(el, 'marginTop') + css(el, 'marginBottom');
};

})(jQuery);

// Sticky Plugin v1.0.0 for jQuery
// =============
// Author: Anthony Garand
// Improvements by German M. Bravo (Kronuz) and Ruud Kamphuis (ruudk)
// Improvements by Leonardo C. Daronco (daronco)
// Created: 2/14/2011
// Date: 2/12/2012
// Website: http://labs.anthonygarand.com/sticky
// Description: Makes an element on the page stick on the screen as you scroll
//       It will only set the 'top' and 'position' of your element, you
//       might need to adjust the width in some cases.

(function($) {
    var defaults = {
            topSpacing: 0,
            bottomSpacing: 0,
            className: 'is-sticky',
            wrapperClassName: 'sticky-wrapper',
            center: false,
            getWidthFrom: ''
        },
        $window = $(window),
        $document = $(document),
        sticked = [],
        windowHeight = $window.height(),
        scroller = function() {
            var scrollTop = $window.scrollTop(),
                documentHeight = $document.height(),
                dwh = documentHeight - windowHeight,
                extra = (scrollTop > dwh) ? dwh - scrollTop : 0;

            for (var i = 0; i < sticked.length; i++) {
                var s = sticked[i],
                    elementTop = s.stickyWrapper.offset().top,
                    etse = elementTop - s.topSpacing - extra;

                if (scrollTop <= etse) {
                    if (s.currentTop !== null) {
                        s.stickyElement
                            .css('position', '')
                            .css('top', '');
                        s.stickyElement.parent().removeClass(s.className);
                        s.currentTop = null;
                    }
                }
                else {
                    var newTop = documentHeight - s.stickyElement.outerHeight()
                        - s.topSpacing - s.bottomSpacing - scrollTop - extra;
                    if (newTop < 0) {
                        newTop = newTop + s.topSpacing;
                    } else {
                        newTop = s.topSpacing;
                    }
                    if (s.currentTop != newTop) {
                        s.stickyElement
                            .css('position', 'fixed')
                            .css('top', newTop);

                        if (typeof s.getWidthFrom !== 'undefined') {
                            s.stickyElement.css('width', $(s.getWidthFrom).width());
                        }

                        s.stickyElement.parent().addClass(s.className);
                        s.currentTop = newTop;
                    }
                }
            }
        },
        resizer = function() {
            windowHeight = $window.height();
        },
        methods = {
            init: function(options) {
                var o = $.extend(defaults, options);
                return this.each(function() {
                    var stickyElement = $(this);

                    var stickyId = stickyElement.attr('id');
                    var wrapper = $('<div></div>')
                        .attr('id', stickyId + '-sticky-wrapper')
                        .addClass(o.wrapperClassName);
                    stickyElement.wrapAll(wrapper);

                    if (o.center) {
                        stickyElement.parent().css({width:stickyElement.outerWidth(),marginLeft:"auto",marginRight:"auto"});
                    }

                    if (stickyElement.css("float") == "right") {
                        stickyElement.css({"float":"none"}).parent().css({"float":"right"});
                    }

                    var stickyWrapper = stickyElement.parent();
                    stickyWrapper.css('height', stickyElement.outerHeight());
                    sticked.push({
                        topSpacing: o.topSpacing,
                        bottomSpacing: o.bottomSpacing,
                        stickyElement: stickyElement,
                        currentTop: null,
                        stickyWrapper: stickyWrapper,
                        className: o.className,
                        getWidthFrom: o.getWidthFrom
                    });
                });
            },
            update: scroller
        };

    // should be more efficient than using $window.scroll(scroller) and $window.resize(resizer):
    if (window.addEventListener) {
        window.addEventListener('scroll', scroller, false);
        window.addEventListener('resize', resizer, false);
    } else if (window.attachEvent) {
        window.attachEvent('onscroll', scroller);
        window.attachEvent('onresize', resizer);
    }

    $.fn.sticky = function(method) {
        if (methods[method]) {
            return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
        } else if (typeof method === 'object' || !method ) {
            return methods.init.apply( this, arguments );
        } else {
            $.error('Method ' + method + ' does not exist on jQuery.sticky');
        }
    };
    $(function() {
        setTimeout(scroller, 0);
    });
})(jQuery);