/* Dmitry Marinin 2012 */

$(document).ready(function() {
    $('#User_email').blur(function () {
        registration.checkEmail(this);
    });

    $('#User_accept_terms').change(function(){
        $('#reg_submit').attr('disabled', !$('#User_accept_terms').is(':checked'));
    });
});

var registration = {
    checkEmail : function (link) {
        if ($(link).val() != '') {
            if (/^([a-z0-9_\-]+\.)*[a-z0-9_\-]+@([a-z0-9][a-z0-9\-]*[a-z0-9]\.)+[a-z]{2,4}$/i.test($(link).val())) {
                $('#email_loader').html($.ajaxLoader());
                $.get('/user/registration/', 'check_email='+$(link).val(), function (re) {
                    if (re == 0) {
                        $(link).removeClass('error');
                        $('#email div').slideUp(100);
                    } else if (re == 1) {
                        $(link).addClass('error');
                        $('#email div').html(messages.email_used).slideDown(200);
                    }
                    $('#email_loader').html('');
                });
            } else {
                $(link).addClass('error');
                $('#email div').html(messages.email_incorrect).slideDown(200);
            }
        } else {
            $(link).removeClass('error');
            $('#email div').html('').slideUp(100);
        }
    }
};

var messages = {
    email_incorrect : '&uarr; некорректный E-Mail',
    email_used : '&uarr; такой E-Mail уже кем-то используется',
    password_not_match : '&uarr; пароли не совпадают',
    password_incorrect : '&uarr; пароль должен содержать минимум 6 символов'
}