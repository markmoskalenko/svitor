var cart = {
    editVal : function (trigger, id, value_type) {
        var item_id = id;
        if (isNaN(item_id)){
            return false;
        }

        $(trigger).hide();
        var input = $(trigger).parents('.ajax').eq(0).find('input');
        var old_value = input.val();
        var end = input.val().length;
        input.fadeIn(200).focus().selectRange(end, end);

        $(input).keyup(function (event) {
            if (event.keyCode == 13 || event.keyCode == 27)
                input.blur();
        });

        $(input).one('blur', function () {
            cart.saveValue(trigger, item_id, input, old_value, value_type);
        });
    },

    saveValue : function (trigger, item_id, input, old_value, value_type) {
        input.hide();
        var new_value = input.val().replace(/^\s+/, '').replace(/\s+$/, '');

        if (value_type == 'int' && new_value != '0') {
            old_value = old_value.replace(/\D+/, '').replace(/^0{1,}/, '');
            new_value = new_value.replace(/\D+/, '').replace(/^0{1,}/, '');

            if (new_value > 2147483647) // 32bit
                new_value = 2147483647;
        }

        if (empty(old_value))
            old_value = '0';

        if (new_value == '') {
            new_value = old_value;
        }

        input.val(new_value);

        $(trigger).text(new_value).show();

        if (new_value != old_value) {
            $.post('/vendor_cp/orders/saveAjaxValue/',
                { id: item_id, name: input.attr('name'), value: new_value },
                function (response) {
                    var obj = jQuery.parseJSON(response);
                    if(obj.r == 'ok'){
                        $('#sum').text('Сумма, руб.:'+obj.s);
                        $('#delivery .ajax .delivery_active_value').text(obj.d);
                    }
                });
        }
        $.fn.yiiGridView.update('grid-view');
    },

    checkIntValue : function (trigger) {
        var keyCode = window.event.keyCode;   //left           //up             //right          //down           // delete        //backspace     //numpad 0      //enter
        if ((keyCode < 48 || keyCode > 57) && keyCode != 37 && keyCode != 38 && keyCode != 39 && keyCode != 40 && keyCode != 46 && keyCode != 8 && keyCode != 96 && keyCode != 13)
            $(trigger).val(cart.filterIntValue(trigger));
    },

    filterIntValue : function (trigger) {
        var new_value = $(trigger).val().replace(/\D+/, '').replace(/^0{1,}/, '');
        $(trigger).val(new_value);

        new_value = parseInt(new_value);
        if (isNaN(new_value))
            new_value = '';

        return new_value;
    }
}