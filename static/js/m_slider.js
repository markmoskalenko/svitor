/* Dmitry Marinin 2012 */

var slider = {
    autoTimer : '',

    init : function () {
        slider.startTimer();

        $('.main_slider .text_block').hover(function(){
            slider.stopTimer();
        }, function() {
            slider.startTimer();
        }),

            $('.main_slider .switches li').click(function(){

                slider.startTimer();

                var active_slide = $(this).attr('class').replace('s', '');

                if ($(this).hasClass('active'))
                    return;

                $('.main_slider .slide').fadeOut(300, function(){});
                $('.main_slider .slide_'+active_slide).fadeIn(400);


                $('.main_slider .switches li').removeClass('active');
                $(this).addClass('active');
            });
    },
    startTimer : function() {
        var active_slide = $('.main_slider .switches li.active');

        slider.stopTimer();
        slider.autoTimer = setTimeout(function(){

            if (active_slide.next().index() == -1) {
                $('.main_slider .switches li').eq(0).click();
            } else {
                active_slide.next().click();
            }
        }, 8000);
    },
    stopTimer : function() {
        clearTimeout(slider.autoTimer);
    }
};