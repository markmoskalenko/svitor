routine = {
    addNew : function () {
        upBox.ajaxLoad('/routine/add');
    },
    goEdit : function (id) {
        upBox.ajaxLoad('/routine/edit/id/'+id);
    },
    sendForm : function (trigger, act) {
        var data = $('#routineForm form').serialize();
        upBox.sendForm(trigger, '/routine/' + act, data);
    },
    deleteItem : function (id) {
        var item = $('#item_'+id);
        item.addClass('active');
        $('#trash-dialog').dialog({
            'modal'   : true,
            'width' : 400,
            'resizable' : false,
            'buttons' : {
                "Да" : function() {
                    $('#trash-dialog').dialog("close");
                    item.fadeOut(200, function () {
                        $(this).remove();
                        routine.updateTable();
                    })
                    $.get('/routine/delete/id/'+id);
                },
                "Отмена" : function() {
                    $('#trash-dialog').dialog("close");
                    item.removeClass('active');
                }
            },
            'close' : function () {
                item.removeClass('active');
            }
        });
        return false;
    },
    updateTable : function () {
        var items = $('.routine_table tr[class^=item]');

        if (items.size() == 0) {
            $('.thead').hide();
            $('.js_empty_item').show();
        }
    },
    noteSwitch : function (trigger) {
        var display = $('#box_note').css('display');
        if (display == 'none') {
            $(trigger).text('Скрыть примечание');
            $('#box_note').fadeIn(200);
        } else{
            $(trigger).text('Показать примечание');
            $('#box_note').hide();
        }
    }
};