/* Dmitry Marinin 2012 */

var guest = {
    addNew : function () {
        upBox.ajaxLoad('/guests/add/');
    },

    goEdit : function (id) {
        upBox.ajaxLoad('/guests/edit/id/'+id);
    },

    sendForm : function (trigger, act) {
        var data = $('#guestForm form').serialize();
        upBox.sendForm(trigger, '/guests/' + act, data);
    },

    deleteItem : function (id) {
        var all_count = parseInt($('.all_counter').text());
        var notified_count = parseInt($('.notified_counter').text());
        var item = $('#item_'+id);

        item.addClass('active');
        $('#trash-dialog').dialog({
            'modal'   : true,
            'width' : 400,
            'resizable' : false,
            'buttons' : {
                "Да" : function() {
                    all_count--;

                    if ($('#item_'+id+' :checkbox[checked=checked]').size() != 0)
                        notified_count--;

                    $('#trash-dialog').dialog("close");
                    item.fadeOut(200, function () {
                        $(this).remove();
                        guest.updateTable();
                    })
                    $.get('/guests/delete/id/'+id);
                    $('.all_counter').text(all_count);
                    $('.notified_counter').text(notified_count)
                },
                "Отмена" : function() {
                    $('#trash-dialog').dialog("close");
                    item.removeClass('active');
                }
            },
            'close' : function () {
                item.removeClass('active');
            }
        });
        return false;
    },

    updateTable : function () {
        var items = $('.guests_table tr[class^=item]');

        if (items.size() == 0) {
            $('.thead').hide();
            $('.js_empty_item').show();
        }
    },

    notified : function (trigger, id) {
        var notified_count = parseInt($('.notified_counter').text());

        $.get('/guests/notified/id/'+id, '', function (result) {
            if (result == '1') {
                notified_count++;
                $('#item_'+id+' :checkbox').attr('checked', true);
            } else if (result == '0') {
                notified_count--;
                $('#item_'+id+' :checkbox').attr('checked', false);
            }
            $('.notified_counter').text(notified_count);
        });
        return false;
    },

    uploadPhoto : function (button) {
        var old_thumbs = $('#image_thumbs').html();

        new AjaxUpload(button, {
            action : '/file/uploadImg',
            name : 'image',
            onSubmit : function (file, ext) {
                $('#image_thumbs').html($.ajaxLoader());
                $('#upload_photo').hide();
            },
            onComplete : function (file, response) {
                var img = $.parseJSON(response);

                if (img.status) {
                    if (img.status == 'ok') {
                        $('#img_id').val(img.img_id);
                        $('#img_filename').val(img.file_name);
                        $('#image_thumbs').html('<img src="/'+img.path+'/medium/'+img.file_name+'"> <img src="/'+img.path+'/small/'+img.file_name+'">');
                        old_thumbs = $('#image_thumbs').html();
                    } else {
                        alert(img.msg);
                        $('#image_thumbs').html(old_thumbs);
                    }
                } else {
                    alert(response);
                }
                $('#upload_photo').show();
            }
        });
    }
};