/* MTooltip
 * Dmitry Marinin 2012 */

var tooltip = {
    data : {
        timers : new Array()
    },
    show : function (trigger, content, params, event) {

        tooltip.hide(); // hide other tooltips

        $(trigger).live('mouseout', function() {
            tooltip.clearTimers();
            tooltip.hide();
        });

        $(trigger).live('click', function() {
            tooltip.clearTimers();
            tooltip.hide();
        });

        if (!empty(params.delay)) {
            var delay_time = params.delay;
            params.delay = '';

            tooltip.clearTimers();
            tooltip.data.timers.push(setTimeout(function () {
                tooltip.show($(trigger), content, params);
            }, delay_time));

            return false;
        }

        var topPos = $(trigger).offset().top;
        var leftPos = $(trigger).offset().left;
        var rightPos = $(window).width() - leftPos;
        var bottomPos = $(window).height() - $(trigger).height() - topPos;

        var cursor_top = $(window).pageY;
        var cursor_left = $(window).pageX;
        var cursor_right = $(window).width() - cursor_left;
        var cursor_bottom = $(window).height() - cursor_top;

        $('body').append('<div id="tooltip">'+ content + '</div>');
        var tooltip_width =  $(trigger).css('width').split('px')[0];

        $('#tooltip').hide().stop().addClass('tooltip');

        if (empty(params.special_class)) {
            $('#tooltip').addClass('default');
        }
        else {
            $('#tooltip').addClass(params.special_class);
        }

        if (empty(params.position))
            params.position = 'default';

        if (params.position == 'top-right') {
            $('#tooltip').css({left: leftPos - 8, bottom: bottomPos + 30}).addClass('tooltip-leftBottomArr');
        }
        else if (params.position == 'default') {
            $('#tooltip').css({left: leftPos + 20, top: topPos + 20});
        }

        $('#tooltip').fadeIn(200);
    },
    hide : function () {
        $('#tooltip').stop().fadeOut(200).remove();
    },
    clearTimers : function() {
        for (var i = 0; i < tooltip.data.timers.length; i++)
            clearTimeout(tooltip.data.timers[i]);
    }
};